<?php 

  require_once 'header.php';
  $controller = new ControllerDealer();

  $extras = new Extras();
  $dealer_id = $extras->decryptQuery1(KEY_SALT, $_SERVER['QUERY_STRING']);

  if($dealer_id != null) {

        $dealer = $controller->getDealerByDealerId($dealer_id);
        
        if( isset($_POST['submit']) ) {
          $itm = new Dealer();
          $itm->dealer_id = $dealer->dealer_id;
          $itm->lat = trim(strip_tags($_POST['lat']));
          $itm->lon = trim(strip_tags($_POST['lon']));
          $itm->contact_no = trim(strip_tags($_POST['contact_no']));
          $itm->email = trim(strip_tags($_POST['email']));
          $itm->fb = $extras->removeHttp( trim(strip_tags($_POST['fb'])) );
          $itm->twitter = $extras->removeHttp( trim(strip_tags($_POST['twitter'])) );
          $itm->website = $extras->removeHttp( trim(strip_tags($_POST['website'])) );
          $itm->address = trim(strip_tags($_POST['address']));
          $itm->dealer_name = trim(strip_tags($_POST['dealer_name']));
          $itm->details = $extras->removeHttp( trim(strip_tags($_POST['details'])) );
          $itm->sales_hours_monday_friday = trim(strip_tags($_POST['sales_hours_monday_friday']));
          $itm->sales_hours_saturday = trim(strip_tags($_POST['sales_hours_saturday']));
          $itm->sales_hours_sunday = trim(strip_tags($_POST['sales_hours_sunday']));
          $itm->service_hours_monday_friday = trim(strip_tags($_POST['service_hours_monday_friday']));
          $itm->service_hours_saturday = trim(strip_tags($_POST['service_hours_saturday']));
          $itm->service_hours_sunday = trim(strip_tags($_POST['service_hours_sunday']));

          $controller->updateDealer($itm);
          echo "<script type='text/javascript'>location.href='dealers.php';</script>";
        }
  }
  else {
        echo "<script type='text/javascript'>location.href='403.php';</script>";
  }

?>


<!DOCTYPE html>
<html lang="en"><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="http://getbootstrap.com/assets/ico/favicon.ico">

    <title>Car Finder</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="bootstrap/css/navbar-fixed-top.css" rel="stylesheet">
    <link href="bootstrap/css/custom.css" rel="stylesheet">


    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBB7Tce0Xd3GEb838FF5uRcIe8MQIRdQSo&sensor=false"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script type="text/javascript" src="maps.plugin.js"></script>
    <script type="text/javascript">
        $(function(){
            var mapDiv = document.getElementById('map');
            var map = new google.maps.Map(mapDiv, {
              center: new google.maps.LatLng(42.766727, -72.995293),
              zoom: 10,
              mapTypeId: google.maps.MapTypeId.ROADMAP,

            });

            var marker = new google.maps.Marker({
                    position: new google.maps.LatLng( <?php echo "$dealer->lat, $dealer->lon";?>, true ),
                    map: map,
                    title: 'Here!'
                });

            if(marker != null) {
                map.setCenter(marker.getPosition());
            }


            google.maps.event.addListener(map, 'click', function (mouseEvent) {

                if(marker != null)
                  marker.setMap(null);

                var lat = document.getElementById('latitude');
                var longi = document.getElementById('longitude');
                lat.value = mouseEvent.latLng.lat(); //alert(mouseEvent.latLng.toUrlValue());
                longi.value = mouseEvent.latLng.lng();

                marker = new google.maps.Marker({
                    position: mouseEvent.latLng,
                    map: map,
                    title: 'Here!'
                });

            });

        });

        function validateLatLng(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode( key );

            if(theEvent.keyCode == 8 || theEvent.keyCode == 127) {
                
            }
            else {
                var regex = /[0-9.]|\./;
                if( !regex.test(key) ) {
                  theEvent.returnValue = false;
                  if(theEvent.preventDefault) theEvent.preventDefault();
                }  
            }
        }
    </script>


    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <!-- Fixed navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container">


        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Car Finder</a>
        </div>


        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li ><a href="home.php">Home</a></li>
            <li class="active"><a href="dealers.php">Dealers</a></li>
            <li><a href="sellers.php">Sellers</a></li>
            <li><a href="make.php">Make</a></li>
            <li><a href="cars.php">Cars</a></li>
            <li ><a href="users.php">Users</a></li>
            <li ><a href="admin_users.php">Admin (Users)</a></li>
          </ul>
          
          <ul class="nav navbar-nav navbar-right">
            <li ><a href="index.php">Logout</a></li>
          </ul>
        </div><!--/.nav-collapse -->
        
      </div>
    </div>

    <div class="container">

      <!-- Example row of columns -->
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Update Dealer</h3>
        </div>

        <div class="panel-body">
              <div class="row">
                <div class="col-md-7">

                  <form action="" method="POST">

                        <div class="input-group">
                        <span class="input-group-addon"></span>
                        <input type="text" class="form-control" placeholder="Dealer Name" name="dealer_name" required value="<?php echo $dealer->dealer_name; ?>">
                      </div>

                      <br />
                      <div class="input-group">
                        <span class="input-group-addon"></span>
                        <input type="text" class="form-control" placeholder="Contact No" name="contact_no" required value="<?php echo $dealer->contact_no; ?>">
                      </div>

                      <br />
                      <div class="input-group">
                        <span class="input-group-addon"></span>
                        <input type="email" class="form-control" placeholder="Email" name="email" required value="<?php echo $dealer->email; ?>">
                      </div>

                      <br />
                      <div class="input-group">
                        <span class="input-group-addon"></span>
                        <input type="text" class="form-control" placeholder="Facebook" name="fb" value="<?php echo $dealer->fb; ?>" id="fb">
                      </div>

                      <br />
                      <div class="input-group">
                        <span class="input-group-addon"></span>
                        <input type="text" class="form-control" placeholder="Twitter" name="twitter" value="<?php echo $dealer->twitter; ?>" id="twitter">
                      </div>

                      <br />
                      <div class="input-group">
                        <span class="input-group-addon"></span>
                        <input type="text" class="form-control" placeholder="Website" name="website" value="<?php echo $dealer->website; ?>" id="website">
                      </div>

                      <br />
                      <div class="input-group">
                        <span class="input-group-addon"></span>
                        <input type="text" class="form-control" placeholder="Address" name="address" required value="<?php echo $dealer->address; ?>">
                      </div>

                      <br />
                      <div class="input-group">
                        <span class="input-group-addon"></span>
                        <input type="text" class="form-control" placeholder="Sales Hour Monday - Friday" name="sales_hours_monday_friday" value="<?php echo $dealer->sales_hours_monday_friday; ?>" >
                      </div>

                      <br />
                      <div class="input-group">
                        <span class="input-group-addon"></span>
                        <input type="text" class="form-control" placeholder="Sales Hour Saturday" name="sales_hours_saturday" value="<?php echo $dealer->sales_hours_saturday; ?>" >
                      </div>

                      <br />
                      <div class="input-group">
                        <span class="input-group-addon"></span>
                        <input type="text" class="form-control" placeholder="Sales Hour Sunday" name="sales_hours_sunday" value="<?php echo $dealer->sales_hours_sunday; ?>" >
                      </div>

                      <br />
                      <div class="input-group">
                        <span class="input-group-addon"></span>
                        <input type="text" class="form-control" placeholder="Service Hour Monday - Friday" name="service_hours_monday_friday" value="<?php echo $dealer->service_hours_monday_friday; ?>" >
                      </div>

                      <br />
                      <div class="input-group">
                        <span class="input-group-addon"></span>
                        <input type="text" class="form-control" placeholder="Service Hour Saturday" name="service_hours_saturday" value="<?php echo $dealer->service_hours_saturday; ?>" >
                      </div>

                      <br />
                      <div class="input-group">
                        <span class="input-group-addon"></span>
                        <input type="text" class="form-control" placeholder="Service Hour Sunday" name="service_hours_sunday" value="<?php echo $dealer->service_hours_sunday; ?>" >
                      </div>

                      <br />
                      <div class="input-group">
                        <span class="input-group-addon"></span>
                        <input type="text" class="form-control" placeholder="Click on the Map for Latitude" name="lat" onkeypress='validateLatLng(event)' id="latitude" required value="<?php echo $dealer->lat; ?>">
                      </div>

                      <br />
                      <div class="input-group">
                        <span class="input-group-addon"></span>
                        <input type="text" class="form-control" placeholder="Click on the Map for Longitude" name="lon" onkeypress='validateLatLng(event)' id="longitude" required value="<?php echo $dealer->lon; ?>">
                      </div>

                      <br />
                      <div class="input-group">
                        <span class="input-group-addon"></span>
                        <textarea type="text" class="form-control" placeholder="Details" rows="10" name="details" id="details"><?php echo $dealer->details; ?></textarea>
                      </div>

                      <br /> 
                      <p>
                          <button type="submit" name="submit" class="btn btn-info" onclick="checkInput()" role="button">Save</button> 
                          <a class="btn btn-info" href="dealers.php" role="button">Cancel</a>
                      </p>
                  </form> 
                  


                </div>
                <div class="col-md-5">
                  <h4>Click the Map to get latitude/longitude:</h4>
                  <div id="map" style="width:100%; height:400px"></div>
               </div>
        </div>
      </div>


    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="bootstrap/js/jquery.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
    <script>
        function checkInput() {
            var fb = document.getElementById("fb");
            var twitter = document.getElementById("twitter");
            var website = document.getElementById("website");
            var details = document.getElementById("details");


            var strFb = fb.value.replace("http://", "");
            strFb = strFb.replace("https://", "");
            fb.value = strFb;

            var strTwitter = twitter.value.replace("http://", "");
            strFb = strTwitter.replace("https://", "");
            twitter.value = strTwitter;

            var strWebsite = website.value.replace("http://", "");
            strFb = strWebsite.replace("https://", "");
            website.value = strWebsite;

            var strDetails = details.value.replace("http://", "");
            strFb = strDetails.replace("https://", "");
            details.value = strDetails;
        }
    </script>
    
  

</body></html>