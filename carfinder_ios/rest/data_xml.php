<?php

  require '../controllers/ControllerRest.php';
  $controllerRest = new ControllerRest('../application/DB_Connect.php');

  $resultPhotoCar = $controllerRest->getPhotoCarsResult();
  $resultPhotoDealers = $controllerRest->getPhotoDealersResult();
  $resultMake = $controllerRest->getMakeResult();
  $resultSellers = $controllerRest->getSellersResult();
  $resultCars = $controllerRest->getCarsResult();
  $resultDealers = $controllerRest->getDealersResult();

  
  header ("content-type: text/xml");
  // header("Content-Type: application/xml; charset=ISO-8859-1");
  echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
  echo "<data>";

      // PHOTO CARS
      echo "<photo_cars>";
      foreach ($resultPhotoCar as $row) 
      {
          echo"<item>";
          foreach ($row as $columnName => $field) 
          {

            $val = trim(strip_tags($field));
            if(!is_numeric($columnName)) {
                echo "<$columnName>$val</$columnName>";
            }
          }
          echo"</item>";
      }
      echo "</photo_cars>";
      

      // PHOTO DEALERS
      echo "<photo_dealers>";
      foreach ($resultPhotoDealers as $row) 
      {
          echo"<item>";
          foreach ($row as $columnName => $field) 
          {

            $val = trim(strip_tags($field));
            if(!is_numeric($columnName)) {
                echo "<$columnName>$val</$columnName>";
            }
          }
          echo"</item>";
      }
      echo "</photo_dealers>";


      // MAKE
      echo "<make>";
      foreach ($resultMake as $row) 
      {
          echo"<item>";
          foreach ($row as $columnName => $field) 
          {

            $val = trim(strip_tags($field));
            if(!is_numeric($columnName)) {
                echo "<$columnName>$val</$columnName>";
            }
          }
          echo"</item>";
      }
      echo "</make>";


      // SELLERS
      echo "<sellers>";
      foreach ($resultSellers as $row) 
      {
          echo"<item>";
          foreach ($row as $columnName => $field) 
          {

            $val = trim(strip_tags($field));
            if(!is_numeric($columnName)) {
                echo "<$columnName>$val</$columnName>";
            }
          }
          echo"</item>";
      }
      echo "</sellers>";


      // CARS
      echo "<cars>";
      foreach ($resultCars as $row) 
      {
          echo"<item>";
          foreach ($row as $columnName => $field) 
          {

            $val = trim(strip_tags($field));
            if(!is_numeric($columnName)) {
                echo "<$columnName>$val</$columnName>";
            }
          }
          echo"</item>";
      }
      echo "</cars>";

      // DEALERS
      echo "<dealers>";
      foreach ($resultDealers as $row) 
      {
          echo"<item>";
          foreach ($row as $columnName => $field) 
          {

            $val = trim(strip_tags($field));
            if(!is_numeric($columnName)) {
                echo "<$columnName>$val</$columnName>";
            }
          }
          echo"</item>";
      }
      echo "</dealers>";
  
  echo "</data>";

?>