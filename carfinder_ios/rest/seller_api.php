<?php

    require_once '../models/Seller.php';
    require '../controllers/ControllerRestSeller.php';
    require_once '../models/User.php';
    require '../controllers/ControllerUser.php';
    
    $controllerRestSeller = new ControllerRestSeller('../application/DB_Connect.php');
    $controllerUser = new ControllerUser('../application/DB_Connect.php');

    if( !empty($_POST['user_id']) )
        $user_id = $_POST['user_id'];

    if( !empty($_POST['login_hash']) )
        $login_hash = $_POST['login_hash'];

    if( !empty($_POST['seller_name']) )
        $seller_name = trim(strip_tags($_POST['seller_name']));

    if( !empty($_POST['contact_no']) )
        $contact_no = trim(strip_tags($_POST['contact_no']));

    if( !empty($_POST['email']) )
        $email = trim(strip_tags($_POST['email']));

    if( !empty($_POST['lat']) )
        $lat = $_POST['lat'];

    if( !empty($_POST['lon']) )
        $lon = $_POST['lon'];

    if( !empty($_POST['sms']) )
        $sms = trim(strip_tags($_POST['sms']));

    if( !empty($_POST['address']) )
        $address = trim(strip_tags($_POST['address']));

    if( !empty($seller_name) && !empty($contact_no) && !empty($email) && 
        !empty($lat) && !empty($lon) && !empty($sms) && !empty($address) && !empty($login_hash) && !empty($user_id) ) {
          
        $user = $controllerUser->getUserByUserId($user_id);

        $login_hash = str_replace(" ", "+", $login_hash);
        if($user != null) {
            
            if($user->login_hash == $login_hash) {

                $seller = $controllerRestSeller->getUserSellerIfExist($user_id);

                $itm = new Seller();
                $itm->seller_name = $seller_name;
                $itm->contact_no = $contact_no;
                $itm->email = $email;
                $itm->lat = $lat;
                $itm->lon = $lon;
                $itm->sms = $sms;
                $itm->address = $address;
                $itm->user_id = $user_id;

                if($seller != null) {
                    $itm->seller_id = $seller->seller_id;
                    $controllerRestSeller->updateSeller($itm);
                }
                else {
                    $controllerRestSeller->insertSeller($itm);
                }

                $itm = $controllerRestSeller->getUserSellerIfExist($user_id);

                $json = "{
                        \"seller_info\" : {
                                      \"seller_id\" : \"$itm->seller_id\",
                                      \"seller_name\" : \"$itm->seller_name\",
                                      \"contact_no\" : \"$itm->contact_no\",
                                      \"email\" : \"$itm->email\",
                                      \"lat\" : \"$itm->lat\",
                                      \"lon\" : \"$itm->lon\",
                                      \"sms\" : \"$itm->sms\",
                                      \"address\" : \"$itm->address\",
                                      \"user_id\" : \"$itm->user_id\",
                                      \"profile_pic\" : \"$itm->profile_pic\"
                                      },
                        \"status\" : {
                                      \"status_code\" : \"-1\",
                                      \"status_text\" : \"Success.\"
                                    }
                        }";
            }
            else {

                $json = "{
                        \"status\" : {
                                      \"status_code\" : \"5\",
                                      \"status_text\" : \"It seems you are out of sync. Please relogin again.\"
                                    }
                        }";
            }
        }
        else {
            $json = "{
                          \"status\" : {
                                        \"status_code\" : \"5\",
                                        \"status_text\" : \"It seems you are out of sync. Please relogin again.\"
                                      }
                          }";
        }
        
    }
    else {

        $json = "{
                  \"status\" : {
                                \"status_code\" : \"3\",
                                \"status_text\" : \"Invalid Access.\"
                              }
                  }";

        
    }

    echo $json;

?>