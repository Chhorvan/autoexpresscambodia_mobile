<?php

    require_once '../models/Car.php';
    require '../controllers/ControllerRestCar.php';
    require_once '../models/User.php';
    require '../controllers/ControllerUser.php';
    
    $controllerRestCar = new ControllerRestCar('../application/DB_Connect.php');
    $controllerUser = new ControllerUser('../application/DB_Connect.php');


    if( !empty($_POST['user_id']) )
        $user_id = $_POST['user_id'];

    if( !empty($_POST['login_hash']) )
        $login_hash = $_POST['login_hash'];

    $car_id = 0;
    if( !empty($_POST['car_id']) )
        $car_id = $_POST['car_id'];

    $body_style = "";
    if( !empty($_POST['body_style']) )
        $body_style = trim(strip_tags($_POST['body_style']));

    $car_type = 0;
    if( !empty($_POST['car_type']) )
        $car_type = trim(strip_tags($_POST['car_type']));
    //$car_type = "";
      //if( empty($_POST['car_type']) )
      //$car_type="Used";
      //if( !empty($_POST['car_type']) )
        //$car_type = trim(strip_tags($_POST['car_type']));

    $dealer_id = 0;
    if( !empty($_POST['dealer_id']) )
        $dealer_id = $_POST['dealer_id'];

    $desc1 = "";
    if( !empty($_POST['desc1']) )
        $desc1 = trim(strip_tags($_POST['desc1']));

    $doors = "";
    if( !empty($_POST['doors']) )
        $doors = trim(strip_tags($_POST['doors']));

    $drivetrain = "";
    if( !empty($_POST['drivetrain']) )
        $drivetrain = trim(strip_tags($_POST['drivetrain']));

    $engine = "";
    if( !empty($_POST['engine']) )
        $engine = trim(strip_tags($_POST['engine']));

    $exterior_color = "";
    if( !empty($_POST['exterior_color']) )
        $exterior_color = trim(strip_tags($_POST['exterior_color']));

    $featured = "";
    if( !empty($_POST['featured']) )
        $featured = $_POST['featured'];

    $fuel_type = "";
    if( !empty($_POST['fuel_type']) )
      $fuel_type = trim(strip_tags($_POST['fuel_type']));

    $interior_color = "";
    if( !empty($_POST['interior_color']) )
        $interior_color = trim(strip_tags($_POST['interior_color']));

    $make = "";
    if( !empty($_POST['make']) )
        $make = trim(strip_tags($_POST['make']));

    $mileage = "";
    if( !empty($_POST['mileage']) )
        $mileage = trim(strip_tags($_POST['mileage']));

    $model = "";
    if( !empty($_POST['model']) )
        $model = trim(strip_tags($_POST['model']));

    $name = "";
    if( !empty($_POST['name']) )
        $name = trim(strip_tags($_POST['name']));

    $notes = "";
    if( !empty($_POST['notes']) )
        $notes = trim(strip_tags($_POST['notes']));

    $price = "";
    if( !empty($_POST['price']) )
        $price = trim(strip_tags($_POST['price']));

    $transmission = "";
    if( !empty($_POST['transmission']) )
        $transmission = trim(strip_tags($_POST['transmission']));

    $wheelbase = "";
    if( !empty($_POST['wheelbase']) )
        $wheelbase = trim(strip_tags($_POST['wheelbase']));

    $year = "";
    if( !empty($_POST['year']) )
        $year = trim(strip_tags($_POST['year']));

    $seller_id = 0;
    if( !empty($_POST['seller_id']) )
        $seller_id = $_POST['seller_id'];

    $is_deleted = 0;
    if( !empty($_POST['is_deleted']) )
        $is_deleted = $_POST['is_deleted'];

    $steering = 0;
    if( !empty($_POST['steering']) )
        $steering = $_POST['steering'];
    //$steering = "";
      //if( empty($_POST['steering']) )
        //$steering ="LHD";
      //if( !empty($_POST['steering']) )
        //$steering = trim(strip_tags($_POST['steering']));

    $license_plate = 0;
    if( !empty($_POST['license_plate']) )
        $license_plate = $_POST['license_plate'];
    //$license_plate = "";
      //if( empty($_POST['license_plate']) )
        //$license_plate ="Yes";
      //if( !empty($_POST['license_plate']) )
        //$license_plate = trim(strip_tags($_POST['license_plate']));

    $license_paper = 0;
    if( !empty($_POST['license_paper']) )
        $license_paper = $_POST['license_paper'];
    //$license_paper = "";
      //if( empty($_POST['license_paper']) )
        //$license_paper ="Yes";
      //if( !empty($_POST['license_paper']) )
        //$license_paper = trim(strip_tags($_POST['license_paper']));
        
    $city = "";
    if( !empty($_POST['city']) )
        $city = $_POST['city'];
        
    $owner = 0;
    if( !empty($_POST['owner']) )
        $owner = $_POST['owner'];

    if( !empty($car_id) > 0 && !empty($login_hash) && !empty($user_id) && $is_deleted == 1 ) {
        
        $user = $controllerUser->getUserByUserId($user_id);
        $login_hash = str_replace(" ", "+", $login_hash);
        
        if($user != null) {
            
            if($user->login_hash == $login_hash) {
                $controllerRestCar->deleteCar($car_id, 1);
                $json = "{
                        \"car_info\" : {
                                        \"car_id\" : \"$car_id\",
                                        \"is_deleted\" : \"1\"
                                      },
                        \"status\" : {
                                        \"status_code\" : \"-1\",
                                        \"status_text\" : \"Success.\"
                                    }
                        }";

            }
            else {

                $json = "{
                        \"status\" : {
                                      \"status_code\" : \"5\",
                                      \"status_text\" : \"It seems you are out of sync. Please relogin again.\"
                                    }
                        }";
            }
        }
    }

    else if( !empty($car_id) >= 0 && !empty($login_hash) && !empty($user_id) && (!empty($dealer_id) > 0 || !empty($seller_id) > 0) ) {
          
        $user = $controllerUser->getUserByUserId($user_id);

        $login_hash = str_replace(" ", "+", $login_hash);
        if($user != null) {
            
            if($user->login_hash == $login_hash) {

                $car = $controllerRestCar->getCarIfExist($car_id);

                $itm = new Car();
                $itm->car_id = $car_id;
                $itm->body_style = $body_style;
                $itm->car_type = $car_type;
                $itm->dealer_id = $dealer_id;
                $itm->desc1 = $steering;
                $itm->doors = $license_plate;
                $itm->drivetrain = $drivetrain;
                $itm->engine = $engine;
                $itm->exterior_color = $exterior_color;
                $itm->featured = $featured;
                $itm->fuel_type = $fuel_type;
                $itm->interior_color = $license_paper;
                $itm->make = $make;
                $itm->mileage = $mileage;
                $itm->model = $model;
                $itm->name = $name;
                $itm->notes = $notes;
                $itm->price = $price;
                $itm->transmission = $transmission;
                $itm->wheelbase = $wheelbase;
                $itm->year = $year;
                $itm->steering = $steering;
                $itm->license_plate = $license_plate;
                $itm->license_paper = $license_paper;
                $itm->city = $city;
                $itm->owner = $owner;
                $itm->seller_id = $seller_id;

                if($car != null) {
                    $controllerRestCar->updateCar($itm);
                }
                else {
                    $controllerRestCar->insertCar($itm);
                    $car_id = $controllerRestCar->getLastInsertedCarId();
                }

                $itm = $controllerRestCar->getCarIfExist( $car_id );

                $json = "{
                        \"car_info\" : {
                                      \"car_id\" : \"$itm->car_id\",
                                      \"body_style\" : \"$itm->body_style\",
                                      \"car_type\" : \"$itm->car_type\",
                                      \"dealer_id\" : \"$itm->dealer_id\",
                                      \"doors\" : \"$itm->doors\",
                                      \"drivetrain\" : \"$itm->drivetrain\",
                                      \"engine\" : \"$itm->engine\",
                                      \"featured\" : \"$itm->featured\",
                                      \"fuel_type\" : \"$itm->fuel_type\",
                                      \"interior_color\" : \"$itm->interior_color\",
                                      \"is_deleted\" : \"0\",

                                      \"make\" : \"$itm->make\",
                                      \"mileage\" : \"$itm->mileage\",
                                      \"model\" : \"$itm->model\",
                                      \"name\" : \"$itm->name\",
                                      \"notes\" : \"$itm->notes\",
                                      \"price\" : \"$itm->price\",
                                      \"transmission\" : \"$itm->transmission\",
                                      \"wheelbase\" : \"$itm->wheelbase\",
                                      \"year\" : \"$itm->year\",
                                      \"steering\" : \"$itm->steering\",
                                      \"license_plate\" : \"$itm->license_plate\",
                                      \"license_paper\" : \"$itm->license_paper\",
                                      \"city\" : \"$itm->city\",
                                      \"owner\" : \"$itm->owner\",
                                      \"seller_id\" : \"$itm->seller_id\"
                                      },
                        \"status\" : {
                                      \"status_code\" : \"-1\",
                                      \"status_text\" : \"Success.\"
                                    }
                        }";
            }
            else {

                $json = "{
                        \"status\" : {
                                      \"status_code\" : \"5\",
                                      \"status_text\" : \"It seems you are out of sync. Please relogin again.\"
                                    }
                        }";
            }
        }
        else {
            $json = "{
                          \"status\" : {
                                        \"status_code\" : \"5\",
                                        \"status_text\" : \"It seems you are out of sync. Please relogin again.\"
                                      }
                          }";
        }
        
    }
    else {

        $json = "{
                  \"status\" : {
                                \"status_code\" : \"3\",
                                \"status_text\" : \"Invalid Access.\"
                              }
                  }";

        
    }

    echo $json;

?>