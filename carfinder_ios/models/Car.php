<?php
 
class Car
{
    public $car_id;
    public $body_style;
    public $car_type;
    public $dealer_id;
    public $desc1;
    public $doors;
    public $drivetrain;
    public $engine;
    public $exterior_color;
    public $featured;
    public $fuel_type;
    public $interior_color;
    public $make;
    public $mileage;
    public $model;
    public $name;
    public $notes;
    public $price;
    public $transmission;
    public $wheelbase;
    public $year;
    public $steering;
    public $license_plate;
    public $license_paper;
    public $owner;
    public $seller_id;
    public $city;


    // constructor
    function __construct() 
    {

    }
 
    // destructor
    function __destruct() 
    {
         
    }
}
 
?>