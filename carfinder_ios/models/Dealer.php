<?php
 
class Dealer
{
	public $dealer_id;
    public $lat;
    public $lon;
    public $contact_no;
    public $email;
    public $fb;
    public $twitter;
    public $website;
    public $address;
    public $dealer_name;
    public $details;
    public $sales_hours_monday_friday;
    public $sales_hours_saturday;
    public $sales_hours_sunday;
    public $service_hours_monday_friday;
    public $service_hours_saturday;
    public $service_hours_sunday;
    public $user_id;

    // constructor
    function __construct() 
    {

    }
 
    // destructor
    function __destruct() 
    {
         
    }
}
 
?>