<?php

class ControllerRestCar
{
 
    private $db;
    private $pdo;
    function __construct($db_path) 
    {
        require_once $db_path;

        // connecting to database
        $this->db = new DB_Connect();
        $this->pdo = $this->db->connect();
    }

    function setClassPath($class_path) {
        require_once $class_path;
    }
 
    function __destruct() { }

    public function deleteCar($car_id, $is_deleted) 
    {
        $stmt = $this->pdo->prepare('UPDATE product 
                                        SET is_deleted = :is_deleted 
                                        WHERE car_id = :car_id ');
        
        $result = $stmt->execute(
                            array('car_id' => $car_id, 
                                    'is_deleted' => $is_deleted) );

        return $result ? true : false;
    }
 
    public function updateCar($itm) 
    {
        $stmt = $this->pdo->prepare('UPDATE product 
                                        SET body_style = :body_style, 
                                            car_type = :car_type, 
                                            dealer_id = :dealer_id, 
                                            desc1 = :desc1, 
                                            doors = :doors, 
                                            drivetrain = :drivetrain, 
                                            engine = :engine, 
                                            exterior_color = :exterior_color, 
                                            featured = :featured, 
                                            fuel_type = :fuel_type, 
                                            interior_color = :interior_color, 
                                            make = :make, 
                                            mileage = :mileage, 
                                            model = :model, 
                                            name = :name, 
                                            notes = :notes, 
                                            price = :price, 
                                            transmission = :transmission, 
                                            wheelbase = :wheelbase, 
                                            year = :year, 
                                            city = :city,
                                            owner = :owner,
                                            seller_id = :seller_id 
                                        WHERE car_id = :car_id');

        $result = $stmt->execute(
                            array('body_style' => $itm->body_style,
                                    'car_type' => $itm->car_type,  
                                    'dealer_id' => $itm->dealer_id,
                                    'desc1' => $itm->desc1,
                                    'doors' => $itm->doors,
                                    'drivetrain' => $itm->drivetrain,
                                    'engine' => $itm->engine,
                                    'exterior_color' => $itm->exterior_color,
                                    'featured' => $itm->featured,
                                    'fuel_type' => $itm->fuel_type,
                                    'interior_color' => $itm->interior_color,
                                    'make' => $itm->make,
                                    'mileage' => $itm->mileage,
                                    'model' => $itm->model,
                                    'name' => $itm->name,
                                    'notes' => $itm->notes,
                                    'price' => $itm->price,
                                    'transmission' => $itm->transmission,
                                    'wheelbase' => $itm->wheelbase,
                                    'year' => $itm->year,
                                    'city' => $itm->city,
                                    'owner' => $itm->owner,
                                    'seller_id' => $itm->seller_id,
                                    'car_id' => $itm->car_id ) );
        
        return $result ? true : false;
    }

    public function insertCar($itm) 
    {
        $stmt = $this->pdo->prepare('INSERT INTO product( 
                                    body_style, 
                                    car_type, 
                                    dealer_id, 
                                    desc1, 
                                    doors, 
                                    drivetrain, 
                                    engine, 
                                    exterior_color, 
                                    featured, 
                                    fuel_type, 
                                    interior_color, 
                                    make, 
                                    mileage, 
                                    model, 
                                    name, 
                                    notes,
                                    price, 
                                    transmission, 
                                    wheelbase, 
                                    year, 
                                    city,
                                    owner,
                                    seller_id ) 

                                VALUES(
                                    :body_style, 
                                    :car_type, 
                                    :dealer_id, 
                                    :desc1, 
                                    :doors, 
                                    :drivetrain, 
                                    :engine, 
                                    :exterior_color, 
                                    :featured, 
                                    :fuel_type, 
                                    :interior_color, 
                                    :make, 
                                    :mileage, 
                                    :model, 
                                    :name, 
                                    :notes,
                                    :price, 
                                    :transmission, 
                                    :wheelbase, 
                                    :year, 
                                    :city,
                                    :owner,
                                    :seller_id )');
        
        $result = $stmt->execute(
                            array('body_style' => $itm->body_style,
                                    'car_type' => $itm->car_type,  
                                    'dealer_id' => $itm->dealer_id,
                                    'desc1' => $itm->desc1,
                                    'doors' => $itm->doors,
                                    'drivetrain' => $itm->drivetrain,
                                    'engine' => $itm->engine,
                                    'exterior_color' => $itm->exterior_color,
                                    'featured' => $itm->featured,
                                    'fuel_type' => $itm->fuel_type,
                                    'interior_color' => $itm->interior_color,
                                    'make' => $itm->make,
                                    'mileage' => $itm->mileage,
                                    'model' => $itm->model,
                                    'name' => $itm->name,
                                    'notes' => $itm->notes,
                                    'price' => $itm->price,
                                    'transmission' => $itm->transmission,
                                    'wheelbase' => $itm->wheelbase,
                                    'year' => $itm->year,
                                    'city' => $itm->city,
                                    'owner' => $itm->owner,
                                    'seller_id' => $itm->seller_id ) );
        
        return $result ? true : false;

    }

    public function getCarIfExist($car_id) 
    {
        $stmt = $this->pdo->prepare('SELECT * 
                                FROM product 
                                WHERE car_id = :car_id');
        
        $stmt->execute( array('car_id' => $car_id));

        foreach ($stmt as $row) 
        {
            $itm = new Car();
            $itm->car_id = $row['car_id'];
            $itm->body_style = $row['body_style'];
            $itm->car_type = $row['car_type'];
            $itm->dealer_id = $row['dealer_id'];
            $itm->desc1 = $row['desc1'];
            $itm->doors = $row['doors'];
            $itm->drivetrain = $row['drivetrain'];
            $itm->engine = $row['engine'];
            $itm->exterior_color = $row['exterior_color'];
            $itm->featured = $row['featured'];
            $itm->fuel_type = $row['fuel_type'];
            $itm->interior_color = $row['interior_color'];
            $itm->make = $row['make'];
            $itm->mileage = $row['mileage'];
            $itm->model = $row['model'];
            $itm->name = $row['name'];
            $itm->notes = $row['notes'];
            $itm->price = $row['price'];
            $itm->transmission = $row['transmission'];
            $itm->wheelbase = $row['wheelbase'];
            $itm->year = $row['year'];
            $itm->city = $row['city'];
            $itm->owner = $row['owner'];
            $itm->seller_id = $row['seller_id'];

            return $itm;
        } 
        
        return null;
    }

    public function getLastInsertedCarId() {

        return $this->pdo->lastInsertId(); 
    }
 
}
 
?>