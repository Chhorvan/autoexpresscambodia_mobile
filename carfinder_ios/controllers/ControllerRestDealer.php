<?php
 
class ControllerRestDealer
{
 
    private $db;
    private $pdo;
    function __construct($db_path) 
    {
        require_once $db_path;

        // connecting to database
        $this->db = new DB_Connect();
        $this->pdo = $this->db->connect();
    }

    function setClassPath($class_path) {
        require_once $class_path;
    }
 
    function __destruct() { }
 
    public function updateDealer($itm) 
    {
        $stmt = $this->pdo->prepare('UPDATE tbl_carfinder_dealers 
                                        SET 
                                            lat = :lat, 
                                            lon = :lon, 
                                            contact_no = :contact_no, 
                                            email = :email, 
                                            fb = :fb, 
                                            twitter = :twitter, 
                                            website = :website, 
                                            address = :address, 
                                            dealer_name = :dealer_name, 
                                            details = :details, 
                                            sales_hours_monday_friday = :sales_hours_monday_friday, 
                                            sales_hours_saturday = :sales_hours_saturday, 
                                            sales_hours_sunday = :sales_hours_sunday, 
                                            service_hours_monday_friday = :service_hours_monday_friday, 
                                            service_hours_saturday = :service_hours_saturday, 
                                            service_hours_sunday = :service_hours_sunday,
                                            user_id = :user_id  

                                        WHERE dealer_id = :dealer_id');

        $result = $stmt->execute(
                            array('lat' => $itm->lat,
                                    'lon' => $itm->lon,
                                    'contact_no' => $itm->contact_no,
                                    'email' => $itm->email,
                                    'fb' => $itm->fb,
                                    'twitter' => $itm->twitter,
                                    'website' => $itm->website,
                                    'address' => $itm->address,
                                    'dealer_name' => $itm->dealer_name,
                                    'details' => $itm->details,
                                    'sales_hours_monday_friday' => $itm->sales_hours_monday_friday, 
                                    'sales_hours_saturday' => $itm->sales_hours_saturday,
                                    'sales_hours_sunday' => $itm->sales_hours_sunday,
                                    'service_hours_monday_friday' => $itm->service_hours_monday_friday,
                                    'service_hours_saturday' => $itm->service_hours_saturday,
                                    'service_hours_sunday' => $itm->service_hours_sunday,
                                    'user_id' => $itm->user_id,
                                    'dealer_id' => $itm->dealer_id) );
        
        return $result ? true : false;
    }

    public function insertDealer($itm) 
    {
        $stmt = $this->pdo->prepare('INSERT INTO tbl_carfinder_dealers( 
                                    lat, 
                                    lon, 
                                    contact_no, 
                                    email, 
                                    fb, 
                                    twitter, 
                                    website, 
                                    address, 
                                    dealer_name, 
                                    details, 
                                    sales_hours_monday_friday, 
                                    sales_hours_saturday, 
                                    sales_hours_sunday, 
                                    service_hours_monday_friday, 
                                    service_hours_saturday, 
                                    service_hours_sunday,
                                    user_id ) 

                                VALUES(
                                    :lat, 
                                    :lon, 
                                    :contact_no, 
                                    :email, 
                                    :fb, 
                                    :twitter, 
                                    :website, 
                                    :address, 
                                    :dealer_name, 
                                    :details, 
                                    :sales_hours_monday_friday, 
                                    :sales_hours_saturday, 
                                    :sales_hours_sunday, 
                                    :service_hours_monday_friday, 
                                    :service_hours_saturday, 
                                    :service_hours_sunday,
                                    :user_id 
                                 )');

        $result = $stmt->execute(
                            array('lat' => $itm->lat,
                                    'lon' => $itm->lon,
                                    'contact_no' => $itm->contact_no,
                                    'email' => $itm->email,
                                    'fb' => $itm->fb,
                                    'twitter' => $itm->twitter,
                                    'website' => $itm->website,
                                    'address' => $itm->address,
                                    'dealer_name' => $itm->dealer_name,
                                    'details' => $itm->details,
                                    'sales_hours_monday_friday' => $itm->sales_hours_monday_friday, 
                                    'sales_hours_saturday' => $itm->sales_hours_saturday,
                                    'sales_hours_sunday' => $itm->sales_hours_sunday,
                                    'service_hours_monday_friday' => $itm->service_hours_monday_friday,
                                    'service_hours_saturday' => $itm->service_hours_saturday,
                                    'service_hours_sunday' => $itm->service_hours_sunday,
                                    'user_id' => $itm->user_id) );
    
        return $result ? true : false;
    }
 


    public function getUserDealerByUserId($user_id) 
    {
        $stmt = $this->pdo->prepare('SELECT * 
                                FROM tbl_carfinder_dealers 
                                WHERE user_id = :user_id');

        $stmt->execute( array('user_id' => $user_id));

        foreach ($stmt as $row) 
        {
            // do something with $row
            $itm = new Dealer();
            $itm->dealer_id = $row['dealer_id'];
            $itm->lat = $row['lat'];
            $itm->lon = $row['lon'];
            $itm->contact_no = $row['contact_no'];
            $itm->email = $row['email'];
            $itm->fb = $row['fb'];
            $itm->twitter = $row['twitter'];
            $itm->website = $row['website'];
            $itm->address = $row['address'];
            $itm->dealer_name = $row['dealer_name'];
            $itm->details = $row['details'];
            $itm->sales_hours_monday_friday = $row['sales_hours_monday_friday'];
            $itm->sales_hours_saturday = $row['sales_hours_saturday'];
            $itm->sales_hours_sunday = $row['sales_hours_sunday'];
            $itm->service_hours_monday_friday = $row['service_hours_monday_friday'];
            $itm->service_hours_saturday = $row['service_hours_saturday'];
            $itm->service_hours_sunday = $row['service_hours_sunday'];
            $itm->user_id = $row['user_id'];

            return $itm;
        }

        return null;
    }


}
 
?>