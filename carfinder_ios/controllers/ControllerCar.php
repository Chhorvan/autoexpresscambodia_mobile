<?php

require_once './models/Car.php';
 
class ControllerCar
{
 
    private $db;
    private $pdo;
    function __construct() 
    {
        require_once './application/DB_Connect.php';
        // connecting to database
        $this->db = new DB_Connect();
        $this->pdo = $this->db->connect();
    }
 
    function __destruct() { }
 
    public function updateCar($itm) 
    {
        $stmt = $this->pdo->prepare('UPDATE product 
                                        SET body_style = :body_style, 
                                            car_type = :car_type, 
                                            dealer_id = :dealer_id, 
                                            desc1 = :desc1, 
                                            doors = :doors, 
                                            drivetrain = :drivetrain, 
                                            engine = :engine, 
                                            exterior_color = :exterior_color, 
                                            featured = :featured, 
                                            fuel_type = :fuel_type, 
                                            interior_color = :interior_color, 
                                            make = :make, 
                                            mileage = :mileage, 
                                            model = :model, 
                                            name = :name, 
                                            notes = :notes, 
                                            price = :price, 
                                            transmission = :transmission, 
                                            wheelbase = :wheelbase, 
                                            year = :year, 
                                            city = :city,
                                            owner = :owner,
                                            seller_id = :seller_id 
                                        WHERE car_id = :car_id');

        $result = $stmt->execute(
                            array('body_style' => $itm->body_style,
                                    'car_type' => $itm->car_type,  
                                    'dealer_id' => $itm->dealer_id,
                                    'desc1' => $itm->desc1,
                                    'doors' => $itm->doors,
                                    'drivetrain' => $itm->drivetrain,
                                    'engine' => $itm->engine,
                                    'exterior_color' => $itm->exterior_color,
                                    'featured' => $itm->featured,
                                    'fuel_type' => $itm->fuel_type,
                                    'interior_color' => $itm->interior_color,
                                    'make' => $itm->make,
                                    'mileage' => $itm->mileage,
                                    'model' => $itm->model,
                                    'name' => $itm->name,
                                    'notes' => $itm->notes,
                                    'price' => $itm->price,
                                    'transmission' => $itm->transmission,
                                    'wheelbase' => $itm->wheelbase,
                                    'year' => $itm->year,
                                    'city' => $itm->city,
                                    'owner' => $itm->owner,
                                    'seller_id' => $itm->seller_id,
                                    'car_id' => $itm->car_id ) );
        
        return $result ? true : false;

    }


    public function deleteCar($car_id, $is_deleted) 
    {
        $stmt = $this->pdo->prepare('UPDATE product 
                                        SET is_deleted = :is_deleted 
                                        WHERE car_id = :car_id ');
        
        $result = $stmt->execute(
                            array('car_id' => $car_id, 
                                    'is_deleted' => $is_deleted) );

        return $result ? true : false;
    }

    public function updateCarFeatured($itm) 
    {
        $stmt = $this->pdo->prepare('UPDATE product 
                                        SET featured = :featured 
                                        WHERE car_id = :car_id ');
        
        $result = $stmt->execute(
                            array('car_id' => $itm->car_id, 
                                    'featured' => $itm->featured) );
        
        return $result ? true : false;
    }


    public function insertCar($itm) 
    {
        $stmt = $this->pdo->prepare('INSERT INTO product( 
                                    body_style, 
                                    car_type, 
                                    dealer_id, 
                                    desc1, 
                                    doors, 
                                    drivetrain, 
                                    engine, 
                                    exterior_color, 
                                    featured, 
                                    fuel_type, 
                                    interior_color, 
                                    make, 
                                    mileage, 
                                    model, 
                                    name, 
                                    notes,
                                    price, 
                                    transmission, 
                                    wheelbase, 
                                    year, 
                                    city,
                                    owner,
                                    seller_id ) 

                                VALUES(
                                    :body_style, 
                                    :car_type, 
                                    :dealer_id, 
                                    :desc1, 
                                    :doors, 
                                    :drivetrain, 
                                    :engine, 
                                    :exterior_color, 
                                    :featured, 
                                    :fuel_type, 
                                    :interior_color, 
                                    :make, 
                                    :mileage, 
                                    :model, 
                                    :name, 
                                    :notes,
                                    :price, 
                                    :transmission, 
                                    :wheelbase, 
                                    :year, 
                                    :city,
                                    :owner,
                                    :seller_id )');
        
        $result = $stmt->execute(
                            array('body_style' => $itm->body_style,
                                    'car_type' => $itm->car_type,  
                                    'dealer_id' => $itm->dealer_id,
                                    'desc1' => $itm->desc1,
                                    'doors' => $itm->doors,
                                    'drivetrain' => $itm->drivetrain,
                                    'engine' => $itm->engine,
                                    'exterior_color' => $itm->exterior_color,
                                    'featured' => $itm->featured,
                                    'fuel_type' => $itm->fuel_type,
                                    'interior_color' => $itm->interior_color,
                                    'make' => $itm->make,
                                    'mileage' => $itm->mileage,
                                    'model' => $itm->model,
                                    'name' => $itm->name,
                                    'notes' => $itm->notes,
                                    'price' => $itm->price,
                                    'transmission' => $itm->transmission,
                                    'wheelbase' => $itm->wheelbase,
                                    'year' => $itm->year,
                                    'city' => $itm->city,
                                    'owner' => $itm->owner,
                                    'seller_id' => $itm->seller_id ) );
        
        return $result ? true : false;
    }
 

    public function getCars() 
    {
        $stmt = $this->pdo->prepare('SELECT * 
                                        FROM product 
                                        WHERE is_deleted = 0 ORDER BY name ASC');
        
        $stmt->execute();

        $array = array();
        $ind = 0;
        foreach ($stmt as $row) 
        {
            $itm = new Car();
            $itm->car_id = $row['car_id'];
            $itm->body_style = $row['body_style'];
            $itm->car_type = $row['car_type'];
            $itm->dealer_id = $row['dealer_id'];
            $itm->desc1 = $row['desc1'];
            $itm->doors = $row['doors'];
            $itm->drivetrain = $row['drivetrain'];
            $itm->engine = $row['engine'];
            $itm->exterior_color = $row['exterior_color'];
            $itm->featured = $row['featured'];
            $itm->fuel_type = $row['fuel_type'];
            $itm->interior_color = $row['interior_color'];
            $itm->make = $row['make'];
            $itm->mileage = $row['mileage'];
            $itm->model = $row['model'];
            $itm->name = $row['name'];
            $itm->notes = $row['notes'];
            $itm->price = $row['price'];
            $itm->transmission = $row['transmission'];
            $itm->wheelbase = $row['wheelbase'];
            $itm->year = $row['year'];
            $itm->city = $row['city'];
            $itm->owner = $row['owner'];
            $itm->seller_id = $row['seller_id'];

            $array[$ind] = $itm;
            $ind++;
        } 
        
        return $array;
    }

    public function getCarsBySearching($search) 
    {
        $stmt = $this->pdo->prepare('SELECT * 
                                FROM product 
                                WHERE is_deleted = 0 AND name LIKE :search ORDER BY name ASC');
        
        $stmt->execute( array('search' => '%'.$search.'%'));

        $array = array();
        $ind = 0;
        foreach ($stmt as $row) 
        {
            $itm = new Car();
            $itm->car_id = $row['car_id'];
            $itm->body_style = $row['body_style'];
            $itm->car_type = $row['car_type'];
            $itm->dealer_id = $row['dealer_id'];
            $itm->desc1 = $row['desc1'];
            $itm->doors = $row['doors'];
            $itm->drivetrain = $row['drivetrain'];
            $itm->engine = $row['engine'];
            $itm->exterior_color = $row['exterior_color'];
            $itm->featured = $row['featured'];
            $itm->fuel_type = $row['fuel_type'];
            $itm->interior_color = $row['interior_color'];
            $itm->make = $row['make'];
            $itm->mileage = $row['mileage'];
            $itm->model = $row['model'];
            $itm->name = $row['name'];
            $itm->notes = $row['notes'];
            $itm->price = $row['price'];
            $itm->transmission = $row['transmission'];
            $itm->wheelbase = $row['wheelbase'];
            $itm->year = $row['year'];
            $itm->city = $row['city'];
            $itm->owner = $row['owner'];
            $itm->seller_id = $row['seller_id'];

            $array[$ind] = $itm;
            $ind++;
        } 
        
        return $array;
    }


    public function getCarByCarId($car_id) 
    {
        
        $stmt = $this->pdo->prepare('SELECT * 
                                FROM product 
                                WHERE car_id = :car_id');
        
        $stmt->execute( array('car_id' => $car_id));

        foreach ($stmt as $row) 
        {
            $itm = new Car();
            $itm->car_id = $row['car_id'];
            $itm->body_style = $row['body_style'];
            $itm->car_type = $row['car_type'];
            $itm->dealer_id = $row['dealer_id'];
            $itm->desc1 = $row['desc1'];
            $itm->doors = $row['doors'];
            $itm->drivetrain = $row['drivetrain'];
            $itm->engine = $row['engine'];
            $itm->exterior_color = $row['exterior_color'];
            $itm->featured = $row['featured'];
            $itm->fuel_type = $row['fuel_type'];
            $itm->interior_color = $row['interior_color'];
            $itm->make = $row['make'];
            $itm->mileage = $row['mileage'];
            $itm->model = $row['model'];
            $itm->name = $row['name'];
            $itm->notes = $row['notes'];
            $itm->price = $row['price'];
            $itm->transmission = $row['transmission'];
            $itm->wheelbase = $row['wheelbase'];
            $itm->year = $row['year'];
            $itm->city = $row['city'];
            $itm->owner = $row['owner'];
            $itm->seller_id = $row['seller_id'];

            return $itm;
        } 
        
        return null;
    }


    public function getCarFeatured() 
    {
        $stmt = $this->pdo->prepare('SELECT * 
                                FROM product 
                                WHERE featured = 1 AND is_deleted = 0');
        
        $stmt->execute();

        $array = array();
        $ind = 0;
        foreach ($stmt as $row) 
        {
            $itm = new Car();
            $itm->car_id = $row['car_id'];
            $itm->body_style = $row['body_style'];
            $itm->car_type = $row['car_type'];
            $itm->dealer_id = $row['dealer_id'];
            $itm->desc1 = $row['desc1'];
            $itm->doors = $row['doors'];
            $itm->drivetrain = $row['drivetrain'];
            $itm->engine = $row['engine'];
            $itm->exterior_color = $row['exterior_color'];
            $itm->featured = $row['featured'];
            $itm->fuel_type = $row['fuel_type'];
            $itm->interior_color = $row['interior_color'];
            $itm->make = $row['make'];
            $itm->mileage = $row['mileage'];
            $itm->model = $row['model'];
            $itm->name = $row['name'];
            $itm->notes = $row['notes'];
            $itm->price = $row['price'];
            $itm->transmission = $row['transmission'];
            $itm->wheelbase = $row['wheelbase'];
            $itm->year = $row['year'];
            $itm->city = $row['city'];
            $itm->owner = $row['owner'];
            $itm->seller_id = $row['seller_id'];

            $array[$ind] = $itm;
            $ind++;
        } 
        return $array;
    }

}
 
?>