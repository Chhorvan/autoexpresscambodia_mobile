<?php

require_once './models/PhotoDealer.php';
 
class ControllerPhotoDealer
{
 
    private $db;
    private $pdo;
    function __construct() 
    {
        require_once './application/DB_Connect.php';
        // connecting to database
        $this->db = new DB_Connect();
        $this->pdo = $this->db->connect();
    }
 
    function __destruct() { }
 
    public function updatePhotoDealer($itm) 
    {
        $stmt = $this->pdo->prepare('UPDATE tbl_carfinder_photo_dealers 
                                        SET 
                                            photo_url = :photo_url, 
                                            thumb_url = :thumb_url, 
                                            dealer_id = :dealer_id  

                                            WHERE photo_id = :photo_id');

        $result = $stmt->execute(
                            array('photo_url' => $itm->photo_url,
                                    'thumb_url' => $itm->thumb_url,
                                    'dealer_id' => $itm->dealer_id, 
                                    'photo_id' => $itm->photo_id) );
        
        return $result ? true : false;
    }

    public function deletePhotoDealer($photo_id, $is_deleted) 
    {
        $stmt = $this->pdo->prepare('UPDATE tbl_carfinder_photo_dealers 
                                
                                        SET 
                                            is_deleted = :is_deleted 

                                        WHERE photo_id = :photo_id');
        
        $result = $stmt->execute(
                            array('photo_id' => $photo_id,
                                    'is_deleted' => $is_deleted ) );
        
        return $result ? true : false;
    }


    public function insertPhotoDealer($itm) 
    {
        $stmt = $this->pdo->prepare('INSERT INTO tbl_carfinder_photo_dealers( 
                                            photo_url, 
                                            thumb_url, 
                                            dealer_id ) 

                                        VALUES(
                                            :photo_url, 
                                            :thumb_url, 
                                            :dealer_id )');

        $result = $stmt->execute(
                            array('photo_url' => $itm->photo_url,
                                    'thumb_url' => $itm->thumb_url,
                                    'dealer_id' => $itm->dealer_id ) );
        
        return $result ? true : false;
    }
 
    /**
     * Get user by email and password
     */
    public function getPhotoDealers() 
    {
        $stmt = $this->pdo->prepare('SELECT * 

                                        FROM tbl_carfinder_photo_dealers 
                                        WHERE is_deleted = 0');

        $stmt->execute();

        $array = array();
        $ind = 0;
        foreach ($stmt as $row) 
        {
            // do something with $row
            $itm = new PhotoDealer();
            $itm->photo_id = $row['photo_id'];
            $itm->photo_url = $row['photo_url'];
            $itm->thumb_url = $row['thumb_url'];
            $itm->dealer_id = $row['dealer_id'];

            $array[$ind] = $itm;
            $ind++;
        }
        return $array;
    }

    public function getPhotoDealersByDealerId($dealer_id) 
    {
        $stmt = $this->pdo->prepare('SELECT * 

                                        FROM tbl_carfinder_photo_dealers 
                                        WHERE dealer_id = :dealer_id AND is_deleted = 0');

        $result = $stmt->execute(
                            array('dealer_id' => $dealer_id) );

        $array = array();
        $ind = 0;
        foreach ($stmt as $row) 
        {
            // do something with $row
            $itm = new PhotoDealer();
            $itm->photo_id = $row['photo_id'];
            $itm->photo_url = $row['photo_url'];
            $itm->thumb_url = $row['thumb_url'];
            $itm->dealer_id = $row['dealer_id'];

            $array[$ind] = $itm;
            $ind++;
        }
        return $array;
    }


    public function getPhotoDealerByPhotoId($photo_id) 
    {
        $stmt = $this->pdo->prepare('SELECT * 

                                        FROM tbl_carfinder_photo_dealers 
                                        WHERE photo_id = :photo_id');

        $result = $stmt->execute(
                            array('photo_id' => $photo_id) );

        foreach ($stmt as $row) 
        {
            // do something with $row
            $itm = new PhotoDealer();
            $itm->photo_id = $row['photo_id'];
            $itm->photo_url = $row['photo_url'];
            $itm->thumb_url = $row['thumb_url'];
            $itm->dealer_id = $row['dealer_id'];

            return $itm;
        }
        return null;
    }


    public function getNoOfPhotosByDealerId($dealer_id) 
    {
        $stmt = $this->pdo->prepare('SELECT * 

                                        FROM tbl_carfinder_photo_dealers 
                                        WHERE dealer_id = :dealer_id AND is_deleted = 0');

        $result = $stmt->execute(
                            array('dealer_id' => $dealer_id) );

        $no_of_rows = $stmt->rowCount();

       return $no_of_rows;
    }

    public function getPhotoDealerByDealerId($dealer_id) 
    {
        $stmt = $this->pdo->prepare('SELECT * 

                                        FROM tbl_carfinder_photo_dealers 
                                        WHERE dealer_id = :dealer_id');

        $result = $stmt->execute(
                            array('dealer_id' => $dealer_id) );

        foreach ($stmt as $row) 
        {
            // do something with $row
            $itm = new PhotoDealer();
            $itm->photo_id = $row['photo_id'];
            $itm->photo_url = $row['photo_url'];
            $itm->thumb_url = $row['thumb_url'];
            $itm->dealer_id = $row['dealer_id'];

            return $itm;
        }
        return null;

    }


}
 
?>