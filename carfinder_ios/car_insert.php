<?php 

  require_once 'header.php';
  $controllerCar = new ControllerCar();
  $controllerMake = new ControllerMake();
  $controllerSeller = new ControllerSeller();
  $controllerDealer = new ControllerDealer();


  $make = $controllerMake->getMake();
  $sellers = $controllerSeller->getSellers();
  $dealers = $controllerDealer->getDealers();
  
  if( isset($_POST['submit']) ) {
    
    $itm = new Car();

    $itm->body_style = trim(strip_tags($_POST['body_style']));
    $itm->car_type = trim(strip_tags($_POST['car_type']));
    $itm->dealer_id = trim(strip_tags($_POST['dealer_id']));
    $itm->desc1 = trim(strip_tags($_POST['desc1']));
    $itm->doors = trim(strip_tags($_POST['doors']));
    $itm->drivetrain = trim(strip_tags($_POST['drivetrain']));
    $itm->engine = trim(strip_tags($_POST['engine']));
    $itm->exterior_color = trim(strip_tags($_POST['exterior_color']));
    $itm->featured = trim(strip_tags($_POST['featured']));
    $itm->fuel_type = trim(strip_tags($_POST['fuel_type']));
    $itm->interior_color = trim(strip_tags($_POST['interior_color']));
    $itm->make = trim(strip_tags($_POST['make']));
    $itm->mileage = trim(strip_tags($_POST['mileage']));
    $itm->model = trim(strip_tags($_POST['model']));
    $itm->name = trim(strip_tags($_POST['name']));
    $itm->notes = trim(strip_tags($_POST['notes']));
    $itm->price = trim(strip_tags($_POST['price']));
    $itm->transmission = trim(strip_tags($_POST['transmission']));
    $itm->wheelbase = trim(strip_tags($_POST['wheelbase']));
    $itm->year = trim(strip_tags($_POST['year']));
    $itm->steering = trim(strip_tags($_POST['steering']));
    $itm->license_plate = trim(strip_tags($_POST['license_plate']));
    $itm->license_paper = trim(strip_tags($_POST['license_paper']));
    $itm->seller_id = trim(strip_tags($_POST['seller_id']));

    $controllerCar->insertCar($itm);
    echo "<script type='text/javascript'>location.href='cars.php';</script>";

  }

?>


<!DOCTYPE html>
<html lang="en"><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="http://getbootstrap.com/assets/ico/favicon.ico">

    <title>Car Finder</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="bootstrap/css/navbar-fixed-top.css" rel="stylesheet">
    <link href="bootstrap/css/custom.css" rel="stylesheet">
    <script type="text/javascript">
        
        function validateNumber(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode( key );

            if(theEvent.keyCode == 8 || theEvent.keyCode == 127) {
                
            }
            else {
                var regex = /\d+/;
                if( !regex.test(key) ) {
                  theEvent.returnValue = false;
                  if(theEvent.preventDefault) theEvent.preventDefault();
                }  
            }
        }
    </script>


    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <!-- Fixed navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container">


        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Car Finder</a>
        </div>


        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li ><a href="home.php">Home</a></li>
            <li ><a href="dealers.php">Dealers</a></li>
            <li><a href="sellers.php">Sellers</a></li>
            <li><a href="make.php">Make</a></li>
            <li class="active"><a href="cars.php">Cars</a></li>
            <li ><a href="users.php">Users</a></li>
            <li ><a href="admin_users.php">Admin (Users)</a></li>
          </ul>
          
          <ul class="nav navbar-nav navbar-right">
            <li ><a href="index.php">Logout</a></li>
          </ul>
        </div><!--/.nav-collapse -->
        
      </div>
    </div>

    <div class="container">

      <!-- Example row of columns -->
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Add Car</h3>
        </div>

        <div class="panel-body">
              <div class="row">
                <div class="col-md-7">

                  <form action="" method="POST">

                      <div class="input-group">
                        <span class="input-group-addon"></span>
                        <input type="text" class="form-control" placeholder="Car Name" name="name" required>
                      </div>

                      <br />
                      <div class="input-group">
                        <span class="input-group-addon"></span>
                        <input type="text" class="form-control" placeholder="Model" name="model" required>
                      </div>

                      <br />
                      <div class="input-group">
                        <span class="input-group-addon"></span>
                        <input type="text" class="form-control" placeholder="Price" name="price" required>
                      </div>

                      <br />
                      <div class="input-group">
                        <span class="input-group-addon"></span>
                        <input type="text" class="form-control" placeholder="Year" name="year" onkeypress='validateNumber(event)'>
                      </div>

                      <br />
                      <div class="input-group">
                        <span class="input-group-addon"></span>
                        <input type="text" class="form-control" placeholder="Body Style" name="body_style">
                      </div>

                      <br />
                      <div class="input-group">
                        <span class="input-group-addon"></span>
                        <input type="text" class="form-control" placeholder="Description" name="desc1" >
                      </div>

                      <br />
                      <div class="input-group">
                        <span class="input-group-addon"></span>
                        <input type="text" class="form-control" placeholder="Doors" name="doors" >
                      </div>

                      <br />
                      <div class="input-group">
                        <span class="input-group-addon"></span>
                        <input type="text" class="form-control" placeholder="Drive Train" name="drivetrain" >
                      </div>

                      <br />
                      <div class="input-group">
                        <span class="input-group-addon"></span>
                        <input type="text" class="form-control" placeholder="Engine" name="engine" required>
                      </div>

                      <br />
                      <div class="input-group">
                        <span class="input-group-addon"></span>
                        <input type="text" class="form-control" placeholder="Exterior Color" name="exterior_color" >
                      </div>

                      <br />
                      <div class="input-group">
                        <span class="input-group-addon"></span>
                        <input type="text" class="form-control" placeholder="Fuel Type" name="fuel_type" >
                      </div>

                      <br />
                      <div class="input-group">
                        <span class="input-group-addon"></span>
                        <input type="text" class="form-control" placeholder="Interior Color" name="interior_color" >
                      </div>

                      <br />
                      <div class="input-group">
                        <span class="input-group-addon"></span>
                        <input type="text" class="form-control" placeholder="Mileage" name="mileage" required>
                      </div>

                      <br />
                      <div class="input-group">
                        <span class="input-group-addon"></span>
                        <input type="text" class="form-control" placeholder="Transmission" name="transmission" required>
                      </div>

                      <br />
                      <div class="input-group">
                        <span class="input-group-addon"></span>
                        <input type="text" class="form-control" placeholder="Wheelbase" name="wheelbase">
                      </div>

                      <br />
                      <div class="input-group" style="width:100%;" >
                        <!-- <span class="input-group-addon"></span>
                        <input type="text" class="form-control" placeholder="Featured" name="featured" > -->
                        <select class="form-control" style="width:100%;" name="featured">
                          <option value="-1">Select if Car will be featured</option>
                          <option value="1">Car Featured</option>
                          <option value="0">Car Not Featured</option>
                        </select>
                      </div>

                      <br />
                      <div class="input-group" style="width:100%;">
                        <!-- <span class="input-group-addon"></span>
                        <input type="text" class="form-control" placeholder="Brand New" name="brand_new" required> -->
                        <select class="form-control" style="width:100%;" name="car_type">
                          <option value="Not Defined">Select Car Type</option>
                          <option value="1">Brand New</option>
                          <option value="0">Used</option>
                        </select>
                      </div>

                      <br />
                      <div class="input-group" style="width:100%;">
                        <!-- <span class="input-group-addon"></span>
                        <input type="text" class="form-control" placeholder="Make" name="make" > -->
                        <select class="form-control" style="width:100%;" name="make">
                          <option value="None">Select Make</option>
                          <?php
                              if($sellers != null) {
                                  foreach ($make as $mk)  {
                                        echo "<option value='$mk->make_name'>$mk->make_name</option>";
                                  }
                              }
                          ?>
                          
                        </select>
                      </div>

                      <br />
                      <div class="input-group" style="width:100%;">
                        <!-- <span class="input-group-addon"></span>
                        <input type="text" class="form-control" placeholder="seller_id" name="seller_id" required> -->
                        <select class="form-control" style="width:100%;" name="seller_id">
                          <option value="-1">Select a Seller</option>
                          
                          <?php
                              if($sellers != null) {
                                  foreach ($sellers as $seller)  {
                                        echo "<option value='$seller->seller_id'>$seller->seller_name</option>";
                                  }
                              }
                          ?>
                          
                        </select>
                      </div>

                      <br />
                      <div class="input-group" style="width:100%;">
                        <!-- <span class="input-group-addon"></span> -->
                        <select class="form-control" style="width:100%;" name="dealer_id">
                          <option value="-1">Select a Dealer</option>
                          
                          <?php
                              if($dealers != null) {
                                  foreach ($dealers as $dealer)  {
                                        echo "<option value='$dealer->dealer_id'>$dealer->dealer_name</option>";
                                  }
                              }
                          ?>
                          
                        </select>
                      </div>

                      <br />
                      <div class="input-group">
                        <span class="input-group-addon"></span>
                        <textarea type="text" class="form-control" placeholder="Notes" rows="10" name="notes" ></textarea>
                      </div>

                      <br /> 
                      <p>
                          <button type="submit" name="submit" class="btn btn-info"  role="button">Save</button> 
                          <a class="btn btn-info" href="cars.php" role="button">Cancel</a>
                      </p>
                  </form> 
                  


                </div>
                <div class="col-md-5">
                  <h2>Adding a Car</h2>
                  <p>Fill up everything some fields are required and it is important.</p>
               </div>
        </div>
      </div>


    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="bootstrap/js/jquery.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
    
  

</body></html>