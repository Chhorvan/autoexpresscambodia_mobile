<div id="footer">
	<div class="container footer_container">
		<div class="link_top">
			<a href="#top">
				<img src="<?php echo BASE_RELATIVE.'asset/img/totop.png'; ?>" alt="Go to top">
			</a>
		</div>
		<div class="row footerContainer">
		
			<div class="company_profile">
				<article>
					<div  class="header common_blue">
						<h3>Site Map</h3>
					</div>
					<div class="footer_list">
						<ul>

							<li><a href="http://demo.angkorauto.com/">Home</a></li>
							<li><a href="http://demo.angkorauto.com/vehicle">Auto Search</a></li>
							<li><a href="http://demo.angkorauto.com/vehicle?result=1&condition=used">Auto Spare Part</a></li>
							<li><a href="<?php echo BASE_RELATIVE.'aboutus'?>">About Us</a></li>

							
						</ul>
					</div>
				</article>
			</div>
			
			<div class="customer-care clearfix">
				<article>
					<div class="header common_blue">
						<h3>Customer Care</h3>
					</div>
					<div class="footer_list">
						<ul>
							<li><a href="<?php echo BASE_RELATIVE; ?>contact">Contact Us</a></li>
							<li><a href="">Help and Support</a></li>
							<li><a href="">Become Our Seller</a></li>
                            <li><a href="<?php echo BASE_RELATIVE; ?>feedback">Feedback</a></li>
						</ul>
					</div>
				</article>
			</div>
			
			<div class="our-privacy">
				<article>
					<div class="header common_blue">
						<h3>Privacy Statement</h3>
					</div>
					<div class="footer_list">
						<ul>
							<li><a href="<?php echo BASE_RELATIVE.'privacy-policy'?>">Privacy Policy</a></li>
							<li><a href="<?php echo BASE_RELATIVE.'termcondition'?>">Term & Conditions</a></li>
							<li><a href="">Disclaimer</a></li>
						</ul>
					</div>
				</article>
			</div>
			
			<div class="stay-connected">
				<article>
					<div class="header common_blue">
						<h3>Stay Connected</h3>
					</div>
					<div class="footer_list">
					</div>
					<div class="footer_social">
						<ul>
							<li class="facebook"><a href="https://www.facebook.com/angkorauto"><i class="fa fa-facebook"></i></a></li>
							<li class="twitter"><a href="https://twitter.com/angkorauto"><i class="fa fa-twitter"></i></a></li>
							<li class="google-plus"><a href="https://plus.google.com/b/101792630384270764930"><i class="fa fa-google-plus"></i></a></li>
						</ul>
					</div>
				</article>
			</div>
			
		
			
		</div>
		
	</div>
	
	<div class="copyright">
		<div class="container">
           
			<div class="row">
            	<p style="padding-top:4px;">&copy; Copyright 2014 Angkorauto. All Rights Reserved by Softbloom co., ltd</p>			
			</div>

		</div>
	</div>
</div>