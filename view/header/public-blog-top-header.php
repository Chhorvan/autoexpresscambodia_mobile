<?php
ini_set ("display_errors", "0");
mb_internal_encoding( 'utf-8' );
header("Content-Type:text/html; charset=utf-8");

    /*
     * Public top header
     */
?>
<link href="<?php echo BASE_RELATIVE;?>css/common/blog/styles.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo BASE_RELATIVE;?>js/blog/common.js"></script>

</head>
	<body>
    	<div id="sectionWrapper">
        	<div id="sectionHeader">
                <div id="sectionBanner">
                    <img src="<?php echo BASE_RELATIVE;?>/images/banner.jpg" />
                </div>
                <div id="sectionNav">
                	<ul>
                    	<li><a href="<?php echo BASE_RELATIVE;?>blog-main?blog">Home</a></li>
                        <li><a href="<?php echo BASE_RELATIVE;?>blog-company-profile?blog">Company Profile</a></li>
                        <li><a href="<?php echo BASE_RELATIVE;?>blog-product?blog">Products</a></li>
                        <li><a href="<?php echo BASE_RELATIVE;?>blog-photo?blog">Photos</a></li>
                        <li><a href="">Free Board</a></li>
                        <li><a href="<?php echo BASE_RELATIVE;?>blog-contact?blog">Contact Us</a></li>
                        <li><a href="">Guest Book</a></li>                        
                    </ul>
                    <div id="lang"><p>Langauge:</p><select><option>English</option></select></div>
                </div>
            </div>