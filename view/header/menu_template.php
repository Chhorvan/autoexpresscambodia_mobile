<?php
	require_once BASE_CLASS . 'php_cofig.php';
	/* require_once BASE_CLASS . 'class-global.php'; 
	$global = new classglobal(); */
	
	// set session time out for user		
	if(isset($_SESSION['log_email'])){			 
		// set timeout period in seconds
		$session_timeout = 3600; // time out 1h
		
		if (!isset($_SESSION['last_visit'])) { 
			$_SESSION['last_visit'] = time(); 
		} 			
	
		if((time() - $_SESSION['last_visit']) > $session_timeout) { 
		  session_destroy(); 				  		  
		  echo '<script type="text/javascript">window.location="'.BASE_RELATIVE.'login"</script>';
		}
		$_SESSION['last_visit'] = time();
	}	
	

?>



<!-- GoogleAnalytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-58272743-1', 'auto');
  ga('send', 'pageview');

</script>

<div id="header">	
    <div class="container menuHeader">	
        <div id="contaninerHead" class="navbar">
            <div class="navbar-brand logo">
                <a href="<?php echo BASE_RELATIVE; ?>" id="logo">
                    <img src="<?php echo BASE_RELATIVE.'asset/img/softbloom.png'; ?>" alt="Logo">
                </a>
            </div>                       
        
			<!-- Menu Nav -->
			<div id="menuRes">
				<img src="<?php echo BASE_RELATIVE; ?>asset/img/menu_res.png" />
			</div><!-- menuRes -->
		</div><!-- .navbar -->	
		

		<div id="menuNav">
			<div class="collapse navbar-collapse navHeaderCollapse">
				<ul id="dropMenu" class="nav navbar-nav mainmenu">
					<li><a  href="<?php echo BASE_RELATIVE; ?>" title="Research all new, Used cars in Cambodia, Cars prices, Cars reviews and comparison" ><?php echo $_GLANG['PUBLIC_TOPMENU_ALL_VEHICLE'];?></a></li>
					<!--<li class="dropdown">
						<a href="" data-toggle="dropdown" class="dropdown-toggle current-link" title="Quick search new and used cars according to body types"><?php echo $_GLANG['PUBLIC_TOPMENU_VEHICLE_TYPE'];?></a>

						<ul class="dropdown-menu submenu">
						  <?php
							 include('ajax/load_ve_type.php');
							 $recieve_vehicle_type=extract_vehicle_type();//recieve_vehicle_type
							 $num=get_number_of_record();//recieve_number_of_record
							 
							 
							 $i=0;
							 foreach($recieve_vehicle_type as $type){
								$i++;
								$vehicle_type = $type['body_style'];								
								$vehicle_type = str_replace(' ', '+', $vehicle_type);
								if($i<$num){
									echo BASE_RELATIVE. $vehicle_type;
									echo'<li><a href="'.BASE_RELATIVE. $vehicle_type.'">'.$type['body_style'].'</a></li>';
								}
								else{
									//echo"<li style='border-bottom:none;'><a href='#'>$type[vehicle_type]</a></li>";
									echo'<li style="border-bottom:none;"><a href="'.BASE_RELATIVE. $vehicle_type.'">'.$type['body_style'].'</a></li>';
								}
							 }
							?>

						</ul>
					</li>-->
 

					<li><a href="<?php echo BASE_RELATIVE.'0'?>" title="Used cars for sale in Cambodia"><?php echo $_GLANG['PUBLIC_TOPMENU_VEHICLE_USED'];?></a></li>
					<li><a href="<?php echo BASE_RELATIVE.'1'?>" title="Search new cars in quick click"><?php echo $_GLANG['PUBLIC_TOPMENU_VEHICLE_NEW'];?></a></li>
					<li><a href="<?php echo BASE_RELATIVE.'dealer'?>" title="Search new cars in quick click"><?php echo $_GLANG['PUBLIC_TOPMENU_DEALER'];?></a></li>
					<li class="lastMenu"><a href="<?php //echo BASE_RELATIVE.'buy-car'?>" >Buy Car</a></li>

				</ul> 										
			</div>
		</div><!-- #menuNav -->
		
    </div><!-- .menuHeader -->
</div><!-- #header -->