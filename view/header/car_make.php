<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
$type = trim($_GET['product_type']);

if( !isset($type) ){
    exit;
}
require_once dirname(dirname(__FILE__)) . '/config.php';
require_once BASE_CLASS . 'class-connect.php';

$cnx = new Connect();
$cnx->open();

if ($type=="all"){
	if( !$sql = mysql_query("SELECT `id`,`maker` FROM `product_maker`") ){
		require_once BASE_CLASS . 'class-log.php';
		LogReport::write('Unable to load car model list due a query error at ' . __FILE__ . ':' . __LINE__);
		
		$cnx->close();
		echo '';
		exit;
	}

}
else{
	if( !$sql = mysql_query("SELECT `id`,`maker` FROM `product_maker` WHERE `product_type`='$type' ") ){
		require_once BASE_CLASS . 'class-log.php';
		LogReport::write('Unable to load car model list due a query error at ' . __FILE__ . ':' . __LINE__);
		
		$cnx->close();
		echo '';
		exit;
	}

	}

if( @mysql_num_rows($sql) < 1 ){
    $cnx->close();
    echo '';
    exit;
}

$make_arr = array();

while( $r = @mysql_fetch_assoc($sql) )
{
     $make_arr[]=array('id'=>$r['id'],
	 'carmake'=>$r['maker']);
	 
}
 
//sort($model_arr);
echo json_encode($make_arr);
$cnx->close(); 


 