<?php 
/* GET compose message result after user clicked send */
if(isset($_GET['result'])) $result=htmlspecialchars($_GET['result']); else $result='';
if(isset($_GET['id'])) $id=htmlspecialchars($_GET['id']); else $id='';
$sendTo='';
$defaultSubject='';
if(!empty($id)){
    $message_detail=$_CLASS->getMessage();
    $messageAttachments=$_CLASS->getMessageAttachments();
    /* GET the product detail that attach with message */
    $product_detail=$_CLASS->getProductDetail($message_detail['product_id']);
    /* check if logged in user is receiver or sender */
    if($message_detail['sender']!=$_SESSION['log_id']){
        /* If logged in user is receiver then email will be send to sender */
        $defaultSubject="Reply to {$message_detail['sender_company']}";
        if(!empty($message_detail['product_id']))
            $defaultSubject.="'s inquiry for product id: ".$message_detail['product_id'];
        $sendTo=$message_detail['sender'];
    }
    else {
        /* If logged in user is sender then email will be send to receiver */
        $sendTo=$message_detail['receiver'];
    }
}

//Download files

$messageTemplates=$_CLASS->getMessageTemplates();
$productImages=$_CLASS->getProductImages();

// get form feed.
$fstatus = $_CLASS->getFormStatus();
$fmessage = $_CLASS->getFormMessage();
$fstyle = $_CLASS->getFormStyle();
$errorMessages=$_CLASS->getErrorMessages();

$attach_icon['pic_icon']=array('jpg', 'gif', 'bmp', 'png');
$attach_icon['excel_icon']=array('xls');
$attach_icon['word_icon']=array('doc', 'docx');
$attach_icon['ppt_icon']=array('ppt', 'pptx');
$attach_icon['zip_icon']=array('zip');
$attach_icon['pdf_icon']=array('pdf');
$attach_icon['hwp_icon']=array('hwp');
$attach_icon['txt_icon']=array('txt');

?>	


<?php if($result=='success'){?>
    <div style="padding: 50px 0;text-align: center; color:black;font-size:16px;">
        <img src="images/tick.png" style="padding-right:10px;"><font color='green'>Your message has been sent successfully to: <b><?php if(isset($message_detail['sender_company'])) echo $message_detail['sender_company'];?></b>.</font><br/><br/>
        <a href="<?php echo BASE_RELATIVE;?>messages">Click here</a> to go to your Inbox.
    </div>
<?php }elseif($result=='fail'){?>
    <div style="padding: 50px 0;text-align: center; color:black;font-size:16px;">
        <font color='red'>We are sorry. The inquiry was not sent! <br/><br/>There is no item found with this ID at the moment.</font><br/><br/>
        <br/><a href="<?php echo BASE_RELATIVE;?>">Click here</a> to go to homepage.
    </div>
<?php }else{?>
<div id="main-content">
        <?php
            if(count($errorMessages)){
        ?>
        <div class="alert <?php echo $fstyle;?>">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <?php 
                foreach($errorMessages as $message){
                    echo $message."<br/>";
                }
            ?>
        </div>
        <?php
            }
        ?>
    <div class="message_wrapper">     
        <div class="left">
            <div class="page-title">
            <table><tr><td><img class="brick" src="<?php echo BASE_RELATIVE;?>images/common/register_item/title.png" /></td>
            <td>Message<span style="margin-left:255px; font-size:12px; color:#333; font-weight:600;"><?php if(isset($message_detail['created_date'])) echo htmlspecialchars($message_detail['created_date']);?></span></td>
            </tr></table>
            </div>
            
            <div id="detail">
                <table>
                    <tr>
                   
                        <th>Form :</th>
                        
                        <td class="msg_detail">
                            <?php if(isset($message_detail['sender_company'])) echo htmlspecialchars($message_detail['sender_company']);?>
                        </td>
                        
                    </tr><tr>
                        <th>To :</th>
                        <td class="msg_detail">
                            <?php if(isset($message_detail['receiver_company'])) echo htmlspecialchars($message_detail['receiver_company']);?>
                        </td>
                    </tr><tr>
                        <th>Subject :</th>
                        <td class="msg_detail">
                            <?php if(!empty($message_detail['subject'])) echo htmlspecialchars($message_detail['subject']); else echo "<label class='empty_text'>(Empty)</label>";?>
                        </td>
                    </tr>
                    <?php if(!empty($message_detail['product_id'])){?>
                    <tr>
                        <th></th>
                        <td class="msg_detail">
                            <div class="detail">
                                
                                <div id="product_main_image">
                                    <a target="_blank" href="vehicle-detail?cid="><img class="image" src="<?php if(isset($productImages['0']['thumb'])) echo $productImages['0']['thumb'];?>" width="140"></a>
                                </div>
                                <div id="content-text">
                                    <table>
                                        <?php if(!empty($message_detail['product_id'])){ ?>
                                            <tr>
                                                <td>Stock Id: <?php echo $message_detail['product_id']; ?></td>
                                            </tr>
                                        <?php } ?>
                                        <?php if(!empty($product_detail['product_id'])){ ?>
                                            <tr>
                                                <td class="text-detail">Steering: <?php echo $product_detail['steering']; ?></td>
                                            </tr>
                                        <?php } ?>
                                        <?php if(!empty($product_detail['location'])){ ?>
                                            <tr>
                                                <td class="text-detail">
                                                <img width="15" height="12" src="<?php echo BASE_RELATIVE;?>images/flag/<?php echo $product_detail['location']; ?>.png"/>
                                                <span><?php echo $product_detail['country_name']; ?></span>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        <?php if(!empty($product_detail['city'])){ ?>
                                            <tr>
                                                <td class="text-detail"><?php echo $product_detail['city']; ?></td>
                                            </tr>
                                        <?php } ?>
                                        <?php if(!empty($product_detail['price'])){ ?>
                                            <tr>
                                                <td class="text-detail"><font color="#FF0000">FOB: USD <?php echo $product_detail['price']; ?></font></td>
                                            </tr>
                                        <?php } ?>
                                        <?php if(!empty($product_detail['owner'])){ ?>
                                            <tr>
                                                <td class="text-detail">Seller: <?php echo $product_detail['company_name']; ?></td>
                                            </tr>
                                        <?php } ?>
                                        <tr>
                                            <td style="color:red;" class="text-detail"> 
                                            </td>
                                        </tr>
                                        <tr>


                                            <td style="color:blue;" class="text-detail" id="click"><a target="_blank" href="vehicle-detail?cid=<?php echo $message_detail['product_id'];?>"><font color="blue">More Detail</font></a></td>


                                        </tr>
                                    </table>
                                </div>
                               
                           </div>
                        </td>
                    </tr>
                    <?php } ?>
                    <tr>
                   
                        <th>Message :</th>
                        

                        <td class="msg_detail">
                            <div class='message_body'><?php if(!empty($message_detail['message'])) echo htmlspecialchars($message_detail['message']); else echo "<label class='empty_text'>(Empty)</label>"; ?></div>

                        </td>
                        
                    </tr>
                    <?php 
                        //if there are attachment files
                        if(count($messageAttachments)>0){ 

                    ?>
                    <tr>
                   
                        <th>Attachment Files : </th>
                        

                        <td class="msg_detail">
                            <table class="table_attachment">
                            <?php
                                foreach($messageAttachments as $attachment){
                                    /* explode to array temp to get extension */
                                    $temp = explode(".", $attachment['file_name']);

                                    /* get extension eg. jpg or gif*/
                                    $extension = strtolower(end($temp));
                                    
                                    $src='unknown_icon.png';
                                    foreach($attach_icon as $key=>$icon){
                                        if(in_array($extension, $icon)){
                                            $src=$key.".png";
                                        }
                                    }
                                    echo "<tr>
                                            <td><a target='_blank' href='download-attachment?download={$attachment['encode_name']}'><img src='images/$src'/></a></td>
                                            <td><a target='_blank' href='download-attachment?download={$attachment['encode_name']}'>{$attachment['file_title']}</a></td>
                                            <td><a target='_blank' href='download-attachment?download={$attachment['encode_name']}'><img title='download' alt='download' src='images/download_icon.png'/></a></td>
                                        </tr>
                                    ";
                                }
                            ?>
                            </table>
                        </td>
                        
                    </tr>
                    <?php } ?>
                </table>        
            </div>
           
        </div>
        
        <div class="right">
            <div class="page-title">
            <table><tr><td><img class="brick" src="<?php echo BASE_RELATIVE;?>images/common/register_item/title.png" /></td><td>Reply</td></tr></table>
            </div>
            
            <div id="detail-right">

                <form method="post" enctype="multipart/form-data" class="form-horizontal" id="message_form">
                    
                    <input type="hidden" name="sendToInput" value="<?php echo $sendTo;?>"/>
                    <table>
                        <tr>
                            <th>
                                <div style="float:left;width:140px;padding-top:10px">Message Template : </div>
                                <div style="overflow:hidden"> 
                                    <select class="select" id="templateSelect">
                                        <option value="">- Select Template-</option>
                                        <?php 
                                            foreach($messageTemplates as $template){
                                                echo "<option value=\"".htmlspecialchars($template['text'])."\">{$template['title']}</option>";
                                            }
                                        ?>
                                    </select>   
                                </div>
                            </th>
                        </tr><tr>
                            <td colspan="2">
                            
                                <div> <input style="width:100%;" type="text" placeholder="Subject" name="subjectInput" required value="<?php echo $defaultSubject;?>"/></div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                            <textarea name="messageInput" placeholder="Message" required class="textarea0" id="messageInput"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div class="form-group">
                                    
                                    <div class="col-lg-5">
                                        Attach Files: <input type="file" id="upload_attachment" class="file_input form-control" name="file[]" multiple/>

                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                            <span><font color="#FF0000">*</font></span><span class="note">Available File Formats : XLS, DOC, DOCX, PPT, PPS, JPG, GIF, BMP, ZIP, PDF, <span style="padding-left:5px;">PNG, HWP, TXT</span></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            <span><font color="#FF0000">*</font></span><span class="note">Max file upload size : 10MB</span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                            <input type="submit" class="btn-send" name="sendButton" value="SEND" />
                             </td>
                        </tr>
                    </table>
                    
                </form>


               

            </div>
            
        </div>
        <p class="clear"></p>
        
        </div>
    </div>
    <?php }?>
