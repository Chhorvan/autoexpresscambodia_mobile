<?php
	if( !isset($_SESSION['log_group']) ){
		header('Location: '.BASE_RELATIVE.'login/');
	}

/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = true;
//$_CKEDITOR_ID = 'sellerCommentText';
/*
 * VALIDATE ADMINISTRATOR GROUP ------------------------------------------------
 */

    // get form feed.
    $fstatus = $_CLASS->getFormStatus();
    $fmessage = $_CLASS->getFormMessage();
    $fstyle = $_CLASS->getFormStyle();
    $invalidProduct = false;
    // get lists.
    $maker_unique_list = $_CLASS->getMakerList();
    $model_unique_list = $_CLASS->getModelList();
    $body_type_list    = $_CLASS->getBodyList();
	$drive_type_list   = $_CLASS->getDriveList();

	$product_option_list = $_CLASS->getProductOptionList();
    $country_list = $_CLASS->getCountryList();
	$vehicle_type_list = $_CLASS->getVehicleTypeList();
	$car_maker_list = $_CLASS->getCarMakerList();
	$model_list=$_CLASS->getModelList();
	$category_list = $_CLASS->getCategoryList();
	$fuel_type_list = $_CLASS->getFuelTypeList();
	$transmission_list = $_CLASS->getTransmissionList();

    // set input size for maker.
    ( count($maker_unique_list) > 0 ? $imaker = 'style="width:249px;margin-right:-1px;"' : $imaker = '' );
    ( count($model_unique_list) > 0 ? $imodel = 'style="width:249px;margin-right:-1px;"' : $imodel = '' );
    ( count($body_type_list) > 0 ? $ibody = 'style="width:249px;margin-right:-1px;"' : $ibody = '' );
    if( CURRENCY_CODE == 'USD' ){
        $decimal = '.';
        $thousand= ',';
    }
    else {
        $decimal = ',';
        $thousand= '.';
    }
    if(!isset($_GET['cid'])) $getId='';
	else $getId=$_GET['cid'];
	$product=array();

    // get generated car id.
	if(empty($getId)){
    	$carid = $_CLASS->getCarId();
	}else{
		$carid = $getId;
		$product = $_CLASS->getAllProducts($carid);
        $invalidProduct=$_CLASS->invalidProduct;

	}
	(isset($product['payment_terms']) ? $payment_terms = explode('^',$product['payment_terms']) : $payment_terms=array());
	(isset($product['other_options']) ? $other_options = explode('^',$product['other_options']) : $other_options=array());
	(isset($product['safety_device_options']) ? $safety_device_options = explode('^',$product['safety_device_options']) : $safety_device_options=array());
	(isset($product['exterior_options']) ? $exterior_options = explode('^',$product['exterior_options']) : $exterior_options=array());
	(isset($product['interior_options']) ? $interior_options = explode('^',$product['interior_options']) : $interior_options=array());
	(isset($product['fitting_make']) ? $fitting_make_row = explode(';',$product['fitting_make']) : $fitting_make_row=array());
	$fitting_make=array();
	$fitting_make_tmp=array();
	foreach($fitting_make_row as $fr){
		$fitting_make_tmp=explode('^',$fr);
		$tmp_maker=$fitting_make_tmp[0];
		unset($fitting_make_tmp[0]);
		$fitting_make[$tmp_maker]=$fitting_make_tmp;
	}


?>
<script>
var condition="<?php if(isset($product['condition'])) echo $product['condition']; ?>";
var vessel_type="<?php if(isset($product['vessel_type'])) echo $product['vessel_type']; ?>";
var make="<?php if(isset($product['make'])) echo $product['make']; ?>";
var model="<?php if(isset($product['model'])) echo $product['model']; ?>";
var year="<?php if(isset($product['model_year'])) echo $product['model_year']; ?>";
var built_year="<?php if(isset($product['built_year'])) echo $product['built_year']; ?>";
var made_in="<?php if(isset($product['made_in'])) echo $product['made_in']; ?>";
var currency="<?php if(isset($product['currency'])) echo $product['currency']; ?>";
var country="<?php if(isset($product['location'])) echo $product['location']; ?>";
var city="<?php if(isset($product['city'])) echo $product['city']; ?>";
var fuel_type="<?php if(isset($product['fuel_type'])) echo $product['fuel_type']; ?>";
var make="<?php if(isset($product['make'])) echo $product['make']; ?>";
var model="<?php if(isset($product['model'])) echo $product['model']; ?>";
var flight_rule="<?php if(isset($product['flight_rule'])) echo $product['flight_rule']; ?>";
var measureType="<?php if(isset($product['mileage_type'])) echo $product['mileage_type']; ?>";
function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
      }
</script>
        <div id="content-wrapper">


            <div id="my_item">

                <div class="titleTop">
                        <img class="img-responsive" src="<?php echo BASE_RELATIVE; ?>img/user_add_new/img_01.png" />
                        <span>Add new </span>
                    </div>

                 <div id="menuTab" class="clearfix">
                        <ul>
                            <li><a href="<?php echo BASE_RELATIVE; ?>edit-car" ><p>Car </p></a></li>
                            <li><a href="<?php echo BASE_RELATIVE; ?>edit-bus"><p> Bus </p></a></li>
                            <li><a href="#"><p>Track</p></a></li>
                            <li><a href="<?php echo BASE_RELATIVE; ?>edit-motorbike"><p>Motorbike</p></a></li>

                        </ul>
                    </div><!-- #menuTab -->


                </div>


            <!--   <div id="topMenu">
            	<p><a href="#" class="linkfade">Register Item</a> > Ship</p>
            </div>
            <?php include("php/sidebar/my-wini.php");?>

                <div id="inner-content">
                <!-- START CONTENT -->
                <!-- <div class="page-title">
                	<?php
					if(empty($getId)){
						echo "<p>".$_LANG['PRODUCT_HEADER']."</p>";
					}else{
						echo "<p>".$_LANG['PRODUCT_EDIT_HEADER']."</p>";
					}
					?>
                </div> -->

                <?php
                    if( $fstatus ){
                ?>
                <div class="alert <?php echo $fstyle;?>">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <?php echo $fmessage;?>
                </div>
                <?php
                    }
                ?>



                <form id="form-wrap" method="post">
                    <!----------------- FORM CONTAINER -------------------->
                    <?php if(!$invalidProduct){?>
                    <input type="hidden" name="caridInput" id="hiddendusereditproductid" value="<?php echo $carid;?>" />

                    <div id="content1">
                      <table class="edit_product_table">
                            <tr>
                                <td class="edit_haft_row">
			       <div class="add-on title-label"><?php echo $_LANG['PRODUCT_CONDITION_LABEL'];?> <font color="#C1272D">*</font></div>
                               <label><input type="radio" name="conditionInput" value="Used"/><?php echo $_LANG['PRODUCT_CONDITION_USED'];?></label>
                               <label><input type="radio" checked id="conditionInput" name="conditionInput" value="New"/><?php echo $_LANG['PRODUCT_CONDITION_NEW'];?></label>

                        </td>
                        <td class="edit_haft_row">

                            <div class="add-on title-label">frame number</div>
                             <input type="text" value="<?php if(isset($product['chassis_no'])) echo $product['chassis_no'];?>" maxlength="50" name="chassisNoInput" class="content-label" placeholder="<?php echo $_LANG['PRODUCT_CHASSIS_NO_PLACEHOLDER'];?>" />


                        </td>
                        </tr>
                        <tr>
                         <td class="edit_haft_row">

                            <div class="add-on title-label"><?php echo $_LANG['PRODUCT_MAKER_LABEL'];?> <font color="#C1272D">*</font></div>
                            <select required name="makerInputSelect" id="makerInputSelect" class="content-label" >
                                <?php
                                    $_GET['product_type']="Motorbike";
                                    include('ajax/get_product_make_options.php');
                                ?>
                            </select>
                            <input type="text" value="" maxlength="50" name="makerInput" id="makerInput" class="content-label other_input" placeholder="<?php echo $_LANG['PRODUCT_MAKER_PLACEHOLDER'];?>" />



                         </td>
                         <td class="edit_haft_row">

                            <div class="add-on title-label"><?php echo $_LANG['PRODUCT_MANUFACTURER_LABEL'];?></div>
                            <input type="text" value="<?php if(isset($product['manufacturer'])) echo $product['manufacturer'];?>" maxlength="50" name="manufacturerInput" class="content-label" placeholder="<?php echo $_LANG['PRODUCT_MANUFACTURER_PLACEHOLDER'];?>" />


                         </td>
                        </tr>
                        <tr>
                        <td class="edit_haft_row">

                            <div class="add-on title-label"><?php echo $_LANG['PRODUCT_MODEL_LABEL'];?> <font color="#C1272D">*</font></div>
                            <select name="modelInputSelect" id="modelInputSelect" class="content-label" >
                                <option value="">- Model -</option>
                            </select>
                            <input type="text" value="" maxlength="50" name="modelInput" id="modelInput" class="content-label other_input" placeholder="<?php echo $_LANG['PRODUCT_MODEL_PLACEHOLDER'];?>" />


                        </td>
                         <td class="edit_haft_row">

                            <div class="add-on title-label"><?php echo $_LANG['PRODUCT_YEAR_LABEL'];?> <font color="#C1272D">*</font></div>
                            <select id="yearInput" required name="yearInput">
                                <option value="">- Select -</option>
                                <?php
                                for( $i=date('Y'); $i > 1900; $i-- ){
                                ?>
                                    <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                <?php
                                    }
                                ?>
                            </select>

                         </td>
                        </tr>
                        <tr>
                        <td class="edit_haft_row">

                            <div class="add-on title-label"><?php echo $_LANG['PRODUCT_MILES_LABEL'];?> <font color="#C1272D">*</font></div>
                            <input type="text" required value="<?php if(isset($product['mileage'])) echo $product['mileage'];?>" placeholder="<?php echo $_LANG['PRODUCT_MILES_PLACEHOLDER'];?>" name="milesInput" style="width:145px;margin-right:-1px;" class="content-label" onkeypress="return isNumberKey(event)"/>
                            <label><input type="radio" checked value="Km" name="measureTypeInput">Km</label>
                            <label><input type="radio" value="Mile" name="measureTypeInput">Mile</label>


                        </td>
                        <td class="edit_haft_row">

                            <div class="add-on title-label"><?php echo $_LANG['PRODUCT_MADE_IN_LABEL'];?> <font color="#C1272D">*</font></div>

                            <select required name="madeInInputSelect" id="madeInInputSelect">
                                <option value="">- Select -</option>
                                <?php
                                    if( count($country_list['cc']) > 0 ){

                                        for( $i=0; $i < count($country_list['cc']); $i++ ){
                                            if( !empty($country_list['country_name'][$i]) ){
                                    ?>
                                    <option value="<?php echo $country_list['cc'][$i];?>"><?php echo $country_list['country_name'][$i];?></option>
                                <?php
                                        }
                                    }
                                }
                                else {
                            ?>
                                <option value="0"><?php echo $_LANG['PRODUCT_NOCOUNTRY_LABEL'];?></option>
                            <?php
                                }
                            ?>
                            </select>



                        </td>
                        </tr>
                        <tr>
                        <td class="edit_haft_row">

                            <div class="add-on title-label">cylender capacity<font color="#C1272D">*</font></div>
                            <input type="text" required value="<?php if(isset($product['engine_volume'])) echo $product['engine_volume'];?>" name="engineVolumeInput" class="content-label" placeholder="<?php echo $_LANG['PRODUCT_ENGINE_VOLUME_PLACEHOLDER'];?>" onkeypress="return isNumberKey(event)" style="width:145px;"/>(CC)
                         </td>

                        <td class="edit_haft_row">

                            <div class="add-on title-label"><?php echo $_LANG['PRODUCT_FUEL_LABEL'];?> <font color="#C1272D">*</font></div>
                            <select id="carFuelInputSelect" required name="carFuelInputSelect" >
                                <option value="">- Select -</option>
                                <?php
                                    if( count($fuel_type_list['fuel_name']) > 0 ){

                                        for( $i=0; $i < count($fuel_type_list['fuel_name']); $i++ ){
                                            if( !empty($fuel_type_list['fuel_name'][$i]) ){
                                    ?>
                                    <option value="<?php echo $fuel_type_list['fuel_name'][$i];?>"><?php echo $fuel_type_list['fuel_name'][$i];?></option>
                                <?php
                                            }
                                        }
                                    }
                                    else {
                                ?>
                                    <option value="0"></option>
                                <?php
                                    }
                                ?>
                            </select>

                        </td>
                        </tr>
                        <tr>
                        <td class="edit_haft_row">

                            <div class="add-on title-label"><?php echo $_LANG['PRODUCT_CITY_LABEL'];?></div>
                            <select id="citySelect" name="citySelect">
                                <?php
                                    $_GET['country']=$_CLASS->getUserCountry();
                                    include('ajax/load_saved_city.php');
                                ?>
                            </select>
                            <input type="text" maxlength="50" id="cityInput" name="cityInput" class="content-label other_input" placeholder="" />

                        </td>
                         <td class="edit_haft_row">

                            <div class="add-on title-label"><?php echo $_LANG['PRODUCT_PRICE_LABEL'];?></div>

                            <input type="text" value="<?php if(isset($product['price'])) echo $product['price'];?>" name="priceInput" class="content-label" placeholder="<?php echo $_LANG['PRODUCT_PRICE_PLACEHOLDER'];?>" onkeypress="return isNumberKey(event)" style="width:139px;"/>
                            <select name="currencySelect" id="currencySelect">
                                <?php include("ajax/load_currency_list.php");?>
                            </select>

                         </td>
                        </tr>
                        <tr>
                                <td style="border-right:none;padding-left:10px" colspan="2" class="edit_haft_row">
                                    <div class="add-on block-label"><b>Description</b></div>
                                    <textarea id="sellerCommentText" name="sellerCommentText" class="tinymce content-label"><?php if(isset($product['seller_comment'])) echo $product['seller_comment'];?></textarea>
                                </td>
                            </tr>
                        <tr>
                            <td colspan="2" >
                         <div id="video-wrap" style="display:none;" class="input-prepend input-append inputap">
                            <div class="add-on title-label"><?php echo $_LANG['PRODUCT_YOUTUBE_URL_LABEL'];?></div>
                            <input id="videoUrlInput" type="text" class="content-label" style="width:435px;" placeholder="<?php echo $_LANG['PRODUCT_YOUTUBE_URL_PLACEHOLDER'];?>" />
                            <input id="vidBtn" onclick="saveVideoHandler('<?php echo $carid;?>');return false;" type="submit" class="btn btn-info" style="width:100px;" value="<?php echo $_LANG['PRODUCT_SAVE_BUTTON'];?>" />
                        </div>
                       <div id="upload-wrap" style="display:none;float:left;width:100%;height:75px" >
                            <h4  style="padding:10px;"><b><?php echo $_LANG['PRODUCT_UPLOAD_LABEL'];?></b></h4>
                            <iframe id="iupload" width="90%" style="border:0;float:left"></iframe>


                            <input style="float:left;" id="submitbtn" type="submit" name="savebtn" class="btn btn-small btn-info" value="<?php echo $_LANG['PRODUCT_SAVE_BUTTON'];?>" />
                    </div>
                                 </td>
                            </tr>
                        </table>

                        <!-------------- UPLOAD MEDIA BLOCK ---------->


                    <br>
                    <div id="outputwrap"></div>
                    <div id="outoutimg" class="clearfix"></div>
                    <div class="clearfix"></div>
                    <br>
                    <a id="prevbtn" href="#" onclick="return previousFormHandler()" class="btn btn-small" style="display:none;margin-right:10px;">
                        <i class="icon-circle-arrow-left" style="margin-right:10px;"></i><?php echo $_LANG['PRODUCT_PREVIOUS_BUTTON_LABEL'];?>
                    </a>
                    <a id="nextbtn" href="#" onclick="return nextFormHandler()" class="btn btn-small" style="display:none;margin-right:10px;">
                        <?php echo $_LANG['PRODUCT_NEXT_BUTTON_LABEL'];?><i class="icon-circle-arrow-right" style="margin-left:10px;"></i>
                    </a>



                      <!--End Auto Save Disable And Save Button-->


                    <!-------------- END UPLOAD MEDIA BLOCK ---------->
                <!----------------- END FORM CONTAINER -------------------->
                <?php
                }
                else{
                    echo "<font color='#C1272D'>Product with this ID is does not exist!</font>";
                }


                ?>
                </form>
                <!-- END CONTENT -->



                    </div>


                    <div class="clearfix"></div>
                    <div id="content2">

            </div>
            <div class="clearfix"></div>
        </div>
        <script language="javascript" type="text/javascript">
            /* preview uploaded image */
            $(document).ready(function(e){
                displayController('<?php echo BASE_RELATIVE;?>view/3rdparty/file_uploader/user-addpic.php?cid=<?php echo $carid;?>');
            });

            function basename(path) {
               return path.split('/').reverse()[0];
            }

			function save_data_form(){

				$('#submitbtn').trigger('click');

			}
		    function check_vehicle_data(){
				 var data_return=false;
				 if($('#makerInputSelect').val()=="" || $('#modelInputSelect').val()=="" || $('#countryInputSelect').val()=="" ||  $('#madeInInputSelect').val()==""){
					data_return= false;
				 }
				 else{
					 data_return= true;
				 }
				 return data_return;

			}



			 function carImageUploadedNew_Photo(id,proid,url,mode,fname,i){
                        var did = 'img' + Math.round((Math.random()*1000));
                        var rd='rd' + id;
                        var wrap = document.getElementById('outoutimg');
                        var html = '<div id="'+did+'" class="img-wrap">';
                        html += '<img src="'+url+'" width="200" height="150" style="margin:5px;" />';
                        html += '<p style="margin:0;padding-left:5px;">';

						if(i==0){

							 html +='<input type="radio"  onclick="checkimg(\''+id+'\',\''+proid+'\',\''+rd+'\');return true;" id='+rd+' name="primary_photo[]" value="'+fname+'"  checked="checked"  />Set as Primary Photo';
						}
                        else{
							 html +='<input type="radio" onclick="checkimg(\''+id+'\',\''+proid+'\',\''+rd+'\');return true;" id='+rd+' name="primary_photo[]" value="'+fname+'"  />Set as Primary Photo';
						}
                        html += '<a href="#" onclick="removeImageHandler(\''+id+'\',\''+basename(url)+'\',\''+did+'\',\''+rd+'\');return false;" class="btn btn-small btn-danger" style="float:right;margin-right:5px;">×</a>';
                        html += '</p>';
                        html += '</div>';

                        wrap.innerHTML += html;
                    }
                    function ucfirst (str) {
                        str += '';
                        var f = str.charAt(0).toUpperCase();
                        return f + str.substr(1);
                   }




            function carImageUploaded(id,proid,url,mode,fname,primary_photo){
                        var did = 'img' + Math.round((Math.random()*1000));
                        var rd='rd' + id;
                        var wrap = document.getElementById('outoutimg');
                        var html = '<div id="'+did+'" class="img-wrap">';
                        html += '<img src="'+url+'" width="200" height="150" style="margin:5px;" />';
                        html += '<p style="margin:0;padding-left:5px;">';

						if(primary_photo==1){

							 html +='<input type="radio"  onclick="checkimg(\''+id+'\',\''+proid+'\',\''+rd+'\');return true;" id='+rd+' name="primary_photo[]" value="'+fname+'"  checked="checked"  />Set as Primary Photo';
						}
                        else{
							 html +='<input type="radio" onclick="checkimg(\''+id+'\',\''+proid+'\',\''+rd+'\');return true;" id='+rd+' name="primary_photo[]" value="'+fname+'"  />Set as Primary Photo';
						}
                        html += '<a href="#" onclick="removeImageHandler(\''+id+'\',\''+basename(url)+'\',\''+did+'\',\''+rd+'\');return false;" class="btn btn-small btn-danger" style="float:right;margin-right:5px;">×</a>';
                        html += '</p>';
                        html += '</div>';

                        wrap.innerHTML += html;
                    }
                    function ucfirst (str) {
                        str += '';
                        var f = str.charAt(0).toUpperCase();
                        return f + str.substr(1);
                   }
        </script>
