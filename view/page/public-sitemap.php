<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';
// get form feed.
$fstatus = $_CLASS->getFormStatus();
$fmessage = $_CLASS->getFormMessage();
$fstyle = $_CLASS->getFormStyle();
$group = 'public';
$slug  = 'car';
// load page content.
$pg_details = $_CLASS->getPageHTML();

?>


<link rel="stylesheet" href="<?php echo BASE_RELATIVE;?>css/sitemap.css" />

 <div id="sectionContenWrapper">
        
    <div id="sectionContent">
    	<div id="sitemap">
        	<div id="sitemap_title">
                <p>Trade Guide</p>
            </div>
            <table width="960" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <th><p>Item</p></th>
                </tr>
                <tr>
                    <td>
                    	<ul>
                        	<li class="title"><a href="">CAR</a></li>
                            <li><a href="">Used Hyundai</a></li>
                            <li><a href="">Used Toyota</a></li>
                            <li><a href="">Used Kia</a></li>
                            <li><a href="">Used Honda</a></li>
                            <li><a href="">Used GM Daewoo</a></li>
                        </ul>
                        <ul>
                        	<li class="title"><a href="">TRUCK</a></li>
                            <li><a href="">Used Cargo Trucks</a></li>
                            <li><a href="">Used wing Body Trucks</a></li>
                            <li><a href="">Used Tractor Heads</a></li>
                            <li><a href="">Used Refrigerated Trucks</a></li>
                        </ul>
                        <ul>
                        	<li class="title"><a href="">BUS</a></li>
                            <li><a href="">Used Hyundai Buses</a></li>
                            <li><a href="">Used Kia Buses</a></li>
                            <li><a href="">Used Daewoo Buses</a></li>
                            <li><a href="">Used Nissan Buses</a></li>
                            <li><a href="">Used Toyota Buses</a></li>
                        </ul>
                        <ul>
                        	<li class="title"><a href="">Heavy Machine</a></li>
                            <li><a href="">Excavators</a></li>
                            <li><a href="">Dump Trucks</a></li>
                            <li><a href="">Attachments</a></li>
                            <li><a href="">Parts/Accessories</a></li>
                            <li><a href="">Loaders/Dozers/Graders</a></li>
                        </ul>
                        <ul>
                        	<li class="title"><a href="">PARTS/ACCESSORY</a></li>
                            <li><a href="">Used Engines</a></li>
                            <li><a href="">Used Parts</a></li>
                            <li><a href="">Chrome Accessories</a></li>
                            <li><a href="">Brake Pads</a></li>
                            <li><a href="">Compressors</a></li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <th><p>Register Item</p></th>
                </tr>
                <tr>
                    <td>
                    	<ul id="category">
                        	<li><a href="">Place an AD</a></li>
                            <li><a href="">Membership Guide</a></li>
                            <li><a href="">Register My Car</a></li>
                            <li><a href="">Register MyTruck</a></li>
                            <li><a href="">Register My Bus</a></li>
                            <li><a href="">Register My Heavy Machine</a></li>
                            <li><a href="">Register My Part</a></li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <th><p>Buyer's Inquiry</p></th>
                </tr>
                <tr>
                    <td>
                    	<ul id="category">
                        	<li><a href="">See Buyer's Inquiry</a></li>
                            <li><a href="">Write an inquiry</a></li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <th><p>Find Trader</p></th>
                </tr>
                <tr>
                    <td>
                    	<ul id="category">
                        	<li><a href="">Expert Company</a></li>
                            <li><a href="">Seller's Directory</a></li>
                            <li><a href="">Buyer's Directory</a></li>
                            <li><a href="">Forwarder's Directory</a></li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <th><p>Help</p></th>
                </tr>
                <tr>
                    <td>
                    	<ul id="category">
                        	<li><a href="">How to Buy</a></li>
                            <li><a href="shipping-estimate">Shipping Information</a></li>
                            <li><a href="">Trade Guide</a></li>
                            <li><a href="">Blacklist Report/Search</a></li>
                            <li><a href="">FAQ</a></li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <th><p>Community</p></th>
                </tr>
                <tr>
                    <td>
                    	<ul id="category">
                        	<li><a href="">Deal of the week</a></li>
                            <li><a href="">Trade Information</a></li>
                            <li><a href="">Auto Information</a></li>
                            <li><a href="">News</a></li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <th><p>My Wini</p></th>
                </tr>
                <tr>
                    <td>
                    	<ul id="category">
                        	<li><a href="">My Information</a></li>
                            <li><a href="">Message Box</a></li>
                            <li><a href="">My Inquiry</a></li>
                            <li><a href="">My Inventory</a></li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <th><p>Login</p></th>
                </tr>
                <tr>
                    <td>
                    	<ul id="category">
                        	<li><a href="">Register</a></li>
                            <li><a href="">Sign in</a></li>
                            <li><a href="">ID Search</a></li>
                            <li><a href="">Password Search</a></li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <th><p>About Us</p></th>
                </tr>
                <tr>
                    <td>
                    	<ul id="category">
                        	<li><a href="">About Us</a></li>
                            <li><a href="">Contact Us</a></li>
                        </ul>
                    </td>
                </tr>
            </table>

        </div>
        
    </div><!-- end div id="sectionContent"-->
</div><!-- end div id="sectionContentWraper" -->       
  
  
  