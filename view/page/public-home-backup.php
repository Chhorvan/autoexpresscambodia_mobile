
<link href="<?php echo BASE_RELATIVE;?>css/home/home.css" rel="stylesheet" type="text/css" />

<?php 
	include("ajax/count_country.php");
	
?>
<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE..
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';

// get form feed.
$fstatus = $_CLASS->getFormStatus();
$fmessage = $_CLASS->getFormMessage();
$fstyle = $_CLASS->getFormStyle();
$listnews = $_CLASS->getNewsList();
// get maker list.
$maker_list = $_CLASS->getCar();
$deal_week = $_CLASS->loadDealweek();
$special_offer = $_CLASS->getSpecialOffer();
$totalBuyerDirectory=$_CLASS->getBuyerDirectory();
//$buyer_inquiry_list=$_CLASS->getBuyerInquiryList();

?>          
	<div id="sectionContenWrapper">
        	<!--sectionSidebar-->
        	<div id="sectionSidebar">          
            	<div id="topSidebar">
                	<ul>
                    	<li class="sidebarCar"><a href="vehicle?product_type=Car" class="linkfade">Cars</a></li>
                        <li class="sidebarTruck"><a href="vehicle?product_type=Truck" class="linkfade">Trucks</a> </li>
                        <li class="sidebarBus"><a href="vehicle?product_type=Bus" class="linkfade">Buses</a></li>
                        <li class="sidebarTrucktor"><a href="vehicle?product_type=Equipment" class="linkfade">Heavy Machines</a></span></li>
                        <li class="sidebarAcessory"><a href="vehicle?product_type=Part" class="linkfade">Auto Part/Acessories</a></li>
                    </ul>
                </div>
                <div id="itemsFrom">
                	
                </div>
                <div id="fromCountry">
                    <?php 
                        $product_type="";
                        $country_count_num=array(
                            "kr"=>countCountry('kr', $product_type),
                            "jp"=>countCountry('jp', $product_type),
                            "ru"=>countCountry('ru', $product_type),
                            "us"=>countCountry('us', $product_type),
                            "vn"=>countCountry('vn', $product_type),
                            "ae"=>countCountry('ae', $product_type),
                            "cn"=>countCountry('cn', $product_type),
                            "be"=>countCountry('be', $product_type)

                        );
                        $country_count_item=array(
                            "kr"=>"<li id='korea'><a href='vehicle?country=kr' class='linkfade'>S.Korea <span>({$country_count_num['kr']})</span></a></li>",
                            "jp"=>"<li id='japan'><a href='vehicle?country=jp' class='linkfade'>Japan <span>({$country_count_num['jp']})</span></a></li>",
                            "ru"=>"<li id='russia'><a href='vehicle?country=ru' class='linkfade'>Russia <span>({$country_count_num['ru']})</span></a></li>",
                            "us"=>"<li id='usa'><a href='vehicle?country=us' class='linkfade'>USA <span>({$country_count_num['us']})</span></a></li>",
                            "vn"=>"<li id='vn'><a href='vehicle?country=vn' class='linkfade'>Vietnam <span>({$country_count_num['vn']})</span></a></li>",
                            "ae"=>"<li id='uae'><a href='vehicle?country=ae' class='linkfade'>U.A.E <span>({$country_count_num['ae']})</span></a></li>",
                            "cn"=>"<li id='china'><a href='vehicle?country=cn' class='linkfade'>China <span>({$country_count_num['cn']})</span></a></li>",
                            "be"=>"<li id='belguim'><a href='vehicle?country=be' class='linkfade'>Belgium <span>({$country_count_num['be']})</span></a></li>"        
                        );
                        arsort($country_count_num);

                    ?>
                    <ul>
                        <?php foreach($country_count_num as $key=>$value){
                            //echo $value;
                            echo $country_count_item[$key];
                            //echo $key;
                        }
                        ?>
                    </ul>
                </div>
                <div id="weeklyCarTitle">
                	
                </div>
                <div id="weeklyCar">                
                	<?php foreach ($deal_week as $row){?>
                        <div class="carDetail">
                            <?php                                       
                                    if(!file_exists($row['source'])){                                                  
                                    ?>
                                        <img src="<?php echo BASE_RELATIVE;?>images/noimg.jpg" width="210" height="155" />
                                <?php                                           
                                    }else{
                                ?>
                                        <img src="<?php echo BASE_RELATIVE.$row['source'];?>"  />
                                <?php
                                        }
                                ?>

                            
                            <p>
                                <a href="<?php echo BASE_RELATIVE;?>community-view?id=<?php echo $row['id'].'&owner='.$row['owner']?>" class="linkfade"> <?php echo $row['title']?> </a>
                            </p>
                        </div>
                    <?php } ?>
                </div>
                
                <div id="newsTitle">                	
                </div>
                <div id="newsTitleBox">
                    <?php 
                        foreach($listnews as $new){
                    ?>
                	<p ><?php echo $new['date'];?><span>
                        <a href="<?php echo BASE_RELATIVE.'community-news-details?id='.$new['id'];?>" class="linkfade">
                        <?php echo substr($new['title'], 0,25)." ... ";?>
                        </a>
                    </span></p>
                    <?php
                        }
                    ?>
                </div>
            <!--Sidebar--></div>
            
            <div id="sectionContent">
            	<div id="banner">                	
                
                
                
                 <?php
                    if( !empty($_NIVO_SLIDER) )
                    {
                        $blist = $_NIVO_SLIDER->getBannerList();

                        if( count($blist) > 0 )
                        {
                ?>
                     
                <?php
                            for( $i = 0; $i < count($blist); $i++ )
                            {
                                $arr = $blist[$i];

                              
                                
                ?>
                       <img src="<?php echo BASE_RELATIVE . $blist[$i]['source'];?>"  />     
                <?php

                            }
                ?>
                        
                <?php
                        }
                    }
                ?>
                
                    
                </div>	
                <div id="btnSlideWraper">
                	<div id="btnSlide">
                    	<ul id="nav"> </ul>
                    </div>
                </div><!-- end div id="btnSlideWraper"-->
                <div id="specialOffer">
                	<img src="<?php echo BASE_RELATIVE;?>images/home/specialoffer.png" />
                </div>
                <div id="carOffer">
                    <?php
                    foreach ($special_offer as $index => $value) {
                        if(($index % 4) === 0){ 
                    ?>
                            <div id="carDetail" class="clearfix">
                    <?php
                        }
                    ?>
                        <div id="showCar">
                            <div class="carImg">
                                <a href="#" class="linkfade">
                                <?php                                       
                                    if(!file_exists('image/upload/cars/special_offer-'.$value['carImg'])){                                                  
                                    ?>
                                        <img src="<?php echo BASE_RELATIVE;?>images/noimg.jpg" width="157" height="89" />
                                <?php                                           
                                    }else{
                                ?>
                                        <img src="<?php echo BASE_RELATIVE; ?>image/upload/cars/special_offer-<?php echo $value['carImg'];?>" />
                                <?php
                                        }
                                ?>
                                </a>
                            </div>
                            <div class="carName">
                                <p><?php echo $value['model_year']. ' '. $value['make']. ' '. $value['model'] ?><br/>
                                    Transform
                                </p>
                            </div>
                            <div class="carPrice">
                                <p> <span>USD</span> <?php echo $value['price'] ?> </p>
                            </div>
                        </div>
                    <?php
                        if((($index + 1) % 4 === 0) || (($index + 1) === count($special_offer))){ ?>
                            </div>  
                    <?php 
                        }                 
                    }
                    ?>
                </div><!-- end div id="carOffer"-->
                <div id="items" class="clearfix">
                	<div id="directory">
                    	<p class="center">Seller's Directory</p>
                    </div>
                    <div id="newestAutoTrade">
                    	<p class="center">Buyer's Directory </p>
                    </div>
                    <div id="newestAutoTrade">
                    	<p class="center"> Newest Auto Trade</p>
                    </div>
                    <div id="directoryDetial">
                    	<p class="car"><a href="#" class="linkfade">Cars</a></p>
                        <p class="truck"><a href="#" class="linkfade">Trucks</a></p>
                        <p class="bus"><a href="#" class="linkfade">Buses</a></p>
                        <p class="trucktor"><a href="#" class="linkfade">Heavy Machines</a></p>
                        <p class="door"><a href="#" class="linkfade">Equipment parts</a></p>
                        <p class="wheel"><a href="#" class="linkfade">Spare Parts</a></p>
                        <p class="acessory"><a href="#" class="linkfade">Attachments</a></p>                       
                    </div>
                    <div id="newestAutoTradeDetial">
                    	<p id="korea"><a href="buyer-directory?country=kr" class="linkfade">S.Korea <span>(<?php echo (isset($totalBuyerDirectory['kr']) ? $totalBuyerDirectory['kr'] : '0'); ?>)</span></a></p>
                        <p id="japan"><a href="buyer-directory?country=jp" class="linkfade">Japan <span>(<?php echo (isset($totalBuyerDirectory['jp']) ? $totalBuyerDirectory['jp'] : '0'); ?>)</span></a></p>
                        <p id="russia"><a href="buyer-directory?country=ru" class="linkfade">Russia <span>(<?php echo (isset($totalBuyerDirectory['ru']) ? $totalBuyerDirectory['ru'] : '0'); ?>)</span></a></p>
                        <p id="usa"><a href="buyer-directory?country=us" class="linkfade">USA <span>(<?php echo (isset($totalBuyerDirectory['us']) ? $totalBuyerDirectory['us'] : '0'); ?>)</span></a></p>
                        <p id="vn"><a href="buyer-directory?country=vn" class="linkfade">Vietnam <span>(<?php echo (isset($totalBuyerDirectory['vn']) ? $totalBuyerDirectory['vn'] : '0'); ?>)</span></a></p>
                        <p id="uae"><a href="buyer-directory?country=ae" class="linkfade">U.A.E <span>(<?php echo (isset($totalBuyerDirectory['ae']) ? $totalBuyerDirectory['ae'] : '0'); ?>)</span></a></p>
                        <p id="china"><a href="buyer-directory?country=cn" class="linkfade">China <span>(<?php echo (isset($totalBuyerDirectory['cn']) ? $totalBuyerDirectory['cn'] : '0'); ?>)</span></a></p>
                        <p id="belguim"><a href="buyer-directory?country=be" class="linkfade">Belgium <span>(<?php echo (isset($totalBuyerDirectory['be']) ? $totalBuyerDirectory['be'] : '0'); ?>)</span></a></p>
                        
                    </div>
                    <div id="newestAutoTradeDetial">
                    	<p id="korea"><a href="#" class="linkfade">S.Korea</a> </p>
                        <p id="japan"><a href="#" class="linkfade">Japan</a></p>
                        <p id="russia"><a href="#" class="linkfade">Russia</a> </p>
                        <p id="usa"><a href="#" class="linkfade">USA</a></p>
                        <p id="vn"><a href="#" class="linkfade">Vietnam</a> </p>
                        <p id="uae"><a href="#" class="linkfade">U.A.E</a> </p>
                        <p id="china"><a href="#" class="linkfade">China</a> </p>
                        <p id="belguim"><a href="#" class="linkfade">Belgium</a> </p>
                    </div>
                </div><!-- end div id="items"-->
                
                <div id="itemsTab">
                	<div id="buyerInquiry"></div>
                    <div id="tabs">
                    	<ul>
                        	<li id="tabAll" class="current tabsWrap all"><a title="all" href="#">All</a></li>
                            <li id="tabCar" class="tabsWrap"><a title="car" href="#">Car</a></li>
                            <li id="tabTruck" class="tabsWrap"><a title="truck" href="#">Truck</a></li>
                            <li id="tabBus" class="tabsWrap"><a title="bus" href="#">Bus</a></li>
                            <li id="tabEquipment" class="tabsWrap"><a title="equipment" href="#">Equipment</a></li>
                            <li id="tabPart" class="tabsWrap"><a title="auto" href="#">Auot Parts</a></li>
                        </ul>
                    </div>
                </div>
                <div id="tab_all" class="subTab">   

                    <ul>
                        
                    </ul>
            	</div>
                <div id="tab_car" class="subTab">                    
                    <ul>
                        
                        
                        
                    </ul>                    
            	</div>
                <div id="tab_truck" class="subTab">                    
                    <ul>
                                              
                    </ul>                    
            	</div>
                <div id="tab_bus" class="subTab">                    
                    <ul>
                                                 
                    </ul>                    
            	</div>
                <div id="tab_equipment" class="subTab">                    
                    <ul>
                                             
                    </ul>                    
            	</div>
                <div id="tab_auto" class="subTab">                    
                     <ul>
                                             
                    </ul>                 
            	</div>
            </div>
        </div><!-- end div id="sectionContent" --> 
	<div class="clearfix"></div>

