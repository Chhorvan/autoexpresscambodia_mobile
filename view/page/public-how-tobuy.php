<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';
// get form feed.
$fstatus = $_CLASS->getFormStatus();
$fmessage = $_CLASS->getFormMessage();
$fstyle = $_CLASS->getFormStyle();
$group = 'public';
$slug  = 'car';
// load page content.
$pg_details = $_CLASS->getPageHTML();

?>




 <div id="sectionContenWrapper">
    <!--sectionSidebar--> 
    <div id="topMenu">
        <p><a href="#" class="linkfade">Help</a> > How to Buy</p>
    </div>
    <?php include("php/sidebar/help.php");?>    
    <div id="contact_info">
            	<p>Contact</p>
            </div>
            <div id="contact_detail">
            	<p class="title">BLAUDA CO., LTD.</p>
                <p>ADDRESS : M-2602, 32, Songdo <br/>
                    <span>Gwahak-ro, Yeonsu-gu,</span> <br/>
                    <span>Incheon, Korea.</span> <br/>
                    TEL            : +82-32-715-7098    <br/>
                    FAX            : +82-32-712-4885<br/>
                    WEBSITE  : <a href="http://www.blauda.com" class="linkfade">www.blauda.com</a>
				</p>
            </div>
    </div>           
    <div id="sectionContent"> 
        <div id="help_title">
            <p>How to Buy</p>
        </div>
        <div class="contentText">
        	<p class="simple"><span>Simple & Easy!</span> That is how it works with Global iBlue! See instructions here about how to purchase new or used 
vehicles and parts at Global iBlue. Just follow the steps below. It is very easy to start!</p>
        	<div class="title margin_top"><p>Step.01 : Search Item</p></div>
            <div class="title_content1 clearfix">            	
                <div class="text">
                	<p>Search from Global iBlue online inventory to choose your <br/>target item by category!</p><br/>
                    <p>Go to <span>Buyer's Inquiry > Write</span> an inquiry if you are looking <br/>
for certain items that are not advertised on Global iBlue.com.</p>
                </div>
                <div class="image"><img src="<?php echo BASE_RELATIVE;?>images/how-to-buy/pc.png" /></div>
            </div>
            <div class="title margin_top"><p>Step.02 : Send an inquiry</p></div>
            <div class="title_content2 clearfix">
            	<div class="image"><img src="<?php echo BASE_RELATIVE;?>images/how-to-buy/sent_inquiry.png" /></div>
                <div class="text">
                	<p>You can easily reach the seller at the Item detail page.</p><br/>
                    <p>Global iBlue message system provides the quality sample messages in <br/>
arious languages for the convenience of our members.</p><br/>
					<p>Go to My wini > message box and check the Seller's reply and <br/>
messages you sent.</p>
                </div>
            </div>
            <div class="title margin_top"><p>Step.03 : Negotiate with the seller</p></div>
            <div class="title_content1 clearfix">            	
                <div class="text">
                	<p>Check the seller's reply through email or our inquiry system and <br/>
just keep on messaging with the seller.</p><br/>
                    <p>Using <span class="auto">Global iBlue's inquiry</span> system will be a lot easier and safer than <br/>
using personal emails to communicate with. It may probably cause <br/>
distraction to communicate with the seller because the sellers are <br/>
usually dealing with the large amount of items at once. Global iBlue's <br/>
inquiry system is convenient for exchanging seller's information and <br/>
the messages each other.</p><br/>
                </div>
                <div class="image"><img src="<?php echo BASE_RELATIVE;?>images/how-to-buy/negotiate.png" /></div>
            </div>
            <div class="title margin_top"><p>Step 04. Request for proforma Invoice seller</p></div>
            <div class="title_content2 clearfix">
            	<div class="image"><img src="<?php echo BASE_RELATIVE;?>images/how-to-buy/request.png" /></div>
                <div class="text">
                	<p>Once both has agreed on purchase, ask seller to issue proforma <br/>
invoice</p><br/>
                    <p>Seller will send you an invoice with vehicle's VIN number <br/>
(Chassis number) and the bank account details.</p>
                </div>
            </div>
            <div class="title margin_top"><p>Step 05. Shipment schedule & Shoring</p></div>
            <div class="title_content1 clearfix">            	
                <div class="text">
                	<p>When the seller receives payments from the bank, he will prepare <br/>
shipment for you.</p><br/>
                    <p>The seller will deliver the shipment schedule for delivery to you after <br/>
getting it from the freight forwarder.</p><br/>
					<p>In the case of container shipping, the seller will give you the information <br/>
& photos of shoring (prior consent before processing).</p>
                </div>
                <div class="image"><img src="<?php echo BASE_RELATIVE;?>images/how-to-buy/shipping.png" /></div>
            </div>
             <div class="title margin_top"><p>Step 06. Get shipping documents</p></div>
            <div class="title_content2 clearfix">
            	<div class="image"><img src="<?php echo BASE_RELATIVE;?>images/how-to-buy/get_shipping.png" /></div>
                <div class="text">
                	<p>Once both has agreed on purchase, ask seller to issue proforma <br/>
invoice</p><br/>
                    <p>Seller will send you an invoice with vehicle's VIN number <br/>
(Chassis number) and the bank account details.</p>
                </div>
            </div>
            <div class="title margin_top"><p>Step 07. Pick up the item at the port</p></div>
            <div class="title_content1 clearfix">            	
                <div class="text">
                	<p>When the seller receives payments from the bank, he will prepare <br/>
shipment for you.</p><br/>
                    <p>You pick up the stock at your port when the item you purchased arrived. <br/>
To get your item, you are required to hand in the original documents of <br/>
B/L to the particular shipping company.</p>
                </div>
                <div class="image"><img src="<?php echo BASE_RELATIVE;?>images/how-to-buy/pick_item.png" /></div>
            </div>
        </div>
        
        
        
    </div><!-- end div id="sectionContent"-->
</div><!-- end div id="sectionContentWraper" -->       
  
  
  