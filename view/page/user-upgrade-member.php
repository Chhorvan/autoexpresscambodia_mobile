<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';

// get form feed.
$fstatus = $_CLASS->getFormStatus();
$fmessage = $_CLASS->getFormMessage();
$fstyle = $_CLASS->getFormStyle();

$news = $_CLASS->getNews();
$userInfo = $_CLASS->getUserInfo();
$_CLASS->upgradeMember();
?>
<link rel="stylesheet" href="<?php echo BASE_RELATIVE;?>css/upgrade_member/upgrade_member.css" />
<script type="text/javascript" src="<?php echo BASE_RELATIVE;?>js/fancybox/jquery.fancybox.js"></script>
<script type="text/javascript" src="<?php echo BASE_RELATIVE;?>js/blog/common.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		
        $('#back a').click(function(){
		parent.$.fancybox.close();
		});
    });
</script>
<div id="content">
	<div id="title">
    	<p> Apply for Expert Company</p>
    </div>
    <div id="table_content">
    	<form action="<?php echo BASE_RELATIVE;?>upgrade-member?popup" method="post" enctype="multipart/form-data">
    	<table border="0" cellpadding="0" cellspacing="0">
        	<?php
				foreach($userInfo as $row){
			?>
        	<tr>
            	<td class="label">User ID</td>
                <td class="form">Blaudacam</td>
            </tr>
            <tr>
            	<td class="label">Name</td>
                <td class="form"><input type="text" name="name" class="text_box" placeholder="<?php echo $row['name']?>" /></td>
            </tr>
            <tr>
            	<td class="label">Email</td>
                <td class="form"><input type="text" name="email" class="text_box" placeholder="<?php echo $row['email']?>" /></td>                
            </tr>
            <tr>
            	<td class="label">Tel</td>
                <td class="form"><input type="text" class="text_box" placeholder="<?php echo $row['tel']?>"/></td>
            </tr>
            <tr>
            	<td class="label">Country</td>
                <td class="form"><input type="text" class="text_box" placeholder="Cambodia"/></td>
            </tr>
            <tr>
            	<td class="label">Company Name</td>
                <td class="form"><input type="text" class="text_box" placeholder="<?php echo $row['company_name']?>"/></td>
            </tr>
            <tr>
            	<td class="label">Homepage</td>
                <td class="form"><input type="text" class="text_box" placeholder="<?php echo $row['web']?>"/></td>
            </tr>
            <tr>
            	<td class="label_checkbox">Available Service</td>
                <td class="form">
                	<ul class="clearfix">
                    	<li><input type="checkbox" value="car" name="car"/> Car</li>
                        <li><input type="checkbox" value="track" name="track"/> Truck</li>
                        <li><input type="checkbox" value="bus" name="bus"/> Bus</li>
                        <li><input type="checkbox" value="construction_equipment" name="construction_equipment"/> Construction Equipment</li>	
                        <li><input type="checkbox" value="attachment" name="attachment"/> Attachment</li>
                        <li><input type="checkbox" value="equipment_part" name="equipment_part"/> Equipment Parts</li>
                        <li><input type="checkbox" value="special_vehivle" name="special_vehivle"/> Special Vehicle</li>
                        <li><input type="checkbox" value="auto_part" name="auto_part"/> Auto Parts</li>
                        <li><input type="checkbox" value="auto_accessory" name="auto_accessory"/> Auto Accessories</li>
                        <li><input type="checkbox" value="automobile_recycling" name="automobile_recycling"/> Automobile recycling</li>
                        <li><input type="checkbox" value="engine_oil" name="engine_oil"/> Engine oil & Chemicals</li>
                        <li><input type="checkbox" value="shipping_shoring" name="shipping_shoring"/> Shipping & Shoring</li>
                        
                    </ul>
                </td>
            </tr>
            <tr>
            	<td class="label_checkbox">Message</td>
                <td class="form"><textarea class="text_area" name="message"></textarea></td>
            </tr>
            <tr>
            	<td class="label_checkbox">Attached File</td>
                <td class="form">
                	<div id="attach" class="clearfix">                    	                     
                        <div class='file_upload' id='f1'><input name='fileInput[]' type='file'/></div>
                        <div id='file_tools'>                        	
                            <img class="button" src='images/btnadd.gif' id='add_file' title='Add new input'/>
                            <img class="button" src='images/btndelete.gif' id='del_file' title='Delete'/>
                        </div>
                    </div>
                </td>
            </tr>
            <?php
				}
			?>
        </table>
        <div id="button_wrap">
            
                <input type="submit" class="upload" value="" name="upgrade-member"/>
                <input type="hidden" name="member_type" value="<?php echo $_GET['member_type']?>" />
                <div id="back">
                    <a href="#"><img src="<?php echo BASE_RELATIVE;?>images/common/button/cancle.png" /></a>
                </div>
            
        </div>
        </form>
    </div>
</div><!-- end div id ="content"-->
