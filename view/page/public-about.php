<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';
// get form feed.
$fstatus = $_CLASS->getFormStatus();
$fmessage = $_CLASS->getFormMessage();
$fstyle = $_CLASS->getFormStyle();
$group = 'public';
$slug  = 'car';
// load page content.
$pg_details = $_CLASS->getPageHTML();

?>

<link rel="stylesheet" href="<?php echo BASE_RELATIVE;?>css/auto_forget_password/style.css" />
<div id="forget-password-wrapper">
   <p>Forgot User ID or Password</p>
   <div class="container-form">
    <form action="" method="post">
	 <table>
		<tr>
			<td><p>Please fill in your email in form below</p></td>
		</tr>
		<tr>
			<td><input class="form-control border input-email" type="text" name="emailaddress" placeholder="enter your email address"/></td>
		</tr>
		<tr>
			<td>
			    <div class="radio-space">
				<input type="radio" name="forgot"/>&nbsp;&nbsp;<font>I forgot my user ID</font>&nbsp;&nbsp;
				<input type="radio" name="forgot"/>&nbsp;&nbsp;<font>I forgot my password</font>
				</div>
			</td>
		</tr>
		<tr>
			<td>
			   <div>
					<input type="submit" class="submit-space" value="" style="opacity: 1;"/>
			   </div>
			</td>
		</tr>
	 </table>
	 </form>
   </div>
   <div class="container-img">
		<img src="<?php echo BASE_RELATIVE;?>img/public_forgot_password/lamp.jpg"/ >
	</div>	
</div>