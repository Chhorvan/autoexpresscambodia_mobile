<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';

// get form feed.
$fstatus = $_CLASS->getFormStatus();
$fmessage = $_CLASS->getFormMessage();
$fstyle = $_CLASS->getFormStyle();

// load car details list.
$car_detail = $_CLASS->getCarDetails();

require_once BASE_CLASS . 'class-utilities.php';
?>
        <div id="content-wrapper">
            <div id="vertical-menu">
                <?php $_MENU->loadMenu($_GLANG); ?>
            </div>
            <div id="inner-content">
                <!-- START CONTENT -->
                <h1><?php echo $_LANG['ADVANCED_SEARCH_HEADER'];?></h1>
                <?php
                    if( $fstatus ){
                ?>
                <div class="alert <?php echo $fstyle;?>">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <?php echo $fmessage;?>
                </div>
                <?php
                    }
                    
                    // validate number of results.
                    if( count($car_detail) < 1 ){
                ?>
                <div class="alert alert-info">
                    <?php echo $_LANG['ADVANCED_SEARCH_NORESULT_LABEL'];?>
                </div>
                <?php
                    }
                    else {
                ?>
                <form method="post" class="searchForm">
                    <div class="tabDiv">
                        <p><?php echo $_LANG['ADVANCED_SEARCH_DOOR_LABEL'];?></p>
                        <select name="doorInput" style="width:226px;">
                            <option value="any">- <?php echo $_LANG['ADVANCED_SEARCH_OPTION_ANY_LABEL'];?> -</option>
                            <option disabled="disabled">------------------------------</option>
                            <?php
                                $door = $car_detail['door'];
                                
                                if( count($door) > 0 ){
                                    for( $i=0; $i < count($door); $i++ ){
                            ?>
                            <option value="<?php echo stripslashes($door[$i]);?>"><?php echo stripslashes($door[$i]);?></option>
                            <?php
                                    }
                                }
                            ?>
                        </select>
                        <p><?php echo $_LANG['ADVANCED_SEARCH_COLOR_LABEL'];?></p>
                        <select name="colorInput" style="width:226px;">
                            <option value="any">- <?php echo $_LANG['ADVANCED_SEARCH_OPTION_ANY_LABEL'];?> -</option>
                            <option disabled="disabled">------------------------------</option>
                            <?php
                                $color = $car_detail['color'];
                                
                                if( count($color) > 0 ){
                                    for( $i=0; $i < count($color); $i++ ){
                            ?>
                            <option value="<?php echo stripslashes($color[$i]);?>"><?php echo stripslashes($color[$i]);?></option>
                            <?php
                                    }
                                }
                            ?>
                        </select>
                        <p><?php echo $_LANG['ADVANCED_SEARCH_TRIM_LABEL'];?></p>
                        <select name="trimInput" style="width:226px;">
                            <option value="any">- <?php echo $_LANG['ADVANCED_SEARCH_OPTION_ANY_LABEL'];?> -</option>
                            <option disabled="disabled">------------------------------</option>
                            <?php
                                $trim = $car_detail['trim'];
                                
                                if( count($trim) > 0 ){
                                    for( $i=0; $i < count($trim); $i++ ){
                            ?>
                            <option value="<?php echo stripslashes($trim[$i]);?>"><?php echo stripslashes($trim[$i]);?></option>
                            <?php
                                    }
                                }
                            ?>
                        </select>
                        <p><?php echo $_LANG['ADVANCED_SEARCH_MILAGE_KILOMETER_LABEL'];?></p>
                        <select name="milesInput" style="width:226px;">
                            <option value="any">- <?php echo $_LANG['ADVANCED_SEARCH_OPTION_ANY_LABEL'];?> -</option>
                            <option disabled="disabled">------------------------------</option>
                            <?php
                                $miles = $car_detail['miles'];
                                
                                if( count($miles) > 0 ){
                                    for( $i=0; $i < count($miles); $i++ ){
                            ?>
                            <option value="<?php echo stripslashes($miles[$i]);?>"><?php echo str_replace(':',' ',$miles[$i]);?></option>
                            <?php
                                    }
                                }
                            ?>
                        </select>
                        <p><?php echo $_LANG['ADVANCED_SEARCH_TORQUE_LABEL'];?></p>
                        <select name="torqueInput" style="width:226px;">
                            <option value="any">- <?php echo $_LANG['ADVANCED_SEARCH_OPTION_ANY_LABEL'];?> -</option>
                            <option disabled="disabled">------------------------------</option>
                            <?php
                                $torque = $car_detail['torque'];
                                
                                if( count($torque) > 0 ){
                                    for( $i=0; $i < count($torque); $i++ ){
                            ?>
                            <option value="<?php echo stripslashes($torque[$i]);?>"><?php echo stripslashes($torque[$i]);?></option>
                            <?php
                                    }
                                }
                            ?>
                        </select>                        
                    </div>
                    <div class="tabDiv">
                        <p><?php echo $_LANG['ADVANCED_SEARCH_FUEL_TYPE_LABEL'];?></p>
                        <select name="fuelInput" style="width:226px;">
                            <option value="any">- <?php echo $_LANG['ADVANCED_SEARCH_OPTION_ANY_LABEL'];?> -</option>
                            <option disabled="disabled">------------------------------</option>
                            <?php
                                $fuel = $car_detail['fuel'];
                                
                                if( count($fuel) > 0 ){
                                    for( $i=0; $i < count($fuel); $i++ ){
                            ?>
                            <option value="<?php echo stripslashes($fuel[$i]);?>"><?php echo stripslashes($fuel[$i]);?></option>
                            <?php
                                    }
                                }
                            ?>
                        </select>
                        <p><?php echo $_LANG['ADVANCED_SEARCH_YEAR_LABEL'];?></p>
                        <select name="yearInput" style="width:226px;">
                            <option value="any">- <?php echo $_LANG['ADVANCED_SEARCH_OPTION_ANY_LABEL'];?> -</option>
                            <option disabled="disabled">------------------------------</option>
                            <?php
                                $year = $car_detail['year'];
                                
                                if( count($year) > 0 ){
                                    for( $i=0; $i < count($year); $i++ ){
                            ?>
                            <option value="<?php echo stripslashes($year[$i]);?>"><?php echo stripslashes($year[$i]);?></option>
                            <?php
                                    }
                                }
                            ?>
                        </select>
                        <p><?php echo $_LANG['ADVANCED_SEARCH_TYPE_LABEL'];?></p>
                        <select name="typeInput" style="width:226px;">
                            <option value="any">- <?php echo $_LANG['ADVANCED_SEARCH_OPTION_ANY_LABEL'];?> -</option>
                            <option disabled="disabled">------------------------------</option>
                            <?php
                                $type = $car_detail['type'];
                                
                                if( count($type) > 0 ){
                                    for( $i=0; $i < count($type); $i++ ){
                            ?>
                            <option value="<?php echo stripslashes($type[$i]);?>"><?php echo stripslashes($type[$i]);?></option>
                            <?php
                                    }
                                }
                            ?>
                        </select>
                        <p><?php echo $_LANG['ADVANCED_SEARCH_PRICE_LABEL'];?></p>
                        <select name="priceInput" style="width:226px;">
                            <option value="any">- <?php echo $_LANG['ADVANCED_SEARCH_OPTION_ANY_LABEL'];?> -</option>
                            <option disabled="disabled">------------------------------</option>
                            <?php
                                $price = $car_detail['price'];
                                
                                if( count($price) > 0 ){
                                    for( $i=0; $i < count($price); $i++ ){
                            ?>
                            <option value="<?php echo stripslashes($price[$i]);?>"><?php echo CURRENCY_SYMBOL . ' ' . Utilities::formatPrice($price[$i],CURRENCY_CODE) . ' ' . CURRENCY_CODE;?></option>
                            <?php
                                    }
                                }
                            ?>
                        </select>
                        <p><?php echo $_LANG['ADVANCED_SEARCH_ENGINE_KW_LABEL'];?></p>
                        <select name="kwInput" style="width:226px;">
                            <option value="any">- <?php echo $_LANG['ADVANCED_SEARCH_OPTION_ANY_LABEL'];?> -</option>
                            <option disabled="disabled">------------------------------</option>
                            <?php
                                $kw = $car_detail['kw'];
                                
                                if( count($kw) > 0 ){
                                    for( $i=0; $i < count($kw); $i++ ){
                            ?>
                            <option value="<?php echo stripslashes($kw[$i]);?>"><?php echo stripslashes($kw[$i]);?></option>
                            <?php
                                    }
                                }
                            ?>
                        </select>
                    </div>
                    <div class="tabDiv">
                        <p><?php echo $_LANG['ADVANCED_SEARCH_TRANSMISSION_LABEL'];?></p>
                        <select name="transmissionInput" style="width:226px;">
                            <option value="any">- <?php echo $_LANG['ADVANCED_SEARCH_OPTION_ANY_LABEL'];?> -</option>
                            <option disabled="disabled">------------------------------</option>
                            <?php
                                $transmission = $car_detail['transmission'];
                                
                                if( count($transmission) > 0 ){
                                    for( $i=0; $i < count($transmission); $i++ ){
                            ?>
                            <option value="<?php echo stripslashes($transmission[$i]);?>"><?php echo stripslashes($transmission[$i]);?></option>
                            <?php
                                    }
                                }
                            ?>
                        </select>
                        <p><?php echo $_LANG['ADVANCED_SEARCH_ENGINE_SIZE_LABEL'];?></p>
                        <select name="engineSizeInput" style="width:226px;">
                            <option value="any">- <?php echo $_LANG['ADVANCED_SEARCH_OPTION_ANY_LABEL'];?> -</option>
                            <option disabled="disabled">------------------------------</option>
                            <?php
                                $engine = $car_detail['engine_size'];
                                
                                if( count($engine) > 0 ){
                                    for( $i=0; $i < count($engine); $i++ ){
                            ?>
                            <option value="<?php echo stripslashes($engine[$i]);?>"><?php echo stripslashes($engine[$i]);?></option>
                            <?php
                                    }
                                }
                            ?>
                        </select>
                        <p><?php echo $_LANG['ADVANCED_SEARCH_GEAR_LABEL'];?></p>
                        <select name="gearInput" style="width:226px;">
                            <option value="any">- <?php echo $_LANG['ADVANCED_SEARCH_OPTION_ANY_LABEL'];?> -</option>
                            <option disabled="disabled">------------------------------</option>
                            <?php
                                $gear = $car_detail['gear'];
                                
                                if( count($gear) > 0 ){
                                    for( $i=0; $i < count($gear); $i++ ){
                            ?>
                            <option value="<?php echo stripslashes($gear[$i]);?>"><?php echo stripslashes($gear[$i]);?></option>
                            <?php
                                    }
                                }
                            ?>
                        </select>
                        <p><?php echo $_LANG['ADVANCED_SEARCH_ENERGY_CLASS_LABEL'];?></p>
                        <select name="ecoInput" style="width:226px;">
                            <option value="any">- <?php echo $_LANG['ADVANCED_SEARCH_OPTION_ANY_LABEL'];?> -</option>
                            <option disabled="disabled">------------------------------</option>
                            <?php
                                $eco = $car_detail['eco'];
                                
                                if( count($eco) > 0 ){
                                    for( $i=0; $i < count($eco); $i++ ){
                            ?>
                            <option value="<?php echo stripslashes($eco[$i]);?>"><?php echo stripslashes($eco[$i]);?></option>
                            <?php
                                    }
                                }
                            ?>
                        </select>
                        <p><?php echo $_LANG['ADVANCED_SEARCH_TAX_BAND_LABEL'];?></p>
                        <select name="taxBandInput" style="width:226px;">
                            <option value="any">- <?php echo $_LANG['ADVANCED_SEARCH_OPTION_ANY_LABEL'];?> -</option>
                            <option disabled="disabled">------------------------------</option>
                            <?php
                                $tax = $car_detail['tax'];
                                
                                if( count($tax) > 0 ){
                                    for( $i=0; $i < count($tax); $i++ ){
                            ?>
                            <option value="<?php echo stripslashes($tax[$i]);?>"><?php echo stripslashes($tax[$i]);?></option>
                            <?php
                                    }
                                }
                            ?>
                        </select>
                    </div>
                    <div class="clearfix"></div>
                    <input type="submit" name="filterbtn" class="btn btn-info btn-small" value="<?php echo $_LANG['ADVANCED_SEARCH_BUTTON_LABEL'];?>" style="margin-left:10px;"/>
                </form>
                <?php
                    }
                    
                    // display last 10 featured vehicles (if any)
                ?>
                <!-- END CONTENT -->
            </div>
            <div class="clearfix"></div>
        </div>
