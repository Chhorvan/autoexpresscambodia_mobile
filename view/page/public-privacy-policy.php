<div id="wrapper-privacy-policy">
  <h1>Privacy Policy</h1>
  <h2>Prohibited Activities</h2>
  <p>The following is a partial list of the kinds of activities that are prohibited on or through the Site:</p>
  <ul>
	<li>
	    <font style='color:#0071ba;padding-right:3px;'>(a)</font> Submitting Material that is patently offensive to the online community, such as content that promotes racism, bigotry, hatred or physical harm of any kind against any 
group or individual.
	</li>
	<li>
	  <font style='color:#0071ba;padding-right:3px;'>(b)</font>Engaging in activities or submitting Material that could be harmful to minors.
	</li>
	<li>
	  <font style='color:#0071ba;padding-right:3px;'>(c)</font>Engaging in activity or submitting Material that harasses or advocates harassment of another person.
	</li>
	<li>
	  <font style='color:#0071ba;padding-right:3px;'>(d)</font>Engaging in activity that involves the transmission of "junk mail" or unsolicited mass mailing or "spam" to angkorauto.com Users or other.
	</li>
	<li>
	  <font style='color:#0071ba;padding-right:3px;'>(e)</font>Engaging in activity or submitting Material, promoting information, that is fraudulent, misleading, promotes illegal activities or conduct that is abusive, threatening, and obscene.
	</li>
	<li>
	  <font style='color:#0071ba;padding-right:3px;'>(f)</font>Submitting Material that contains restricted, hidden pages and images.
	</li>
	<li>
	  <font style='color:#0071ba;padding-right:3px;'>(g)</font>Submitting Material that provides instructional information about illegal activities violating someone's privacy, or providing or creating computer viruses.
	</li>
	<li>
		<font style='color:#0071ba;padding-right:3px;'>(h)</font>Engaging in commercial activities and/or sales without our prior written consent, such as contests, sweepstakes, barter, and advertising and pyramid scheme.
	</li>
	<li>
	    <font style='color:#0071ba;padding-right:3px;'>(i)</font>Using the Site's lead forms and/or toll-free numbers to advertise or promote products and services to angkorauto.com advertisers.
	</li>
	<li>
		<font style='color:#0071ba;padding-right:3px;'>(j)</font>Using any automatic device, or a manual process, to monitor web pages the Content or for any other unauthorized purpose without our prior expressed written permission.
	</li>
	<li>
		<font style='color:#0071ba;padding-right:3px;'>(k)</font>Using any device, software or routine to interfere or attempt to interfere with the proper working of the Site.
	</li>
	<li>
	   <font style='color:#0071ba;padding-right:3px;'>(l)</font>Decompiling, reverse engineering, disassembling or otherwise attempting to obtain the source code for the Software.
	</li>
	<li>
		<font style='color:#0071ba;padding-right:1px;'>(m)</font>Taking any action that imposes an unreasonable or disproportionately large load on angkorauto.com’s hardware and software infrastructure (collectively, "Prohibited Activities").
	</li>
  </ul>
</div>