	
    
    <div id="main-content">
    	<div class="page-title">
			<div class="title-left">Success</div>
            <div id="title"><img class="space" src="<?php echo BASE_RELATIVE;?>images/logout-icon/Cute-Ball-Go-icon.png" /></div>
            <p class="clear"></p>
		</div>
        
        <p class="first">Your password has been changed!</p>
        <p class="second">Go to <span><a class="link" href="<?php echo BASE_RELATIVE;?>login">log in</a></span> page now.</p>
		<p class="third"><span class="third-child">Having trouble accessing your account?</span><span> Please contact </span>
        <span> <a class="link-contact" href="mailto:support@motorbb.com">Online Customer Support.</a></span><p>
    </div>