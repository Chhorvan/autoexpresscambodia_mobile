<?php 
	
	//$get_carinfo=$_CLASS->car_info;
	
?>

<div id="wrapperPromotion" class="clearfix"> 
	<h1>My Promotions</h1>
	
	<div id="containerLeft">
		<h2>Add Promotion</h2>
		<form method="post" enctype="multipart/form-data">
			<table>
				<tr>
					<td class="tdLeft"><font class='fontColor'>Title :</font></td>
					<td class="tdRight"><input type='text' name='txt_title' class='textBox' required /></td>
				</tr>
				<tr>
					<td class="tdLeft"><font class='fontColor'>Image :</font></td>
					<td class="tdRight">
						<div class="borderText">
							<input id="uploadImage1" onchange="PreviewImage(1);" type="file" name="file" accept="image/*" class="textBox" required />							
						</div>												
					</td>					
				</tr>
				<tr>
					<td class="tdLeft"></td>
					<td class="tdRight"><p class="info">Image must be  860x140, otherwise it will not shown up</p></td>
				</tr>
				<tr>
					<td class="tdLeft"></td>
					<td class="tdRight"><img id="uploadPreview1" src="<?php echo BASE_RELATIVE; ?>img/user_promotion/upload/blank.jpg" /></td>
				</tr>
				<tr>
					<td class="tdLeft"></td>
					<td class="tdRight"><input type="submit" name="btn_add" value="Add" class="linkimg" /></td>
				</tr>
			</table>
		</form>
		
		<h2 class='myPromotion'>My Promotions</h2>
		<div id="imgView" class="clearfix">
			<img src="<?php echo BASE_RELATIVE; ?>img/user_promotion/upload/blank.jpg" />
			<span class="action pending">
				<img src="<?php echo BASE_RELATIVE; ?>img/user_promotion/pending.png" />
				<font>Pending</font>
			</span>
			<span class="action approved">
				<img src="<?php echo BASE_RELATIVE; ?>img/user_promotion/approved.png" />
				<font>Approved</font>
			</span>
			
			<span class="deleted">
				<a href="#">
					<img src="<?php echo BASE_RELATIVE; ?>img/user_promotion/btn_delete.png" class="linkimg" />
				</a>	
			</span>
		</div>
	</div><!-- End of containerLeft -->
	
	<div id='containerRight'>
		<img src='<?php echo BASE_RELATIVE.'img/user_promotion/poster.jpg'?>'/>
	</div><!-- End of container right -->
	
</div><!-- #wrapperPromotion -->
<script type="text/javascript">
    function PreviewImage(no) {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("uploadImage"+no).files[0]);
		
		var fileInput = document.getElementById("uploadImage1").files[0];
		var allowed = ["jpeg","png"];
		var found = false;
		var fsize=((((fileInput.size)/1024)/1024)/1024);
		var fsize_mb=(fsize.toFixed(5));
		
		
		allowed.forEach(function(extension) {
                   // alert(fsize_mb);


			if (fileInput.type.match('image/'+extension) && fsize_mb <=2) {
					found = true;
					 oFReader.onload = function (oFREvent) {
					document.getElementById("uploadPreview"+no).src = oFREvent.target.result;
					document.getElementById("clear-message").value=""; //Clear message (please choose image.)
					};
							
			}
				
		})
		if(found==false){
		  document.getElementById("filenotfound").innerHTML="Upload only file(jpeg , png) and less than 2MB";
		  //document.getElementById("uploadPreview1").src = "images/profile.png";
		  document.getElementById("uploadImage1").value="";
		  
		}
		else{
			document.getElementById("filenotfound").innerHTML="";
		}
	
    }
	

</script>