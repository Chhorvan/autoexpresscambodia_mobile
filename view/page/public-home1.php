<?php
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';


//get used vehicle.
$used_vehicle = $_CLASS->getVehicle_used();

//get used vehicle.
$new_vehicle = $_CLASS->getVehicle_new();

//get special offer.
$specialoffer = $_CLASS->specialoffer();

//get show vehicle type on form.

$show_vehicle=$_CLASS->getVehicleType();
?>

<script type="text/javascript">
 function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
</script>


<body id="top" class="page-id-home">
<div id="homeContainer">
	<div class="container clearfix">		
		<div id="homeSidebar">
			<div id="bgHomeSidebar">
				<div id="quickSearch">
					<div id="titleTop">
						<span><?php echo $_LANG['PUBLIC_HOME_QUICK_SEARCH'];?></span>
					</div>

					<div id="divFrm" class="clearfix">

							<table cellspacing="0" cellpadding="0">
								<tbody>
									<tr>
										<td>
											<div class="longStl fa-caret-down">
												<select name="product_type_name" id="product_type">

													<!--<option>Vehicle Type</option>-->
													<option style='text-align:left;' id='reload' value=''><?php echo $_LANG['PUBLIC_HOME_SELECT_TYPE_LABEL'];?></option>
													<?php
														/*include('ajax/load_car_product_type.php');*/
														
														include('ajax/load_vechicle_type.php');
														$rec_vehicle=getvechicle_type();
														foreach($rec_vehicle as $key_type){
															echo"<option>$key_type[vehicle_type]</option>";
														}

													?>
												</select>
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="longStl fa-caret-down">
												<select name="make" id="product_make">
													<option value='' id="clearmake"><?php echo $_LANG['PUBLIC_HOME_ANY_MAKER_OPTION'];?></option>
												</select>
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="longStl fa-caret-down">
												<select name="model" id="product_model">
													<option value=""><?php echo $_LANG['PUBLIC_HOME_ANY_MODEL_OPTION'];?></option>
												</select>
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="shortStl fa-caret-down">
												<select name="min_year" id="year_from">
													<option value=""><?php echo $_LANG['PUBLIC_HOME_MIN_YEAR_LABEL'];?></option>
													<?php $min_year=date('Y'); for ($i=$min_year;$i>=1990;$i--){ ?>
														<option><?php echo $i; ?></option>
													<?php } ?>
												</select>
											</div>
											<!--<span>~</span> -->
											<div class="shortStl fa-caret-down flRight">
												<select name="max_year" id="year_to">
													<option value=""><?php echo $_LANG['PUBLIC_HOME_MAX_YEAR_LABEL'];?></option>
													<?php $max_year=date('Y'); for ($i=$max_year;$i>=1990;$i--){ ?>
														<option><?php echo $i; ?></option>
													<?php } ?>
												</select>
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="shortinput">
												<input type="text" onKeyPress="return isNumberKey(event)" name="txtprice1" id="txtprice1" class="form-control text-box" placeholder="<?php echo $_LANG['PUBLIC_HOME_MIN_PRICE_LABEL'];?>"/>
											</div>
											<!--<span>~</span> -->
											<div class="shortinput leftshort flRight">
												<input type="text" onKeyPress="return isNumberKey(event)" name="txtprice2" id="txtprice2" class="form-control text-box" placeholder="<?php echo $_LANG['PUBLIC_HOME_MAX_PRICE_LABEL'];?>"/>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
							<div id="bntsubmit">
								<input id="submit" type="submit" value="<?php echo $_LANG['PUBLIC_HOME_SEARCH'];?>" class="linkimg" />
							</div>

					</div>
				</div><!-- #quickSearch -->



				<div class="bgBox" >
					<div class="divTitle">
						<span><?php echo $_LANG['PUBLIC_HOME_SPECIAL_DEAL'];?></span>
					</div>


					<div class="wrapper"><!--  wrapper div special deal -->
						<div class="demo1">
							<ul>
								<?php
									$i=1;
									if(count($specialoffer)>0){
									foreach($specialoffer as $row){

								?>
								<li class="clearfix" > <!-- list special deal car -->
									<p class="title-special-deal">
									   <?php
										
										if(!empty($row['make'])){
										   echo " {$row['make']}&nbsp;";
										}
									   
									   if(!empty($row['model'])){
										   echo "&nbsp;{$row['model']} &nbsp;";
										}
									   
										if(!empty($row['model_year'])){
										   $model_year= "{$row['model_year']}";
										   echo $model_year;
										}else{
										   echo $model_year='';
										}
									   
										if(!empty($row['part_name'])){
										   echo "&nbsp;{$row['part_name']}";
										}
									   
									   ?>
									</p>
									<div class="divimg" ><!-- div image -->
										<?php
											if($row['thumb'] == ""){
												echo '<img src="'.BASE_RELATIVE.'img/public_home/No-img-slide.jpg" class="blank" />';

										}else{ ?>
										<a href="<?php echo BASE_RELATIVE;?>vehicle-detail?product_type=<?php echo $row['product_type']."&cid=".				$row['id']."&owner=".$row['comid']; ?>" class="linkfade">
											<?php
					if($row['thumb'] == ""){
						echo '<img style="margin-top:-3px;" src="'.BASE_RELATIVE.'img/public_home/Noimage.jpg" class="blank" />';
					?>
					<?php }else{?>
						<img src="<?php echo $row['thumb']?>" class="linkimg" style=" position:relative"/>
					<?php }?>
                    	<?php if( $row['status']=='Sold' ) { ?>
    <img src="images/sold.png" style="position:absolute; top:98px; left:64px; z-index:999; width:66px; height:66px" />
                         <?php }?>
                         <?php if( $row['status']=='Reserved' ) { ?>
    <img src="images/reserved.png" style="position:absolute; top:98px; left:64px; z-index:999; width:66px; height:66px" />
                         <?php }?>
										</a>

										<?php }?>
                                        
                                        

									</div>

									<div class="divText">										
											<p class="borderTop"><?php echo $_LANG['PUBLIC_HOME_STEERING'];?>
											<span><?php if(!empty($row['steering'])){
										   echo $row['steering'];} ?></span></p>

											<p class="borderTop"><?php echo $_LANG['PUBLIC_HOME_FUEL_TYPE'];?>
											<span><?php if(!empty($row['fuel_type'])){
										   echo $row['fuel_type'];} ?></span></p>

											<p class="borderTop borderBottom"><?php echo $_LANG['PUBLIC_HOME_MILEAGE'];?>
											<span><?php if(!empty($row['mileage'])){
										   echo "Km ". $row['mileage'];} ?></span></p>

											<p class="price">
												<span class="price">
												<?php 
													if(!empty($row['price'])){
										   				echo number_format($row['price'])." $";
													}else{
														echo "Ask for price";
													}
												?>
                                           		</span>
											</p>
									</div>
								</li><!-- list special deal car -->
								<?php } } ?>
							</ul>
						</div>
					</div><!--  wrapper div special deal -->
				</div>

				<div class="bgBox" style="display:none;">
					<div class="divTitle">
						<img src="<?php echo BASE_RELATIVE.'img/public_home/img_01.png'; ?>" />
						<span>Maker</span>
					</div>
				</div>
			</div>
		</div><!-- #homeSidebar -->

		<div id="homeLeftSide" class="clearfix">
			<div class="clearfix">
				<div id="sliderWrapper">
					<div class="slider">
						<!--<img src="img/public_home/slider/dummy.gif" width="830">-->
						<div class="imgSlider">
							<div class="divImgSlider">
								<img src="<?php echo BASE_RELATIVE.'img/public_home/slider/sample_christmas01.jpg'; ?>">
							</div>
							<div class="bgDetails">
								<div class="textTop">
									<p class="register">Register now</p>
									<p class="subText">Join us now to post your products for free</p>
								</div>									
								<div id="bgOder">
									<a href="<?php echo BASE_RELATIVE.'register';?>" >
										<img src="<?php echo BASE_RELATIVE.'img/public_home/slider/bg_register.png';?>" class="linkimg"   />
									</a>
								</div>
							</div>
						</div>
						
						<div class="imgSlider">
							<div class="divImgSlider">
								<img src="<?php echo BASE_RELATIVE.'img/public_home/slider/slider_mazda_1.jpg'; ?>">
							</div>
							<div class="bgDetails">
								<div class="textTop">
									<p>Mazda
									<span>AZ 3 2014</span></p>
								</div>
								<div class="textPrice">
									<p>$39,000</p>
								</div>
								<div class="divText">
									<p>Fully Electric Tesla S-Model Performance coming to our big show room</p>
								</div>
								<div id="bgOder">
									<a href="http://angkorauto.com/vehicle-detail?product_type=Car&cid=BYQ0CH&owner=471" >
										<img src="<?php echo BASE_RELATIVE.'img/public_home/slider/Contact.png';?>" class="linkimg"   />
									</a>
								</div>
							</div>
						</div>

						<div class="imgSlider">
							<div class="divImgSlider">
								<img src="<?php echo BASE_RELATIVE.'img/public_home/slider/slider_lexus.jpg' ?>">
							</div>
							<div class="bgDetails">
								<div class="textTop">
									<p>LEXUS
									<span>LX570 2014</span></p>
								</div>
								<div class="textPrice">
									<p>$120,000</p>
								</div>
								<div class="divText">
									<p>Fully Electric Tesla S-Model Performance coming to our big show room</p>
								</div>
								<div id="bgOder">
									<a href="http://angkorauto.com/vehicle-detail?product_type=Car&cid=YD37JS&owner=461" >
										<img src="<?php echo BASE_RELATIVE.'img/public_home/slider/Contact.png'; ?>"  class="linkimg" />
									</a>
								</div>
							</div>
						</div>

						<div class="imgSlider">
							<div class="divImgSlider">
								<img src="<?php echo BASE_RELATIVE.'img/public_home/slider/slider_mazda_2.jpg'; ?>">
							</div>
							<div class="bgDetails">
								<div class="textTop">
									<p>CHEVROLET
									<span>SPARK 2014</span></p>
								</div>
								<div class="textPrice">
									<p>$32,000</p>
								</div>
								<div class="divText">
									<p>Fully Electric Tesla S-Model Performance coming to our big show room</p>
								</div>
								<div id="bgOder">
									<a href="http://angkorauto.com/vehicle-detail?product_type=Car&cid=381056&owner=2" >
										<img src="<?php echo BASE_RELATIVE.'img/public_home/slider/Contact.png'; ?>" class="linkimg" />
									</a>
								</div>
							</div>
						</div>

						<div class="imgSlider">
							<div class="divImgSlider">
								<img src="<?php echo BASE_RELATIVE.'img/public_home/slider/sample_car02.jpg';?>">
							</div>
							<div class="bgDetails">
								<div class="textTop">
									<p>VOLVO
									<span>S60 Polestar</span></p>
								</div>
								<div class="textPrice">
									<p>$89,000</p>
								</div>
								<div class="divText">
									<p>Fully Electric Tesla S-Model Performance coming to our big show room</p>
								</div>
								<div id="bgOder">
									<a href="#" >
										<img src="<?php echo BASE_RELATIVE.'img/public_home/slider/bg_order.png'; ?>" class="linkimg" />
									</a>
								</div>
							</div>
						</div>

					</div>

					<div id="slidePaging"></div>
				</div>

				<div id="advertising">
					<img src="<?php echo BASE_RELATIVE.'img/public_home/banner.png'; ?>" />
				</div>
			</div><!-- End Slide -->

			<div id="divAdv">
				<img src="<?php echo BASE_RELATIVE.'img/public_home/banner_auto.jpg'; ?>" />
			</div>

			<!-- Respon tab menu -->
			<div id="responTab">
				<div id="sltTab" class="col-lg-12 fa-caret-down">
					<select name="">
						<option>Cars</option>
						<option>Truck</option>
						<option>sample</option>
						<option>sample</option>
						<option>sample</option>
					</select>
				</div>
			</div>
			<!-- End respon tab menu -->


			<div id="newRecent">
				<div id="divTop" class="clearfix">
					<div id="lastUsed" class="bgCurrent">
						<p class="title"><?php echo $_LANG['PUBLIC_HOME_USED_CAR'];?></p>
					</div>
					<div id="lastNew">
						<p class="title"><?php echo $_LANG['PUBLIC_HOME_NEW_CAR'];?></p>
					</div>
				</div>

				<ul id="showUsed" class="clearfix"><!--start used car list-->
					<?php

					$i=1;
					if(count($used_vehicle)>0){
					foreach($used_vehicle as $used){

				?>
					<li>

			<a href="<?php echo BASE_RELATIVE;?>vehicle-detail?product_type=<?php echo $used['product_type']."&cid=".				$used['id']."&owner=".$used['comid']; ?>" class="linkfade">
				
				<?php
					if($used['thumb'] == ""){
						echo '<img style="margin-top:-3px;" src="'.BASE_RELATIVE.'img/public_home/Noimage.jpg" class="blank" />';
					?>
					<?php }else{?>
						<img src="<?php echo $used['thumb']?>" class="linkimg" style="width:198px; position:relative"/>
					<?php }?>
                    	<?php if( $used['status']=='Sold' ) { ?>
    <img src="images/sold.png" style="position:absolute; top:64px; left:132px; z-index:999; width:66px; height:66px" />
                         <?php }?>
                         <?php if( $used['status']=='Reserved' ) { ?>
    <img src="images/reserved.png" style="position:absolute; top:64px; left:132px; z-index:999; width:66px; height:66px" />
                         <?php }?>
			</a>

						<div class="divInfo">
							<p class="details">
								<?php 
									/*echo '
										'.$used['make'].' '.$used['model'].' <br/> '.$used['model_year'].'
									';
									*/
									if($used['model_year']=="0"){
									  $year='';
									  $aligment_used="<br/>";
									}
									else{
									  $year=$used['model_year'];
									   $aligment_used="";
									}
									echo $used['make']." ". $aligment_used.$used['model']."<br/>".$year;
								?>
							</p>

											<p class="borderTop"><?php echo $_LANG['PUBLIC_HOME_STEERING'];?>
											<span><?php if(!empty($used['steering'])){
										   echo $used['steering'];} ?></span></p>

											<p class="borderTop"><?php echo $_LANG['PUBLIC_HOME_FUEL_TYPE'];?>
											<span><?php if(!empty($used['fuel_type'])){
										   echo $used['fuel_type'];} ?></span></p>

											<p class="borderTop"><?php echo $_LANG['PUBLIC_HOME_MILEAGE'];?>
											<span><?php if(!empty($used['mileage'])){
										   echo "Km ". $used['mileage'];} ?></span></p>

											<p class="prices">
												<span class="price">
												<?php 
													if(!empty($used['price'])){
										  				echo number_format($used['price'])." $";
													}else{
														echo "Ask for price";
													}
												?>
                                           		</span>
											</p>

						</div>
					</li>
					<?php } } ?>
				</ul><!--end used car list-->


				<ul id="showNew" class="clearfix"><!--start new car list-->
					<?php

					$i=1;
					if(count($new_vehicle)>0){
					foreach($new_vehicle as $new){

				?>
					<li>

			<a href="<?php echo BASE_RELATIVE;?>vehicle-detail?product_type=<?php echo $new['product_type']."&cid=".				$new['id']."&owner=".$new['comid']; ?>" class="linkfade">

            <?php
					if($new['thumb'] == ""){
						echo '<img style="margin-top:-3px;" src="'.BASE_RELATIVE.'img/public_home/Noimage.jpg" class="blank" />';
					?>
					<?php }else{?>
						<img src="<?php echo $new['thumb']?>" class="linkimg" style="width:198px; position:relative"/>
					<?php }?>
                    	<?php if( $new['status']=='Sold' ) { ?>
    <img src="images/sold.png" style="position:absolute; top:64px; left:132px; z-index:999; width:66px; height:66px" />
                         <?php }?>
                         <?php if( $new['status']=='Reserved' ) { ?>
    <img src="images/reserved.png" style="position:absolute; top:64px; left:132px; z-index:999; width:66px; height:66px" />
                         <?php }?>
            </a>
				
						<div class="divInfo">
							<p class="details">
								<?php 
									/*echo '
										'.$new['make'].' '.$new['model'].' <br/> '.$new['model_year'].'
									';
									*/
									
									
									if($new['model_year']=="0"){
									  $year='';
									  $aligment_new="<br/>";
									}
									else{
									  $year=$new['model_year'];
									  $aligment_new="";
									}
									echo $new['make']." ".$aligment_new.$new['model']."<br/>".$year;
									
									
								?>
							</p>
							<p class="borderTop"><?php echo $_LANG['PUBLIC_HOME_STEERING'];?>
								<span><?php if(!empty($new['steering'])){
								echo $new['steering'];} ?></span></p>
							<p class="borderTop"><?php echo $_LANG['PUBLIC_HOME_FUEL_TYPE'];?>
								<span><?php if(!empty($new['fuel_type'])){
								echo $new['fuel_type'];} ?></span></p>
							<p class="borderTop"><?php echo $_LANG['PUBLIC_HOME_MILEAGE'];?>
								<span><?php if(!empty($new['mileage'])){
								echo "Km ". $new['mileage'];} ?></span></p>
							<p class="prices">
								<span class="price">
									<?php if(!empty($new['price'])){
											echo number_format($new['price'])." $";
										  }
										  else{
										     echo"Ask for Price";
										  }
									?>
								</span></p>
						</div>

					</li>
					<?php } } ?>
				</ul><!--end new car list-->


			</div><!-- #newRecent -->
		</div><!-- #homeLeftSide -->    	
    </div><!-- .container -->


</div><!-- #homeContainer -->

<div id="popularBrand" class="clearfix">
	
	<div id="divTop" class="clearfix">
						<div id="most-popular-brand" class="bgCurrent">
							<p class="title">MOST POPULAR BRANDS</p>
						</div>
						<div id="most-popular-seller">
							<p class="title">MOST POPULAR SELLERS</p>
						</div>
						<div id="most-popular-products">
							<p class="title">MOST POPULAR PRODUCTS</p>
						</div>
	</div>
	
	
</div>
<div id="brands" class="row">
	<div style="float:left; margin-top:10px;">
		<a href="#"><img class="img-responsive imgCenter"  src="<?php echo BASE_RELATIVE.'img/public_home/pre.jpg'; ?>" /></a>
	</div>
	<ul >
	    
		<li><img class="img-responsive imgCenter" src="<?php echo BASE_RELATIVE.'img/public_home/brands/hyundai.png'; ?>" /></li>
		<li><img class="img-responsive imgCenter" src="<?php echo BASE_RELATIVE.'img/public_home/brands/bmw.png'; ?>" /></li>
		<li><img class="img-responsive imgCenter" src="<?php echo BASE_RELATIVE.'img/public_home/brands/audi.png'; ?>" /></li>
		<li><img class="img-responsive imgCenter" src="<?php echo BASE_RELATIVE.'img/public_home/brands/mercedes_benz.png'; ?>" /></li>
		<!--<li><img class="img-responsive imgCenter" src="<?php echo BASE_RELATIVE.'img/public_home/brands/volvo.png'; ?>" /></li>-->
		<li><img class="img-responsive imgCenter" src="<?php echo BASE_RELATIVE.'img/public_home/brands/peugeot.png'; ?>" /></li>
		<li><img class="img-responsive imgCenter" src="<?php echo BASE_RELATIVE.'img/public_home/brands/brand_01.png'; ?>" /></li>
		<li><img class="img-responsive imgCenter" src="<?php echo BASE_RELATIVE.'img/public_home/brands/ferrari.png'; ?>" /></li>
		
	</ul>
	<div style="float:left;margin-top:10px;">
		<a href="#"><img class="img-responsive imgCenter" src="<?php echo BASE_RELATIVE.'img/public_home/next.jpg'; ?>" /></a>
	</div>	
</div>

<!---Customize jquery---->
<script src="<?php echo BASE_RELATIVE.'autocam_js/public_home/jquery.cycle.all.js'; ?>"></script>
<script src="<?php echo BASE_RELATIVE.'autocam_js/public_home/index.js'; ?>"></script>
<script type="text/javascript">
	$(document).ready(function() {
		var product_type = "";
		$('#product_type').change(function(e) {
				product_type=$(this).val();	
				product_type=product_type.split(' ').join('+');	
				if(product_type=$('#product_type option:selected').text()){
                $('#product_make').load('ajax/load_car_make.php?product_type='+encodeURIComponent(product_type));
				}else if(product_type=$('#product_type option:eq(0)').text()){

					$('#product_make').reload();
					$('#product_model').reload();

				}

		});
		$('#product_make').change(function(e) {
				product_type=product_type.split(' ').join('+');	
				var make = $(this).val();
				make = '&make='+make.split(' ').join('+');								
				$('#product_model').load('ajax/load_car_model.php?vehicle_type='+product_type+make);
				if(make=$(this).click()){
					$('#product_model').reload();
				}
        });
	});

</script>

<!-- Script For image Scrollbar-->
<script src="<?php echo BASE_RELATIVE.'autocam_js/public_home/jquery.easy-ticker.js'?>"></script>
<script type="text/javascript">
	$(function(){
		$('.demo1').easyTicker({
			direction: 'up',
			easing: 'swing',
			speed: 'slow',
			interval: 4000,
			height: 890,
			visible: 4
		});
	});

    $('#submit').click(function(event) {

        var vehicle_type=$('#product_type option:selected').val();

        if(vehicle_type!=''){

            vehicle_type="&vehicle_type="+vehicle_type.split(' ').join('+');
        }
        var make = $('#product_make option:selected').val();
         if(make!=''){
            make="&make="+make;
        }
         var model = $('#product_model option:selected').val();
         if(model!=''){
            model="&model="+model;
        }

        var year_from = $('#year_from option:selected').val();
         if(year_from!=''){
            year_from="&year_from="+year_from;
        }
        var year_to = $('#year_to option:selected').val();
         if(year_to!=''){
            year_to="&year_from="+year_to;
        }

        var price_from = $('#txtprice1').val();
         if(price_from!=''){
            price_from="&price_from="+price_from;
        }
         var price_to = $('#txtprice2').val();
         if(price_to!=''){
            price_to="&price_to="+price_to;
        }
        window.location.href="vehicle?result=1"+vehicle_type+make+model+year_from+year_to+price_from+price_to;

    });


</script>
