<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';
/*
 * VALIDATE ADMINISTRATOR GROUP ------------------------------------------------
 */
if( $_SESSION['log_group'] == 'admin' )
{
    // get form feed.
    $fstatus = $_CLASS->getFormStatus();
    $fmessage = $_CLASS->getFormMessage();
    $fstyle = $_CLASS->getFormStyle();
    
    // get vehicle data
    $car = $_CLASS->getVehicle();
    
    // get all feature list.
    $all_list = $_CLASS->getAllFeaturedList();
    
    // get feature list.
    $list = $_CLASS->getFeatureList();
?>
        <div id="content-wrapper">
            <div id="vertical-menu">
                <?php $_MENU->loadMenu($_GLANG); ?>
            </div>
            <div id="inner-content">
                <!-- START CONTENT -->
                <h1><?php echo $_LANG['EDIT_FEAT_HEADER'];?></h1>
                <?php
                    if( $fstatus ){
                ?>
                <div class="alert <?php echo $fstyle;?>">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <?php echo $fmessage;?>
                </div>
                <?php
                    }
                    
                    // display list.
                    if( count($car) > 0 ){
                ?>
                <h4><?php echo $car['maker'] . ' ' . $car['model'] . ' ' . $car['year'];?></h4>
                <form method="post" action="?cid=<?php echo $_GET['cid'];?>">
                <?php
                        // display all features list and mark selected.
                        for( $i=0; $i < count($all_list); $i++ ){
                            $label = $all_list[$i]['label'];
                            
                            if(strlen($label) > 10){
                                $label = substr($label,0,7) . '...';
                            }
                            
                            $checked = '';
                            
                            for( $u=0; $u < count($list); $u++ ){
                                if( $list[$u]['feat_name'] == $all_list[$i]['label'] ){
                                    $checked = 'checked="checked"';
                                    break;
                                }
                            }
                            
                ?>
                <div class="item-wrap">
                    <label>
                        <input class="ckb" type="checkbox" <?php echo $checked;?> name="featureInput[]" value="<?php echo base64_encode($all_list[$i]['label'].':'.$all_list[$i]['source']);?>"/>
                        <img src="<?php echo BASE_RELATIVE . $all_list[$i]['source'];?>" alt="<?php echo $all_list[$i]['label'];?>" title="<?php echo $all_list[$i]['label'];?>" width="50" height="50" class="img-item" />
                        <span class="title-item"><?php echo $label;?></span>
                    </label>
                </div>
                <?php
                        }
                ?>
                    <div class="clearfix"></div>
                    <br>
                    <input type="submit" name="savebtn" class="btn btn-small btn-success" value="<?php echo $_LANG['EDIT_FEAT_UPDATE_BUTTON'];?>" />
                </form>
                <?php
                    }
                ?>
                <!-- END CONTENT -->
            </div>
            <div class="clearfix"></div>
        </div>
<?php
} // end validation.
else
{
?>
        <p class="text-warning"><?php echo $_LOCAL['UNAUTHORIZED_ACCESS_MESSAGE'];?></p>
<?php
}