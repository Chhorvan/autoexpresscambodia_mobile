<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Page Sign In</title>
<meta content="Car Stock, auto-cambodia.com, used car, T7, spare parts, used bus, used truck, used motorbike" name="keywords">
	<meta content="Auto Cambodia is the most largest second hand vehicle & Brand-new Spare parts sales company in Cambodia, who provides Hyundai, Kia, Daewoo, Ssangyong and Renault Samsung vehicles, and Top Quality Auto Parts to the Cambodia Market." name="description">
	<meta property="og:title" content="Auto Cambodia - Used Cars & Spare Parts Company">
	<meta property="og:description" content="Auto Cambodia is the most largest second hand vehicle & Brand-new Spare parts sales company in Cambodia, who provides Hyundai, Kia, Daewoo, Ssangyong and Renault Samsung vehicles, and Top Quality Auto Parts to the Cambodia Market.">
	<meta property="og:url" content="http://www.auto-cambodia.com/">
	<meta property="og:image" content="http://www.auto-cambodia.com/ogp.jpg">
	<meta property="og:type" content="website">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">		
	<link rel="stylesheet" href="<?php echo base_url('asset/css/import.css'); ?>">
    
    <link rel="stylesheet" href="<?php echo base_url('css/public_sign_in/style.css'); ?>">		
	
	<script src="<?php echo base_url('asset/js/jquery-1.8.3.js'); ?>"></script>
	<script src="<?php echo base_url('asset/js/jquery.easing.1.3.js'); ?>"></script>
	<script src="<?php echo base_url('asset/js/bootstrap.min.js'); ?>"></script>
	<script src="<?php echo base_url('asset/js/jquery.lazyload.js'); ?>"></script>
    
    <!---jquery popup libary--->
	<script src="<?php echo base_url('asset/js/fancybox/jquery.fancybox-1.3.4.pack.js'); ?>"></script>
    <link rel="stylesheet"  type="text/css" media="screen"  href="<?php echo base_url('asset/js/fancybox/jquery.fancybox-1.3.4.css'); ?>">
    
	<script src="<?php echo base_url('asset/js/custome.js'); ?>"></script>

	
</head>

<body>
<div id="supper-container">

   <div class="container">
	<div id="main">
		<div class="row">
			 <div id="label-sign-in">
			    <p>
				  Sign in
				</p>
			 </div>
		</div>
		
		<div class="row">
		  <p id="form-introduction">
		    Lorem ipsum dolor sit amet, conse ctetur Aenean pulvinar ligula eget quam
dolor sit amet, conse ctetur adipiscing elit.
		  </p>
		</div>
   	 
	 
		<div class="row">
		
		<div id="form-wrapper">
		
		<?php if (isset($error) && $error): ?>
			<div class="alert alert-error">
				<a class="close" data-dismiss="alert" href="#">×</a>Incorrect Username or Password!
			</div>
        <?php endif; ?>
		
        <?php echo form_open('public_sign_in/login_validation', array('id'=>'loginForm')); echo validation_errors();?>
		<?php
			echo validation_errors();
		?>
		   <table >
		    <tbody>
              <tr>
				<td><label>Email</label></td>
				<td><input type="text" name="txtemail" class="form-control" id="txt-box-email"/></td>
			  </tr>	
			  <tr>
				<td><label>Password</label></td>
				<td><input type="password" name="txtpassword" class="form-control" id="txt-box-password"/></td>
			  </tr>
			  <tr>
			    <td colspan="2"><input type="submit" value="Sign in" name="cmdsignin" class="btn btn-primary btn-sm" id="button-signin"/></td>
			  </tr>
			  <tr>
			    
			    <td colspan="2">
				   <label id="decorate-text-link-password"><a href="#" >Forget your password?</a></label> <br/>
				   <label>Not a member yet? <a href="#">Sign up</a></label>
				</td>
			  </tr>
			</tbody>
		   </table>
		<?php echo form_close();?><!-- close form-->	
		
			
		
		</div> <!-- #form-wrapper-->
        </div>
		
		
		
   </div><!--#main--->
   </div><!--.container-->
<div><!-- supper-container -->
</body>
</html>
