<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */

$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';
?>
        <div id="content-wrapper">
            <div id="vertical-menu">
                
            </div>
            <div id="inner-content">
                <!-- START CONTENT -->
                <h1 class="header_error"><?php echo $_LANG['p404_HEADER'];?></h1>
                <p><?php echo $_LANG['p404_LABEL'];?></p>
                <div id="head404"></div>
                <!-- END CONTENT -->
            </div>
            <div class="clearfix"></div>
        </div>