<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';

// get form feed.
$fstatus = $_CLASS->getFormStatus();
$fmessage = $_CLASS->getFormMessage();
$fstyle = $_CLASS->getFormStyle();

// car list.
$car_list = $_CLASS->getCarList();
// getCarMedia($car_id);
// getCarFeatures($car_id);
// pagination list.
$pagination = $_CLASS->getPaginationList();
            
// [2013.07.27] initialize grid session.
if( !isset($_SESSION['grid_type']) ){
    $_SESSION['grid_type'] = 'list';
}

// define selected grid type.
$glarge = '';
$gsmall = '';

require_once BASE_CLASS . 'class-utilities.php';
?>
        <div id="content-wrapper">
            <div id="vertical-menu">
                <?php $_MENU->loadMenu($_GLANG); ?>
                <!-- display maker logo -->
                <?php
                    // display maker logo if at least 1 entry is displayed.
                    if( count($car_list) > 0 ){
                        // get list of all makers.
                        $maker_list = $_CLASS->getMakerList();
                        $maker_logo_list = $_CLASS->getMakerLogoList();
                                                
                        if( count($maker_list) > 0 ){                        
                ?>
                <div id="maker-wrap">
                    <?php
                        // get matching makers.
                        for( $i=0; $i < count($maker_list); $i++ ){
                            $mk =  strtolower($maker_list[$i]);
                            
                            if( is_array($maker_logo_list) ){
                                foreach( $maker_logo_list as $k => $v ){
                                    if( $mk == $v['maker'] ){
                                        $q = BASE_RELATIVE . DEFAULT_HOME_SLUG . '/search/?q=' . base64_encode(ucfirst($mk) . '{_:_}all{_:_}all');
                    ?>
                    <a href="<?php echo $q;?>" class="logo-btn-wrap" style="background-image:url(<?php echo $v['src'];?>);"></a>
                    <?php
                                    }
                                }
                            }
                        }
                        
                        
                    ?>
                    <div class="clear"></div>
                </div>
                <?php
                        }
                    }
                ?>
                <!-- end display maker logo -->
            </div>
            <div id="top-page" style="z-index:9999;bottom:0px;" onclick="javascript: $('html, body').animate({scrollTop: '0px'}, 300);"></div>
            <div id="inner-content">
                <!-- START CONTENT -->
                <h1><?php echo $_LANG['SEARCH_HEADER'];?></h1>
                <?php
                    if( $fstatus && isset($ftype) && $ftype == 'top' ){
                ?>
                <div class="alert <?php echo $fstyle;?>">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <?php echo $fmessage;?>
                </div>
                <?php
                    }
                    
                    // only display order controls if there is at least 1 result. 
                    if( count($car_list) > 0 ){
                        // set order attribute
                        ( isset($_GET['o']) ? $ordered_by = trim($_GET['o']) : $ordered_by = 'featured' );
                        
                        switch($ordered_by){
                            case 'maker':
                                $ordered_name = $_LANG['SEARCH_MAKER_LABEL'];
                                break;
                            case 'model':
                                $ordered_name = $_LANG['SEARCH_MODEL_LABEL'];
                                break;
                            case 'body_type':
                                $ordered_name = $_LANG['SEARCH_BODY_TYPE_LABEL'];
                                break;
                            case 'price':
                                $ordered_name = $_LANG['SEARCH_PRICE_LABEL'];
                                break;
                            case 'year':
                                $ordered_name = $_LANG['SEARCH_YEAR_LABEL'];
                                break;
                            case 'miles':
                                $ordered_name = $_LANG['SEARCH_KILOMETER_LABEL'] . '/' . $_LANG['SEARCH_MILES_LABEL'];
                                break;
                            case 'door':
                                $ordered_name = $_LANG['SEARCH_DOORS_LABEL'];
                                break;
                            case 'featured':
                                $ordered_name = $_LANG['SEARCH_FEATURED_LABEL'];
                                break;
                        }
                        
                        // set order type.
                        ( isset($_GET['ot']) ? $order_type = trim($_GET['ot']) : $order_type = 'a' );
                        
                        switch($order_type){
                            case 'a':
                                $order_type_name = $_LANG['SEARCH_ORDER_ASC_LABEL'];
                                break;
                            case 'd':
                                $order_type_name = $_LANG['SEARCH_ORDER_DESC_LABEL'];
                                break;
                            default:
                                $order_type = 'a';
                        }
                        
                        // set results per page.
                        ( isset($_GET['r']) ? $rrp = (int)$_GET['r'] : $rrp = 20 );
                        
                        if( $rrp < 20 ){
                            $rrp = 20;
                        }
                        
                        // set displaying page.
                        ( isset($_GET['pg']) ? $pageno = (int)$_GET['pg'] : $pageno = 1 );
                        
                        if( $pageno < 1 ){
                            $pageno = 1;
                        }
                        
                        // set passthrough vars.
                        $maker = '';
                        $body  = '';
                        $qr    = '';
                        
                        if( isset($_GET['maker']) ){
                            $maker = trim($_GET['maker']);
                            $maker = str_replace('%20', ' ', $maker);
                            $model = '';
                            $body  = '';
                        }
                        if( isset($_GET['body']) ){
                            $body = trim($_GET['body']);
                            $body = str_replace('%20', ' ', $body);
                            $maker = '';
                            $model = '';
                        }
                        if( isset($_GET['q']) ){
                            $qr = trim($_GET['q']);
                        }
                ?>
                <h4><?php echo $_LANG['SEARCH_RESULT_CONTROL_HEADER'];?></h4>
                <form method="post" id="toolbarform">
                    <div class="input-append input-prepend" style="margin-left:1px;">
                        <span class="add-on" style="width:80px;"><?php echo $_LANG['SEARCH_ORDER_BY_LABEL'];?></span>
                        <select id="orderInput" style="width:140px;">
                            <optgroup label="<?php echo $_LANG['SEARCH_OPTION_SELECTED'];?>">
                                <option value="<?php echo $ordered_by;?>"><?php echo $ordered_name;?></option>
                            </optgroup>
                            <optgroup label="<?php echo $_LANG['SEARCH_OPTION_OPTIONS'];?>">
                                <option value="maker"><?php echo $_LANG['SEARCH_MAKER_LABEL'];?></option>
                                <option value="model"><?php echo $_LANG['SEARCH_MODEL_LABEL'];?></option>
                                <option value="body_type"><?php echo $_LANG['SEARCH_BODY_TYPE_LABEL'];?></option>
                                <option value="price"><?php echo $_LANG['SEARCH_PRICE_LABEL'];?></option>
                                <option value="year"><?php echo $_LANG['SEARCH_YEAR_LABEL'];?></option>
                                <option value="miles"><?php echo $_LANG['SEARCH_KILOMETER_LABEL'] . '/' . $_LANG['SEARCH_MILES_LABEL'];?></option>
                                <option value="doors"><?php echo $_LANG['SEARCH_DOORS_LABEL'];?></option>
                                <option value="featured"><?php echo $_LANG['SEARCH_FEATURED_LABEL'];?></option>
                            </optgroup>
                        </select>
                        <select id="orderTypeInput" style="width:100px;margin-left:-1px;">
                            <optgroup label="<?php echo $_LANG['SEARCH_OPTION_SELECTED'];?>">
                                <option value="<?php echo $order_type;?>"><?php echo $order_type_name;?></option>
                            </optgroup>
                            <optgroup label="<?php echo $_LANG['SEARCH_OPTION_OPTIONS'];?>">
                                <option value="a"><?php echo $_LANG['SEARCH_ORDER_ASC_LABEL'];?></option>
                                <option value="d"><?php echo $_LANG['SEARCH_ORDER_DESC_LABEL'];?></option>
                            </optgroup>
                        </select>
                        
                        <span class="add-on" style="width:130px;"><?php echo $_LANG['SEARCH_RESULTS_PER_PAGE_LABEL'];?></span>
                        <select id="resultPerPageInput" style="width:65px;">
                            <optgroup label="<?php echo $_LANG['SEARCH_OPTION_SELECTED'];?>">
                                <option value="<?php echo $rrp;?>"><?php echo $rrp;?></option>
                            </optgroup>
                            <optgroup label=" &nbsp; <?php echo $_LANG['SEARCH_OPTION_OPTIONS'];?> &nbsp; ">
                                <option value="10">10</option>
                                <option value="15">15</option>
                                <option value="20">20</option>
                                <option value="25">25</option>
                                <option value="30">30</option>
                                <option value="35">35</option>
                                <option value="40">40</option>
                            </optgroup>
                        </select>
                        
                        <span class="add-on" style="width:65px;"><?php echo $_LANG['SEARCH_DISPLAYING_PAGE_LABEL'];?></span>
                        <select id="pageInput" style="width:65px;">
                            <optgroup label="<?php echo $_LANG['SEARCH_OPTION_SELECTED'];?>">
                                <option value="<?php echo $pageno;?>"><?php echo $pageno;?></option>
                            </optgroup>
                            <optgroup label=" &nbsp; <?php echo $_LANG['SEARCH_OPTION_OPTIONS'];?> &nbsp; ">
                                <?php
                                    for( $i=0; $i < count($pagination); $i++ ){
                                ?>
                                <option value="<?php echo $pagination[$i];?>"><?php echo $pagination[$i];?></option>
                                <?php
                                    }
                                ?>
                            </optgroup>
                        </select>
                        
                        <input type="hidden" id="makerInput" value="<?php echo $maker;?>" />
                        <input type="hidden" id="bodyInput" value="<?php echo $body;?>" />
                        <input type="hidden" id="queryInput" value="<?php echo $qr;?>" />
                        <input type="submit" name="gobtn" onclick="processSearchForm(); return false;" class="btn" style="width:56px;" value="<?php echo $_LANG['SEARCH_CONTROL_BUTTON_LABEL'];?>" />
                    </div>
                </form>
                <?php
                    }
                 ?>
                
                <!-- display grid tools -->
                <h4 id="grid-header"><?php echo $_CLASS->getTotalResults();?> <?php echo $_LANG['SEARCH_TOTAL_RESULTS_LABEL'];?></h4>
                <div id="grid-wrap">
                    <a id="glarge" onclick="displayGrid('list','<?php echo $_SESSION['grid_type'];?>'); return false;" class="<?php echo $glarge;?>" href="#"></a>
                    <a id="gsmall" onclick="displayGrid('thumb','<?php echo $_SESSION['grid_type'];?>'); return false;" class="<?php echo $gsmall;?>" href="#"></a>
                </div>
                <div class="clearfix"></div>
                <script language="javascript" type="text/javascript">
                    current_dir = '<?php echo BASE_RELATIVE;?>';
                    var gtype = '<?php echo $_SESSION['grid_type'];?>';
                    
                    // set selected grid type icon to grid toolbar.
                    if( gtype == 'list' ){
                        document.getElementById('glarge').style.background = 'url(<?php echo BASE_RELATIVE;?>image/glarge.png) no-repeat right';
                    } else {
                        document.getElementById('gsmall').style.background = 'url(<?php echo BASE_RELATIVE;?>image/gsmall.png) no-repeat right';
                    }
                    
                    if( gtype == 'list' ){
                        $(document).ready(function(){doGridList();});
                    } else {
                        $(document).ready(function(){doGridThumb();});
                    }
                </script>
                <!-- end grid tools -->
                
                <!-- display car list -->
                <div id="car-list-wrapper">
                    <?php
                        for( $i=0; $i < count($car_list); $i++ ){                            
                            // define images.
                            $img_arr = $_CLASS->getCarMedia($car_list[$i]['id']);
                            
                            // initialize image/video arrays.
                            $interior_arr = array();
                            $exterior_arr = array();
                            $video_arr = array();
                            
                            if( count($img_arr) > 0 ){
                        
                            // define featured image.
                            $featured = '';
                            
                            if( $car_list[$i]['featured'] ){
                                $featured = '<img src="'.BASE_RELATIVE.'image/featured.png" width="64" height="64" alt="Featured" style="position:absolute;left:0;top:0;" />';
                            }
                                
                                for( $a=0; $a < count($img_arr); $a++ ){
                                    // handle images.
                                    if( $img_arr[$a]['type'] == 'image' ){
                                        
                                        if($img_arr[$a]['mode'] == 'interior') {
                                            array_push($interior_arr, $img_arr[$a]['source']);
                                        }
                                        else {
                                            array_push($exterior_arr, $img_arr[$a]['source']);
                                        }
                                        
                                        // $main_img = BASE_RELATIVE . $img_arr[$a]['source']; // this gets the last uploaded image.
                                        $main_img = BASE_RELATIVE . $img_arr[0]['source'];     // and this gets the very first image. updated at July 9th 2013.
                                    }
                                    // handle video.
                                    else {
                                        array_push($video_arr, $img_arr[$a]['source']);
                                    }
                                }
                            }
                            else {
                                array_push($interior_arr, 'image/default_main_image.jpg');
                                array_push($exterior_arr, 'image/default_main_image.jpg');
                                $main_img = BASE_RELATIVE . 'image/default_main_image.jpg';
                            }
                            
                            // update 2013.05.28: add eco icon.
                            $ico = BASE_RELATIVE . 'image/eco_' . strtolower($car_list[$i]['eco']) . '.png';
                            
                            // build a fake slug containing the vehicle's name, model and year
                            // for SEO purposes (update:2013.06.12)
                            $car_slug = Utilities::genetateSlug($car_list[$i]['maker'] . ' ' .$car_list[$i]['model'] . ' ' .$_CLASS->getBodyTypeByID($car_list[$i]['body_type']) . ' ' .$car_list[$i]['year']);
                    ?>
                    <img class="ecoIcon" src="<?php echo $ico;?>" width="32" height="24" alt="Energy class <?php echo $car_list[$i]['eco'];?>" title="Energy class <?php echo $car_list[$i]['eco'];?>" />
                    <div class="car-item-wrapper">
                        <a id="mainImgAnchor<?php echo $i;?>" class="main-image" href="<?php echo $main_img;?>" rel="prettyPhoto" style="position:relative;"><img id="mainImg<?php echo $i;?>" class="img-polaroid img-item" src="<?php echo $main_img;?>" width="300" height="200" alt="<?php echo $car_list[$i]['maker'];?> <?php echo $car_list[$i]['model'];?>" /><?php echo ( isset($featured) ? $featured : '');?></a>
                        <h4>
                            <span class="label label-success" style="float:right;"><?php echo CURRENCY_SYMBOL;?> <?php echo Utilities::formatPrice($car_list[$i]['price'],CURRENCY_CODE);?></span>
                            <a href="<?php echo BASE_RELATIVE . $car_slug;?>/car-detail/?cid=<?php echo ( isset($car_list[$i]['id']) ? $car_list[$i]['id'] : '');?>&o=<?php echo ( isset($_GET['o']) ? trim($_GET['o']) : '');?>&ot=<?php echo ( isset($_GET['ot']) ? trim($_GET['ot']) : '');?>&r=<?php echo ( isset($_GET['r']) ? trim($_GET['r']) : '');?>&pg=<?php echo ( isset($_GET['pg']) ? trim($_GET['pg']) : '1' );?>" style="color:#666;text-decoration:none;"><?php echo $car_list[$i]['maker'];?> <?php echo $car_list[$i]['model'];?> <?php echo $_CLASS->getBodyTypeByID($car_list[$i]['body_type']);?> <?php echo $car_list[$i]['year'];?></a>
                        </h4>
                        <div class="item-gallery">
                            <ul class="nav nav-tabs" id="imgTab<?php echo $i;?>">
                                <?php
                                    // hide interior tab if nothing's there to be displayed (updated: July 9th 2013)
                                    if( count($interior_arr) > 1 && $interior_arr[0] != 'image/default_main_image.jpg' ){
                                ?>
                                <li class="active"><a href="#interior<?php echo $i;?>"><?php echo $_LANG['SEARCH_INTERIOR_LABEL'];?></a></li>
                                <?php
                                    }
                                ?>
                                <li><a href="#exterior<?php echo $i;?>"><?php echo $_LANG['SEARCH_EXTERIOR_LABEL'];?></a></li>
                                <?php
                                    // hide video tab if no entries are found.
                                    if( count($video_arr) > 0 ){
                                ?>
                                <li><a href="#video<?php echo $i;?>"><?php echo $_LANG['SEARCH_VIDEO_LABEL'];?></a></li>
                                <?php
                                    }
                                ?>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="interior<?php echo $i;?>">
                                    <?php
                                        // display interior images.
                                        if( count($interior_arr) < 1 ){
                                    ?>
                                    <a href="#" onclick="displayMainImage('mainImg','<?php echo BASE_RELATIVE . $interior_arr[$k];?>','mainImgAnchor'); return false;">
                                        <img src="<?php echo BASE_RELATIVE . 'image/default_main_image.jpg';?>" width="74" height="50" alt="<?php if( isset($car['maker']) ){ echo $car['maker']; };?> <?php if( isset($car['model']) ){ echo $car['model']; };?>" class="img-thumb-item img-polaroid" />
                                    </a>
                                    <?php
                                        } else {
                                            $max_interior = count($interior_arr);
                                            
                                            // limit to max 8 images.
                                            if( $max_interior > 8 ){
                                                $max_interior = 8;
                                            }
                                            
                                            for( $k=0; $k < $max_interior; $k++ ){
                                    ?>
                                    <a href="#" onclick="displayMainImage('mainImg<?php echo $i;?>','<?php echo BASE_RELATIVE . $interior_arr[$k];?>','mainImgAnchor<?php echo $i;?>'); return false;">
                                        <img src="<?php echo BASE_RELATIVE . $interior_arr[$k];?>" width="74" height="50" alt="<?php echo $car_list[$i]['maker'];?> <?php echo $car_list[$i]['model'];?>" class="img-thumb-item img-polaroid" />
                                    </a>
                                    <?php
                                            }
                                        }
                                    ?>
                                </div>
                                <div class="tab-pane" id="exterior<?php echo $i;?>">
                                    <?php
                                        // display exterior images.
                                        if( count($exterior_arr) < 1 ){
                                    ?>
                                    <a href="#" onclick="displayMainImage('mainImg','<?php echo BASE_RELATIVE . $interior_arr[$k];?>','mainImgAnchor'); return false;">
                                        <img src="<?php echo BASE_RELATIVE . 'image/default_main_image.jpg';?>" width="74" height="50" alt="<?php echo $car['maker'];?> <?php echo $car['model'];?>" class="img-thumb-item img-polaroid" />
                                    </a>
                                    <?php
                                        } else {
                                            $max_exterior = count($exterior_arr);
                                            
                                            if( $max_exterior > 8 ){
                                                $max_exterior = 8;
                                            }
                                            
                                            for( $k=0; $k < $max_exterior; $k++ ){
                                    ?>
                                    <a href="#" onclick="displayMainImage('mainImg<?php echo $i;?>','<?php echo BASE_RELATIVE . $exterior_arr[$k];?>','mainImgAnchor<?php echo $i;?>'); return false;">
                                        <img src="<?php echo BASE_RELATIVE . $exterior_arr[$k];?>" width="74" height="50" alt="<?php echo $car_list[$i]['maker'];?> <?php echo $car_list[$i]['model'];?>" class="img-thumb-item img-polaroid" />
                                    </a>
                                    <?php
                                            }
                                        }
                                    ?>
                                </div>
                                <div class="tab-pane" id="video<?php echo $i;?>">
                                    <?php
                                        // display videos.
                                        if( count($video_arr) < 1 ){
                                    ?>
                                    <p><?php echo $_LANG['SEARCH_NO_VIDEO_LABEL'];?></p>
                                    <?php
                                        }
                                        
                                        $max_vid = count($video_arr);
                                        
                                        if( $max_vid > 4 ){
                                            $max_vid = 4;
                                        }
                                        
                                        for( $k=0; $k < $max_vid; $k++ ){
                                    ?>
                                    <a href="<?php echo BASE_RELATIVE . $video_arr[$k];?>" rel="prettyPhoto">
                                        <img src="<?php echo BASE_RELATIVE;?>image/default_video.jpg" width="74" height="50" alt="<?php echo $car_list[$i]['maker'];?> <?php echo $car_list[$i]['model'];?>" class="img-thumb-item" />
                                    </a>
                                    <?php
                                        }
                                    ?>
                                </div>
                            </div>
                            <script language="javascript" type="text/javascript">
                                $(function () {
                                    $('#imgTab<?php echo $i;?> a:first').tab('show');
                                    
                                    $('#imgTab<?php echo $i;?> a').click(function(e){
                                        e.preventDefault();
                                        $(this).tab('show');
                                    });
                                })
                            </script>
                        </div>
                        <div class="clearfix"></div>
                        <?php
                            // display feature icons with tooltip.
                            $car_features = $_CLASS->getCarFeatures($car_list[$i]['id']);
                            
                            if( count($car_features) < 1 ){
                                // do nothing;
                            }
                            else {
                        ?>
                        <div class="features-wrapper">
                            <h4><?php echo $_LANG['SEARCH_CAR_FEATURES_HEADER'];?></h4>
                            <?php
                                for( $k=0; $k < count($car_features); $k++ ){
                            ?>
                            <img class="feature-icon" src="<?php echo BASE_RELATIVE . $car_features[$k]['feat_icon'];?>" title="<?php echo $car_features[$k]['feat_name'];?>" width="50" height="50" alt="<?php echo $car_features[$k]['feat_name'];?>" />
                            <?php
                                }
                            ?>
                        </div>
                        <?php
                            }
                            
                            // set reserve car link.
                            if( $_SESSION['log_group'] == 'public' || !isset($_SESSION['log_id']) ){
                                $reserve_car_link = '#';
                                $reserve_car_onclick = 'onclick="bootbox.alert(\''.addslashes($_LANG['SEARCH_RESERVE_CAR_LOGIN_REQUIRED']).'\');return false"';
                            }
                            else {
                                $reserve_car_link = BASE_RELATIVE . 'reserve-car/?cid=' . $car_list[$i]['id'];
                                $reserve_car_onclick = '';
                            }
                        ?>
                        
                        <div class="clearfix"></div>
                        <div class="handle-wrapper">
                            <span class="label-wrap"><?php echo CURRENCY_SYMBOL;?> <?php echo Utilities::formatPrice($car_list[$i]['price'],CURRENCY_CODE);?></span>
                            <div class="toolbar-wrapper">
                                <a href="#" onclick="addToFavorites('<?php echo $car_list[$i]['id'];?>'); return false;" class="btn btn-small btn-warning" style="margin-right:10px;"><i class="icon-star-empty" style="margin-right:10px;"></i> <?php echo $_LANG['SEARCH_FAV_BUTTON_LABEL'];?> </a>
                                <?php
                                    if( $_SETTING->getAllowReservation() ){
                                ?>
                                <a href="<?php echo $reserve_car_link;?>" <?php echo $reserve_car_onclick;?> class="btn btn-small btn-info" style="margin-right:10px;"><i class="icon-inbox" style="margin-right:10px;"></i> <?php echo $_LANG['SEARCH_RESERVE_BUTTON_LABEL'];?> </a>
                                <?php
                                    }
                                ?>
                                <a href="<?php echo BASE_RELATIVE . $car_slug;?>/car-detail/?cid=<?php echo ( isset($car_list[$i]['id']) ? $car_list[$i]['id'] : '');?>&o=<?php echo ( isset($_GET['o']) ? trim($_GET['o']) : '');?>&ot=<?php echo ( isset($_GET['ot']) ? trim($_GET['ot']) : '');?>&r=<?php echo ( isset($_GET['r']) ? trim($_GET['r']) : '');?>&pg=<?php echo ( isset($_GET['pg']) ? trim($_GET['pg']) : '1' );?>" class="btn btn-small btn-success"><i class="icon-tags" style="margin-right:10px;"></i> <?php echo $_LANG['SEARCH_DETAILS_BUTTON_LABEL'];?> </a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <?php
                        } // end for.
                    ?>
                </div>
                <div id="bpagination">
                    <?php
                        // display bottom pagination links [update:2013.07.11]
                        for( $i=0; $i < count($pagination); $i++ ){
                            // set current page color.
                            ( isset($_GET['pg']) ? $cp = (int)$_GET['pg'] : $cp = 1 );
                            ( $cp == ($i+1) ? $class = 'class="selectedbtn"' : $class = '' );
                    ?>
                    <a href="<?php echo BASE_RELATIVE;?>search/?o=<?php echo ( isset($_GET['o']) ? trim($_GET['o']) : 'featured');?>&ot=<?php echo ( isset($_GET['ot']) ? trim($_GET['ot']) : 'a');?>&r=<?php echo ( isset($_GET['r']) ? trim($_GET['r']) : '20');?>&pg=<?php echo ($i+1);?>" <?php echo $class;?>><?php echo ($i+1);?></a>
                    <?php
                        }
                    ?>
                </div>
                <script language="javascript" type="text/javascript">
                    /* add car to favorites */
                    function addToFavorites(carid){
                        // validate car id.
                        if( carid == undefined || carid.length < 1 ){
                            bootbox.alert('<?php echo addslashes($_LANG['SEARCH_FAVORITE_ERROR']);?>');
                        }
                        else {
                            // send add request.
                            console.log('called');
                            $.ajax({
                                url: '<?php echo BASE_RELATIVE;?>ajax/save_to_favorite.php',
                                type: "POST",
                                data: {car: carid},
                                success: function(data, textStatus, xhr){
                                    if( data.length > 20 ){
                                        bootbox.alert('<?php echo addslashes($_LANG['SEARCH_FAVORITE_ERROR']);?>');
                                    }
                                    else {
                                        // handle responses.
                                        if( data == 'ok' ){
                                            bootbox.alert('<?php echo addslashes($_LANG['SEARCH_FAVORITE_SUCCESS']);?>');
                                        }
                                        else if( data == 'error' ){
                                            bootbox.alert('<?php echo addslashes($_LANG['SEARCH_FAVORITE_ERROR']);?>');
                                        }
                                        else if( data == 'login' ){
                                            bootbox.alert('<?php echo addslashes($_LANG['SEARCH_FAVORITE_PHP_ERROR']);?>');
                                        }
                                        else {
                                            bootbox.alert('<?php echo addslashes($_LANG['SEARCH_FAVORITE_ERROR']);?>');
                                        }
                                    }
                                    
                                },
                                error: function(XMLHttpRequest, textStatus, errorThrown){
                                    bootbox.alert('<?php echo $_LANG['SEARCH_FAVORITE_ERROR'];?>');
                                }
                            });
                        }
                    }
                </script>
                <!-- END CONTENT -->
            </div>
            <div class="clearfix"></div>
        </div>
