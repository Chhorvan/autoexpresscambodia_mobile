<?php
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';


//get used vehicle.
$used_vehicle = $_CLASS->getVehicle_used();

//get used vehicle.
$new_vehicle = $_CLASS->getVehicle_new();

//get special offer.
$specialoffer = $_CLASS->specialoffer();

//get News.
$listnews = $_CLASS->loadNews();

// get dealder
$listdealer = $_CLASS->load_dealer();

//get show vehicle type on form.

$show_vehicle=$_CLASS->getVehicleType();
?>

<script type="text/javascript">
	var product_type='Car';

	var make ='';
	var model='';
	var ch='';
	var search="<?php if(isset($_GET['search'])) echo htmlspecialchars($_GET['search']);?>";
	// product_type="<?php if(isset($_GET['product_type'])) echo htmlspecialchars($_GET['product_type']);?>";

	var condition="<?php if(isset($_GET['condition'])) echo htmlspecialchars($_GET['condition']);?>";
	var fuel_type="<?php if(isset($_GET['fuel_type'])) echo htmlspecialchars($_GET['fuel_type']);?>";
	var engine_volume="<?php if(isset($_GET['engine_volume'])) echo htmlspecialchars($_GET['engine_volume']);?>";
	var engine_volume_from="<?php if(isset($_GET['engine_volume_from'])) echo htmlspecialchars($_GET['engine_volume_from']);?>";
	var engine_volume_to="<?php if(isset($_GET['engine_volume_to'])) echo htmlspecialchars($_GET['engine_volume_to']);?>";
	var country="<?php if(isset($_GET['country'])) echo htmlspecialchars($_GET['country']);?>";
	var transmission="<?php if(isset($_GET['transmission'])) echo htmlspecialchars($_GET['transmission']);?>";
	var vehicle_type="<?php if(isset($_GET['body_style'])) echo htmlspecialchars($_GET['body_style']);?>"
	var drive_type="<?php if(isset($_GET['drive_type'])) echo htmlspecialchars($_GET['drive_type']);?>";
	var category="<?php if(isset($_GET['category'])) echo htmlspecialchars($_GET['category']);?>";
	var category2="<?php if(isset($_GET['category2'])) echo htmlspecialchars($_GET['category2']);?>";
	var category3="<?php if(isset($_GET['category3'])) echo htmlspecialchars($_GET['category3']);?>";
	make="<?php if(isset($_GET['make'])) echo htmlspecialchars($_GET['make']);?>";
	model="<?php if(isset($_GET['model'])) echo htmlspecialchars($_GET['model']);?>";
	var steering="<?php if(isset($_GET['steering'])) echo htmlspecialchars($_GET['steering']);?>";
	var country="<?php if(isset($_GET['country'])) echo htmlspecialchars($_GET['country']);?>";
	var year_from="<?php if(isset($_GET['year_from'])) echo htmlspecialchars($_GET['year_from']); elseif(isset($_GET['man_year_from'])) echo htmlspecialchars($_GET['man_year_from']);?>";
	var year_to="<?php if(isset($_GET['year_to'])) echo htmlspecialchars($_GET['year_to']); elseif(isset($_GET['man_year_to'])) echo htmlspecialchars($_GET['man_year_to']);?>";
	var price_from="<?php if(isset($_GET['price_from'])) echo htmlspecialchars($_GET['price_from']);?>";
	var price_to="<?php if(isset($_GET['price_to'])) echo htmlspecialchars($_GET['price_to']);?>";
	var month_from="<?php if(isset($_GET['month_from'])) echo htmlspecialchars($_GET['month_from']); elseif(isset($_GET['man_month_from'])) echo htmlspecialchars($_GET['man_month_from']);?>";
	var month_to="<?php if(isset($_GET['month_to'])) echo htmlspecialchars($_GET['month_to']); elseif(isset($_GET['man_month_to'])) echo htmlspecialchars($_GET['man_month_to']);?>";
	var car_id="<?php if(isset($_GET['car_id'])) echo htmlspecialchars($_GET['car_id']);?>";
	var customSearch = "<?php if(isset($_GET['customSearch'])) echo htmlspecialchars($_GET['customSearch']);?>";
	if(product_type!=''){
		product_type="&product_type="+product_type;
	}

	if(vehicle_type!=''){
		ch =vehicle_type;
		vehicle_type="&vehicle_type="+vehicle_type.split(' ').join('+');

	}
	if(make!=''){
		make="&make="+make;
	}
	if(model!=''){
		model="&model="+model;
	}
	if(year_from!=''){
		year_from="&year_from="+year_from;
	}
	if(year_to!=''){
		year_to="&year_to="+year_to;
	}
	if(price_from!=''){
		price_from="&price_from="+price_from;
	}
	if(price_to!=''){
		price_to="&price_to="+price_to;
	}
</script>

<script>
    function hit_counter(id){
       $.post('<?php echo BASE_RELATIVE;?>counter',{myid:id});

    }

</script>
 
<body id="top" class="page-id-home">
<div id="homeContainer">
	<div class="container clearfix">		
		<div id="dealerSearch">
			<form action="" method="post">
				<input type="text" name="dealer_search" placeholder="Dealer's Name...." />
				<input type="submit" name="btn_search_dealer" value="Search" class="linkimg" />
			</form>
		</div><!-- #dealerSearch -->
		
		<div id="topDealer">
			<p class="title">Top Dealer</p>
			
			<ul class="clearfix">
				<?php

					$i=1;
					if(count($listdealer)>0){
					foreach($listdealer as $dealer){

				?>
				
				<li>
					<div class="dealerBlock">
						<?php
							if($dealer['image'] == ""){
							echo '<img width="40px" height="40px" style="border:1px solid #333" src="'.BASE_RELATIVE.'images/dealer_sample.png" />';
						?>
						<?php }else{?>
							<img src="<?php echo BASE_RELATIVE.'upload/crop-image.php?onthefly='.$dealer['image'].'&resize=40,40'; ?>" width="40px" height="40px" style="border:none" />
						<?php }?>
					
						<?php 
							if(!empty($dealer['username'])){
						?>
							<a href="<?php echo BASE_RELATIVE;?>company-detail?&id=<?php echo $dealer['user_id'];?>" title="<?php echo $dealer['company_name'];?>" >
								<p class="line">
									<?php 
										$count_len = strlen($dealer['company_name']);
										
										if($count_len <= 20){
											echo $dealer['company_name'];
										}else{
											echo substr($dealer['company_name'],0,17)."..."; 
										}
									?>
								</p>
							</a>
						<?php } ?>	
						
						<p class="tel">
						<?php 
							if(!empty($dealer['mobile'])){ 
								echo " Tel : ".$dealer['mobile']." ";
							}
						 ?></p>
						 
						<p class="email" title="<?php echo $dealer['email'];?>">
						<?php 
							$count_len = strlen($dealer['email']);
							
							if($count_len <= 30){
								echo 'Email : '.$dealer['email'].'';
							}else{
								echo 'Email : '.substr($dealer['email'],0,17)."...".'';
							}
						?></p>
						
						<p class="p">
							<a class="total">
								<?php 
									if(!empty($dealer['count'])){
										echo "Total : ".$dealer['count']." Cars";
									}else{
										echo "Total : 0 Cars";
									} 
								?>
							</a>
						</p>
						<div class="more">
							<a href="<?php echo BASE_RELATIVE;?>company-detail?&id=<?php echo $dealer['user_id'];?>" title="<?php echo $dealer['company_name'];?>" >
								<img src="<?php echo BASE_RELATIVE; ?>img/public_home/detail.png" />
							</a>
						</div>
					</div>				
				</li>
				
				<?php } } ?>
			</ul>
		</div><!-- #topDealer -->
		
		<div id="carSearch">			
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<select name="make" id="product_make">						
							<option style='text-align:left;' id='reload' value=''>Maker</option>
							<?php																
								$make = $_CLASS->load_make();
								foreach($make as $row){
									echo"<option>$row[make]</option>";
								}

							?>
						</select>
					</td>
				</tr>				
				<tr>
					<td>
						<select name="model" id="product_model">
							<option value=""><?php echo $_LANG['PUBLIC_HOME_ANY_MODEL_OPTION'];?></option>
						</select>
					</td>
				</tr>
				<tr>
					<td>
						<input type="text" id="searchCar" name="search_car" placeholder="Keyword...." />
					</td>
				</tr>
				<tr>
					<td>
						<input id="submit" type="submit" name="bnt_search" value="<?php echo $_LANG['PUBLIC_HOME_SEARCH'];?>" class="linkimg" />
					</td>
				</tr>
			</table>			
		</div><!-- #carSearch -->				
		
		<div id="lastestCar">
			<p class="title">LASTEST USED CARS</p>
			
			<div class="detail_sideright"> </div><!--End detail-->					
		
		</div><!-- #lastestCar -->
		<div id="pagegination">
			<!--<p class='total-record'>Total:</p>
			<p class='record'><?php echo $total_dealer; ?></p>
			<p class='label-record'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Records</p>-->
			<?php
				 echo "<p style='clear:both;'>$pagination_html</p>";
			?>
		</div>	
    </div><!-- .container -->


	
	
	
	
</div><!-- #homeContainer -->


<!---Customize jquery---->

<script src="<?php echo BASE_RELATIVE.'autocam_js/public_home/index.js'; ?>"></script>

<!-- Import script -->
<!--<script src="<?php //echo BASE_RELATIVE.'autocam_js/public_auto_search/ajax.js'; ?>"></script>-->
<!--<script src="<?php //echo BASE_RELATIVE.'autocam_js/public_auto_search/index.js'; ?>"></script>-->

<script type="text/javascript">
	$(document).ready(function() {
		
		var make = "";
		$('#product_make').change(function(e) {
				//product_type=product_type.split(' ').join('+');	
				var make = $(this).val();
				make = make.split(' ').join('+');				
				$('#product_model').load('ajax/load_car_model.php?make='+make);
				if(make=$(this).click()){
					$('#product_model').reload();
				}
        });		
	});

</script>

<!-- Script For image Scrollbar-->

<script type="text/javascript">
	/* Submit value */
	$('#submit').click(function(event) {				
		
		search_car = $('#searchCar').val();
		if(search_car!=''){
            search_car="&search_car="+search_car;
        }
		
		make = $('#product_make option:selected').val();		
         if(make!=''){
            make="&make="+make;
        }
		
		model = $('#product_model option:selected').val();		
         if(model!=''){
            model="&model="+model;
        }
		
        year_from = $('#year_from option:selected').val();		
         if(year_from!=''){
            year_from="&year_from="+year_from;
        }
        year_to = $('#year_to option:selected').val();
         if(year_to!=''){
            year_to="&year_to="+year_to;
        }
		
		
        /* price_from = $('#txtprice1').val();		
        if(price_from!=''){
            price_from="&price_from="+price_from;
        }
         price_to = $('#txtprice2').val();
         if(price_to!=''){
            price_to="&price_to="+price_to;
        } */
		
		/* if(r.checked){
			condition='0';			
			n.checked=false;
		}

		if(n.checked){
			condition='1';
			r.checked=false;
		}
		if(condition!=''){
			condition="&condition="+condition;					
		} */
		
		
		
		$(".detail_sideright").load("ajax/vechicle/load_vechicle.php?result=1"+search_car+make+model+year_from+year_to, function() {
			
		});		
    });

	$(function() {
		$('.scroll-pane').jScrollPane();
	});

</script>

<!-- Menu condition -->
<?php 		
		
	// vehicle_type
	$vehicle_type = $_GET["vehicle_type"];	
	$class_vehicle_type = $_GET["vehicle_type"];		

	if(isset($_GET["vehicle_type"])){
		$vehicle_type = str_replace(' ', '+', $vehicle_type);
		$class_vehicle_type = str_replace(' ', '_', $class_vehicle_type);
		$class_vehicle_type = str_replace('/', '_', $class_vehicle_type);
		?>
			<script type="text/javascript">
				vehicle_type="&vehicle_type=<?php echo $vehicle_type ?>";			
				
				$(".<?php echo $class_vehicle_type; ?>").prop('checked', true);
				
				
				$(".detail_sideright").load("ajax/vechicle/load_vechicle.php?result=1"+vehicle_type+make+model+year_from+year_to+price_from+price_to+condition+steering+fuel_type+engine_volume_from+engine_volume_to+engine_volume, function() {
					
				});				
			</script>
		<?php
	}
	
	// condition
	$condition = $_GET["condition"];
	
	if(isset($_GET["condition"])){
		?>
			<script type="text/javascript">
				condition="&condition=<?php echo $condition ?>";
				$(".<?php echo $condition; ?>").prop('checked', true);
				$(".detail_sideright").load("ajax/vechicle/load_vechicle.php?result=1"+vehicle_type+make+model+year_from+year_to+price_from+price_to+condition+steering+fuel_type+engine_volume_from+engine_volume_to+engine_volume, function() {
					
				});					
			</script>
		<?php
	}
			
	
	?>
		<script type="text/javascript">
			$(".detail_sideright").load("ajax/vechicle/load_vechicle.php?result=1"+vehicle_type+make+model+year_from+year_to+price_from+price_to+condition+steering+fuel_type+engine_volume_from+engine_volume_to+engine_volume, function() {
			
			});						
		</script>
	<?php
	
?>