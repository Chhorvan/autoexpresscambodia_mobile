<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';
/*
 * VALIDATE ADMINISTRATOR GROUP ------------------------------------------------
 */
if( $_SESSION['log_group'] == 'admin' )
{
    // get form feed.
    $fstatus = $_CLASS->getFormStatus();
    $fmessage = $_CLASS->getFormMessage();
    $fstyle = $_CLASS->getFormStyle();
    
    // get news list.
    $registerTypeList = $_CLASS->getRegisterTypeList();
?>
<link rel="stylesheet" href="<?php echo BASE_RELATIVE;?>css/community/community.css" />
            <div id="sectionContenWrapper">
                <div id="topMenu">
                    <p><a href="#" class="linkfade">Site Manager</a></p>

                </div>
                <?php include("php/sidebar/community.php");?>
            </div>
            <div id="sectionContent">
                <?php
                    if( $fstatus ){
                ?>
                <div class="alert <?php echo $fstyle;?>">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <?php echo $fmessage;?>
                </div>
                <?php
                    }
                ?>
                <table id="member_type_wrapper" class="member-setting-wrapper">
                    <tr>
                        <th>N<sup>o</sup></th>
                        <th>Member Type</th>
                        <th>Max Product</th>
                        <th>Max Product Photos</th>
                    </tr>
                    <tr class="row_add">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><a href="edit-register-type" class="button_add" value="Add"/>Add</td>
                    </tr>
                    <?php 
                    $i=0;
                    foreach($registerTypeList as $registerType){
                        $i++;
                        echo "<tr>
                            <td><label>{$i}</label></td>
                            <td>{$registerType['title']}</td>
                            <td>{$registerType['max_product']}</td>
                            <td>{$registerType['max_image']}</td>
                            <td class='button_wrapper'>
                                <form action='member-settings' method='post'/>
                                    <a class='sample_button hover' href='edit-register-type?id={$registerType['id']}'>Edit</a>
                                    
                                </form>
                            </td>
                        </tr>";
                    }

                    ?>
                    
                </table>
            </div>
            <div class="clearfix"></div>
        </div>
<?php
} // end validation.
else
{
?>
        <p class="text-warning"><?php echo $_LOCAL['UNAUTHORIZED_ACCESS_MESSAGE'];?></p>
<?php
}