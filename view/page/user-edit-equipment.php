<?php
	if( !isset($_SESSION['log_group']) ){
		header('Location: '.BASE_RELATIVE.'login/');
	}

/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = true;
$_CKEDITOR_ID = '_ebody';
/*
 * VALIDATE ADMINISTRATOR GROUP ------------------------------------------------
 */

    // get form feed.
    $fstatus = $_CLASS->getFormStatus();
    $fmessage = $_CLASS->getFormMessage();
    $fstyle = $_CLASS->getFormStyle();
    $invalidProduct = false;
    // get lists.
    $maker_unique_list = $_CLASS->getMakerList();
    $model_unique_list = $_CLASS->getModelList();
    $body_type_list    = $_CLASS->getBodyList();
	$drive_type_list   = $_CLASS->getDriveList();

	$product_option_list = $_CLASS->getProductOptionList();
    $country_list = $_CLASS->getCountryList();
	$vehicle_type_list = $_CLASS->getVehicleTypeList();
	$car_maker_list = $_CLASS->getCarMakerList();
	$category_list = $_CLASS->getCategoryList();
	$fuel_type_list = $_CLASS->getFuelTypeList();
	$transmission_list = $_CLASS->getTransmissionList();
    // set input size for maker.
    ( count($maker_unique_list) > 0 ? $imaker = 'style="width:249px;margin-right:-1px;"' : $imaker = '' );
    ( count($model_unique_list) > 0 ? $imodel = 'style="width:249px;margin-right:-1px;"' : $imodel = '' );
    ( count($body_type_list) > 0 ? $ibody = 'style="width:249px;margin-right:-1px;"' : $ibody = '' );
    if( CURRENCY_CODE == 'USD' ){
        $decimal = '.';
        $thousand= ',';
    }
    else {
        $decimal = ',';
        $thousand= '.';
    }
    if(!isset($_GET['cid'])) $getId='';
	else $getId=$_GET['cid'];
	$product=array();

    // get generated car id.
	if(empty($getId)){
    	$carid = $_CLASS->getCarId();
	}else{
		$carid = $getId;
		$product = $_CLASS->getAllProducts($carid);
        $invalidProduct=$_CLASS->invalidProduct;
	}
	(isset($product['payment_terms']) ? $payment_terms = explode('^',$product['payment_terms']) : $payment_terms=array());
	(isset($product['other_options']) ? $other_options = explode('^',$product['other_options']) : $other_options=array());
	(isset($product['safety_device_options']) ? $safety_device_options = explode('^',$product['safety_device_options']) : $safety_device_options=array());
	(isset($product['exterior_options']) ? $exterior_options = explode('^',$product['exterior_options']) : $exterior_options=array());
	(isset($product['interior_options']) ? $interior_options = explode('^',$product['interior_options']) : $interior_options=array());



?>
<script>
var category="<?php if(isset($product['category'])) echo $product['category']; ?>";
var subCategoryGroup="<?php if(isset($product['sub_category_group'])) echo $product['sub_category_group']; ?>";
var subCategory="<?php if(isset($product['sub_category'])) echo $product['sub_category']; ?>";
var madeIn="<?php if(isset($product['made_in'])) echo $product['made_in']; ?>";
var condition="<?php if(isset($product['condition'])) echo $product['condition']; ?>";
var vehicleType="<?php if(isset($product['vehicle_type'])) echo $product['vehicle_type']; ?>";
var make="<?php if(isset($product['make'])) echo $product['make']; ?>";
var model="<?php if(isset($product['model'])) echo $product['model']; ?>";
var year="<?php if(isset($product['model_year'])) echo $product['model_year']; ?>";
var measureType="<?php if(isset($product['mileage_type'])) echo $product['mileage_type']; ?>";
var transmission="<?php if(isset($product['transmission'])) echo $product['transmission']; ?>";
var fuelType="<?php if(isset($product['fuel_type'])) echo $product['fuel_type']; ?>";
var currency="<?php if(isset($product['currency'])) echo $product['currency']; ?>";
var country="<?php if(isset($product['location'])) echo $product['location']; ?>";
var city="<?php if(isset($product['city'])) echo $product['city']; ?>";
var door="<?php if(isset($product['door'])) echo $product['door']; ?>";
var driveType="<?php if(isset($product['drive_type'])) echo $product['drive_type']; ?>";
var driveAvail="<?php if(isset($product['drive_availability'])) echo $product['drive_availability']; ?>";
var wheelType="<?php if(isset($product['wheel_type'])) echo $product['wheel_type']; ?>";
function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
      }
</script>
        <div id="content-wrapper">
       
            <div id="my_item">
                <ul class="clearfix myitem">
                    <li><a href="<?php echo BASE_RELATIVE; ?>my-info"><img src="<?php echo BASE_RELATIVE; ?>images/mywini/my_infonew.png" class="linkfade" /></a></li>
                    <li><a href="<?php echo BASE_RELATIVE; ?>messages"><img src="<?php echo BASE_RELATIVE; ?>images/mywini/message_boxnew.png" class="linkfade" /></a></li>
                    <li><a href="<?php echo BASE_RELATIVE; ?>my-inventory"><img src="<?php echo BASE_RELATIVE; ?>images/mywini/my_inventorynew.png" class="linkfade" /></a></li>
                    <li><a href="<?php echo BASE_RELATIVE; ?>register-product"><img src="<?php echo BASE_RELATIVE; ?>images/mywini/register_productover.png" class="linkfade" /></a></li>
                </ul>

                <p>Categories</p>

               <div id="amazingcarousel-1" style="display:block;position:relative;margin-top:20px; margin-bottom:30px; width:87.8%">
        		<div class="amazingcarousel-list-container">
                  <ul class="clearfix item amazingcarousel-list">
                    <li title="car" class="amazingcarousel-item">
                        <a href="<?php echo BASE_RELATIVE; ?>edit-car" >
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/car_over.png" />
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/car.png" class="linkfade car" />
                        </a>
                    </li>
                    <li title="suv" class="amazingcarousel-item">
                        <a href="<?php echo BASE_RELATIVE; ?>edit-suv">
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/suv_hover.png" />
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/suv.png" class="linkfade suv " />

                        </a>
                    </li>
                    <li title="van" class="amazingcarousel-item">
                        <a href="<?php echo BASE_RELATIVE; ?>edit-van">
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/van_hover.png" />
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/van.png" class="linkfade van " />

                        </a>
                    </li>
                    <li title="pickup" class="amazingcarousel-item">
                        <a href="<?php echo BASE_RELATIVE; ?>edit-pickup">
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/pickup_hover.png" />
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/pickup.png" class="linkfade pickup " />

                        </a>
                    </li>
                    <li title="truck" class="amazingcarousel-item">
                        <a href="<?php echo BASE_RELATIVE; ?>edit-truck">
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/truck_over.png" />
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/truck.png" class="linkfade truck" />
                        </a>
                    </li>
                    <li title="bus" class="amazingcarousel-item">
                        <a href="<?php echo BASE_RELATIVE; ?>edit-bus">
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/bus_over.png" />
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/bus.png" class="linkfade bus" />
                        </a>
                    </li>
                    <li title="part" class="amazingcarousel-item">
                        <a href="<?php echo BASE_RELATIVE; ?>edit-part">
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/part_over.png" />
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/part.png" class="linkfade part" />

                        </a>
                    </li>

                    <li title="equipment" class="amazingcarousel-item">
                        <a href="#">
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/equipment_over.png" />
                        </a>
                    </li>
                    <li title="watercraft" class="amazingcarousel-item">
                        <a href="<?php echo BASE_RELATIVE; ?>edit-watercraft">
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/watercraft_over.png" />
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/watercraft.png" class="linkfade watercraft" />

                        </a>
                    </li>
                    <li title="aircraft" class="amazingcarousel-item">
                        <a href="<?php echo BASE_RELATIVE; ?>edit-aircraft">
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/aircraft_over.png"  />
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/aircraft.png" class="linkfade aircraft"  />
                        </a>
                    </li>
                    <li title="motorbike" class="amazingcarousel-item">
                        <a href="<?php echo BASE_RELATIVE; ?>edit-motorbike">
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/motorbike_mobileover.png" />
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/motorbike-mobile.png" class="linkfade motorbike" />

                        </a>
                    </li>
                </ul>
                 </div>
                 <div class="amazingcarousel-prev"></div>
                <div class="amazingcarousel-next"></div>
                <div class="amazingcarousel-nav"></div>
                </div>
            </div>
            
            <!-- <div id="topMenu">
            	<p><a href="#" class="linkfade">Register Item</a> > Heavy Machine</p>
            </div>
            <?php include("php/sidebar/my-wini.php");?>
            </div> -->
            <div id="inner-content">
                <!-- START CONTENT -->
                 <!-- <div class="page-title">
                	<?php
					if(empty($getId)){
						echo "<p>".$_LANG['NEWCAR_HEADER']."</p>";
					}else{
						echo "<p>".$_LANG['NEWCAR_EDIT_HEADER']."</p>";
					}
					?>
                </div> -->
                <?php
                    if( $fstatus ){
                ?>
                <div class="alert <?php echo $fstyle;?>">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <?php echo $fmessage;?>
                </div>
                <?php
                    }
                ?>

 

                <form id="form-wrap" method="post">
                <!----------------- FORM CONTAINER -------------------->
                    <?php if(!$invalidProduct){?>
                    <input type="hidden" name="caridInput" id="hiddendusereditproductid" value="<?php echo $carid;?>" />
                    <div id="content1">
                       <table class="edit_product_table">
                            <tr>
                             <td class="edit_haft_row">
                            <div class="add-on title-label"><?php echo $_LANG['NEWCAR_CONDITION_LABEL'];?> <font color="#C1272D">*</font></div>
                            <label><input type="radio" name="conditionInput" value="Used"/><?php echo $_LANG['NEWCAR_CONDITION_USED'];?></label>
                            <label><input type="radio" checked id="conditionInput" name="conditionInput" value="New"/><?php echo $_LANG['NEWCAR_CONDITION_NEW'];?></label>

                       </td>
                                <td class="edit_haft_row">
                            <div class="add-on title-label"><?php echo $_LANG['NEWCAR_CATEGORY_LABEL'];?> <font color="#C1272D">*</font></div>
                            <select name="categorySelect" required id="categorySelect">
                            	<option value="">- Select -</option>
                            <?php
                                if( count($category_list['Equipment']) > 0 ){

                                    for( $i=0; $i < count($category_list['Equipment']); $i++ ){
                                        if( !empty($category_list['Equipment'][$i]) ){
                                ?>
                                <option value="<?php echo $category_list['Equipment'][$i];?>"><?php echo $category_list['Equipment'][$i];?></option>
                            <?php
                                        }
                                    }
                                }
                                else {
                            ?>
                                <option value="">Other</option>
                            <?php
                                }
                            ?>
                            </select>

                        </td>

                       </tr>
                       <td class="edit_haft_row">

                            <div class="add-on title-label"><?php echo $_LANG['NEWCAR_MAKER_LABEL'];?> <font color="#C1272D">*</font></div>


                            <select required id="makerInputSelect" name="makerInputSelect">

                                <?php
                                    $_GET['product_type']="Equipment";
                                    include('ajax/get_product_make_options.php');
                                ?>

                            </select>
                            <input type="text" maxlength="30" id="makerInput" name="makerInput" class="content-label other_input" placeholder="" />

                       </td>
                       <td class="edit_haft_row">
                            <div class="add-on title-label"><?php echo $_LANG['NEWCAR_BUCKET_SIZE_LABEL'];?></div>

                            <input type="text" value="<?php if(isset($product['bucket_size'])) echo $product['bucket_size'];?>" name="bucketSizeInput" class="content-label" maxlength="20" placeholder="<?php echo $_LANG['NEWCAR_BUCKET_SIZE_PLACEHOLDER'];?>" />

                       </td>

                       </tr>
                       <tr>
                       <td class="edit_haft_row">

                            <div class="add-on title-label"><?php echo $_LANG['NEWCAR_MODEL_LABEL'];?> <font color="#C1272D">*</font></div>

                             <select required id="modelInputSelect" name="modelInputSelect">

                                <option value="">- Select -</option>

                            </select>
                            <input type="text" maxlength="50" id="modelInput" name="modelInput" class="content-label other_input" placeholder="" />

                       </td>
                       <td class="edit_haft_row">

                            <div class="add-on title-label"><?php echo $_LANG['NEWCAR_TOTAL_WEIGHT_LABEL'];?></div>
                            <input type="text" value="<?php if(isset($product['total_weight'])) echo $product['total_weight'];?>" name="totalWeightInput" class="content-label" placeholder="<?php echo $_LANG['NEWCAR_TOTAL_WEIGHT_PLACEHOLDER'];?>" />



                       </td>
                       </tr>
                        <tr>
                       <td class="edit_haft_row">

                            <div class="add-on title-label"><?php echo $_LANG['NEWCAR_YEAR_LABEL'];?> <font color="#C1272D">*</font></div>

                            <select required id="yearInput" name="yearInput">

                                <option value="">- Select -</option>
                                <?php
                                for( $i=date('Y'); $i > 1900; $i-- ){
                                ?>
                                    <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                <?php
                                    }
                                ?>
                            </select>


                       </td>
                       <td class="edit_haft_row">

                            <div class="add-on title-label"><?php echo $_LANG['NEWCAR_LOADING_WEIGHT_LABEL'];?></div>

                            <input type="text" value="<?php if(isset($product['loading_weight'])) echo $product['loading_weight'];?>" name="loadingWeightInput" class="content-label" placeholder="<?php echo $_LANG['NEWCAR_LOADING_WEIGHT_PLACEHOLDER'];?>" />

                       </td>
                       </tr>
                       <tr>
                        <td class="edit_haft_row">

                            <div class="add-on title-label"><?php echo $_LANG['NEWCAR_LENGTH_LABEL'];?></div>
                            <input type="text" value="<?php if(isset($product['length'])) echo $product['length'];?>" id="lengthInput" name="lengthInput" class="content-label" placeholder="<?php echo $_LANG['NEWCAR_LENGTH_PLACEHOLDER'];?>"/>


                        </td>
                        <td class="edit_haft_row">

                            <div class="add-on title-label">VIN</div>
                            <input type="text" value="<?php if(isset($product['chassis_no'])) echo $product['chassis_no'];?>" maxlength="50" name="chassisNoInput" class="content-label" placeholder="A123456" />


                        </td>
                       </tr>
                       <tr>
                        <td class="edit_haft_row">
                            <div class="add-on title-label"><?php echo $_LANG['NEWCAR_HEIGHT_LABEL'];?></div>
                            <input type="text" value="<?php if(isset($product['height'])) echo $product['height'];?>" name="heightInput" class="content-label" placeholder="<?php echo $_LANG['NEWCAR_HEIGHT_PLACEHOLDER'];?>" />


                        </td>
                        <td class="edit_haft_row">

                            <div class="add-on title-label"><?php echo $_LANG['NEWCAR_PRICE_LABEL'];?></div>

                            <input type="text"  style="width:139px;" value="<?php if(isset($product['price'])) echo $product['price'];?>" name="priceInput" class="content-label" placeholder="<?php echo $_LANG['NEWCAR_PRICE_PLACEHOLDER'];?>" onkeypress="return isNumberKey(event)" />
                            <select name="currencySelect" id="currencySelect">
                                <?php include("ajax/load_currency_list.php");?>
                            </select>

                        </td>
                        </tr>
                        <tr>
                            <td class="edit_haft_row">

                                <div class="add-on title-label"><?php echo $_LANG['NEWCAR_WIDTH_LABEL'];?></div>
                                <input type="text" value="<?php if(isset($product['width'])) echo $product['width'];?>" name="widthInput" class="content-label" placeholder="<?php echo $_LANG['NEWCAR_WIDTH_PLACEHOLDER'];?>" />


                            </td>
                            <td class="edit_haft_row">
                                <div class="add-on title-label">Location </div>
                                <select id="citySelect" name="citySelect" style="">
                                    <?php
                                        $_GET['country']=$_CLASS->getUserCountry();
                                        include('ajax/load_saved_city.php');
                                    ?>
                                </select>
                                <input type="text" maxlength="50" id="cityInput" name="cityInput" class="content-label other_input" placeholder="" />
                            </td>
                        </tr>
                        <tr>
                         <td class="edit_haft_row" style="border-right:none;padding-left:10px" colspan="2"  >

                            <div class="add-on title-label"><?php echo $_LANG['NEWCAR_WORK_TIME_LABEL'];?></div>
                            <input type="text" value="<?php if(isset($product['work_time'])) echo $product['work_time'];?>" name="workTimeInput" class="content-label" maxlength="11" placeholder="<?php echo $_LANG['NEWCAR_WORK_TIME_PLACEHOLDER'];?>" onkeypress="return isNumberKey(event)"/>

                         </td>

                         </tr>
                         <tr>
                         <td class="edit_haft_row" style="border-right:none;padding-left:10px" colspan="2"  >

                            <div class="add-on title-label"><?php echo $_LANG['NEWCAR_WHEEL_TYPE_LABEL'];?> <font color="#C1272D">*</font></div>
                            <select required name="wheelTypeSelect" id="wheelTypeSelect">
                                <option value="">- Select -</option>
                                <option value="Wheel">Wheel</option>
                                <option value="Caterpillar">Caterpillar</option>
                            </select>

                        </td>
                         </tr>
                         <tr>
                                <td style="border-right:none;padding-left:10px" colspan="2" class="edit_haft_row">
                                    <div class="add-on block-label"><b>Description</b></div>
                                    <textarea id="sellerCommentText" name="sellerCommentText" class="tinymce content-label"><?php if(isset($product['seller_comment'])) echo $product['seller_comment'];?></textarea>
                                </td>
                        </tr>
                       </table>


                        <!-------------- UPLOAD MEDIA BLOCK ---------->

                        <div id="video-wrap" style="display:none;" class="input-prepend input-append inputap">
                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_YOUTUBE_URL_LABEL'];?></span>
                            <input id="videoUrlInput" type="text" class="content-label" style="width:435px;" placeholder="<?php echo $_LANG['NEWCAR_YOUTUBE_URL_PLACEHOLDER'];?>" />
                            <input id="vidBtn" onclick="saveVideoHandler('<?php echo $carid;?>');return false;" type="submit" class="btn btn-info" style="width:100px;" value="<?php echo $_LANG['NEWCAR_SAVE_BUTTON'];?>" />
                        </div>
                        <div id="upload-wrap"  style="display:none; border-bottom:none" class="input-prepend input-append inputap">
                            <h4 style="padding:10px;"><?php echo $_LANG['NEWCAR_UPLOAD_LABEL'];?></h4>
                            <iframe id="iupload" width="90%" height="65" style="border:0;"></iframe>
                        </div>

						<input style="margin:-65px 0px 0px 0px;float:right; position:relative;z-index:999;" id="submitbtn" type="submit" name="savebtn" class="btn btn-small btn-info" value="<?php echo $_LANG['NEWCAR_SAVE_BUTTON'];?>" />
                    </div>
                    <br>
                    <div id="outputwrap"></div>
                    <div id="outoutimg" class="clearfix"></div>
                    <div class="clearfix"></div>
                    <br>
                    <a id="prevbtn" href="#" onclick="return previousFormHandler()" class="btn btn-small" style="display:none;margin-right:10px;">
                        <i class="icon-circle-arrow-left" style="margin-right:10px;"></i><?php echo $_LANG['NEWCAR_PREVIOUS_BUTTON_LABEL'];?>
                    </a>
                    <a id="nextbtn" href="#" onclick="return nextFormHandler()" class="btn btn-small" style="display:none;margin-right:10px;">
                        <?php echo $_LANG['NEWCAR_NEXT_BUTTON_LABEL'];?><i class="icon-circle-arrow-right" style="margin-left:10px;"></i>
                    </a>
                    <!--For Auto Save Disable And Save Button-->

                      <!--End Auto Save Disable And Save Button-->

                    <!-------------- END UPLOAD MEDIA BLOCK ------------>
                    <!----------------- END FORM CONTAINER -------------------->
                    <?php
                    }
                    else{
                        echo "<font color='#C1272D'>Product with this ID is does not exist!</font>";
                    }


                    ?>

                </form>
                <!-- END CONTENT -->

            

                        <!-------------- END UPLOAD MEDIA BLOCK ---------->
                    </div>


                    <div class="clearfix"></div>
                    <div id="content2">

            </div>
            <div class="clearfix"></div>
        </div>
        <script language="javascript" type="text/javascript">
            /* preview uploaded image */
            $(document).ready(function(e){
                displayController('<?php echo BASE_RELATIVE;?>view/3rdparty/file_uploader/user-addpic.php?cid=<?php echo $carid;?>');
            });

            function basename(path) {
               return path.split('/').reverse()[0];
            }


		   function save_data_form(){

				$('#submitbtn').trigger('click');

			}
		    function check_vehicle_data(){
				 var data_return=false;
				 if($('#categorySelect').val()=="" || $('#modelInputSelect').val()=="" || $('#countryInputSelect').val()=="" || $('#makerInputSelect').val()=="" || $('#yearInput').val()=="" || $('#wheelTypeSelect').val()==""){
					data_return= false;
				 }
				 else{
					 data_return= true;
				 }
				 return data_return;

			}


			 function carImageUploadedNew_Photo(id,proid,url,mode,fname,i){

                        var did = 'img' + Math.round((Math.random()*1000));
                        var rd='rd' + id;
                        var wrap = document.getElementById('outoutimg');
                        var html = '<div id="'+did+'" class="img-wrap">';
                        html += '<img src="'+url+'" width="200" height="150" style="margin:5px;" />';
                        html += '<p style="margin:0;padding-left:5px;">';

						if(i==0){

							 html +='<input type="radio"  onclick="checkimg(\''+id+'\',\''+proid+'\',\''+rd+'\');return true;" id='+rd+' name="primary_photo[]" value="'+fname+'"  checked="checked"  />Set as Primary Photo';
						}
                        else{
							 html +='<input type="radio" onclick="checkimg(\''+id+'\',\''+proid+'\',\''+rd+'\');return true;" id='+rd+' name="primary_photo[]" value="'+fname+'"  />Set as Primary Photo';
						}
                        html += '<a href="#" onclick="removeImageHandler(\''+id+'\',\''+basename(url)+'\',\''+did+'\',\''+rd+'\');return false;" class="btn btn-small btn-danger" style="float:right;margin-right:5px;">×</a>';
                        html += '</p>';
                        html += '</div>';

                        wrap.innerHTML += html;
                    }
                    function ucfirst (str) {
                        str += '';
                        var f = str.charAt(0).toUpperCase();
                        return f + str.substr(1);
                   }




            function carImageUploaded(id,proid,url,mode,fname,primary_photo){
                        var did = 'img' + Math.round((Math.random()*1000));
                        var rd='rd' + id;
                        var wrap = document.getElementById('outoutimg');
                        var html = '<div id="'+did+'" class="img-wrap">';
                        html += '<img src="'+url+'" width="200" height="150" style="margin:5px;" />';
                        html += '<p style="margin:0;padding-left:5px;">';

						if(primary_photo==1){

							 html +='<input type="radio"  onclick="checkimg(\''+id+'\',\''+proid+'\',\''+rd+'\');return true;" id='+rd+' name="primary_photo[]" value="'+fname+'"  checked="checked"  />Set as Primary Photo';
						}
                        else{
							 html +='<input type="radio" onclick="checkimg(\''+id+'\',\''+proid+'\',\''+rd+'\');return true;" id='+rd+' name="primary_photo[]" value="'+fname+'"  />Set as Primary Photo';
						}
                        html += '<a href="#" onclick="removeImageHandler(\''+id+'\',\''+basename(url)+'\',\''+did+'\',\''+rd+'\');return false;" class="btn btn-small btn-danger" style="float:right;margin-right:5px;">×</a>';
                        html += '</p>';
                        html += '</div>';

                        wrap.innerHTML += html;
                    }
                    function ucfirst (str) {
                        str += '';
                        var f = str.charAt(0).toUpperCase();
                        return f + str.substr(1);
                   }
        </script>
