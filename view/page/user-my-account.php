<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';
/*
 * VALIDATE ADMINISTRATOR GROUP ------------------------------------------------
 */
if( $_SESSION['log_group'] == 'user' )
{
    // get form feed.
    $fstatus = $_CLASS->getFormStatus();
    $fmessage = $_CLASS->getFormMessage();
    $fstyle = $_CLASS->getFormStyle();
    
    
?>
        <div id="content-wrapper">
            <div id="vertical-menu">
                <?php $_MENU->loadMenu($_GLANG); ?>
            </div>
            <div id="inner-content">
                <!-- START CONTENT -->
                <h1><?php echo $_LANG['MY_ACCOUNT_UPDATE_HEADER_LABEL'];?></h1>
                <?php
                    if( $fstatus ){
                ?>
                <div class="alert <?php echo $fstyle;?>">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <?php echo $fmessage;?>
                </div>
                <?php
                    }
                    
                    // get account information.
                    $acc = $_CLASS->getAccountInfo();
                    
                    if( count($acc) < 1 )
                    {
                ?>
                <p><?php echo $_LANG['MY_ACCOUNT_LOADER_QUERY_ERROR'];?></p>
                <?php
                    }
                    else
                    {
                ?>
                <p><span class="label label-info"><?php echo $_LANG['MY_ACCOUNT_NOTICE_LABEL'];?></span> <?php echo $_LANG['MY_ACCOUNT_NOTICE_TXT_LABEL'];?></p>
                <form method="post">
                    <div class="input-prepend">
                        <span class="add-on" style="width:120px;"><?php echo $_LANG['MY_ACCOUNT_EMAIL_LABEL'];?></span>
                        <input type="text" style="width:550px;" disabled="disabled=" value="<?php echo $acc['email'];?>" />
                    </div>
                    <div class="input-prepend">
                        <span class="add-on" style="width:120px;"><?php echo $_LANG['MY_ACCOUNT_PASSWORD_LABEL'];?></span>
                        <input type="password" name="passInput" style="width:550px;" placeholder="<?php echo $_LANG['MY_ACCOUNT_PASSWORD_PLACEHOLDER'];?>" />
                    </div>
                    <div class="input-prepend input-append" style="margin-left:2px;">
                        <span class="add-on" style="width:120px;"><?php echo $_LANG['MY_ACCOUNT_FULL_NAME_LABEL'];?> <span class="required-field">*</span></span>
                        <input type="text" name="nameInput" style="width:460px;" placeholder="<?php echo $_LANG['MY_ACCOUNT_FULL_NAME_PLACEHOLDER'];?>" value="<?php echo htmlentities($acc['name']);?>" />
                        <input type="hidden" name="uidHid" value="<?php echo $acc['id'];?>"/>
                        <input type="submit" name="updatebtn" class="btn btn-info" value="<?php echo $_LANG['MY_ACCOUNT_UPDATE_BUTTON_LABEL'];?>" style="width:90px;overflow:hidden;" />
                    </div>
                </form>
                <br>
                <h2><?php echo $_LANG['MY_ACCOUNT_REMOVE_HEADER_LABEL'];?></h2>
                <p><span class="label label-important"><?php echo $_LANG['MY_ACCOUNT_IMPORTANT_LABEL'];?></span> <?php echo $_LANG['MY_ACCOUNT_IMPORTANT_TXT_LABEL'];?></p>
                <form method="post">
                    <input type="hidden" name="accId" value="<?php echo $acc['id'];?>"/>
                    <input type="submit" name="removebtn" class="btn btn-danger" value="<?php echo $_LANG['MY_ACCOUNT_REMOVE_BUTTON_LABEL'];?>" onclick="return confirm('<?php echo $_LANG['MY_ACCOUNT_CONFIRM_REMOVE_LABEL'];?>');" />
                </form>
                <?php
                    }
                ?>
                <!-- END CONTENT -->
            </div>
            <div class="clearfix"></div>
        </div>
<?php
} // end validation.
else
{
?>
        <p class="text-warning"><?php echo $_LOCAL['UNAUTHORIZED_ACCESS_MESSAGE'];?></p>
<?php
}