	
	<link rel="stylesheet" href="<?php echo BASE_RELATIVE."view/theme/jtheme/css/public-success.css"; ?>">
	
	
    <div id="main-control">
   
    	<div class="page-title">
			 <div class="title-left">Success</div>
             <div id="title" ><img class="space" src="<?php echo BASE_RELATIVE;?>images/logout-icon/Cute-Ball-Go-icon.png" /></div>
      
		</div>
        
      <div style="padding:2%">
   		<p class="first">Thank you for Your Registration</p>
        <p class="second">Thank you for registering with Angkorauto.</p>
		<p class="third">Your account has been created and a verification email has been sent to your registered email address.<p>
		<p class="third">Please click on the verification link included in the email to activate your account.</p>
		<p class="fourth">Your account will not be activated until you verify your email address.</p>
		<p class="five"><span class="fourth">Having trouble accessing your account?</span> <span class="five">Please contact</span> 
        <span class="sixth"><a class="sixth" href="mailto:support@angkorauto.com">Online Customer Support.</a></span> </p>
    </div>
    </div>