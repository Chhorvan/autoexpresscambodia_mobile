<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';
/*
 * VALIDATE ADMINISTRATOR GROUP ------------------------------------------------
 */
if( $_SESSION['log_group'] == 'user' )
{
    // get form feed.
    $fstatus = $_CLASS->getFormStatus();
    $fmessage = $_CLASS->getFormMessage();
    $fstyle = $_CLASS->getFormStyle();
    
    $car = $_CLASS->getCar();
    
    require_once BASE_CLASS . 'class-utilities.php';
    $reservation_code = Utilities::generateRandomString(7);
?>
        <div id="content-wrapper">
            <div id="vertical-menu">
                <?php $_MENU->loadMenu($_GLANG); ?>
            </div>
            <div id="inner-content">
                <!-- START CONTENT -->
                <h1><?php echo $_LANG['RESERVE_VEHICLE_RESERVATION_HEADER'];?></h1>
                <?php
                    if( $fstatus ){
                ?>
                <div class="alert <?php echo $fstyle;?>">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <?php echo $fmessage;?>
                </div>
                <?php
                    }
                    
                    // check if car reservation is allowed.
                    if( $_SETTING->getAllowReservation() ){
                        // check if user has already reserved any vehicles.
                        if( !$_CLASS->allowReservation() ){
                ?>
                <div class="alert alert-info">
                    <?php echo $_LANG['RESERVE_USER_UNALLOW_RESERVATION_WARNING'];?>
                </div>
                <?php
                        }
                        else {
                            if( $fstyle != 'alert-success' ){
                ?>
                <p><?php echo $_LANG['RESERVE_HEADER_PLACEHOLDER'];?>:</p>
                <hr>
                <form method="post">
                    <h4><?php echo $_LANG['RESERVE_FULL_NAME_HEADER'];?></h4>
                    <div class="input-prepend">
                        <span class="add-on" style="width:240px;font-weight:bold;color:#666;"><?php echo $_LANG['RESERVE_FULL_NAME_LABEL'];?></span>
                        <input type="text" style="width:464px;" value="<?php echo $_SESSION['log_name'];?>" disabled="disabled" />
                    </div>
                    <h4><?php echo $_LANG['RESERVE_CONTACT_HEADER'];?></h4>
                    <div class="input-prepend">
                        <span class="add-on" style="width:240px;font-weight:bold;color:#666;"><?php echo $_LANG['RESERVE_EMAIL_LABEL'];?></span>
                        <input type="text" style="width:464px;" value="<?php echo $_SESSION['log_email'];?>" disabled="disabled" />
                    </div>
                    <div class="input-prepend">
                        <span class="add-on" style="width:240px;font-weight:bold;color:#666;"><?php echo $_LANG['RESERVE_PHONE_LABEL'];?></span>
                        <input type="text" name="phoneInput" style="width:464px;" value="<?php echo ( isset($_SESSION['reserve_phone']) ? $_SESSION['reserve_phone'] : '' );?>" placeholder="<?php echo $_LANG['RESERVE_PHONE_PLACEHOLDER'];?>" />
                    </div>
                    <h4><?php echo $_LANG['RESERVE_ADDRESS_HEADER'];?></h4>
                    <div class="input-prepend">
                        <span class="add-on" style="width:240px;font-weight:bold;color:#666;"><?php echo $_LANG['RESERVE_ADDRESS_LABEL'];?></span>
                        <input type="text" name="addressInput" style="width:464px;" value="<?php echo ( isset($_SESSION['reserve_address']) ? $_SESSION['reserve_address'] : '' );?>" placeholder="<?php echo $_LANG['RESERVE_ADDRESS_PLACEHOLDER'];?>" />
                    </div>
                    <div class="input-prepend">
                        <span class="add-on" style="width:240px;font-weight:bold;color:#666;"><?php echo $_LANG['RESERVE_CITY_LABEL'];?></span>
                        <input type="text" name="cityInput" style="width:464px;" value="<?php echo ( isset($_SESSION['reserve_city']) ? $_SESSION['reserve_city'] : '' );?>" placeholder="<?php echo $_LANG['RESERVE_CITY_PLACEHOLDER'];?>" />
                    </div>
                    <div class="input-prepend">
                        <span class="add-on" style="width:240px;font-weight:bold;color:#666;"><?php echo $_LANG['RESERVE_ZIP_LABEL'];?></span>
                        <input type="text" name="zipInput" style="width:464px;" value="<?php echo ( isset($_SESSION['reserve_zip']) ? $_SESSION['reserve_zip'] : '' );?>" placeholder="<?php echo $_LANG['RESERVE_ZIP_PLACEHOLDER'];?>" />
                    </div>
                    <div class="input-prepend">
                        <span class="add-on" style="width:240px;font-weight:bold;color:#666;"><?php echo $_LANG['RESERVE_COUNTRY_LABEL'];?></span>
                        <input type="text" name="countryInput" style="width:464px;" value="<?php echo ( isset($_SESSION['reserve_country']) ? $_SESSION['reserve_country'] : '' );?>" placeholder="<?php echo $_LANG['RESERVE_COUNTRY_PLACEHOLDER'];?>" />
                    </div>
                    <h4><?php echo $_LANG['RESERVE_VEHICLE_HEADER'];?></h4>
                    <div class="input-prepend">
                        <span class="add-on" style="width:240px;font-weight:bold;color:#666;"><?php echo $_LANG['RESERVE_MAKER_LABEL'];?></span>
                        <input type="text" style="width:464px;" value="<?php echo $car['maker'];?>" disabled="disabled" />
                    </div>
                    <div class="input-prepend">
                        <span class="add-on" style="width:240px;font-weight:bold;color:#666;"><?php echo $_LANG['RESERVE_MODEL_LABEL'];?></span>
                        <input type="text" style="width:464px;" value="<?php echo $car['model'];?>" disabled="disabled" />
                    </div>
                    <div class="input-prepend">
                        <span class="add-on" style="width:240px;font-weight:bold;color:#666;"><?php echo $_LANG['RESERVE_TYPE_LABEL'];?></span>
                        <input type="text" style="width:464px;" value="<?php echo $_CLASS->getBodyTypeByID($car['body_type']);?>" disabled="disabled" />
                    </div>
                    <div class="input-prepend">
                        <span class="add-on" style="width:240px;font-weight:bold;color:#666;"><?php echo $_LANG['RESERVE_YEAR_LABEL'];?></span>
                        <input type="text" style="width:464px;" value="<?php echo $car['year'];?>" disabled="disabled" />
                    </div>
                    <div class="input-prepend">
                        <span class="add-on" style="width:240px;font-weight:bold;color:#666;"><?php echo $_LANG['RESERVE_ID_LABEL'];?></span>
                        <input type="text" style="width:464px;" value="<?php echo $reservation_code;?>" disabled="disabled" />
                    </div>
                    <div class="input-prepend">
                        <span class="add-on" style="width:240px;font-weight:bold;color:#666;"><?php echo $_LANG['RESERVE_PRICE_LABEL'];?></span>
                        <input type="text" style="width:464px;" value="<?php echo CURRENCY_SYMBOL . Utilities::formatPrice($car['price'],CURRENCY_CODE);?>" disabled="disabled" />
                    </div>
                    <h4><?php echo $_LANG['RESERVE_POLICY_HEADER'];?></h4>
                    <?php
                        echo $_CLASS->getReservationPolicy();
                    ?>
                    <div class="clearfix"></div>
                    <h4 style="color:#CC0000;"><?php echo $_LANG['RESERVE_AGREEMENT_HEADER'];?></h4>
                    <label>
                        <input type="checkbox" name="agreeInput" value="1" style="margin-right:20px;margin-top:0px;" /> <?php echo $_LANG['RESERVE_AGREEMENT_LABEL'];?>
                    </label>
                    <div class="clearfix"></div>
                    <br>
                    <input type="hidden" name="rcode" value="<?php echo $reservation_code;?>"/>
                    <input type="submit" name="okbtn" class="btn btn-small btn-success" value="<?php echo $_LANG['RESERVE_RESERVE_BUTTON'];?>" />
                </form>
                <?php
                            }
                        }
                    } // end allow car reservation validation.
                    else {
                ?>
                <div class="alert alert-info">
                    <?php echo $_LANG['RESERVE_RESERVATION_DEACTIVATED'];?>
                </div>
                <?php
                    }
                ?>
                <!-- END CONTENT -->
            </div>
            <div class="clearfix"></div>
        </div>
<?php
} // end validation.
else
{
?>
        <p class="text-warning"><?php echo $_LOCAL['UNAUTHORIZED_ACCESS_MESSAGE'];?></p>
<?php
}