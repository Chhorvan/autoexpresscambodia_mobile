<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */

$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';
// get form feed.
$fstatus = $_CLASS->getFormStatus();
$fmessage = $_CLASS->getFormMessage();
$fstyle = $_CLASS->getFormStyle();
$pagination_html = $_CLASS->pagination_html;
$products = $_CLASS->getProductList();
$total_row = $_CLASS->total_num_row;
$row_per_page = $_CLASS->row_per_page;
if(isset($_GET['s'])) $offset=htmlspecialchars($_GET['s']); else $offset=0;
if(isset($_GET['product_status'])) $product_status=htmlspecialchars($_GET['product_status']); else $product_status=2;
//var_dump($products);
$group = 'admin';
$slug  = 'car';
// load page content.

?>
<script type="text/javascript">
    var offset=<?php if(isset($_GET['s'])) echo htmlspecialchars($_GET['s']); else echo '0';?>;
    var del_flag="<?php if(isset($_GET['product_status'])) echo htmlspecialchars($_GET['product_status']); else echo '2';?>";
    var company_id="<?php if(isset($_GET['company_id'])) echo htmlspecialchars($_GET['company_id']); else echo '';?>";
    var company_name= "<?php if(isset($_GET['company_name'])) echo htmlspecialchars($_GET['company_name']); else echo '';?>";
    var product_type="<?php if(isset($_GET['categorySelect'])) echo htmlspecialchars($_GET['categorySelect']); else echo '';?>";
    var make="<?php if(isset($_GET['makeSelect'])) echo htmlspecialchars($_GET['makeSelect']); else echo '';?>";
    var model="<?php if(isset($_GET['modelSelect'])) echo htmlspecialchars($_GET['modelSelect']); else echo '';?>";
    var min_price="<?php if(isset($_GET['price_from'])) echo htmlspecialchars($_GET['price_from']); else echo '';?>";
    var max_price="<?php if(isset($_GET['price_to'])) echo htmlspecialchars($_GET['price_to']); else echo '';?>";
    var currency="<?php if(isset($_GET['currencySelect'])) echo htmlspecialchars($_GET['currencySelect']); else echo '';?>";
</script>



 <div id="sectionContenWrapper">              
    <div id="topMenu">
        <p><a href="#" class="linkfade">Site Manager</a></p>

    </div>
    <?php include("php/sidebar/community.php");?>
    </div>
    <div id="sectionContent">
        <div class="page_title">Product Manager</div> 
        
        
        
        <?php
        ///////////Show saving result
            if( $fstatus ){
        ?>
        <div class="alert <?php echo $fstyle;?>">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <?php echo $fmessage;?>
        </div>
        <?php
            }
        ///////////End Show saving result
        ?>
        <!-- Content body -->

        <div class="tab_content" id="seller_members_wrapper">
            <form method="get" action="product-manager">
                <div class="search_wrapper">
                    <div class="search_container">
                        <label>Show only: </label>
                        <label><input type="radio" name="product_status" checked="" value="2"/>Not Verified</label>
                        <label><input type="radio" name="product_status" value="0"/>Verified</label>
                        <label><input type="radio" name="product_status" value="1"/>Deleted</label>
                    </div>
                    <div class="search_container">
                        <input type="hidden" name="company_id" id="company_id" value=""/>
                        <input type="hidden" name="company_name" id="company_name" value=""/>
                        <label>Company: </label><b><label id="company_label"></label></b><a id="company-btn" href="#">Select Company</a> - <a id="clear-company-btn" href="#">Clear</a>
                    </div>
                    <div class="search_container">
                        <select name="categorySelect" id="categorySelect">
                            <option value="">- All Category -</option>
                            <option value="car">Car</option>
                            <option value="suv">Suv</option>
                            <option value="van">Van</option>
                            <option value="pickup">Pickup</option>
                            <option value="truck">Truck</option>
                            <option value="bus">Bus</option>
                            <option value="equipment">Heavy Machinery</option>
                            <option value="part">Auto Part</option>
                            <option value="accessory">Accessory</option>
                            <option value="motorbike">Motorbike</option>
                            <option value="aircraft">Aircraft</option>
                            <option value="watercraft">Watercraft</option>
                        </select>
                        <select name="makeSelect" id="makeSelect">
                            <option value="">-- Make --</option>
                        </select>
                        <select name="modelSelect" id="modelSelect">
                            <option value="">-- Model --</option>
                        </select>
                        
                    </div>
                    <div class="search_container">
                        <input type="text" value="" name="price_from" id="price_from" placeholder="Min Price"/>
                        <input type="text" value="" name="price_to" id="price_to" placeholder="Max Price"/>
                        <select name="currencySelect" class="currencySelect" id="currencySelect">
                            
                        </select>
                    </div>
                    <div class="search_container">
                        <input type="submit" name="submit_search" class="search_button" value=""/>
                    </div>
                </div>
            </form>
            <!--
            <form method="post">

                <div class="selected_button_wrapper">
                    <input type="hidden" id="product_type" name="product_type" value="Car"/>
                    <input type="submit" name="delete_selected" class="selected_button" id="delete_selected" value="DELETE SELECTED">
                </div>
                
                
                <div id="product_table_wrapper">
                    <div></div>
                </div>
            </form>
            -->
            <div class="result_title">
                <div class="left"><label>Search Result</label></div>
                <div class="right">
                    
                </div>
            </div>
            <div class="product_mode_wrapper">
                <input type="checkbox" name="checkall" id="checkall"/>
                <?php if($product_status==2){?>
                    <input type="button" class="normal-btn mode-selected-btn" data-mode="0" value="Accept Selected"/>
                    <input type="button" class="normal-btn delete-selected-btn" data-mode="1" value="Delete Selected"/>
                <?php }elseif($product_status==0){?>
                    <input type="button" class="normal-btn mode-selected-btn" data-mode="2" value="Reject Selected"/>
                    <input type="button" class="normal-btn delete-selected-btn" data-mode="1" value="Delete Selected"/>
                <?php }elseif($product_status==1){?>
                    <input type="button" class="normal-btn mode-selected-btn" data-mode="0" value="Accept Selected"/>
                    <input type="button" class="normal-btn mode-selected-btn" data-mode="2" value="Restore Selected"/>
                <?php }?>
                
            </div>
            <div class="product_wrapper" id="product_table">
                <!-- Product table -->
                
                <!-- Product table -->
            </div>
            
        </div>
        <!-- End Content body -->
        
       
        <!-- end register Tab -->
    </div><!-- end div id="sectionContent"-->
</div><!-- end div id="sectionContentWraper" -->       

<div style="clear:both"></div>
  