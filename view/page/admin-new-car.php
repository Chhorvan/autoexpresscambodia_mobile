<?php

/*

 * If you need to use CKEditor in this page, make sure to set the controller

 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.

 */

$_CKEDITOR_ENABLED = true;

$_CKEDITOR_ID = '_ebody';

/*

 * VALIDATE ADMINISTRATOR GROUP ------------------------------------------------

 */

if( $_SESSION['log_group'] == 'admin' )

{

    // get form feed.

    $fstatus = $_CLASS->getFormStatus();

    $fmessage = $_CLASS->getFormMessage();

    $fstyle = $_CLASS->getFormStyle();

    

    // get lists.

    $maker_unique_list = $_CLASS->getMakerList();

    $model_unique_list = $_CLASS->getModelList();

    $body_type_list    = $_CLASS->getBodyList();

    $feature_icon_list = $_CLASS->getFeatureIconList();

    

    // set input size for maker.

    ( count($maker_unique_list) > 0 ? $imaker = 'style="width:249px;margin-right:-1px;"' : $imaker = '' );

    ( count($model_unique_list) > 0 ? $imodel = 'style="width:249px;margin-right:-1px;"' : $imodel = '' );

    ( count($body_type_list) > 0 ? $ibody = 'style="width:249px;margin-right:-1px;"' : $ibody = '' );

    if( CURRENCY_CODE == 'USD' ){

        $decimal = '.';

        $thousand= ',';

    }

    else {

        $decimal = ',';

        $thousand= '.';

    }

    

    // get generated car id.

    $carid = $_CLASS->getCarId();

?>

        <div id="content-wrapper">

            <div id="vertical-menu">

                <?php $_MENU->loadMenu($_GLANG); ?>

            </div>

            <div id="inner-content">

                <!-- START CONTENT -->

                <h1><?php echo $_LANG['NEWCAR_HEADER'];?></h1>

                <?php

                    if( $fstatus ){

                ?>

                <div class="alert <?php echo $fstyle;?>">

                    <button type="button" class="close" data-dismiss="alert">×</button>

                    <?php echo $fmessage;?>

                </div>

                <?php

                    }

                ?>

                <div id="stepper-wrap">

                    <div id="step1">

                        <p class="inner-step-p"><?php echo $_LANG['NEWCAR_STEP1_LABEL'];?></p>

                        <p class="inner-step-p unselected-p"><?php echo $_LANG['NEWCAR_STEP2_LABEL'];?></p>

                        <p class="inner-step-p unselected-p"><?php echo $_LANG['NEWCAR_STEP4_LABEL'];?></p>

                        <p class="inner-step-p unselected-p"><?php echo $_LANG['NEWCAR_STEP3_LABEL'];?></p>

                        <div class="clearfix"></div>

                    </div>

                    <div id="step2">

                        <p class="inner-step-p unselected-p"><?php echo $_LANG['NEWCAR_STEP1_LABEL'];?></p>

                        <p class="inner-step-p"><?php echo $_LANG['NEWCAR_STEP2_LABEL'];?></p>

                        <p class="inner-step-p unselected-p"><?php echo $_LANG['NEWCAR_STEP4_LABEL'];?></p>

                        <p class="inner-step-p unselected-p"><?php echo $_LANG['NEWCAR_STEP3_LABEL'];?></p>

                        <div class="clearfix"></div>

                    </div>

                    <div id="step3">

                        <p class="inner-step-p unselected-p"><?php echo $_LANG['NEWCAR_STEP1_LABEL'];?></p>

                        <p class="inner-step-p unselected-p"><?php echo $_LANG['NEWCAR_STEP2_LABEL'];?></p>

                        <p class="inner-step-p"><?php echo $_LANG['NEWCAR_STEP4_LABEL'];?></p>

                        <p class="inner-step-p unselected-p"><?php echo $_LANG['NEWCAR_STEP3_LABEL'];?></p>

                        <div class="clearfix"></div>

                    </div>

                    <div id="step4">

                        <p class="inner-step-p unselected-p"><?php echo $_LANG['NEWCAR_STEP1_LABEL'];?></p>

                        <p class="inner-step-p unselected-p"><?php echo $_LANG['NEWCAR_STEP2_LABEL'];?></p>

                        <p class="inner-step-p unselected-p"><?php echo $_LANG['NEWCAR_STEP4_LABEL'];?></p>

                        <p class="inner-step-p"><?php echo $_LANG['NEWCAR_STEP3_LABEL'];?></p>

                        <div class="clearfix"></div>

                    </div>

                </div>

                <form id="form-wrap" method="post">

                    <input type="hidden" name="caridInput" value="<?php echo $carid;?>" />

                    <div id="content1">

                        <h4><?php echo $_LANG['NEWCAR_STEP1_HEADER'];?></h4>

                        <div class="input-prepend input-append inputap">

                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_MAKER_LABEL'];?></span>

                            <input type="text" name="makerInput" <?php echo $imaker;?> class="content-label" placeholder="<?php echo $_LANG['NEWCAR_MAKER_PLACEHOLDER'];?>" />

                            <?php

                                if( count($maker_unique_list) > 0 ){

                            ?>

                            <span class="add-on"><?php echo $_LANG['NEWCAR_OR_SELECT_LABEL'];?></span>

                            <select name="makerInputSelect">

                                <?php

                                    for( $i=0; $i < count($maker_unique_list); $i++ ){

                                ?>

                                <option value="<?php echo $maker_unique_list[$i];?>"><?php echo $maker_unique_list[$i];?></option>

                                <?php

                                    }

                                ?>

                            </select>

                            <?php

                                }

                            ?>

                        </div>

                        <div class="input-prepend">

                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_MODEL_LABEL'];?></span>

                            <input type="text" name="modelInput" class="content-label" <?php echo $imodel;?> placeholder="<?php echo $_LANG['NEWCAR_MODEL_PLACEHOLDER'];?>" />

                            <?php

                                if( count($model_unique_list) > 0 ){

                            ?>

                            <span class="add-on"><?php echo $_LANG['NEWCAR_OR_SELECT_LABEL'];?></span>

                            <select name="modelInputSelect">

                                <?php

                                    for( $i=0; $i < count($model_unique_list); $i++ ){

                                ?>

                                <option value="<?php echo $model_unique_list[$i];?>"><?php echo $model_unique_list[$i];?></option>

                                <?php

                                    }

                                ?>

                            </select>

                            <?php

                                }

                            ?>

                        </div>

                        <div class="input-prepend">

                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_BODY_TYPE_LABEL'];?></span>

                            <?php

                                

                            ?>

                            <select name="bodyInputSelect" style="width:548px;">

                            <?php

                                if( count($body_type_list) > 0 ){

                                    for( $i=0; $i < count($body_type_list); $i++ ){

                                        if( !empty($body_type_list[$i]) ){

                                ?>

                                <option value="<?php echo $body_type_list[$i];?>"><?php echo $body_type_list[$i];?></option>

                            <?php

                                        }

                                    }

                                }

                                else {

                            ?>

                                <option value="0"><?php echo $_LANG['NEWCAR_NOBODYTYPE_LABEL'];?></option>

                            <?php

                                }

                            ?>

                            </select>

                        </div>

                        <div class="input-prepend input-append inputap">

                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_PRICE_LABEL'];?></span>

                            <span class="add-on"><?php echo CURRENCY_SYMBOL;?></span>

                            <input type="text" name="thousandInput" style="width:140px;margin-right:-1px" class="content-label" maxlength="3" placeholder="000" />

                            <span class="add-on"><?php echo $thousand;?></span>

                            <input type="text" name="hundredInput" style="width:150px;margin-right:-1px" class="content-label" maxlength="3" placeholder="000" />

                            <span class="add-on"><?php echo $decimal;?></span>

                            <input type="text" name="decimalInput" style="width:140px;" class="content-label" maxlength="2" placeholder="00" />

                        </div>

                        <div class="input-prepend">

                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_YEAR_LABEL'];?></span>

                            <select name="yearInput" style="width:548px;">

                            <?php

                            for( $i=date('Y'); $i > 1900; $i-- ){

                            ?>

                                <option value="<?php echo $i;?>"><?php echo $i;?></option>

                            <?php

                                }

                            ?>

                            </select>

                        </div>

                        <div class="input-prepend input-append inputap">

                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_MILES_LABEL'];?></span>

                            <input type="text" name="milesInput" style="width:393px;margin-right:-1px;" class="content-label" />

                            <select name="measureTypeInput" style="width:142px;">

                                <option value="Km">Km</option>

                                <option value="Miles">Miles</option>

                            </select>

                        </div>

                        <div class="input-prepend">

                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_DOORS_LABEL'];?></span>

                            <select name="doorInput" style="width:549px;">

                                <option value="0">0 <?php echo $_LANG['NEWCAR_DOOR_SELECT_LABEL'];?></option>

                                <option value="1">1 <?php echo $_LANG['NEWCAR_DOOR_SELECT_LABEL'];?></option>

                                <option value="2">2 <?php echo $_LANG['NEWCAR_DOOR_SELECT_LABEL'];?></option>

                                <option value="3">3 <?php echo $_LANG['NEWCAR_DOOR_SELECT_LABEL'];?></option>

                                <option value="4">4 <?php echo $_LANG['NEWCAR_DOOR_SELECT_LABEL'];?></option>

                                <option value="5">5 <?php echo $_LANG['NEWCAR_DOOR_SELECT_LABEL'];?></option>

                                <option value="6">6 <?php echo $_LANG['NEWCAR_DOOR_SELECT_LABEL'];?></option>

                                <option value="7">7 <?php echo $_LANG['NEWCAR_DOOR_SELECT_LABEL'];?></option>

                                <option value="8">8 <?php echo $_LANG['NEWCAR_DOOR_SELECT_LABEL'];?></option>

                                <option value="9">9 <?php echo $_LANG['NEWCAR_DOOR_SELECT_LABEL'];?></option>

                                <option value="10">10 <?php echo $_LANG['NEWCAR_DOOR_SELECT_LABEL'];?></option>

                            </select>

                        </div>

                        <div class="input-prepend">

                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_FEATURED_LABEL'];?></span>

                            <select name="featuredInput" style="width:549px;">

                                <option value="0"><?php echo $_LANG['NEWCAR_NO_LABEL'];?></option>

                                <option value="1"><?php echo $_LANG['NEWCAR_YES_LABEL'];?></option>

                            </select>

                        </div>

                        <div class="input-prepend">

                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_ENERGYCLASS_LABEL'];?></span>

                            <select name="energyInput" style="width:549px;">

                                <option value="A">A</option>

                                <option value="B">B</option>

                                <option value="C">C</option>

                                <option value="D">D</option>

                                <option value="E">E</option>

                                <option value="F">F</option>

                            </select>

                        </div>

                    </div>

                    <div class="clearfix"></div>

                    <div id="content2">

                        <h4><?php echo $_LANG['NEWCAR_FEATURE_ICON_HEADER'];?></h4>

                        <?php

                            for( $i=0; $i < count($feature_icon_list); $i++ ){

                                ( ($i%2) > 0 ? $bg = '#F7F7F7' : $bg = '#FFF') ;

                        ?>

                        <div class="icons-wrap" style="background-color:<?php echo $bg;?>;display:block;width:330px;float:left;">

                            <img src="<?php echo BASE_RELATIVE . $feature_icon_list[$i]['source'] ;?>" width="50" height="50" alt="<?php echo $feature_icon_list[$i]['label'];?>" title="<?php echo $feature_icon_list[$i]['label'];?>" />

                            <label>

                                <input type="checkbox" value="<?php echo $feature_icon_list[$i]['label'].'[:]' . $feature_icon_list[$i]['source'];?>" name="feat[<?php echo $i;?>]" style="margin-right:20px;" />

                                <?php echo $feature_icon_list[$i]['label'];?>

                            </label>

                        </div>

                        <?php

                            }

                        ?>

                        <div class="clearfix"></div>

                    </div>

                    <div class="clearfix"></div>

                    <div id="content3">

                        <div class="input-prepend">

                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_ENGINE_SIZE_LABEL'];?></span>

                            <input type="text" name="engineSizeInput" class="content-label" maxlength="150" placeholder="<?php echo $_LANG['NEWCAR_ENGINE_SIZE_PLACEHOLDER'];?>" />

                        </div>

                        <div class="input-prepend">

                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_TRIM_LABEL'];?></span>

                            <input type="text" name="trimInput" class="content-label" maxlength="150" placeholder="<?php echo $_LANG['NEWCAR_TRIM_PLACEHOLDER'];?>" />

                        </div>

                        <div class="input-prepend">

                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_TYPE_LABEL'];?></span>

                            <input type="text" name="typeInput" class="content-label" maxlength="150" placeholder="<?php echo $_LANG['NEWCAR_TYPE_PLACEHOLDER'];?>" />

                        </div>

                        <div class="input-prepend">

                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_GEAR_LABEL'];?></span>

                            <input type="text" name="gearInput" class="content-label" maxlength="30" maxlength="" placeholder="<?php echo $_LANG['NEWCAR_GEAR_PLACEHOLDER'];?>" />

                        </div>

                        <div class="input-prepend">

                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_TRANSMISSION_LABEL'];?></span>

                            <input type="text" name="transmissionInput" class="content-label" maxlength="150" placeholder="<?php echo $_LANG['NEWCAR_TRANSMISSION_PLACEHOLDER'];?>" />

                        </div>

                        <div class="input-prepend">

                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_FUEL_LABEL'];?></span>

                            <input type="text" name="fuelInput" class="content-label" maxlength="150" placeholder="<?php echo $_LANG['NEWCAR_FUEL_PLACEHOLDER'];?>" />

                        </div>

                        <div class="input-prepend">

                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_BODY_LABEL'];?></span>

                            <input type="text" name="bodyColorInput" class="content-label" maxlength="150" placeholder="<?php echo $_LANG['NEWCAR_BODY_PLACEHOLDER'];?>" />

                        </div>

                        <div class="input-prepend">

                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_OWNER_LABEL'];?></span>

                            <input type="text" name="nrOwnerInput" class="content-label" maxlength="3" placeholder="<?php echo $_LANG['NEWCAR_OWNER_PLACEHOLDER'];?>" />

                        </div>

                        <div class="input-prepend">

                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_LAST_SERVICE_LABEL'];?></span>

                            <input type="text" name="lastServiceInput" class="content-label" maxlength="100" placeholder="<?php echo $_LANG['NEWCAR_LAST_SERVICE_PLACEHOLDER'];?>" />

                        </div>

                        <div class="input-prepend">

                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_MOT_LABEL'];?></span>

                            <input type="text" name="motInput" class="content-label" maxlength="30" placeholder="<?php echo $_LANG['NEWCAR_MOT_PLACEHOLDER'];?>" />

                        </div>

                        <div class="input-prepend">

                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_TAX_LABEL'];?></span>

                            <input type="text" name="taxInput" class="content-label" maxlength="200" placeholder="<?php echo $_LANG['NEWCAR_TAX_PLACEHOLDER'];?>" />

                        </div>

                        <div class="input-prepend">

                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_TOP_SPEED_LABEL'];?></span>

                            <input type="text" name="topSpeedInput" class="content-label" maxlength="30" placeholder="<?php echo $_LANG['NEWCAR_TOP_SPEED_PLACEHOLDER'];?>" />

                        </div>

                        <div class="input-prepend">

                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_TORQUE_LABEL'];?></span>

                            <input type="text" name="torqueInput" class="content-label" maxlength="5" placeholder="<?php echo $_LANG['NEWCAR_TORQUE_PLACEHOLDER'];?>" />

                        </div>

                        <div class="input-prepend">

                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_KW_LABEL'];?></span>

                            <input type="text" name="kwInput" class="content-label" maxlength="5" placeholder="<?php echo $_LANG['NEWCAR_KW_PLACEHOLDER'];?>" />

                        </div>

                        <div class="input-prepend">

                            <h4><?php echo $_LANG['NEWCAR_HTML_HEADER'];?></h4>

                            <textarea id="_ebody" name="htmlInput"></textarea>

                        </div>

                    </div>

                    <div class="clearfix"></div>

                    <div id="content4">

                        <h4><?php echo $_LANG['NEWCAR_UPLOAD_MEDIA_HEADER'];?></h4>

                        <div class="input-prepend input-append inputap">

                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_OPTION_LABEL'];?></span>

                            <select id="opt1" onchange="validateImageType()" style="width:274px;">

                                <option value="none"><?php echo $_LANG['NEWCAR_SELECT_OPTION_SELECT'];?></option>

                                <option value="image"><?php echo $_LANG['NEWCAR_IMAGE_SELECT'];?></option>

                                <option value="video"><?php echo $_LANG['NEWCAR_VIDEO_SELECT'];?></option>

                            </select>

                            <select id="opt2" onchange="displayController('<?php echo BASE_RELATIVE;?>view/3rdparty/file_uploader/admin-addpic.php?cid=<?php echo $carid;?>')" style="width:274px;" disabled="disabled">

                                <option value="none"><?php echo $_LANG['NEWCAR_SELECT_OPTION_SELECT'];?></option>

                                <option value="interior"><?php echo $_LANG['NEWCAR_INTERIOR_SELECT'];?></option>

                                <option value="exterior"><?php echo $_LANG['NEWCAR_EXTERIOR_SELECT'];?></option>

                            </select>

                        </div>

                        <div id="video-wrap" style="display:none;" class="input-prepend input-append inputap">

                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_YOUTUBE_URL_LABEL'];?></span>

                            <input id="videoUrlInput" type="text" class="content-label" style="width:435px;" placeholder="<?php echo $_LANG['NEWCAR_YOUTUBE_URL_PLACEHOLDER'];?>" />

                            <input id="vidBtn" onclick="saveVideoHandler('<?php echo $carid;?>');return false;" type="submit" class="btn btn-info" style="width:100px;" value="<?php echo $_LANG['NEWCAR_SAVE_BUTTON'];?>" />

                        </div>

                        <div id="upload-wrap" style="display:none;" class="input-prepend input-append inputap">

                            <h4><?php echo $_LANG['NEWCAR_UPLOAD_LABEL'];?></h4>

                            <iframe id="iupload" width="700" height="100" style="border:0;"></iframe>

                        </div>

                    </div>

                    <br>

                    <div id="outputwrap"></div>

                    <div id="outoutimg"></div>

                    <div class="clearfix"></div>

                    <br>

                    <a id="prevbtn" href="#" onclick="return previousFormHandler()" class="btn btn-small" style="display:none;margin-right:10px;">

                        <i class="icon-circle-arrow-left" style="margin-right:10px;"></i><?php echo $_LANG['NEWCAR_PREVIOUS_BUTTON_LABEL'];?>

                    </a>

                    <a id="nextbtn" href="#" onclick="return nextFormHandler()" class="btn btn-small" style="margin-right:10px;">

                        <?php echo $_LANG['NEWCAR_NEXT_BUTTON_LABEL'];?><i class="icon-circle-arrow-right" style="margin-left:10px;"></i>

                    </a>

                    <input id="submitbtn" style="display:none;" type="submit" name="savebtn" class="btn btn-small btn-info" value="<?php echo $_LANG['NEWCAR_SAVE_BUTTON'];?>" />

                </form>

                <!-- END CONTENT -->

            </div>

            <div class="clearfix"></div>

        </div>

        <script language="javascript" type="text/javascript">

            /* preview uploaded image */

            function carImageUploaded(url,mode){

                        var did = 'img' + Math.round((Math.random()*1000));

                        var wrap = document.getElementById('outoutimg');

                        var html = '<div id="'+did+'" class="img-wrap">';

                        html += '<img src="<?php echo BASE_RELATIVE;?>'+url+'" width="200" height="150" style="margin:5px;" />';

                        html += '<p style="margin:0;padding-left:5px;">';

                        html += ucfirst(mode);

                        html += '<a href="#" onclick="removeImageHandler(\'<?php echo BASE_ROOT;?>'+url+'\',\''+did+'\');return false;" class="btn btn-small btn-danger" style="float:right;margin-right:5px;">×</a>';

                        html += '</p>';

                        html += '</div>';

                        

                        wrap.innerHTML += html;

                    }

                    function ucfirst (str) {

                        str += '';

                        var f = str.charAt(0).toUpperCase();

                        return f + str.substr(1);

                   }

        </script>

<?php

} // end validation.

else

{

?>

        <p class="text-warning"><?php echo $_LOCAL['UNAUTHORIZED_ACCESS_MESSAGE'];?></p>

<?php

}