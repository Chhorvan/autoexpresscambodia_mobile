<?php
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';

$cid = '';
    if(isset($_GET['cid'])) $cid = $_GET['cid'];
$vehicle_detail = $_CLASS->getVehicleDetail();
$vehicle_detailimg = $_CLASS->getVehicleDetailimg();
$Gallery_List=$_CLASS->getVehicleGallery();
$related_post=$_CLASS->getRelatedPost();

?>

<body id="top" class="page-id-home">

<div id="homeContainer">
	<div class="container clearfix">		
	    	<div id="homeSidebar">
				<div id="bgHomeSidebar">					
					<div class="bgBox" style="display:none;">
						<div class="divTitle">
							<img src="<?php echo BASE_RELATIVE.'img/public_home/img_01.png'; ?>" />
							<span>Maker</span>
						</div>
					</div>
					
				</div>
	        </div>
            <!-- #homeSidebar -->
	        <div id="homeLeftSide" class="clearfix">
				<div class="product_wrapper">
					
					
					<div class="title_product"><?php echo $vehicle_detail[0]['year']." ". $vehicle_detail[0]['make']." ".$vehicle_detail[0]['model'] ?></div>
					
					<!-- Vehicle Detail Show -->
					<div class="image_product" style="position:relative">												
						<img id="loader" style="visibility:hidden;position:absolute;z-index:20;margin:auto;right:0;left:0;top:0;bottom:0" src='images/loading2.gif' align='center'>
						
						<!-- Show Default Images -->		
						<?php if($_CLASS->no_photo=="Noimage"){ ?>
							<img width="100%" src="http://angkorauto.com/img/public_home/no_image_big.png" class="imgchange"/>	
						<?php }?>	
						
						<!-- Show Main Vehicle Images -->
						<div class="picture-slides-container">
							<?php
								$i = 0;
								foreach ($Gallery_List as $lst) {									
									//Reserved car
									$reserved_item = $_CLASS->loadReserve($lst['car_id']);
									//Sold car
									$sold_item = $_CLASS->loadSold($lst['car_id']);
									
								
							?>
							<?php  $info = new SplFileInfo($lst['photo_url']); ?>
							<div class="picture-slides-fade-container">
								<a class="picture-slides-image-link">
									<span class="picture-slides-image-load-fail">The image loading...</span>									
									<img class="picture-slides-image img-responsive" src="http://angkorauto.com/image/upload/crop-image.php?onthefly=<?php echo  $info->getFilename();?>&resize=490,392" style="position:relative;width:100%;height:auto;"/>
									
									<?php if( $sold_item ) { ?>
										<img src="<?php echo BASE_RELATIVE;?>images/sold.png" style="position:absolute; top:0px; right:0; z-index:9; width:120px; height:120px" />
									<?php }?>

									<?php if( $reserved_item ) { ?>
										<img src="<?php echo BASE_RELATIVE;?>images/reserved.png" style="position:absolute; top:0px; right:0; z-index:9; width:120px; height:120px" />
									<?php }?>
								</a>
							</div>
							<?php break; $i++; } ?>							
							
							<!-- Show Thumbnails Images -->
							<ul class="picture-slides-thumbnails clearfix">
								<?php
								   $i=0;
									foreach($Gallery_List as $lst){

										$info = new SplFileInfo($lst['thumb_url']);  
										$i++;
								?>	
								<li>
									<a id="thumbImage" href="<?php echo $lst['photo_url'];?>">
									<img class="smallimg" src="<?php echo $lst['thumb_url']; ?>" title="<?php echo $lst['thumb_url']; ?>" width="115" height="80" /></a>
								</li>								
								<?php } ?>
							</ul>
						</div>	
                    </div><!-- .image_product -->
					
                    <div class="product_detail">
                    	<div class="description"><?php echo $_LANG['PUBLIC_VEHICLE_DESCRIPTION'];?></div>
                        <div class="text_detail">
                        	 
                            <div class="row">
                            <span style="float:left"><?php echo $_LANG['PUBLIC_VEHICLE_DETAIL_STOCK'];?>:</span> <span style="float:right">
                            	<?php echo $_GET['cid'];?>
                            </span>
                            </div>                            
							<div class="row">
                        	<span style="float:left">Condition:</span> <span style="float:right">
								<?php if($vehicle_detail[0]['car_type']=="0"){echo "Used";}?>
								<?php if($vehicle_detail[0]['car_type']=="1"){echo "New";}?>
                            </span>
							</div>
                            <div class="row">
                        	<span style="float:left"><?php echo $_LANG['PUBLIC_VEHICLE_DETAIL_COLOR'];?>:</span> <span style="float:right">
                            <?php echo $vehicle_detail[0]['exterior_color'];?>
                            </span>
                            </div>
                            <div class="row">
                        	<span style="float:left"><?php echo $_LANG['PUBLIC_VEHICLE_DETAIL_ANY_MAKER_OPTION'];?>:</span> <span style="float:right">
                            <?php echo $vehicle_detail[0]['make'];?>
                            </span>
                            </div>
                            <div class="row">
                        	<span style="float:left"><?php echo $_LANG['PUBLIC_VEHICLE_DETAIL_ANY_MODEL_OPTION'];?>:</span> <span style="float:right">
                            <?php echo $vehicle_detail[0]['model'];?>
                            </span>
                            </div>
                            <div class="row">
                        	<span style="float:left"><?php echo $_LANG['PUBLIC_VEHICLE_DETAIL_MODEL_YEAR'];?>:</span> <span style="float:right">
                             <?php echo $vehicle_detail[0]['year'];?>
                            </span>
                            </div>
							 <div class="row">
							<span style="float:left"><?php echo $_LANG['PUBLIC_VEHICLE_DETAIL_LICENCE_PAPER'];?>:</span> <span style="float:right">
							 <?php if($vehicle_detail[0]['interior_color']=="0"){echo "Yes";}?>
							 <?php if($vehicle_detail[0]['interior_color']=="1"){echo "No";}?>
							</span>
							</DIV>
							 <div class="row">
                            
                            <span style="float:left"><?php echo $_LANG['PUBLIC_VEHICLE_DETAIL_LICENCE_PLATE'];?>:</span> <span style="float:right">
							 <?php if($vehicle_detail[0]['doors']=="1"){echo 'Yes';}?>
							  <?php if($vehicle_detail[0]['doors']=="0"){echo 'No';}?>	    
				
							</span>
							</DIV>
							
                            <div class="row">
                        	<span style="float:left"><?php echo $_LANG['PUBLIC_VEHICLE_DETAIL_BODY'];?>:</span> <span style="float:right">
                            <?php echo $vehicle_detail[0]['body_style'];?>
                            </span>
                            </div>
                            <div class="row">
                        	<span style="float:left"><?php echo $_LANG['PUBLIC_VEHICLE_DETAIL_ENGINE'];?>:</span> <span style="float:right">
                            <?php echo $vehicle_detail[0]['engine'];?>
                            </span>
                            </div>
                            <div class="row">
                        	<span style="float:left;"><?php echo $_LANG['PUBLIC_VEHICLE_DETAIL_STEERING'];?>:</span> <span style="float:right">
                            <?php  if( $vehicle_detail[0]['desc1']=="0"){ echo "Left-hand Drive";}?>
							<?php  if( $vehicle_detail[0]['desc1']=="1"){ echo "Right-hand Drive";}?>
                            </span>
                            </div>
                            <div class="row">
                        	<span style="float:left"><?php echo $_LANG['PUBLIC_VEHICLE_DETAIL_TRANSMISSION'];?>:</span> <span style="float:right">
                            <?php echo $vehicle_detail[0]['transmission'];?>
                            </span>
                            </div>
                            <div class="row">
                        	<span style="float:left"><?php echo $_LANG['PUBLIC_VEHICLE_DETAIL_FUEL_TYPE'];?>:</span> <span style="float:right">
                            <?php echo $vehicle_detail[0]['fuel_type'];?>
                            </span>
                            </div>                            
                            <div class="row">
                        	<span style="float:left"><?php echo $_LANG['PUBLIC_VEHICLE_DRIVE'];?>:</span> <span style="float:right"><?php echo $vehicle_detail[0]['drivetrain'];?></span>
                            </div>
							<div class="row">
								<span style="float:left"><?php echo $_LANG['PUBLIC_VEHICLE_DETAIL_LOCATION'];?>:</span> <span style="float:right">
									<?php
										if($vehicle_detail[0]['city']!= ""){
											echo $vehicle_detail[0]['city'];
										}else{
											echo 'Phnom Penh';
										}
									?>								
								</span>
                            </div>
                            <div style="margin:20px 0 50px 0; text-align:center;">
                        		<span style="color:red; font-size:14px;">
                                	  <?php 
												if(!empty($vehicle_detail[0]['price'])){
													  echo "Price : ".number_format($vehicle_detail[0]['price'])." $"."</b>";
												}else{
													  echo"Ask for Price";
												}
									  
									  ?>	
								
                                </span>
                            </div>
                        </div>
                    </div>
                    <div style="clear:both;"></div>
                        
						
                         <div class="company_info">							
                        	<div class="seller" style="text-transform:capitalize"><?php 
								if(!empty($vehicle_detail[0]['company_name'])){
									echo $vehicle_detail[0]['company_name'];	
								}else if(!empty($vehicle_detail[0]['seller_name'])){
									echo $vehicle_detail[0]['seller_name'];	
								}								
							?></div>
							
                            <div class="company_detail">
                            	<div class="info">
                                <p><?php echo $_LANG['PUBLIC_VEHICLE_DETAIL_MOBILE'];?>: <?php 
									if(!empty($vehicle_detail[0]['mobile'])){
										echo "<a href='tel:".$vehicle_detail[0]['mobile']."'>".$vehicle_detail[0]['mobile']."</a>";
									}else if(!empty($vehicle_detail[0]['contact_no'])){
										echo "<a href='tel:".$vehicle_detail[0]['contact_no']."'>".$vehicle_detail[0]['contact_no']."</a>";
									}									
								?></p>
                                <p><?php echo $_LANG['PUBLIC_VEHICLE_DETAIL_EMAIL'];?>: <?php
									// split string
									$vedetail_mail=$vehicle_detail[0]['email'];
									$befor_text=current(explode("@",$vedetail_mail));											
									$array_mail=(explode("@",$vedetail_mail,2));
									$array_mail=$array_mail[1];
									$after_text=current(explode(".",$array_mail));
									?><script>mail2("<?php echo $befor_text;?>","<?php echo $after_text;?>",0,"","<?php echo $vedetail_mail;?>")</script><?php
								?></p>
								<p><?php echo $_LANG['PUBLIC_VEHICLE_DETAIL_LOCATION'];?>:  <?php echo $vehicle_detail[0]['province'];?></p>
                                <!--<p><?php echo $_LANG['PUBLIC_VEHICLE_DETAIL_CONTACT'];?>: <?php echo $vehicle_detail[0]['name'];?></p>-->                                
                                </div>
                                <span class="contact_seller">
                                <div><a href="<?php echo BASE_RELATIVE;?>company-detail/<?php echo $_GET['owner']?>/<?php echo $_GET['cid']?>">
                                	<img src="<?php echo BASE_RELATIVE;?>images/home_new/view_profile.png" /></a></div>
                                <div><a id="contact_popup" style="cursor:pointer;">
                                	<img src="<?php echo BASE_RELATIVE;?>images/home_new/contact_seller.png" />
                                </a></div>
                                <div id="content_pop">
                                    <div class="popbox_contact" id="popbox">
                                    	<form method="post">
										  
											<input type="email" name="email" class="text_email" placeholder="Company Email" required/>
						
                                            <textarea name="message" class="text_message" placeholder="Your Message" required></textarea>
                                            <a href="<?php echo BASE_RELATIVE."email-sent?cid=".$vehicle_detail[0]['car_id']?>"><input type="submit" class="sends" name="btn_send" value=""></a>
                                        </form>
                                    </div>
                               	</div>
                                </span>
                                <p style="clear:both;"></p>
                            </div>
                        </div>
                    </div>
                    
                </div>
			</div>
            <!-- End Slide -->
						</div>
					</div>
					
				</div><!-- .product_wrapper -->	
			
	        </div><!-- #homeLeftSide -->    	
    </div><!-- .container -->
	

</div><!-- #homeContainer -->

<!-- Import Script -->
<script src="<?php echo BASE_RELATIVE; ?>autocam_js/public_car_detail/PictureSlides-jquery-2.0.js"></script> <!-- Gallery Slide -->
<script src="<?php echo BASE_RELATIVE; ?>autocam_js/public_car_detail/vdetail.js"></script>