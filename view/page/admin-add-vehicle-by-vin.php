<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.gg
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';
/*
 * VALIDATE ADMINISTRATOR GROUP ------------------------------------------------
 */
if( $_SESSION['log_group'] == 'admin' )
{
    // get form feed.
    $fstatus = $_CLASS->getFormStatus();
    $fmessage = $_CLASS->getFormMessage();
    $fstyle = $_CLASS->getFormStyle();
    $save_status = $_CLASS->getSaveStatus();

?>
        <div id="content-wrapper">
            <div id="vertical-menu">
                <?php $_MENU->loadMenu($_GLANG); ?>
            </div>
            <!-- scroll to top button -->
            <div id="top-page" onclick="javascript: $('html, body').animate({scrollTop: '0px'}, 300);"></div>
            <!-- end scroll to top button -->
            <div id="inner-content">
                <!-- START CONTENT -->
                <h1><?php echo $_LANG['ADD_HEADER'];?></h1>
                <?php
                    if( $fstatus ){
                ?>
                <div class="alert <?php echo $fstyle;?>">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <?php echo $fmessage;?>
                </div>
                <?php
                    }

                    if( isset($_SESSION['form_car']) || isset($_POST['saveBtn']) ){
                        if( !$save_status ){
                ?>

                <form method="post">
                    <h3 class="header-title"><?php echo $_LANG['ADD_VEHICLE_INFO_HEADER'];?></h3>
                    <div class="input-prepend">
                        <span class="add-on" style="width:150px;"><?php echo $_LANG['ADD_MAKER_LABEL'];?></span>
                        <input type="text" name="makerInput" value="<?php echo ( isset($_SESSION['form_car']['maker']) ? $_SESSION['form_car']['maker'] : '' );?>" style="width:555px;"/>
                    </div>
                    <div class="input-prepend">
                        <span class="add-on" style="width:150px;"><?php echo $_LANG['ADD_MODEL_LABEL'];?></span>
                        <input type="text" name="modelInput" value="<?php echo ( isset($_SESSION['form_car']['model']) ? $_SESSION['form_car']['model'] : '' );?>" style="width:555px;"/>
                    </div>
                    <div class="input-prepend">
                        <span class="add-on" style="width:150px;"><?php echo $_LANG['ADD_YEAR_LABEL'];?></span>
                        <input type="text" name="yearInput" value="<?php echo ( isset($_SESSION['form_car']['year']) ? $_SESSION['form_car']['year'] : '' );?>" style="width:555px;"/>
                    </div>
                    <div class="input-prepend">
                        <span class="add-on" style="width:150px;"><?php echo $_LANG['ADD_BODY_LABEL'];?></span>
                        <input type="text" name="bodyInput" value="<?php echo ( isset($_SESSION['form_car']['body_type']) ? $_SESSION['form_car']['body_type'] : '' );?>" style="width:555px;"/>
                    </div>
                    <div class="input-prepend">
                        <span class="add-on" style="width:150px;"><?php echo $_LANG['ADD_PRICE_LABEL'];?></span>
                        <input type="text" name="priceInput" value="<?php echo ( isset($_SESSION['form_car']['price_base']) ? $_SESSION['form_car']['price_base'] : '0' );?>" style="width:555px;"/>
                    </div>
                    <div class="input-prepend">
                        <span class="add-on" style="width:150px;"><?php echo $_LANG['ADD_DOORS_LABEL'];?></span>
                        <input type="text" name="doorInput" value="<?php echo ( isset($_SESSION['form_car']['doors']) ? $_SESSION['form_car']['doors'] : '' );?>" style="width:555px;"/>
                    </div>
                    <div class="input-prepend">
                        <span class="add-on" style="width:150px;"><?php echo $_LANG['ADD_MILES_LABEL'];?></span>
                        <input type="text" name="milesInput" value="0" style="width:555px;"/>
                    </div>
                    <div class="input-prepend">
                        <span class="add-on" style="width:150px;"><?php echo $_LANG['ADD_MARK_FEATURED_LABEL'];?></span>
                        <select name="featuredInput" style="width:564px;">
                            <option value="0"><?php echo $_LANG['ADD_OPTION_FEATURED_NO'];?></option>
                            <option value="1"><?php echo $_LANG['ADD_OPTION_FEATURED_YES'];?></option>
                        </select>
                    </div>
                    <div class="input-prepend">
                        <span class="add-on" style="width:150px;"><?php echo $_LANG['ADD_ENERGY_LABEL'];?></span>
                        <select name="ecoInput" style="width:564px;">
                            <option value="A">A</option>
                            <option value="B">B</option>
                            <option value="C">C</option>
                            <option value="D">D</option>
                            <option value="E">E</option>
                            <option value="F">F</option>
                        </select>
                    </div>

                    <h3 class="header-title"><?php echo $_LANG['ADD_DETAILS_HEADER'];?></h3>
                    <div class="input-prepend">
                        <span class="add-on" style="width:150px;"><?php echo $_LANG['ADD_ENGINE_SIZE_LABEL'];?></span>
                        <input type="text" name="engineSizeInput" value="<?php echo ( isset($_SESSION['form_car']['engine_size']) ? $_SESSION['form_car']['engine_size'] : '' );?>" style="width:555px;"/>
                    </div>
                    <div class="input-prepend">
                        <span class="add-on" style="width:150px;"><?php echo $_LANG['ADD_TRIM_LABEL'];?></span>
                        <input type="text" name="trimInput" value="<?php echo ( isset($_SESSION['form_car']['trim']) ? $_SESSION['form_car']['trim'] : '' );?>" style="width:555px;"/>
                    </div>
                    <div class="input-prepend">
                        <span class="add-on" style="width:150px;"><?php echo $_LANG['ADD_TYPE_LABEL'];?></span>
                        <input type="text" name="typeInput" value="<?php echo ( isset($_SESSION['form_car']['type']) ? $_SESSION['form_car']['type'] : '' );?>" style="width:555px;"/>
                    </div>
                    <div class="input-prepend">
                        <span class="add-on" style="width:150px;"><?php echo $_LANG['ADD_GEAR_LABEL'];?></span>
                        <input type="text" name="gearInput" value="<?php echo ( isset($_SESSION['form_car']['gear']) ? $_SESSION['form_car']['gear'] : '' );?>" style="width:555px;"/>
                    </div>
                    <div class="input-prepend">
                        <span class="add-on" style="width:150px;"><?php echo $_LANG['ADD_TRANSMISSION_LABEL'];?></span>
                        <input type="text" name="transmissionInput" value="<?php echo ( isset($_SESSION['form_car']['transmission']) ? $_SESSION['form_car']['transmission'] : '' );?>" style="width:555px;"/>
                    </div>
                    <div class="input-prepend">
                        <span class="add-on" style="width:150px;"><?php echo $_LANG['ADD_FUEL_LABEL'];?></span>
                        <input type="text" name="fuelInput" value="<?php echo ( isset($_SESSION['form_car']['fuel']) ? $_SESSION['form_car']['fuel'] : '' );?>" style="width:555px;"/>
                    </div>
                    <div class="input-prepend">
                        <span class="add-on" style="width:150px;"><?php echo $_LANG['ADD_BODY_COLOR_LABEL'];?></span>
                        <input type="text" name="bodyColorInput" style="width:555px;"/>
                    </div>
                    <div class="input-prepend">
                        <span class="add-on" style="width:150px;"><?php echo $_LANG['ADD_PREV_OWNERS_LABEL'];?></span>
                        <input type="text" name="prevOwnersInput" style="width:555px;"/>
                    </div>
                    <div class="input-prepend">
                        <span class="add-on" style="width:150px;"><?php echo $_LANG['ADD_LAST_SERVICE_LABEL'];?></span>
                        <input type="text" name="lastServiceInput" placeholder="YYYY-MM-DD" style="width:555px;"/>
                    </div>
                    <div class="input-prepend">
                        <span class="add-on" style="width:150px;"><?php echo $_LANG['ADD_MOT_LABEL'];?></span>
                        <input type="text" name="motInput" placeholder="YYY-MM-DD" style="width:555px;"/>
                    </div>
                    <div class="input-prepend">
                        <span class="add-on" style="width:150px;"><?php echo $_LANG['ADD_TAX_BAND_LABEL'];?></span>
                        <input type="text" name="taxBandInput" style="width:555px;"/>
                    </div>
                    <div class="input-prepend">
                        <span class="add-on" style="width:150px;"><?php echo $_LANG['ADD_TOP_SPEED_LABEL'];?></span>
                        <input type="text" name="topSpeedInput" style="width:555px;"/>
                    </div>
                    <div class="input-prepend">
                        <span class="add-on" style="width:150px;"><?php echo $_LANG['ADD_TORQUE_LABEL'];?></span>
                        <input type="text" name="torqueInput" style="width:555px;"/>
                    </div>
                    <div class="input-prepend">
                        <span class="add-on" style="width:150px;"><?php echo $_LANG['ADD_POWER_LABEL'];?></span>
                        <input type="text" name="powerInput" style="width:555px;"/>
                    </div>

                    <!-- featured panel -->
                    <h3 class="header-title"><?php echo $_LANG['ADD_FEATURE_HEADER'];?></h3>
                    <?php
                        $feat = $_CLASS->getFeatureList();

                        if( count($feat) < 1 ){
                    ?>
                    <p class="alert alert-info"><?php echo $_LANG['ADD_NO_FEATURE_LABEL'];?></p>
                    <?php
                        }
                        else {
                            ( isset($_SESSION['form_feature']) ? $feat_session = (array)$_SESSION['form_feature'] : $feat_session = array() );

                            $max = floor(count($feat) / 2);
                            $max_n = floor(count($feat_session) / 2);


                            // display col A.
                    ?>
                    <div>
                        <p><strong><?php echo $_LANG['ADD_NON_LISTED_FEATURES'];?></strong></p>
                        <?php
                            if( count($feat_session) < 1 ){
                        ?>
                        <p>--</p>
                        <?php
                            } else {

                                echo '<div class="coll-wrapper">' . "\r\n";
                                echo '  <ul class="col">' . "\r\n";
                                for( $i=0; $i < $max_n; $i++ ){
                                    echo '<li><i class="icon-asterisk"></i> &nbsp; '.$feat_session[$i].'</li>' . "\r\n";
                                }
                                echo '  </ul>' . "\r\n";
                                echo '  <ul class="col">' . "\r\n";
                                for( $i=$max_n; $i < count($feat_session); $i++ ){
                                    echo '<li><i class="icon-asterisk"></i> &nbsp; '.$feat_session[$i].'</li>' . "\r\n";
                                }
                                echo '  </ul>' . "\r\n";
                                echo '</div>' . "\r\n";
                                echo '<div class="clearfix"></div><br/>' . "\r\n";
                            }
                        ?>
                    </div>
                    <p><strong><?php echo $_LANG['ADD_EXTRA_FEATURES'];?></strong></p>
                    <div class="coll-wrapper">
                        <div class="col">
                            <?php
                                for( $i=0; $i < $max; $i++ ){
                                    $sel = '';

                                    for( $u=0; $u < count($feat_session); $u++ ){
                                        if( $feat_session[$u] == $feat[$i]['label'] ){
                                            $sel = 'checked="checked"';
                                        }
                                    }
                            ?>
                            <label>
                                <input type="checkbox" name="featInput[]" <?php echo $sel;?> value="<?php echo $feat[$i]['label'].':'.$feat[$i]['source'];?>" /> &nbsp; <?php echo $feat[$i]['label'];?>
                            </label>
                            <?php
                                }
                            ?>
                        </div>

                        <div class="col">
                            <?php
                                for( $i=$max; $i < count($feat); $i++ ){
                                    $sel = '';

                                    for( $u=0; $u < count($feat_session); $u++ ){
                                        if( $feat_session[$u] == $feat[$i]['label'] ){
                                            $sel = 'checked="checked"';
                                        }
                                    }
                            ?>
                            <label>
                                <input type="checkbox" name="feat[]" <?php echo $sel;?> value="<?php echo $feat[$i]['label'].':'.$feat[$i]['source'];?>" /> &nbsp; <?php echo $feat[$i]['label'];?>
                            </label>
                            <?php
                                }
                            ?>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <?php
                        }
                    ?>

                    <!-- media panel -->
                    <h3 class="header-title"><?php echo $_LANG['ADD_MEDIA_HEADER'];?></h3>
                    <p><?php echo $_LANG['ADD_MEDIA_PLACEHOLDER'];?></p>
                    <?php
                        ( isset($_SESSION['form_media']) ? $media_list = (array)$_SESSION['form_media'] : $media_list = array() );
                        $interior = $media_list['interior'];
                        $exterior = $media_list['exterior'];

                        if( count($interior) < 1 && count($exterior) < 1 ){
                    ?>
                    <p class="alert alert-info"><?php echo $_LANG['ADD_NO_MEDIA_MESSAGE'];?></p>
                    <?php
                        } else {
                            // display interior.
                            if( count($interior) > 0 ){
                                for( $i=0; $i < count($interior); $i++ ){
                    ?>
                    <img src="<?php echo $interior[$i];?>" class="img-polaroid" style="width:340px;float:left;margin-right:10px;margin-bottom:10px;" />
                    <?php
                                }
                            }

                            // display exterior.
                            if( count($exterior) > 0 ){
                                for( $i=0; $i < count($exterior); $i++ ){
                    ?>
                    <img src="<?php echo $exterior[$i];?>" class="img-polaroid" style="width:340px;float:left;margin-right:10px;margin-bottom:10px;" />
                    <?php
                                }
                            }
                        }
                    ?>
                    <div class="clearfix"></div>
                    <br/>
                    <input type="submit" name="saveBtn" class="btn btn-info" value="<?php echo $_LANG['ADD_SAVE_BUTTON'];?>"/>
                </form>

                <?php
                        } else {
                ?>
                <p><?php echo $_LANG['ADD_SUCCESS_LABEL'];?></p>
                <?php
                        }
                    } // end validation.
                    else {
                ?>
                <p class="alert alert-info"><?php echo $_LANG['ADD_INVALID_REQUEST_ERROR'];?></p>
                <?php
                    }
                ?>
                <!-- END CONTENT -->
            </div>
            <div class="clearfix"></div>
        </div>
<?php
} // end validation.
else
{
?>
        <p class="text-warning"><?php echo $_LOCAL['UNAUTHORIZED_ACCESS_MESSAGE'];?></p>
<?php
}
