<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';
/*
 * VALIDATE ADMINISTRATOR GROUP ------------------------------------------------
 */
if( $_SESSION['log_group'] == 'user' )
{
    // get form feed.
    $fstatus = $_CLASS->getFormStatus();
    $fmessage = $_CLASS->getFormMessage();
    $fstyle = $_CLASS->getFormStyle();
    
    // get reservation list.
    $list = $_CLASS->getReservationList();
?>
        <div id="content-wrapper">
            <div id="vertical-menu">
                <?php $_MENU->loadMenu($_GLANG); ?>
            </div>
            <div id="inner-content">
                <!-- START CONTENT -->
                <h1><?php echo $_LANG['RESERV_HEADER'];?></h1>
                <?php
                    if( $fstatus ){
                ?>
                <div class="alert <?php echo $fstyle;?>">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <?php echo $fmessage;?>
                </div>
                <?php
                    }
                    
                    // display reservation list.
                    if( count($list) < 1 ){
                ?>
                <div class="alert alert-info">
                    <?php echo $_LANG['RESERV_NORESULT_LABEL'];?>
                </div>
                <?php
                    }
                    else {
                        require_once BASE_CLASS . 'class-utilities.php';
                        for( $i=0; $i < count($list); $i++ ){
                            
                            // format date.
                            ( $_SESSION['log_language_iso'] == 'en' ?  $format = 'mmddyyyy' : $format = 'ddmmyyyy' );
                            $date = explode('-',$list[$i]['date']);
                            $day = $date[2];
                            $month = $date[1];
                            $year = $date[0];
                            
                            $car = $_CLASS->getCarById($list[$i]['car_id']);
                            
                            ( ($i%2) > 0 ? $bg = '#F7F7F7' : $bg = '#FFF' );
                ?>
                <div>
                    <div style="margin-bottom:5px;padding:3px;background:<?php echo $bg;?>;">
                        <h4>
                            <span style="display:block;float:left;width:150px;"><?php echo Utilities::checkDateFormat($year, $month,$day,$format);?></span>
                            <span style="display:block;float:left;width:150px;"><?php echo stripslashes($list[$i]['code']);?></span>
                            <span style="display:block;float:left;width:388px;"><?php echo stripslashes($list[$i]['name']);?></span>
                            <a href="#" onclick="displayReservationTab('info<?php echo $i;?>','ico<?php echo $i;?>');return false;" style="display:block;float:right;" class="btn btn-small"><i id="ico<?php echo $i;?>" class="icon-chevron-down"></i></a>
                        </h4>
                        <div class="clearfix"></div>
                    </div>
                    <div id="info<?php echo $i;?>" class="content-info">
                        <div class="input-prepend">
                            <span class="add-on" style="width:180px;"><?php echo $_LANG['RESERV_DATE_LABEL'];?></span>
                            <span class="add-on" style="background:#FFF;width:514px;text-align:left;"><?php echo Utilities::checkDateFormat($year, $month,$day,$format);?></span>
                        </div>
                        <div class="input-prepend">
                            <span class="add-on" style="width:180px;"><?php echo $_LANG['RESERV_VEHICLE_LABEL'];?></span>
                            <span class="add-on" style="background:#FFF;width:514px;text-align:left;"><?php echo $car['maker'] . ' ' . $car['model'] . ' ' . $car['year'];?></span>
                        </div>
                        <div class="input-prepend">
                            <span class="add-on" style="width:180px;"><?php echo $_LANG['RESERV_PRICE_LABEL'];?></span>
                            <span class="add-on" style="background:#FFF;width:514px;text-align:left;"><?php echo CURRENCY_SYMBOL . Utilities::formatPrice($car['price'],CURRENCY_CODE);?></span>
                        </div>
                        <div class="input-prepend">
                            <span class="add-on" style="width:180px;"><?php echo $_LANG['RESERV_RESERVED_FOR_LABEL'];?></span>
                            <span class="add-on" style="background:#FFF;width:514px;text-align:left;"><?php echo stripslashes($list[$i]['name']);?></span>
                        </div>
                        <div class="input-prepend">
                            <span class="add-on" style="width:180px;"><?php echo $_LANG['RESERV_EMAIL_LABEL'];?></span>
                            <span class="add-on" style="background:#FFF;width:514px;text-align:left;"><?php echo $list[$i]['email'];?></span>
                        </div>
                        <div class="input-prepend">
                            <span class="add-on" style="width:180px;"><?php echo $_LANG['RESERV_PHONE_LABEL'];?></span>
                            <span class="add-on" style="background:#FFF;width:514px;text-align:left;"><?php echo $list[$i]['phone'];?></span>
                        </div>
                        <div class="input-prepend">
                            <span class="add-on" style="width:180px;"><?php echo $_LANG['RESERV_ADDRESS_LABEL'];?></span>
                            <span class="add-on" style="background:#FFF;width:514px;text-align:left;"><?php echo $list[$i]['address'];?></span>
                        </div>
                        <div class="input-prepend">
                            <span class="add-on" style="width:180px;"><?php echo $_LANG['RESERV_CITY_LABEL'];?></span>
                            <span class="add-on" style="background:#FFF;width:514px;text-align:left;"><?php echo $list[$i]['city'];?></span>
                        </div>
                        <div class="input-prepend">
                            <span class="add-on" style="width:180px;"><?php echo $_LANG['RESERV_ZIP_LABEL'];?></span>
                            <span class="add-on" style="background:#FFF;width:514px;text-align:left;"><?php echo $list[$i]['zip'];?></span>
                        </div>
                        <div class="input-prepend">
                            <span class="add-on" style="width:180px;"><?php echo $_LANG['RESERV_COUNTRY_LABEL'];?></span>
                            <span class="add-on" style="background:#FFF;width:514px;text-align:left;"><?php echo $list[$i]['country'];?></span>
                        </div>
                        <div class="input-prepend">
                            <span class="add-on" style="width:180px;"><?php echo $_LANG['RESERV_CODE_LABEL'];?></span>
                            <span class="add-on" style="background:#FFF;width:514px;text-align:left;"><?php echo $list[$i]['code'];?></span>
                        </div>
                        <div class="input-prepend">
                            <span class="add-on" style="width:180px;"><?php echo $_LANG['RESERV_IP_LABEL'];?></span>
                            <span class="add-on" style="background:#FFF;width:514px;text-align:left;"><?php echo $list[$i]['ip'];?></span>
                        </div>
                        <form method="post" style="margin:0;text-align:right;">
                            <input type="hidden" name="cidInput" value="<?php echo $list[$i]['car_id'];?>" />
                            <input type="hidden" name="ridInput" value="<?php echo $list[$i]['id'];?>" />
                            <input onclick="return confirm('<?php echo $_LANG['RESERV_REMOVE_CONFIRM_LABEL'];?>');" type="submit" name="removebtn" value="<?php echo $_LANG['RESERV_REMOVE_BUTTON'];?>" class="btn btn-small btn-danger" />&nbsp;
                            
                        </form>
                    </div>
                </div>
                <?php
                        }
                    }
                ?>
                <!-- END CONTENT -->
            </div>
            <div class="clearfix"></div>
        </div>
<?php
} // end validation.
else
{
?>
        <p class="text-warning"><?php echo $_LOCAL['UNAUTHORIZED_ACCESS_MESSAGE'];?></p>
<?php
}