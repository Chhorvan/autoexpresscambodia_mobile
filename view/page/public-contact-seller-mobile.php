
<?php

if(!isset($_SESSION)) @session_start();
	$stock_detail = $_CLASS->stock_detail;
    $result=$_CLASS->result;
    if(isset($_GET['result'])) $result=htmlspecialchars($_GET['result']);
    //var_dump($_SESSION);
    //if(isset($_SESSION['log_group'])) $log_group=$_SESSION['log_group']; $log_group='';

    if($_SESSION['log_group']=='user'||$_SESSION['log_group']=='admin'){
        $login_popup="#";
        $button_id="action_submit";
    }else{
        $login_popup="#popup_area";
        $button_id="";
    }

    //var_dump($_SESSION);
?>


<div class="main-div">

    <div class="page-title">
		<table><tr><td><img class="brick" src="<?php echo BASE_RELATIVE;?>images/logout-icon/pop-up-sign-up.png" /></td><td>Contact Seller</td></tr></table>
	</div>

    <div id="content-seller">
        <?php if(empty($result)){ ?>
        <form method="post" id="form_message">
    	<div id="sub-content">
        	<input type="hidden" value="<?php if(isset($_GET['cid'])) echo htmlspecialchars($_GET['cid']); ?>" name="product_id"/>
            	<table width="100%">
                	<tr>
                    	<th id="label">Form :</th>
                        <?php
							$disabled;
                        	if($_SESSION['log_id']>0){
								$disabled="disabled";
							}else
								$disabled="";
						?>
                   		<td>
                        <input type="text" name="emailInput" id="txtemail" class="textbox" value="<?php echo $_SESSION['log_email'];?>" placeholder="email address" onblur="validateEmail(this);" <?php echo $disabled?>/>
                        </td>
                    </tr>

                    
                    <tr>
                    	<th id="label">To :</th>
                        <td>
                        <label class="label-sell"><?php echo $stock_detail[0]['company_name']?></label>
                        </td>
                    </tr>
                    <tr>
                    	<th></th>
                        <td>
                        	<div class="detail">
                                <hr class="line"/>
                                <a target="_blank" href="vehicle-detail?cid=<?php echo $stock_detail[0]['id']?>"><img class="image" src="<?php echo $stock_detail[0]['thumb']?>"></a>
                                <span id="content-text">
                                    <table>
                                        <tr>
                                            <td class="text-detail">Stock Id : <?php echo $stock_detail[0]['id']?></td>
                                        </tr>
                                        <tr>
                                            <td class="text-detail">Steering : <?php echo $stock_detail[0]['steering']?></td>
                                        </tr>
                                        <tr>
                                            <td class="text-detail">
                                            <img width="15" height="12" src="<?php echo BASE_RELATIVE;?>images/flag/<?php echo $stock_detail[0]['flag']?>.png"/>
                                            <?php echo $stock_detail[0]['country']?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-detail">Seller : <?php echo "<span class='company'>". $stock_detail[0]['company_name']."</span>"?></td>
                                        </tr>
                                        <tr>
                                            <td style="color:red;" class="text-detail">FOB : <?php echo $stock_detail[0]['currency']?> <?php echo $stock_detail[0]['price']?></td>
                                        </tr>
                                        <tr>

                                            <td style="color:blue;" class="text-detail" id="click"><a target="_blank" href="vehicle-detail?cid="><font color="blue">More Detail</font></a></td>

                                        </tr>
                                    </table>
                                </span>
                                <p class="clear"></p>
                           </div>
                        </td>
                    </tr>
                	<tr>
                    	<th id="label">Subject :</th>
                        <td>
                        <input type="text" id="subjectInput" name="subjectInput" class="textbox" value="I am interested in your" />
                    </tr>
                    <tr>
                    	<th id="label" valign="top">Message :</th>
                        <td>

                        <textarea dropzone="txtmessage" name="messageInput" id="txtarea" class="textarea"></textarea>

                        </td>
                    </tr>

                    <tr>
                    	<td></td>
                    	<td align="right">
							<?php
                            if($_SESSION['log_id']>0){ ?>
                            <input type="submit" value="Send" name="submit" class="hover" id="submit_send"/>
<?php }else{ ?>
                                             <a href="<?php echo $login_popup;?>" id="a-link" rel="facebox"><img class="btn-send" src="<?php echo BASE_RELATIVE;?>images/logout-icon/Contact-Seller.png" /></a>

                       <?php } ?>
                        </td>
                    </tr>

                </table>
        </form>

        
                <div id="popup_area" style="display:none;">
            <div id="tabs-container">
                <ul class="tabs-menu">
                    <li id="tab-1"><a href="javascript:void(0)">SignIn</a></li>
                    <li id="tab-2"><a href="javascript:void(0)">SignUp</a></li>
                </ul>
                <div class="tab">
                    <div class="tab-1 tab-content">
                        <div class="content-area">
                            <form action="contact-seller" method="POST" name="loginform" id="loginform">
                                <p class="label">ID<label class="star">*</label></p>
                                <!-- <input id="tab-username" type="text" name="emailInput" class="text" value="4-12 characters" /> -->
                                <input type="text" id="username" class="user_name" name="emailInput" value="ID...">
                                <p class="label">Password<label class="star">*</label></p>
                                <!-- <input id="tab-pass" type="text" name="passwordInput" class="text" value="Password" /> -->
                                <input type="password" id="pass" class="password" value="Password" name="passwordInput"><br />
                                <table cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <label><input type="checkbox" name="remember" class="check" /> Remember Me</label>
                                            <p><a href="#">Forget your password</a></p>
                                        </td>
                                        <td><div id="sign_in">
                                    <input type="button" id="sign_inbtn_new" class="sign_in linkfade"  value="" name="loginBtn">
                                    <input type="hidden" value="<?php if(isset($_GET['cid'])) echo htmlspecialchars($_GET['cid']); ?>" name="product_id"/>
                                     <input type="hidden" id="subjectInput" name="subjectInput" class="textbox" value="I am interested in your  <?php echo $stock_detail[0]['model_year'].'  '.   $stock_detail[0]['make'].'  '.$stock_detail[0]['model']?>"/>
                                      <textarea  style="display: none;"  dropzone="txtmessage" name="messageInput" id="get" class="textarea"></textarea>



                                </div></td>
                                    </tr>
                                </table>
                            </form>

                        </div>

                        <ul>
                            <li><a href="https://motorbb.com/social_network/address/signin/index.php?route=authentications/authenticatewith/facebook" class="facebook_login"><img  src="<?php echo BASE_RELATIVE;?>images/contact-seller/bt-fb.png" alt="" /></a></li>
                           <!-- <li><a href="#"><img src="<?php //echo BASE_RELATIVE;?>images/contact-seller/bt-tw.png" alt="" /></a></li>
                            <li><a href="#"><img src="<?php //echo BASE_RELATIVE;?>images/contact-seller/bt-g.png" alt="" /></a></li>
                            <li><a href="#"><img src="<?php //echo BASE_RELATIVE;?>images/contact-seller/bt-ld.png" alt="" /></a></li>-->
                        </ul>
                    </div>
                    <div class="tab-2 tab-content">
                        <div class="content-area">
                            <form action="" method="POST">
                                <p class="label">ID<label class="star">*</label></p>
                                <input type="text" name="username" class="tab-username text" required value="4-12 characters" />
                                <p class="label">Password<label class="star">*</label></p>
                                <input type="text" name="paswword" required class="tab-pass text" value="Password" />
                                <p class="label">Confirm Password<label class="star">*</label></p>
                                <input type="text" name="cpaswword" required class="tab-cpass text" value="Enter your password again" />
                                <p class="label">Company Name<label class="star">*</label></p>
                                <input type="text" name="cname" required class="text" value="" />
                                <p class="label">Country<label class="star">*</label></p>
                                <select>
                                    <option value="">country</option>
                                    <option value="Cambodia">Cambodia</option>
                                    <option value="Korea">Korea</option>
                                    <option value="Japan">Japan</option>


                                </select>
                                <p class="label">Security Question<label class="star">*</label></p>
                                <table cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td><p>How much is: 4 + 6?</p></td>
                                        <td><input type="text" name="cname" required class="text short-text" value="" /></td>
                                    </tr>
                                </table>


                                <input type="image" class="submit" name="signup" src="<?php echo BASE_RELATIVE;?>images/contact-seller/bt-sign.png" />

                            </form>
                    </div>
                </div>
            </div>
   		 </div>
     </div>
      <?php }elseif($result=="success"){ ?>
            <div style="padding: 100px 0;text-align: center; color:green;">
                <img src="images/tick.png" style="padding-right:10px;">
                The inquiry has been sent!
            </div>
        <?php } ?>
     </div>
      <div class="clear"></div>
     </div>
   </div>  
      <script>
$(document).ready(function() {
$('#txtarea').change(function(event) {

var get=$('#txtarea').val();
$('#get').text(get);


    });

});
$(document).ready(function() {
$(document.body).on('click', "#sign_inbtn_new", function(e){
     $.ajax({
        url     : 'ajax/get-session.php',
        type    : 'POST',
        data    :  $('#facebox #loginform').serialize(),
        success : function(data){
         // console.log(data);
        if(data.data!=null){
            window.location.href="contact-seller" + data.pro;

        }else{

            $('#facebox #username').css({"color":"red"});
            $('#facebox #pass').css({"color":"red"});

        }


        },
        error: function(){
            console.log('error');


        }
    });

    });
});

</script>