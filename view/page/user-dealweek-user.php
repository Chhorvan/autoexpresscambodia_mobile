<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';
// get form feed.
$fstatus = $_CLASS->getFormStatus();
$fmessage = $_CLASS->getFormMessage();
$fstyle = $_CLASS->getFormStyle();
$group = 'public';
$slug  = 'car';
// load page content.
$pg_details = $_CLASS->getPageHTML();



if(isset($_POST['deal_search'])){
    $deal_week = $_CLASS->searchDealweek();   
}
else {
   $deal_week = $_CLASS->loadDealweek($_GET['name']);    
}  


?>
<?php 
    $pagination=$_CLASS->pagination_html;
?>

<link rel="stylesheet" href="<?php echo BASE_RELATIVE;?>css/community/community.css" />
<link rel="stylesheet" href="<?php echo BASE_RELATIVE;?>css/pagination.css" />
 <div id="sectionContenWrapper">
        	<!--sectionSidebar--> 
            <div id="topMenu">
            	<p><a href="#" class="linkfade">Community</a> > Deal of the Week</p>
            </div>
        	<?php include("php/sidebar/community.php");?>
            
            </div>           
            <div id="sectionContent"> 
            	<div id="community_title">
                	<p>Deal of the Week</p>
                </div>
                <div id="auto_detail" class="clearfix">
                	<div id="text">
                   		<p class="first">Get all the information on all kinds of vehicles here.</p>
                        <p>This is a section where you can post any kinds of information yourself and share with those in need!</p>
                    </div>
                    <div id="button">
                    	<div id="article">
                        	<a href="<?php echo BASE_RELATIVE;?>register-dealoftheweek"><img src="<?php echo BASE_RELATIVE;?>images/community/write.png"  class="linkfade"/></a>
                        </div>
                        
                    </div>
                </div>
                <div id="deal_week">
                	<form action="<?php echo BASE_RELATIVE;?>dealof-theweek" method ="post" >
                	
                    <div id="textbox_wrap" class="clearfix">
                    	<select class="topic" name="deal_option"><option>Topic</option><option>Writer</option></select>
                       
                        <input type="text" class="search" name="deal_text" />
                        <div id="btn_search">
                        	<input type="submit" class="deal_search" value="" name="deal_search">
                        </div>
                   	</div>
                    </form>                    
                	<div class="deal_detail clearfix">
                    	<?php $count=1; foreach($deal_week as $row){
                            $str = $row['date'];  
                            $i = strrpos($str, " ");
                            $l = strlen($str) - $i;
                            $str = substr($str, $i, $l);
                            $date = str_ireplace($str, "", $row['date']);
                        ?>

                    	<div class="other_child <?php if($count%5==0) echo 'first_child'?>">
                        	<div class="car_img">

                            <?php                                       
                                if(!file_exists($row['source'])){                                                  
                            ?>
                                    <img src="<?php echo BASE_RELATIVE;?>images/noimg.jpg" width="125" height="70" />
                            <?php                                           
                                }else{
                            ?>
                                    <img src="<?php echo BASE_RELATIVE.$row['source'];?>"  width="125" height="70" />
                            <?php
                                }
                            ?>
                            
                            </div>
                            <p class="title"><a href="<?php echo BASE_RELATIVE;?>community-view?id=<?php echo $row['id'].'&owner='.$row['owner']?>" class="linkfade" style="color:#424242;"><?php echo $row['title']?></a></p>
                            <p><?php echo $date?></p>
                            <p><a href="<?php echo BASE_RELATIVE;?>community-view?id=<?php echo $row['id'].'&owner='.$row['owner']?>" class="linkfade">Writer</a></p>
                        </div>
                        <?php $count++;}?>
                    </div>
                </div>
                <div id="pagination">
                	
                    <?php 
                          
                         echo $pagination;
                                            
                    ?>

                </div>
            </div><!-- end div id="sectionContent"-->
        </div><!-- end div id="sectionContentWraper" -->       
  
  
  