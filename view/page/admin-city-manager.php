<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';
/*
 * VALIDATE ADMINISTRATOR GROUP ------------------------------------------------
 */
if( $_SESSION['log_group'] == 'admin' )
{
    // get form feed.
    $fstatus = $_CLASS->getFormStatus();
    $fmessage = $_CLASS->getFormMessage();
    $fstyle = $_CLASS->getFormStyle();
    
    // get news list.
    //$registerTypeList = $_CLASS->getRegisterTypeList();
    //$cityList=$_CLASS->getCityList();
    //var_dump($cityList);
    if(isset($_GET['country'])) $country=htmlspecialchars($_GET['country']); else $country='';
    if(isset($_POST['countrySelect'])) $country=htmlspecialchars($_POST['countrySelect']); 
?>
<script>
    var country='<?php echo $country;?>';
</script>
<link rel="stylesheet" href="<?php echo BASE_RELATIVE;?>css/community/community.css" />
          <div id="sectionContenWrapper">
                <div id="topMenu">
                    <p><a href="#" class="linkfade">Site Manager</a></p>

                </div>
                <?php include("php/sidebar/community.php");?>
            </div>
            <div id="sectionContent">
                <div class="page_title">
                    City Manager
                </div>
                <?php
                    if( $fstatus ){
                ?>
                <div class="alert <?php echo $fstyle;?>">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <?php echo $fmessage;?>
                </div>
                <?php
                    }
                ?>
                <form method='post'>
                    <div>Show Cities in 
                        <select id="countrySelect" name="countrySelect">
                            <?php include('ajax/get_country_list.php');?>
                        </select>
                    </div>
                    <div class="selected_button_wrapper">
                        <input type="submit" name="save_changes" class="selected_button" id="save_changes" value="SAVE CHANGES">
                        <input type="submit" name="delete_selected" class="selected_button" id="delete_selected" value="DELETE SELECTED">
                    </div>
                    <div id="city_table_wrapper">
                        

                    </div>
                </form>
            </div>
            <div class="clearfix"></div>
        </div>
<?php
} // end validation.
else
{
?>
        <p class="text-warning"><?php echo $_LOCAL['UNAUTHORIZED_ACCESS_MESSAGE'];?></p>
<?php
}
?>

<div style="clear:both"></div>
