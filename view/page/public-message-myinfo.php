    <?php if ($_SESSION['mobile']==false) { ?>
    <div id="main-content">
    <?php } else { ?>
    <div id="main-content" style="width:100%">
    <?php } ?>
    
    	<div class="page-title">
        
        <?php if ($_SESSION['mobile']==false) { ?>
			<div class="title-left">Success</div>
            <div id="title"><img class="space" src="<?php echo BASE_RELATIVE;?>images/logout-icon/Cute-Ball-Go-icon.png" /></div>
         <?php } else { ?>
         <div class="title-left" style="width:30%; margin-left:30%; float:left;">Success</div>
            <div id="title" style="width:5%; float:left"><img class="space" style=" margin-left:2%" src="<?php echo BASE_RELATIVE;?>images/logout-icon/Cute-Ball-Go-icon.png" /></div>
          <?php } ?>
            <p class="clear"></p>
		</div>
        
        <div style="padding:1%">
        <?php if ($_SESSION['mobile']==false) { ?>
        <p class="first">Your profile has been changed!</p>
         <?php }else { ?>
         <p class="first" style="font-size:22px">Your profile has been changed!</p>
          <?php } ?>
        <p class="second">Go to <span><a class="link" href="<?php echo BASE_RELATIVE;?>login">log in</a></span> page now.</p>
		<p class="third"><span class="third-child">Having trouble accessing your account?</span><span> Please contact </span>
        <span> <a class="link-contact" href="mailto:support@ankorauto.com">Online Customer Support.</a></span><p>
    </div></div>