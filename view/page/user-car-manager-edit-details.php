<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = true;
$_CKEDITOR_ID = 'ebody';
/*
 * VALIDATE ADMINISTRATOR GROUP ------------------------------------------------
 */
if( $_SESSION['log_group'] == 'user' )
{
    // get form feed.
    $fstatus = $_CLASS->getFormStatus();
    $fmessage = $_CLASS->getFormMessage();
    $fstyle = $_CLASS->getFormStyle();
    
    // get car details.
    $details = $_CLASS->getVehicleDetails();
    $car     = $_CLASS->getVehicle();
?>
        <div id="content-wrapper">
            <div id="vertical-menu">
                <?php $_MENU->loadMenu($_GLANG); ?>
            </div>
            <div id="inner-content">
                <!-- START CONTENT -->
                <h1><?php echo $_LANG['EDIT_DETAILS_HEADER'];?></h1>
                <?php
                    if( $fstatus ){
                ?>
                <div class="alert <?php echo $fstyle;?>">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <?php echo $fmessage;?>
                </div>
                <?php
                    }
                    
                    // display form if details are not empty.
                    if( count($car) > 0 ){
                ?>
                <h4><?php echo $car['maker'] . ' ' . $car['model'] . ' ' . $car['year'];?></h4>
                <form method="post" action="?cid=<?php echo trim($_GET['cid']);?>">
                    <div class="input-prepend">
                        <span class="add-on" style="width:150px;"><?php echo $_LANG['EDIT_DETAILS_ENGINE_SIZE_LABEL'];?></span>
                        <input type="text" name="engineInput" style="width:554px;" value="<?php echo $details['engine_size'];?>" />
                    </div>
                    <div class="input-prepend">
                        <span class="add-on" style="width:150px;"><?php echo $_LANG['EDIT_DETAILS_TRIM_LABEL'];?></span>
                        <input type="text" name="trimInput" style="width:554px;" value="<?php echo $details['trim'];?>" />
                    </div>
                    <div class="input-prepend">
                        <span class="add-on" style="width:150px;"><?php echo $_LANG['EDIT_DETAILS_TYPE_LABEL'];?></span>
                        <input type="text" name="typeInput" style="width:554px;" value="<?php echo $details['type'];?>" />
                    </div>
                    <div class="input-prepend">
                        <span class="add-on" style="width:150px;"><?php echo $_LANG['EDIT_DETAILS_GEAR_LABEL'];?></span>
                        <input type="text" name="gearInput" style="width:554px;" value="<?php echo $details['gear'];?>" />
                    </div>
                    <div class="input-prepend">
                        <span class="add-on" style="width:150px;"><?php echo $_LANG['EDIT_DETAILS_MOT_LABEL'];?></span>
                        <input type="text" name="motInput" style="width:554px;" value="<?php echo $details['mot'];?>" />
                    </div>
                    <div class="input-prepend">
                        <span class="add-on" style="width:150px;"><?php echo $_LANG['EDIT_DETAILS_FUEL_TYPE_LABEL'];?></span>
                        <input type="text" name="fuelInput" style="width:554px;" value="<?php echo $details['fuel'];?>" />
                    </div>
                    <div class="input-prepend">
                        <span class="add-on" style="width:150px;"><?php echo $_LANG['EDIT_DETAILS_COLOR_LABEL'];?></span>
                        <input type="text" name="colorInput" style="width:554px;" value="<?php echo $details['color'];?>" />
                    </div>
                    <div class="input-prepend">
                        <span class="add-on" style="width:150px;"><?php echo $_LANG['EDIT_DETAILS_PREV_OWNER_LABEL'];?></span>
                        <input type="text" name="prevOwnerInput" style="width:554px;" value="<?php echo $details['prev_owners'];?>" />
                    </div>
                    <div class="input-prepend">
                        <span class="add-on" style="width:150px;"><?php echo $_LANG['EDIT_DETAILS_LAST_SERVICE_LABEL'];?></span>
                        <input type="text" name="lastServiceInput" style="width:554px;" value="<?php echo $details['last_service'];?>" />
                    </div>
                    <div class="input-prepend">
                        <span class="add-on" style="width:150px;"><?php echo $_LANG['EDIT_DETAILS_TAX_BAND_LABEL'];?></span>
                        <input type="text" name="taxInput" style="width:554px;" value="<?php echo $details['tax_band'];?>" />
                    </div>
                    <div class="input-prepend">
                        <span class="add-on" style="width:150px;"><?php echo $_LANG['EDIT_DETAILS_TOP_SPEED_LABEL'];?></span>
                        <input type="text" name="topSpeedInput" style="width:554px;" value="<?php echo $details['top_speed'];?>" />
                    </div>
                    <div class="input-prepend">
                        <span class="add-on" style="width:150px;"><?php echo $_LANG['EDIT_DETAILS_ENGINE_TORQUE_LABEL'];?></span>
                        <input type="text" name="torqueInput" style="width:554px;" value="<?php echo $details['engine_torque_rpm'];?>" />
                    </div>
                    <div class="input-prepend">
                        <span class="add-on" style="width:150px;"><?php echo $_LANG['EDIT_DETAILS_ENGINE_POWER_KW_LABEL'];?></span>
                        <input type="text" name="powerInput" style="width:554px;" value="<?php echo $details['engine_power_kw'];?>" />
                    </div>
                    <div class="input-prepend">
                        <span class="add-on" style="width:150px;"><?php echo $_LANG['EDIT_DETAILS_TRANSMISSION_TYPE_LABEL'];?></span>
                        <input type="text" name="transmissionInput" style="width:554px;" value="<?php echo $details['transmission_type'];?>" />
                    </div>
                    <div class="input-prepend">
                        <h4><?php echo $_LANG['EDIT_DETAILS_MORE_DETAILS_LABEL'];?></h4>
                        <textarea id="ebody" name="htmlInput"><?php echo $details['html'];?></textarea>
                    </div>
                    <br>
                    <input type="submit" class="btn btn-small btn-success" name="savebtn" value="<?php echo $_LANG['EDIT_DETAILS_UPDATE_BUTTON'];?>" />
                </form>
                <?php
                    }
                ?>
                <!-- END CONTENT -->
            </div>
            <div class="clearfix"></div>
        </div>
<?php
} // end validation.
else
{
?>
        <p class="text-warning"><?php echo $_LOCAL['UNAUTHORIZED_ACCESS_MESSAGE'];?></p>
<?php
}