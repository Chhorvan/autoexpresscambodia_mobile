<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = true;
$_CKEDITOR_ID = 'ebody';
/*
 * VALIDATE ADMINISTRATOR GROUP ------------------------------------------------
 */
if( $_SESSION['log_group'] == 'admin' )
{
    
    // set form feedback vars.
    $fstatus = $_CLASS->getFormStatus();
    $fstyle  = $_CLASS->getFormStyle();
    $fmessage= $_CLASS->getFormStatusMessage();
?>
        <div id="content-wrapper">
            <div id="vertical-menu">
                <?php $_MENU->loadMenu($_GLANG); ?>
            </div>
            <div id="inner-content" style="height:1000px;">
                <!-- START CONTENT -->
                <p>
                    <h3><?php echo $_LOCAL['REPLY_TO'];?> <?php echo $_CLASS->getName();?></h3>
                    <a class="btn btn-info" href="<?php echo BASE_RELATIVE;?>/home/inbox/"><?php echo $_LOCAL['REPLY_GOBACK_LABEL'];?></a>
                </p>
                <?php
                    if( $fstatus ) {
                ?>
                <div class="alert <?php echo $fstyle;?>">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <?php echo $fmessage;?>
                </div>
                <?php } ?>
                <form method="post">
                    <div class="input-prepend">
                        <span class="add-on" style="width: 150px;"><?php echo $_LOCAL['REPLY_SUBJECT'];?><span class="required-field">*</span></span>
                        <input type="text" style="width: 555px;" class="span2" name="subject" placeholder="<?php echo $_LOCAL['REPLY_SUBJECT'];?>" />
                    </div>
                    <textarea id="ebody" name="sendMsg"><?php echo $_CLASS->getTemplateMetadata();?></textarea>
                    <input type="hidden" name="destination" value="<?php echo $_CLASS->getTOEmail();?>" />
                    <input type="hidden" name="destname" value="<?php echo $_CLASS->getName();?>" />
                    <input type="hidden" name="msgid" value="<?php echo $_CLASS->getMessageID();?>" />
                </form>
                <!-- END CONTENT -->
            </div>
            <div class="clearfix"></div>
        </div>
<?php
} // end validation.
else
{
?>
        <p class="text-warning"><?php echo $_LOCAL['UNAUTHORIZED_ACCESS_MESSAGE'];?></p>
<?php
}