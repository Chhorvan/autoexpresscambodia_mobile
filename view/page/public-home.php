<?php
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';

// define version
$version=VERSION;
$version=time();
?>

<script type="text/javascript">
	// loading vechicle
	function showLoading(){
		$("#loading_image").css("display", "block");
	}
	function hideLoading(){
		$("#loading_image").css("display", "none");	
	}

	var product_type='Car';

	var make ='';
	var model='';
	var ch='';
	var search="<?php if(isset($_GET['search'])) echo htmlspecialchars($_GET['search']);?>";
	// product_type="<?php if(isset($_GET['product_type'])) echo htmlspecialchars($_GET['product_type']);?>";

	var condition="<?php if(isset($_GET['condition'])) echo htmlspecialchars($_GET['condition']);?>";
	var fuel_type="<?php if(isset($_GET['fuel_type'])) echo htmlspecialchars($_GET['fuel_type']);?>";
	var engine_volume="<?php if(isset($_GET['engine_volume'])) echo htmlspecialchars($_GET['engine_volume']);?>";
	var engine_volume_from="<?php if(isset($_GET['engine_volume_from'])) echo htmlspecialchars($_GET['engine_volume_from']);?>";
	var engine_volume_to="<?php if(isset($_GET['engine_volume_to'])) echo htmlspecialchars($_GET['engine_volume_to']);?>";
	var country="<?php if(isset($_GET['country'])) echo htmlspecialchars($_GET['country']);?>";
	var transmission="<?php if(isset($_GET['transmission'])) echo htmlspecialchars($_GET['transmission']);?>";
	var vehicle_type="<?php if(isset($_GET['body_style'])) echo htmlspecialchars($_GET['body_style']);?>"
	var drive_type="<?php if(isset($_GET['drive_type'])) echo htmlspecialchars($_GET['drive_type']);?>";
	var category="<?php if(isset($_GET['category'])) echo htmlspecialchars($_GET['category']);?>";
	var category2="<?php if(isset($_GET['category2'])) echo htmlspecialchars($_GET['category2']);?>";
	var category3="<?php if(isset($_GET['category3'])) echo htmlspecialchars($_GET['category3']);?>";
	make="<?php if(isset($_GET['make'])) echo htmlspecialchars($_GET['make']);?>";
	model="<?php if(isset($_GET['model'])) echo htmlspecialchars($_GET['model']);?>";
	var steering="<?php if(isset($_GET['steering'])) echo htmlspecialchars($_GET['steering']);?>";
	var country="<?php if(isset($_GET['country'])) echo htmlspecialchars($_GET['country']);?>";
	var year_from="<?php if(isset($_GET['year_from'])) echo htmlspecialchars($_GET['year_from']); elseif(isset($_GET['man_year_from'])) echo htmlspecialchars($_GET['man_year_from']);?>";
	var year_to="<?php if(isset($_GET['year_to'])) echo htmlspecialchars($_GET['year_to']); elseif(isset($_GET['man_year_to'])) echo htmlspecialchars($_GET['man_year_to']);?>";
	var price_from="<?php if(isset($_GET['price_from'])) echo htmlspecialchars($_GET['price_from']);?>";
	var price_to="<?php if(isset($_GET['price_to'])) echo htmlspecialchars($_GET['price_to']);?>";
	var month_from="<?php if(isset($_GET['month_from'])) echo htmlspecialchars($_GET['month_from']); elseif(isset($_GET['man_month_from'])) echo htmlspecialchars($_GET['man_month_from']);?>";
	var month_to="<?php if(isset($_GET['month_to'])) echo htmlspecialchars($_GET['month_to']); elseif(isset($_GET['man_month_to'])) echo htmlspecialchars($_GET['man_month_to']);?>";
	var car_id="<?php if(isset($_GET['car_id'])) echo htmlspecialchars($_GET['car_id']);?>";
	var customSearch = "<?php if(isset($_GET['customSearch'])) echo htmlspecialchars($_GET['customSearch']);?>";
	if(product_type!=''){
		product_type="&product_type="+product_type;
	}

	if(vehicle_type!=''){
		ch =vehicle_type;
		vehicle_type="&vehicle_type="+vehicle_type.split(' ').join('+');

	}
	if(make!=''){
		make="&make="+make;
	}
	if(model!=''){
		model="&model="+model;
	}
	if(year_from!=''){
		year_from="&year_from="+year_from;
	}
	if(year_to!=''){
		year_to="&year_to="+year_to;
	}
	if(price_from!=''){
		price_from="&price_from="+price_from;
	}
	if(price_to!=''){
		price_to="&price_to="+price_to;
	}
</script>
 
<body id="top" class="page-id-home">
<div id="homeContainer">
	<div class="container clearfix">		
		<div id="carSearch">			
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<select name="make" id="product_make" class="selectCar">						
							<option style='text-align:left;' id='reload' value=''>Maker</option>
							<?php																
								$make = $_CLASS->load_make();
								foreach($make as $row){
									echo"<option>$row[make]</option>";
								}

							?>
						</select>
					</td>
				</tr>				
				<tr>
					<td>
						<select name="model" id="product_model" class="selectCar">
							<option value=""><?php echo $_LANG['PUBLIC_HOME_ANY_MODEL_OPTION'];?></option>
						</select>
					</td>
				</tr>
				<tr>
					<td>
						<select name="year" id="product_year" class="selectCar">
							<option value="">Year</option>
							<?php $min_year=date('Y'); for ($i=$min_year;$i>=1990;$i--){ ?>
									<option><?php echo $i; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td>
						<input type="text" autocomplete="off" id="searchCar" name="search_car" placeholder="Keyword...." class="search" />
						<div style="position:relative;">
							<div id="result"></div>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<input id="submit" type="submit" name="bnt_search" value="<?php echo $_LANG['PUBLIC_HOME_SEARCH'];?>" class="linkimg" />
					</td>
				</tr>
			</table>			
		</div><!-- #carSearch -->				
		
		<div id="lastestCar">
			<p class="title">Vehicle List</p>
			
			<!-- loading image -->
			<div id="loadingVehicle">	
				<img id="loading_image" src="<?php echo BASE_RELATIVE;?>images/loading_75.gif" />	
			</div>
			
			<div class="load_Car"> </div><!--End detail-->
		
		</div><!-- #lastestCar -->		
    
	</div><!-- .container -->
</div><!-- #homeContainer -->

<!-- Send best url to javascript -->
<input type="hidden" id="baseurl" name="baseurl" value="<?php echo BASE_RELATIVE; ?>" />

<!---Import jquery---->
<script src="<?php echo BASE_RELATIVE.'autocam_js/public_home/index.js?v='.$version; ?>"></script>

<!-- Menu condition -->
<?php		
	// vehicle_type
	$vehicle_type = isset($_GET["vehicle_type"]) ? $_GET["vehicle_type"] : '';	
	$class_vehicle_type = isset($_GET["vehicle_type"]) ? $_GET["vehicle_type"] : '';

	// caught url vehicle_type 
	$url_vehicle_type=$_SERVER['REQUEST_URI'];
	$ar_page=explode('/', $url_vehicle_type);
	$v_type=end($ar_page);
	if($v_type!="1" AND $v_type!="0"){
		$vehicle_type = str_replace(' ', '+', $v_type);
		$class_vehicle_type = str_replace(' ', '_', $v_type);
		$class_vehicle_type = str_replace('/', '_', $v_type);
		?>
			<script type="text/javascript">
				// loading vechicle
				showLoading();
			
				vehicle_type="&vehicle_type=<?php echo $vehicle_type ?>";			
				
				$(".<?php echo $class_vehicle_type; ?>").prop('checked', true);
				
				
				$(".load_Car").load("ajax/vechicle/load_vechicle.php?result=1"+vehicle_type+make+model+year_from+year_to+price_from+price_to+condition+steering+fuel_type+engine_volume_from+engine_volume_to+engine_volume, function() {
					// hidden loading vechicle
					hideLoading();
					$("img.lazy").lazyload({
						   effect : "fadeIn"
					});
				});					
			</script>
		<?php
	}
	
	// condition
	$condition = isset($_GET["condition"]) ? $_GET["condition"] : '';
	
	// caught url 
	$sys_url=$_SERVER['REQUEST_URI'];
	$ar_page=explode('/', $sys_url);
	$condition_url=end($ar_page);
	 if($condition_url=="1" OR $condition_url=="0"){	
		?>
			<script type="text/javascript">
				// loading vechicle
				showLoading();
			
				condition="&condition=<?php echo $condition_url ?>";
				//$(".<?php echo $condition; ?>").prop('checked', true);
				$(".load_Car").load("ajax/vechicle/load_vechicle.php?result=1"+vehicle_type+make+model+year_from+year_to+price_from+price_to+condition+steering+fuel_type+engine_volume_from+engine_volume_to+engine_volume, function() {
					// hidden loading vechicle
					hideLoading();
					$("img.lazy").lazyload({
						   effect : "fadeIn"
					});
				});						
			</script>
		<?php
	} 
			
	
	?>
		<script type="text/javascript">
			// loading vechicle
			showLoading();
			$(".load_Car").load("ajax/vechicle/load_vechicle.php?result=1"+vehicle_type+make+model+year_from+year_to+price_from+price_to+condition+steering+fuel_type+engine_volume_from+engine_volume_to+engine_volume, function() {
				// hidden loading vechicle
				hideLoading();
				$("img.lazy").lazyload({
					   effect : "fadeIn"
				});
			});				
		</script>
	<?php
	
?>