<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';
// get form feed.
$fstatus = $_CLASS->getFormStatus();
$fmessage = $_CLASS->getFormMessage();
$fstyle = $_CLASS->getFormStyle();
$group = 'public';
$slug  = 'car';
// load page content.
$pg_details = $_CLASS->getPageHTML();

?>




 <div id="sectionContenWrapper">
    <!--sectionSidebar--> 
    <div id="topMenu">
        <p><a href="#" class="linkfade">Help</a> > Trade Guide</p>
    </div>
    <?php include("php/sidebar/help.php");?>
    
    </div>           
    <div id="sectionContent"> 
        <div id="help_title">
            <p>Trade Guide</p>
        </div>
        <div class="contentText">
        	<div class="title"><p>Negotiation</p></div>
            <p class="text">
            	The negotiation will be used on Global iBlue.com before the buyer and seller reach agreement.
The buyer and seller will undergo a round of negotiations on the item lists, price, payment conditions and shipping 
freight forwarding information through Global iBlue.com. The mandatory payment terms and the trade conditions are as 
below.
            </p>
        </div>
        
        <div class="contentText">
        	<div class="title1"><p><span>&nbsp;</span>Terms of payment</p></div>
            <p class="text">
            	<span>- T/T(telegraphic transfer)</span> The buyer will hand over the payment to the remitting bank (seller’s account).<br />
                <span>- L/C(letter of credit) </span>Banks issue letters of credit as a way to ensure sellers that they will get paid.
            </p>
            
        </div>
        
        <div class="contentText">
        	<div class="title1"><p><span>&nbsp;</span>Trade conditions</p></div> 
            <img src="<?php echo BASE_RELATIVE;?>images/help/trade_guide.jpg" style="float:left; margin-top:15px; margin-bottom:20px;" />
            <p class="text">
            	<span>- EXW(Ex Works) </span>Sellers do not take responsibility for delivery and buyers shoulder all expenses.<br />
                <span>- FOB(Free on Board) </span>Sellers are required to cover the expenses to the port of registry and buyers shoulder all the 
other expenses.<br />
				<span>- CFR(Cost and Freight) </span>Sellers are required to cover the shipping expenses to the port of arrival and the buyers 
shoulder all the other expenses.<br />
				<span>- CIF(Cost, Insurance and Freight) </span>Sellers are required to cover the shipping expenses and insurance fee to the port 
of arrival and the buyers shoulder all the other expenses (CFR+Insurance).
            </p>           
        </div>
        
        <div class="contentText">
        	<div class="title"><p>Payment</p></div>
            <p class="text">
            	Buyer check out the seller’s information (price & account) and make a payment.
            </p>
        </div>
        <div class="contentText">
        	<div class="title"><p>Shipment</p></div>
            <p class="text">
            	Seller make a shipment of vehicle after payment was received.
            </p>
        </div>
        <div class="contentText">
        	<div class="title"><p>Forwarding documents</p></div>
            <p class="text">
            	Bill of lading will be issued by the shipping company after shipment.<br />
Seller is required to send all related documents below include B/L to the particular buyer.
            </p>
        </div>
        <div class="contentText">
        	<div class="title1"><p><span>&nbsp;</span>Shipping documents</p></div> 
            
            <p class="text">
            	<span>- Proforma Invoice </span>A document that states a commitment from to seller to sell goods to the buyer at specified price 
and terms.<br />
                <span>- Contract </span>A contract is an agreement entered into voluntarily by seller and buyer with the intention of creating a legal 
obligation, which includes information on price, items, terms of payment, and ways of shipment.<br />
				<span>- Commercial Invoice </span>The seller’s claim for payment to the buyer under the terms of sales contract include the 
description of the goods exactly as stated in the documentary credit.<br />
				<span>- Packing List </span>The contents of each packing unit showing actual packing and/or weight details which must agree with 
the other documents.<br />
				<span>- B/L, Bill of lading </span>A receipt given to the seller by the shipping company for goods accepted for carriage by sea.
            </p>           
        </div>
        <div class="contentText">
        	<div class="title"><p>Vehicle pick</p></div>
            <p class="text">
            	When the goods arrived at the port, a buyer is required to hand in the original documents of B/L to the particular <br />
shipping company to get the vehicle. The customs service of some countries may ask about submitting the <br />
commercial invoice.
            </p>
        </div>
        
    </div><!-- end div id="sectionContent"-->
</div><!-- end div id="sectionContentWraper" -->       
  
  
  