<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';
// get form feed.
$fstatus = $_CLASS->getFormStatus();
$fmessage = $_CLASS->getFormMessage();
$fstyle = $_CLASS->getFormStyle();
$group = 'public';
$slug  = 'car';
// load page content.
$pg_details = $_CLASS->getPageHTML();
$listnews = $_CLASS->getNewsList();
 

$pagination=$_CLASS->pagination_html;
    $total_row=$_CLASS->total_num_row;
    $current_page=$_CLASS->current_page;
    $total_page=$_CLASS->total_page;
?>


            <div id="sectionContenWrapper">
            	<!--sectionSidebar--> 
                <div id="topMenu">
                	<p><a href="#" class="linkfade">Community</a> > News</p>
                </div>
            	<?php include("php/sidebar/community.php");?>
                
            </div>           
            <div id="sectionContent"> 
            	<div id="community_title">
                	<p>News</p>
                </div>
                <div id="news_content">
                	<ul>
                        <?php
                            foreach($listnews as $new){
                                ( $_SESSION['log_language_iso'] == 'en' ? $format = 'yyyymmdd' : $format = 'ddmmyyyy' );
                        ?>
	                    <li><div class="number"><?php echo $new['id'];?>
                            </div>
                            <a href="<?php echo BASE_RELATIVE.'community-news-details?id='.$new['id'];?>">
                            <?php echo '['.$new['type'].'] '.$new['title'];?></a> 
                         <p><?php echo  $new["date"] ;?></p></li>
                    	 
                        <?php
                            }
                        ?>
                    </ul>
                </div>
 
                   <span class="page-count">Total :<?php echo $total_row.' items   page: '.$current_page.' / '.$total_page;?></span>
                   <div id="pagination">
                    <?php 
      
                    echo $pagination;
                        
                    ?>

               </div>      
 
            </div><!-- end div id="sectionContent"-->
        </div><!-- end div id="sectionContentWraper" -->   

  
  
  
  
  
  