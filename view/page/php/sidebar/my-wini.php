<div id="sectionSidebar">          
	<div id="topSideBar">
    	<ul>
        	<li class="first_child"><a href="<?php echo BASE_RELATIVE;?>my-info" class="linkfade">My Information</a></li>
            <li><a href="<?php echo BASE_RELATIVE;?>messages" class="linkfade">Message Box</a></li>
            <li><a href="<?php echo BASE_RELATIVE;?>myfreight-cost" class="linkfade">My Frieght Cost</a></li>
            <li><a href="<?php echo BASE_RELATIVE;?>vehicle?page=inventory" class="linkfade">My Inventory</a></li>
            <li class="linkexpand">
                <a class="dead-link" href="#" >Register Items</a>
                <ul>
                    <li><a href="<?php echo BASE_RELATIVE;?>edit-car" class="linkfade">Register Car</a></li>
                    <li><a href="<?php echo BASE_RELATIVE;?>edit-truck" class="linkfade">Register Truck</a></li>
                    <li><a href="<?php echo BASE_RELATIVE;?>edit-bus" class="linkfade">Register Bus</a></li>
                    <li><a href="<?php echo BASE_RELATIVE;?>edit-equipment" class="linkfade">Register Heavy Machine</a></li>
                    <li><a href="<?php echo BASE_RELATIVE;?>edit-part" class="linkfade">Register Part</a></li>
                    <li><a href="<?php echo BASE_RELATIVE;?>edit-motorbike" class="linkfade">Register Motorbike</a></li>
                    <li><a href="<?php echo BASE_RELATIVE;?>edit-aircraft" class="linkfade">Register Aircraft</a></li>
                    <li><a href="<?php echo BASE_RELATIVE;?>edit-watercraft" class="linkfade">Register Watercraft</a></li>
                </ul>
            </li>    
                    
        </ul>
    </div>
     