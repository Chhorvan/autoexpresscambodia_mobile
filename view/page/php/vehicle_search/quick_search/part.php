




                    <?php
                        if ($_SESSION['mobile']==false) {
                    ?>

                    <div id="second_search">
                        <!-- <div id="search_title" class="clearfix">
                            <p><a href="#" class="current" id="quick_search">Quick Search</a> <span>&nbsp;&nbsp; | &nbsp;&nbsp;</span> <a href="#" id="ad_search">Advance Search</a></p>
                        </div> -->
                        <div id="search_wrap" class="clearfix">
                            <form method="get" action="my-inventory">
                                <input type="hidden" class="formSelection" name="product_type" id="product_type" value="<?php if(isset($_GET['product_type'])) echo htmlspecialchars($_GET['product_type']);?>"/>

                                <input type="hidden" class="formSelection" name="page" id="page" value="<?php if(isset($_GET['page'])) echo htmlspecialchars($_GET['page']);?>"/>

                            <tr>
                            <td class="top category">
                                <select class="formSelection category categorypart" name="category" id="category">
                                    <?php
                                        include('ajax/load_category.php');
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="top category2">
                                <select class="formSelection category2 categorypart" name="category2" id="category2">
                                    <option value="">- Category 2 -</option>

                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="top category2">
                                <select class="formSelection category2 categorypart" name="category3" id="category3">
                                    <option value="">- Category 3 -</option>

                                </select>
                            </td>
                        </tr>
                                <select class="condition formSelection" name="condition" id="condition">
                                    <option value="">- Condition -</option>
                                    <option value="New">New</option>
                                    <option value="Used">Used</option>
                                    <option value="Remade">Remade</option>

                                </select>

                                <select class=" formSelection " name="country" id="country_adv">
                                    <?php include('ajax/load_country.php'); ?>
                                </select>
                                <input type="text" class="textBox" name="customSearch" placeholder="Model name, Item No..." id="customSearch" />

                                    <input type="submit" value="" class="search linkfade" />
                            </form>
                        </div>
                        <!-- <div id="advance_search" class="clearfix">
                            <form method="get" action="vehicle">
                                <input type="hidden" class="formSelection" name="product_type" id="product_type" value="<?php if(isset($_GET['product_type'])) echo htmlspecialchars($_GET['product_type']);?>"/>
                                <input type="hidden" name="search" value="advance"/>

                                  <tr>
                            <td class="top category">
                                <select class="formSelection category " name="category" id="category">
                                    <?php
                                        include('ajax/load_category.php');
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="top category2">
                                <select class="formSelection category2" name="category2" id="category2">
                                    <option value="">- Category 2 -</option>

                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="top category2">
                                <select class="formSelection category2" name="category3" id="category3">
                                    <option value="">- Category 3 -</option>

                                </select>
                            </td>
                        </tr>
                                <select class="condition formSelection moto" name="condition" id="condition">
                                    <option value="">- Condition -</option>
                                    <option value="New">New</option>
                                    <option value="Used">Used</option>
                                    <option value="Remade">Remade</option>

                                </select>

                                <select class=" formSelection country_adv" name="country" id="country_adv">
                                    <?php include('ajax/load_country.php'); ?>
                                </select>


                                <span> Price </span>
                                <input type="text" class="price pricepart" name="price_from" value="<?php echo $price_from;?>" placeholder="$1000" id="price_from_adv" onkeypress="return isNumberKey(event)" />
                                <span class="sep"> - </span>

                                <input type="text" class="price pricepart" name="price_to" value="<?php echo $price_to;?>" placeholder="$1000" id="price_to_adv" onkeypress="return isNumberKey(event)" />

                                <input type="submit" value="" class="search linkfade" />
                            
                        </div> -->
                        </form>
                    </div>


                    <?php }
                    else{
                        ?>
                    <div class="new_searchwrap">
                        <div class="newsearch">
                            <form method="get" action="vehicle">
                                <input type="hidden" class="formSelection" name="product_type" id="product_type" value="<?php if(isset($_GET['product_type'])) echo htmlspecialchars($_GET['product_type']);?>"/>
                                <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class=" top">

                                        <select class="condition formSelection" name="condition" id="condition">
                                            <option value="">- Condition -</option>
                                            <option value="New">New</option>
                                            <option value="Used">Used</option>

                                        </select>

                                    </td>
                                </tr>
                                <tr>
                                    <td >
                                        <select class="make formSelection" name="make" id="make">
                                            <?php
                                                include('ajax/load_car_make.php');
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td >
                                        <select class="model formSelection" name="model" id="model">
                                            <option value="">- Model -</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td >
                                        <select class="steering formSelection" name="steering" id="steering">
                                            <option selected value="">- Steering -</option>
                                            <option value="LHD">LHD</option>
                                            <option value="RHD">RHD</option>
                                        </select>
                                    </td>
                                </tr>
                                <!--<tr>
                                    <td >
                                        <select class="country formSelection" name="country" id="country">
                                            <?php
                                                include('ajax/load_country.php');
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                                -->
                                <tr>
                                    <td >
                                        <select class="ful_type formSelection" name="fuel_type" id="fuel_type">
                                            <?php
                                                include('load_saved_fuel_type.php');
                                            ?>
                                        </select>
                                    </td>
                                </tr>


                                <tr>
                                    <td >
                                        <div id="divYear">
                                            <!-- <div id="year"> <p>Year :</p></div> -->
                                            <select class="year formSelection" name="year_from" id="year_from">
                                                <option value="">From</option>
                                                 <?php
                                                 for($i=1990;$i<=2014;$i++){
                                                     echo "<option value='$i'>$i</option>";
                                                 }
                                                 ?>
                                            </select>

                                            <select class="year_end toSelection" name="year_to" id="year_to">
                                                <option value="">To</option>
                                                 <?php
                                                 for($i=1990;$i<=2014;$i++){
                                                     echo "<option value='$i'>$i</option>";
                                                 }
                                                 ?>
                                            </select>
                                        </div>

                                    </td>
                                </tr>
                                <tr>
                                    <td class="search">
                                        <input type="text" class="textBox" name="customSearch" placeholder="Model, name..." id="customSearch" />
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <input type="submit" value="" class="search" />
                                    </td>
                                </tr>
                            </table>
                            </form>
                        </div>
                    </div>
                        <?php
                    }
                    ?>






