


                
                    <form method="get" action="vehicle">
                    <?php 
                        if ($detect->isMobile()==false) {
                    ?>
                    <div id="second_search">
                        <!-- <div id="search_title" class="clearfix">
                            <p><a href="#" class="current" id="quick_search">Quick Search</a> <span>&nbsp;&nbsp; | &nbsp;&nbsp;</span> <a href="#" id="ad_search">Advance Search</a></p>
                        </div> -->
                        <div id="search_wrap" class="clearfix">
                            <form method="get" action="my-inventory">
                                <input type="hidden" class="formSelection" name="product_type" id="product_type" value="<?php if(isset($_GET['product_type'])) echo htmlspecialchars($_GET['product_type']);?>"/>

                                <select class="truck_first make formSelection" name="make" id="make">
                                    <?php
                                        include('ajax/load_car_make.php');
                                    ?>
                                </select>
                                
                                
                                <select class="truck model formSelection" name="model" id="model">
                                    <option value="">- Model -</option>
                                </select>
                                <select class=" truck condition formSelection " name="condition" id="condition">
                                    <option value="">- Condition -</option>
                                    <option value="New">New</option>
                                    <option value="Used">Used</option>
                                    
                                </select>
                                <select class="truck steering formSelection" name="steering" id="steering">
                                    <option selected value="">- Steering -</option>
                                    <option value="LHD">LHD</option>
                                    <option value="RHD">RHD</option>
                                </select>
                                <select class="truck_first category formSelection" name="category" id="category">
                                    <?php 
                                        include('ajax/load_saved_category.php');
                                    ?>
                                </select>
                                <select style="width:130px;" class=" formSelection " name="country" id="country">
                                    <?php include('ajax/load_country.php'); ?>
                                </select>
                                <select class="year formSelection" name="year_from" id="year_from">
                                    <option value="">From...</option>
                                     <?php
                                     for($i=1990;$i<=2014;$i++){
                                         echo "<option value='$i'>$i</option>";
                                     }
                                     ?>
                                </select>
                                     
                                <select class="year toSelection" name="year_to" id="year_to">
                                    <option value="">To...</option>
                                     <?php
                                     for($i=1990;$i<=2014;$i++){
                                         echo "<option value='$i'>$i</option>";
                                     }
                                     ?>
                                </select>
                                <input type="text" class="textBox" name="customSearch" placeholder="Model, name..." id="customSearch" />
                                <input type="submit" value="" class="search linkfade" />
                            </form>
                        </div>
                        <!-- <div id="advance_search" class="clearfix">
                            <form method="get" action="vehicle">
                                <input type="hidden" class="formSelection" name="product_type" id="product_type" value="<?php if(isset($_GET['product_type'])) echo htmlspecialchars($_GET['product_type']);?>"/>
                                <input type="hidden" name="search" value="advance"/>
                                <select style="width:180px;" class="make formSelection" name="make" id="make_adv">
                                    <?php
                                        include('ajax/load_car_make.php');
                                    ?>
                                </select>
                                
                                
                                <select style="width:180px;" class="model formSelection" name="model" id="model_adv">
                                    <option value="">- Model -</option>
                                </select>
                                <select style="width:110px;" class="condition formSelection " name="condition" id="condition_adv">
                                    <option value="">- Condition -</option>
                                    <option value="New">New</option>
                                    <option value="Used">Used</option>
                                </select>
                                <select style="width:110px;" class="steering formSelection" name="steering" id="steering_adv">
                                    <option selected value="">- Steering -</option>
                                    <option value="LHD">LHD</option>
                                    <option value="RHD">RHD</option>
                                </select>
                                <select style="width:180px;" class="truck_first category formSelection" name="category" id="category_adv">
                                    <?php 
                                        include('ajax/load_saved_category.php');
                                    ?>
                                </select>
                                <select style="width:160px;" class=" formSelection " name="country" id="country_adv">
                                    <?php include('ajax/load_country.php'); ?>
                                </select>
                                <select class="year formSelection" name="year_from" id="year_from_adv">
                                    <option value="">From...</option>
                                     <?php
                                     for($i=1990;$i<=2014;$i++){
                                         echo "<option value='$i'>$i</option>";
                                     }
                                     ?>
                                </select>
                                     
                                <select class="year toSelection" name="year_to" id="year_to_adv">
                                    <option value="">To...</option>
                                     <?php
                                     for($i=1990;$i<=2014;$i++){
                                         echo "<option value='$i'>$i</option>";
                                     }
                                     ?>
                                </select>
                                <select style="width:150px;" class=" formSelection " name="transmission" id="transmission_adv">
                                    <?php include('ajax/load_saved_transmission.php'); ?>
                                </select>
                                
                                <select style="width:150px;" class=" formSelection " name="fuel_type" id="fuel_type_adv">
                                    <?php
                                        include('ajax/load_saved_fuel_type.php');
                                    ?>
                                </select>
                                
                                <span class="displacement"> Displacement </span>
                                <select class="any formSelection " name="engine_volume_from" id="engine_volume_from_adv">
                                    <option value="">--Any--</option>
                                    <?php 
                                        for($i=500;$i<=6000;$i+=100){
                                            echo "<option value='{$i}'>{$i}</option>";
                                        }
                                    ?>
                                </select>
                                <select class="any formSelection " name="engine_volume_to" id="engine_volume_to_adv">
                                    <option value="">--Any--</option>
                                    <?php 
                                        for($i=500;$i<=6000;$i+=100){
                                            echo "<option value='{$i}'>{$i}</option>";
                                        }
                                    ?>
                                </select>
                                <span> Price </span>
                                <input type="text" class="price" name="price_from" value="<?php echo $price_from;?>" placeholder="$1000" id="price_from_adv" onkeypress="return isNumberKey(event)" />
                                <span class="sep"> - </span>

                                <input type="text" class="price" name="price_to" value="<?php echo $price_to;?>" placeholder="$1000" id="price_to_adv" onkeypress="return isNumberKey(event)" />
                                <input type="text" class="text_search" name="customSearch" placeholder="Model, Company Name, Chassis no." id="customSearch_adv" />

                                <input type="submit" value="" class="search linkfade" />                            
                        </div> -->                  
                        </form>
                    </div>
                    <!-- <table border="0" cellspacing="0" cellpadding="0">
                        <input type="hidden" class="formSelection" name="product_type" id="product_type" value="<?php if(isset($_GET['product_type'])) echo htmlspecialchars($_GET['product_type']);?>"/>
                        <tr>
                            <td class="top condition">                                
                                <select class="formSelection condition" name="condition" id="condition">
                                    <option value="">- Condition -</option>
                                    <option value="New">New</option>
                                    <option value="Used">Used</option>
                                    
                                </select>                                
                            </td>
                            <td class="make">
                                <select class="formSelection make" name="make" id="make">
                                    <?php 
                                        include('ajax/load_car_make.php');
                                    ?>
                                </select>
                            </td>
                            <td class="model">
                                <select class="formSelection model" name="model" id="model">
                                    <option value="">- Model -</option>                                    
                                </select>
                            </td>
                            <td class="steering">
                                <select class="formSelection steering" name="steering" id="steering">
                                    <option selected value="">- Steering -</option>
                                    <option value="LHD">LHD</option>
                                    <option value="RHD">RHD</option>
                                </select>
                            </td>
                            <td class="country">
                                <select class="formSelection country" name="country" id="country">
                                    <?php 
                                        include('ajax/load_country.php');
                                    ?>
                                </select>
                            </td>
                            <td class="year">
                                <div id="divYear">
                                    
                                    <select class="year formSelection" name="year_from" id="year_from">
                                        <option value="">Year</option>
                                         <?php
                                         for($i=1990;$i<=2014;$i++){
                                             echo "<option value='$i'>$i</option>";
                                         }
                                         ?>
                                    </select>
                                         ~ 
                                    <select class="year toSelection" name="year_to" id="year_to">
                                        <option value="">Year</option>
                                         <?php
                                         for($i=1990;$i<=2014;$i++){
                                             echo "<option value='$i'>$i</option>";
                                         }
                                         ?>
                                    </select>
                                </div>
                                
                            </td>
                            <td class="search">
                                <input type="text" class="textBox" name="customSearch" placeholder="Model, name..." id="customSearch" />                      
                                <input type="submit" value="" class="search" />
                                
                            </td>
                          </tr>
                          
                    </table> -->
                    <?php }
                    else{
                        ?>
                            <table border="0" cellspacing="0" cellpadding="0">
                        <input type="hidden" class="formSelection" name="product_type" id="product_type" value="<?php if(isset($_GET['product_type'])) echo htmlspecialchars($_GET['product_type']);?>"/>
                        <tr>
                            <td class="top condition">                                
                                <select class="formSelection condition" name="condition" id="condition">
                                    <option value="">- Condition -</option>
                                    <option value="New">New</option>
                                    <option value="Used">Used</option>
                                    
                                </select>                                
                            </td>
                        </tr>
                        <tr>
                            <td class="make">
                                <select class="formSelection make" name="make" id="make">
                                    <?php 
                                        include('ajax/load_car_make.php');
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="model">
                                <select class="formSelection model" name="model" id="model">
                                    <option value="">- Model -</option>                                    
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="steering">
                                <select class="formSelection steering" name="steering" id="steering">
                                    <option selected value="">- Steering -</option>
                                    <option value="LHD">LHD</option>
                                    <option value="RHD">RHD</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="country">
                                <select class="formSelection country" name="country" id="country">
                                    <?php 
                                        include('ajax/load_country.php');
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="year">
                                <div id="divYear">
                                    <!-- <div id="year"> <p>Year :</p></div> -->
                                    <select class="year formSelection" name="year_from" id="year_from">
                                        <option value="">From</option>
                                         <?php
                                         for($i=1990;$i<=2014;$i++){
                                             echo "<option value='$i'>$i</option>";
                                         }
                                         ?>
                                    </select>
                                          
                                    <select class="year_end toSelection" name="year_to" id="year_to">
                                        <option value="">To</option>
                                         <?php
                                         for($i=1990;$i<=2014;$i++){
                                             echo "<option value='$i'>$i</option>";
                                         }
                                         ?>
                                    </select>
                                </div>
                                
                            </td>
                        </tr>
                        <tr>
                            <td class="search">
                                <input type="text" class="textBox" name="customSearch" placeholder="Model, name..." id="customSearch" />                      
                                
                                
                            </td>                            
                        </tr>
                        <tr>
                            <td>
                                <input type="submit" value="" class="search" />
                            </td>
                        </tr>
                          
                    </table>
                        <?php
                    }
                    ?>
                    </form>
                