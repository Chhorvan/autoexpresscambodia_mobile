
<!-- Script for hidden Location-->
<script type="text/javascript">
  $(document).ready(function(){
   $("#citySelect").hide();

});
</script>


<?php
	if( !isset($_SESSION['log_group']) ){
		header('Location: '.BASE_RELATIVE.'register/');
	}

/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE. test For CK
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '_ebody';
/*
 * VALIDATE ADMINISTRATOR GROUP ------------------------------------------------
 */

    // get form feed.
    $fstatus = $_CLASS->getFormStatus();
    $fmessage = $_CLASS->getFormMessage();
    $fstyle = $_CLASS->getFormStyle();
    $invalidProduct = false;
    // get lists.
    $maker_unique_list = $_CLASS->getMakerList();
    $model_unique_list = $_CLASS->getModelList();
    $body_type_list    = $_CLASS->getBodyList();
	$drive_type_list   = $_CLASS->getDriveList();

	$product_option_list = $_CLASS->getProductOptionList();
    $country_list = $_CLASS->getCountryList();
	$vehicle_type_list = $_CLASS->getVehicleTypeList();
	//$car_maker_list = $_CLASS->getCarMakerList();
	$category_list = $_CLASS->getCategoryList();
	$fuel_type_list = $_CLASS->getFuelTypeList();
	$transmission_list = $_CLASS->getTransmissionList();
	$province_list = $_CLASS->getProvinceList();
    // set input size for maker.
    // ( count($maker_unique_list) > 0 ? $imaker = 'style="width:249px;margin-right:-1px;"' : $imaker = '' );
    // ( count($model_unique_list) > 0 ? $imodel = 'style="width:249px;margin-right:-1px;"' : $imodel = '' );
    ( count($body_type_list) > 0 ? $ibody = 'style="width:249px;margin-right:-1px;"' : $ibody = '' );
    if( CURRENCY_CODE == 'USD'){
        $decimal = '.';
        $thousand= ',';
    }
    else {
        $decimal = ',';
        $thousand= '.';
    }
    if(!isset($_GET['cid'])) $getId='';
	else $getId=$_GET['cid'];
	$product=array();

    // get generated car id.
	if(empty($getId)){
    	$carid = $_CLASS->getCarId();
	}else{
		$carid = $getId;
		$product = $_CLASS->getAllProducts($carid);
        $invalidProduct=$_CLASS->invalidProduct;
	}
	(isset($product['payment_terms']) ? $payment_terms = explode('^',$product['payment_terms']) : $payment_terms=array());
	(isset($product['other_options']) ? $other_options = explode('^',$product['other_options']) : $other_options=array());
	(isset($product['safety_device_options']) ? $safety_device_options = explode('^',$product['safety_device_options']) : $safety_device_options=array());
	(isset($product['exterior_options']) ? $exterior_options = explode('^',$product['exterior_options']) : $exterior_options=array());
	(isset($product['interior_options']) ? $interior_options = explode('^',$product['interior_options']) : $interior_options=array());



?>

<script type="text/javascript">
	var category="<?php if(isset($product['category'])) echo $product['category']; ?>";
	var subCategoryGroup="<?php if(isset($product['sub_category_group'])) echo $product['sub_category_group']; ?>";
	var subCategory="<?php if(isset($product['sub_category'])) echo $product['sub_category']; ?>";
	var madeIn="<?php if(isset($product['made_in'])) echo $product['made_in']; ?>";
	var condition="<?php if(isset($product['car_type'])) echo $product['car_type']; ?>";
	//var vehicleType="<?php if(isset($product['body_style'])) echo $product['body_style']; ?>";
	var make="<?php if(isset($product['make'])) echo $product['make']; ?>";
	var model="<?php if(isset($product['model'])) echo $product['model']; ?>";
	var year="<?php if(isset($product['year'])) echo $product['year']; ?>";
	var exterior_color="<?php if(isset($product['exterior_color'])) echo $product['exterior_color']; ?>";
	var measureType="<?php if(isset($product['mileage_type'])) echo $product['mileage_type']; ?>";
	var transmission="<?php if(isset($product['transmission'])) echo $product['transmission']; ?>";
	var fuelType="<?php if(isset($product['fuel_type'])) echo $product['fuel_type']; ?>";
    var currency="<?php if(isset($product['currency'])) echo $product['currency']; ?>";
	var country="<?php if(isset($product['location'])) echo $product['location']; ?>";
    var city="<?php if(isset($product['city'])) echo $product['city']; ?>";
	var door="<?php if(isset($product['door'])) echo $product['door']; ?>";
	var driveType="<?php if(isset($product['drivetrain'])) echo $product['drivetrain']; ?>";
	var driveAvail="<?php if(isset($product['drive_availability'])) echo $product['drive_availability']; ?>";
	var steering="<?php if(isset($product['desc1'])) echo $product['desc1']; ?>";
	var city="<?php if(isset($product['city'])) echo $product['city']; ?>";
	var engineSize="<?php if(isset($product['engine'])) echo $product['engine']; ?>";
	var bodyStyle="<?php if(isset($product['body_style'])) echo $product['body_style']; ?>";	
	var licencePaper="<?php if(isset($product['interior_color'])) echo $product['interior_color']; ?>";
	var licensePlate="<?php if(isset($product['doors'])) echo $product['doors']; ?>";
	

	$(document).ready(function() {
	  var nowTemp = new Date();
	    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

	  $('#manDateInput').datepicker({
	    format: "yyyy-mm",
	    viewMode: "months",
	    minViewMode: "months",
	    startDate: "-30y",
	    endDate: "now",
	    todayBtn: "linked",
	    autoclose: true,
	  });
});
</script>
		
        <div id='wrapper-edit-car'>
			<div class="marBT" >
				<div id="my_item">
					<div class="titleTop">
						<img class="img-responsive" src="<?php echo BASE_RELATIVE; ?>img/user_add_new/img_01.png" />
						<span><?php echo $_LANG['PRODUCT_ADD_NEW'];?> </span>
					</div>

					<div id="menuTab" class="clearfix">
						<ul>
							<!--<li><a id="cars" href="#" ><p>Car </p></a></li>
							<li><a href="<?php echo BASE_RELATIVE; ?>edit-bus"><p> Bus </p></a></li>
								<li><a href="<?php echo BASE_RELATIVE; ?>edit-truck"><p>Track</p></a></li>
								<li><a href="<?php echo BASE_RELATIVE; ?>edit-motorbike"><p>Motorbike</p></a></li>
							-->

						</ul>
					</div><!-- #menuTab -->


				</div>

				<!--For Mobile Version-->



				<div id="inner-content">
					<!-- START CONTENT -->
					<?php
						if( $fstatus ){
					?>
					<div class="alert <?php echo $fstyle;?>">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<?php echo $fmessage;?>
					</div>
					<?php
						}
					?>


					<form id="form-wrap" method="post" >

					<!----------------- FORM CONTAINER -------------------->
						<?php if(!$invalidProduct){ ?>
                        
						<input type="hidden" name="caridInput" id="hiddendusereditproductid" value="<?php echo $carid;?>" />
						<div id="content1">
							<table class="edit_product_table">
								<tr>
									<td class="edit_haft_row">
										<div class="add-on title-label"><?php echo $_LANG['PRODUCT_MAKER_LABEL'];?> <font color="#C1272D">*</font></div>

										<select id="makerInputSelect" required name="makerInputSelect" style="">
											<?php
												$_GET['product_type']="Car";
												include('ajax/get_product_make_options.php');
											?>
										</select>
										<input type="text" maxlength="30" id="makerInput" name="makerInput" class="content-label other_input" placeholder="Other Make" />									

									</td>
									<td class="edit_haft_row">
                                    
                                    		
                                        <div class="add-on title-label"><?php echo $_LANG['PRODUCT_DRIVE_LABEL'];?></div>
										<select id="driveInputSelect" name="driveInputSelect" style="">
											<option value="">- Select -</option>
											<?php
												for( $i=0; $i < count($drive_type_list); $i++ ){
											?>
											<option value="<?php echo $drive_type_list[$i];?>"><?php echo $drive_type_list[$i];?></option>
											<?php
												}
											?>
										</select>
										
									</td>
								</tr><tr>
									<td class="edit_haft_row">
										<div class="add-on title-label"><?php echo $_LANG['PRODUCT_MODEL_LABEL'];?> <font color="#C1272D">*</font></div>
										<select id="modelInputSelect" required name="modelInputSelect" style="">
											<option value="">- Select -</option>

										</select>
										<input type="text" maxlength="50" id="modelInput" name="modelInput" class="content-label other_input" placeholder="Other Model" />
	
									</td><td class="edit_haft_row">
										<div class="add-on title-label"><?php echo $_LANG['PRODUCT_CONDITION_COLOR'];?></div>
										<select id="bodyColorInput" name="bodyColorInput">
											<option value="">- Select -</option>
											<option value="White">White</option>
											<option value="Black">Black</option>
											<option value="Silver">Silver</option>
											<option value="Gold">Gold</option>
											<option value="Brown">Brown</option>
											<option value="Red">Red</option>
											<option value="Orange">Orange</option>
											<option value="Blue">Blue</option>
											<option value="Yellow">Yellow</option>
											<option value="Green">Green</option>
											<option value="Gray">Gray</option>
											<option value="Other">Other</option>
										</select>		
									</td>
								</tr><tr>
									<td class="edit_haft_row">
										<div class="add-on title-label"><?php echo $_LANG['PRODUCT_YEAR_LABEL'];?><font color="#C1272D">*</font></div>
										<select id="yearInput" name="yearInput" style="">
											<option value="">- Select -</option>
											<?php
											for( $i=date('Y'); $i > 1900; $i-- ){
											?>
												<option value="<?php echo $i;?>"><?php echo $i;?></option>
											<?php
												}
											?>
										</select>
										

									</td><td class="edit_haft_row">
										<div class="add-on title-label"><?php echo $_LANG['PRODUCT_PRICE_LABEL'];?></div>
										<input type="text" value="<?php if(isset($product['price'])) echo $product['price'];?>" id="priceInput" name="priceInput" class="content-label" placeholder="<?php echo $_LANG['PRODUCT_PRICE_PLACEHOLDER'];?>" onkeypress="return isNumberKey(event)" />
										<!--<select name="currencySelect" id="currencySelect">
											<?php include("ajax/load_currency_list.php");?>
										</select>-->
										
									</td>
								</tr><tr>
									<td class="edit_haft_row">
										<div class="add-on title-label"><?php echo $_LANG['PRODUCT_CONDITION_BODYTYPE'];?></div>

										<select id="vehicleTypeSelect" name="vehicleTypeSelect">
											<option value="">- Select -</option>
											<?php
												foreach($vehicle_type_list['type_name'] as $type_name){
											?>
												<option value="<?php echo $type_name;?>"><?php echo $type_name;?></option>
											<?php
												}
											?>
										</select>	

									</td>
									<td class="edit_haft_row">
                                    	<div class="add-on title-label"><?php echo $_LANG['PRODUCT_STEERING_LABEL'];?> <font color="#C1272D">*</font></div>
										<label><input type="radio" checked name="steeringInput" value="0"/><?php echo $_LANG['PRODUCT_STEERING_LHD'];?></label>
										<label><input type="radio" name="steeringInput" value="1"/><?php echo $_LANG['PRODUCT_STEERING_RHD'];?></label>
                                    	
									</td>
								</tr><tr>
									<td class="edit_haft_row">
										<div class="add-on title-label"><?php echo $_LANG['PRODUCT_CONDITION_ENGINESIZE'];?><font color="#C1272D">*</font></div>
										<!--<input type="text" value="<?php //if(isset($product['engine_model'])) echo $product['engine_model'];?>" maxlength="50" name="engineModelInput" class="content-label" placeholder="E001" />-->
										<select name="engineVolumeInput" id="engineModelInput">
											<option value="">- Select -</option>
											<option value="1.1">1.1</option>
											<option value="1.2">1.2</option>
											<option value="1.3">1.3</option>
											<option value="1.4">1.4</option>
											<option value="1.5">1.5</option>
											<option value="1.6">1.6</option>
											<option value="1.7">1.7</option>
											<option value="1.8">1.8</option>
											<option value="1.9">1.9</option>
											<option value="2.0">2.0</option>
											<option value="2.1">2.1</option>
											<option value="2.2">2.2</option>
											<option value="2.3">2.3</option>
											<option value="2.4">2.4</option>
											<option value="2.5">2.5</option>
											<option value="2.6">2.6</option>
											<option value="2.7">2.7</option>
											<option value="2.8">2.8</option>
											<option value="2.9">2.9</option>
											<option value="3.0">3.0</option>
											<option value="3.1">3.1</option>
											<option value="3.2">3.2</option>
											<option value="3.3">3.3</option>
											<option value="3.4">3.4</option>
											<option value="3.5">3.5</option>
											<option value="3.6">3.6</option>
											<option value="3.7">3.7</option>
											<option value="3.8">3.8</option>
											<option value="3.9">3.9</option>
											<option value="4.0">4.0</option>
											<option value="4.1up">4.1up</option>
										</select>		
									</td>
									<td class="edit_haft_row">
                                    	
                                        <div class="add-on title-label"><?php echo $_LANG['PRODUCT_CONDITION_LABEL'];?> <font color="#C1272D">*</font></div>
										<label><input type="radio" checked name="conditionInput" value="0"/><?php echo $_LANG['PRODUCT_CONDITION_USED'];?></label>
										<label><input type="radio"  id="conditionInput" name="conditionInput" value="1"/><?php echo $_LANG['PRODUCT_CONDITION_NEW'];?></label>	
												
									</td>
								</tr><tr>
									<td class="edit_haft_row">
                                    	<div class="add-on title-label"><?php echo $_LANG['PRODUCT_TRANSMISSION_LABEL'];?><font color="#C1272D">*</font></div>
										<select id="transmissionInputSelect" name="transmissionInputSelect" style="">
											<option value="">- Select -</option>
											<?php
												if( count($transmission_list['transmission_name']) > 0 ){

													for( $i=0; $i < count($transmission_list['transmission_name']); $i++ ){
														if( !empty($transmission_list['transmission_name'][$i]) ){											 
														
												?>
												<option value="<?php echo $transmission_list['transmission_name'][$i];?>">
												    <?php 
															if($transmission_list['transmission_name'][$i]=="AT" and $_LANG['PRODUCT_AT']=="ប្រអប់លេខអូតូ"){
																echo "ប្រអប់លេខអូតូ";
															}
															elseif($transmission_list['transmission_name'][$i]=="AT" and $_LANG['PRODUCT_AT']=="AT" ){
																echo "AT";
															}
															else{
															    if($_LANG['PRODUCT_MT']=="ប្រអប់លេខដៃ"){
																   echo $_LANG['PRODUCT_MT'];
																}
																else{
																	echo $_LANG['PRODUCT_MT'];
																}
															}
													?>    
												</option>
											<?php
														}
													}
												}
												else {
											?>
												<option value="0"></option>
											<?php
												}
											?>
										</select>
                                    	
										<!--<div class="add-on title-label"><?php echo $_LANG['PRODUCT_MILES_LABEL'];?></div>
										<input type="text" value="<?php if(isset($product['mileage'])) echo $product['mileage'];?>" placeholder="<?php echo $_LANG['PRODUCT_MILES_PLACEHOLDER'];?>" id="milesInput" name="milesInput" class="content-label" <?php /*?>onkeypress="return isNumberKey(event)"<?php */?> />
										<label><input type="radio" checked value="Km" name="measureTypeInput"/>Km</label>
										<label><input type="radio" value="Mile" name="measureTypeInput"/>Mile</label>-->
									</td>
									<td class="edit_haft_row">
										<div class="add-on title-label"><?php echo $_LANG['PRODUCT_LICENSE_PLATE_LABEL'];?> <font color="#C1272D">*</font></div>
										<label><input type="radio" checked  name="license_plate" value="0"/><?php echo $_LANG['PRODUCT_LICENSE_PLATE_NO'];?></label>	

										<label><input type="radio"  name="license_plate" value="1"/><?php echo $_LANG['PRODUCT_LICENSE_PLATE_YES'];?></label>										
										
									</td>
								</tr><tr>
									<td class="edit_haft_row">
                                    	
										<div class="add-on title-label"><?php echo $_LANG['PRODUCT_FUEL_LABEL'];?><font color="#C1272D">*</font></div>
										<select id="carFuelInputSelect" name="carFuelInputSelect" style="">
											<option value="">- Select -</option>
											<?php
												if( count($fuel_type_list['fuel_name']) > 0 ){

													for( $i=0; $i < count($fuel_type_list['fuel_name']); $i++ ){
														if( !empty($fuel_type_list['fuel_name'][$i]) ){
												?>
												<option value="<?php echo $fuel_type_list['fuel_name'][$i];?>"><?php echo $fuel_type_list['fuel_name'][$i];?></option>
											<?php
														}
													}
												}
												else {
											?>
												<option value="0"></option>
											<?php
												}
											?>
										</select>
										<!--<div class="add-on title-label"><?php echo $_LANG['PRODUCT_CONDITION_CYLINDERCAPACITY'];?></div>
										<input type="text" value="<?php if(isset($product['engine_volume'])) echo $product['engine_volume'];?>" id="engineVolumeInput" name="engineVolumeInput" class="content-label" placeholder="<?php echo $_LANG['PRODUCT_ENGINE_VOLUME_PLACEHOLDER'];?>" onkeypress="return isNumberKey(event)" /> CC-->
										
									</td>
									
									<td class="edit_haft_row" colspan="2">
										<div class="add-on title-label"><?php echo $_LANG['PRODUCT_LICENSE_PAPER_LABEL'];?> <font color="#C1272D">*</font></div>
                                        <label><input type="radio" checked name="licence_paper" value="0"/><?php echo $_LANG['PRODUCT_LICENSE_PAPER_YES'];?></label>
                                        
                                        <label><input type="radio" name="licence_paper" value="1"/><?php echo $_LANG['PRODUCT_LICENSE_PAPER_NO'];?></label>	
												
									</td>
								</tr>
								<tr>
									<td class="edit_haft_row">
                                    	<div class="add-on title-label"><?php echo $_LANG['PRODUCT_PROVINCE_LIST_LABEL'];?></div>
										<select id="provinceName" required name="provinceName">
											<option value="">- Select -</option>
                                            <?php
												if( count($province_list['province_name']) > 0 ){
													for( $i=0; $i < count($province_list['province_name']); $i++ ){
														if( !empty($province_list['province_name'][$i]) ){
											?>
														<option value="<?php echo $province_list['province_name'][$i];?>">
															<?php echo $province_list['province_name'][$i]?>
														</option>
											<?php
														}
													}
												}
											?>
										</select>                                    
										<!--<div class="add-on title-label"><?php echo $_LANG['PRODUCT_CONDITION_CYLINDERCAPACITY'];?></div>
										<input type="text" value="<?php if(isset($product['engine_volume'])) echo $product['engine_volume'];?>" id="engineVolumeInput" name="engineVolumeInput" class="content-label" placeholder="<?php echo $_LANG['PRODUCT_ENGINE_VOLUME_PLACEHOLDER'];?>" onkeypress="return isNumberKey(event)" /> CC
										-->
										
									</td>
									<td class="edit_haft_row">
										
										
									</td>
								</tr>
                                
                                
                                <!--// Disable Exterior and Interior option
                                
                                <tr>
									<td style="border-right:none;padding-left:10px" colspan="2" class="edit_haft_row">
										<div class="add-on block-label"><?php echo $_LANG['PRODUCT_EXTERIOR_OPTIONS'];?></div>
										<?php foreach($product_option_list['Exterior Option'] as $option){ 
											if(in_array($option, $exterior_options)){
												$check="checked";
											}else{
												$check="";
											}
										?>
											<div class="product_option_container"><label><input value="<?php echo htmlspecialchars($option);?>" <?php echo $check;?> type="checkbox" name="exOptionsCheck[]"><?php echo htmlspecialchars($option);?></label></div>
										<?php } ?>
									</td>
								</tr>
                                <tr>
									<td style="border-right:none;padding-left:10px" colspan="2" class="edit_haft_row">
										<div class="add-on block-label"><?php echo $_LANG['PRODUCT_INTERIOR_OPTIONS'];?></div>
										<?php foreach($product_option_list['Interior Option'] as $option){ 
											if(in_array($option, $interior_options)){
												$check="checked";
											}else{
												$check="";
											}
										?>
											<div class="product_option_container"><label><input value="<?php echo htmlspecialchars($option);?>" <?php echo $check;?> type="checkbox" name="inOptionsCheck[]"><?php echo htmlspecialchars($option);?></label></div>
										<?php } ?>
									</td>
								</tr>-->
                                <tr>
									<td style="border-right:none;padding-left:10px" colspan="2" class="edit_haft_row">
										<div class="add-on block-label" style="padding-top:30px"><?php echo $_LANG['PRODUCT_Description'];?></div>
										<textarea id="sellerCommentText" name="sellerCommentText" class="tinymce content-label"><?php if(isset($product['notes'])) echo $product['notes'];?></textarea>
									</td>
								</tr>
								<tr>
									<td colspan="2" >
										<div id="video-wrap" style="display:none;" class="input-prepend input-append inputap">
											<div class="add-on title-label"><?php echo $_LANG['PRODUCT_YOUTUBE_URL_LABEL'];?></div>
											<input id="videoUrlInput" type="text" class="content-label" style="width:435px;" placeholder="<?php echo $_LANG['PRODUCT_YOUTUBE_URL_PLACEHOLDER'];?>" />
											<input id="vidBtn" onclick="saveVideoHandler('<?php echo $carid;?>');return false;" type="submit" class="btn btn-info" style="width:100px;" value="<?php echo $_LANG['PRODUCT_SAVE_BUTTON'];?>" />
										</div>
										<div id="upload-wrap" style="display:none;float:left;width:100%;height:70px" >
											<h4 style="padding:10px;"><?php echo $_LANG['PRODUCT_UPLOAD_LABEL'];?></h4>
											<iframe id="iupload" width="90%" style="border:0;float:left"></iframe>
											<input style="float:right;" id="submitbtn" type="submit" name="savebtn" class="btn btn-small btn-info linkimg" value="<?php echo $_LANG['PRODUCT_SAVE_BUTTON'];?>" />
										</div>

									</td>
								</tr>
							</table>

						</div>
						<br/>
						<p id="alter"></p>
						<div id="outputwrap"></div>
						<div id="outoutimg" class="clearfix"></div>
						<div class="clearfix"></div>
						<br/>
						<a id="prevbtn" href="#" onclick="return previousFormHandler()" class="btn btn-small" style="display:none;margin-right:10px;">
							<i class="icon-circle-arrow-left" style="margin-right:10px;"></i><?php echo $_LANG['PRODUCT_PREVIOUS_BUTTON_LABEL'];?>
						</a>
						<a id="nextbtn" href="#" onclick="return nextFormHandler()" class="btn btn-small" style="display:none;margin-right:10px;">
							<?php echo $_LANG['PRODUCT_NEXT_BUTTON_LABEL'];?><i class="icon-circle-arrow-right" style="margin-left:10px;"></i>
						</a>
						<!--For Auto Save Disable And Save Button-->
						<?php
						 $style_visible="inline";
						/* if(isset($_GET['cid'])){
							 $style_visible="inline";
						 }
						 else{
							 $style_visible="none";
						 }*/
						?>


						  <!--End Auto Save Disable And Save Button-->

						<!-------------- END UPLOAD MEDIA BLOCK ---------->
						<!----------------- END FORM CONTAINER -------------------->
						<?php
						}
						else{
							echo "<font color='#C1272D'>Product with this ID does not exist!</font>";
						}
						?>

					</form>

					<!-- END CONTENT -->


				<!--==== END UPLOAD MEDIA BLOCK ====-->
				</div>

				<div class="clearfix"></div>
				<div id="content2">

				</div>

				<div class="clearfix"></div>
			</div><!-- .row -->
        </div><!--- #content-wrapper -->
		
        <script language="javascript" type="text/javascript">
        $("#cars").click(function(event){
       $("#war-paper").removeAttr("style");
       $("#car_over").remove();


                 });
            /* preview uploaded image */

            $(document).ready(function(e){
                // $('#uploading').hide();
                // $('#submitbtn').css('float', 'left');

                displayController('<?php echo BASE_RELATIVE;?>view/3rdparty/file_uploader/user-addpic.php?cid=<?php echo $carid;?>');

            });


			$("#submitbtn").submit(function(event){
                    if($('#makerInputSelect').val()=="" || $('#modelInputSelect').val()=="" || $('#countryInputSelect').val()=="" ){
                    alert("Please Input Data");

                    return false;
                   }
                 });

            function basename(path) {
               return path.split('/').reverse()[0];
            }

			function save_data_form(){

			 	$('#submitbtn').trigger('click');


			}
		    function check_vehicle_data(){
				 var data_return=false;
				 if($('#makerInputSelect').val()=="" || $('#modelInputSelect').val()=="" || $('#countryInputSelect').val()=="" ){
                    alert("Please Input Data");
					data_return= false;
                    return false;
				 }
				 else{
					 data_return= true;
				 }
				 return data_return;


			}

			 function carImageUploadedNew_Photo(id,proid,url,mode,fname,i){

                        var did = 'img' + Math.round((Math.random()*1000));
                        var rd='rd' + id;

                        var wrap = document.getElementById('outoutimg');
                        var html = '<div id="'+did+'" class="img-wrap">';
                        html += '<img src="'+url+'" width="200" height="150" style="margin:5px;" />';
                        html += '<p style="margin:0;padding-left:5px;">';

						if(i==0){

						   html +='<input type="radio"  onclick="checkimg(\''+id+'\',\''+proid+'\',\''+rd+'\');return true;" id='+rd+' name="primary_photo[]" value="'+fname+'"  checked="checked"  />Set as Primary Photo';
            		}
                        else{
					       html +='<input type="radio" onclick="checkimg(\''+id+'\',\''+proid+'\',\''+rd+'\');return true;" id='+rd+' name="primary_photo[]" value="'+fname+'"  />Set as Primary Photo';
            			}
                        html += '<a href="#" onclick="removeImageHandler(\''+id+'\',\''+basename(url)+'\',\''+did+'\',\''+rd+'\');return false;" class="btn btn-small btn-danger" style="float:right;margin-right:5px;">×</a>';
                        html += '</p>';
                        html += '</div>';

                        wrap.innerHTML += html;

                    }
                    function ucfirst (str) {
                        str += '';
                        var f = str.charAt(0).toUpperCase();
                        return f + str.substr(1);
                   }

            function carImageUploaded(id,proid,url,mode,fname,primary_photo){
                        var did = 'img' + Math.round((Math.random()*1000));
                        var rd='rd' + id;
                        var wrap = document.getElementById('outoutimg');
                        var html = '<div id="'+did+'" class="img-wrap">';
                        html += '<img src="'+url+'" width="200" height="150" style="margin:5px;" />';
                        html += '<p style="margin:0;padding-left:5px;">';

						if(primary_photo==1){

							 html +='<input type="radio"  onclick="checkimg(\''+id+'\',\''+proid+'\',\''+rd+'\');return true;" id='+rd+' name="primary_photo[]" value="'+fname+'"  checked="checked"  />Set as Primary Photo';
						}
                        else{
							 html +='<input type="radio" onclick="checkimg(\''+id+'\',\''+proid+'\',\''+rd+'\');return true;" id='+rd+' name="primary_photo[]" value="'+fname+'"  />Set as Primary Photo';
						}
                        html += '<a href="#" onclick="removeImageHandler(\''+id+'\',\''+basename(url)+'\',\''+did+'\',\''+rd+'\');return false;" class="btn btn-small btn-danger" style="float:right;margin-right:5px;">×</a>';
                        html += '</p>';
                        html += '</div>';

                        wrap.innerHTML += html;
                    }
                    function ucfirst (str) {
                        str += '';
                        var f = str.charAt(0).toUpperCase();
                        return f + str.substr(1);
                   }
        </script>
