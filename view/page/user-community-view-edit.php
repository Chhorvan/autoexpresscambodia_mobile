<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = true;
$_CKEDITOR_ID = '_ebody';

// get form feed.
$fstatus = $_CLASS->getFormStatus();
$fmessage = $_CLASS->getFormMessage();
$fstyle = $_CLASS->getFormStyle();
$group = 'user';
$slug  = 'car';
$get_dealweek = $_CLASS->loadDealweek($_GET['id']);
$get_register = $_CLASS->loadUserName($_GET['owner']);

 

if($get_register['id']!=$_SESSION['log_id']){    
    header("location:dealof-theweek");
}
else{
    if(isset($_POST['update_dealweek'])){
        $_CLASS->updateDealWeek($_SESSION['log_name'],$get_register['name']);
        $status = $_CLASS->getstatus();
        if( $status == 'ok' )
        {
          FeedbackMessage::displayOnPage('Submit Sucessfully', 'success');
        ?>
        <p class="alert alert-success" style="text-align:center;"><strong><?php echo  "SUBMIT COMPLETE";?></strong></p>

        <?php
            }

        if($status=='error'){
            ?>    
            <p class="alert alert-error" style="text-align:center;"><strong><?php echo 'You are not the owner of this product.';?></strong></p>

                <?php
        }
    }    
        
}





$str = $get_dealweek['date'];  
$i = strrpos($str, " ");
$l = strlen($str) - $i;
$str = substr($str, $i, $l);
$date = str_ireplace($str, "", $get_dealweek['date']);

// load page content.

$pg_details = $_CLASS->getPageHTML();

?>
<script type="text/javascript" src="<?php echo BASE_RELATIVE;?>js/blog/common.js"></script>
<script type="text/javascript">
$('#ebody').ready(function(){
    CKEDITOR.config.height = 600;
    CKEDITOR.config.toolbar = 'Cms';
    CKEDITOR.config.toolbar_Cms =
    [
        { name: 'document', items : [ 'Source','DocProps','Print','-','Templates' ] },
	{ name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
	{ name: 'editing', items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
	'/',
	{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
	{ name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv',
	'-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
	{ name: 'links', items : [ 'Link','Unlink','Anchor' ] },
	{ name: 'insert', items : [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe' ] },
	{ name: 'styles', items : [ 'Styles','Format','Font','FontSize' ] },
	{ name: 'colors', items : [ 'TextColor','BGColor' ] },
	{ name: 'tools', items : [ 'Maximize', 'ShowBlocks' ] }
    ];
	

});
</script>

<link rel="stylesheet" href="<?php echo BASE_RELATIVE;?>css/community/community.css" />

 <div id="sectionContenWrapper">
        	<!--sectionSidebar--> 
            <div id="topMenu">
            	<p><a href="#" class="linkfade">Community</a> > <a href="#" class="linkfade">Deal of the Week</a> > View</p>
            </div>
        	<?php include("php/sidebar/community.php");?>
            
            </div>           
            <div id="sectionContent"> 
            	<div id="community_title">
                	<p>Deal of the Week</p>
                </div>
                <div id="community_view" class="clearfix">
                	<div id="logo">
                        <?php                                       
                            if(!file_exists('upload/'.$get_register['image']) || $get_register['image']==""){                                         
                        ?>
                                <img src="<?php echo BASE_RELATIVE;?>images/noimg.jpg" width="40" height="40" />
                        <?php                                           
                            }else{
                        ?>
                                <img src="<?php echo BASE_RELATIVE;?>upload/<?php echo $get_register['image'];?>"  width="40" height="40" />
                        <?php
                            }
                        ?>
                    	
                    </div>
                    <div id="detail">
                    	<p class="title">
                            <?php echo $get_dealweek['title']?>
                        </p>
                        <p>Written by : <span class="writer"><?php echo $get_register['name']?></span>	Date : <?php echo $date?>	<span>Click : 10</span></p>
                    </div>
                    <form action="<?php echo BASE_RELATIVE;?>community-view-edit?id=<?php echo $_GET['id'];?>&owner=<?php echo $_GET['owner'];?>" method ="post" enctype="multipart/form-data">
                    <div id="img_wrap" class="clearfix">
                        <div id="title_text"><p style="color:#424242;">Title</p></div>                   
                        <input type="text" class="title_textbox" name="rd_title" value="<?php echo $get_dealweek['title']?>" />
                        <input type="hidden"  name="get_id" value="<?php echo $_GET['id'];?>"  />
                        
                        <div style="clear:both"></div>
                        <div id="text_editor" style="margin-top:20px;">
                            <textarea id="_ebody" name="text_message">
                                <?php echo $get_dealweek['message'];?>
                            </textarea>
                            
                            <div class='file_upload' id='f1' style="margin-top:20px;"><input name='myfile' type='file'  /></div>
                            <div id='file_tools' style="padding:10px; float:left; margin-top:20px;">                         
                                <p> Select your file</p>
                            </div>
                        </div>                    	
                    </div>
                             
                </div>
                <div id="button_wrap">                    	
                        <input type="submit" class="update_deal" name="update_dealweek" value=""/>
                        </form>
                        <div id="back">
                        	<a href="<?php echo BASE_RELATIVE;?>dealof-theweek"><img src="<?php echo BASE_RELATIVE;?>images/community/back_to_list.png" /></a>
                        </div>
                </div>
            </div><!-- end div id="sectionContent"-->
        </div><!-- end div id="sectionContentWraper" -->       
  
  
  