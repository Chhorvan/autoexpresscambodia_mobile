<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = true;
$_CKEDITOR_ID = '_ebody';
// get form feed.
$fstatus = $_CLASS->getFormStatus();
$fmessage = $_CLASS->getFormMessage();
$fstyle = $_CLASS->getFormStyle();
$group = 'public';
$slug  = 'car';
// load page content.
$pg_details = $_CLASS->getPageHTML();
$status = $_CLASS->getstatus();
$product_maker = $_CLASS->loadProduct();
$get_autoinfo = $_CLASS->loadAutoInfo($_GET['id']);


$str = $get_autoinfo['date'];  
$i = strrpos($str, " ");
$l = strlen($str) - $i;
$str = substr($str, $i, $l);
$date = str_ireplace($str, "", $get_autoinfo['date']);

?>
<script type="text/javascript" src="<?php echo BASE_RELATIVE;?>js/blog/common.js"></script>
<script type="text/javascript">
$('#ebody').ready(function(){
    CKEDITOR.config.height = 600;
    CKEDITOR.config.toolbar = 'Cms';
    CKEDITOR.config.toolbar_Cms =
    [
        { name: 'document', items : [ 'Source','DocProps','Print','-','Templates' ] },
	{ name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
	{ name: 'editing', items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
	'/',
	{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
	{ name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv',
	'-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
	{ name: 'links', items : [ 'Link','Unlink','Anchor' ] },
	{ name: 'insert', items : [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe' ] },
	{ name: 'styles', items : [ 'Styles','Format','Font','FontSize' ] },
	{ name: 'colors', items : [ 'TextColor','BGColor' ] },
	{ name: 'tools', items : [ 'Maximize', 'ShowBlocks' ] }
    ];
	

});
</script>

<link rel="stylesheet" href="<?php echo BASE_RELATIVE;?>css/community/community.css" />

 <div id="sectionContenWrapper">
        	<!--sectionSidebar--> 
            <div id="topMenu">
            	<p><a href="#" class="linkfade">Community</a> > Auto Infomation</p>
            </div>
        	<?php include("php/sidebar/community.php");?>
            
            </div>           
            <div id="sectionContent"> 
            	<div id="community_title">
                	<p>Auto Informatino</p>
                </div>
                <div id="register_info">
                	<form action="<?php echo BASE_RELATIVE;?>register-info" method="post" enctype="multipart/form-data">
					
                    <div id="table_wrap">
                        <p class="title"><?php echo $get_autoinfo['topic'];?></p>
                        <p>Writen by: <span style="color:#FB5B1C; padding-right:10px;"><?php echo $get_autoinfo['writen_by'];?></span> Date: <span><?php echo $date;?></span></p>
                    	<!-- <table border="0" cellpadding="0" cellspacing="0">
                        	<tr>
                            	<td class="label">Item</td>
                                <td>                                	
                                    <?php echo $get_autoinfo['item'];?>   
                                </td>
                            </tr>
                            <tr>
                            	<td class="label">Maker</td>
                                <td>                                	
                                    <?php echo $get_autoinfo['make'];?>                                    
                                </td>
                            </tr>
                            <tr>
                            	<td class="label">Model</td>
                                <td>
                                	<?php echo $get_autoinfo['model'];?>
                                </td>
                            </tr>
                             <tr>
                            	<td class="label">Title</td>
                                <td>
                                	<?php echo $get_autoinfo['topic'];?>
                                </td>
                            </tr>
                        </table> -->
                    </div>
                    <div id="text_editor">
                    	<p>Message</p>
                        <?php echo $get_autoinfo['message'];?>
                    </div>
                    
                    <div id="button_wrap">
                    	<div id="upload">
                        	<div id="update">
                                <a href="<?php echo BASE_RELATIVE;?>edit-autoinfo?id=<?php echo $_GET['id'];?>&owner=<?php echo $get_autoinfo['owner'];?>"><img src="<?php echo BASE_RELATIVE;?>images/community/update.png" /></a>
                            </div>
                            <div id="back">
                            	<a href="<?php echo BASE_RELATIVE;?>auto-info"><img src="<?php echo BASE_RELATIVE;?>images/community/back.png" /></a>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div><!-- end div id="sectionContent"-->
        </div><!-- end div id="sectionContentWraper" -->       
  
  
  