<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';

/*
 * Set security question variables ---------------------------------------------------
 */
$_SESSION['security_question_a'] = mt_rand('1','9');
$_SESSION['security_question_b'] = mt_rand('1','9');
$countryList=$_CLASS->getCountryList();


include('php/ip2locationlite.class.php');
//Load the class
$ipLite = new ip2location_lite;
$ipLite->setKey('0ceddca8fc2527329d305fb922e7bbade230ca741a2a68055b649696326992be');

//Get errors and locations
$locations = $ipLite->getCity($_SERVER['REMOTE_ADDR']);
$errors = $ipLite->getError();
?>


<?php
	// if form is submit, get registration status to display feed
	// back message.
	$status = $_CLASS->getRegistrationStatus();

	if( !empty($status) )
	{
		if( $status == 'complete' )
		{
			FeedbackMessage::displayOnPage($_LOCAL['REGISTER_COMPLETE_STATUS'], 'success');
?>
 
<?php
		}
		else if($status == 'invalidUserId')
		{
			FeedbackMessage::displayOnPage("Invalid User ID format");
?>
<?php
		}
		else if($status == 'query')
		{
			FeedbackMessage::displayOnPage($_LOCAL['REGISTER_ERROR_STATUS']);
?>
<?php
		}
		else if($status == 'errorinput')
		{
			FeedbackMessage::displayOnPage("Incorrect security code");
					
		
?>
<?php
		}
		else if($status == 'errorblank')
		{
			FeedbackMessage::displayOnPage("Security code can't be blank");
					
		
?>
<?php
		}
		else if($status == 'email' )
		{
			FeedbackMessage::displayOnPage($_LOCAL['REGISTER_EMAIL_ERROR_STATUS']);
?>
<?php
		}
		else if($status == 'log_id' )
		{
			FeedbackMessage::displayOnPage($_LOCAL['REGISTER_ID_EXIST_STATUS']);
?>
<?php
		}
		else if($status == 'upload' )
		{
			FeedbackMessage::displayOnPage($_LOCAL['REGISTER_UPLOAD_ERROR_STATUS']);
?>
<?php
		}
		else if($status == 'upload_error1' )
		{
			FeedbackMessage::displayOnPage("error1");
?>
<?php
		}
		else if($status == 'upload_error2' )
		{
			FeedbackMessage::displayOnPage("error2");
?>
<?php
		}
		else
		{
			FeedbackMessage::displayOnPage($_LOCAL['REGISTER_FIELDALLFIELDS_LABEL']);
?>
<?php
		}
	}
?>

<script type="text/javascript">
	$( document ).ready(function() {
	$('select#country').change(function(){
		var country=$(this).val();
		$('select#country').css('background','url(images/flag/'+country+'.png) no-repeat 10px center');
		$('select#country').css('border','1px solid #abadb3');
		$('select#country').css('padding-left','30px');
	});
});
</script>

<!--/END OF ORIGINAL CODE-->

<?php if ($_SESSION['mobile']==false) {?>
<div class="container" style="height:500px;">
<?php }
else{
?>
<div class="container" style="width:100%;">
<?php

}?>
	<?php if ($_SESSION['mobile']==false) {?>
	<?php /*include('php/public-register-left-sidebar.php')*/;?>
	<?php }?>
	<div class="content-wrapper"><form method="post" onsubmit="return validateRegisterForm();" enctype="multipart/form-data" >

		<div class="page-title">
			<table><tr><td><img class="brick" src="<?php echo BASE_RELATIVE;?>images/common/register_item/blue-brick.png" /></td><td>Register</td></tr></table>
		</div>
        
        <div><span class="fillin-title">Personal Information</span> <span class="fillin-title-right">Contact Info</span></div>
		
        <div class="buttom-wrapper">
			<div id="information" class="border_right">
		<table class="fillin-table vehicle-info-table">
			
			<tr>
				<td class="left">ID<font color="#C1272D">*</font></td>
				<td id="top">

					<input type="text" id="id" name="log_id" required placeholder="4-12 characters" value="<?php echo ( isset($_SESSION['reg_log_id']) ? $_SESSION['reg_log_id'] : '' );?>" />
					<span class="error" id="id_error"><?php echo $_LOCAL['REGISTER_ID_STATUS']; ?></span>
					<span class="error" id="id_length_error"><?php echo $_LOCAL['REGISTER_ID_LENGTH_STATUS']; ?></span>
					<span class="error" id="id_exist_error"><?php echo $_LOCAL['REGISTER_ID_EXIST_STATUS']; ?></span>
					<span class="error" id="vehicle_availability_result"></span>
					<div class="input-note">Containing only letters, numbers and underscores</div>
				</td>
			</tr>
			<tr>
				<td class="left">Email<font color="#C1272D">*</font></td>
				<td>
					<input type="email" required name="email" placeholder="Please enter your valid email address" />
					<span class="error" id="email_error"><?php echo $_LOCAL['REGISTER_EMAIL_STATUS']; ?></span>
					<span class="error" id="invalid_email_error"><?php echo $_LOCAL['REGISTER_INVALID_EMAIL_STATUS']; ?></span>
					<div class="input-note">If not, registration will be cancelled.</div>
				</td>
			</tr>
			<tr>
				<td class="left">Password<font color="#C1272D">*</font></td>
				<td>
					<input type="password" name="password" required id="password" placeholder="4-12 characters" onkeypress="capLock(event)"/>
					<div id="divPass" style="visibility:hidden">Caps Lock is on.</div>
					<span class="error" id="password_error"><?php echo $_LOCAL['REGISTER_PASSWORD_STATUS']; ?></span>
				</td>
			</tr><tr>
				<td class="left">Confirm Password<font color="#C1272D">*</font></td>
				<td>
					<input type="password" required name="confirm_password" placeholder="Please enter your password again." id="confirm_password" onkeypress="capLock1(event)" />
					<div id="divPass1" style="visibility:hidden">Caps Lock is on.</div>
					<span class="error" id="confirm_password_error"><?php echo $_LOCAL['REGISTER_CONFIRMPASSWORD_STATUS']; ?></span>
					<!--<div class="input-note">Please enter your password again.</div>-->
				</td>
			</tr>
			<tr class="radio-row">
				<td class="left">Member Type<font color="#C1272D">*</font></td>
				<td id="buttom">
					<label><input type="radio" id="member_type_1_0" value="Buyer" name="member_type_1[0]" <?php echo ( isset($_SESSION['reg_member_type_1_0']) ? 'checked="checked"' : '' ); ?> /><span>Buyer</span></label>
					<label><input type="radio" id="member_type_1_1" value="Seller" name="member_type_1[0]" <?php echo ( isset($_SESSION['reg_member_type_1_1']) ? 'checked="checked"' : '' ); ?> /><span>Seller</span></label>
					<label><input type="radio" id="member_type_1_2" value="Both" name="member_type_1[0]" <?php echo ( isset($_SESSION['reg_member_type_1_2']) ? 'checked="checked"' : '' ); ?> /><span>Both</span></label>
					<!-- <label><input type="checkbox" id="member_type_1_2" value="Forwarder" name="member_type_1[2]" <?php echo ( isset($_SESSION['reg_member_type_1_2']) ? 'checked="checked"' : '' ); ?> /><span>Forwarder</span></label> -->
					<span class="error" id="member_type_1_error"><?php echo $_LOCAL['REGISTER_MEMBER_STATUS'];?></span>
				</td>
			</tr>
			
		</table>
        
		
			</div>
 
			<div id="contect" class="padding_right">
				<!--<div class="fillin-title">Contact Info.</div>-->
		<table class="fillin-table vehicle-contact-table">
			<tr>
				<td>Company Name</td>
				<td id="top">
					<input type="text" id="com_name" name="company_name" value="<?php echo ( isset($_SESSION['reg_company_name']) ? $_SESSION['reg_company_name'] : '' ); ?>" />
                    <input type="hidden" name="reg_name" id="reg_name" value="" />
				</td>
			</tr>
            <tr>
            	<td>Contact Person</td>
                <td>
                	<input type="text" name="reg_name" value="<?php echo ( isset($_SESSION['reg_name']) ? $_SESSION['reg_name'] : '' ); ?>" />
                 </td>
            </tr>
			<tr>
				<td>Country<font color="#C1272D">*</font></td>
				<td>


                	<?php
						$countryCode = '';
						//Getting the result
						if (!empty($locations) && is_array($locations)) {
						  $countryCode= strtolower($locations['countryCode']);
						  $_SESSION['countryCode']=	$countryCode;
						}

					?>



					<select id="country"  required name="country" style="color:#424242; text-transform:uppercase; background:url(images/flag/<?php echo $_SESSION['countryCode'].'.png'; ?>) no-repeat 10px center; padding-left:30px; border:1px solid #abadb3; ">


						<?php foreach($countryList as $country){?>

                        <option label="<?php echo $country['phonecode']?>"  value='<?php echo $country['cc']; ?>'<?php if($country['cc'] == $countryCode ){echo " selected=selected ";}?>style='background:url(images/flag/<?php echo $country['cc'].'.png'; ?>) no-repeat left center; padding-left:20px;'> <?php echo $country['country_name'];?></option>

						<?php }?>
					</select>

					<span class="error" id="country_error"><?php echo $_LOCAL['REGISTER_COUNTRY_STATUS']; ?></span>
				</td>
			</tr>
            	
			<tr>
				<td>Mobile <p class="phonecode"></p></td>
				<td>
					<input type="text"  name="mobile" id="mobile" value="<?php echo ( isset($_SESSION['reg_mobile']) ? $_SESSION['reg_mobile'] : '' ); ?>" placeholder="Please enter your phone number"/>
                    <input type="hidden" id="phonecode" name="phonecode" value=""/>
                    <span class="error" id="mobile_error"><?php echo $_LOCAL['REGISTER_MOBILE_STATUS']; ?></span>
					<span class="error" id="invalid_mobile_error"><?php echo $_LOCAL['REGISTER_INVALID_MOBILE_STATUS']; ?></span>
				</td>
			</tr>		
            	
            <!--<tr>
            	<th>abcd</th>
            	<td>
                <input type="text" id="abc" value=""/>
                </td>
            </tr>-->
            	
			<tr>
				<td><?php echo $_LOCAL['REGISTER_SECURITYQUESTION_LABEL'];?><font color="#C1272D">*</font></td>
				<td>
				<?php echo $_LOCAL['REGISTER_SECURITYQUESTION_QUESTION'];?>:&nbsp;<?php echo $_SESSION['security_question_a'];?> + <?php echo $_SESSION['security_question_b'];?>?
				<input type="text" required id="inputsecurity" style="width: 120px" name="securityInput" placeholder="<?php echo $_LOCAL['REGISTER_SECURITYQUESTION_LABEL'];?>" />
				<span id="fsecurity"><?php echo $_LOCAL['REGISTER_SECURITYQUESTION_STATUS'];?></span>
				</td>
			</tr>
			<tr class="agreement-row">
				<td colspan="2" id="buttom_right">
					<label>
						<input type="checkbox" name="term" value="accept" />
						<span>I agree with the <a href="#"><font color="#C1272D">[Terms of Use]</font></a> ,<a href="#"> <font color="#C1272D"> [Privacy Policy]</font></a></span>
					</label>
				<input type="hidden" id="inputres" name="hidres" value="<?php echo ($_SESSION['security_question_a'] + $_SESSION['security_question_b']);?>" />
                
				<input type="submit" class="inquiry-button" value="Submit" name="regbtn" id="btn-inquiry-submit" class="inquiry-button"/>
				</td>
				
			</tr>
		</table>
		
			</div>
           <div class="clear"></div>
		</div>
        
	</div>
    
</div>



