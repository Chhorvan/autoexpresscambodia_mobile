<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';
// get form feed.
$fstatus = $_CLASS->getFormStatus();
$fmessage = $_CLASS->getFormMessage();
$fstyle = $_CLASS->getFormStyle();
$group = 'public';
$slug  = 'car';

// load page content.
$pg_details = $_CLASS->getPageHTML();
$countries = $_CLASS->getCountries();
$seller  = $_CLASS->getSeller();

$get_imgname = $_GET['popup'];
?>
<link href="<?php echo BASE_RELATIVE;?>css/vehicle/vehicle_detail.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo BASE_RELATIVE;?>css/common/botsidebar/botsidebar.css" />
<link rel="stylesheet" href="<?php echo BASE_RELATIVE;?>css/pagination.css" />
<link rel="stylesheet" href="<?php echo BASE_RELATIVE;?>css/jquery.fancybox.css" />
<script type="text/javascript" src="<?php echo BASE_RELATIVE;?>js/jquery.fancybox.js"></script>


<div id="carSms" class="clearfix">
    <div id="orderThis">
        <img src="<?php echo BASE_RELATIVE;?>images/vehicle/message.png" />
        <p>Contact Supplier</p>
    </div>
    <div id="sendTo" class="clearfix">
        <p><img src="<?php echo BASE_RELATIVE;?>images/vehicle/blackhouse.png" />To: <?php echo $seller['name']; ?></p>
        
    </div>
    <form method="post" action="">
        <table border="0" cellpadding="0" cellspacing="0" width="315">
            <tr>
                <td class="teaxArea" colspan="2">
                    <textarea class="formTextArea" name="description"></textarea>
                </td>
            </tr>
            <tr>
                <td class="left">Country :</td>
                <td>                    
                    <select class="formSelection" name="countryLoadingSelect" id="countryLoadingSelect">
                        <option value="">--Select--</option>
                        <?php foreach($countries as $country)
                        {
                            echo "<option value=\"{$country['cc']}\">{$country['country_name']}</option>";
                        } ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="left">Port Name :</td>
                <td>                    
                    <select class="formSelection" name="portLoadingSelect" id="portLoadingSelect">
                        <option value=""></option>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="left">Payment Terms :</td>
                <td>                    
                    <input type="checkbox" name="payment_term[0]" value="T/T" class="formCheckBox"/> <span>T/T</span>
                    <input type="checkbox" name="payment_term[1]" value="L/C" class="formCheckBox"/> <span>L/C</span>
                    <input type="checkbox" name="payment_term[2]" value="Other" class="formCheckBox"/> <span>Other</span>
                </td>
            </tr>
            <tr>
                <td class="left">Price Terms : </td>
                <td>
                    
                    <input type="checkbox" name="price_term[0]" value="FOB" class="formCheckBox"/> <span>FOB</span>
                    <input type="checkbox" name="price_term[1]" value="CFR" class="formCheckBox"/> <span>CFR</span>
                    <input type="checkbox" name="price_term[2]" value="CFI" class="formCheckBox"/> <span>CFI</span>
                    <input type="checkbox" name="price_term[3]" value="Other" class="formCheckBox"/> <span>Other</span>
                </td>
            </tr>
            <tr>

                <td colspan="2">
                    <a id="btn-order" href="javascript:this onclick();"><img src="<?php echo BASE_RELATIVE;?>images/vehicle/ordernow.png" id="order" class="linkfade" /></a>
                    <input type="submit" id="submit" name="submit" onchange="readURL(this);" />
                </td>
            </tr>
        </table>
    </form>
</div>
<script>
$('#countryLoadingSelect').on('change', function(){
    var cc = $(this).val();

    $('#portLoadingSelect').empty();
    if(cc === '') return;

    $.ajax({
        url: 'ajax/load_port.php',
        type: 'GET',
        data: 'country=' + cc,
        success: function(data){            
            $('#portLoadingSelect').append(data);
        },
        error: function(status){
            console.log(status);
        }
    });
});

$('#btn-order').on('click', function(){
    $('#submit').click();
});

$(function(){
    var is_submitted = <?php echo isset($_POST['submit']) ? 'true' : 'false'; ?>;

    if (is_submitted) {
        parent.$.fancybox.close();
    }
})();
</script>