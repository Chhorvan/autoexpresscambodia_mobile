<?php
 
?> 

<script type="text/javascript">
		$(document).ready(function() {
			$('.fancybox').fancybox();
						
		});
</script>

 <div class="container">
            <?php include('php/public-member-item-left-sidebar.php');?>
            <div class="content-wrapper">
                 <div class="page-title">
                    
                        <table><tr><td><img class="brick" src="images/common/register_item/blue-brick.png" /></td><td>Membership Guide</td></tr></table>
                     
                 </div>
                 <div class="membership-detail">
                 	<p>If you want to advertise your products on Autowini, please join our business membership. Autowini.com is the ultimate website 
specially designed to promote cars, trucks, buses, heavy machines, motorbikes, aircrafts and ships for world auto traders. Promote your business & product at 
reasonable price!</p>
                 </div>
                 <table class="member-type">
                 	<tr class='odd'>
                    	<td>
                        	<div class="member-type-name">Basic Member</div>
                        	<div class="member-type-price">Free</div></td>
                        <td>
                        	<div class="member-type-name">Power Member</div>
                            <div class="member-type-price"><span style="font-size: 21px;vertical-align: top;">$ </span>1500</div>/ year
                        </td>
                        <td>
                        	<div class="member-type-name">Expert Member</div>
                            <div class="member-type-price"><span style="font-size: 21px;vertical-align: top;">$ </span>3900 </div>/ year
                        </td>
                    </tr><tr class='even'>
                    	<td>One Category (Select from Car, Truck, Bus, Heavy Machine, Parts & Accessories)</td>
                        <td>One Category (Select from Car, Truck, Bus, Heavy Machine, Parts & Accessories)</td>
                        <td>All Categories</td>
                    </tr><tr class='odd'>
                    	<td>Advertise 2 units for free</td>
                        <td>Advertise 30 units (Items are replaceable)</td>
                        <td>Unlimited AD Listings - Listing Priority</td>
                    </tr><tr class='even'>
                    	<td>Upload max 20 photos, 1 video per each item</td>
                        <td>Upload max 20 photos, 1 video per each item</td>
                        <td>Upload max 40 photos, 1 video per each item</td>
                    </tr><tr class='odd'>
                    	<td>Fully detailed Mini Homepage (Profile, Item, Photo, Guest Book)</td>
                        <td>Fully detailed Mini Homepage (Profile, Item, Photo, Guest Book)</td>
                        <td>
                        	Fully customized blog in 7 languages 
(Company Profile, Item, Photo, 
Homepage board, Guest Book, 
Contact us)
						</td>
                    </tr><tr class='even'>
                    	<td style="text-align:center;">-</td>
                        <td>List your company to Autowini sellers Directory</td>
                        <td>Top priority listings in Sellers Directory</td>
                    </tr><tr class='odd'>
                    	<td style="text-align:center;">-</td>
                        <td>Power Member Badge
                        </td><td>Expert Company Badge</td>
                    </tr><tr class='even'>
                    	<td><a href="<?php echo BASE_RELATIVE;?>upgrade-member?popup&member_type=basic_member" class="btn-register <?php if( $_SESSION['log_group']!= 'public'){echo "fancybox fancybox.iframe";}?>">Register</a></td>
                        <td><a href="<?php echo BASE_RELATIVE;?>upgrade-member?popup&member_type=power_member" class="btn-register <?php if( $_SESSION['log_group']!= 'public'){echo "fancybox fancybox.iframe";}?>">Register</a></td>
                        <td><a href="<?php echo BASE_RELATIVE;?>upgrade-member?popup&member_type=expert_member" class="btn-register <?php if( $_SESSION['log_group']!= 'public'){echo "fancybox fancybox.iframe";}?>">Register</a></td>
                    </tr>
                 </table>
                 <div class="upgrade-note">
                 	The membership upgrade from Power Member to Expert company is only available within 3 months from the date of registration.
                 </div>
                 <div class="add-service-title">Additional Services</div>
                 <table class="add-service">
                 	<tr>
                    	<td>Special Offer</td>
                        <td>Premium List</td>
                    </tr>
                    <tr>
                    	<td>
                        	. Top priority listing on search results (Random Exposure)<br/>
							. Recommend sellers for short sale with more 
buyers' inquiries.
							
						</td>
                        <td>
                        	. Top priority listing on search results<br/>
							. Get more exposure for your item photos(5 images)
                           
                        </td>
                    </tr>
                    <tr>
                    	<td>
                            <div class="btn-usd-month"><a href="#">USD 55 / month</a></div>
                        </td>
                        <td>
                        	<div class="btn-usd-month"><a href="#">USD 55 / month</a></div>
                        </td>
                    </tr>
                 </table>
            </div>
           
        </div>