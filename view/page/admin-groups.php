<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';
/*
 * VALIDATE ADMINISTRATOR GROUP ------------------------------------------------
 */
if( $_SESSION['log_group'] == 'admin' )
{
    // get form feed.
    $fstatus = $_CLASS->getFormStatus();
    $fmessage = $_CLASS->getFormMessage();
    $fstyle = $_CLASS->getFormStyle();
    $ftype  = $_CLASS->getFormType();
    
?>
        <div id="content-wrapper">
            <div id="vertical-menu">
                <?php $_MENU->loadMenu($_GLANG); ?>
            </div>
            <div id="inner-content">
                <!-- START CONTENT -->
                <h1><?php echo $_LOCAL['GROUPS_NEW_GROUP_LABEL'];?></h1>
                <p><?php echo $_LOCAL['GROUPS_IMPORTANT_LABEL'];?></p>
                <?php
                    if( $fstatus && $ftype != 'manager'){
                ?>
                <div class="alert <?php echo $fstyle;?>">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <?php echo $fmessage;?>
                </div>
                <?php
                    }
                ?>
                <form method="post">
                    <div class="input-prepend input-append" style="margin-left:1px;">
                        <span class="add-on" style="width:150px;"><?php echo $_LOCAL['GROUPS_GROUP_NAME_LABEL'];?></span>
                        <input type="text" name="groupInput" style="width:430px;" placeholder="<?php echo $_LOCAL['GROUPS_GROUP_NAME_PLACEHOLDER'];?>" />
                        <input type="submit" name="saveBtn" class="btn" value="<?php echo $_LOCAL['GROUPS_SAVE_LABEL'];?>" />
                    </div>
                </form>
                
                <h1>Group manager</h1>
                <?php
                    if( $fstatus && $ftype == 'manager' ){
                ?>
                <div class="alert <?php echo $fstyle;?>">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <?php echo $fmessage;?>
                </div>
                <?php
                    }
                    
                    // display created groups.
                    $group_list = $_CLASS->getGroupList();
                    
                    if( count($group_list) < 1 )
                    {
                ?>
                <div class="text-info"><?php echo $_LOCAL['GROUPS_NOGROUP_LABEL'];?></div>
                <?php
                    }
                    else
                    {
                        for( $i=0; $i < count($group_list); $i++ )
                        {
                ?>
                <form method="post">
                    <div class="input-prepend input-append" style="margin-left:1px;">
                        <span class="add-on" style="width:150px;"><?php echo $_LOCAL['GROUPS_GROUP_LABEL'];?></span>
                        <input type="text" disabled="disabled" value="<?php echo $group_list[$i]['group'];?>" style="width:430px;" />
                        <input type="hidden" name="idInput" value="<?php echo $group_list[$i]['id'];?>" />
                        <input type="submit" name="deleteBtn" onclick="return confirm('<?php echo $_LOCAL['GROUPS_CONFIRM_DELETE'];?>');" class="btn btn-danger" value="<?php echo $_LOCAL['GROUPS_DELETE_BUTTON_LABEL'];?>" />
                    </div>
                </form>
                <?php
                        }
                    }
                ?>
                
                <!-- END CONTENT -->
            </div>
            <div class="clearfix"></div>
        </div>
<?php
} // end validation.
else
{
?>
        <p class="text-warning"><?php echo $_LOCAL['UNAUTHORIZED_ACCESS_MESSAGE'];?></p>
<?php
}