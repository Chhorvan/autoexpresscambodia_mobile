<?php

/*
 * If you need to use CKEditor in this page, make sure to set the controller Test 3
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '_ebody';

// get form feed.
    $fstatus = $_CLASS->getFormStatus();
    $fmessage = $_CLASS->getFormMessage();
    $fstyle = $_CLASS->getFormStyle();
/*
 * Set security question variables ---------------------------------------------
 */
$_SESSION['security_question_a'] = mt_rand('1','9');
$_SESSION['security_question_b'] = mt_rand('1','9');
$listinquiry=$_CLASS->listinquiry();
$listinquiryimg=$_CLASS->listinquiryimg();
$country_list = $_CLASS->getCountryList();
 if(!isset($_GET['id'])) $id='';
	else $id=$_GET['id'];

$imagelist = $_CLASS->getAllImage();

// calculate max. upload filesize.
$max_upload     = (int)(ini_get('upload_max_filesize'));
$max_post       = (int)(ini_get('post_max_size'));
$memory_limit   = (int)(ini_get('memory_limit'));
$upload_mb      = min($max_upload, $max_post, $memory_limit);


?>

<?php
   //load_province
  $get_province=$_CLASS->load_province();
  
?>




<script type="text/javascript">
  $(document).ready(function(){
   $("#country").hide();

});
</script>



<script type="text/javascript">
	function readURL(input)
{
   var filename=$('#file').val();
  if (input.files && input.files[0])
  {
    var reader = new FileReader();

    reader.onload = function (e)
    {
      $('#blah').attr('src', e.target.result).width(160);
      $('#myurl').val(filename);
    };
    reader.readAsDataURL(input.files[0]);
  }
}
</script>
<!--/END OF ORIGINAL CODE-->





<div id="container" >
<div class="page-title">
			<table><tr><td><img class="brick" src="<?php echo BASE_RELATIVE;?>images/common/register_item/blue-brick.png" /></td><td>Edit Profile</td></tr></table>
	</div>
	<div class="content-wrapper">


		<form action="" method="post" id="defaultForm" class="form-horizontal" enctype="multipart/form-data" >

				<?php
                    if( $fstatus ){
                ?>
                <div class="alert <?php echo $fstyle;?>">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <?php echo $fmessage;?>
                </div>
                <?php
                    }
                ?>

		<div id="tab_wrap" class="clearfix" style="width:100%">
            <div class="fillin-title">General Information.</div><div class="fillin-title" id="tap-title">Sign in Information.</div>
		</div>

		<div id="myinfo_wrap">

			<?php
        				foreach ($listinquiry as $lst) {

     		?>

            <div id="sub-content-wrapper" ><!--Start sub-wrapper-->
            <div id="slide-content-wrapper" class="border-content">
            <table class="fillin-table vehicle-info-table" >



				<tr>
					<td>First Name:</td>
					<td style='padding-left:15px;'>
						<div class="form-group">
							<input  class="form-control border" type="text" name="first_name" id="user_id" placeholder="First Name" value="<?php echo $lst['firstname']; ?>" style="width:309px" />
						</div>
					</td>
				</tr>
				<tr>
					<td>Last Name:</td>
					<td style='padding-left:15px;'>
						<div class="form-group">
							<input  class="form-control border" type="text" name="last_name" id="user_id" placeholder="Last Name"  value="<?php echo $lst['lastname']; ?>" style='width:309px;' />
						</div>
					</td>
				</tr>
				<tr>
					<td>Email:</td>
					<td style='padding-left:15px;'>
						<div class="form-group">
                	
                    	<?php
						$lstt="";
						$lstt .=trim($lst['email']);
						?>
						<input style='width:309px;' class="form-control border my-email" type="text" name="email" id="email" value="<?php echo $lstt; ?>" />
                    
                    </div>
					</td>
				</tr>
				<tr>
					<td>Company Name:</td>
					<td style='padding-left:15px;'>
						<div class="form-group">
								<input class="form-control border" type="text" name="company_name" id="com_name" placeholder="Company Name"  value="<?php echo $lst['company_name'];?>" style="width:309px;"/>
						</div>
					</td>
				</tr>
				<tr>
					<td>Phone Number:</td>
					<td style='padding-left:15px;'>
						<div class="form-group">
							<input type="text" name="mobile" id="mobile" class="form-control border"  placeholder="Phone Number" value="<?php echo $lst['mobile'];?>" style="width:309px;"/>
					    </div>
					</td>
				</tr>
				<tr>
					<td>City / Province:</td>
					<td style='padding-left:15px;'>
						<div class="form-group">
							<select id="sex" class="form-control border"  name="province_city" style="width:309px">
							<!--<option selected="selected" value=''>City / Province</option>-->
                            <?php foreach($get_province as $value){?>
							      <?php if($value['province_name']==$lst['province']){?>
										<option selected="selected" value='<?php echo $value['province_name'];?>'><?php echo $value['province_name']; ?></option>
								  <?php }else{?>
										<option  value='<?php echo $value['province_name'];?>'><?php echo $value['province_name']; ?></option>
								  <?php }?>
					      <?php }?> 
                           </select>	
			
						</div>
					</td>
				</tr>
				<tr>
					<td>Address:</td>
					<td style='padding-left:15px;'>
						<div class="form-group">
							<textarea  id="address_seller" name="address" class="form-control border"   style="width:309px;height:150px;" value="<?php echo $lst['address'];?>"/><?php echo $lst['address'];?></textarea>
						</div>
					</td>
				</tr>
				
				<!--<tr class="white-space-row"><td colspan="2"></td></tr>-->

               <!-- <tr>
					<td class="left">User ID<font color="#C1272D">*</font></td>
					<td style='padding-left:15px;'>

                    <div class="form-group" >
                	
						<input style='width:309px;' class="form-control border my-id" type="text" id="user_id" name="id" placeholder="5-25 characters" value="<?php echo ( isset($lst['username']) ? $lst['username'] : '' ); ?>" />
					
                    </div>
					</td>
				</tr>
                <tr>
					<td class="left">Email<font color="#C1272D">*</font></td>
					<td style='padding-left:15px;'>
                    <div class="form-group">
                	
                    	<?php
						$lstt="";
						$lstt .=trim($lst['email']);
						?>
						<input style='width:309px;' class="form-control border my-email" type="text" name="email" id="email" value="<?php echo $lstt; ?>" />
                    
                    </div>
					</td>
				</tr>
				<tr>
					<td class="left">Current Password</td>
					<td>
                    <div class="form-group">
                	<div class="col-lg-5">
						<?php if($_CLASS->message_for_current_password){?>
							<input style="border:1px solid red;width:309px;margin-left:-5px;" class="form-control border current-password" type="password" name="cpassword" id="c-password"  />
						<?php }else{?>
							<input style='width:309px;margin-left:-5px;' class="form-control border current-password" type="password" name="cpassword" id="c-password"  />
						<?php }?>
					</div>
                    </div>

					</td>
				</tr>
				<tr>
					<td class="left">New Password</td>
					<td>
                     <div class="form-group">
                	<div class="col-lg-5">
					   <?php if($_CLASS->message_for_confirm_password){?>
							<input style="border:1px solid red;width:309px;margin-left:-5px;" class="form-control border new-password" type="password" name="password" id="password" />
					   <?php }else{?>
							<input style='width:309px;margin-left:-5px;'class="form-control border new-password" type="password" name="password" id="password" />
					   <?php }?>
                     </div>
                    </div>
					</td>
				</tr><tr>
					<td class="left">Confirm New Password</td>
					<td>
                     <div class="form-group">
                	<div class="col-lg-5">
						<?php if($_CLASS->message_for_confirm_password){?>
							<input style="border:1px solid red;width:309px;margin-left:-5px" class="form-control border confi-password" type="password" name="confirm_password" id="confirm_password" />
						<?php }else{?>
							<input style='width:309px;margin-left:-5px;' class="form-control border confi-password" type="password" name="confirm_password" id="confirm_password" />
						<?php }?>
                    </div>
                    	</div>
					</td>
				</tr> -->
				
				
				<!--<tr class="radio-row">

					<td class="left">Member Type</td>
					<td>
						<?php
							$st=explode(",", $lst['member_type1']);
							$buyer="";
							$seller="";
							$forwarder="";

		 					foreach($st as $s){
		 						if($s=='Buyer'){
		 							$buyer=$s;
		 						}
		 						elseif($s=='Seller'){
		 							$seller=$s;
		 						}
		 						elseif($s=='Forwarder'){
		 							$forwarder=$s;
		 						}
		 					}

		 				 ?>
						<label><input type="radio" id="member_type_1_1" value="Buyer" name="member_type_1[1]" <?php if($buyer!=''){echo 'checked="checked"';} ?> /><span>Buyer</span></label>
						<label><input type="radio" id="member_type_1_1" value="Seller" name="member_type_1[1]" <?php if($seller!=''){echo 'checked="checked"';} ?>/><span>Seller</span></label>
                        <label><input type="radio" id="member_type_1_1" value="Both" name="member_type_1[1]" <?php if($buyer=='' && $seller==''){echo 'checked="checked"';}?>/><span>Both</span></label>
						<!-- <label><input type="checkbox" id="member_type_1_2" value="Forwarder" name="member_type_1[2]" <?php if($forwarder!=''){echo 'checked="checked"';} ?>/><span>Forwarder</span></label> 


					</td>
				</tr>-->
				
				<tr class="radio-row">
					<!-- <td>Member Type 2<font color="#C1272D">*</font></td>
					<td>
						<?php
							$mt2=explode(",", $lst['member_type2']);
							$pm="";
							$bm="";
							foreach($mt2 as $m2){
								if($m2=='Private Member')
								{
									$pm=$m2;
								}
								elseif($m2=='Business Member')
								{
									$bm=$m2;
								}
							}
						?>
						<label><input type="checkbox" id="member_type_2_0" value="Private Member" name="member_type_2[0]" <?php if($pm!=''){echo  'checked="checked"';} ?> /><span>Private Member</span></label>

						<label><input type="checkbox" id="member_type_2_1" value="Business Member" name="member_type_2[1]" <?php if($bm!=''){echo  'checked="checked"';} ?>/><span>Business Member</span></label>

						<span class="error" id="member_type_2_error"><?php echo $_LOCAL['REGISTER_MEMBER_STATUS'];?></span>
					</td> -->
				</tr><tr class="radio-row">
					<!-- <td>Business Type<font color="#C1272D">*</font></td>
					<td>
						<?php
							$bt=explode(",", $lst['business_type']);
							$tr="";
							$mf="";
							foreach($bt as $b)
							{
								if($b=='Trader')
								{
									$tr=$b;
								}
								elseif($b=='Manufacturer')
								{
									$mf=$b;
								}
							}
						?>
						<label><input type="checkbox" id="business_type_0" value="Trader" name="business_type[0]" <?php echo (  $tr!='' ? 'checked="checked"' : '' ); ?> /><span>Trader</span></label>
						<label><input type="checkbox" id="business_type_1" value="Manufacturer" name="business_type[1]" <?php echo ( $mf!='' ? 'checked="checked"' : '' ); ?> /><span>Manufacturer</span></label>
						<span class="error" id="business_type_error"><?php echo $_LOCAL['REGISTER_BUSINESS_TYPE_STATUS'];?></span>
					</td> -->
				</tr><tr class="grid-3-column-row">
					<td class="left"></td>

					<td>

						<?php
							/* $checked_bf=explode(",", $lst['business_field']);
							$checked=array();
							foreach($checked_bf as $b){
								$checked[$b]=true;
							} */
						?>
                        <div id="chek-slide" >
                                <div class="grid-3-column" id="sub-slide">
                                    <label>
                                        <!--<input type="checkbox" id="business_field_0" name="business_field[0]" value="Car" <?php echo ( isset($checked['Car']) ? 'checked="checked"' : '' ); ?> />
                                        <span>Cars</span>-->
                                    </label>
                                </div>
                                <div id="sub-slide" class="sub-slide-right">
                                    <label>
                                        <!--<input type="checkbox" id="business_field_1" name="business_field[1]" value="Bus" <?php echo (  isset($checked['Bus']) ? 'checked="checked"' : '' ); ?> />
                                        <span>Buses</span> -->
                                    </label>
                                </div>
                        </div>
                        </td>
                       </tr><tr>
                       	<th></th>
                        <td>
                       	<div id="chek-slide">
                                <div class="grid-3-column" id="sub-slide">
                                    <label>
                                        <!--<input type="checkbox" id="business_field_2" name="business_field[2]" value="Truck" <?php echo (  isset($checked['Truck']) ? 'checked="checked"' : '' ); ?> />
                                        <span>Trucks</span>-->
                                    </label>
                                </div>

                                <div id="sub-slide" class="slide-right" style="width:125px; ">

                                    <label>
                                        <!--<input type="checkbox" id="business_field_3" name="business_field[3]" value="Equipment" <?php echo (  isset($checked['Equipment']) ? 'checked="checked"' : '' ); ?> />
                                        <span>Heavy Machinery</span>-->
                                    </label>
                                </div>
                        </div>
                       	 </td>
                       </tr><tr>
                       	<th></th>
                        <td>
                        <div id="chek-slide">
                                <div class="grid-3-column" id="sub-slide">
                                    <label>
                                        <!--<input type="checkbox" id="business_field_4" name="business_field[4]" value="Part" <?php echo ( isset($checked['Part']) ? 'checked="checked"' : '' ); ?> />
                                        <span>Auto Parts</span>-->
                                    </label>
                                </div>

                                <div id="sub-slide" class="slide-right" style="width:125px; ">

                                    <label>
                                        <!--<input type="checkbox" id="business_field_5" name="business_field[5]" value="Accessories" <?php echo (  isset($checked['Accessories']) ? 'checked="checked"' : '' ); ?> />
                                        <span>Auto Accessories</span>-->
                                    </label>
                                </div>
                        </div>
                        </td>
                       </tr><tr>
                       	<th></th>
                        <td>
                        <div id="chek-slide">
                                <div class="grid-3-column" id="sub-slide">
                                    <label>
                                        <!--<input type="checkbox" id="business_field_6" name="business_field[6]" value="Motorbike" <?php echo (  isset($checked['Motorbike']) ? 'checked="checked"' : '' ); ?> />
                                        <span>Motorbikes</span>-->
                                    </label>
                                </div>
                                <div id="sub-slide" class="sub-slide-right">
                                    <label>
                                        <!--<input type="checkbox" id="business_field_7" name="business_field[7]" value="Aircraft" <?php echo (  isset($checked['Aircraft']) ? 'checked="checked"' : '' ); ?> />
                                        <span>Aircrafts</span>-->
                                    </label>
                                </div>
                        </div>
                       	</td>
                       </tr><tr>
                       	<th></th>
                        <td>
                        <div id="chek-slide">
                                <div class="grid-3-column" id="sub-slide">
                                    <label>
                                        <!--<input type="checkbox" id="business_field_8" name="business_field[8]" value="Watercraft" <?php echo ( isset($checked['Watercraft']) ? 'checked="checked"' : '' ); ?> />
                                        <span>Watercrafts</span>-->
                                    </label>
                                </div>
                                <div id="sub-slide" class="sub-slide-right">
                                    <label>
                                        <!--<input type="checkbox" id="business_field_9" name="business_field[9]" value="Others" <?php echo ( isset($checked['Others']) ? 'checked="checked"' : '' ); ?> />
                                        <span>Others</span>-->
                                    </label>
                                </div>
                     	</div>

						</td>


				</tr>
			</table>

            </div>


			<!--<div class="fillin-title">Contact Info.</div>-->

            <div id="slide-content-wrapper" class="border-content-right">

			<table class="fillin-table">


				<!-- <tr class="profile-intro">
                   	<th valign="top" style="font-weight:300; padding-top:15px;">Logo</th>
					<td>
                    <?php

					  $url_img="";

					  if(file_exists((BASE_ROOT.'upload/thumb/'.$lst['image']))){

						  $url_img=BASE_RELATIVE.'upload/thumb/'.$lst['image'];
						  $_SESSION['old_image']=$lst['image'];
					  }
					  else{
						   $url_img=BASE_RELATIVE.'images/register/profile-pic.jpg';
					  }
					?>

                    <div id="logo">
                    	<div id="slide-logo">
							<img id="blah" src="<?php //echo  $url_img; ?>" height="100" width="160" />
                        </div>
                        <div class="resize">
                            <div class="profile-button-wrapper">
                                <input type="file" id="file" name="file1" onchange="readURL(this);" style="opacity:0; z-index:99;"/>
                                <input type="button" id="btnProfileUpload" name="btnProfileUpload" value="Upload"  />
                                <input type="hidden" name="myurl" id="myurl"/>
                            </div>
                            <div class="profile-button-wrapper">
                                <input type="button" name="btnDelete" id="btnDelete" value="Delete"/>
                            </div>
                        </div>
                    </div>
					</td>
               </tr> -->
			   
			   <!--<tr>
					<td><div style='margin-top:-14px;'>Current Password</div></td>
					<td>
						<div class="form-group">
						<?php if($_CLASS->message_for_current_password){?>
							<input style="border:1px solid red;width:309px;margin-left:-5px;margin-top:-14px;" class="form-control border current-password" type="password" name="cpassword" id="c-password"  />
						<?php }else{?>
							<input style='width:309px;margin-left:-5px;margin-top:-14px;' class="form-control border current-password" type="password" name="cpassword" id="c-password"  />
						<?php }?>
					
						</div>
					</td>
			   </tr>-->
               <tr>
					<td>User ID:<font color="red">*</font></td>
					<td>
						<div class="form-group" >
                	
							<input style='width:309px;margin-left:-5px;' class="form-control border my-id" type="text" id="user_id" name="id" placeholder="5-25 characters" value="<?php echo ( isset($lst['username']) ? $lst['username'] : '' ); ?>" />
					
						</div>
					</td>
			   </tr>
			   <tr>
					<td>New Password:</td>
					<td>
						<div class="form-group" >
							<input style='width:309px;margin-left:-5px;'class="form-control border new-password" type="password" name="password" id="password"  />
					   </div>
					</td>
			   </tr>
			   <tr>
					<td>Confirm New Password:</td>
					<td>
						<div class="form-group" >
	
							<input style='width:309px;margin-left:-5px;' class="form-control border confi-password" type="password" name="confirm_password" id="confirm_password"  />
						
						</div>
					</td>
			   </tr>
			   
			   
               <!--<tr>
					<td class="left">

					 	Introduction
                    </td>
					
                    <td>
                     <div class="form-group">
							<div >
								<textarea  name="introduction" class="tinymce_myinfo textarea" ><?php echo ( isset($lst['introduction']) ? $lst['introduction'] : '' ); ?></textarea>
							</div>
						</div>
					</td>
				</tr> -->
				<!--<tr>
					<td class="left">Company Name</td>
					<td>
						<input class="form-control border" type="text" name="company_name" value="<?php echo ( isset($lst['company_name']) ? $lst['company_name']:'' );?>" />
					</td>
				</tr>-->
				<!-- <tr>
					<td>Name<font color="#C1272D">*</font></td>
					<td>
						<input type="text"  name="name" id="name" value="<?php echo ( isset($lst['name']) ? $lst['name']:'' );?>" />
						<span class="error" id="name_error"><?php echo $_LOCAL['REGISTER_NAME_STATUS']; ?></span>
						<span class="error" id="invalid_name_error"><?php echo $_LOCAL['REGISTER_INVALID_NAME_STATUS']; ?></span>
						<div class="input-note">Please enter English characters only...</div>
					</td>
				</tr>
				<tr>
					<td>Card ID<font color="#C1272D">*</font></td>
					<td>
						<input type="text" name="card_id" id="card_id" value="<?php echo ( isset($lst['card_id']) ? $lst['card_id']:'' );?>" />
						<span class="error" id="card_id_error"><?php echo $_LOCAL['REGISTER_CARD_ID_STATUS']; ?></span>
					</td>
				</tr> -->
                <!--<tr>
            	<td class="left">Contact Person</td>
                <td>
                	<input class="form-control border" type="text" name="reg_name" value="<?php echo ( isset($lst['name']) ? $lst['name']:'' );?>" />
                 </td>
            	</tr> -->
				<!--<tr>-->
					<!--<td class="left">Country<font color="#C1272D">*</font></td>-->
					<!--<td>-->
                    <div class="form-group">
                	<div>
						<!--<select id="country" class="form-control border" name="country" style="background:url(images/flag/<?php echo $lst['country'].'.png'; ?>) no-repeat 10px center; padding-left:30px; border:1px solid #abadb3; color:#000;">
							<option value="<?php echo ( isset($lst['country']) ? $lst['country']:'' ); ?>" style='background:url(images/flag/kh.png) no-repeat left center; padding-left:20px;'><?php echo ( isset($lst['country']) ? $lst['country']:'-- Select --' ); ?></option> 
                                <?php
                                    if( count($country_list['cc']) > 0 ){
                                        for( $i=0; $i < count($country_list['cc']); $i++ ){
                                            if( !empty($country_list['country_name'][$i])){
                                            	if( $lst['country']==$country_list['cc'][$i]){
                                            		$select="Selected";
													
                                            	}else{
                                            		$select="";
                                            	}

                                          ?>
                                    		<option for="<?php echo $country_list['phonecode'][$i]?>" value="<?php echo $country_list['cc'][$i];?>" style='background:url(images/flag/<?php echo $country_list['cc'][$i];?>.png) no-repeat left center; padding-left:20px;' <?php echo $select;?>> <?php echo $country_list['country_name'][$i];?></option>
                                          <?php
                                            }
                                        }
                                    }
                                    else {
                                ?>
                                    <option value="0"> Undefined Country </option>
                                <?php
                                    }
                                ?>
						</select>-->
						</div>
                        </div>
					<!--</td>-->
				<!--</tr>-->
	            <!--<tr>
					<td class="left">Address</td>
					<td>
						<input class="form-control border" type="text" name="address" id="address" value="<?php echo ( isset($lst['address']) ? $lst['address'] : '' ); ?>" />
	                    <span class="error" id="address_error"><?php echo $_LOCAL['REGISTER_ADDRESS_STATUS']; ?></span>

					</td>
				</tr>
				<tr>
					<td class="left">Post Code</td>
					<td>
						<input class="form-control border" type="text" name="post_code" value="<?php echo ( isset($lst['post_code']) ? $lst['post_code'] : '' ); ?>" />
					</td>
				</tr>
				<tr>
					<td class="left">Tel<p class="phonecode">+855</p></td>
					<td>
						<input class="form-control border" type="text" name="tel" id="tel" value="<?php echo ( isset($lst['tel']) ? $lst['tel'] : '' ); ?>" />

					</td>
				</tr>
				<tr>
					<td class="left">Fax<p class="phonecode-0">+855</p></td>
					<td>
						<input class="form-control border" type="text" name="fax" value="<?php echo ( isset($lst['fax']) ? $lst['fax'] : '' ); ?>" />
					</td>
				</tr>
				<tr>

					<td class="left">Mobile<p class="phonecode-1">+855</p></td>

					<td>
                    <div >
                	<div >
						<input type="text" class="form-control border " name="mobile" id="mobile" value="<?php echo ( isset($lst['mobile']) ? $lst['mobile'] : '' ); ?>" />
                        <input type="hidden" id="phonecode" name="phonecode" value=""/>
						<span class="error" id="mobile_error"><?php echo $_LOCAL['REGISTER_MOBILE_STATUS']; ?></span>
						<span class="error" id="invalid_mobile_error"><?php echo $_LOCAL['REGISTER_INVALID_MOBILE_STATUS']; ?></span>
                        </div>
                     </div>
					</td>
				</tr>
				<tr>
					<td class="left">Website</td>
					<td>
						<input class="form-control border" type="text" name="website" value="<?php echo ( isset($lst['web']) ? $lst['web'] : '' ); ?>"/>
					</td>
				</tr>
				-->

			</table>

            </div>
            <div class="clear"></div>

            </div><!--End sub sub-wrapper-->
            <table style="width:100%;margin-bottom:50px;">
            <tr>
            <h4 style=" margin-bottom: 10px; margin-top: 5px;"><b>Upload Picture</b></h4>
            <td style="width:50%;">
                            <div class="profile-button-wrapper" >
                            <div  style="border:1px solid #ccc; box-sizing:border-box; width:50%">
                            <img src="<?php echo BASE_RELATIVE; ?>images/upload-btn.png" id="upfile" style="cursor:pointer; margin:2px" />
         <input type="file" id="file" multiple name="fileInput[]" accept="image/*" style="display:none;"/>

         </div>
         <p style="color:red;font-size: 12px;margin-top: 10px;"><?php echo $_SESSION['error_extantion']; ?></p>
         <span><img id="uploading" style="float: right;
								    margin-right: 242px; margin-top:-30px"  src="<?php echo BASE_RELATIVE; ?>upload/myinfo/uploading.gif " /></span>
                                <input type="button" id="btnProfileUpload" name="btnProfileUpload" value="Upload" style="display:none;" />
                                <input type="hidden" name="myurl" id="myurl"/>
                                <input type="hidden" name="caridInput" value="<?php echo $id;?>" />
                    			<input type="submit" id="btn_upload_photo" name="savebtn" class="btn btn-success" value="" style="display:none;" />

                            </div>



            </td>
            </tr>




            </table>




			   <?php
          			}

     			?>

		</div>
		<a name="image_upload"></a>
		<!--End My Info -->
		  <?php

        				foreach ($listinquiryimg as $lstimg) {



					  $url_img="";
					
					if (!empty($lstimg['m_image'])){
						
					
					  if(file_exists(BASE_ROOT."upload/profile/".$lstimg['m_image'])){
					  	 $url_img=BASE_RELATIVE."upload/profile/".$lstimg['m_image'];
						  $_SESSION['old_image']=$lstimg['m_image'];
						 ?>


					  	<div id="<?php echo $lstimg['id'] ; ?>" class="clearfix" style="border: 1px solid #ccc; border-radius: 6px; height:auto; margin: 10px;overflow: hidden; width: 220px; float:left;">
							<img  src="<?php echo  $url_img; ?>" height="150" width="208" style="margin:5px;" />
							<p style="margin:0;padding-left:5px;">
							<?php if($lstimg['primary_photo']==0){ ?>
							  <input type="radio"  onclick="checkimg(<?php echo $lstimg['id'] ; ?>,<?php echo $lstimg['owner'] ; ?>);return true;" id="<?php echo $lstimg['owner'] ; ?><?php echo $lstimg['id'] ; ?>" name="primary_photo" /> Set as Primary Photo

							<?php } else { ?>
							  <input type="radio"  onclick="checkimg(<?php echo $lstimg['id'] ; ?>,<?php echo $lstimg['owner'] ; ?>);return true;" id="<?php echo $lstimg['owner'] ; ?><?php echo $lstimg['id'] ; ?>" name="primary_photo" checked="checked"  /> Set as Primary Photo
            				<?php } ?>
					      <a href="#" onclick="removeImageHandler(<?php echo $lstimg['id'] ; ?>,<?php echo $lstimg['id'] ; ?>,<?php echo $lstimg['owner'] ; ?><?php echo $lstimg['id'] ; ?>,'<?php echo $lstimg['m_thumb'] ; ?>','<?php echo $lstimg['m_image'] ; ?>');return false;" class="btn btn-small btn-danger" >×</a>
                        </p>
                        </div>
                        


					<?php  }
					  else{
						   $url_img=BASE_RELATIVE.'images/register/profile-pic.jpg';
					  }
					}

					}
					?>
                 <div style="clear:left;"></div>
				<hr style="border:1px dotted #ccc;" />
			<div id="upload_wrap" >
				<input type="submit" id="btnupdate" name="btnupdate" class="em_upload" value="" >
			</div>

			<!-- Popup form -->
			<div id="popbox" style='display:none;'>
				<!--<input type="submit"  name="btnupdate" class="em_upload" value="" >-->
				<div class="wrapper-popupform">
					<form method="post">
				    <div class="password">
					     <input type="password" name="vpassword" class="txtpassword" placeholder="Verify your password"/>
						 <input type="submit" id="btnupdate" name="btnupdate" class="em_upload linkimg" value="Verify">
					</div>
					</form>
				</div>
			</div>

		</form>
	</div>

<br/>
<br/>
<br/>

</div>



<!-- Script Pop up verify textbox -->
<script type='text/javascript'>

$(function(){
		
		$('#btnupdate').mouseenter(function(){
			document.getElementById("btnupdate").disabled=true;
			$('#popbox').fadeToggle(1000);
		});
			
	});



function myFunction() {
    var person = prompt("Please enter your password");
    
    if (person != null) {
        document.getElementById("demo").innerHTML =
        "Hello " + person + "! How are you today?";
    }
}
</script>


<script type="text/javascript">
	$("#upfile").click(function () {
    $("#file").trigger('click');
	});

</script>

<script type="text/javascript">

jQuery(document).ready(function($) {

    $('#defaultForm').bootstrapValidator({
        //live: 'disabled',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            id:{
                message: 'The id is not valid',
                validators: {
                    notEmpty: {
                        message: 'The ID is required and cannot be empty'
                    },

                    stringLength: {
                        min: 5,
                        max: 25,
                        message: 'The ID must be more than 5 and less than 25 characters long'
                    },
                    regexp: {
                        //regexp: /^[a-zA-Z0-9_\.]+$/,
						regexp:"^[a-zA-Z0-9]+$",
                        message: 'The ID can only Contain letters and number'
                    },
					/*remote: {
                        url: 'ajax/myinfo_chek_available.php',
                        message: 'The ID is not available',
						 data: function(validator) {
                            return {
                                user_id: validator.getFieldElements('id').val()
							};
						}
					}
					*/
				}


            },
            email: {
                validators: {
					 /*notEmpty: {
                        message: 'The email is required and cannot be empty'
                    },*/
                    emailAddress: {
                        message: 'The Input is not a valid email address.'
                    }
					/*remote: {

                        url: 'ajax/myinfo_chek_available.php',
                        message: 'The ID is not available',
						 data: function(validator) {
                            return {
                                email: validator.getFieldElements('email').val()
							};
						}
					}*/
                }
            }
			 
        }
    });

});
</script>




 <script type="text/javascript">

				$(document).ready(function(e) {
                    var select_file=0;
                    $('#uploading').hide();

                   $('form').submit(function(e) {
                   var max_image_upload='<?php echo $_SESSION['user_max_image_upload']; ?>';

                  						// if(parent.check_vehicle_data()==false){

						// 	alert('Please Insert Data in First');
						// 	e.preventDefault();
						//    return false;
						// }
					  // if(select_file==0){


       //                      alert('Please select image to upload!');
       //                      e.preventDefault();
       //                     return false;

       //                }

                    });


                     // Check for file upload limit
					$('#file').change(function(){
						 $('#uploading').show();
						select_file=this.files.length;
				        $('#btn_upload_photo').click();







					});
                        // Prevent submission if limit is exceeded.

                        // End Check for file upload limit


                });
    function checkimg(id,owner){
       // validate.


    if( id == undefined || id == null ){
        console.log('URL is undefined.');
        return false;
    }
    if( owner == undefined || owner == null ){
        console.log('URL is undefined.');
        return false;
    }
     $.post(
        'ajax/setprimaryimagemyinfo.php',
        {

            imgid: id,
            pro:owner

        },
        function(data,status){
         console.log(data);
        }
    );
 }

    function removeImageHandler(id,divid,rd,url,url2){
    // validate.
     if( url == undefined || url == null ){
        console.log('URL is undefined.');
        return false;
    }
    if( url2 == undefined || url2 == null ){
        console.log('URL is undefined.');
        return false;
    }

	if( id == undefined || id == null ){
        console.log('URL is undefined.');
        return false;
    }

    var d = document.getElementById(divid);

    if( d == undefined || d == null ){
        console.log('Image divider is undefined.');
        return false;
    }
    var r = document.getElementById(rd);


    if( r == undefined || r == null ){
        console.log('Image divider is undefined.');
        return false;
    }

    if(r.checked){
        alert('Can not Remove Primary Photo');
    return false;
}


    // send ajax request.
    $.post(
        'ajax/remove_image.php',
        {
            loc: url,
            murl: url2,
			imgid: id

        },
        function(data,status){
		 // console.log(data);
            var result = data.split(',');

            if( result[0] == 'ng' ){
                alert('Unable to remove image due an internal error.');
                console.log(result[0] + ':' + result[1]);
                return false;
            }
            else if( result[0] == 'ok' ){
                // remove image.
                d.remove();

                return false;
            }
            else {
                // unknown error.
                alert('Unknown error: ' + result[0] + ':' + result[1]);
                return;
            }
        }
    );
}



</script>