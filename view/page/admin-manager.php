<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';
/*
 * VALIDATE ADMINISTRATOR GROUP ------------------------------------------------
 */
if( $_SESSION['log_group'] == 'admin' )
{
    // get form feed.
    $fstatus = $_CLASS->getFormStatus();
    $fmessage = $_CLASS->getFormMessage();
    $fstyle = $_CLASS->getFormStyle();
    
    // get news list.
    //$news = $_CLASS->getNewsList();
?>
<link rel="stylesheet" href="<?php echo BASE_RELATIVE;?>css/community/community.css" />
            <div id="sectionContenWrapper">
                <div id="topMenu">
                    <p><a href="#" class="linkfade">Site Manager</a></p>

                </div>
                <?php include("php/sidebar/community.php");?>
            </div>
            <div id="sectionContent">
                
            </div>
            <div class="clearfix"></div>
        </div>
<?php
} // end validation.
else
{
?>
        <p class="text-warning"><?php echo $_LOCAL['UNAUTHORIZED_ACCESS_MESSAGE'];?></p>
<?php
}