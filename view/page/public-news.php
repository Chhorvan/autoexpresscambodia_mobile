<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';

// get form feed.
$fstatus = $_CLASS->getFormStatus();
$fmessage = $_CLASS->getFormMessage();
$fstyle = $_CLASS->getFormStyle();

$news = $_CLASS->getNews();
?>
        <div id="content-wrapper">
            <div id="vertical-menu">
                <?php $_MENU->loadMenu($_GLANG); ?>
            </div>
            <div id="inner-content">
                <!-- START CONTENT -->
                <h1><?php echo $_LANG['NEWS_HEADER'];?></h1>
                <?php
                    if( $fstatus ){
                ?>
                <div class="alert <?php echo $fstyle;?>">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <?php echo $fmessage;?>
                </div>
                <?php
                    }
                    
                    // display news.
                    if( count($news) < 1 ){
                ?>
                <div class="alert alert-info">
                    <?php echo $_LANG['NEWS_NONEWS_LABEL'];?>
                </div>
                <?php
                    }
                    else {
                        for( $i=0; $i < count($news); $i++ ){
                ?>
                <div class="news-item">
                    <h4><?php echo $news[$i]['title'];?>
                        <span class="label label-info" style="float:right;"><?php echo $news[$i]['date'] . ' ' . $news[$i]['time'];?></span>
                    </h4>
                    <p><?php echo $news[$i]['html'];?></p>
                    <p class="author">- by <?php echo $news[$i]['author'];?></p>
                </div>
                <?php
                        }
                    }
                ?>
                <!-- END CONTENT -->
            </div>
            <div class="clearfix"></div>
        </div>
