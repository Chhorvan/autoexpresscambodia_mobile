<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */

if( !isset($_SESSION['log_group']) ){
	header('Location: '.BASE_RELATIVE.'login/');
}else{
	if($_SESSION['log_group'] == 'public'){
		header('Location: '.BASE_RELATIVE.'login/');
	}
}
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';
// get form feed.
$fstatus = $_CLASS->getFormStatus();
$fmessage = $_CLASS->getFormMessage();
$country_list = $_CLASS->getCountryList();

$fstyle = $_CLASS->getFormStyle();
$group = 'public';
$slug  = 'car';
// load page content.
$pg_details = $_CLASS->getPageHTML();
$expert_company = $_CLASS->loadExpertCompany();
$listmyinquiry = $_CLASS->loadinquiry();
      $vt="";
      $cond="";
      $makid="";
      $mak=""; 
      $modelid="";
      $mol="";
      $yf="";
      $yt="";
      $st="";
      $tran="";
      $ft="";
      $ordq="";
      $cc="";
      $con="";
      $po="";
      $pf="";
      $pt="";
      $fob=""; 
      $cfr=""; 
      $cif=""; 
      $other1="";
      $ip=""; 
      $is=""; 
      $ci=""; 
      $sl=""; 
      $se=""; 
      $ins="";
      $t=""; 
      $l=""; 
      $o="";
      $mss="";
      $url="";

if(sizeof($listmyinquiry)!=0){
  foreach($listmyinquiry as $inq)
  {
      $vt=$inq['vehicle_type'];
      $cond=$inq['condition'];
      $makid=$inq['makid'];
      $mak=$inq['make'];
      $modelid=$inq['modelid'];
      $mol=$inq['model'];
      $yf=$inq['year_from'];
      $yt=$inq['year_to'];
      $st=$inq['steering'];
      $tran=$inq['transmission'];
      $ft=$inq['fuel_type'];
      $ordq=$inq['order_quantity'];
      $con=$inq['country_name'];
      $cc=$inq['country'];
      $po=$inq['port'];
      $pf=$inq['price_from'];
      $pt=$inq['price_to'];
      $pterms=explode(",", $inq['price_term']);

      foreach($pterms as $p)
      {
        if($p=="fob")
        {
          $fob=$p;
         
        }
        elseif($p=="cfr")
        {
          $cfr=$p;
          
        }
        elseif($p=="cif")
        {
          $cif=$p;
           
        }
        elseif($p=="other")
        {
          $other1=$p;

        }
      }
      $wtps=explode(",", $inq['want_to_receive']);
      foreach($wtps as $w)
      {
        if($w=="item-photo")
        {
          $ip=$w;
        }
        elseif($w=="item-spec")
        {
          $is=$w;
        }
        elseif($w=="company-info")
        {
          $ci=$w;
        }
        elseif($w=="stock-list")
        { 
          $sl=$w;
        }
        elseif($w=="shipping-estimate")
        {
          $se=$w;
        }
        elseif($w=="inspection")
        {
          $ins=$w;
        }
      }
      $payterms=explode(",", $inq['payment_terms']);
      foreach($payterms as $pa)
      {
        if($pa=="T/T")
        {
          $t=$pa;
        }
        elseif($pa=="L/C")
        {
          $l=$pa;
        }
        elseif($pa=="Other")
        {
          $o=$pa;
        }
      }
      $mss=$inq['message'];
      $url=$inq['fileurl'];
  }
}
?>

 
<link rel="stylesheet" href="<?php echo BASE_RELATIVE;?>css/register_item.css" />
<link rel="stylesheet" href="<?php echo BASE_RELATIVE;?>css/inquiry-left-sidebar.css" />
<link rel="stylesheet" href="<?php echo BASE_RELATIVE;?>css/write_inquiry.css" />
 
<script type="text/javascript">
 
 $(document).ready(function(e) {
		 
		 
		 $('.myerror').hide(); 
		 
	   $('.year option:first-child').attr('hidden','hidden');
     var z ='<?php echo ($makid!=""?$makid:'');?>';
	   if(z!="")
     {
       
        var maker= <?php echo ($makid!=""?$makid:0);?>;  
         var respone_data="";
         $.getJSON('ajax/car_model.php?maker='+ maker,function(json){ 
              $.each(json,function(i,data_json){ 
                  respone_data=respone_data+"<option value='"+data_json.id+"'>"+data_json.carmodel+"</option>";
                  });
         $('#model').html(null);
         $('#model').prepend("<option value='<?php echo($modelid!=""?$modelid:'');?>'><?php echo($mol!=""? $mol :'-- Select --');?></option>")
         $('#model option:first-child').attr('hidden','hidden'); 
         $('#model').append(respone_data);
        });
     }
		$('#make').on('change',function(){
				 var maker=$(this).val();  
				 var respone_data="";
   			 $.getJSON('ajax/car_model.php?maker='+ maker,function(json){ 
      				$.each(json,function(i,data_json){ 
       						respone_data=respone_data+"<option value='"+data_json.id+"'>"+data_json.carmodel+"</option>";
      						});
				 $('#model').html(null);
         $('#model').prepend("<option value='<?php echo($modelid!=""?$modelid:'');?>'><?php echo($mol!=""? $mol :'-- Select --');?></option>")
         $('#model option:first-child').attr('hidden','hidden'); 
				 $('#model').append(respone_data);
				});
			 }); 
			 
				 var respone_data="";
				
				 $.getJSON('ajax/car_make.php?product_type=all',function(json){ 
						$.each(json,function(i,data_json){ 
								respone_data=respone_data+"<option value='"+data_json.id+"'>"+data_json.carmake+"</option>";
						});
				 $('#make').html(null);
         $('#make').prepend("<option value='<?php echo($makid!=""?$makid:'');?>'><?php echo($mak!=""? $mak :'-- Select --');?></option>")
         $('#make option:first-child').attr('hidden','hidden');
				 $('#make').append(respone_data);  
		});
		 
		
			 
      $('.rdo').click(function(e) { 
			 var type=$(this).val(); 
	 		 var respone_data="";
			 
   			 $.getJSON('ajax/car_make.php?product_type='+ type,function(json){ 
      				$.each(json,function(i,data_json){ 
       						respone_data=respone_data+"<option value='"+data_json.id+"'>"+data_json.carmake+"</option>";
      				});
			 $('#make').html(null);
       $('#make').prepend("<option value='<?php echo($makid!=""?$makid:'');?>'><?php echo($mak!=""?$mak :'-- Select --');?></option>")
       $('#make option:first-child').attr('hidden','hidden');
			 $('#make').append(respone_data); 
			 
     		});  
			
        });
		 
		 	$('#make').change(function(e){
				 $('#err-make').hide(); 
			});

      $('#model').change(function(e){
          $('#err-model').hide();
      });

      $('#yearfrom').change(function(e){
        $('#err-my').hide();
      });

      $('#yearto').change(function(e){
        $('#err-my').hide();
      });

      $('#transmission').change(function(e){
        $('#err-tran').hide();
      });

      $('#fueltype').change(function(e){
        $('#err-ft').hide();
      });

      $('#country').change(function(e){
        $('#err-cou').hide();
      });

      $('#orderqty').keypress(function(e){
        $('#err-order').hide();
      });



      $('#port').change(function(e){
        $('#err-port').hide();
      });

      $('#pricefrom').change(function(e){
        $('#err-pric').hide();
      });

      $('#priceto').change(function(e){
        $('#err-pric').hide();
      });

      $('#fob').click(function(e){
        $('#err-pterm').hide();
      }); 

      $('#cfr').click(function(e){
        $('#err-pterm').hide();
      });

      $('#cif').click(function(e){
        $('#err-pterm').hide();
      });

      $('#p-other').click(function(e){
        $('#err-pterm').hide();
      });

      $('#item-photo').click(function(e){
        $('#err-wtc').hide();
      });

      $('#item-spec').click(function(e){
        $('#err-wtc').hide();
      });

      $('#item-coinfo').click(function(e){
        $('#err-wtc').hide();
      });

      $('#stock-list').click(function(e){
        $('#err-wtc').hide();
      });

      $('#shiping-est').click(function(e){
        $('#err-wtc').hide();
      });

      $('#inspection').click(function(e){
        $('#err-wtc').hide();
      });

      $('#tt').click(function(e){
        $('#err-payment').hide();
      });

      $('#lc').click(function(e){
        $('#err-payment').hide();
      });

      $('#pt-other').click(function(e){
        $('#err-payment').hide();
      });

      $('#attach').click(function(e){
        $('#err-uphoto').hide();
        $('#file').click();

      });


		  $('#btnsubmit').click(function(e){

  			  if($('#make option:selected').val()=="")
  			  {  
  			  	  
  				  $('#err-make').show();
           
           
  				  e.preventDefault();
  				 
  			  }
  			  else if($('#model option:selected').val()=='')
  			  {
  				  $('#err-model').show();
  				  e.preventDefault();
  			  }
  			  else if($('#yearfrom').val()=="")
  			  {
  				  $('#err-my').show();
  				  e.preventDefault();
  			  }
  			  else if($('#yearto').val()=="")
  			  {
				  $('#err-my').show();
				  e.preventDefault();
				  }
  			  else if($('#transmission').val()=="")
  			  {
				  $('#err-tran').show();
				  e.preventDefault();
				  }
			   else if($('#fueltype').val()=="")
			   {
				   $('#err-ft').show();
				   e.preventDefault();
				   }
			   else if($('#orderqty').val()=="")
			 {
				$('#err-order').show();
				e.preventDefault();
			 }
			 else if($('#country').val()=="")
			 {
				$('#err-cou').show();
				e.preventDefault();
			 }
			 else if($('#port').val()=="")
			 {
				$('#err-port').show();
				e.preventDefault();
			 }
			 else if($('pricefrom').val()=="")
			 {
				$('#err-pric').show();
				e.preventDefault();
			 }
			 else if($('#priceto').val()=="")
			 {
				$('#err-pric').show();
				e.preventDefault();
			 }


			 else if($('#fob').is(':checked')==0 && 
					 $('#cfr').is(':checked')==0 && 
					 $('#cif').is(':checked')==0 && 
					 $('#p-other').is(':checked')==0 )
			 {
				$('#err-pterm').show();
				e.preventDefault();
			 }
			 else if($('#item-photo').is(':checked')==0 &&
					 $('#item-spec').is(':checked')==0 &&
					 $('#item-coinfo').is(':checked')==0 &&
					 $('#stock-list').is(':checked')==0 &&
					 $('#shiping-est').is(':checked')==0 &&
					 $('#inspection').is(':checked')==0)
			 {
				$('#err-wtc').show();
				e.preventDefault();
			 }
			 else if($('#tt').is(':checked')==0 &&
					 $('#lc').is(':checked')==0 &&
					 $('#pt-other').is(':checked')==0)
			 {
				$('#err-payment').show();
				e.preventDefault();
			 }
       else if ($('#file').val()=="")
       {
          var id='<?php echo (isset($_GET['in_id'])?$_GET['in_id']:'');?>';
          if (id==""){
          $('#err-uphoto').show();
          e.preventDefault();
        }
       }
			 else
			 {
				$('.myerror').hide(); 
			 }


		});
		

    });
 
function readURL(input) {
  var filename=$('#file').val();
        
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
      $('#blah').attr('src', e.target.result).width(160);
      $('#myupload').html(filename);
    };
    reader.readAsDataURL(input.files[0]);
  }
}

</script>

<?php
  // if form is submit, get registration status to display feed
  // back message.
  $status=$_CLASS->getstatus();
  
  if( !empty($status) )
  {
    if( $status == 'complete' )
    {
      if(isset($_GET['in_id']))
        {FeedbackMessage::displayOnPage('Update Sucessfully', 'success');}
      else{FeedbackMessage::displayOnPage('Submit Sucessfully', 'success');}

        
?>
<p class="alert alert-success" style="text-align:center;"><strong><?php echo (isset($_GET['in_id'])?"UPDATE COMPLETE":"SUBMIT COMPLETE");?></strong></p>
<?php
    }
    else if($status == 'query' )
    {
      FeedbackMessage::displayOnPage( 'Unable to Submit due an internal error. Please notify the system administrator.','Error');
?>
<p class="alert alert-error" style="text-align:center;"><strong><?php echo  'Unable to Submit due an internal error. Please notify the system administrator.' ;?></strong></p>
<?php           
    }
    else if($status == 'upload' )
    {
      FeedbackMessage::displayOnPage( 'Cannot upload the picture. Make sure the extension is JPEG or PNG and its size is less than 20MB','Upload Error');
?>
<p class="alert alert-error" style="text-align:center;"><strong><?php echo 'Cannot upload the picture. Make sure the extension is JPEG or PNG and its size is less than 20MB';?></strong></p>
<?php           
    }
    else
    {
      FeedbackMessage::displayOnPage( 'Please fill all fields before submitting the form.');
?>
<p class="alert alert-error" style="text-align:center;"><strong><?php echo 'Please fill all fields before submitting the form.';?></strong></p>
<?php
    }
  }
?>
 
 
<div class="container">
            <?php include('php/public-write-inquiry-left-sidebar.php');?>
            <div class="content-wrapper">
                 <div class="page-title">
                    
                  <table><tr><td><img class="brick" src="images/common/register_item/blue-brick.png" /></td><td>Write an Inquiry</td></tr></table>
                     
                 </div>
<form method="post" enctype="multipart/form-data">
                 <div class="vehicle-info-title">Vehicle Information</div>
                 <table class="vehicle-info">
                 	
                    <tr class="vehicle-type">
                    	<td>Vehicle Type</td>
                        <td>
                        	<label><input class="rdo" <?php if($vt=="All" and isset($_GET['in_id'])){echo 'checked="checked"';}
                                                          else{echo 'checked="checked"';} ?> type="radio" value="All" name="rdo-car-type" /><span>All</span></label>
                            <label><input class="rdo" <?php if($vt=="Car") echo 'checked="checked"';?> type="radio" value="Car" name="rdo-car-type" /><span>Car</span></label>
                            <label><input class="rdo" <?php if($vt=="Bus") echo 'checked="checked"';?> type="radio" value="Bus" name="rdo-car-type" /><span>Bus</span></label>
                            <label><input class="rdo" <?php if($vt=="Truck") echo 'checked="checked"';?> type="radio" value="Truck" name="rdo-car-type" /><span>Truck</span></label>
                            <label><input class="rdo" <?php if($vt=="Equipment") echo 'checked="checked"';?> type="radio" value="Equipment" name="rdo-car-type" /><span>Heavy Machine</span></label>
                            <label><input class="rdo" <?php if($vt=="Part") echo 'checked="checked"';?> type="radio" value="Part" name="rdo-car-type" /><span>Parts/Accessories</span></label>
                        </td>

                   	</tr><tr class="radio-row">
                    	<td>Condition</td>
                        <td>
                          
                        	<label><input <?php if($cond=="All" and isset($_GET['in_id'])){echo 'checked="checked"';}
                                                          else{echo 'checked="checked"';} ?>  type="radio" value="All" name="rdo-car-condition" /><span>All</span></label>
                            <label><input <?php if($cond=="New") echo 'checked="checked"';?> type="radio" value="New" name="rdo-car-condition" /><span>New</span></label>
                            <label><input <?php if($cond=="Use") echo 'checked="checked"';?> type="radio" value="Use" name="rdo-car-condition" /><span>Used</span></label>
                            
                            
                        </td>
                   	</tr><tr>
                    	<td>Make<font color="#C1272D">*</font></td>
                        <td>
                           
                        	<select name="make" id="make" >
                             <option value="all">-- Select --</option> 
                            </select>
                            <br/>
                            <span class="myerror" id="err-make"><?php echo '*Please Select Make';?></span>
                        </td>
                   	</tr><tr>
                    	<td>Model<font color="#C1272D">*</font></td>
                        <td>
                        	<select name="model" id="model" >
                            	<option value='<?php echo($modelid!=""?$modelid:'');?>'><?php echo($mol!=""? $mol :'-- Select --');?></option>
                            </select>
                          <br/>
                          <span class="myerror" id="err-model"><?php echo '*Please Select Model';?></span>
                        </td>
                   	</tr><tr>
                    	<td>Model Year<font color="#C1272D">*</font></td>
                        <td>
                        	<div class="inquiry-input-wrapper">
                            	<div class="inquiry-input">
                                    <select name="model-year-from" class="year" id="yearfrom">
                                        <option value="<?php echo($yf!=''?$yf:'');?>"><?php echo($yf!=''?$yf:'From ...');?></option>
                                        <?php 
                                            for ($x=1990; $x<=2014; $x++)
                                              {
                                              echo "<option value=".$x.">$x</option>";
                                              } 
                                        ?>
                                        
                                    </select>
                                </div>
                                <div class="inquiry-input">
                                    <select name="model-year-to" class="year" id="yearto">
                                        <option value="<?php echo($yt!=''?$yt:'');?>"><?php echo($yf!=''?$yt:'To ...');?></option>
                                        <?php 
                                            for ($x=1990; $x<=2014; $x++)
                                              {
                                              echo "<option value=".$x.">$x</option>";
                                              } 
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <br/>
                            <br/>
                            <br/>
                            <span class="myerror" id="err-my"><?php echo '*Please Select Model year from and Model year to.';?></span>
                        </td>
                   	</tr><tr class="radio-row">
                    	<td>Steering</td>
                        <td>
                        	<label><input <?php if($st=="Any" and isset($_GET['in_id'])){echo 'checked="checked"';}
                                                          else{echo 'checked="checked"';} ?> type="radio" value="Any" name="rdo-steering" /><span>Any</span></label>
                            <label><input <?php if($st=="LHD") echo 'checked="checked"';?> type="radio" value="LHD" name="rdo-steering" /><span>LHD</span></label>
                            <label><input <?php if($st=="RHD") echo 'checked="checked"';?> type="radio" value="RHD" name="rdo-steering" /><span>RHD</span></label>
                        </td>
                   	</tr><tr>
                    	<td>Transmission<font color="#C1272D">*</font></td>
                        <td>
                        	<select name="transmission" class="year" id="transmission">
                            	<option value="<?php echo($tran!=''?$tran:'');?>"><?php echo($tran!=''?$tran:'-- Select --');?></option>
                                <option value="AT">AT</option>
                                <option value="MT">MT</option>
                            </select>
                            <br/>
                            <span class="myerror" id="err-tran"><?php echo '*Please Select transmission';?></span>
                        </td>
                   	</tr><tr>
                    	<td>Fuel Type<font color="#C1272D">*</font></td>
                        <td>
                        	<select name="fuel-type" class="year" id="fueltype">
                            	<option value="<?php echo($ft!=''?$ft:'');?>"><?php echo($ft!=''?$ft:'-- Select --');?></option>
                              <option value="Gasoline">Gasoline</option>
                              <option value="Diesel">Diesel</option>
                              <option value="CNG">CNG</option>
                              <option value="Electric">Electric</option>
                              <option value="Hybrid Electric">Hybrid Electric</option>
                              <option value="Biodiesel">Biodiesel</option>
                              <option value="LPG">LPG</option>
                              <option value="Gasoline LPG">Gasoline LPG</option>
                              <option value="Others">Others</option>

                            </select>
                            <br/>
                            <span class="myerror" id="err-ft"><?php echo '*Please Select Fuel Type.';?></span>
                        </td>
                   	</tr><tr class="high-row">
                    	<td>Order Quantity<font color="#C1272D">*</font></td>
                        <td>
                        	<input type="text" name="order-qty" id="orderqty" value="<?php echo($ordq!=''?$ordq:'');?>"/>
                          <br/>
                          <span class="myerror" id="err-order"><?php echo '*Please Fill in the Ordery Quantity';?></span>
                            <div class="ex-order-qty"><font color="#C1272D">Ex : Container by 20 FT, 1 Unit</font></div>

                        </td>
                   	</tr>
                 </table>
                 <div class="buyer-message-title">Buyer's Message</div>
                 <table class="buyer-message">
                 	
                    <tr class="country">
                    	<td>Country<font color="#C1272D">*</font></td>
                        <td>
                           
                        	<select name="country" class="year" id="country">
                            	<option value="<?php echo($cc!=''?$cc:'');?>"><?php echo($con!=''?$con:'-- Select --');?></option>
                                <?php
                                    if( count($country_list['cc']) > 0 ){
                                    
                                        for( $i=0; $i < count($country_list['cc']); $i++ ){
                                            if( !empty($country_list['country_name'][$i]) ){
                                          ?>
                                    <option value="<?php echo $country_list['cc'][$i];?>"><?php echo $country_list['country_name'][$i];?></option>
                                          <?php
                                            }
                                        }
                                    }
                                    else {
                                ?>
                                    <option value="0"> Undefined Country </option>
                                <?php
                                    }
                                ?>
                            </select>
                            <br/>
                            <span class="myerror" id="err-cou"><?php echo '*Please Select Country';?></span>
                        </td>
                   	</tr><tr>
                    	<td>Port<font color="#C1272D">*</font></td>
                        <td>
                        	<select name="port" class="year" id="port">
                            	<option value="<?php echo($po!=''?$po:'');?>"><?php echo($po!=''?$po:'-- Select --');?></option>
                                <option value="Sihanouk">Sihanouk Port</option>
                            </select>
                            <br/>
                            <span class="myerror" id="err-port"><?php echo '*Please Select Port';?></span>
                        </td>
                   	</tr>
                    <tr>
                    	<td>Price($)<font color="#C1272D">*</font></td>
                        <td>
                        	<div class="inquiry-input-wrapper">
                            	<div class="inquiry-input">
                                    <select name="price-from" class="year" id="pricefrom">
                                        <option value="<?php echo($pf!=''?$pf:'');?>"><?php echo($pf!=''?$pf:'From ...');?></option>
                                        <?php 
                                            for ($x=0; $x<=100; $x+=5 )
                                              {
                                                $va=$x*1000;
                                                echo "<option value=".$va.">".$va."</option>";
                                              } 
                                        ?>
                                    </select>
                                </div>
                                <div class="inquiry-input">
                                    <select name="price-to" class="year" id="priceto">
                                        <option value="<?php echo($pt!=''?$pt:'');?>"><?php echo($pt!=''?$pt:'To ...');?></option>
                                         <?php 
                                            for ($x=0; $x<=100; $x+=5 )
                                              {
                                                $va=$x*1000;
                                              echo "<option value=".$va.">".$va."</option>";
                                              } 
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <br/>
                            <br/>
                            <br/>
                            <span class="myerror" id="err-pric"><?php echo '*Please Select Price from and Price To';?></span>
                        </td>
                   	</tr><tr>
                    	<td>Price Terms<font color="#C1272D">*</font></td>
                      <!--dd-->
                        <td> 
                        	<label><input  <?php if($fob!="")echo "checked=checked";?> type="checkbox" name="priceterm[]" value="fob" id="fob" /> <span>FOB</span></label>
                            <label><input <?php if($cfr!="")echo "checked=checked";?> type="checkbox" name="priceterm[]" value="cfr" id="cfr"/> <span>CFR</span></label>
                            <label><input <?php if($cif!="")echo "checked=checked";?> type="checkbox" name="priceterm[]" value="cif" id="cif"/> <span>CIF</span></label>
                            <label><input <?php if($other1!="")echo "checked=checked";?> type="checkbox" name="priceterm[]" value="other" id="p-other"/> <span>Others</span></label>
                            <br/>
                            <span class="myerror" id="err-pterm"><?php echo '*Please Check priceterms.';?></span>
                        </td>
                   	</tr><tr class="receive-wanted-row">
                    	<td>I want to receive<font color="#C1272D">*</font></td>
                        <td>
                        	<div class="receive-wanted-box"><label><input <?php if($ip!="")echo "checked=checked";?> id="item-photo" type="checkbox" name="receive-wanted[]" value="item-photo" /><span>Item Photos</span></label></div>
                            <div class="receive-wanted-box"><label><input <?php if($is!="")echo "checked=checked";?> id="item-spec" type="checkbox" name="receive-wanted[]" value="item-spec" /><span>Item Spec</span></label></div>
                            <div class="receive-wanted-box"><label><input <?php if($ci!="")echo "checked=checked";?> id="item-coinfo" type="checkbox" name="receive-wanted[]" value="company-info" /><span>Company Information</span></label></div>
                            <div class="receive-wanted-box"><label><input <?php if($sl!="")echo "checked=checked";?> id="stock-list" type="checkbox" name="receive-wanted[]" value="stock-list" /><span>Stock List</span></label></div>
                            <div class="receive-wanted-box"><label><input <?php if($se!="")echo "checked=checked";?> id="shiping-est" type="checkbox" name="receive-wanted[]" value="shipping-estimate" /><span>Shipping Estimates</span></label></div>
                            <div class="receive-wanted-box"><label><input <?php if($ins!="")echo "checked=checked";?> id="inspection" type="checkbox" name="receive-wanted[]" value="inspection" /><span>Inspection</span></label></div>
                            <br/>
                            <br/>
                            <br/>
                            <br/>
                            <span class="myerror" id="err-wtc"><?php echo '*Please Check what you want to Receive.';?></span>
                        </td>
                   	</tr><tr>
                    	<td>Payment Terms<font color="#C1272D">*</font></td>
                        <td>
                        	
                            <label><input <?php if($t!="")echo "checked=checked";?> id="tt" type="checkbox" name="payment-term[]" value="T/T" /><span>T/T</span></label>
                            <label><input <?php if($l!="")echo "checked=checked";?> id="lc" type="checkbox" name="payment-term[]" value="L/C" /><span>L/C</span></label>
                            <label><input <?php if($o!="")echo "checked=checked";?> id="pt-other" type="checkbox" name="payment-term[]" value="Other" /><span>Others</span></label>
                             <br/> 
                             <span class="myerror" id="err-payment"><?php echo '*Please Check Payment Term.';?></span> 
                        </td>
                   	</tr><tr class="high-row">
                    	<td>Message</td>
                        <td> 
                        	<textarea name="message"><?php if($mss!="")echo $mss;?></textarea> 
                            <div class="attach-wrapper">
                              <img id="blah" src="<?php echo ($url!=""?BASE_RELATIVE.$url:BASE_RELATIVE.'images/register/profile-pic.jpg')?>" />
                              <a href="javascript:void(0)" id="attach">Attachment</a> 
                              <span>: aaa.jpg, bbb.jpg</span>
                              <br/>
                              <span id="myupload" style="color:#C1272D;"></span>
                              <span class="myerror" id="err-uphoto"> *Please, Upload you photo.</span>
                            </div>  
                            <ul class="attach-detail list-style-x">
                            	<li>Available Formats:JPG, GIF, BMP, PNG</li>
                                <li>Max file upload size : 20MB</li>
                                
                            </ul>
                          <br/>
                          <div id="con-upload"> 

                            <input type="file" name="myfile" id="file" onchange="readURL(this);"
                            style="display:none;border:1px solid #ccc;width:400px;height:30px;"/>

                             
                          </div>
                        </td>
                   	</tr>
                    <tr class="button-row">
                    	<td></td>
                        <td>
                        	<input type="submit" value="Submit" name="submit" id="btnsubmit" class="inquiry-button"/>
                          <input type="submit" value="Cancel" name="cancel" id="btn-inquiry-cancel" class="inquiry-button"/>
                        </td>
                   	</tr>
                 </table>
                 
            </div>
        </form>
           
        </div>