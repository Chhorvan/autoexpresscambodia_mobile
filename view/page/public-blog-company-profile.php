
<link href="<?php echo BASE_RELATIVE;?>css/blog/blog-company-profile.css" rel="stylesheet" type="text/css" />
    
<div id="sectionContent" class="clearfix">    	
	<?php include('layout/sidebar.php'); ?>
    <div id="sectionFeature" class="clearfix">
    	<h2>COMPANY PROFILE</h2>
        <div id="listData">
        	<div class="section clearfix">
            	<div class="title"><p>INTRODUCTION</p></div>
                <p>
                	Hello.
Thank you for visiting.
CARNURI is specialized in Korean used & new car business.
Since the 'trust' and 'honesty' are the most important factors to our business, CARNURI will be trying to find a win-win solution for both buyers and our company
for mutual benefits.

Компания CARNURI, ориентируясь в основном на Россию, экспортирует машины во многие страны мира. Главный приоритет нашей компании – Доверие. Мы делаем все возможное для того чтобы максимально удовлетворить требования клиента, и ведем честный, открытый бизнес построенный на доверии - купив однажды у нас машину, клиенты без сомнений приходят к нам снова и снова.
Мы гордимся и продолжаем делать все возможное для того чтобы предоставлять автомобили нашим клиентам по самым честным и выгодным в Корее ценам. 
                </p>
            </div>
            
            <div class="section clearfix">
            	<div class="title"><p>COMPANY HISTORY</p></div>
                <p>
                	Hello.
Thank you for visiting.
CARNURI is specialized in Korean used & new car business.
Since the 'trust' and 'honesty' are the most important factors to our business, CARNURI will be trying to find a win-win solution for both buyers and our company
for mutual benefits.

Компания CARNURI, ориентируясь в основном на Россию, экспортирует машины во многие страны мира. Главный приоритет нашей компании – Доверие. Мы делаем все возможное для того чтобы максимально удовлетворить требования клиента, и ведем честный, открытый бизнес построенный на доверии - купив однажды у нас машину, клиенты без сомнений приходят к нам снова и снова.
Мы гордимся и продолжаем делать все возможное для того чтобы предоставлять автомобили нашим клиентам по самым честным и выгодным в Корее ценам. 
                </p>
            </div>
            
            <div class="section clearfix">
            	<div class="title"><p>Office View</p></div>
                <p>
                	<img src="images/blog/company-profile/office-view.jpg" />
                </p>
            </div>
            <div class="section clearfix">
            	<div class="title"><p>Yard View</p></div>
                <p>
                	<img src="images/blog/company-profile/yard-view.jpg" />
                </p>
            </div>
            <div class="section clearfix">
            	<div class="title"><p>Spare View</p></div>
                <p>
                	<img src="images/blog/company-profile/spare-view.jpg" />
                </p>
            </div>
            
            <div class="section clearfix">
            	<div class="title"><p>Contact Us</p></div>
                <div class="contact clearfix">
                	<p class="contactImg"><img src="images/blog/company-profile/pic.jpg" /></p>
                    <p class="contactInfo">
                    	<span class="name">Kim So Young</span><br /><br />
                        <span>Email :</span><br />
                        <span>Mobile :</span><br />
                        <span>TEL :</span><br />
                        <span>FAX :</span><br />
                        <span>Address :</span>
                    </p>
                    <p class="logo"><img src="images/blog/company-profile/logo.jpg" /></p>
                </div>
            </div>
        	
        </div>
    </div>
</div>