<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';
/*
 * VALIDATE ADMINISTRATOR GROUP ------------------------------------------------
 */
if( $_SESSION['log_group'] == 'admin' )
{
    // get form feed.
    $fstatus = $_CLASS->getFormStatus();
    $fmessage = $_CLASS->getFormMessage();
    $fstyle = $_CLASS->getFormStyle();
    
    // load media list.
    $list = $_CLASS->getMediaList();
    $car = $_CLASS->getVehicleMaker();
    $car_id = trim($_GET['cid']);
?>
        <div id="content-wrapper">
            <div id="vertical-menu">
                <?php $_MENU->loadMenu($_GLANG); ?>
            </div>
            <div id="inner-content">
                <!-- START CONTENT -->
                <h1><?php echo $_LANG['MEDIA_MANAGER_HEADER'];?></h1>
                <?php
                    if( $fstatus ){
                ?>
                <div class="alert <?php echo $fstyle;?>">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <?php echo $fmessage;?>
                </div>
                <?php
                    }
                ?>
                <h2 class="h2Line"><?php echo $car['maker'] . ' ' . $car['model'] . ' ' . $car['year'];?></h2>
                <!-- media uploader -->
                <form method="post" class="uploadForm" enctype="multipart/form-data" action="?cid=<?php echo trim($_GET['cid']);?>">
                    <input type="hidden" name="carIdInput" value="<?php echo $car_id;?>" />
                    <h3 style="margin-top:0;"><?php echo $_LANG['MEDIA_MANAGER_UPLOADER_HEADER'];?></h3>
                    <div class="input-prepend">
                        <span class="add-on" style="width:150px;font-weight:bold;"><?php echo $_LANG['MEDIA_MANAGER_UPLOAD_TYPE_LABEL'];?></span>
                        <select id="typeSelector" name="typeInput" onchange="displayTypeTab();" style="width:534px;">
                            <option value="image"><?php echo $_LANG['MEDIA_MANAGER_IMAGE_LABEL'];?></option>
                            <option value="video"><?php echo $_LANG['MEDIA_MANAGER_VIDEO_LABEL'];?></option>
                        </select>
                    </div>
                    
                    <div id="innerSelector" class="input-prepend">
                        <span class="add-on" style="width:150px;font-weight:bold;"><?php echo $_LANG['MEDIA_MANAGER_IMAGE_TYPE_LABEL'];?></span>
                        <select name="typeInput" style="width:534px;">
                            <option value="interior"><?php echo $_LANG['MEDIA_MANAGER_INTERIOR_LABE'];?></option>
                            <option value="exterior"><?php echo $_LANG['MEDIA_MANAGER_EXTERIOR_LABEL'];?></option>
                        </select>
                    </div>
                    
                    <div id="videoUpload" class="input-prepend input-append" style="margin-left:1px;display:none;">
                        <span class="add-on" style="width:150px;font-weight:bold;"><?php echo $_LANG['MEDIA_MANAGER_VIDEO_URL'];?></span>
                        <input type="text" name="videoInput" style="width:431px;" placeholder="<?php echo $_LANG['MEDIA_MANAGER_VIDEO_PLACEHOLDER'];?>" maxlength="250" />
                        <input type="submit" name="vidbtn" class="btn btn-info" value="<?php echo $_LANG['MEDIA_MANAGER_VIDEO_SAVE_BUTTON'];?>" style="width:90px;" />
                    </div>
                    
                    <div id="imageUpload" class="input-prepend input-append" style="margin-left:1px;">
                        <span class="add-on" style="width:150px;font-weight:bold;"><?php echo $_LANG['MEDIA_MANAGER_IMAGE_UPLOAD_LABEL'];?></span>
                        <input type="file" name="fileInput" style="width:443px;border:1px solid #CCC;height:28px;background:#FFF;" />
                        <input type="submit" name="imgbtn" class="btn btn-info" value="<?php echo $_LANG['MEDIA_MANAGER_IMAGE_SAVE_BUTTON'];?>" style="width:90px;" />
                    </div>
                    
                </form>
                <!-- uploaded media list -->
                <?php
                    if( count($list) < 1 ){
                ?>
                <div class="alert alert-info">
                    <?php echo $_LANG['MEDIA_MANAGER_NOMEDIA_LABEL'];?>
                </div>
                <?php
                    }
                    else {
                        for( $i=0; $i < count($list); $i++ ){
                ?>
                <div>
                    <?php
                        // display video
                        if( $list[$i]['type'] == 'video' ){
                    ?>
                    <div id="i<?php echo $i;?>" class="vidWrap">
                        <span class="title"><?php echo $_LANG['MEDIA_MANAGER_VIDEO_LABEL'];?></span>
                        <span>
                            <a href="<?php echo $list[$i]['source'];?>?iframe=true" rel="prettyPhoto"><?php echo $list[$i]['source'];?></a>
                        </span>
                        <a href="#" class="btn btn-small btn-danger remove-btn" onclick="removeMedia('<?php echo $list[$i]['id'];?>','<?php echo addslashes($_LANG['MEDIA_MANAGER_REMOVE_CONFIRM']);?>','i<?php echo $i;?>');return false;"><i class=" icon-remove"></i></a>
                    </div>
                    <?php
                        }
                        else {
                    ?>
                    <div id="i<?php echo $i;?>" class="imgItem">
                        <a href="<?php echo BASE_RELATIVE . $list[$i]['source'];?>" rel="prettyPhoto">
                            <img src="<?php echo BASE_RELATIVE . $list[$i]['source'];?>" width="200" height="150" class="img-polaroid" />
                        </a>
                        <a href="#" class="btn btn-small btn-danger remove-btn" onclick="removeMedia('<?php echo $list[$i]['id'];?>','<?php echo addslashes($_LANG['MEDIA_MANAGER_REMOVE_CONFIRM']);?>','i<?php echo $i;?>');return false;"><i class=" icon-remove"></i></a>
                        <p style="height:23px;line-height:23px;margin:0;margin-top:5px;"><?php echo $list[$i]['mode'];?></p>
                    </div>
                    <?php
                        }
                    ?>
                </div>
                <?php
                        }
                    }
                ?>
                <!-- END CONTENT -->
            </div>
            <div class="clearfix"></div>
        </div>
<?php
} // end validation.
else
{
?>
        <p class="text-warning"><?php echo $_LOCAL['UNAUTHORIZED_ACCESS_MESSAGE'];?></p>
<?php
}