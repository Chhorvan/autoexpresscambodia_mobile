<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';
/*
 * VALIDATE ADMINISTRATOR GROUP ------------------------------------------------
 */
if( $_SESSION['log_group'] == 'admin' )
{
    // get form feed.
    $fstatus = $_CLASS->getFormStatus();
    $fmessage = $_CLASS->getFormMessage();
    $fstyle = $_CLASS->getFormStyle();
    
    // get news list.
    //$registerTypeList = $_CLASS->getRegisterTypeList();
    if(isset($_GET['id'])) $id=htmlspecialchars($_GET['id']); else $id="";
    if(!empty($id)){
        $memberSetting=$_CLASS->getMemberSetting($id);
    }else{
        $memberSetting=array();
    }

?>
<script type="text/javascript">
    function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
      }
</script>
<link rel="stylesheet" href="<?php echo BASE_RELATIVE;?>css/community/community.css" />
            <div id="sectionContenWrapper">
                <div id="topMenu">
                    <p><a href="#" class="linkfade">Site Manager</a></p>

                </div>
                <?php include("php/sidebar/community.php");?>
            </div>
            <div id="sectionContent">
                <?php
                    if( $fstatus ){
                ?>
                <div class="alert <?php echo $fstyle;?>">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <?php echo $fmessage;?>
                </div>
                <?php
                    }
                ?>
                <?php 
                    if(empty($id) || count($memberSetting)>0){
                ?>
                <form method="post"> 
                <table id="member_type_wrapper" class="member-setting-wrapper edit-member-type">
                    <tr>
                        <td>Member Type</td><td><input type="text" value="<?php if(isset($memberSetting[0]['title'])) echo $memberSetting[0]['title'];?>" name="title"/></td><td></td>
                    </tr><tr>
                        <td>Max Product</td><td><input type="text" value="<?php if(isset($memberSetting[0]['max_product'])) echo $memberSetting[0]['max_product'];?>" name="max_product" onkeypress="return isNumberKey(event)"/></td><td>(Number only)</td>
                    </tr><tr>
                        <td>Max Image</td><td><input type="text" value="<?php if(isset($memberSetting[0]['max_image'])) echo $memberSetting[0]['max_image'];?>" name="max_image" onkeypress="return isNumberKey(event)"/></td><td>(Number only)</td>
                    </tr><tr>
                        <td>Note</td><td><textarea name="note"><?php if(isset($memberSetting[0]['note'])) echo $memberSetting[0]['note'];?></textarea></td><td></td>
                    </tr><tr>
                        <td></td><td><input type="submit" id="submit" name="submit" value="Save"/></td><td></td>
                    </tr>
                    
                </table>
                </form>
                <?php 
                    }else{
                        echo "<font color='red'>No register type found!</font>";
                    }
                ?>
            </div>
            <div class="clearfix"></div>
        </div>
<?php
} // end validation.
else
{
?>
        <p class="text-warning"><?php echo $_LOCAL['UNAUTHORIZED_ACCESS_MESSAGE'];?></p>
<?php
}