<?php
    if( !isset($_SESSION['log_group']) ){
        header('Location: '.BASE_RELATIVE.'login/');
    }
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';
// get form feed.
$fstatus = $_CLASS->getFormStatus();
$fmessage = $_CLASS->getFormMessage();
$fstyle = $_CLASS->getFormStyle();
$group = 'public';
$slug  = 'car';
/////test////load page content.
$pg_details = $_CLASS->getPageHTML();
$product_type_list = $_CLASS->getProductType();
$makers = isset($_GET['product_type']) ? $_CLASS->getMaker() : array();
$models = isset($_GET['make']) ? $_CLASS->getModel() : array();
$vehicle = $_CLASS->getVehicleData();
$total_record = $_CLASS->getPagination()->getTotalRecord();
$current_page = $_CLASS->getPagination()->getCurrentPage();
$total_page = $_CLASS->getPagination()->getNumberOfPage();

if(!empty($fstatus) )
{
    if( $fstatus == 'complete' ) {
        FeedbackMessage::displayOnPage('Items have been deleted successfully', 'success');
    }
    else if( $fstatus == 'select' ) {
        FeedbackMessage::displayOnPage("Some items cannot be deleted due to internal error.");
    }
    else {
        FeedbackMessage::displayOnPage('Fail to delete items due to internal error');
    }
}
if(isset($_POST['redirect'])):
?>
window.location.href = <?php echo "'" . $_POST['redirect'] . "'"; ?>;
</script>
<?php
endif
?>



 <div id="sectionContenWrapper">
            <!--sectionSidebar-->
            <div id="topMenu">
                <p><a href="#" class="linkfade">My iBlue</a> > My Inventory</p>
            </div>
            <?php include("php/sidebar/my-wini.php");?>

            </div>
            <div id="sectionContent">
                <div id="payment_list">
                    <p>My Inventory</p>
                </div>
                <div id="checking">
                    <p>
                    Get prompt inquires from the buyers by managing your registered items. If you want to register more items and get <br/>
more quality services, please upgrade your membership.   <span>Go to <a href="#" class="my_inventory linkfade">Membership Guide >></a></span>
                    </p>
                </div>
                <form id="frm-inventory-search" name="frm-inventory-search" method="GET" action="<?php echo BASE_RELATIVE;?>vehicle?page=inventory">
                <div id="inventory_search">
                    <select class="category" name="product_type" id="product_type" >
                        <option value="">--Category--</option>
                        <?php
                            $selected_product_type = '';
                            if(isset($_GET['product_type'])){
                                $selected_product_type = $_GET['product_type'];
                            }
                            foreach ($product_type_list as $product_type) {
                                $product_name = $product_type["product_type"];

                                if($product_name == $selected_product_type)
                                {
                                    echo "<option value=\"{$product_name}\" selected>{$product_name}</option>";
                                }
                                else
                                {
                                    echo "<option value=\"{$product_name}\">{$product_name}</option>";
                                }
                            }
                        ?>
                    </select>
                    <select class="make_model" name="make" id="make">
                        <option value="">--Make--</option>
                        <?php 
                            foreach ($makers as $maker) {
                                if($maker['make'] === $_GET['make'])
                                {
                                    echo "<option value=\"{$maker['make']}\" selected>{$maker['make']}</option>";
                                }
                                else
                                {
                                    echo "<option value=\"{$maker['make']}\">{$maker['make']}</option>";
                                }
                            }
                        ?>
                    </select>
                    <select class="make_model" name="model" id="model">
                        <option value="">-- Model --</option>
                        <?php 
                            foreach ($models as $model) {
                                if($model['model'] === $_GET['model'])
                                {
                                    echo "<option value=\"{$model['model']}\" selected>{$model['model']}</option>";
                                }
                                else
                                {
                                    echo "<option value=\"{$model['model']}\">{$model['model']}</option>";
                                }
                            }
                        ?>
                    </select>
                    <input type="text" name='chassis_no' class="text_search" id="text_chassis_search" placeholder="Chassis No.">
                    <input type="submit" name="btn-search" id="btn-search" class="search_button" value=""/>
                </div>

               <div id="inventory_count" class="clearfix">
                        <p>Total : <span id="total-car"><?php echo $total_record; ?></span>   items     Page   <span id="current_page"><?php echo $current_page; ?></span>/<span id="total-page"><?php echo $total_page; ?></span></p>
                        
                        <select id="sort" name="sort"class="sort" style="width: 100px; height: 25px; margin-top: 5px;">
                            <option value="DESC" <?php echo (@$_GET['sort']=='DESC') ? 'selected' : ''; ?>>New</option>
                            <option value="ASC" <?php echo (@$_GET['sort']=='ASC') ? 'selected' : ''; ?>>Old</option>
                        </select>
                        <span style="width: auto;
                                    height: 19px;
                                    margin-top: 5px;
                                    margin-right: 14px;
                                    border-left: 1px #ccc solid;
                                    padding: 7px 0 0 10px;
                                    float: right;">Sort by</span>
                        <a href="javascript:this onclick" class="delete-selected-items" style="float: right; margin-right: 14px; margin-top: 12px; text-decoration: underline; font-weight: bold; color: blue;">Delete</a>
               </div>
               <div id="inventory_carwrap">
                        <table border="0" cellpadding="0" cellpadding="0">
                            <?php foreach($vehicle as $index => $vehicle_data){?>
                            <tr class="inventory_car_list" data-index="<?php echo $index; ?>">
                                <td class="check_box"><input type="checkbox" class="selected-vehicle-item" data-field="<?php echo $vehicle_data['id']; ?>" /></td>
                                <td class="car_img">
                                    <div style="position:relative; height:100px;">
                                        <?php
                                        $product_type_url="";
                                        if($vehicle_data['product_type']=="Part"){
                                            $product_type_url="edit-part?cid=";
                                        }elseif($vehicle_data['product_type']=="Car"){
                                            $product_type_url="edit-car?cid=";
                                        }elseif($vehicle_data['product_type']=="Bus"){
                                            $product_type_url="edit-bus?cid=";
                                        }elseif($vehicle_data['product_type']=="Truck"){
                                            $product_type_url="edit-truck?cid=";
                                        }elseif($vehicle_data['product_type']=="Equipment"){
                                            $product_type_url="edit-equipment?cid=";
                                        }
                                        ?>
                                    <a href="<?php echo BASE_RELATIVE.$product_type_url.$vehicle_data['id']; ?>">
                                    <?php
                                    if(!file_exists('image/upload/cars/special_offer-'.$vehicle_data['carImg'])){
                                    ?>
                                        <img class="show_car_img" src="<?php echo BASE_RELATIVE;?>images/noimg.jpg" width="180" height="100" style="position:absolute; z-index:1"  />
                                    <?php
                                        }else{
                                    ?>
                                            <img class="show_car_img" src="<?php echo BASE_RELATIVE; ?>image/upload/cars/special_offer-<?php echo $vehicle_data['carImg'];?>" width="180" height="100" style="position:absolute; z-index:1;" />
                                    <?php
                                            }
                                    ?>

                                    <?php if($vehicle_data['arrival'] === 'new'): ?>
                                        <img class="show_new_img" src="<?php echo BASE_RELATIVE;?>images/vehicle/new.png" width="67" height="16" style="position:relative; z-index:2;" />
                                    <?php endif ?>
                                    </a>
                                    </div>
                                </td>
                                <td>
                                    <table>
                                        <tr class="car_detail">
                                            <td>
                                                <span class="title"><?php echo $vehicle_data['model_year']." ".$vehicle_data['make']." ".$vehicle_data['model'];  ?> </span> <img class="status" src="<?php echo BASE_RELATIVE;?>images/common/button/inventory/<?php echo strtolower($vehicle_data['status'] === null ? 'for_sale' : $vehicle_data['status']); ?>.png" /><br/>
                                                <span class="chassis_no"><?php echo $vehicle_data['chassis_no']; ?></span><span class="condition"><?php echo " | " . $vehicle_data['condition']; ?></span><span class="steering"><?php echo " | " . $vehicle_data['steering']; ?></span><span class="fuel_type"><?php echo " | " . $vehicle_data['fuel_type'];  ?></span><br/>
                                                <span class="mileage_transmission"><?php echo $vehicle_data['mileage']." Km | ".$vehicle_data['transmission'];  ?></span><br/>
                                                <span><span class="currency">$</span><span class="price"><?php echo $vehicle_data['price'];  ?></span></span>
                                            </td>
                                            <td>
                                                <span class="car_create_date"><?php echo date("F j, Y g:i a",strtotime($vehicle_data['created_date']));?></span>
                                                <div class="action_button">
                                                    <a href="<?php echo BASE_RELATIVE;?>apply-pre?popup&p_id=<?php echo $vehicle_data['id']?>" class="btn-register fancybox fancybox.iframe apply-pre">
                                                        <img src="<?php echo BASE_RELATIVE;?>images/my-inventory/apply.png" class="linkfade" />
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr colspan="2">
                                            <td colspan="2">
                                                <span>HIT : <span class="hit"><?php echo $vehicle_data['hit_count']; ?> </span>&nbsp &nbsp MESSAGE : <span class="message_status"><?php echo $vehicle_data['unread_message']; ?>/<?php echo $vehicle_data['message']; ?></span></span>
                                                <div class="button_wrap">
                                                    <input type="submit" value="" class="reserved" data-field="<?php echo $vehicle_data['id']; ?>" data-status="<?php echo $vehicle_data['status']; ?>"/>
                                                    <input type="submit" value="" class="sold" data-field="<?php echo $vehicle_data['id']; ?>" data-status="<?php echo $vehicle_data['status']; ?>"/>
                                                    <input type="submit" value=""  class="delete" data-field="<?php echo $vehicle_data['id']; ?>"/>
                                                    <!-- <input id="btndelete" class="delete" type="button" name="delete" value="" onclick="delete_inventory(<?php echo $vehicle_data['id']; ?>)" /> -->

                                                    <div class="edit">
                                                        <?php
                                                        $product_type_url="";
                                                        if($vehicle_data['product_type']=="Part"){
                                                            $product_type_url="edit-part?cid=";
                                                        }elseif($vehicle_data['product_type']=="Car"){
                                                            $product_type_url="edit-car?cid=";
                                                        }elseif($vehicle_data['product_type']=="Bus"){
                                                            $product_type_url="edit-bus?cid=";
                                                        }elseif($vehicle_data['product_type']=="Truck"){
                                                            $product_type_url="edit-truck?cid=";
                                                        }elseif($vehicle_data['product_type']=="Equipment"){
                                                            $product_type_url="edit-equipment?cid=";
                                                        }
                                                        ?>
                                                        <?php if(!empty($product_type_url)){?>
                                                            <a href="<?php echo BASE_RELATIVE.$product_type_url.$vehicle_data['id']; ?>">
                                                        <?php } ?>
                                                        <img src="<?php echo BASE_RELATIVE;?>images/common/button/search.png" /></a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <?php }?>
                        </table>
               </div>
               <div id="pagination">
                    <?php echo $_CLASS->getPagination()->generateLink('pagination'); ?>
               </div>
            </form>
    </div>
    <!-- end div id="sectionContent"-->
</div><!-- end div id="sectionContentWraper" -->
<script id="ajax-category-search">
$(function(){

    $('#product_type').on('change',function(){
        var obj = $('#make');

        obj.empty();
        obj.append('<option value="">--Make--</option>');

        // Set model category to default.
        $('#model').empty();
        $('#model').append('<option value="">- -Model --</option>');

        // Reset Chassis No. text
        $('#text_chassis_search').val('');

        // Return if NULL.
        if($(this).val() === ''){
            return;
        }

        // Load product type.
        $.ajax({
            url: 'ajax/my-inventory/load_car_make.php',
            type: 'GET',
            dataType: 'json',
            data: 'product_type=' + $(this).val(),
            success: function(data){
                for(var i=0; i<data.length; i++){
                    obj.append('<option value="' + data[i].make + '">' + data[i].make + '</option>');            
                }            
            }
        });
    });

    $('#make').on('change',function(){
        var obj = $('#model');

        obj.empty();
        obj.append('<option value="">-- Model --</option>');

        // Return if NULL
        if($(this).val() === ''){
            return;
        }

        // Load car model.
        $.ajax({
            url: 'ajax/my-inventory/load_car_model.php',
            type: 'GET',
            dataType: 'json',
            data: 'make=' + $(this).val(),
            success: function(data){
                for(var i=0; i<data.length; i++){
                    obj.append('<option value="' + data[i].model + '">' + data[i].model + '</option>');
                }
            }
        });
    });

    // Ajax delete selected car.
    $('.delete-selected-items').click(function(){

        // Selected car items.
        var selected_items = $('.selected-vehicle-item'),           
            car_id_collection = [],
            index_collection = []
            index = 0;
        
        for(var i=0; i<selected_items.length; i++){
            if(selected_items[i].checked){
                car_id_collection[index] = $(selected_items[i]).attr('data-field');
                index_collection[index] = parseInt($(selected_items[i]).parents('.inventory_car_list').attr('data-index'));
                index++;
            }
        }

        // Return if no message selected.
        if(car_id_collection.length === 0){
            alert('Messages have not been selected.');
            return;
        }

        // Alert message before deleting car.
        if(!confirm('Are you sure to delete these items?')) return;

        // Delete selected items.
        if(car_id_collection.length > 0) deleteCar(car_id_collection, index_collection);

        // Release items checked
        $(selected_items).prop('checked', false);
    });

    // Ajax delete car.
    $('.delete').click(function(e){
        e.preventDefault();

        // Delete car.
        deleteCar($(this).attr('data-field'), $(this).parents('.inventory_car_list').attr('data-index'));
    });

    deleteCar = function(car_id_collection, indexes){
        var query_product_type = "<?php echo isset($_GET['product_type']) ? $_GET['product_type'] : ''; ?>",
            query_make = "<?php echo isset($_GET['make']) ? $_GET['make'] : ''; ?>",
            query_model = "<?php echo isset($_GET['model']) ? $_GET['model'] : ''; ?>",
            query_chassis_no = "<?php echo isset($_GET['chassis_no']) ? $_GET['chassis_no'] : ''; ?>",
            query_current_page = <?php echo isset($_GET['page']) ? $_GET['page'] : 1; ?>;
            form_data = {};

        form_data['delete'] = 'true';   
        form_data['p_id'] = car_id_collection;     
        form_data['product_type'] = query_product_type;
        form_data['make'] = query_make;
        form_data['model'] = query_model;
        form_data['chassis_no'] = query_chassis_no;
        form_data['current_page'] = query_current_page;
        form_data['index'] = indexes;

        $.ajax({
            type: "POST",
            url: 'ajax/update-my-inventory.php',
            data: form_data,
            success: function(data){
                var current_row_index = parseInt(form_data['index']);

                if(data.status === 1){
                    var inventory_car_list = $('#inventory_carwrap .inventory_car_list'),
                        selected_vehicle_item = inventory_car_list.find('.selected-vehicle-item'),
                        car_img_show = inventory_car_list.find('.car_img .show_car_img'),
                        car_new = inventory_car_list.find('.car_img .show_new_img'),
                        btn_car_img = inventory_car_list.find('.car_img a'),
                        title = inventory_car_list.find('.car_detail .title'),
                        status = inventory_car_list.find('.car_detail .status'),
                        chassis_no = inventory_car_list.find('.car_detail .chassis_no'),
                        condition = inventory_car_list.find('.car_detail .condition'),
                        steering = inventory_car_list.find('.car_detail .steering'),
                        fuel_type = inventory_car_list.find('.car_detail .fuel_type'),
                        mileage_transmission = inventory_car_list.find('.car_detail .mileage_transmission'),
                        price = inventory_car_list.find('.car_detail .price'),
                        car_create_date = inventory_car_list.find('.car_create_date'),
                        apply_pre = inventory_car_list.find('.apply-pre'),
                        hit = inventory_car_list.find('.hit'),
                        message_status = inventory_car_list.find('.message_status'),
                        btn_reserved = inventory_car_list.find('.reserved'),
                        btn_sold = inventory_car_list.find('.sold'),
                        btn_delete = inventory_car_list.find('.delete'),
                        btn_edit = inventory_car_list.find('.edit a');

                for(var i=current_row_index; i<data.pagination.records_per_page; i++){
                      var index = i - current_row_index;

                      // Remove undefined row.
                      if(data.vehicle[index] === undefined){
                        if(inventory_car_list[i] !== undefined){
                          $(inventory_car_list[i]).remove();
                        }

                        break;
                      }
                      /////////////


                      $(selected_vehicle_item[i]).attr('data-field', data.vehicle[index].id);

                      // Show car image.
                      $(car_img_show[i]).on('error', function(){$(this).attr('src', 'images/noimg.jpg');});
                      $(car_img_show[i]).attr('src', 'image/upload/cars/special_offer-' + data.vehicle[index].carImg);


                      // Whether to the car is new.
                      if(data.vehicle[index].arrival == 'new' && car_new[i] === undefined){
                        var car_img_a = inventory_car_list.find('.car_img a');
                        $(car_img_a[i]).append('<img class="show_new_img" src="images/vehicle/new.png" width="67" height="16" style="position:relative; z-index:2;" />');
                      }
                      else if(data.vehicle[index].arrival != 'new' && car_new[i] !== undefined){
                        $(car_new[i]).remove();
                      }

                      $(title[i]).html(data.vehicle[index].model_year + ' ' + data.vehicle[index].make + ' ' + data.vehicle[index].model);
                      $(status[i]).attr('src', 'images/common/button/inventory/for_sale.png');
                      $(chassis_no[i]).html(data.vehicle[index].chassis_no);
                      $(condition[i]).html(' | ' + data.vehicle[index].condition);
                      $(steering[i]).html(' | ' + data.vehicle[index].steering);
                      $(fuel_type[i]).html(' | ' + data.vehicle[index].fuel_type);
                      $(mileage_transmission[i]).html(data.vehicle[index].mileage + " Km | " + data.vehicle[index].transmission);
                      $(price[i]).html(data.vehicle[index].price);
                      $(apply_pre[i]).attr('href', '<?php echo BASE_RELATIVE;?>apply-pre?popup&p_id=' + data.vehicle[index].id);

                      // Format date of created car.
                      var create_date = new Date(data.vehicle[index].created_date),
                          locale = "en-US",
                          options = {year: "numeric", month: "short", day: "numeric", hour: "2-digit", minute: "2-digit"},
                          create_date_format = new Intl.DateTimeFormat(locale, options).format(create_date);

                      $(car_create_date[i]).html(create_date_format);

                      //.......
                      $(hit[i]).html(data.vehicle[index].hit_count);
                      $(message_status[i]).html(data.vehicle[index].unread_message + '/' + data.vehicle[index].message);

                      //.......
                      $(btn_reserved[i]).attr('data-field', data.vehicle[index].id);
                      $(btn_sold[i]).attr('data-field', data.vehicle[index].id);
                      $(btn_delete[i]).attr('data-field', data.vehicle[index].id);

                      var product_type_url = '';
                      if(data.vehicle[index].product_type == "Part"){
                          product_type_url = "edit-part?cid=";
                      }
                      else if(data.vehicle[index].product_type == "Car"){
                          product_type_url = "edit-car?cid=";
                      }
                      else if(data.vehicle[index].product_type == "Bus"){
                          product_type_url="edit-bus?cid=";
                      }
                      else if(data.vehicle[index].product_type == "Truck"){
                          product_type_url = "edit-truck?cid=";
                      }
                      else if(data.vehicle[index].product_type == "Equipment"){
                          product_type_url = "edit-equipment?cid=";
                      }

                      $(btn_edit[i]).attr('href', '<?php echo BASE_RELATIVE; ?>' + product_type_url + data.vehicle[index].id);
                      $(btn_car_img[i]).attr('href', '<?php echo BASE_RELATIVE; ?>' + product_type_url + data.vehicle[index].id);
                    }

                    $('#pagination .pagination').remove();
                    $('#pagination').append(data.pagination.link);

                    $('#inventory_count #total-car').empty();
                    $('#inventory_count #total-car').append(data.pagination.total_car);

                    $('#inventory_count #current_page').empty();
                    $('#inventory_count #current_page').append(data.pagination.current_page);

                    $('#inventory_count #total-page').empty();
                    $('#inventory_count #total-page').append(data.pagination.total_page);
                }
            }
        });
    }

    // Ajax sold car.
    $('.sold').click(function(e){
      e.preventDefault();
        var car_id= $(this).attr('data-field'),
            car_status=$(this).attr('data-status'),
            inventory_car_list = $(this).parents('.inventory_car_list');
        $.ajax({
            type: "POST",
            //url: "<?php echo BASE_RELATIVE.'my-inventory?action=sold'; ?>",
            url: 'ajax/update-my-inventory.php',
            //data: $("#frm-inventory-search").serialize(),
            data: 'sold=true&p_id=' + car_id,
            success: function(data){
             if(data['status'] === 1){
                var img_status = $(inventory_car_list).find('img.status');
                $(img_status).attr('src', 'images/common/button/inventory/sold.png');
              }
            }
        });
    });

    // Ajax reserved car.
    $('.reserved').click(function(e){
      e.preventDefault();
        var car_id= $(this).attr('data-field'),
            car_status=$(this).attr('data-status'),
            inventory_car_list = $(this).parents('.inventory_car_list');
        $.ajax({
            type: "POST",
            url: 'ajax/update-my-inventory.php',
            data: 'reserved=true&p_id=' + car_id,
            success: function(data){
                var img_status = $(inventory_car_list).find('img.status');
                $(img_status).attr('src', 'images/common/button/inventory/reserved.png');
            },
        });
    });
});
</script>
