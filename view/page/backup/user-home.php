<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';
/*
 * VALIDATE ADMINISTRATOR GROUP ------------------------------------------------
 */
if( $_SESSION['log_group'] == 'user' )
{
    // get form feed.
    $fstatus = $_CLASS->getFormStatus();
    $fmessage = $_CLASS->getFormMessage();
    $fstyle = $_CLASS->getFormStyle();
    
    
?>
        <div id="content-wrapper">
            <div id="vertical-menu">
                <?php $_MENU->loadMenu($_GLANG); ?>
            </div>
            <div id="inner-content">
                <!-- START CONTENT -->
                <h1><?php echo $_LANG['ADMIN_HOME_NOTIFICATION_HEADER'];?></h1>
                <?php
                    if( $fstatus ){
                ?>
                <div class="alert <?php echo $fstyle;?>">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <?php echo $fmessage;?>
                </div>
                <?php
                    }
                ?>
                <!-- notification tab -->
                <div id="notification-wrap">
                    <div class="alert alert-info" style="margin:5px;">
                        <span class="label-item"><?php echo $_LANG['ADMIN_HOME_USER_REG_LABEL'];?>:</span>
                        <span><?php echo $_LANG['ADMIN_HOME_NOTICE_LABEL_A'];?> <?php echo $_CLASS->getRegisteredUsers();?> <?php echo $_LANG['ADMIN_HOME_NOTICE_ACTIVE_USER'];?></span>
                        <div class="clearfix"></div>
                    </div>
                    <?php if( $_CLASS->getInbox() > 0 ){ ?>
                    <div class="alert alert-warning" style="margin:5px;">
                        <span class="label-item"><?php echo $_LANG['ADMIN_HOME_INBOX_LABEL'];?>:</span>
                        <span><?php echo $_LANG['ADMIN_HOME_NOTICE_LABEL_A'];?> <?php echo $_CLASS->getInbox();?> <?php echo $_LANG['ADMIN_HOME_INBOX_NOTICE'];?></span>
                        <div class="clearfix"></div>
                    </div>
                    <?php } ?>
                    <?php if( $_CLASS->getFWLog() > 0 ){ ?>
                    <div class="alert alert-warning" style="margin:5px;">
                        <span class="label-item"><?php echo $_LANG['ADMIN_HOME_WEBSITE_LABEL'];?>:</span>
                        <span><?php echo $_LANG['ADMIN_HOME_NOTICE_LABEL_A'];?> <?php echo $_CLASS->getFWLog();?> <?php echo $_LANG['ADMIN_HOME_WEBSITE_NOTICE'];?></span>
                        <div class="clearfix"></div>
                    </div>
                    <?php } ?>
                    <div class="alert alert-info" style="margin:5px;">
                        <span class="label-item"><?php echo $_LANG['ADMIN_HOME_TOTAL_INVENTORY_LABEL'];?>:</span>
                        <span><?php echo $_LANG['ADMIN_HOME_NOTICE_LABEL_A'];?> <?php echo $_CLASS->getTotalVehiclesInInventory();?> <?php echo $_LANG['ADMIN_HOME_TOTAL_IN_INVENTORY'];?></span>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <!-- end notification tab -->
                <!-- page rank -->
                <!--
                <h1><?php echo $_LANG['ADMIN_HOME_PR_CHECKE_HEADER'];?></h1>
                <div class="alert alert-success">
                    <?php $rank = $_CLASS->getPageRank(); echo $_LANG['ADMIN_HOME_PAGERANK_LABEL'] . ' ' . ( empty($rank) ? '<em>' . $_LANG['ADMIN_HOME_PAGE_RANK_ERROR'] . '</em>' : '<b>' . $rank . '</b>.'); ?>
                </div>
                -->
                <!-- end page rank -->
                <!-- VIN checker
                <br/>
                <br/>
                <form method="post" style="border:1px solid #CCC;padding:10px;border-radius:6px;-moz-border-radius:6px;-webkit-border-radius:6px;-ms-border-radius:6px;-o-border-radius:6px;">
                    
                <br/>
                <!-- end register vehicle by VIN -->
                <!-- clear tmp dir -->
                <form method="post" style="border:1px solid #CCC;padding:10px;border-radius:6px;-moz-border-radius:6px;-webkit-border-radius:6px;-ms-border-radius:6px;-o-border-radius:6px;">
                    <h1><?php echo $_LANG['ADMIN_HOME_CLEAR_TMP_HEADER'];?></h1>
                    <p><?php echo $_LANG['ADMIN_HOME_CLEAR_TMP_DESCRIPTION'];?></p>
                    <input type="submit" name="tmpbtn" class="btn btn-warning" value="<?php echo $_LANG['ADMIN_HOME_CLEAR_TMP_BTN'];?>"/>
                    <div class="clear"></div>
                </form>
                <!-- end clear tmp dir -->
                
                <!-- END CONTENT -->
            </div>
            <div class="clearfix"></div>
        </div>
<?php
} // end validation.
else {
?>
        <p class="text-warning"><?php echo $_LOCAL['UNAUTHORIZED_ACCESS_MESSAGE'];?></p>
<?php
}