<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';
// get form feed.
$fstatus = $_CLASS->getFormStatus();
$fmessage = $_CLASS->getFormMessage();
$fstyle = $_CLASS->getFormStyle();
$group = 'public';
$slug  = 'car';
// load page content.
$pg_details = $_CLASS->getPageHTML();

?>




 <div id="sectionContenWrapper">
    <!--sectionSidebar--> 
    <div id="topMenu">
        <p><a href="#" class="linkfade">Help</a> > FAQ</p>
    </div>
    <?php include("php/sidebar/help.php");?>    
    
    </div>           
    <div id="sectionContent"> 
        <div id="help_title">
            <p>FAQ</p>
        </div>
        <div id="context_text1">
        	<p>This page contains frequently asked questions about using GlobaliBlue.com. Please read carefully before using <br/>
Autowini.com for much better understanding of our process.</p>
        </div>
        <p class="faq_title">The Common questions about Global iBlue.com</p>
        <?php for($i=1;$i<=7;$i++){?>
        <div class="question" >
        	<p><a href="#" class="linkfade commonQuest" title="<?php echo $i?>">What is Global iBlue ?</a></p>
        </div>
        <div class="answer anCollape<?php echo $i;?>" title="<?php echo $i?>">
        	<p>
            	GlobaliBlue.com is a specially designed web site for world auto traders who want to promote their products and companies through on-line. Our mission 
at GlobaliBlue.com is to link all buyers and sellers throughout the world providing truly convenient and reliable on-line trading system. <br/><br/>
For more details please go to About GlobliBlue.com
            </p>
        </div>
        <?php }?>
        
        <p class="faq_title">Buyer's Guide</p>
        <?php for($i=1;$i<=5;$i++){?>
        <div class="question" >
        	<p><a href="#" class="linkfade buyerQuest" title="<?php echo $i?>">What is Global iBlue ?</a></p>
        </div>
        <div class="answer buyerQuest<?php echo $i;?>" title="<?php echo $i?>">
        	<p>
            	GlobaliBlue.com is a specially designed web site for world auto traders who want to promote their products and companies through on-line. Our mission 
at GlobaliBlue.com is to link all buyers and sellers throughout the world providing truly convenient and reliable on-line trading system. <br/><br/>
For more details please go to About GlobaliBlue.com
            </p>
        </div>
        <?php }?>
        
        <p class="faq_title">Seller's Guide</p>
        <?php for($i=1;$i<=5;$i++){?>
        <div class="question" >
        	<p><a href="#" class="linkfade sellerQuest" title="<?php echo $i?>">What is Global iBlue ?</a></p>
        </div>
        <div class="answer sellerQuest<?php echo $i;?>" title="<?php echo $i?>">
        	<p>
            	GlobaliBlue.com is a specially designed web site for world auto traders who want to promote their products and companies through on-line. Our mission 
at GlobaliBlue.com is to link all buyers and sellers throughout the world providing truly convenient and reliable on-line trading system. <br/><br/>
For more details please go to About GlobaliBlue.com
            </p>
        </div>
        <?php }?>
    </div><!-- end div id="sectionContent"-->
</div><!-- end div id="sectionContentWraper" -->       
  
  
  