<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = true;
$_CKEDITOR_ID = 'ebody';
/*
 * VALIDATE ADMINISTRATOR GROUP ------------------------------------------------
 */
if( $_SESSION['log_group'] == 'admin' )
{
    // get form feed.
    $fstatus = $_CLASS->getFormStatus();
    $fmessage = $_CLASS->getFormMessage();
    $fstyle = $_CLASS->getFormStyle();
    
    // get page id.
    $pid = trim($_GET['pid']);
    $page = $_CLASS->getPageContent($pid);
    
?>
        <div id="content-wrapper">
            <div id="vertical-menu">
                <?php $_MENU->loadMenu($_GLANG); ?>
            </div>
            <div id="inner-content">
                <!-- START CONTENT -->
                <h1 style="font-size:18px;"><?php echo $_LANG['CMS_EDIT_HEADER_LABEL'];?>: <?php echo $page['page_title'];?></h1>
                <?php
                    if( $fstatus ){
                ?>
                <div class="alert <?php echo $fstyle;?>">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <?php echo $fmessage;?>
                </div>
                <?php
                    }
                    
                    // check if page exists. only process if page exists.                    
                    if( is_array($page) && count($page) > 0 )
                    {
                ?>
                <form method="post">
                    <div class="input-prepend">
                        <textarea id="ebody" name="phtml" style="width:550px;"><?php echo $page['html'];?></textarea>
                    </div>
                    <input type="hidden" name="pid" value="<?php echo trim($_GET['pid']);?>" />
                    <input type="submit" name="updateBtn" value="<?php echo $_LANG['CMS_EDIT_SAVE_BUTTON'];?>" class="btn btn-info" />
                    <a href="#" class="btn" onclick="javascript:displayCSSTab();return false;"><i class="icon-cog"></i> <?php echo $_LANG['CMS_EDIT_EDIT_CSS_BUTTON'];?></a>
                    <a href="#" class="btn" onclick="javascript:displayJSTab();return false;"><i class="icon-cog"></i> <?php echo $_LANG['CMS_EDIT_EDIT_JS_BUTTON'];?></a>
                </form>
                <?php
                    }
                    else
                    {
                ?>
                <p><?php echo $_LANG['CMS_EDIT_REQUEST_ERROR_LABEL'];?></p>
                <?php
                    }
                    
                    // get page css and js.
                    $css = $_CLASS->getCSSContent($page['page_slug'],$page['page_group']);
                    $js  = $_CLASS->getJSContent($page['page_slug'],$page['page_group']);
                ?>
                <form id="_css" method="post" style="display:none;">
                    <h4><?php echo $_LANG['CMS_EDIT_CSS_HEADER'];?></h4>
                    <textarea name="cssInput" style="width:715px;height:350px;border:1px solid #CCC;"><?php echo stripslashes($css);?></textarea>
                    <input type="hidden" name="cssslug" value="<?php echo $page['page_group'] . '-' . $page['page_slug'];?>" />
                    <input type="submit" name="updatecssbtn" class="btn btn-info" value="<?php echo $_LANG['CMS_EDIT_SAVE_BUTTON'];?>" />
                </form>
                <form id="_js" method="post" style="display:none;">
                    <h4><?php echo $_LANG['CMS_EDIT_JS_HEADER'];?></h4>
                    <textarea name="jsInput" style="width:715px;height:350px;border:1px solid #CCC;"><?php echo stripslashes($js);?></textarea>
                    <input type="hidden" name="jsslug" value="<?php echo $page['page_group'] . '-' . $page['page_slug'];?>" />
                    <input type="submit" name="updatejsbtn" class="btn btn-info" value="<?php echo $_LANG['CMS_EDIT_SAVE_BUTTON'];?>" />
                </form>
                <!-- END CONTENT -->
            </div>
            <div class="clearfix"></div>
        </div>
<?php
} // end validation.
else
{
?>
        <p class="text-warning"><?php echo $_LOCAL['UNAUTHORIZED_ACCESS_MESSAGE'];?></p>
<?php
}