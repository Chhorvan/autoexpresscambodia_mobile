<?php
	if( !isset($_SESSION['log_group']) ){
		header('Location: '.BASE_RELATIVE.'login/');
	}
	
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = true;
$_CKEDITOR_ID = '_ebody';
/*
 * VALIDATE ADMINISTRATOR GROUP ------------------------------------------------
 */
if( $_SESSION['log_group'] == 'user' )
{
    // get form feed.
    $fstatus = $_CLASS->getFormStatus();
    $fmessage = $_CLASS->getFormMessage();
    $fstyle = $_CLASS->getFormStyle();
    
    // get lists.
    $maker_unique_list = $_CLASS->getMakerList();
    $model_unique_list = $_CLASS->getModelList();
    $body_type_list    = $_CLASS->getBodyList();
	$drive_type_list   = $_CLASS->getDriveList();
    
	$product_option_list = $_CLASS->getProductOptionList();
    $country_list = $_CLASS->getCountryList();
	$vehicle_type_list = $_CLASS->getVehicleTypeList();
	$car_maker_list = $_CLASS->getCarMakerList();
	$category_list = $_CLASS->getCategoryList();
	$fuel_type_list = $_CLASS->getFuelTypeList();
	$transmission_list = $_CLASS->getTransmissionList();
    // set input size for maker.
    ( count($maker_unique_list) > 0 ? $imaker = 'style="width:249px;margin-right:-1px;"' : $imaker = '' );
    ( count($model_unique_list) > 0 ? $imodel = 'style="width:249px;margin-right:-1px;"' : $imodel = '' );
    ( count($body_type_list) > 0 ? $ibody = 'style="width:249px;margin-right:-1px;"' : $ibody = '' );
    if( CURRENCY_CODE == 'USD' ){
        $decimal = '.';
        $thousand= ',';
    }
    else {
        $decimal = ',';
        $thousand= '.';
    }
    if(!isset($_GET['cid'])) $getId='';
	else $getId=$_GET['cid'];
	$product=array();
	
    // get generated car id.
	if(empty($getId)){
    	$carid = $_CLASS->getCarId();
	}else{
		$carid = $getId;
		$product = $_CLASS->getAllProducts($carid);
	}
	(isset($product['payment_terms']) ? $payment_terms = explode('^',$product['payment_terms']) : $payment_terms=array());
	(isset($product['other_options']) ? $other_options = explode('^',$product['other_options']) : $other_options=array());
	(isset($product['safety_device_options']) ? $safety_device_options = explode('^',$product['safety_device_options']) : $safety_device_options=array());
	(isset($product['exterior_options']) ? $exterior_options = explode('^',$product['exterior_options']) : $exterior_options=array());
	(isset($product['interior_options']) ? $interior_options = explode('^',$product['interior_options']) : $interior_options=array());
	
	
		
?>
<script>
var category="<?php if(isset($product['category'])) echo $product['category']; ?>";
var subCategoryGroup="<?php if(isset($product['sub_category_group'])) echo $product['sub_category_group']; ?>";
var subCategory="<?php if(isset($product['sub_category'])) echo $product['sub_category']; ?>";
var madeIn="<?php if(isset($product['made_in'])) echo $product['made_in']; ?>";
var condition="<?php if(isset($product['condition'])) echo $product['condition']; ?>";
var vehicleType="<?php if(isset($product['vehicle_type'])) echo $product['vehicle_type']; ?>";
var make="<?php if(isset($product['make'])) echo $product['make']; ?>";
var model="<?php if(isset($product['model'])) echo $product['model']; ?>";
var year="<?php if(isset($product['model_year'])) echo $product['model_year']; ?>";
var measureType="<?php if(isset($product['mileage_type'])) echo $product['mileage_type']; ?>";
var transmission="<?php if(isset($product['transmission'])) echo $product['transmission']; ?>";
var fuelType="<?php if(isset($product['fuel_type'])) echo $product['fuel_type']; ?>";
var country="<?php if(isset($product['location'])) echo $product['location']; ?>";
var door="<?php if(isset($product['door'])) echo $product['door']; ?>";
var driveType="<?php if(isset($product['drive_type'])) echo $product['drive_type']; ?>";
var driveAvail="<?php if(isset($product['drive_availability'])) echo $product['drive_availability']; ?>";
var steering="<?php if(isset($product['steering'])) echo $product['steering']; ?>";
</script>
        <div id="content-wrapper">
            <div id="vertical-menu">
                <?php $_MENU->loadMenu($_GLANG); ?>
            </div>
            <div id="inner-content">
                <!-- START CONTENT -->
                <h1><?php echo $_LANG['NEWCAR_HEADER'];?></h1>
                <?php
                    if( $fstatus ){
                ?>
                <div class="alert <?php echo $fstyle;?>">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <?php echo $fmessage;?>
                </div>
                <?php
                    }
                ?>
                <div id="stepper-wrap">
                    <div id="step1">
                        <p class="inner-step-p"><?php echo $_LANG['NEWCAR_STEP1_LABEL'];?></p>
                       
                        <p class="inner-step-p unselected-p"><?php echo $_LANG['NEWCAR_STEP3_LABEL'];?></p>
                        
                        <div class="clearfix"></div>
                    </div>
                    
                    
                    <div id="step2">
                        <p class="inner-step-p unselected-p"><?php echo $_LANG['NEWCAR_STEP1_LABEL'];?></p>
                        
           
                        <p class="inner-step-p"><?php echo $_LANG['NEWCAR_STEP3_LABEL'];?></p>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <form id="form-wrap" method="post">
                    <input type="hidden" name="caridInput" value="<?php echo $carid;?>" />
                    <div id="content1">
                        <h4><?php echo $_LANG['NEWCAR_STEP1_HEADER'];?></h4><br/><br/>
                        <div class="input-prepend input-append inputap">
                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_CATEGORY_LABEL'];?></span>
                            <select name="categorySelect" id="categorySelect" style="width:280px;">
                            	<option value="">- Select -</option>
                            <?php
                                if( count($category_list['Part']) > 0 ){
									
                                    for( $i=0; $i < count($category_list['Part']); $i++ ){
                                        if( !empty($category_list['Part'][$i]) ){
                                ?>
                                <option value="<?php echo $category_list['Part'][$i];?>"><?php echo $category_list['Part'][$i];?></option>
                            <?php
                                        }
                                    }
                                }
                                else {
                            ?>
                                <option value="">Other</option>
                            <?php
                                }
                            ?>
                            </select>
                            
                        </div>
                        <div class="input-prepend input-append inputap">
                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_SUB_CATEGORY_GROUP_LABEL'];?></span>
                            <select name="subCategoryGroupSelect" id="subCategoryGroupSelect" style="width:280px;"> 
                                <option value="">Other</option>
                            </select>
                            
                        </div>
                        <div class="input-prepend input-append inputap">
                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_SUB_CATEGORY_LABEL'];?></span>
                            <select name="subCategorySelect" id="subCategorySelect" style="width:280px;"> 
                                <option value="">Other</option>
                            </select>
                            
                        </div>
                        <div class="input-prepend input-append inputap">
                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_PART_NAME_LABEL'];?></span>
                            <input type="text" value="<?php if(isset($product['part_name'])) echo $product['part_name'];?>" maxlength="100" name="partNameInput" class="content-label" placeholder="<?php echo $_LANG['NEWCAR_PART_NAME_PLACEHOLDER'];?>" />
                            
                        </div>
						<div class="input-prepend input-append inputap">
                        	
                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_CONDITION_LABEL'];?></span>
                            <label><input type="radio" checked id="conditionInput" name="conditionInput" value="New"/><?php echo $_LANG['NEWCAR_CONDITION_NEW'];?></label>
                           	<label><input type="radio" name="conditionInput" value="Used"/><?php echo $_LANG['NEWCAR_CONDITION_USED'];?></label>
                            <label><input type="radio" name="conditionInput" value="Salvage"/><?php echo $_LANG['NEWCAR_CONDITION_SALVAGE'];?></label>
                            <label><input type="radio" name="conditionInput" value="Remade"/><?php echo $_LANG['NEWCAR_CONDITION_REMADE'];?></label>
                            
                        </div>
                        <div class="input-prepend input-append inputap">
                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_UNIT_WEIGHT_LABEL'];?></span>
                            <input type="text" value="<?php if(isset($product['unit_weight'])) echo $product['unit_weight'];?>" maxlength="20" name="unitWeightInput" class="content-label" placeholder="<?php echo $_LANG['NEWCAR_UNIT_WEIGHT_PLACEHOLDER'];?>" />
                            
                        </div>
                        <div class="input-prepend input-append inputap">
                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_PART_NUMBER_LABEL'];?></span>
                            <input type="text" value="<?php if(isset($product['part_number'])) echo $product['part_number'];?>" name="partNumberInput" class="content-label" placeholder="<?php echo $_LANG['NEWCAR_PART_NUMBER_PLACEHOLDER'];?>" />
                            
                        </div>
                        <div class="input-prepend input-append inputap">
                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_MANUFACTURER_LABEL'];?></span>
                            <input type="text" value="<?php if(isset($product['manufacturer'])) echo $product['manufacturer'];?>" maxlength="50" name="manufacturerInput" class="content-label" placeholder="<?php echo $_LANG['NEWCAR_MANUFACTURER_PLACEHOLDER'];?>" />
                            
                        </div>
                        <div class="input-prepend input-append inputap">
                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_MADE_IN_LABEL'];?></span>
							
                            <select name="madeInInputSelect" id="madeInInputSelect" style="width:280px;">
                            	<option value="">- Select -</option>
								<?php
                                    if( count($country_list['cc']) > 0 ){
                                    
                                        for( $i=0; $i < count($country_list['cc']); $i++ ){
                                            if( !empty($country_list['country_name'][$i]) ){
                                    ?>
                                    <option value="<?php echo $country_list['cc'][$i];?>"><?php echo $country_list['country_name'][$i];?></option>
                                <?php
                                        }
                                    }
                                }
                                else {
                            ?>
                                <option value="0"><?php echo $_LANG['NEWCAR_NOCOUNTRY_LABEL'];?></option>
                            <?php
                                }
                            ?>
                            </select>
                        
                        </div>
                        <div class="input-prepend input-append inputap">
                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_FITTING_MAKE_LABEL'];?></span>
                            <input type="text" value="<?php if(isset($product['fitting_make'])) echo $product['fitting_make'];?>" name="fittingMakeInput" class="content-label" placeholder="<?php echo $_LANG['NEWCAR_FITTING_MAKE_PLACEHOLDER'];?>" />
                            
                        </div>
                        <div class="input-prepend input-append inputap">
                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_COLOR_LABEL'];?></span>
                            <input type="text" value="<?php if(isset($product['color'])) echo $product['color'];?>" maxlenth="50" name="colorInput" class="content-label" placeholder="<?php echo $_LANG['NEWCAR_COLOR_PLACEHOLDER'];?>" />
                            
                        </div>
                        <div class="input-prepend input-append inputap">
                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_VEHICLE_TYPE_LABEL'];?></span>
                            <select id="vehicleTypeSelect" name="vehicleTypeSelect" style="width:280px;">
                            	<option value="">- Select -</option>
								<?php
                                    if( count($vehicle_type_list['type_name']) > 0 ){
                                    
                                        for( $i=0; $i < count($vehicle_type_list['type_name']); $i++ ){
                                            if( !empty($vehicle_type_list['type_name'][$i]) ){
                                    ?>
                                    <option value="<?php echo $vehicle_type_list['type_name'][$i];?>"><?php echo $vehicle_type_list['type_name'][$i];?></option>
                                <?php
                                            }
                                        }
                                    }
                                    else {
                                ?>
                                <option value="0"></option>
                            <?php
                                }
                            ?>
                            </select>
                            
                        </div>
                        <div class="input-prepend input-append inputap">
                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_MAKER_LABEL'];?></span>
                            
                            <select style="width:280px" id="makerInputSelect" name="makerInputSelect" style="width:280px;">
                            	<option value="">- Select -</option>
								<?php
                                    
                                    if( count($car_maker_list['id']) > 0 ){
                                    
                                        for( $i=0; $i < count($car_maker_list['id']); $i++ ){
                                            if( !empty($car_maker_list['id'][$i]) ){
                                    ?>
                                    <option value="<?php echo $car_maker_list['id'][$i];?>"><?php echo $car_maker_list['maker'][$i];?></option>
                                <?php
                                            }
                                        }
                                    }
                                    else {
                                ?>
                               
                                <option value="Others">Others</option>
                            <?php
                                }
                            ?>
                            </select>
                            <input style="width:250px;display:none;" type="text" maxlength="30" id="makerInput" name="makerInput" class="content-label" placeholder="" />
                        </div>
                       
                        <div class="input-prepend inputap">
                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_MODEL_LABEL'];?></span>
                             <select style="width:280px" id="modelInputSelect" name="modelInputSelect" style="width:280px;">
                                <option value="">- Select -</option>
                                <option value="Others">Others</option>
                            </select>
                            <input  style="width:250px;display:none" type="text" maxlength="50" id="modelInput" name="modelInput" class="content-label" placeholder="" />
                        </div>
                        <div class="input-prepend input-append inputap">
                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_CLASS_LABEL'];?></span>
                            <input type="text" value="<?php if(isset($product['class'])) echo $product['class'];?>" maxlength="50" name="classInput" class="content-label" placeholder="<?php echo $_LANG['NEWCAR_CLASS_PLACEHOLDER'];?>" />
                            
                        </div>
                        <div class="input-prepend input-append inputap">
                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_CHASSIS_NO_LABEL'];?></span>
                            <input type="text" value="<?php if(isset($product['chassis_no'])) echo $product['chassis_no'];?>" maxlength="50" name="chassisNoInput" class="content-label" placeholder="<?php echo $_LANG['NEWCAR_CHASSIS_NO_PLACEHOLDER'];?>" />
                            
                        </div>
                        <div class="input-prepend input-append inputap">
                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_MANU_DATE_LABEL'];?></span>
                            <input type="text" value="<?php if(isset($product['manufacturer_date'])) echo $product['manufacturer_date'];?>" name="manDateInput" id="manDateInput" class="content-label" placeholder="<?php echo $_LANG['NEWCAR_MANU_DATE_PLACEHOLDER'];?>" />
                            
                        </div>
                        <div class="input-prepend inputap">
                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_YEAR_LABEL'];?></span>
                            <select id="yearInput" name="yearInput" style="width:280px;">
                            	<option value="">- Select -</option>
								<?php
                                for( $i=date('Y'); $i > 1900; $i-- ){
                                ?>
                                    <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                <?php
                                    }
                                ?>
                            </select>
                        </div>
						
						
						
						
						<div class="input-prepend input-append inputap">
                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_MILES_LABEL'];?></span>
                            <input type="text" value="<?php if(isset($product['mileage'])) echo $product['mileage'];?>" placeholder="<?php echo $_LANG['NEWCAR_MILES_PLACEHOLDER'];?>" name="milesInput" style="width:280px;margin-right:-1px;" class="content-label" />
                            <label><input type="radio" checked value="Km" name="measureTypeInput">Km</label>
                            <label><input type="radio" value="Mile" name="measureTypeInput">Mile</label>
                            
                        </div>
                        <div class="input-prepend input-append inputap">
                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_ENGINE_VOLUME_LABEL'];?></span>
                            <input type="text" value="<?php if(isset($product['engine_volume'])) echo $product['engine_volume'];?>" name="engineVolumeInput" class="content-label" placeholder="<?php echo $_LANG['NEWCAR_ENGINE_VOLUME_PLACEHOLDER'];?>" />
                            
                        </div>
                        <div class="input-prepend input-append inputap">
                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_AXLE_QTY_LABEL'];?></span>
                            <input type="text" value="<?php if(isset($product['axle_qty'])) echo $product['axle_qty'];?>" name="axleQtyInput" class="content-label" placeholder="<?php echo $_LANG['NEWCAR_AXLE_QTY_PLACEHOLDER'];?>" />
                            
                        </div>
						<div class="input-prepend inputap">
                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_STEERING_LABEL'];?></span>
                            <label><input type="radio" checked name="steeringInput" value="LHD"/><?php echo $_LANG['NEWCAR_STEERING_LHD'];?></label>
                           	<label><input type="radio" name="steeringInput" value="RHD"/><?php echo $_LANG['NEWCAR_STEERING_RHD'];?></label>
                           
                        </div>
                        <div class="input-prepend inputap">
                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_TRANSMISSION_LABEL'];?></span>
                            <select id="transmissionInputSelect" name="transmissionInputSelect" style="width:280px;">
                                <option value="">- Select -</option>
                                <?php
                                    if( count($transmission_list['transmission_name']) > 0 ){
                                    
                                        for( $i=0; $i < count($transmission_list['transmission_name']); $i++ ){
                                            if( !empty($transmission_list['transmission_name'][$i]) ){
                                    ?>
                                    <option value="<?php echo $transmission_list['transmission_name'][$i];?>"><?php echo $transmission_list['transmission_name'][$i];?></option>
                                <?php
                                            }
                                        }
                                    }
                                    else {
                                ?>
                                    <option value="0"></option>
                                <?php
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="input-prepend inputap">
                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_FUEL_LABEL'];?></span>
                            <select id="carFuelInputSelect" name="carFuelInputSelect" style="width:280px;">
                                <option value="">- Select -</option>
                                <?php
                                    if( count($fuel_type_list['fuel_name']) > 0 ){
                                    
                                        for( $i=0; $i < count($fuel_type_list['fuel_name']); $i++ ){
                                            if( !empty($fuel_type_list['fuel_name'][$i]) ){
                                    ?>
                                    <option value="<?php echo $fuel_type_list['fuel_name'][$i];?>"><?php echo $fuel_type_list['fuel_name'][$i];?></option>
                                <?php
                                            }
                                        }
                                    }
                                    else {
                                ?>
                                    <option value="0"></option>
                                <?php
                                    }
                                ?>
                            </select>
                        </div>
                         <div class="input-prepend input-append inputap">
                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_NUMBER_CARGO_LABEL'];?></span>
                            <input type="text" value="<?php if(isset($product['number_cargo'])) echo $product['number_cargo'];?>" name="numberCargoInput" class="content-label" placeholder="<?php echo $_LANG['NEWCAR_NUMBER_CARGO_PLACEHOLDER'];?>" />
                            
                        </div>
                        <div class="input-prepend input-append inputap">
                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_CARGO_BOX_SIZE_LABEL'];?></span>
                            <input type="text" value="<?php if(isset($product['cargo_box_size'])) echo $product['cargo_box_size'];?>" name="cargoBoxSizeInput" class="content-label" placeholder="<?php echo $_LANG['NEWCAR_CARGO_BOX_SIZE_PLACEHOLDER'];?>" />
                            
                        </div>
                        <div class="input-prepend input-append inputap">
                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_LENGTH_LABEL'];?></span>
                            <input type="text" value="<?php if(isset($product['length'])) echo $product['length'];?>" id="lengthInput" name="lengthInput" class="content-label" placeholder="<?php echo $_LANG['NEWCAR_LENGTH_PLACEHOLDER'];?>" />
                            
                        </div>
                        <div class="input-prepend input-append inputap">
                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_HEIGHT_LABEL'];?></span>
                            <input type="text" value="<?php if(isset($product['height'])) echo $product['height'];?>" name="heightInput" class="content-label" placeholder="<?php echo $_LANG['NEWCAR_HEIGHT_PLACEHOLDER'];?>" />
                            
                        </div>
                        <div class="input-prepend input-append inputap">
                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_WIDTH_LABEL'];?></span>
                            <input type="text" value="<?php if(isset($product['width'])) echo $product['width'];?>" name="widthInput" class="content-label" placeholder="<?php echo $_LANG['NEWCAR_WIDTH_PLACEHOLDER'];?>" />
                            
                        </div>
                         <div class="input-prepend input-append inputap">
                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_TOTAL_WEIGHT_LABEL'];?></span>
                            <input type="text" value="<?php if(isset($product['total_weight'])) echo $product['total_weight'];?>" name="totalWeightInput" class="content-label" placeholder="<?php echo $_LANG['NEWCAR_TOTAL_WEIGHT_PLACEHOLDER'];?>" />
                            
                        </div>
						<div class="input-prepend inputap">
                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_COUNTRY_LIST_LABEL'];?></span>
                            <?php
                                
                            ?>
							
                            <select id="countryInputSelect" name="countryInputSelect" style="width:280px;">
                                <option value="">- Select -</option>
                                <?php
                                    if( count($country_list['cc']) > 0 ){
                                    
                                        for( $i=0; $i < count($country_list['cc']); $i++ ){
                                            if( !empty($country_list['country_name'][$i]) ){
                                    ?>
                                    <option value="<?php echo $country_list['cc'][$i];?>"><?php echo $country_list['country_name'][$i];?></option>
                                <?php
                                            }
                                        }
                                    }
                                    else {
                                ?>
                                    <option value="0"><?php echo $_LANG['NEWCAR_NOCOUNTRY_LABEL'];?></option>
                                <?php
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="input-prepend input-append inputap">
                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_PRICE_LABEL'];?></span>
                            
                            <input type="text" value="<?php if(isset($product['price'])) echo $product['price'];?>" name="priceInput" class="content-label" placeholder="<?php echo $_LANG['NEWCAR_PRICE_PLACEHOLDER'];?>" />  
                        </div>
                        <div class="input-prepend input-append inputap">
                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_LOADING_WEIGHT_LABEL'];?></span>
                            
                            <input type="text" value="<?php if(isset($product['loading_weight'])) echo $product['loading_weight'];?>" name="loadingWeightInput" class="content-label" placeholder="<?php echo $_LANG['NEWCAR_LOADING_WEIGHT_PLACEHOLDER'];?>" />  
                        </div>
                        <div class="input-prepend inputap">
                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_VIN_LABEL'];?></span>
                            <input type="text" value="<?php if(isset($product['vin_number'])) echo $product['vin_number'];?>" name="vinInput" class="content-label" maxlength="150" placeholder="<?php echo $_LANG['NEWCAR_VIN_PLACEHOLDER'];?>" />
                        </div>
                        <div class="input-prepend inputap">
                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_NUMBER_PASSENGER_LABEL'];?></span>
                            <input type="text" value="<?php if(isset($product['number_passenger'])) echo $product['number_passenger'];?>" name="numberPassengerInput" class="content-label" maxlength="150" placeholder="<?php echo $_LANG['NEWCAR_NUMBER_PASSENGER_PLACEHOLDER'];?>" />
                        </div>
                        <div class="input-prepend inputap">
                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_DOORS_LABEL'];?></span>
                            <select id="doorInput" name="doorInput" style="width:280px;">
                                
                                <option value="1">1 <?php echo $_LANG['NEWCAR_DOOR_SELECT_LABEL'];?></option>
                                <option value="2">2 <?php echo $_LANG['NEWCAR_DOOR_SELECT_LABEL'];?></option>
                                <option value="3">3 <?php echo $_LANG['NEWCAR_DOOR_SELECT_LABEL'];?></option>
                                <option value="4">4 <?php echo $_LANG['NEWCAR_DOOR_SELECT_LABEL'];?></option>
                                <option value="5">5 <?php echo $_LANG['NEWCAR_DOOR_SELECT_LABEL'];?></option>
                                <option value="6">6 <?php echo $_LANG['NEWCAR_DOOR_SELECT_LABEL'];?></option>
                                <option value="7">7 <?php echo $_LANG['NEWCAR_DOOR_SELECT_LABEL'];?></option>
                                <option value="8">8 <?php echo $_LANG['NEWCAR_DOOR_SELECT_LABEL'];?></option>
                                <option value="9">9 <?php echo $_LANG['NEWCAR_DOOR_SELECT_LABEL'];?></option>
                                <option value="10">10 <?php echo $_LANG['NEWCAR_DOOR_SELECT_LABEL'];?></option>
                            </select>
                        </div>
                        <div class="input-prepend inputap">
                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_BODY_LABEL'];?></span>
                            <input type="text" value="<?php if(isset($product['exterior_color'])) echo $product['exterior_color'];?>" name="bodyColorInput" class="content-label" maxlength="150" placeholder="<?php echo $_LANG['NEWCAR_BODY_PLACEHOLDER'];?>" />
                        </div>
                        
                        <div class="input-prepend input-append inputap">
                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_ENGINE_POWER_LABEL'];?></span>
                            <input type="text" value="<?php if(isset($product['engine_power'])) echo $product['engine_power'];?>" name="enginePowerInput" class="content-label" placeholder="<?php echo $_LANG['NEWCAR_ENGINE_POWER_PLACEHOLDER'];?>" />  
                        </div>
                        <div class="input-prepend inputap">
                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_NUMBER_CYLINDER_LABEL'];?></span>
                            <input type="text" value="<?php if(isset($product['number_cylinder'])) echo $product['number_cylinder'];?>" name="numberCylinderInput" class="content-label" maxlength="150" placeholder="<?php echo $_LANG['NEWCAR_NUMBER_CYLINDER_PLACEHOLDER'];?>" />
                        </div>
                        <div class="input-prepend inputap">
                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_DRIVE_AVAIL_LABEL'];?></span>
                            <label><input type="radio" name="driveAvail" value="N"/><?php echo $_LANG['NEWCAR_IMPOSSIBLE_DRIVE'];?></label>
                           	<label><input type="radio" checked name="driveAvail" value="Y"/><?php echo $_LANG['NEWCAR_POSSIBLE_DRIVE'];?></label>
                           
                        </div>
                        
                        <div class="input-prepend inputap">
                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_DRIVE_LABEL'];?></span>
                            
                            <select id="driveInputSelect" name="driveInputSelect" style="width:280px;">
                            	<option value="">- Select -</option>
                                <?php
                                    for( $i=0; $i < count($drive_type_list); $i++ ){
                                ?>
                                <option value="<?php echo $drive_type_list[$i];?>"><?php echo $drive_type_list[$i];?></option>
                                <?php
                                    }
                                ?>
                            </select>
                            
                        </div>
                        
                        
                        
                        <div class="input-prepend inputap">
                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_PAYMENT_TERM_LABEL'];?></span>
                           
                            <label><input <?php if (in_array("T/T", $payment_terms)) echo "checked";?> type="checkbox" name="paymentTermCheck[]" value="T/T" /> <span>T/T</span></label>
                            <label><input <?php if (in_array("L/C", $payment_terms)) echo "checked";?> type="checkbox" name="paymentTermCheck[]" value="L/C" /> <span>L/C</span></label>
                            <label><input <?php if (in_array("Other", $payment_terms)) echo "checked";?> type="checkbox" name="paymentTermCheck[]" value="Other" /> <span>Other</span></label>
                           
                            
                        </div>
                        <div class="input-prepend inputap option">
                            <span style="width:100%;" class="add-on title-label"><?php echo $_LANG['NEWCAR_OTHER_OPTIONS_LABEL'];?></span>
                            	<?php
									
									
                            		for( $i=0; $i < count($product_option_list['Bus Options']); $i++ ){	
									?>					
                            <label><input type="checkbox" name="otherOptionsCheck[]" <?php if (in_array($product_option_list['Bus Options'][$i], $other_options)) echo "checked";?> value="<?php echo $product_option_list['Bus Options'][$i];?>"/> <span><?php echo $product_option_list['Bus Options'][$i];?></span></label>
                            		<?php }?>						
							</ul>
                           
                            
                        </div>
                        <div class="input-prepend inputap option">
                            <span style="width:100%;" class="add-on title-label"><?php echo $_LANG['NEWCAR_SAFETY_DEVICE_OPTIONS_LABEL'];?></span>
                            	<?php
									
                            		for( $i=0; $i < count($product_option_list['Safety Device']); $i++ ){	
									?>					
                            <label><input type="checkbox" name="safetyDeviceOptionsCheck[]" <?php if (in_array($product_option_list['Safety Device'][$i], $safety_device_options)) echo "checked";?> value="<?php echo $product_option_list['Safety Device'][$i];?>"/> <span><?php echo $product_option_list['Safety Device'][$i];?></span></label>
                            		<?php }?>						
							</ul>
                           
                            
                        </div>
                        <div class="input-prepend inputap option">
                            <span style="width:100%;" class="add-on title-label"><?php echo $_LANG['NEWCAR_EX_OPTIONS_LABEL'];?></span>
                            	<?php
									
                            		for( $i=0; $i < count($product_option_list['Exterior Option']); $i++ ){	
									?>					
                            <label><input type="checkbox" name="exOptionsCheck[]" <?php if (in_array($product_option_list['Exterior Option'][$i], $exterior_options)) echo "checked";?> value="<?php echo $product_option_list['Exterior Option'][$i];?>"/> <span><?php echo $product_option_list['Exterior Option'][$i];?></span></label>
                            		<?php }?>						
							</ul>
                           
                            
                        </div>
                        <div class="input-prepend inputap option">
                            <span style="width:100%;" class="add-on title-label"><?php echo $_LANG['NEWCAR_IN_OPTIONS_LABEL'];?></span>
                            	<?php
									
                            		for( $i=0; $i < count($product_option_list['Interior Option']); $i++ ){	
									?>					
                            <label>
                            <input type="checkbox" name="inOptionsCheck[]" <?php if (in_array($product_option_list['Interior Option'][$i], $interior_options)) echo "checked";?> value="<?php echo $product_option_list['Interior Option'][$i];?>"/>
                            <span><?php echo $product_option_list['Interior Option'][$i];?></span>
                            </label>
                            		<?php }?>						
							</ul>
                           
                            
                        </div>
                        
                        <div class="rich-input">
                        	
                            <h4><?php echo $_LANG['NEWCAR_SELLER_COMMENT_HEADER'];?></h4>
                            <textarea id="sellerCommentText" name="sellerCommentText">
								<?php if(isset($product['seller_comment'])) echo $product['seller_comment'];?>
                            </textarea>
                            
           				</div>
                        <!-------------- UPLOAD MEDIA BLOCK --------->
                        <h4><?php echo $_LANG['NEWCAR_UPLOAD_MEDIA_HEADER'];?></h4>
                        <div class="input-prepend input-append inputap">
                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_OPTION_LABEL'];?></span>
                            <select id="opt1" onchange="validateImageType()" style="width:274px;">
                                <option value="none"><?php echo $_LANG['NEWCAR_SELECT_OPTION_SELECT'];?></option>
                                <option value="image"><?php echo $_LANG['NEWCAR_IMAGE_SELECT'];?></option>
                                <option value="video"><?php echo $_LANG['NEWCAR_VIDEO_SELECT'];?></option>
                            </select>
                            <select id="opt2" onchange="displayController('<?php echo BASE_RELATIVE;?>view/3rdparty/file_uploader/user-addpic.php?cid=<?php echo $carid;?>')" style="width:274px;" disabled="disabled">
                                <option value="none"><?php echo $_LANG['NEWCAR_SELECT_OPTION_SELECT'];?></option>
                                <option value="interior"><?php echo $_LANG['NEWCAR_INTERIOR_SELECT'];?></option>
                                <option value="exterior"><?php echo $_LANG['NEWCAR_EXTERIOR_SELECT'];?></option>
                            </select>
                        </div>
                        <div id="video-wrap" style="display:none;" class="input-prepend input-append inputap">
                            <span class="add-on title-label"><?php echo $_LANG['NEWCAR_YOUTUBE_URL_LABEL'];?></span>
                            <input id="videoUrlInput" type="text" class="content-label" style="width:435px;" placeholder="<?php echo $_LANG['NEWCAR_YOUTUBE_URL_PLACEHOLDER'];?>" />
                            <input id="vidBtn" onclick="saveVideoHandler('<?php echo $carid;?>');return false;" type="submit" class="btn btn-info" style="width:100px;" value="<?php echo $_LANG['NEWCAR_SAVE_BUTTON'];?>" />
                        </div>
                        <div id="upload-wrap" style="display:none;" class="input-prepend input-append inputap">
                            <h4><?php echo $_LANG['NEWCAR_UPLOAD_LABEL'];?></h4>
                            <iframe id="iupload" width="700" height="100" style="border:0;"></iframe>
                        </div>
                    </div>
                    <br>
                    <div id="outputwrap"></div>
                    <div id="outoutimg"></div>
                    <div class="clearfix"></div>
                    <br>
                    <a id="prevbtn" href="#" onclick="return previousFormHandler()" class="btn btn-small" style="display:none;margin-right:10px;">
                        <i class="icon-circle-arrow-left" style="margin-right:10px;"></i><?php echo $_LANG['NEWCAR_PREVIOUS_BUTTON_LABEL'];?>
                    </a>
                    <a id="nextbtn" href="#" onclick="return nextFormHandler()" class="btn btn-small" style="display:none;margin-right:10px;">
                        <?php echo $_LANG['NEWCAR_NEXT_BUTTON_LABEL'];?><i class="icon-circle-arrow-right" style="margin-left:10px;"></i>
                    </a>
                    <input id="submitbtn" style="" type="submit" name="savebtn" class="btn btn-small btn-info" value="<?php echo $_LANG['NEWCAR_SAVE_BUTTON'];?>" />
                </form>
                <!-- END CONTENT -->
                        
                        
                        <!-------------- END UPLOAD MEDIA BLOCK --------->
                    </div>
                   
                    
                    <div class="clearfix"></div>
                    <div id="content2">
                        
            </div>
            <div class="clearfix"></div>
        </div>
        <script language="javascript" type="text/javascript">
            /* preview uploaded image */
            function carImageUploaded(url,mode){
				        var pid= Math.round((Math.random()*1000));
                        var did = 'img' +pid;
                        var wrap = document.getElementById('outoutimg');
                        var html = '<div id="'+did+'" class="img-wrap">';
		
                        html += '<img src="<?php echo BASE_RELATIVE;?>'+url+'" width="200" height="150" style="margin:5px;" />';
					 
                        html += '<p style="margin:0;padding-left:5px;">';
                        html += "<input type='radio' name='primary_photo[]' value="+pid+">Primary Photo";
                        html += '<a href="#" onclick="removeImageHandler(\'<?php echo BASE_ROOT;?>'+url+'\',\''+did+'\');return false;" class="btn btn-small btn-danger" style="float:right;margin-right:5px;">×</a>';
                        html += '</p>';
                        html += '</div>';
                        
                        wrap.innerHTML += html;
                    }
                    function ucfirst (str) {
                        str += '';
                        var f = str.charAt(0).toUpperCase();
                        return f + str.substr(1);
                   }
        </script>
<?php
} // end validation.
else
{
?>
        <p class="text-warning"><?php echo $_LOCAL['UNAUTHORIZED_ACCESS_MESSAGE'];?></p>
<?php
}