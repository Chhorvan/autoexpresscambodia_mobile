

/*
 * DISPLAY ALERT FEEDBACK BLOCKS -----------------------------------------------
 * Used by class-form-feedback to display alert messages. DO NOT DELETE!
 */

$('#feed_return_block').ready(function(){
    if( $('#feed_return_block').length > 0 )
    {
        setTimeout(hideBox, 5000);
    }
});

function hideBox()
{
    $('#feed_return_block').slideToggle(1000, removeBox);
}
function removeBox()
{
    $('#feed_return_block').remove();
}

/*
 * Handle admin menu collapse --------------------------------------------------
 */
function displayMenuTab(tab)
{
    var tar = document.getElementById(tab);

    if( tar != null && tar != undefined ){
        $(tar).slideToggle();
    }

    return;
}

