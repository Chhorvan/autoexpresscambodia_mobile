/* handle next form (btn click) */
var cur_step = 1;
var cur_img;
var tab;
var next_btn;
var prev_btn;
var img_st1;
 


$(document).ready(function(){
	
    tab = document.getElementById('content1');
    next_btn = document.getElementById('nextbtn');
    prev_btn = document.getElementById('prevbtn');
    img_st1 = document.getElementById('step1');
    
    opt1 = document.getElementById('opt1');
    opt2 = document.getElementById('opt2');
    vid  = document.getElementById('video-wrap');
    upl  = document.getElementById('upload-wrap');
    
    cur_img = img_st1;
	$("#sellerCommentText").ckeditor();
	$("#modelInputSelect").change(function(e) {
		if($("#modelInputSelect option:selected").text()=='Others'||$("#modelInputSelect option:selected").text()=='Other')
			$("#modelInput").css('display','inline');
		else{
			$("#modelInput").css('display','none');
			$("#modelInput").val('');
		}
	});
	$( "#makerInputSelect" ).change(function(e) {
		if($("#makerInputSelect option:selected").text()=='Others'||$("#makerInputSelect option:selected").text()=='Other')
			$("#makerInput").css('display','inline');
		else{
			$("#makerInput").css('display','none');
			$("#makerInput").val('');
		}
		var mid=$(this).val();
      	$("#modelInputSelect").load('../ajax/get_car_model.php?mid='+mid, function() {
			if($("#modelInputSelect option:selected").text()=='Others'||$("#modelInputSelect option:selected").text()=='Other')
				$("#modelInput").css('display','inline');
			else{
				$("#modelInput").css('display','none');
				$("#modelInput").val('');
			}
		});
    });
	$( "#categorySelect" ).change(function(e) {
		
		var cname=encodeURIComponent($("#categorySelect").val());
		var ptype='Part';
		var scgroup='';
		
      	
		$("#subCategoryGroupSelect").load('../ajax/get_sub_category_group.php?cname='+cname+'&ptype='+ptype, function() {
			
			scgroup=encodeURIComponent($("#subCategoryGroupSelect").val());
			$("#subCategorySelect").load('../ajax/get_sub_category.php?scgroup='+scgroup+'&cname='+cname+'&ptype='+ptype);
		});
		
		
		
    });
	$( "#subCategoryGroupSelect" ).change(function(e) {
		var cname=encodeURIComponent($("#categorySelect").val());
		var ptype='Part';
		if ($("#subCategoryGroupSelect").length > 0){
			scgroup=encodeURIComponent($("#subCategoryGroupSelect").val());
		}
		
      	$("#subCategorySelect").load('../ajax/get_sub_category.php?scgroup='+scgroup+'&cname='+cname+'&ptype='+ptype);
		
    });
	loadAllDropdown();
	//setDropdownSelection();
});
	
function loadAllDropdown(){
	if(category!='')
		$("#categorySelect").val(category);
	var cname=encodeURIComponent($("#categorySelect").val());
	var foundInMakerSelect=false;
	var ptype='Part';
	var scgroup='';
	$("#subCategoryGroupSelect").load('../ajax/get_sub_category_group.php?cname='+cname+'&ptype='+ptype, function() {
		if(subCategoryGroup!='')
			$("#subCategoryGroupSelect").val(subCategoryGroup);
		scgroup=encodeURIComponent($("#subCategoryGroupSelect").val());
		
		$("#subCategorySelect").load('../ajax/get_sub_category.php?scgroup='+scgroup+'&cname='+cname+'&ptype='+ptype, function() {
			if(subCategory!='')
			$("#subCategorySelect").val(subCategory);
		});	
		
	});
	$("#madeInInputSelect").val(madeIn);
	if(condition!='') $("[name=conditionInput]").val([condition]);
	//alert(vehicleType);
	$("#vehicleTypeSelect").val(vehicleType);
	
	
	if($("#makerInputSelect option:selected").text()=='Others'||$("#makerInputSelect option:selected").text()=='Other')
		$("#makerInput").css('display','inline');
	else{
		$("#makerInput").css('display','none');
		$("#makerInput").val('');
	}
	$("#makerInputSelect option").each(function() {
		if($(this).text() == make) {
			foundInMakerSelect = true;
			$(this).attr('selected', 'selected');            
		}  
	}); 
	if(foundInMakerSelect==false&&make!=''){
		$("#makerInputSelect option").each(function() {
			if($(this).text() == "Other"||$(this).text() == "Others") {
				foundInMakerSelect = true;
				$(this).attr('selected', 'selected');            
			}  
		}); 
		$("#makerInput").css('display','inline');
		$("#makerInput").val(make);
	}
	var mid=$("#makerInputSelect").val();
	$("#modelInputSelect").load('../ajax/get_car_model.php?mid='+mid, function() {
		
		$("#modelInputSelect").val(model);
		if($("#modelInputSelect").val()!=model && model!=''){
			//$("#modelInputSelect").val('Other');
			$("#modelInput").val(model);
		}
		if($("#modelInputSelect option:selected").text()=='Others'||$("#modelInputSelect option:selected").text()=='Other')
			$("#modelInput").css('display','inline');
			
	});
	$("#yearInput").val(year);
	
	if(measureType!='') $("[name=measureTypeInput]").val([measureType]);
	//alert(transmission);
	$("#transmissionInputSelect").val(transmission);
	$("#carFuelInputSelect").val(fuelType);
	$("#countryInputSelect").val(country);
	$("#doorInput").val(door);
	if(steering!='') $("[name=steeringInput]").val([steering]);
	$("#driveInputSelect").val(driveType);
	if(driveAvail!='') $("[name=driveAvail]").val([driveAvail]);
	//alert(make);
	//$("#makerInputSelect").val($("#makerInputSelect option:contains('"+make+"')").val());
							 
	
	//$("[name=makerInputSelect]").val([make]);
	//$("#modelInputSelect").val(model);
	
}

function nextFormHandler(){
    if( tab == undefined || next_btn == undefined || prev_btn == undefined ){
        console.log('undefined objects cannot be set!');
        return false;
    }
    
    cur_step += 1;
    
    // display image step tracker.
    cur_img.style.display = 'none';
    
    var iname = 'step' + cur_step;
    cur_img = document.getElementById(iname);
    cur_img.style.display = 'block';
    
    // display previous button.
    document.getElementById('prevbtn').style.display = 'inline-block';
    
    // disable next button if reaches step 4 and submit button.
    if( cur_step >= 2 ){
        document.getElementById('nextbtn').style.display = 'none';
        document.getElementById('submitbtn').style.display = 'inline-block';
    }
    
    var aname = 'content' + cur_step;
    
    $(tab).fadeOut();
    
    tab = document.getElementById(aname);
    
    $(tab).fadeIn();
    
    return false;
}

/* handle previous form */
function previousFormHandler(){
    if( tab == undefined || next_btn == undefined || prev_btn == undefined ){
        console.log('undefined objects cannot be set!');
        return false;
    }
    
    // hide submit button.
    console.log('current step: ' + cur_step);
    
    if( cur_step >= 2 ){
        document.getElementById('submitbtn').style.display = 'none';
    }
    
    cur_step -= 1;
    
    // display image step tracker.
    cur_img.style.display = 'none';
    
    var iname = 'step' + cur_step;
    cur_img = document.getElementById(iname);
    cur_img.style.display = 'block';
    
    // display next button.
    document.getElementById('nextbtn').style.display = 'inline-block';
    
    // hide previous if first page.
    if( cur_step <= 1 ){
        document.getElementById('prevbtn').style.display = 'none';
    }
    
    var aname = 'content' + cur_step;
    
    $(tab).fadeOut();
    
    tab = document.getElementById(aname);
    
    $(tab).fadeIn();
    
    return false;
}

/* redefine wysiwyg */


/* handle step4 selectors */
var opt1;
var opt2;
var vid;
var upl;
function validateImageType(){
    if( opt1 == undefined || opt2 == undefined || vid == undefined || upl == undefined ){
        opt1 = document.getElementById('opt1');
        opt2 = document.getElementById('opt2');
        vid  = document.getElementById('video-wrap');
        upl  = document.getElementById('upload-wrap');
    }
    
    var val = opt1.options[opt1.selectedIndex].value;
    
    // disable and hide all controllers.
    if( val == 'none' ){
        opt2.disabled = true;
        opt2.selectedIndex = 0;
        vid.style.display = 'none';
        upl.style.display = 'none';
    }
    
    // display video controller.
    else if( val == 'video' ){
        opt2.disabled = true;
        opt2.selectedIndex = 0;
        vid.style.display = 'inline-block';
        upl.style.display = 'none';
    }
    
    // display option 2 but hide controllers.
    else {
        opt2.disabled = false;
        vid.style.display = 'none';
        upl.style.display = 'none';
    }
}

function displayController(url){
    var val = opt2.options[opt2.selectedIndex].value;
    
    // hide controllers if none.
    if( val == 'none' ){
        
        vid.style.display = 'none';
        upl.style.display = 'none';
    }
    
    // display image uploader.
    else if( val == 'interior' || val == 'exterior' ){
        vid.style.display = 'none';
        upl.style.display = 'inline-block';
    }
    
    // display video field.
    else {
        vid.style.display = 'inline-block';
        upl.style.display = 'none';
    }
    
    // add interior/exterior to iframe src.
    document.getElementById('iupload').src = url + '&mode=' + val;
    
}

/* handle save video url form */
function saveVideoHandler(cid){
    // car id must be defined.
    if( cid == undefined || cid.length < 5 ){
        return false;
    }
    
    var value = document.getElementById('videoUrlInput').value;
    
    if( value == undefined || value.length < 1 ){
        return false;
    }
    
    // disable save button.
    document.getElementById('vidBtn').disabled = true;
    
    // send ajax request.
    $.post(
        '../ajax/save_video_url.php',
        {
            carid: cid,
            url: value
        },
        function(data,status){
            var result = data.split(',');
            
            document.getElementById('videoUrlInput').value = ''; // enpty video url input field.
            document.getElementById('vidBtn').disabled = false;   // enable button.
            
            if( result[0] == 'ir' ){
                alert(result[1]);
                return;
            }
            else if( result[0] == 'ok' ){
                // video url add successfully. display list.
                var div = document.getElementById('outputwrap');
                var itemid = 'link' + Math.floor((Math.random()*1000)+1);
                div.innerHTML += '<p id="'+itemid+'"><b>Video URL:</b> ' + result[1] + '<a href="#" onclick="removeVideo(\''+result[1]+'\',\''+cid+'\',\''+itemid+'\'); return false;" class="btn btn-small btn-danger" style="float:right;">×</a></p>';
                return;
            }
            else {
                // unknown error.
                alert('Unknown error: ' + result[0] + ':' + result[1]);
                return;
            }
        }
    );
}

/* handle upload image form */
function uploadImageHandler(cid){
    // car id must be defined.
    if( cid == undefined || cid.length < 5 ){
        return false;
    }
    
    
}

/* remove video link */
function removeVideo(url,carid,itemid){
    if( url == undefined || carid == undefined || itemid == undefined ){
        return false;
    }
    
    $.post(
        '../ajax/remove_vid_url.php',
        {
            cid: carid,
            dest: url
        },
        function(data,status){
            var result = data.split(',');
                        
            if( result[0] == 'ng' ){
                alert(result[1]);
                return;
            }
            else if( result[0] == 'ok' ){
                // video url add successfully. display list.
                var div = document.getElementById('outputwrap');
                var p = document.getElementById(itemid);
                
                if(  p != undefined ){
                    div.removeChild(p);
                }
                
                return;
            }
            else {
                // unknown error.
                alert('Unknown error: ' + result[0] + ':' + result[1]);
                return;
            }
        }
    );
}

/* handle remove image */
function removeImageHandler(url,divid){
    // validate.
    if( url == undefined || url == null ){
        console.log('URL is undefined.');
        return false;
    }
    
    var d = document.getElementById(divid);
    
    if( d == undefined ){
        console.log('Image divider is undefined.');
        return false;
    }
    
    // send ajax request.
    $.post(
        '../ajax/remove_car_image.php',
        {
            loc: url
        },
        function(data,status){
            var result = data.split(',');
            
            if( result[0] == 'ng' ){
                alert('Unable to remove image due an internal error.');
                console.log(result[0] + ':' + result[1]);
                return false;
            }
            else if( result[0] == 'ok' ){
                // remove image.
                var wrap = document.getElementById('outoutimg');
                wrap.removeChild(d);
                return false;
            }
            else {
                // unknown error.
                alert('Unknown error: ' + result[0] + ':' + result[1]);
                return;
            }
        }
    );
}