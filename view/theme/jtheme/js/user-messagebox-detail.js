
$(document).ready(function($) {
	var control = $("#upload_attachment");
	var allowedExts = ['xls', 'doc', 'docx', 'ppt', 'pptx', 'pps', 'jpg', 'gif', 'bmp', 'zip', 'pdf', 'png', 'hwp', 'txt'];
	var validFileNames=true;

	$("#templateSelect").change(function(e){
		$("#messageInput").val($(this).val());
	});
	
    $("#upload_attachment").change(function(e){
    	//checkFile();
    	var fileElement=document.getElementById('upload_attachment');
    	var fileNameArray=new Array();
    	var fileSizeArray=new Array();
    	var totalSize = 0;

    	for (var x = 0; x < fileElement.files.length; x++) {
			//add to list	
			var inputfilename= fileElement.files[x].name;
			var inputfilesize= fileElement.files[x].size;
			fileNameArray.push(inputfilename);
			fileSizeArray.push(inputfilesize);
			totalSize += inputfilesize << 0;
			checkFile(inputfilename);
		}
		
		if(validFileNames==false || checkFileSize(totalSize)==false){
			clearInputFile();
		}
		//alert(fileNameArray);
    });
 
	function checkFile(filename) {
        //var fileElement = document.getElementById("upload_attachment");
        var fileExtension = "";
        if (filename.lastIndexOf(".") > 0) {
            fileExtension = filename.substring(filename.lastIndexOf(".") + 1, filename.length);
        }
        
        if (jQuery.inArray(fileExtension.toLowerCase(), allowedExts)>-1) {
            validFileNames = true;
            return true;
        }
        else {
            alert("Sorry, your selected file type is not allowed.");
            validFileNames = false;
            clearInputFile();
            return false;
        }
    }
    function checkFileSize(filesize){
    	/* Allow 10 MB */
    	if(filesize > (1024 * 1024 * 10)){
    		alert("Sorry, your selected file size is exceed the limit.");
    		return false;
    	}else{
    		return true;
    	}
    }

    function clearInputFile(){
    	control.val('');
    }
});