/* handle search form controls */
var current_dir;

function processSearchForm(){
    var orderby = document.getElementById('orderInput');
    var ordertype = document.getElementById('orderTypeInput');
    var resultsperpage = document.getElementById('resultPerPageInput');
    var page = document.getElementById('pageInput');
    var maker = document.getElementById('makerInput');
    var body = document.getElementById('bodyInput');
    var query = document.getElementById('queryInput');
    
    // validate
    if( orderby == undefined || ordertype == undefined || resultsperpage == undefined || page == undefined ){
        console.log('There are undefined form data. Form not processed to avoid errors.');
        return false;
    }
    
    var ob = orderby.options[orderby.selectedIndex].value;
    var ot = ordertype.options[ordertype.selectedIndex].value;
    var rp = resultsperpage.options[resultsperpage.selectedIndex].value;
    var pg = page.options[page.selectedIndex].value;
    var result = '';
    
    // reset previously declared attr.
    if( maker != undefined && maker.value.length > 0 ){
        result += 'maker=' + maker.value + '&';
    }
    if( body != undefined && body.value.length > 0 ){
        result += 'body=' + body.value + '&';
    }
    if( query != undefined && query.value.length > 0 ){
        result += 'q=' + query.value + '&';
    }
    
    if( ob.length < 1 && ot.length < 1 && rp.length < 1 && pg.length < 1 ){
        // there are no values set to any of the filters.
        console.log('No filter has a value. Form not processed.');
        return false;
    }
    
    if( ob.length > 0 ){
        result += "o=" + ob + '&';
    }
    if( ot.length > 0 ){
        result += 'ot=' + ot + '&';
    }
    if( rp.length > 0 ){
        result += 'r=' + rp + '&';
    }
    if( pg.length > 0 ){
        result += 'pg=' + pg + '&';
    }
    
    result = result.substring(0, result.length - 1);
    
    // send data to page.
    window.location = '?' + result;
    
    return false;
}

/* display thumb as main image */
function displayMainImage(main,url,anchor){
    var main = document.getElementById(main);
    var anchor = document.getElementById(anchor);
    
    if( main != undefined ){
        main.src = url;
    }
    if( anchor != undefined ){
        anchor.href = url;
    }
}

/* [2013.07.27] change listing grid handler */
var selected_grid_type;

// mouse click handler.
function displayGrid(gtype, stype){
    // if selected type is the same as the requested type, ignore request.
    if( selected_grid_type != undefined && selected_grid_type == gtype ){
        return;
    }

    // send each request to specific method to make it easier for develp.
    if( gtype == 'list' ){
        doGridList();
        selected_grid_type = 'list';
        return;
    } else {
        selected_grid_type = 'thumb';
        doGridThumb();
    }
}

function doGridThumb(){        
    // remove eco icon.
    var eco = document.getElementsByTagName('img'), i;

    for( i in eco ){
        if( eco[i].className == 'ecoIcon' ){
            eco[i].style.display = 'none';
        }
    }
            
    // hide unnecessary elements for the grid.
    var elems = document.getElementsByTagName('div'), i;
    
    for( i in elems ){
        if( elems[i].className == 'car-item-wrapper' ){
            // redefine width.
            elems[i].style.width = "315px";
            elems[i].style.float = "left";
            elems[i].style.margin = "13px";
            
             // hide header.
            //elems[i].getElementsByTagName('h4')[0].style.display = 'none';
            elems[i].getElementsByTagName('h4')[0].style.width = '320px';
            elems[i].getElementsByTagName('h4')[0].style.height = '20px';
            elems[i].getElementsByTagName('h4')[0].style.overflow = 'hidden';
            elems[i].getElementsByTagName('h4')[0].style.marginTop = '225px';
            elems[i].getElementsByTagName('h4')[0].className = 'clearfix';
            
            // hide thumbs.
            elems[i].getElementsByClassName('item-gallery')[0].style.display = 'none';
            
            // hide features.
            elems[i].getElementsByClassName('features-wrapper')[0].style.display = 'none';
            
            // re-format price.
            var price = elems[i].getElementsByClassName('handle-wrapper');
            price[0].style.width = '320px';
            price = elems[i].getElementsByTagName('div');
            price[8].getElementsByTagName('span')[0].style.display = 'none';
            price[8].getElementsByTagName('span')[0].style.width = 'auto';
            
            // reformat toolbar.
            var tool = elems[i].getElementsByClassName('toolbar-wrapper');
                        
            // hide favorites and reserve btn
            tool = elems[i].getElementsByTagName('a');
            
            for( u in tool ){
                if( tool[u].className == 'btn btn-small btn-warning' ){
                    tool[u].style.display = 'none';
                }
                
                if( tool[u].className == 'btn btn-small btn-info' ){
                    tool[u].style.display = 'none';
                }
            }
        }
    }
    
    // update session.
    updateGridSession('thumb');
}
function doGridList(){
    // restore eco icon.
    var eco = document.getElementsByTagName('img'), i;

    for( i in eco ){
        if( eco[i].className == 'ecoIcon' ){
            eco[i].style.display = '';
        }
    }
            
    // hide unnecessary elements for the grid.
    var elems = document.getElementsByTagName('div'), i;
    
    for( i in elems ){
        if( elems[i].className == 'car-item-wrapper' ){
            // restore width.
            elems[i].style.width = "708px";
            elems[i].style.float = "none";
            elems[i].style.margin = "0";
            elems[i].style.marginBottom = "15px";
            
             // hide header.
            elems[i].getElementsByTagName('h4')[0].style.display = '';
            elems[i].getElementsByTagName('h4')[0].style.width = 'auto';
            elems[i].getElementsByTagName('h4')[0].style.height = 'auto';
            elems[i].getElementsByTagName('h4')[0].style.overflow = 'none';
            elems[i].getElementsByTagName('h4')[0].style.marginTop = '0';
            elems[i].getElementsByTagName('h4')[0].className = '';
            
            // hide thumbs.
            elems[i].getElementsByClassName('item-gallery')[0].style.display = '';
            
            // hide features.
            elems[i].getElementsByClassName('features-wrapper')[0].style.display = '';
            
            // re-format price.
            var price = elems[i].getElementsByClassName('handle-wrapper');
            price[0].style.width = 'auto';
            price = elems[i].getElementsByTagName('div');
            price[8].getElementsByTagName('span')[0].style.display = '';
            price[8].getElementsByTagName('span')[0].style.width = 'auto';
            
            // reformat toolbar.
            var tool = elems[i].getElementsByClassName('toolbar-wrapper');
                        
            // hide favorites and reserve btn
            tool = elems[i].getElementsByTagName('a');
            
            for( u in tool ){
                if( tool[u].className == 'btn btn-small btn-warning' ){
                    tool[u].style.display = '';
                }
                
                if( tool[u].className == 'btn btn-small btn-info' ){
                    tool[u].style.display = '';
                }
            }
        }
    }
    
    // update session.
    updateGridSession('list');
}
function updateGridSession(stype){
    $.ajax({
        type: "POST",
        url: current_dir + "ajax/update_grid_status.php",
        dataType: "text",
        data: {sess: stype},
        success: function(msg){
            console.log(msg)
        }
    });
}

