var message_form;
$(document).ready(function($) {
	var jsonMessages=[];
	var type='inbox';
	var offset=0;
	loadAllMessages(type);
	
	$(document.body).on("change","#all_msg_check", function(e){
		$(".msg_check").prop('checked', $(this).prop('checked'));
	});
	/* Event for click any page number */
	$(document.body).on("click",".pagination a", function(e){
		loadAllMessages(type, $(this).attr('offset'));

		return false;
	});
	/* Event for button delete selected */
	$(document.body).on("click", "#delete_selected", function(e){
		if(!confirm("Are you sure want to delete the selected messages?"))
			return;
		message_form=$("#message_form").serializeArray();
		$.ajax({
            type:"post",
            url: "ajax/messages/user-ajax-messages.php?type="+type+"&s="+offset,
            data: $.param(message_form),
            success: function(data){
            //do stuff after the AJAX calls successfully completes
            	$("#message_container").html(data);
            }
        });
	});


	/* Top 3 Tab click event */
	$("#tab_inbox").click(function(e){
		type="inbox";
		loadAllMessages(type);
	});
	$("#tab_sent").click(function(e){
		type="sent";
		loadAllMessages(type);
	});
	$("#tab_trash").click(function(e){
		type="trash";
		loadAllMessages(type);
	});

	function loadAllMessages(type, start){
		if(typeof start == 'undefined') start=0;
		$("#message_container").load("ajax/messages/user-ajax-messages.php?type="+type+"&s="+start,function(e){
			offset=start;
		});
	}
});