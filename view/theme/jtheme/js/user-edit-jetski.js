/* handle next form (btn click) */
var cur_step = 1;
var cur_img;
var tab;
var next_btn;
var prev_btn;
var img_st1;
var currentMakeClick="";
var fittingModel = new Array();
var fittingModelBlock="";
Array.prototype.remove = function() {
					var what, a = arguments, L = a.length, ax;
					while (L && this.length) {
						what = a[--L];
						while ((ax = this.indexOf(what)) !== -1) {
							this.splice(ax, 1);
						}
					}
					return this;
				};
$(document).ready(function(){
    
	$( ".accordion" ).accordion({
        collapsible: true,
        heightStyle: "content",
        active: false
    });
    tab = document.getElementById('content1');
    //next_btn = document.getElementById('nextbtn');
//    prev_btn = document.getElementById('prevbtn');
    //img_st1 = document.getElementById('step1');
    
    opt1 = document.getElementById('opt1');
    opt2 = document.getElementById('opt2');
    vid  = document.getElementById('video-wrap');
    upl  = document.getElementById('upload-wrap');
    
    cur_img = img_st1;
	$("#sellerCommentText").ckeditor();
	$( "#vesselTypeInputSelect" ).change(function(e) {
        if($(this).val()=='Others')
            $("#vesselTypeInput").css('display','inline');
        else{
            $("#vesselTypeInput").css('display','none');
            $("#vesselTypeInput").val('');
        }
    });
	$( "#makerInputSelect" ).change(function(e) {
		// if($(this).val()=='Others')
		// 	$("#makerInput").css('display','inline');
		// else{
		// 	$("#makerInput").css('display','none');
		// 	$("#makerInput").val('');
            
		// }
		var mid=$(this).val();
		
		$("#modelInputSelect").load('ajax/load_saved_model.php?product_type=Jet+Ski&mid='+mid, function() {
			// if($(this).val()=='Others')
			// 	$("#modelInput").css('display','inline');
			// else{
			// 	$("#modelInput").css('display','none');
			// 	$("#modelInput").val('');
			// }
		});
    });

	$("#modelInputSelect" ).change(function(e) {
		// if($(this).val()=='Others')
		// 	$("#modelInput").css('display','inline');
		// else{
		// 	$("#modelInput").css('display','none');
		// 	$("#modelInput").val('');
		// }
	});
	loadAllDropdown();
	
	//setDropdownSelection();
	
});




function loadAllDropdown(){
	
	var cname=encodeURIComponent($("#categorySelect").val());
	//var foundInMakerSelect=false;
	var ptype='Part';
	var scgroup='';

	if(condition!='') $("[name=conditionInput]").val([condition]);
	
	if(flight_rule!='') $("#flightRuleInputSelect").val(flight_rule);
    if(built_year!='') $("#builtYearInputSelect").val(built_year);
     if(made_in!='') $("#madeInInputSelect").val(made_in);
    
    if(vessel_type!=''){
        $("#vesselTypeInputSelect").val(vessel_type);
        if(vessel_type=='Others'){
            $("#vesselTypeInput").css('display','inline');
        }
        else{
            $("#vesselTypeInput").css('display','none');
            $("#vesselTypeInput").val('');
        }
    };
	$("#countryInputSelect").val(country);
	if(year!=''){
		$("#yearInput").val(year);
	}

	if(make!=''){
		$("#makerInputSelect").val(make);
		// if(make=='Others'){
		// 	$("#makerInput").css('display','inline');
		// }
		// else{
		// 	$("#makerInput").css('display','none');
		// 	$("#makerInput").val('');
		// }
		var mid=$("#makerInputSelect").val();
		$("#modelInputSelect").load('ajax/load_saved_model.php?product_type=Jet+Ski&mid='+ encodeURIComponent(mid), function() {
			if(model!=''){
				$(this).val(model);
			}
			// if($(this).val()=='Others')
			// 	$("#modelInput").css('display','inline');
			// else{
			// 	$("#modelInput").css('display','none');
			// 	$("#modelInput").val('');
			// }
			
		});
	}

	
}


/* redefine wysiwyg */


/* handle step4 selectors */
var opt1;
var opt2;
var vid;
var upl;
function validateImageType(){
    if( opt1 == undefined || opt2 == undefined || vid == undefined || upl == undefined ){
        opt1 = document.getElementById('opt1');
        opt2 = document.getElementById('opt2');
        vid  = document.getElementById('video-wrap');
        upl  = document.getElementById('upload-wrap');
    }
    
    var val = opt1.options[opt1.selectedIndex].value;
    
    // disable and hide all controllers.
    if( val == 'none' ){
        opt2.disabled = true;
        opt2.selectedIndex = 0;
        vid.style.display = 'none';
        upl.style.display = 'none';
    }
    
    // display video controller.
    else if( val == 'video' ){
        opt2.disabled = true;
        opt2.selectedIndex = 0;
        vid.style.display = 'inline-block';
        upl.style.display = 'none';
    }
    
    // display option 2 but hide controllers.
    else {
        opt2.disabled = false;
        vid.style.display = 'none';
        upl.style.display = 'none';
    }
}

function displayController(url){
    var val = "interior";
    
        vid.style.display = 'none';
        upl.style.display = 'inline-block';
   
    document.getElementById('iupload').src = url + '&mode=' + val;
    
}

/* handle save video url form */
function saveVideoHandler(cid){
    // car id must be defined.
    if( cid == undefined || cid.length < 5 ){
        return false;
    }
    
    var value = document.getElementById('videoUrlInput').value;
    
    if( value == undefined || value.length < 1 ){
        return false;
    }
    
    // disable save button.
    document.getElementById('vidBtn').disabled = true;
    
    // send ajax request.
    $.post(
        'ajax/save_video_url.php',
        {
            carid: cid,
            url: value
        },
        function(data,status){
            var result = data.split(',');
            
            document.getElementById('videoUrlInput').value = ''; // enpty video url input field.
            document.getElementById('vidBtn').disabled = false;   // enable button.
            
            if( result[0] == 'ir' ){
                alert(result[1]);
                return;
            }
            else if( result[0] == 'ok' ){
                // video url add successfully. display list.
                var div = document.getElementById('outputwrap');
                var itemid = 'link' + Math.floor((Math.random()*1000)+1);
                div.innerHTML += '<p id="'+itemid+'"><b>Video URL:</b> ' + result[1] + '<a href="#" onclick="removeVideo(\''+result[1]+'\',\''+cid+'\',\''+itemid+'\'); return false;" class="btn btn-small btn-danger" style="float:right;">×</a></p>';
                return;
            }
            else {
                // unknown error.
                alert('Unknown error: ' + result[0] + ':' + result[1]);
                return;
            }
        }
    );
}

/* handle upload image form */
function uploadImageHandler(cid){
    // car id must be defined.
    if( cid == undefined || cid.length < 5 ){
        return false;
    }
    
    
}

/* remove video link */
function removeVideo(url,carid,itemid){
    if( url == undefined || carid == undefined || itemid == undefined ){
        return false;
    }
    
    $.post(
        'ajax/remove_vid_url.php',
        {
            cid: carid,
            dest: url
        },
        function(data,status){
            var result = data.split(',');
                        
            if( result[0] == 'ng' ){
                alert(result[1]);
                return;
            }
            else if( result[0] == 'ok' ){
                // video url add successfully. display list.
                var div = document.getElementById('outputwrap');
                var p = document.getElementById(itemid);
                
                if(  p != undefined ){
                    div.removeChild(p);
                }
                
                return;
            }
            else {
                // unknown error.
                alert('Unknown error: ' + result[0] + ':' + result[1]);
                return;
            }
        }
    );
}

/* handle remove image */
function removeImageHandler(url,divid){
    // validate.
    if( url == undefined || url == null ){
        console.log('URL is undefined.');
        return false;
    }
    
    var d = document.getElementById(divid);
    
    if( d == undefined ){
        console.log('Image divider is undefined.');
        return false;
    }
    
    // send ajax request.
    $.post(
        'ajax/remove_car_image.php',
        {
            loc: url
        },
        function(data,status){
            var result = data.split(',');
            
            if( result[0] == 'ng' ){
                alert('Unable to remove image due an internal error.');
                console.log(result[0] + ':' + result[1]);
                return false;
            }
            else if( result[0] == 'ok' ){
                // remove image.
                var wrap = document.getElementById('outoutimg');
                wrap.removeChild(d);
                return false;
            }
            else {
                // unknown error.
                alert('Unknown error: ' + result[0] + ':' + result[1]);
                return;
            }
        }
    );
}