function displayTab(tab,btn){
    var t = document.getElementById(tab);
    var b = document.getElementById(btn);
    
    if( t == undefined || b == undefined ){
        return;
    }
    
    if( t.style.display == '' || t.style.display == 'none' ){
        $(t).slideDown();
        b.className = 'icon-chevron-up';
    }
    else {
        $(t).slideUp();
        b.className = 'icon-chevron-down';
    }
}