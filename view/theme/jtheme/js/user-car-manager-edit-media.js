/**
 * Handle remove media.
 */
function removeMedia(iid,msg,tag){
    var c = confirm(msg);
    var t = document.getElementById(tag);
    
    if( !c ){
        return false;
    }
    
    if( t == undefined || t == null ){
        console.log('Invalid tag data declaration.')
        return false;
    }
    
    // handle remove media by id.
    if( iid == undefined || iid < 1 ){
        return false;
    }
    
    $.post(
        '../ajax/remove_media_by_id.php',
        {
            mid: iid
        },
        function(data,status){
            if( status == 'success' ){
                var res = data.split(',');
                
                if( res[0] == 'ok' ){
                    // hide media from html.
                    $(t).fadeOut();
                }
            }
        }
    );
}

/**
 * Handle uploader type selection
 */
function displayTypeTab(){
    var type = document.getElementById('typeSelector');
    var inner = document.getElementById('innerSelector');
    var upload = document.getElementById('imageUpload');
    var video = document.getElementById('videoUpload');
    
    if( type == undefined || inner == undefined || upload == undefined || video == undefined ){
        return false;
    }
    
    // display as video
    if(type.options[type.selectedIndex].value == 'video'){
        $(upload).fadeOut();
        $(video).fadeIn();
        $(inner).fadeOut();
    }
    
    // display as image
    else {
        $(upload).fadeIn();
        $(inner).fadeIn();
        $(video).fadeOut();
    }
}