
var selectedCountryLoading="";
var selectedCountryDest="";
var selectedVesselType="";
$(document).ready(function(){

	/**** + DEFAULT PORT SELECT AFTER PAGE LOADED ****/
    //selectedCountryLoading = $("#countryLoadingSelect").val();
	//$("#portLoadingSelect").load("../ajax/load_port.php?country="+selectedCountryLoading);
	// var countryLoading="<?php if(isset($shippingInfo['country_loading'])) echo $shippingInfo['country_loading'];?>";
 //    var countryDest="<?php if(isset($shippingInfo['country_dest'])) echo $shippingInfo['country_dest'];?>";
 //    var portLoading="<?php if(isset($shippingInfo['port_loading'])) echo $shippingInfo['port_loading'];?>";
 //    var portDest="<?php if(isset($shippingInfo['port_dest'])) echo $shippingInfo['port_dest'];?>";
 //    var vesselType="<?php if(isset($shippingInfo['vessel_type'])) echo $shippingInfo['vessel_type'];?>";
 //    var size="<?php if(isset($shippingInfo['size'])) echo $shippingInfo['size'];?>";
 //    var transitTime="<?php if(isset($shippingInfo['transit_time'])) echo $shippingInfo['transit_time'];?>";
 //    var line="<?php if(isset($shippingInfo['line'])) echo $shippingInfo['line'];?>";

    if(countryLoading!=""){ 
    	$("#countryLoadingSelect").val(countryLoading);
    	$("#portLoadingSelect").load("../ajax/load_port.php?country="+countryLoading, function(e){
    		if(portLoading!=""){
    			$(this).val(portLoading);
    		}
    	});
    }
    if(countryDest!=""){
    	$("#countryDestSelect").val(countryDest);
    	$("#portDestSelect").load("../ajax/load_port.php?country="+countryDest, function(e){
    		if(portDest!=""){
    			$(this).val(portDest);
    		}
    	});
    }
    if(vesselType!=""){
    	$("#vesselTypeSelect").val(vesselType);
    	$("#sizeSelect").load("../ajax/load_size.php?vessel_type="+vesselType, function(e){
    		if(size!=""){
    			$(this).val(size);
    		}
    	});
    } 
    if(transitTime!="") $("#transitTimeSelect").val(transitTime);
    if(line!="") $("#lineSelect").val(line);

	/**** - DEFAULT PORT SELECT AFTER PAGE LOADED ****/

	/**** + WHEN USER CLICK ON ANY COUNTRY IT LOADS PORT LIST OF THAT COUNTRY ****/
	$("#countryLoadingSelect").change(function(e) {
		selectedCountryLoading = $("#countryLoadingSelect").val();
		$("#portLoadingInput").css('display','none');
		$("#portLoadingInput").val('');
		$("#portLoadingSelect").load("../ajax/load_port.php?country="+selectedCountryLoading);
	    
    });

    $("#countryDestSelect").change(function(e) {
		selectedCountryDest = $("#countryDestSelect").val();
		$("#portDestInput").css('display','none');
		$("#portDestInput").val('');
		$("#portDestSelect").load("../ajax/load_port.php?country="+selectedCountryDest);
	     
    });

    $("#vesselTypeSelect").change(function(e) {
		selectedVesselType = $("#vesselTypeSelect").val();
		$("#sizeInput").css('display','none');
		$("#sizeInput").val('');
		$("#sizeSelect").load("../ajax/load_size.php?vessel_type="+selectedVesselType);
	    
    });
    
    /**** - WHEN USER CLICK ON ANY COUNTRY IT LOADS PORT LIST OF THAT COUNTRY ****/

    /**** + WHEN USER SELECT OTHER IT SHOW UP INPUT TEXT ****/
	$("#portLoadingSelect, #portDestSelect, #vesselTypeSelect, #sizeSelect, #lineSelect").change(function(e) {
    	if($(this).find("option:selected").text()=='Others'||$(this).find("option:selected").text()=='Other')
	         $(this).nextAll("input").css('display','inline');
	    else{
	          $(this).nextAll("input").css('display','none');
	          $(this).nextAll("input").val('');
	    }
    });
    /**** - WHEN USER SELECT OTHER IT SHOW UP INPUT TEXT ****/
	/**** + CHECK IF USER SELECT ALL REQUIRED FIELD ****/
	$("#submitbtn").click(function(e){
		var valid=true;
		var msg="";
		if($("#portLoadingSelect").val()=='' && $("#portLoadingInput").val()==''){
			$("#portLoadingInput").css("border","1px solid #FC9D99");
			$("#portLoadingInput").css("box-shadow","0 0 1px 1px #F8C1BF");
			msg+="Please define port of loading of the country!";
			$("#portLoadingInput").focus();
			valid= false;
		}else{
			$("#portLoadingInput").css("border","");
			$("#portLoadingInput").css("box-shadow","")
			
		}
		if($("#portDestSelect").val()=='' && $("#portDestInput").val()==''){
			
			$("#portDestInput").css("border","1px solid #FC9D99");
			$("#portDestInput").css("box-shadow","0 0 1px 1px #F8C1BF");
			msg+="\nPlease define port of destination of the country!";
			$("#portDestInput").focus();
			valid= false;
		}else{
			$("#portDestInput").css("border","");
			$("#portDestInput").css("box-shadow","");
		}
		if(valid==false){
			alert(msg);
			return false;
		}
	});
	/**** - CHECK IF USER SELECT ALL REQUIRED FIELD ****/
});
	




