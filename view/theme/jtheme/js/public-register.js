/* 
 * Validate register form ------------------------------------------------------
 */
 
function capLock(e){
 kc = e.keyCode?e.keyCode:e.which;
 sk = e.shiftKey?e.shiftKey:((kc == 16)?true:false);
 if(((kc >= 65 && kc <= 90) && !sk)||((kc >= 97 && kc <= 122) && sk))
  document.getElementById('divPass').style.visibility = 'visible';
 else
  document.getElementById('divPass').style.visibility = 'hidden';
}
function capLock1(e){
 kc = e.keyCode?e.keyCode:e.which;
 sk = e.shiftKey?e.shiftKey:((kc == 16)?true:false);
 if(((kc >= 65 && kc <= 90) && !sk)||((kc >= 97 && kc <= 122) && sk))
  document.getElementById('divPass1').style.visibility = 'visible';
 else
  document.getElementById('divPass1').style.visibility = 'hidden';
}
function validateRegisterForm()
{
	/* input initialize
	---------------------------- */
    var result = true;
	
	//CheckBox
	/* member type 1 */
	var member_type_1 = new Array();
	var member_type_1_checked = 0;
	for( var i = 0; i < 3; i++ ) {
		member_type_1[i] = document.getElementById('member_type_1_'+i);
		member_type_1_checked+=member_type_1[i].checked;
	}
	/* member type 2 */
	var member_type_2 = new Array();
	var member_type_2_checked = 0;
	for( var i = 0; i < 2; i++ ) {
		member_type_2[i] = document.getElementById('member_type_2_'+i);
		member_type_2_checked+=member_type_2[i].checked;
	}
	/* business type */
	var business_type = new Array();
	var business_type_checked = 0;
	for( var i = 0; i < 2; i++ ) {
		business_type[i] = document.getElementById('business_type_'+i);
		business_type_checked+=business_type[i].checked;
	}
	/* business field */
	var business_field = new Array();
	var business_field_checked = 0;
	for( var i = 0; i < 10; i++ ) {
		business_field[i] = document.getElementById('business_field_'+i);
		business_field_checked+=business_field[i].checked;
	}

	// TextBox
	var id = document.getElementById('id');
	var password = document.getElementById('password');
	var confirm_password = document.getElementById('confirm_password');
	var email = document.getElementById('email');
	var confirm_email = document.getElementById('confirm_email');
	var name = document.getElementById('name');
	var card_id = document.getElementById('card_id');
	var country = document.getElementById('country');
	var address = document.getElementById('address');
	var tel = document.getElementById('tel');
	var mobile = document.getElementById('mobile');
    var sf = document.getElementById('inputsecurity');
    var rf = document.getElementById('inputres');
    
    // reset all displayed errors.
    resetErrors();
    
    // validate fields.
    if( !member_type_1_checked == 1 ) {
		result = false;
		document.getElementById('member_type_1_error').style.display = 'inline-block';
    }
    if( !member_type_2_checked == 1 ) {
		result = false;
		document.getElementById('member_type_2_error').style.display = 'inline-block';
    }
    if( !business_type_checked == 1 ) {
		result = false;
		document.getElementById('business_type_error').style.display = 'inline-block';
    }
    if( !business_field_checked == 1 ) {
		result = false;
		document.getElementById('business_field_error').style.display = 'inline-block';
    }
	if( id.value.length < 1 ) {
		result = false;
		id.style.color = '#F00';
		document.getElementById('id_error').style.display = 'inline-block';
	} else {
		if( id.value.length < 4 || id.value.length > 12  ) {
			result = false;
			id.style.color = '#F00';
			document.getElementById('id_length_error').style.display = 'inline-block';
		}
	}
	if( password.value.length < 5 || password.value.length > 20  ) {
		result = false;
		password.style.color = '#F00';
		document.getElementById('password_error').style.display = 'inline-block';
	} else {
		if( password.value != confirm_password.value  ) {
			result = false;
			password.style.color = '#F00';
			confirm_password.style.color = '#F00';
			document.getElementById('confirm_password_error').style.display = 'inline-block';
		} else {
		
		}
	}
	if( email.value.length < 1  ) {
		result = false;
		email.style.color = '#F00';
		document.getElementById('email_error').style.display = 'inline-block';
	} else {
		if( email.value != confirm_email.value  ) {
			result = false;
			email.style.color = '#F00';
			confirm_email.style.color = '#F00';
			document.getElementById('confirm_email_error').style.display = 'inline-block';
		} if( !IsEmail(email.value) ) {
			result = false;
			email.style.color = '#F00';
			document.getElementById('invalid_email_error').style.display = 'inline-block';
		}
	}
    if( name.value.length < 1 ) {
		result = false;
		name.style.color = '#F00';
		document.getElementById('name_error').style.display = 'inline-block';
    } else {
		if( !latinName(name.value) ) {
			result = false;
			name.style.color = '#F00';
			document.getElementById('invalid_name_error').style.display = 'inline-block';
		}
	}
    if( card_id.value.length < 1 ) {
		result = false;
		card_id.style.color = '#F00';
		document.getElementById('card_id_error').style.display = 'inline-block';
    }
    if( country.value.length < 1 ) {
		result = false;
		country.style.color = '#F00';
		document.getElementById('country_error').style.display = 'inline-block';
    }
	if( address.value.length < 1 ) {
		result = false;
		address.style.color = '#F00';
		document.getElementById('address_error').style.display = 'inline-block';
    }
    if( tel.value.length < 1 ) {
		result = false;
		tel.style.color = '#F00';
		document.getElementById('tel_error').style.display = 'inline-block';
    } else {
		if( !IsNumber(tel.value) ) {
			result = false;
			tel.style.color = '#F00';
			document.getElementById('invalid_tel_error').style.display = 'inline-block';
		}
	}
    if( mobile.value.length < 1 ) {
		result = false;
		mobile.style.color = '#F00';
		document.getElementById('mobile_error').style.display = 'inline-block';
    } else {
		if( !IsNumber(mobile.value) ) {
			result = false;
			mobile.style.color = '#F00';
			document.getElementById('invalid_mobile_error').style.display = 'inline-block';
		}
	}
    if( sf.value != rf.value ) {
		result = false;
		sf.style.color = '#F00';
		document.getElementById('fsecurity').style.display = 'inline-block';
    }
        
    return result;
}


function cancel() {
	document.getElementById('file').value="";
	$('#blah').attr('src', '../images/register/profile-pic.jpg').width(161);
}

function upload() {
	document.getElementById('file').click();
}
function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#blah').attr('src', e.target.result).width(160);
		};
		reader.readAsDataURL(input.files[0]);
	}
}
function IsEmail(email) 
{
	var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	return regex.test(email);
}
function latinName(name) {
	var alpha = /^[a-zA-Z-,]+(\s{0,1}[a-zA-Z-, ])*$/  
	return name.match(alpha);
}
function IsNumber(number) {
	var regex = /^[0-9]*$/;
	return regex.test(number);
}
function resetErrors()
{
    // hide messages.
	document.getElementById('member_type_1_error').style.display = 'none';
	document.getElementById('member_type_2_error').style.display = 'none';
	document.getElementById('business_type_error').style.display = 'none';
	document.getElementById('business_field_error').style.display = 'none';
	document.getElementById('id_error').style.display = 'none';
	document.getElementById('id_length_error').style.display = 'none';
	document.getElementById('id_exist_error').style.display = 'none';
	document.getElementById('password_error').style.display = 'none';
	document.getElementById('confirm_password_error').style.display = 'none';
	document.getElementById('email_error').style.display = 'none';
	document.getElementById('confirm_email_error').style.display = 'none';
	document.getElementById('invalid_email_error').style.display = 'none';
	document.getElementById('name_error').style.display = 'none';
	document.getElementById('invalid_name_error').style.display = 'none';
	document.getElementById('card_id_error').style.display = 'none';
	document.getElementById('country_error').style.display = 'none';
	document.getElementById('address_error').style.display = 'none';
	document.getElementById('tel_error').style.display = 'none';
	document.getElementById('invalid_tel_error').style.display = 'none';
	document.getElementById('mobile_error').style.display = 'none';
	document.getElementById('invalid_mobile_error').style.display = 'none';
    document.getElementById('fsecurity').style.display = 'none';
    
    // reset font color.
	document.getElementById('id').style.color = '#333';
	document.getElementById('password').style.color = '#333';
	document.getElementById('confirm_password').style.color = '#333';
	document.getElementById('email').style.color = '#333';
	document.getElementById('confirm_email').style.color = '#333';
	document.getElementById('name').style.color = '#333';
	document.getElementById('card_id').style.color = '#333';
	document.getElementById('country').style.color = '#333';
	document.getElementById('tel').style.color = '#333';
	document.getElementById('mobile').style.color = '#333';
    document.getElementById('inputsecurity').style.color = '#333';
}


/* 
 * Ajax check register form ------------------------------------------------------
 */
$(document).ready(function() {
  
	//the min chars for vehicle_id  
	var min_chars = 4; 
	var max_chars = 12;

	//result texts  
	var characters_error = 'Minimum amount of chars is 3';  
	var checking_html = 'Checking...';

	//when textbox is inputed  
	// $('#id').keyup(function(){
	// 	$('#vehicle_availability_result').html(checking_html);
	// 	check_availability();
	// });

});

//$(document).ready(function(e) {
//     $('#country').change(function(){
		//$('#mobile').val("(+"+$('#country option:selected').attr('label').valueOf()+")");
							
	//});
//}); 
$(document).ready(function(e) {
		$('.phonecode').text("+"+$('#country option:selected').attr('for').valueOf());
        $('#country').change(function(){
		
		$('.phonecode').text("+"+$('#country option:selected').attr('for').valueOf());
		
		$('#mobile').on("keyup",function () {
	  		var phone=$(this).val();
			$('#phonecode').val("+"+$('#country option:selected').attr('for').valueOf()+''+phone);
		});	
						
	});
	
    $('#mobile').keyup(function(){
		var value=$(this).val();
		value=value.replace(/^(0*)/,"");
		$(this).val(value);
		});
		
	//$('#mobile').bind('keypress', function (e) {
       // return !(e.which != 8 && e.which != 0 &&
                //(e.which < 48 || e.which > 57) && e.which != 46);
    //});
	
	
    $("#mobile").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode,[45,46, 8, 9, 27, 13, 110,190,189]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 48 && e.ctrlKey === true) || 
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)){
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

	
	$("#com_name").on("keyup",function(){
			var company=$(this).val();
			$("#reg_name").val(company);
		});	
	
});

  
//function to check username availability  
// function check_availability(){

// 	var base_relative = $('#base_relative').text();

// 	//get the username  
// 	var vehicle_id = $('#id').val();

// 	//use ajax to run the check  
// 	$.post("" + base_relative + "view/theme/jtheme/js/register_chek_available.php", { vehicle_id: vehicle_id },
// 		function(result){
// 			//if the result is 1  
// 			if(result == 1){
// 				//show that the vehicle id is available
// 				$('#id').css({'color': '#333'});
// 				$('#vehicle_availability_result').css({'display': 'none'}).html(vehicle_id + ' is Available');  
// 			}else{
// 				//show that the vehicle id is NOT available
// 				$('#id').css({'color': '#f00'});
// 				$('#vehicle_availability_result').css({'display': 'block'}).html(vehicle_id + ' is not Available');  
// 			}
// 		}
// 	);

// }