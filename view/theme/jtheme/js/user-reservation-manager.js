function displayReservationTab(wrap,icon){
    var w = document.getElementById(wrap);
    var i = document.getElementById(icon);
    
    if( w == undefined || i == undefined ){
        return;
    }
    
    // display tab.
    if( w.style.display == '' || w.style.display == 'none' ){
        $(w).slideDown();
        i.className = 'icon-chevron-up';
    }
    // hide tab.
    else {
        $(w).slideUp();
        i.className = 'icon-chevron-down';
    }
    
    return false;
}