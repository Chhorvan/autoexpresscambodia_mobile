/* handle toggle tab */
var disp;

function displayManagerTab(tab,icon){
    var t = document.getElementById(tab);
    var i = document.getElementById(icon);
    
    if( t == undefined || i == undefined ){
        console.log('request to a null component.');
        return false;
    }
    
    disp = t.style.display;
    
    if( disp == 'none' ){
        $(t).slideDown();
        i.className = 'icon-chevron-up';
    }
    else {
        $(t).slideUp();
        i.className = 'icon-chevron-down';
    }
    
    return false;
}

function loadPage(){
    var url = document.getElementById('pageInput');
    url = url.options[url.selectedIndex].value;
    
    window.location = '?pg=' + url;
}