<?php
/**
 * JTHEME THEME
 * This is the default theme. It should not be removed because the default theme
 * is used for default file reference in case a template malfunction or doesn't
 * have all the required theme files.
 * -----------------------------------------------------------------------------
 * The setting.php file serves as a tracker for the developer to declare notes
 * and credits. All variables are required to be declared for the template to be
 * functional.
 * -----------------------------------------------------------------------------
 * The theme screenshot image must be 300x200 pixels in JPG format and should be
 * named [screenshot.jpg]. Any other format or extension will be ignored.
 */
$_THEME_DIR_NAME                = 'jtheme';
$_THEME_AUTHOR                  = 'J. Taniguchi';
$_THEME_AUTHOR_URL_REFERENCE    = 'http://intersofts.com';
$_THEME_DATE                    = '2013-06-08';     // year-month-date
$_THEME_VERSION                 = '1.0.0';
$_THEME_SUPPORTED_VERSION       = '1.3.x';
$_THEME_LICENSE                 = 'GNU / MIT';
$_THEME_AUTHOR_NOTES            = 'This is the default template of jT CarFramework which was designed during the platform\'s development.';


