<?php
    /**
     * set language.
     */
    $_LANG = array(
        'TITLE_HEADER' => 'File Upload',
        'SAVE_EXIT_BUTTON' => 'Save and exit',
        'ADD_FILES_BUTTON' => 'Add files...',
        'START_UPLOAD_BUTTON' => 'Start upload',
        'CANCEL_UPLOAD_BUTTON' => 'Cancel upload',
        'DELETE_BUTTON' => 'Delete',
        'SELECT_ALL_CHECKBOX' => 'Select all files',
        'DOWNLOAD_BUTTON' => 'Download',
        'SLIDESHOW_BUTTON' => 'Slideshow',
        'NEXT_BUTTON' => 'Next',
        'PREVIOUS_BUTTON' => 'Previous',
        'START_BUTTON' => 'Start',
        'CANCEL_BUTTON' => 'Cancel',
        'INVALID_REQUEST' => 'Invalid request. Please go back and select an article.'
    );
    
    require_once '../../../config.php';
?>
<!DOCTYPE HTML>
<html lang="en">
<head>
    <!-- Force latest IE rendering engine or ChromeFrame if installed -->
    <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
    <meta charset="utf-8">
    <title><?php echo $_LANG['TITLE_HEADER'];?></title>
    <!-- Bootstrap CSS Toolkit styles -->
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <!-- Bootstrap styles for responsive website layout, supporting different screen sizes -->
    <link rel="stylesheet" href="../bootstrap/css/bootstrap-responsive.min.css">
    <!-- Bootstrap CSS fixes for IE6 -->
    <!--[if lt IE 7]><link rel="stylesheet" href="../bootstrap/css/bootstrap-ie6.min.css"><![endif]-->
    <!-- Bootstrap Image Gallery styles -->
    <link rel="stylesheet" href="../bootstrap/css/bootstrap-image-gallery.min.css">
    <!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
    <link rel="stylesheet" href="css/jquery.fileupload-ui.css">
    <!-- CSS adjustments for browsers with JavaScript disabled -->
    <noscript><link rel="stylesheet" href="css/jquery.fileupload-ui-noscript.css"></noscript>
    <!-- Shim to make HTML5 elements usable in older Internet Explorer versions -->
    <!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body>
    <?php
        /**
         * Make sure the article id is declared.
         */
        if( !isset($_GET['aid']) || !is_numeric($_GET['aid']) )
        {
            die('<p class="alert alert-error">' . $_LANG['INVALID_REQUEST'] . '</p>');
        }
    ?>
    <div class="container" style="width:714px;overflow:hidden;">

        <!-- The file upload form used as target for the file upload widget -->
        <form id="fileupload" action="//jquery-file-upload.appspot.com/" method="POST" enctype="multipart/form-data">
            <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
            <div class="row fileupload-buttonbar" style="position:fixed;background:#FFF;border-bottom:1px dotted #CCC;">
                <a href="<?php echo str_replace('/view/3rdparty/file_uploader','',BASE_RELATIVE);?>uploader/add-upload-to-library/?aid=<?php echo $_GET['aid'];?>" target="_parent" class="btn"><span class="icon-cog"></span> <?php echo $_LANG['SAVE_EXIT_BUTTON'];?></a>
                <div class="span7">
                    <!-- The fileinput-button span is used to style the file input field as button -->
                    <span class="btn btn-success fileinput-button">
                        <i class="icon-plus icon-white"></i>
                        <span><?php echo $_LANG['ADD_FILES_BUTTON'];?></span>
                        <input type="file" name="files[]" multiple>
                    </span>
                    <button type="submit" class="btn btn-primary start">
                        <i class="icon-upload icon-white"></i>
                        <span><?php echo $_LANG['START_UPLOAD_BUTTON'];?></span>
                    </button>
                    <button type="reset" class="btn btn-warning cancel">
                        <i class="icon-ban-circle icon-white"></i>
                        <span><?php echo $_LANG['CANCEL_UPLOAD_BUTTON'];?></span>
                    </button>
                    <button type="button" class="btn btn-danger delete">
                        <i class="icon-trash icon-white"></i>
                        <span><?php echo $_LANG['DELETE_BUTTON'];?></span>
                    </button>
                    <label>
                        <input type="checkbox" class="toggle"> <?php echo $_LANG['SELECT_ALL_CHECKBOX'];?>
                    </label>
                </div>
                <!-- The global progress information -->
                <div class="span5 fileupload-progress fade" style="height:20px;overflow:hidden;">
                    <!-- The global progress bar -->
                    <div class="progress progress-success progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                        <div class="bar" style="width:0%;"></div>
                    </div>
                    <!-- The extended global progress information -->
                    <div class="progress-extended">&nbsp;</div>
                </div>
            </div>
            <div style="height:85px;"></div>
            <!-- The loading indicator is shown during file processing -->
            <div class="fileupload-loading"></div>
            <br>
            <!-- The table listing the files available for upload/download -->
            <table role="presentation" class="table table-striped" style="width:710px;"><tbody class="files" data-toggle="modal-gallery" data-target="#modal-gallery" style="width:710px;"></tbody></table>
            <br>
        </form>
        <br>
    </div>
    <!-- modal-gallery is the modal dialog used for the image gallery -->
    <div id="modal-gallery" class="modal modal-gallery hide fade" data-filter=":odd" tabindex="-1">
        <div class="modal-header">
            <a class="close" data-dismiss="modal">&times;</a>
            <h3 class="modal-title"></h3>
        </div>
        <div class="modal-body"><div class="modal-image"></div></div>
        <div class="modal-footer">
            <a class="btn modal-download" target="_blank">
                <i class="icon-download"></i>
                <span><?php echo $_LANG['DOWNLOAD_BUTTON'];?></span>
            </a>
            <a class="btn btn-success modal-play modal-slideshow" data-slideshow="5000">
                <i class="icon-play icon-white"></i>
                <span><?php echo $_LANG['SLIDESHOW_BUTTON'];?></span>
            </a>
            <a class="btn btn-info modal-prev">
                <i class="icon-arrow-left icon-white"></i>
                <span><?php echo $_LANG['PREVIOUS_BUTTON'];?></span>
            </a>
            <a class="btn btn-primary modal-next">
                <span><?php echo $_LANG['NEXT_BUTTON'];?></span>
                <i class="icon-arrow-right icon-white"></i>
            </a>
        </div>
    </div>
    <!-- The template to display files available for upload -->
    <script id="template-upload" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
        <tr class="template-upload fade">
            <td class="preview"><span class="fade"></span></td>
            <td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
            {% if (file.error) { %}
                <td class="error" colspan="2"><span class="label label-important">Error</span> {%=file.error%}</td>
            {% } else if (o.files.valid && !i) { %}
                <td style="width: 150px;">
                    <div class="progress progress-success progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0" style="width: 150px;"><div class="bar" style="width:0%;"></div></div>
                </td>
                <td class="start">{% if (!o.options.autoUpload) { %}
                    <button class="btn btn-primary" style="width: 150px;">
                        <i class="icon-upload icon-white"></i>
                        <?php echo $_LANG['START_BUTTON'];?>
                    </button>
                {% } %}</td>
            {% } else { %}
                <td colspan="2"></td>
            {% } %}
            <td class="cancel">{% if (!i) { %}
                <button class="btn btn-warning" style="width: 150px;">
                    <i class="icon-ban-circle icon-white"></i>
                    <?php echo $_LANG['CANCEL_BUTTON'];?>
                </button>
            {% } %}</td>
        </tr>
    {% } %}
    </script>
    <!-- The template to display files available for download -->
    <script id="template-download" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
        <tr class="template-download fade">
            {% if (file.error) { %}
                <td></td>
                <td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
                <td class="error" colspan="2"><span class="label label-important">Error</span> {%=file.error%}</td>
            {% } else { %}
                <td class="preview">{% if (file.thumbnail_url) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" data-gallery="gallery" download="{%=file.name%}"><img src="{%=file.thumbnail_url%}"></a>
                {% } %}</td>
                <td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
                <td colspan="2"></td>
            {% } %}
            <td class="delete" style="width: 150px;">
                <button class="btn btn-danger" data-type="{%=file.delete_type%}" data-url="{%=file.delete_url%}"{% if (file.delete_with_credentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                    <i class="icon-trash icon-white"></i>
                    <?php echo $_LANG['DELETE_BUTTON'];?>
                </button>
                <input type="checkbox" name="delete" value="1" style="margin-left: 5px;">
            </td>
        </tr>
    {% } %}
    </script>
    <script src="../jquery/jquery.js"></script>
    <script src="js/vendor/jquery.ui.widget.js"></script>
    <script src="js/tmpl.min.js"></script>
    <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
    <script src="js/load-image.min.js"></script>
    <!-- The Canvas to Blob plugin is included for image resizing functionality -->
    <script src="js/canvas-to-blob.min.js"></script>
    <!-- Bootstrap JS and Bootstrap Image Gallery are not required, but included for the demo -->
    <script src="../bootstrap/js/bootstrap.min.js"></script>
    <script src="../bootstrap/js/bootstrap-image-gallery.min.js"></script>
    <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
    <script src="js/jquery.iframe-transport.js"></script>
    <!-- The basic File Upload plugin -->
    <script src="js/jquery.fileupload.js"></script>
    <!-- The File Upload file processing plugin -->
    <script src="js/jquery.fileupload-fp.js"></script>
    <!-- The File Upload user interface plugin -->
    <script src="js/jquery.fileupload-ui.js"></script>
    <!-- The main application script -->
    <script src="js/main.js"></script>
    <!-- The XDomainRequest Transport is included for cross-domain file deletion for IE8+ -->
    <!--[if gte IE 8]><script src="js/cors/jquery.xdr-transport.js"></script><![endif]-->
</body> 
</html>
