<?php
if(!isset($_SESSION)) @session_start();
require_once '../../../config.php';
require_once BASE_ROOT . 'language/' . $_SESSION['log_language_iso'] . '/admin-new-car.php';
include('ImageManipulator.php');

// calculate max. upload filesize.
$max_upload     = (int)(ini_get('upload_max_filesize'));
$max_post       = (int)(ini_get('post_max_size'));
$memory_limit   = (int)(ini_get('memory_limit'));
$upload_mb      = min($max_upload, $max_post, $memory_limit);
$cid            = trim($_GET['cid']);
$mode           = trim($_GET['mode']);


$product_media  = array();

function existedProduct(){
    global $cid;
    global $product_media;
    require_once BASE_CLASS . 'class-connect.php';

                        $cnx = new Connect();
                        $cnx->open();
    if( !$sql = @mysql_query("SELECT * FROM `car_media` WHERE author='".(int)$_SESSION['log_id']."' AND `car_id`='$cid'") ){
        require_once BASE_CLASS . 'class-log.php';
        LogReport::write('Unable to load image from car_media table due a query error at ' . __FILE__ . ':' . __LINE__);

    }else{
        if(mysql_num_rows($sql)>0){
            while( $r = @mysql_fetch_assoc($sql) ){
            foreach($r as $column=>$value) {
                    $product_media[$column][]=$value;
                }
            }
            return true;
        }
    }
}

/**
 * Handle file upload teset kean
 * <br>-------------------------------------------------------------------------
 */
	$prodId = $_GET['cid'];

if( isset($_POST['savebtn']) ){
	return ;
	$primary_photo=1;
    $cid = trim($_POST['caridInput']);

    //$files[]= $_FILES['fileInput'];

	//echo "No. files uploaded : ".count($_FILES['fileInput']['name'])."<br>";

  //Count number of files in array, loop through each file

  for($j=0; $j<count($_FILES['fileInput']['name']); $j++) {

    if( empty($cid) || !isset($_FILES['fileInput']['name'][$j]) ){
        $form_error = true;
        $form_message = $_LANG['NEWCAR_INVALID_REQUEST_ID_LABEL'];
    }
    else {

        // get file extension.
        $ext =$_FILES['fileInput']['name'][$j];
        $ext = explode('.', $ext);
        $ext = $ext[(count($ext)-1)];
        $ext = strtolower($ext);

        switch($ext){
            case 'jpg':
            case 'gif':
            case 'png':
                $nogood = false;
                break;
            default:
                $nogood = true;

        }

        if( $nogood ){ ?>
            <script type="text/javascript">
             alert('Invalid extension ! We has accept extension jpg,gif,png only');
            </script>
        <?php

            // $_SESSION['error_extantion']='Invalid extension !';
             echo '<meta http-equiv="refresh" content="0" />';

        }
        else {
            // upload file.
            $path  = BASE_ROOT . 'image/upload/cars/';
            $fname = date('Ymd_His') . '_' . mt_rand('0000000','9999999') . '.' . $ext;

			$preview_gallery_name="preview_gallery-".$fname;
			$special_offer_name="special_offer-".$fname;
			$list_car_name="list_car-".$fname;
            $thumb_gallery_name="thumb_gallery-".$fname;

            // upload failed.
			
            if( $_FILES['fileInput']['size'][$j] < 1 ){				
                $form_error = true;
                $form_message = $_LANG['NEWCAR_INVALID_IMAGE_SIZE_LABEL'];				
				echo '<script>alert("'.$form_message.'");</script>';
				echo '<meta http-equiv="refresh" content="0" />';
            }
            else {
            	 // save to database.
                        require_once BASE_CLASS . 'class-connect.php';

                        $cnx = new Connect();
                        $cnx->open();



						$query = "SELECT COUNT(car_media.car_id) AS pro_count, register_types.max_image
									FROM car_media
									INNER JOIN register ON register.user_id=car_media.author
									INNER JOIN register_types ON register.register_type = `register_types`.`code`
									WHERE car_media.car_id='".$cid."' AND register.user_id='".$_SESSION['log_id']."' GROUP BY car_media.car_id";



						$result = mysql_query($query);
						$max_image = 1;
						$pro_count = 0;

						while($row = mysql_fetch_array($result)){
							$max_image = $row['max_image'];
							$pro_count = $row['pro_count'];
						}

						if(($pro_count < $max_image) ){ ?>
						<script type="text/javascript">
         				$('#sal').hide();
						</script>


				<?php
                // upload the image.
                if( !@move_uploaded_file($_FILES['fileInput']['tmp_name'][$j], $path.$fname) ){
                    $form_error = true;
                    echo $form_message = $_LANG['NEWCAR_UPLOAD_FAILURE_LABEL'];
                    require_once BASE_CLASS . 'class-log.php';
                    LogReport::write('Unable to upload image at ' . __FILE__ . ':' . __LINE__);
                }
                else {
                    // resize image.
					//copy($path.$fname,$path.$medium_name);
					//copy($path.$fname,$path.$small_name);


					// *** 1) Initialise / load image


                 	$main_image=new ImageManipulator($path.$fname);

				//	$preview_gallery = new resize($path.$fname);
				 	//$special_offer = new resize($path.$fname);
				   //	$list_car = new resize($path.$fname);
		 		 	$thumb_gallery = new ImageManipulator($path.$fname);
					// *** 2) Resize image (options: exact, portrait, landscape, auto, crop)
					$main_image->resize(800, 600);
					//$main_image->watermark('watermark/watermark.png', 50, -10);
				//	$preview_gallery->resizeImage(365, 210, 'crop');
			       // $special_offer->resizeImage(156, 90, 'crop');
				   // $list_car->resizeImage(315, 225, 'crop');
				    $thumb_gallery->resize(200,150);
					 
					// *** 3) Save image

					$main_image->save($path.$fname);
					//$preview_gallery->saveImage($path.$preview_gallery_name, 100);
					//$special_offer->saveImage($path.$special_offer_name, 100);
				//	$list_car->saveImage($path.$list_car_name, 100);
					$thumb_gallery->save($path.$thumb_gallery_name);


                    // check if thumb was created successfully.




							//Define path upload to database
							$image_path='/image/upload/cars/';
							$protocol = strpos(strtolower($_SERVER['SERVER_PROTOCOL']),'https') === FALSE ? 'http://' : 'https://';
							$host = $_SERVER['HTTP_HOST'];
							$full_url=$protocol.$host.$_SERVER['REQUEST_URI'];
							//Condition for debug in local
							if($host=='localhost'){
								$start=strlen($protocol.$host);
								$end = strpos($full_url, '/', $start + 1);
								$length=$end-$start;
								$local_host=substr($full_url, $start +1, $length-1);
								$host="localhost/{$local_host}";
							}

							//Reset Primary photo and Set it Again
							//generate full destination
							$destination = $protocol.$host.$image_path.$fname;
							$thumb=$protocol.$host.$image_path."thumb_gallery-".$fname;
							if( !@mysql_query("INSERT INTO `car_media` (
																`car_id`,
																`type`,
																`author`,
																`mode`,
																`photo_url`,
																`thumb_url`,
																`primary_photo`
																) VALUES (
																'$cid',
																'image',
																'".(int)$_SESSION['log_id']."',
																'$mode',
																'$destination',
																'$thumb',
																'$primary_photo'
																)") ){
								require_once BASE_CLASS . 'class-log.php';
								LogReport::write('Unable to save image for car_media table due a query error at ' . __FILE__ . ':' . __LINE__);
								$form_error = true;
								echo $form_message = $_LANG['NEWCAR_UPLOAD_QUERY_ERROR'];
								$cnx->close();
								@unlink($path.$fname);
							}
							else {
								$primary_photo=0;
								$last_id=mysql_insert_id();


								// notify parent window to preview image.
							?>
							<script language="javascript">

								parent.carImageUploadedNew_Photo(<?php echo $last_id;?>,'<?php echo $cid;?>','image/upload/cars/<?php echo $thumb_gallery_name;?>','<?php echo $mode;?>','<?php echo $thumb_gallery_name;?>',<?php echo $j; ?>);
							</script>
							<?php
							}


                    }
                }else{

							echo "<p id='sal' style='color:red;font-size: 10px;margin-top: 35px;position: absolute;'>Please Upgrade Your Account to Upload More Then ".$max_image." Images</p>";
							$cnx->close();
				}

                }

            }

        }

    }
?>
<script type="text/javascript">
	//parent.save_data_form();
</script>
<?php
 }else{
    if(existedProduct()){
        for($i=0;$i<count($product_media['car_id']);$i++){
            echo "<script language='javascript'>parent.carImageUploaded('{$product_media['photo_id'][$i]}','{$cid}','{$product_media['photo_url'][$i]}','{$product_media['mode'][$i]}','{$product_media['photo_url'][$i]}','{$product_media['primary_photo'][$i]}');</script>";
        }

    }
 }

// end file uploader handler dd---------------------------------------------------
$form_error = false;

// internals -------------------------------------------------------------------
function formatBytes($bytes, $precision = 2) {
    $units = array('B', 'KB', 'MB', 'GB', 'TB');

    $bytes = max($bytes, 0);
    $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
    $pow = min($pow, count($units) - 1);
    $bytes /= pow(1024, $pow);

    return round($bytes, $precision) . ' ' . $units[$pow];
}

// end internals ---------------------------------------------------------------
?>
<!DOCTYPE HTML>
<html lang="en">
    <head>
        <!-- Force latest IE rendering engine or ChromeFrame if installed -->
        <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
        <meta charset="utf-8">
        <title>File uploader</title>
        <!-- Bootstrap CSS Toolkit styles -->
        <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
        <!-- Bootstrap styles for responsive website layout, supporting different screen sizes -->
        <link rel="stylesheet" href="../bootstrap/css/bootstrap-responsive.min.css">
        <!-- Bootstrap CSS fixes for IE6 -->
        <!--[if lt IE 7]><link rel="stylesheet" href="../bootstrap/css/bootstrap-ie6.min.css"><![endif]-->
        <!-- CSS adjustments for browsers with JavaScript disabled -->
        <noscript><link rel="stylesheet" href="css/jquery.fileupload-ui-noscript.css"></noscript>
        <!-- Shim to make HTML5 elements usable in older Internet Explorer versions -->
        <!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
        <script src="../jquery/jquery-2.1.1.min.js"></script>
        <!-- Bootstrap JS and Bootstrap Image Gallery are not required, but included for the demo -->
        <script src="../bootstrap/js/bootstrap.min.js"></script>
        
    </head>
    <body>
    
   
        <script type="text/javascript">
		
				
				
				$(document).ready(function() {
                     					   
					 	
						
                     $('#uploading').hide();
                 	
						
						// Check for file upload limit
					$('#product_images').change(function(e){
							
                         //$('#uploading').show();
                         // $('#submitbtn').css('float', 'right');
						//select_file=this.files.length;
				      $('#uploading').show();
					 	
					 var productId = '<?php echo $prodId;?>';
						
					   
					   var file = e.target.files[0];
								//alert(file.name);
                                // RESET
                                //$('#area p span').css('width', 0 + "%").html('');
                                //$('#area img, #area canvas').remove();
                                //$('#area i').html(JSON.stringify(e.target.files[0]).replace(/,/g, ", <br/>"));

                                // CANVAS RESIZING
                                canvasResize(file, {
                                    width: 800,
                                    height: 600,
                                    crop: false,
                                    quality: 1200,
                                    rotate: 0,
                                    callback: function(data, width, height) {

                                        // SHOW AS AN IMAGE
                                        // =================================================
                                        //alert(width);
                                        //$(img).attr('src', data);									
                                        // Create a new formdata
                                        var fd = new FormData();
                                        // Add file data
                                        var f = canvasResize('dataURLtoBlob', data);
                                        f.name = file.name;
                                        fd.append("photo", f);
										fd.append("id",productId);
                                        var xhr = new XMLHttpRequest();
										xhr.onreadystatechange=function()
  											{
  										if (xhr.readyState==4 && xhr.status==200)
									    {
											//alert("sysdf");
											//$("#error").html(xhr.responseText);
											 $('#uploading').hide();
											var info = JSON.parse(xhr.responseText);
											for (var i = 0; i<info.length;i++){
												parent.carImageUploaded(info[i].id,info[i].cid,info[i].url,info[i].mode,info[i].url,info[i].primary);
												}
											
											//	alert(info[0].id);
											//parent.carImageUploaded();
    										//xhr.responseText;
    										}
  										}
										
										
                                        xhr.open('POST', '../file_uploader/js_upload/uploader.php', true);
										
										
                                        xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
                                        xhr.setRequestHeader("pragma", "no-cache");
                                        //Upload progress
                                        
									/*	xhr.upload.addEventListener("progress", function(e) {
                                            if (e.lengthComputable) {
                                                var loaded = Math.ceil((e.loaded / e.total) * 100);
                                                $('#area p span').css({
                                                    'width': loaded + "%"
                                                }).html(loaded + "%");
                                            }
                                        }, false);*/
                                        // File uploaded
                                    /*    xhr.addEventListener("load", function(e) {
                                            var response = JSON.parse(e.target.responseText);
                                            if (response.filename) {
                                                // Complete
                                                $('#area p span').html('done');
                                                $('#area b').html(response.filename);
                                                $('<img>').attr({
                                                    'src': response.filename
                                                }).appendTo($('#area div'));
                                            }
                                        }, false);*/
                                        // Send data
										
										
                                        xhr.send(fd);
										
										   //$('form').submit();
                                        // /IMAGE UPLOADING
                                        // =================================================               
                                    }
                                });

					   
					   
					   
					});
				 
				 
				 
					
                    });


                 


						<?php
					/*	$cnx = new Connect();
						$cnx->open();

							$max_image = 0;
							$max_pro = 0;
							$pro_count = 0;
							$register_type=$_SESSION['register_type'];*/
							?>
							//$query = "SELECT * FROM register_types WHERE code='".$register_type."'";
							/*$query="SELECT COUNT(car_media.car_id) AS pro_count, register_types.max_image
FROM car_media
INNER JOIN register ON register.id=car_media.author
INNER JOIN register_types ON register.register_type = `register_types`.`code`
WHERE car_media.car_id='".$cid."' AND register.id='".$_SESSION['log_id']."' GROUP BY car_media.car_id";*/


							//Count all products user upload

							/*$query = "SELECT COUNT(product.car_id) AS owner_pro_count
										FROM product
										WHERE product.owner='".$_SESSION['log_id']."'";
							$result = mysql_query($query);
							$owner_pro_count = '';

							$row = mysql_fetch_array($result);
							$owner_pro_count = $row['owner_pro_count'];*/

							/*if($owner_pro_count == $_SESSION['user_max_image_upload']){

							}else{

							}*/


							//Count all products that have image or not
							/*
							$query="SELECT COUNT(tmp.car_id) AS pro_count, tmp.max_image, tmp.max_product
									FROM (SELECT cm.car_id, rg.user_id, rgt.max_image, rgt.max_product
												FROM car_media cm
												INNER JOIN register rg ON rg.user_id=cm.author
												INNER JOIN register_types rgt ON `rgt`.`code` = rg.register_type
												WHERE rg.user_id='".$_SESSION['log_id']."') tmp
									WHERE tmp.car_id='$cid'";

							$result = mysql_query($query);

							while($row = mysql_fetch_array($result)){
								if($row['max_image'] == ''){

									$max_image = $_SESSION['user_max_image_upload'];
								}else{
									$max_image = $row['max_image'];

								}

								if($row['max_product'] == ''){

									$max_pro = $_SESSION['user_max_pro'];
								}else{
									$max_pro = $row['max_product'];

								}



								$max_pro = $row['max_product'];
								$pro_count = $row['pro_count'];
							}

						?>



						/*var max_image = <?php //echo $max_image; ?>;
						var pro_count = <?php //echo $pro_count; ?>;
						var owner_pro_count = <?php //echo $owner_pro_count;?>;*/
						//alert(this.files.length + " Max_image: " + max_image +" Product_count: "+pro_count);


					/*});*/
                        // Prevent submission if limit is exceeded.

                        // End Check for file upload limit
              //  });
        </script>
    
    
    
    
    
    
    
    
    
    
    

        <?php
            /**
             * Make sure the article id is declared.
             */
            if(  $_SESSION['log_group'] != 'user' && $_SESSION['log_group'] != 'admin' )
            {
                die('<p class="alert alert-error">' . $_LANG['NEWCAR_INVALID_UPLOAD_REQUEST_LABEL'] . '</p>');
            }

            // get car id.
            if( empty($_GET['cid']) ){
                die('<p class="alert alert-error">' . $_LANG['NEWCAR_INVALID_REQUEST_ID_LABEL'] . '</p><p><a href="admin-addpic.php?cid='.$cid.'&mode='.$mode.'">'.$_LANG['NEWCAR_GOBACK_LABEL'].'</a></p>');
            }

            // form check
            if( $form_error ){
                die('<p class="alert alert-error">' . $form_message . '</p><p><a href="admin-addpic.php?cid='.$cid.'&mode='.$mode.'">'.$_LANG['NEWCAR_GOBACK_LABEL'].'</a></p>');
            }
            else {
        ?>

 <span><img id="uploading" style="float: right;margin-right:10px;"  src="<?php echo BASE_RELATIVE; ?>img/uploading.gif " /></span>

        <div style="overflow:hidden;">

            <!-- The file upload form used as target for the file upload widget -->
            <!--<form method="POST" enctype="multipart/form-data" style="margin:0;" action="?cid=<?php //echo $cid;?>&mode=<?php //ssecho $mode;?>">-->
				
                <div class="input-append">
               
                <div style="border:1px solid #ccc; box-sizing:border-box; width:82%;">
					<img src="../../../images/upload-btn.png" id="upfile" style="cursor:pointer; margin:2px" />
                    <input type="file" id="product_images" name="fileInput" accept="image/*" style="display:none" />
                </div>

                 
                    <input type="submit" id="btn_upload_photo" name="savebtn" class="btn btn-success" value="<?php echo $_LANG['NEWCAR_UPLOAD_BUTTON'];?>" style="display:none;" />
				
                </div>
                <div id="error"></div>
                <?php 
				
			

							//Reset Primary photo and Set it Again
							//generate full destination
						

				
				
				?>
           <!-- </form>-->
            <br>
        </div>
        <?php
            }
        ?>
<script type="text/javascript">
	$("#upfile").click(function () {
    $("#product_images").trigger('click');
	});

</script>
 <script src="js_upload/binaryajax.js"></script>
                    <script src="js_upload/exif.js"></script>
                    <script src="js_upload/canvasResize.js"></script>

    </body>
</html>
