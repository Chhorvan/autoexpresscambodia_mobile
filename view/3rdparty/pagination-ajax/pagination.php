﻿<?php
include('page.php');
class Pagination{

  private $num_pages = 1;
  var $start = 0;
  private $display;
  var $start_display;
  private $page;
  private $current_page=1;


  function __construct ($query, $display=6) {
    global $page;
    $page=new Pages();

    if (!empty($query)) {
      $this->display = $display;
      if (isset($_GET['display']) && is_numeric($_GET['display'])) $this->display = (int) $_GET['display'];
      //if (isset($_GET['np']) && is_numeric($_GET['np']) && $_GET['np'] > 0) { 
      //  $this->num_pages = (int) $_GET['np'];
      //} else {
        if (is_numeric($query)) {
          $num_records = $query;
        } else {
          $result = db_query ($query);
          if ($result->num_rows > 1 || strstr($query, 'COUNT') === false) {
            $num_records = $result->num_rows;
          } else {
            $row = $result->fetch_row();
            $num_records = $row[0];
          }
        }
        if ($num_records > $this->display) $this->num_pages = ceil ($num_records/$this->display);
      //} 
      if (isset($_GET['s']) && is_numeric($_GET['s']) && $_GET['s'] > 0) $this->start = (int) $_GET['s'];
      
      $this->start_display = " LIMIT {$this->start}, {$this->display}";
    }
  }

  public function display ($split=3) {
    global $page;
    $html = '';
    if ($this->num_pages <= 1) return $html;
    // $page->link('pagination.css');
    $url="";
    //$url = $page->url ('add', '', 'np', $this->num_pages);

    $current_page = ($this->start/$this->display) + 1;
    $begin = $current_page - $split;
    $end = $current_page + $split;
    if ($begin < 1) {
      $begin = 1;
      $end = $split * 2 + 1;
    }
    if ($end > $this->num_pages) {
      $end = $this->num_pages;
      $begin = $end - ($split * 2 + 1);
      $begin++; // add one so that we get double the split at the end
      if ($begin < 1) $begin = 1;
    }
    
    if ($current_page != 1) {
      $html .= '<a offset="0" class="first" title="First" id="link-first" href="#">&laquo;</a>';
      $html .= '<a offset="'.($this->start - $this->display).'" class="prev" title="Previous" id="link-prev" href="#">Previous</a>';
    } else {
      $html .= '<span class="disabled first" title="First">&laquo;</span>';
      $html .= '<span class="disabled prev" title="Previous">Previous</span>';
    }
    for ($i=$begin; $i<=$end; $i++) {
      if ($i != $current_page) {
        $html .= '<a offset="'.($this->display * ($i - 1)).'" title="' . $i . '" href="#">' . $i . '</a>';
      } else {
        $this->current_page=$i;
        $html .= '<span class="current">' . $i . '</span>';
      }
    }
    if ($current_page != $this->num_pages) {
      $html .= '<a offset="'.($this->start + $this->display).'" class="next" title="Next" id="link-next" href="#">Next</a>';
      $last = ($this->num_pages * $this->display) - $this->display;
      $html .= '<a offset="'.$last.'" class="last" title="Last" href="#">&raquo;</a>';
    } else {
      $html .= '<span class="disabled next" title="Next">Next</span>';
      $html .= '<span class="disabled last" title="Last">&raquo;</span>';
    }
    //display current page position
    //$page_position="Page: ".$this->current_page."/".$this->num_pages." ";

    return '<div class="pagination">'. $html . '</div>';
  }

  public function currentPage(){
      return $this->current_page;
  }

  public function numPages(){
    return $this->num_pages;
  }

  public function limit () {
    return $this->start_display;
  }

}

?>