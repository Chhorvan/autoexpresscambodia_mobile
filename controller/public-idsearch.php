<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
class publicidsearch
{
    protected $lang;
    protected $group = 'public';
    protected $slug = 'car';
    protected $form_status;
    protected $form_message;
    protected $form_style;
	protected $country_list;
    /*
     * Constructor -------------------------------------------------------------
     */
    public function publicidsearch($lang=array())
    {
		$this->loadCountryList();
        $this->lang = $lang;
    }

    /*
     * Public method: get page content data ------------------------------------
     * @return array | false.
     */
    public function getPageHTML()
    {
        require_once BASE_ROOT . 'core/class-connect.php';

        $cnx = new Connect();
        $cnx->open();

        if( !$sql = @mysql_query("SELECT * FROM `page` WHERE `page_slug`='$this->slug' AND `page_group`='$this->group' LIMIT 1;") )
        {
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }

        if( @mysql_num_rows($sql) != 1 )
        {
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }

        $r = @mysql_fetch_assoc($sql);

        $result                     = array();
        $result['id']               = $r['id'];
        $result['meta_keywords']    = $r['meta_keywords'];
        $result['meta_description'] = $r['meta_description'];
        $result['meta_robots']      = $r['meta_robots'];
        $result['page_title']       = $r['page_title'];
        $result['page_slug']        = $r['page_slug'];
        $result['page_publish']     = $r['page_publish'];
        $result['page_author']      = $r['page_author'];
        $result['creation']         = $r['creation'];
        $result['editable']         = $r['editable'];
        $result['page_group']       = $r['page_group'];
        $result['html']             = $r['html'];

        @mysql_free_result($sql);
        $cnx->close();

        return $result;
    }

	private function loadCountryList(){
        require_once BASE_CLASS . 'class-connect.php';

        $cnx = new Connect();
        $cnx->open();

        if( !$sql = @mysql_query("SELECT * FROM `country_list` ORDER BY `country_name` ASC") ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load country list due a query error at ' . __FILE__ . ':' . __LINE__);

            $this->country_list = array();

            return;
        }

        if( @mysql_num_rows($sql) < 0 ){
            $cnx->close();
            return;
        }

        $this->country_list = array();
        while( $r = @mysql_fetch_assoc($sql) ){
            array_push($this->country_list, $r);

        }
        @mysql_free_result($sql);
        $cnx->close();
    }

	public function getUserID(){
		require_once BASE_CLASS . 'class-connect.php';

        $cnx = new Connect();
        $cnx->open();

		$country = $_POST['country'];
		$email = $_POST['email_id'];


		$query = "SELECT log_id FROM register WHERE email='".$email."' AND country='".$country."'";
		$result=mysql_query($query);
		if( mysql_num_rows($result) > 0){
			$log_id = '';
			while($row = mysql_fetch_array($result)){
				$log_id = $row['log_id'];
			}

			$emial_to = $_POST['email_id'];

			$subject = 'Website Change Request';

			$headers = "From: support@motorbb.com\r\n";
			$headers .= "Reply-To: support@motorbb.com\r\n";
			$headers .= "CC: support@motorbb.com\r\n";
			$headers .= "MIME-Version: 1.0\r\n";
			$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

			$message = '<html><body>';
			$message .= '<p>Here is your userID: '.$log_id.'</p>';
			$message .= '</body></html>';

			if(mail($emial_to, $subject, $message, $headers)){
				echo '<p style="color:#900000">Please check your email, Your userId already send to your email!</p><br>';
			}

		}

	}

	public function getCountryList(){
        return $this->country_list;
    }
    /*
     * Public method: get form status ------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }

    /*
     * Public method: get form message -----------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /*
     * Public method: get form style -------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }

}
?>
