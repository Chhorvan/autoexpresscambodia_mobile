<?php
require_once BASE_ROOT.'config.php';
if(!isset($_SESSION)) @session_start();
class publiccontactseller
{
	public $stock_detail;
	public $buyer_detail;
	public $result='';
    public $error=array();
    protected $_LANG;
    protected $country_list;
    protected $last_register_id='';
		 /*
     * Constructor -------------------------------------------------------------
     */
    public function publiccontactseller($lang=array())
    {




        // set language vars.
        $this->_LANG = $lang;
        $this->loadCountryList();

		$this->getstock_detail();
		if($this->isLoggedIn()){
			$this->getBuyerDetail();
			if($this->resubmit()){
				$this->sendEmail();
			}elseif(isset($_POST['submit'])){
                extract($_POST);
                        $cid="";
                if(isset($_GET['cid']))
                $cid=$_GET['cid'];
                if(isset($_POST['product_id'])) $product_id=$_POST['product_id']; else $product_id=$cid;

                $this->sendEmail();


				     Header("Location: contact-seller?result=success&cid=".$product_id);

			}
		}

		if( isset($_POST['loginBtn']) )
        {
        	extract($_POST);
            $this->handleLoginSubmit();
             // $this->sendEmail();

        }
        if( isset($_POST['regbtn']) )        {


            $this->processRegisterForm2();


        }

	}
	protected function handleLoginSubmit()
    {
     $cid="";
        if(isset($_GET['cid']))
        $cid=$_GET['cid'];
        if(isset($_POST['product_id'])) $product_id=$_POST['product_id']; else $product_id='';
       $pro='?cid='.$product_id."&action=submit";

        $email = trim($_POST['emailInput']);
        $pass  = trim($_POST['passwordInput']);
        require_once BASE_CLASS . 'class-utilities.php';


        if( strlen($pass) < 5 || strlen($pass) > 20 )
        {
            $this->error_status = true;
            $this->error_type = 'error';

            $this->error_formname = 'login';
            return;
        }

        // check if email matches the super admin's.
        if( $email == ADMIN_CONTACT_EMAIL )
        {
            // validate admin password.
            require_once BASE_CLASS . 'class-connect.php';

            $db = new Connect();
            $db->open();

            $pass = md5($pass);
            $email = mysql_real_escape_string($email);

            if( !$sql = mysql_query("SELECT `admin_email`, `admin_pass` FROM `setting` WHERE `admin_email`='$email' AND `admin_pass`='$pass'") )
            {
                require_once BASE_CLASS . 'class-log.php';

                LogReport::write('Unable to query database at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

                $db->close();

                $this->error_status = true;
                $this->error_type = 'warning';
                $this->error_message = $this->lang['LOGIN_QUERY_ERROR'];
                $this->error_formname = 'login';
                return;
            }

            if( @mysql_num_rows($sql) != 1 )
            {
                $db->close();

                $this->error_status = true;
                $this->error_type = 'error';
                $this->error_message = $this->lang['LOGIN_INVALID_EMAIL'];
                $this->error_formname = 'login';
                return;
            }
            else
            {
                // set admin session.
                $_SESSION['log_group'] = 'admin';
                $_SESSION['log_name']  = 'Administrator';
                $_SESSION['log_id']    = -1;
                $_SESSION['log_email'] = $email;
                $_SESSION['tmp_product_id']=$_POST['product_id'];


    //              $this->sendEmail();
				// Header("Location: contact-seller?result=".$this->result);

               // Header("Location: contact-seller".$pro);
                exit;
            }
        }
        // user is not administrator.
        else
        {
            require_once BASE_CLASS . 'class-connect.php';

            $db = new Connect();
            $db->open();

            $pass = md5($pass);
            $email = mysql_real_escape_string($email);


            //echo $email."Test Email";
            if( !$sql = mysql_query("SELECT * FROM `register` WHERE `log_id`='$email' AND `password`='$pass'") )
            {
                require_once BASE_CLASS . 'class-log.php';

                LogReport::write('Unable to query database at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

                $db->close();

                $this->error_status = true;
                $this->error_type = 'warning';
                $this->error_message = $this->lang['LOGIN_QUERY_ERROR'];
                $this->error_formname = 'login';
                return;
            }

            if( @mysql_num_rows($sql) != 1 )
            {
                $db->close();

                $this->error_status = true;
                $this->error_type = 'error';
                $this->error_message = $this->lang['LOGIN_INVALID_EMAIL'];
                $this->error_formname = 'login';
                return;
            }

            // check if account is active and user is not banned.
            $r = mysql_fetch_assoc($sql);

            @mysql_free_result($sql);
            $db->close();

            if( (bool)$r['banned'] )
            {
                $this->error_status = true;
                $this->error_type = 'warning';
                $this->error_message = $this->lang['LOGIN_BANNED_ACCOUNT'];
                $this->error_formname = 'login';
                return;
            }

            if( !(bool)$r['activated'] )
            {
                $this->error_status = true;
                $this->error_type = 'warning';
                $this->error_message = $this->lang['LOGIN_NON_ACTIVE_ACCOUNT'];
                $this->error_formname = 'login';
                return;
            }

            // set login session.
            $_SESSION['log_group'] = $r['group'];
            $_SESSION['log_name']  = $r['name'];
			$_SESSION['company_name']=$r['company_name'];
			$_SESSION['log_id']    = $r['id'];
            $_SESSION['userlog_id']    = $r['log_id'];
            $_SESSION['log_email'] = $r['email'];
			$_SESSION['register_type'] = $r['register_type'];
			$_SESSION['pro']=$pro;
			$_SESSION['tmp_product_id']=$_POST['product_id'];
			$_SESSION['tmp_message_input']=$_POST['messageInput'];
			$_SESSION['tmp_subject_input']=$_POST['subjectInput'];


			// Header("Location: contact-seller?result=".$this->result);





			$db = new Connect();
            $db->open();

			$query="SELECT * FROM register_types WHERE register_types.code='".$_SESSION['register_type']."'";
			$result = mysql_query($query);

			$row = mysql_fetch_array($result);

			$_SESSION['user_max_image_upload'] = $row['max_image'];
			$_SESSION['user_max_pro'] = $row['max_product'];
			$_SESSION['tmp_product_id']=$_POST['product_id'];
			$_SESSION['tmp_message_input']=$_POST['messageInput'];
			$_SESSION['tmp_subject_input']=$_POST['subjectInput'];
			$_SESSION['log_email']=$r['email'];
             $this->processReplyForm();



           // Header("Location: contact-seller?cid=".$_POST['product_id']."&action=submit");


           // Header("Location: contact-seller".$pro);
            exit;
        }
    }

        protected function processRegisterForm()
    {
        require_once BASE_CLASS . 'class-connect.php';
        require_once BASE_CLASS .'resize-class.php';
        // Initialize
        $member_type_1 = array();
        $member_type_1_string = "";
        $member_type_2 = array();
        $member_type_2_string = "";
        $business_type = array();
        $business_type_string = "";
        $business_field = array();
        $business_field_string = "";
        $log_id = "";
        $password = "";
        $confirm_password = "";
        $email = "";
        $email_error = "";
        $confirm_email = "";
        $introduction = "";
        $msg = "";
        $company_name = "";
        $name = "";
        $card_id = "";
        $country = "";
        $address = "";
        $post_code = "";
        $tel = "";
        $fax = "";
        $mobile = "";
        $website = "";
        $available_services = array();
        $available_services_string = "";
        $term = "";
        $securityInput;
        extract($_POST);

        // check if email is already registered.
        require_once BASE_CLASS . 'class-connect.php';

        $db = new Connect();
        $db->open();

        $query = "SELECT email FROM register WHERE email = '$email' || log_id = '$log_id' ";
        $sql = @mysql_query($query);


        if( @mysql_num_rows($sql) > 0 ) {
                $db->close();
                $this->register_form_status = 'email';
                return;
            }
            else {


                // filter values.
                $email = mysql_real_escape_string($email);
               $passMD5 = md5($password);
                $rows = mysql_fetch_array(mysql_query("SELECT * FROM register ORDER BY id DESC"));
                $ids = $rows['id'];
                $ids++;

                // register the user account.

                if( !$sql = @mysql_query("INSERT INTO `register` (`log_id`, `email`, `password`, `group`, `creation`, `banned`, `activated`, `member_type1`, `company_name`, `country`,`register_type`)
                    VALUES('$log_id', '$email', '$passMD5', 'user', '".date('Y-m-d')."', '0', '0', 'Buyer','$company_name','$country','m1')") ) {
                    require_once BASE_CLASS . 'class-log.php';
                    LogReport::write('Unable to query database at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

                    $db->close();
                    $this->register_form_status = 'query';

                    return;
				}
                else
                {

                    $reg_id = mysql_insert_id();

                    $query = "INSERT INTO data_count SET register_id = '".$reg_id."',
                                                         country = '$country',
                                                         car_count = '0',
                                                         truck_count = '0',
                                                         bus_count = '0',
                                                         part_count = '0',
                                                         accessories_count = '0',
                                                         equipment_count = '0',
                                                         motorbike_count = '0',
                                                         aircraft_count = '0',
                                                         watercraft_count = '0'";
                    mysql_query($query);

                        // echo "Success Update Query";
                    // Сheck that we have a file



                    // set send mail message.
                    //$message = $this->_LANG['SENDEMAIL_MESSAGE_REGISTRATION'];
                    $message = '<a href="'.BASE_SITE.'activate/?account='.base64_encode($email).'">' . $this->_LANG['SENDEMAIL_ACTVATE_NOW'] . '</a><br>';

                    // send activation email to user.
                    require BASE_CLASS . 'class-sendmail.php';

                    $send = new SendEmail();
                    $send->setEmail($email);
                    $send->setMessage($message);
                    $send->setSubject($this->_LANG['SENDEMAIL_SUBJECT_LABEL']);
                    $send->send();

                    $db->close();


                    $this->register_form_status = 'complete';

                header("Location: " .BASE_RELATIVE . "signup-success");

                }
            }
        }
         protected function processRegisterForm2()
    {
        require_once BASE_CLASS . 'class-connect.php';
        require_once BASE_CLASS .'resize-class.php';
        // Initialize
        $member_type_1 = array();
        $member_type_1_string = "";
        $member_type_2 = array();
        $member_type_2_string = "";
        $business_type = array();
        $business_type_string = "";
        $business_field = array();
        $business_field_string = "";
        $log_id = "";
        $password = "";
        $confirm_password = "";
        $email = "";
        $email_error = "";
        $confirm_email = "";
        $introduction = "";
        $msg = "";
        $company_name = "";
        $contactpersion="";
        $name = "";
        $card_id = "";
        $country = "";
        $address = "";
        $post_code = "";
        $tel = "";
        $fax = "";
        $mobile = "";
        $website = "";
        $available_services = array();
        $available_services_string = "";
        $term = "";
        $securityInput;
        extract($_POST);
         $cid="";
        if(isset($_GET['cid']))
        $cid=$_GET['cid'];
        if(isset($_POST['product_id'])) $product_id=$_POST['product_id']; else $product_id=$cid;
       $pro='?cid='.$product_id."&action=submit";

        // check if email is already registered.
        require_once BASE_CLASS . 'class-connect.php';

        $db = new Connect();
        $db->open();

        $query = "SELECT email FROM register WHERE email = '$email' || log_id = '$log_id' ";
        $sql = @mysql_query($query);


        if( @mysql_num_rows($sql) > 0 ) {
                $db->close();
                $this->register_form_status = 'email';
                return;
            }
            else {


                // filter values.
                $email = mysql_real_escape_string($email);
               $passMD5 = md5($password);
                $rows = mysql_fetch_array(mysql_query("SELECT * FROM register ORDER BY id DESC"));
                $ids = $rows['id'];
                $ids++;
                // register the user account.
                if( !$sql = @mysql_query("INSERT INTO `register` (`log_id`, `email`, `password`, `group`, `creation`, `banned`, `activated`, `member_type1`, `company_name`, `country`,`register_type`,`name`)
                    VALUES('$log_id', '$email', '$passMD5', 'user', '".date('Y-m-d')."', '0', '0', 'Buyer','$company_name','$country','m1','$contactpersion')") ) {
                    require_once BASE_CLASS . 'class-log.php';
                    LogReport::write('Unable to query database at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

                    $db->close();
                    $this->register_form_status = 'query';

                    return;
                }
                else
                {

                    $reg_id = mysql_insert_id();

                    $query = "INSERT INTO data_count SET register_id = '".$reg_id."',
                                                         country = '$country',
                                                         car_count = '0',
                                                         truck_count = '0',
                                                         bus_count = '0',
                                                         part_count = '0',
                                                         accessories_count = '0',
                                                         equipment_count = '0',
                                                         motorbike_count = '0',
                                                         aircraft_count = '0',
                                                         watercraft_count = '0'";
                    mysql_query($query);
                    $this->last_register_id=$reg_id;
                        // echo "Success Update Query";
                    // Сheck that we have a file



                    // set send mail message.
                    //$message = $this->_LANG['SENDEMAIL_MESSAGE_REGISTRATION'];
                    $message = '<a href="'.BASE_SITE.'activate/?account='.base64_encode($email).'">' . $this->_LANG['SENDEMAIL_ACTVATE_NOW'] . '</a><br>';

                    // send activation email to user.
                    require BASE_CLASS . 'class-sendmail.php';

                    $send = new SendEmail();
                    $send->setEmail($email);
                    $send->setMessage($message);
                    $send->setSubject($this->_LANG['SENDEMAIL_SUBJECT_LABEL']);
                    $send->send();

                    $db->close();


                    $this->register_form_status = 'complete';
                    // $this->sendEmail();
                 // Header("Location: contact-seller".$pro);


                   // Header("Location: contact-seller?result=".$this->result."&cid=".$product_id);

                }
            }
            // set login session.
            $_SESSION['company_name']=$_POST['company_name'];

            $_SESSION['log_email'] = $_POST['email'];


             $this->sendEmail();


               Header("Location: contact-seller?result=success&cid=".$product_id);
              // Header("Location: contact-seller?result=".$this->result."&cid=".$product_id);

        }

    public function getIpAddresses()
    {
       $ipAddresses = array();
       if (isset($_SERVER["HTTP_X_FORWARDED_FOR"])) {

          $ipAddresses['proxy'] = isset($_SERVER["HTTP_CLIENT_IP"]) ? $_SERVER["HTTP_CLIENT_IP"] : $_SERVER["REMOTE_ADDR"];
          $ipAddresses['user'] = $_SERVER["HTTP_X_FORWARDED_FOR"];

       } else {
          $ipAddresses['user'] = isset($_SERVER["HTTP_CLIENT_IP"]) ? $_SERVER["HTTP_CLIENT_IP"] : $_SERVER["REMOTE_ADDR"];
       }
       return $ipAddresses;
    }

    public function processReplyForm()
    {
        // Send email
        require_once dirname(dirname(__FILE__)) . '/config.php';
        require_once BASE_ROOT . 'core/class-connect.php';
        require_once BASE_ROOT . 'core/class-file-system.php';

        $cnx = new Connect();
        $cnx->open();
        mysql_query("SET Names utf8");


        $sender = "";
        $receiver = "";
        $product = "";
        $i_sMessage = "";
        $str = "";
        $m_id = "";

        extract($_POST);
        extract($_GET);


        $get_ip = $this->getIpAddresses();
        $ips = $get_ip['user'];
        // $str = $i_sMessage;
        // $i = strrpos($str, "---------- [ Original Message ] ----------");
        // $i = strrpos($str, "To :");

        // $l = strlen($str) - $i;

        // $str = substr($str, $i, $l);

        // $i_sMessage = str_ireplace($str, "", $i_sMessage);
        // $i_sMessage = mysql_real_escape_string($i_sMessage);
        // $i_sMessage = htmlentities($i_sMessage);
        // echo  $sender;

        $row = mysql_fetch_array(mysql_query("SELECT * FROM message ORDER BY id DESC"));
        $id = $row['id'];
        $id++;

        $row_rep = mysql_fetch_array(mysql_query("SELECT * FROM message WHERE id = '$m_id'"));

        $port = $row_rep['port_name'];
        $pay_term = $row_rep['payment_term'];
        $price_term = $row_rep['price_term'];
        $country = $row_rep['country'];
        $buyer_id = $row_rep['buyer_id'];


        if( $sql = mysql_query("INSERT INTO `message` (`id`, `message`, `sent_id`, `response_id`, `duration`, `seen`, `country`, `port_name`, `payment_term`, `price_term`, `product_id`, `buyer_id`, `ip`)
                                VALUES('$id++', '$i_sMessage', '$sender', '$receiver', '".date('Y-m-d H:i:s')."', '0', '$country', '$port', '$pay_term', '$price_term', '$product', $buyer_id, '$ips')") )
        {
            if(isset($_POST['att_file_name'])){
                foreach ($_POST['att_file_name'] as $key => $value) {
                    $explode_file = explode('/', $_POST['att_file_name'][$key]);
                    $file_name = $explode_file[0];
                    $string_date = $explode_file[1];
                    $extension = end($explode_file);
                    $encode_name = md5($file_name . $string_date). '.' .$extension;

                    $location = "upload/message-attachment/{$receiver}/";
                    if(FileSystem::createDir($dir = dirname(dirname(__FILE__)) . '/' . $location)){
                        rename($dir. $file_name. $string_date .'.tmp', $dir. $encode_name);
                        mysql_query("INSERT INTO message_attachment (message_ref, file_name, encode_name, location) VALUES ($id, '{$file_name}.{$extension}', '$encode_name', '$location')");
                    }
                }
            }

            $this->form_status = 'complete';
        } else {
            require_once BASE_CLASS . 'class-log.php';

            LogReport::write('Unable to query database at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_status = 'query';
        }
        $cnx->close();

        unset($_GET);
    }


	//Get product that buyer want by id
	private function getstock_detail(){
		require_once BASE_CLASS . 'class-connect.php';

		$cid="";
		if(isset($_GET['cid']))
		$cid=$_GET['cid'];

        $cnx = new Connect();
        $cnx->open();

		$query = "SELECT p.*, c.source as carImg, c.thumb as thumb, cl.country_name as country,
                            cl.cc as flag ,cl.country_name, rg.company_name, rg.name, rg.email
                            FROM product as p
                            LEFT JOIN car_media as c on p.id=c.product_id AND c.primary_photo='1' OR c.primary_photo='2'
                            INNER JOIN country_list as cl on p.location=cl.cc
                            LEFT JOIN register as rg on p.owner=rg.id
							WHERE p.id='$cid'";

		$result = mysql_query($query);
		if(!$result){
			require_once BASE_CLASS . 'class-log.php';
            $cnx->close();

			$this->form_message = $this->lang['MANAGE_NEWS_LOAD_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
		}

		$this->stock_detail = array();
		if(mysql_num_rows($result)>0){

			while( $r = @mysql_fetch_assoc($result) ){
	            array_push($this->stock_detail, $r);
			}
		}
		$cnx->close();

	}
	//Check if the user is already logged in
	private function isLoggedIn(){
		if(isset($_SESSION['log_group'])) $log_group=$_SESSION['log_group']; else $log_group="";
		if($log_group=='user'){
			return true;
		}
	}
	//Get buyer detail if user is logged in
	private function getBuyerDetail(){
		if(isset($_SESSION['log_id'])) $log_id=$_SESSION['log_id']; else $log_id="";
		require_once BASE_CLASS . 'class-connect.php';
        $buyer_detail=array();
        $cnx = new Connect();
        $cnx->open();
		$sql="SELECT rg.email FROM register rg WHERE id='$log_id'";
		$result=mysql_query($sql);
		while($row=mysql_fetch_assoc($result)){
			array_push($buyer_detail, $row);
		}
		$this->buyer_detail=$buyer_detail['0'];
		$cnx->close();
	}
	//Process sending email if user is logged in and clicked on submit button
	private function sendEmail(){
        require BASE_CLASS . 'class-user.php';

		//Get all values by input
        //Required Fields: product id, subject, email
		if(isset($_POST['product_id'])) $product_id=$_POST['product_id']; else $product_id='';
		if(isset($_POST['subjectInput'])) $subjectInput=$_POST['subjectInput']; else $subjectInput='Testing';
		if(isset($_POST['messageInput'])) $messageInput=$_POST['messageInput']; else $messageInput='';
        if(isset($_POST['emailInput'])) $emailInput=$_POST['emailInput']; else $emailInput='';

		//Get all values by session if exists
		if(isset($_SESSION['tmp_product_id'])) $product_id=$_SESSION['tmp_product_id'];
		if(isset($_SESSION['tmp_subject_input'])) $subjectInput=$_SESSION['tmp_subject_input'];
		if(isset($_SESSION['tmp_message_input'])) $messageInput=$_SESSION['tmp_message_input'];

        $fromProfile=new User();
        if(!empty($this->last_register_id)){

            $fromProfile->loadProfile($this->last_register_id, array('company_name', 'address', 'country', 'address', 'tel', 'email', 'country_list.country_name as country_name'));


        }else{

            if(isset($_SESSION['log_id'])) $log_id=$_SESSION['log_id'];
            $fromProfile->loadProfile($log_id, array('company_name', 'address', 'country', 'address', 'tel', 'email', 'country_list.country_name as country_name'));
        }
        $fromProfileDetail=$fromProfile->getProfile();

        $log_email=$fromProfileDetail['email'];
        $from=$log_email;
        if(isset($_POST['emailInput'])){
            if($log_email!=$emailInput){

                $from=$emailInput;

                if($this->validate_email($from)){
                    if(!$this->updateBuyerEmail($from)){
                        $this->error['email']="Please enter a valid email!";
                    }
                }else{
                    $this->error['email']="Please enter a valid email!";
                }
            }
        }

       // if(isset($_POST['email'])) $from=$_POST['email'];
        if(empty($from)){
            $this->error['email']="Email cannot be empty!";
        }


		//$from = $this->buyer_detail['email'];
        //var_dump($this->error);

        if(count($this->error)==0){
		     // $to='nochneang@yahoo.com';
              $to=$this->stock_detail['0']['email'];

    		require BASE_CLASS . 'class-user-sendmail.php';

            require BASE_CLASS . 'class-message.php';

            /* Initial Class */
    		$send = new UserSendEmail();
            $message=new Message();


            /* Set values to member of class */
            $send->setId($this->stock_detail['0']['id']);
            $send->setModelYear($this->stock_detail['0']['model_year']);
            $send->setModel($this->stock_detail['0']['model']);
            $send->setMake($this->stock_detail['0']['make']);
            $send->setProductImage($this->stock_detail['0']['thumb']);

    		$send->setMessage($messageInput);
        	$send->setSubject($subjectInput);
        	$send->setEmail($to);
        	$send->setToCompanyName($this->stock_detail['0']['company_name']);

            //$send->setFromCompanyName($fromCompanyName);
            $send->setEmailFrom($log_email);
            $send->setFromProfileLink($fromProfile->getProfileLink());
            //$send->setFromAddress($fromProfileDetail['address'].", ".$fromProfileDetail['country_name']);
            $send->setFromProfileContainer(array("Company Name"=>"<a href='".$fromProfile->getProfileLink()."'/>".$fromProfileDetail['company_name']."</a>",
                                                    "Country"=>"<img src='".$fromProfile->getFlagUrl($fromProfileDetail['country'])."'/>".$fromProfileDetail['country_name'],
                                                    "Address"=>$fromProfileDetail['address'],
                                                    "Tel"=>$fromProfileDetail['tel'],
                                                    "Email"=>$fromProfileDetail['email']
                                        ));
            $send->setFromProfileImage($fromProfile->getPrimaryThumbUrl());

    		//var_dump($send);
    		$send->send();
            //Also save message in the databse
            $message->composeMessage($this->stock_detail['0']['owner'], $subjectInput, $messageInput, $this->stock_detail['0']['id'], $this->last_register_id);

    		// echo "dgasg".$send->viewMessage();
    		$this->result="success";
    		//Clear all inquiry after sending email
    		$this->clearTmpSession();
            return true;

        }

	}
    private function updateBuyerEmail($email){
        require_once BASE_CLASS . 'class-connect.php';
        $buyer_detail=array();
        $cnx = new Connect();
        $cnx->open();

        if(isset($_SESSION['log_id'])) $log_id=$_SESSION['log_id']; else $log_id='';
        $email=mysql_real_escape_string(stripslashes($email));


        $sql="UPDATE register SET email='$email' WHERE id='$log_id'";
        if(mysql_query($sql)){
            $_SESSION['log_email']=$email;
            return true;
        }
        $cnx->close();
    }
    function validate_email($email){
        $exp = "/^(([^()[\]\\.,;:\s@\”]+(\.[^()[\]\\.,;:\s@\”]+)*)|(\”.+\”))@(([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/";
        //condition 1: check id
        if(preg_match($exp,$email)){
            //condition 2: check email
            if(checkdnsrr(array_pop(explode("@",$email)),"MX")){
                return true;
            }
        }

	}
    private function loadCountryList(){
        require_once BASE_CLASS . 'class-connect.php';

        $cnx = new Connect();
        $cnx->open();

        if( !$sql = @mysql_query("SELECT * FROM `country_list` ORDER BY `country_name` ASC") ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load country list due a query error at ' . __FILE__ . ':' . __LINE__);

            $this->country_list = array();

            return;
        }

        if( @mysql_num_rows($sql) < 0 ){
            $cnx->close();
            return;
        }

        $this->country_list = array();

        while( $r = @mysql_fetch_assoc($sql) ){
            array_push($this->country_list, $r);

        }

        @mysql_free_result($sql);
        $cnx->close();
    }
    public function getCountryList(){
        return $this->country_list;

    }
	private function clearTmpSession(){
		if(isset($_SESSION['tmp_product_id'])) unset($_SESSION['tmp_product_id']);
		if(isset($_SESSION['tmp_subject_input'])) unset($_SESSION['tmp_subject_input']);
		if(isset($_SESSION['tmp_message_input'])) unset($_SESSION['tmp_message_input']);
	}
	private function resubmit(){

		if(isset($_GET['cid'])) $id=$_GET['cid']; else $id='';
		if(isset($_GET['action'])) $action=$_GET['action']; else $action='';
		if(isset($_SESSION['tmp_product_id'])) $tmp_id=$_SESSION['tmp_product_id']; else $tmp_id='';

		if($action=='submit'&&$id==$tmp_id){
			return true;
		}
	}

}
?>
