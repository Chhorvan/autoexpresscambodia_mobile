<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################

class publiccardetail {
    protected $lang;
    protected $form_status;
    protected $form_message;
    protected $form_style;
    protected $car_id;
    private   $car_information;
    private   $car_detail;
    private   $car_feature;
    private   $car_media;
    private   $body_type_list;
    private   $extras_list;

    /**
     * Constructor
     * <br>---------------------------------------------------------------------
     */
    public function publiccardetail($lang=array()){
        $this->lang = $lang;
        
        // include body type list.
        $path = BASE_ROOT . 'language/' . $_SESSION['log_language_iso'] . '/car-body-types.php';
        
        if( file_exists($path) ){
            require_once $path;
            $this->body_type_list = $_CAR_BODY;
        }
        else {
            $this->body_type_list = array();
        }
        
        // the car id.
        $this->car_id = $_GET['cid'];
        $this->car_id = addslashes($this->car_id);
        
        // load car information.
        $this->loadCarInfo();
    }
    
    /**
     * Private method: load car information.
     * <br>---------------------------------------------------------------------
     * @return void
     */
    private function loadCarInfo(){
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        // initialize vars.
        $this->car_detail = array();
        $this->car_feature = array();
        $this->car_information = array();
        $this->car_media = array();
        
        // load car data.
        if( !$sql = @mysql_query("SELECT * FROM `car` WHERE `id`='$this->car_id' AND `status`='0' LIMIT 1;") ){
            require_once BASE_CLASS . 'class-log.php';
            
            LogReport::write('Unable to query car information due a query error at ' . __FILE__ . ':' . __LINE__);
            
            $cnx->close();
            $this->form_message = $this->lang['CAR_DETAIL_CAR_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            
            return;
        }
        
        if( @mysql_num_rows($sql) != 1 ){
            $cnx->close();
            return;
        }
        
        $r = @mysql_fetch_assoc($sql);
        @mysql_free_result($sql);
        
        $this->car_information = array(
            'id' => $r['id'],
            'maker' => $r['maker'],
            'model' => $r['model'],
            'body_type' => $r['body_type'],
            'price' => $r['price'],
            'year' => $r['year'],
            'miles' => $r['miles'],
            'measure_type' => $r['measure_type'],
            'doors' => $r['doors'],
            'featured' => $r['featured'],
            'eco' => $r['eco']
        );
        
        // load car details.
        if( !$sql = @mysql_query("SELECT * FROM `car_details` WHERE `car_id`='$this->car_id' LIMIT 1;") ){
            require_once BASE_CLASS . 'class-log.php';
            
            LogReport::write('Unable to load car details due a query error at ' . __FILE__ . ':' . __LINE__);
            
            $this->form_message = $this->lang['CAR_DETAIL_CAR_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
        }
        else {
            if( @mysql_num_rows($sql) == 1 ){
                $r = @mysql_fetch_assoc($sql);
                @mysql_free_result($sql);
                
                $this->car_detail = array(
                    'id' => $r['id'],
                    'trim' => stripslashes($r['trim']),
                    'type' => stripslashes($r['type']),
                    'fuel' => stripslashes($r['fuel']),
                    'color' => stripslashes($r['color']),
                    'prev_owners' => $r['prev_owners'],
                    'last_service' => $r['last_service'],
                    'mot' => $r['mot'],
                    'tax_band' => stripslashes($r['tax_band']),
                    'top_speed' => $r['top_speed'],
                    'engine_torque_rpm' => $r['engine_torque_rpm'],
                    'engine_power_kw' => $r['engine_power_kw'],
                    'engine_size' => stripslashes($r['engine_size']),
                    'gear' => stripslashes($r['gear']),
                    'transmission_type' => $r['transmission_type'],
                    'html' => stripslashes($r['html'])
                );
            }
        }
        
        unset($r);
        
        // load car features.
        if( !$sql = @mysql_query("SELECT * FROM `car_features` WHERE `car_id`='$this->car_id' ORDER BY `feat_name` ASC") ){
            require_once BASE_CLASS . 'class-log.php';
            
            LogReport::write('Unable to load car features due a query error at ' . __FILE__ . ':' . __LINE__);
            
            $this->form_message = $this->lang['CAR_DETAIL_CAR_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
        }
        else {
            if( @mysql_num_rows($sql) > 0 ){
                while( $r = @mysql_fetch_assoc($sql) ){
                    $obj = array(
                        'id' => $r['id'],
                        'car_id' => $r['car_id'],
                        'feat_name' => stripslashes($r['feat_name']),
                        'feat_icon' => $r['feat_icon']
                    );

                    array_push($this->car_feature, $obj);
                }
                @mysql_free_result($sql);
            }
        }
        
        unset($obj);
        
        // load car media.
        if( !$sql = @mysql_query("SELECT * FROM `car_media` WHERE `car_id`='$this->car_id' ORDER BY `mode` ASC") ){
            require_once BASE_CLASS . 'class-log.php';
            
            LogReport::write('Unable to load car media due a query error at ' . __FILE__ . ':' . __LINE__);
            
            $this->form_message = $this->lang['CAR_DETAIL_CAR_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
        }
        else {
            if( @mysql_num_rows($sql) > 0 ){
                while( $r = @mysql_fetch_assoc($sql) ){
                    $obj = array(
                        'id' => $r['id'],
                        'car_id' => $r['car_id'],
                        'type' => $r['type'],
                        'mode' => $r['mode'],
                        'source' => $r['source']
                    );
                    
                    array_push($this->car_media,$obj);
                }
                @mysql_free_result($sql);
            }
        }
        
        unset($r);
        
        // load extras list.
        $maker = $this->car_information['maker'];
        $this->extras_list = array();
        
        if( !$sql = @mysql_query("SELECT `id`,`maker`,`model`,`price`,`year` FROM `car` WHERE `maker`='$maker' AND `status`='0' ORDER BY RAND() LIMIT 0,4;") ){
            require_once BASE_CLASS . 'class-log.php';
            
            LogReport::write('Unable to load car extras due a query error at ' . __FILE__ . ':' . __LINE__);
        } else {
            if( @mysql_num_rows($sql) > 0 ){
                while( $r = @mysql_fetch_assoc($sql) ){
                    $obj = array(
                        'id'    => $r['id'],
                        'maker' => stripslashes($r['maker']),
                        'model' => stripslashes($r['model']),
                        'price' => stripslashes($r['price']),
                        'year'  => stripslashes($r['year'])
                    );
                    array_push($this->extras_list, $obj);
                }
                
                @mysql_free_result($sql);
                
                // get 1 image for each entry.
                for( $i=0; $i < count($this->extras_list); $i++ ){
                    if( !$sql = @mysql_query("SELECT `car_id`,`type`,`source` FROM `car_media` WHERE `car_id`='".$this->extras_list[$i]['id']."' LIMIT 1;") ){
                        require_once BASE_CLASS . 'class-log.php';
            
                        LogReport::write('Unable to load car media due a query error at ' . __FILE__ . ':' . __LINE__); 
                    } else {
                        if( @mysql_num_rows($sql) == 1 ){
                            $r = @mysql_fetch_assoc($sql);
                            
                            @mysql_free_result($sql);
                            
                            $this->extras_list[$i]['image'] = $r['source'];
                        } else {
                            $this->extras_list[$i]['image'] = 'image/default_main_image.jpg';
                        }
                    }
                }
            }
        }
        
        unset($sql);
        unset($r);
        unset($obj);
        
        $cnx->close();
        return;
    }
    
    /**
     * Public method: get extra vehicles list
     * @return array
     */
    public function getExtrasList(){
        return $this->extras_list;
    }
    
    /**
     * Public method: get body type by id
     * @return string
     */
    public function getBodyTypeByID($id){
        if( count($this->body_type_list) < 1 ){
            return $this->lang['CAR_DETAIL_BODY_TYPE_UNKNOWN'];
        }
        
        $result = '';
        
        foreach($this->body_type_list as $k => $v ){
            if( $k == $id ){
                $result = $v;
                break;
            }
        }
        
        return $result;
    }
        
    /**
     * Public method: get car information
     * <br>---------------------------------------------------------------------
     * @return array
     */
    public function getCarInformation(){
        return $this->car_information;
    }
    
    /**
     * Public method: get car features
     * <br>---------------------------------------------------------------------
     * @return array
     */
    public function getCarFeatures(){
        return $this->car_feature;
    }
    
    /**
     * Public method: get car media
     * <br>---------------------------------------------------------------------
     * @return array
     */
    public function getCarMedia(){
        return $this->car_media;
    }
    
    /**
     * Public method: get car details
     * <br>---------------------------------------------------------------------
     * @return array
     */
    public function getCarDetails(){
        return $this->car_detail;
    }
    
    /**
     * Public method: get form status
     * <br>---------------------------------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }

    /**
     * Public method: get form message 
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /**
     * Public method: get form style
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }
}