<?php
if(!isset($_SESSION)) @session_start();

################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################

class publiclogin
{
    protected $lang;
    protected $error_status = false;
    protected $error_message;
    protected $error_type;
    protected $error_formname;
    protected $cdata; // holds the tid and ref passed to login page if accessed through
                      // the edit_ds page.
    var $redirect_url;
	var $invalid_log;
    protected $existedSocialAccount=false;
    /*
     * Constructor -------------------------------------------------------------
     */
    public function publiclogin($lang=array())
    {

        // if group session is set, take user to the home page. no double login!
        if( isset($_SESSION['log_group']) && $_SESSION['log_group'] != 'public'  && $_SESSION['log_group'] != 'admin' )
        {
            header("Location: " . BASE_RELATIVE);
            exit;
        }
        if( isset($_SESSION['log_group']) && $_SESSION['log_group'] == 'admin' )
        {
            header("Location: " . BASE_RELATIVE);
            exit;
        }

        // set language.
        $this->lang = $lang;

        // defined cdata.
        ( isset($_GET['pg']) ? $page_return = trim($_GET['pg']) : $page_return = 1 );
        ( isset($_GET['cid']) ? $car_id = (int)$_GET['cid'] : $car_id = '' );

        if( !empty($page_return) && $car_id > 0 ){
            $this->cdata = $page_return . '/?cid=' . $car_id;
        }
        else {
            $this->cdata = '';
        }

        // check if login form has been submitted ------------------------------
        if( isset($_POST['loginBtn']) )
        {
            $this->handleLoginSubmit();
        }


        // check if recover password form has been submitted -------------------
        if( isset($_POST['fpassBtn']) )
        {
            $this->handlePasswordRecoverySubmit();
        }

        /**** Social Network ****/

        if(isset($_GET['redirect'])) $this->redirect_url=urldecode($_GET['redirect']); else $this->redirect_url='http://angkorauto.com/';
        if(isset($_GET['provider'])){
            require_once BASE_CLASS .'class-social-network.php';
            $social_network=new socialnetwork();

            if(!$social_network->loggedInStatus($_GET['provider'])){
                $social_network->handleLoginPage($_GET['provider']);

            }else{

                $user_profile=$social_network->getUserProfile();


                //Execute if account is not existed
                if($this->handleLoginBySocialAccount($user_profile->identifier, $_GET['provider'])){
                    header("Location: ".$this->redirect_url);
                }
                if(!$this->existedSocialAccount){
                    if($this->handleRegisterSocialAccount($user_profile, $_GET['provider'])){
                        if($this->handleLoginBySocialAccount($user_profile->identifier, $_GET['provider'])){
                            header("Location: ".$this->redirect_url);
                        }
                    }
                }


            }
        }
        /**** End Social Network ****/
    }

    function handleRegisterSocialAccount($user_profile, $provider){
        require_once BASE_CLASS . 'class-connect.php';
        $db = new Connect();
        $db->open();
        $sql="INSERT INTO register (
                `provider`,
                `provider_uid`,
                `company_name`,
                `email`,
                `member_type1`,
                `group`,
                `creation`,
                `activated`,
                `register_type`,
                `address`)
            VALUES(
                '$provider',
                '$user_profile->identifier',
                '$user_profile->displayName',
                '$user_profile->email',
                'Buyer',
                'user',
                NOW(),
                '1',
                'm1',
                '$user_profile->address'

                )
        ";

        if(!mysql_query($sql)){

            return false;
        }else{
            if(mysql_affected_rows()<=0){
                return false;
            }else{
                return true;
            }
        }
        $db->close();
    }

    function handleLoginBySocialAccount($provider_uid, $provider){
        require_once BASE_CLASS . 'class-connect.php';
        $db = new Connect();
        $db->open();
        $sql="SELECT `user_id`, `group`, `member_type1`, `email`, `company_name`, `register_type` FROM register
                WHERE `provider_uid`='$provider_uid' AND `provider`='$provider';
        ";
        $user_detail=array();
        if(!$result=mysql_query($sql)){
            return false;
        }

        if(mysql_num_rows($result)>0){
            while($row=mysql_fetch_assoc($result)){
                array_push($user_detail, $row);
            }
            $_SESSION['log_id']=$user_detail['0']['user_id'];
            $_SESSION['log_group']=$user_detail['0']['group'];
            $_SESSION['member_type1']=$user_detail['0']['member_type1'];
            $_SESSION['log_email']=$user_detail['0']['email'];
            $_SESSION['company_name']=$user_detail['0']['company_name'];
            $_SESSION['register_type']=$user_detail['0']['register_type'];
            $_SESSION['provider_uid']=$user_detail['0']['provider_uid'];
            $_SESSION['provider']=$user_detail['0']['provider'];
            $this->existedSocialAccount=true;
            $db->close();
        }
        $db->close();
        return true;

    }

    /*
     * Protected method: handle login form submit ------------------------------
     * @return void.
     */
    protected function handleLoginSubmit()
    {
      $email = trim($_POST['emailInput']);
        $pass  = trim($_POST['passwordInput']);	
		
        if(isset($_GET['pg'])) $currentPage= urldecode($_GET['pg']); else $currentPage= BASE_RELATIVE;
        // validate.
       // require_once BASE_CLASS . 'class-utilities.php';
//
//        if( !Utilities::checkEmail($email) )
//        {
//            $this->error_status = true;
//            $this->error_type = 'error';
//            $this->error_message = $this->lang['LOGIN_INVALID_EMAIL'];
//            $this->error_formname = 'login';
//            return;
//        }
//
//        if( strlen($pass) < 5 || strlen($pass) > 20 )
//        {
//            $this->error_status = true;
//            $this->error_type = 'error';
//            $this->error_message = $this->lang['LOGIN_INVALID_EMAIL'];
//            $this->error_formname = 'login';
//            return;
//        }
        require_once BASE_CLASS . 'class-utilities.php';

       /* if( !Utilities::checkEmail($email) )
        {
            $this->error_status = true;
            $this->error_type = 'error';
            $this->error_message = $this->lang['LOGIN_INVALID_EMAIL'];
            $this->error_formname = 'login';
            return;
        }*/

        /*if( strlen($pass) >=4 || strlen($pass) <= 30 )
        {
            $this->error_status = true;
            $this->error_type = 'error';
            $this->error_message = $this->lang['LOGIN_INVALID_EMAIL'];
            $this->error_formname = 'login';
            return;
        }
		*/

        // check if email matches the super admin's.
        if( $email == ADMIN_CONTACT_EMAIL )
        {
            // validate admin password.
            require_once BASE_CLASS . 'class-connect.php';

            $db = new Connect();
            $db->open();

            $pass = md5($pass);
            $email = mysql_real_escape_string($email);
			
			

            if( !$sql = mysql_query("SELECT `admin_email`, `admin_pass` FROM `setting` WHERE `admin_email`='$email' AND `admin_pass`='$pass'") )
            {
                require_once BASE_CLASS . 'class-log.php';

                LogReport::write('Unable to query database at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());



                $this->error_status = true;
                $this->error_type = 'warning';
                $this->error_message = $this->lang['LOGIN_QUERY_ERROR'];
                $this->error_formname = 'login';
                return;
                 $db->close();
				

            }else{
                  header("Location: " . BASE_RELATIVE . $currentPage);
                exit;
            }

            if( @mysql_num_rows($sql) != 1 )
            {
                $db->close();

                $this->error_status = true;
                $this->error_type = 'error';
                $this->error_message = $this->lang['LOGIN_INVALID_EMAIL'];
                $this->error_formname = 'login';
                return;
            }
            else
            {
                // set admin session.
                $_SESSION['log_group'] = 'admin';
                $_SESSION['log_name']  = 'Administrator';
                $_SESSION['log_id']    = -1;
                $_SESSION['log_email'] = $email;

                header("Location: " . BASE_RELATIVE . $currentPage);
                exit;
            }
        }
        // user is not administrator.
        else
        {
            require_once BASE_CLASS . 'class-connect.php';

            $db = new Connect();
            $db->open();

            $pass = md5($pass);
            $email = mysql_real_escape_string($email);


            //echo $email."Test Email";
			
			/*
			SELECT r.*, tb_seller.seller_id, m.m_image FROM register r left join more_image m on r.user_id=m.owner inner join tbl_carfinder_sellers tb_seller on r.user_id=tb_seller.user_id WHERE `username`='$email' AND `password`='$pass' AND `activated`='1' and m.primary_photo='1' 
			*/
            if( !$sql = mysql_query("SELECT r.*, tb_seller.seller_id FROM register r inner join tbl_carfinder_sellers tb_seller on r.user_id=tb_seller.user_id WHERE `username`='$email' AND `password`='$pass' AND `activated`='1'") )
			
            {
                require_once BASE_CLASS . 'class-log.php';

                LogReport::write('Unable to query database at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());
                
                $db->close();

                $this->error_status = true;
                $this->error_type = 'warning';
                $this->error_message = $this->lang['LOGIN_QUERY_ERROR'];
                $this->error_formname = 'login';
                return;
            }
			
			if (mysql_num_rows($sql) == 0){
				$this->invalid_log = "Invalid ID or Password";
			}
            
			if( @mysql_num_rows($sql) != 1 )
            {
                $db->close();

                $this->error_status = true;
                $this->error_type = 'error';
                //$this->error_message = $this->lang['LOGIN_INVALID_EMAIL'];
                $this->error_formname = 'login';
                return;
            }

            // check if account is active and user is not banned.
            $r = mysql_fetch_assoc($sql);

            @mysql_free_result($sql);
            $db->close();

            //if( (bool)$r['banned'] )
//            {
//                $this->error_status = true;
//                $this->error_type = 'warning';
//                $this->error_message = $this->lang['LOGIN_BANNED_ACCOUNT'];
//                $this->error_formname = 'login';
//                return;
//            }
//
//            if( !(bool)$r['activated'] )
//            {
//                $this->error_status = true;
//                $this->error_type = 'warning';
//                $this->error_message = $this->lang['LOGIN_NON_ACTIVE_ACCOUNT'];
//                $this->error_formname = 'login';
//                return;
//            }
            //Check if user allow to remember the account
            if(isset($_POST['keep_login'])){
                if(empty($r['session'])){
                    if(empty($r['token'])){
                        $r['token']=strtotime("now");
                    }
                    $r['session']=md5($r['log_id']."^".$r['password']."^".$r['token']);
                    $this->updateSessionInDatabase($r['user_id'], $r['token'], $r['session']);
                }
                setcookie('ussid', $r['session'], strtotime( '+30 days' ) );
            }
			
			//Check Membertype and Create user login session
			if($r['member_type3']=="personal"){
				$_SESSION['user'] = $r['username'];
			}
			if($r['member_type3']=="seller"){
				$_SESSION['user'] = $r['username'];
			
			}
			
			if($r['member_type3']=="dealer"){
			  $_SESSION['user'] = $r['username'];
			}
			
            // set login session.
            $_SESSION['log_group'] = $r['group'];
			$_SESSION['seller_id'] = $r['seller_id'];
            $_SESSION['log_name']  = $r['name'];
			//$_SESSION['company_name']=$r[''];
			$_SESSION['log_id']    = $r['user_id'];
            $_SESSION['userlog_id']    = $r['log_id'];
            $_SESSION['log_email'] = $r['email'];
			//$_SESSION['log_image'] = $r['m_image'];
			$_SESSION['register_type'] = $r['register_type'];



			$db = new Connect();
            $db->open();
			
			
			//query picture profile for user
			
			$queryPicProfile = "SELECT * FROM more_image WHERE owner='".$_SESSION['log_id']."' AND primary_photo = '1' ";
			$resultPicProfile = mysql_query($queryPicProfile);						
			
			$rowPicProfile = mysql_fetch_array($resultPicProfile);
			
			$_SESSION['log_image'] = $rowPicProfile['m_image'];
			
			//query and set max uploading image for user
			$query="SELECT * FROM register_types WHERE register_types.code='".$_SESSION['register_type']."'";
			$result = mysql_query($query);						
			
			$row = mysql_fetch_array($result);

			$_SESSION['user_max_image_upload'] = $row['max_image'];
			$_SESSION['user_max_pro'] = $row['max_product'];
			//Script For Close Fancybox Pop up.
			/* echo '<script language="javascript">				
					parent.location.reload(true);			
			</script>'; */			

			
            header("Location: " .BASE_RELATIVE);
            exit;
        }
    }
    //update session field in table register
    protected function updateSessionInDatabase($id, $token, $session){
        require_once BASE_CLASS . 'class-connect.php';
        $db = new Connect();
        $db->open();
        $sql="UPDATE register SET `token`='$token', `session`='$session' WHERE `user_id`='$id'";
        if(mysql_query($sql)){
            return true;
        }
        $db->close();
    }
    /*
     * Protected method: handle password recovery form submit ------------------
     * @return void.
     */
    protected function handlePasswordRecoverySubmit()
    {
        $email = trim($_POST['femailInput']);

        require_once BASE_CLASS . 'class-utilities.php';

        // validate email.
        if( !Utilities::checkEmail($email) )
        {
            $this->error_status = true;
            $this->error_type = 'error';
            $this->error_message = $this->lang['LOGIN_INVALID_EMAIL_RECOVER'];
            $this->error_formname = 'recover';
            return;
        }

        require_once BASE_CLASS . 'class-connect.php';

        $db = new Connect();
        $db->open();

        // remove all older entries from recover table.
        if( $sql = @mysql_query("SELECT `stamp` FROM `recover_pass_request`") )
        {
            if( @mysql_num_rows($sql) > 0 )
            {
                // if stamp is older than 23 hours, remove it.
                while( $r = @mysql_fetch_assoc($sql) )
                {
                    $stamp = $r['stamp'];

                    if( $stamp <= strtotime('-23 hours') )
                    {
                        @mysql_query("DELETE FROM `recover_pass_request` WHERE `stamp`='$stamp' LIMIT 1;");
                    }
                }

                @mysql_free_result($sql);
            }
        }
        else
        {
            // do not break operation, let it fail silently and log the error.
            require_once BASE_CLASS . 'class-log.php';

            LogReport::write('Unable to remove old recover entries from database at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());
        }

        // check if email exists on database.
        if( !$sql = mysql_query("SELECT `email` FROM `register` WHERE `email`='$email'") )
        {
            require_once BASE_CLASS . 'class-log.php';

            LogReport::write('Unable to query database at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $db->close();

            $this->error_status = true;
            $this->error_type = 'warning';
            $this->error_message = $this->lang['LOGIN_QUERY_ERROR_RECOVER'];
            $this->error_formname = 'recover';
            return;
        }

        if( @mysql_num_rows($sql) != 1 )
        {
            $db->close();

            $this->error_status = true;
            $this->error_type = 'error';
            $this->error_message = $this->lang['LOGIN_INVALID_EMAIL_RECOVER'];
            $this->error_formname = 'recover';
            return;
        }

        // generate a validation code which will be part of the URL sent to user.
        $validation_code = Utilities::generateRandomString(21);

        // define URL.
        $changeURL = BASE_SITE . '/login/forgot-password/?auth=' . base64_encode($validation_code .',' . $email);

        // register update request.
        if( !$sql = @mysql_query("INSERT INTO `recover_pass_request` (`email`,`code`, `stamp`) VALUES ('$email','$validation_code','".time()."')") )
        {
            require_once BASE_CLASS . 'class-log.php';

            LogReport::write('Unable to query database at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $db->close();

            $this->error_status = true;
            $this->error_type = 'warning';
            $this->error_message = $this->lang['LOGIN_QUERY_ERROR_REG_RECOVER'];
            $this->error_formname = 'recover';
            return;
        }

        // define email message.
        $message = $this->lang['LOGIN_EMAIL_MESSAGE'];
        $message .= '<br><a href="'.BASE_RELATIVE.'home/forgot-password/?auth=' . base64_encode($validation_code .',' . $email) . '" target="_blank">' . $this->lang['LOGIN_RESET_LABEL'] . '</a><br>';

        // send email to user.
        require_once BASE_CLASS . 'class-sendmail.php';

        $send = new SendEmail();
        $send->setSubject(FRAMEWORK_NAME . ': ' . $this->lang['LOGIN_EMAIL_SUBJECT']);
        $send->setMessage($message);
        $send->setEmail($email);

        if( !$send->send() )
        {
            // remove entry from database.
            if( !@mysql_query("DELETE FROM `recover_pass_request` WHERE `code`='$validation_code' LIMIT 1;") )
            {

            }

            $db->close();

            require_once BASE_CLASS . 'class-log.php';

            LogReport::write('Unable to query database at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $db->close();

            $this->error_status = true;
            $this->error_type = 'warning';
            $this->error_message = $this->lang['LOGIN_QUERY_ERROR_EMAIL_RECOVER'];
            $this->error_formname = 'recover';
            return;
        }

        $db->close();

        $this->error_status = true;
        $this->error_type = 'complete';
        $this->error_message = $this->lang['LOGIN_EMAIL_COMPLETE'];
        $this->error_formname = 'recover';
        return;
    }

    /*
     * Getter: get form status -------------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->error_status;
    }

    /*
     * Getter: get form status type --------------------------------------------
     * @return string (error | warning | complete).
     */
    public function getFormStatusType()
    {
        return $this->error_type;
    }

    /*
     * Getter: get form status message -----------------------------------------
     * @return string.
     */
    public function getStatusMessage()
    {
        return $this->error_message;
    }

    /*
     * Getter: get form name ---------------------------------------------------
     * @return string.
     */
    public function getFormName()
    {
        return $this->error_formname;
    }
}
