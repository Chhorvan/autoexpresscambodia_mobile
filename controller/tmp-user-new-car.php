<?php
if(!isset($_SESSION)) @session_start();
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################

class usernewcar {
    protected $lang;
    protected $form_status;
    protected $form_message;
    protected $form_style;
    protected $maker_list;
    protected $model_list;
    protected $body_list;
    //protected $feature_icon;
	protected $country_list;
    protected $car_id;
	protected $drive_list;
	protected $vehicle_type_list;
	protected $car_maker_list;
	protected $fuel_type_list;
	protected $transmission_list;
	protected $category_list;
	protected $product_option;
    /**
     * Constructor
     * <br>---------------------------------------------------------------------
     */
    public function usernewcar($lang=array()){
        $this->lang = $lang;
        
        // define car id.
        require_once BASE_CLASS . 'class-utilities.php';
        $this->car_id = Utilities::generateRandomString(21);
                
        // load maker list.
        $this->loadMakerList();
        
        // load body type list.
        $this->loadBodyTypeList();
		
        // load Drive type list.
        $this->loadDriveTypeList();
        
        // load featured list.
       // $this->loadFeaturedList();
		// load product option list.
		$this->loadProductOptionList();
         // load country list.
		$this->loadCountryList();
		  // load car Maker list.
		$this->loadCarMakerList();
		// load car Maker list.
		$this->loadCategoryList();
		
		 // load car Maker list.
		$this->loadFuelTypeList();
		// load car type list.
		$this->loadCarTypeList();
		// load car type list.
		$this->loadTransmissionList();
        // handle save form.
        if( isset($_POST['savebtn']) ){
            $this->handleSaveForm();
        }
    }
    
    /**
     * Private method: handle save form
     * <br>---------------------------------------------------------------------
     * @return void.
     */
    private function handleSaveForm(){        
        // car id.
        $car_id             = trim($_POST['caridInput']);
		$primary_photo=$_POST['primary_photo'];
		var_dump($primary_photo);
		/// Enable only one product type
        $productType		= "Car";
		//$product_type		= "Truck";
		//$product_type		= "Bus";
		//$product_type		= "Equipment";
		//$product_type		= "Part";
		
		
		
		
		( isset($_POST['categorySelect'])       	? 	$categorySelect = trim($_POST['categorySelect']) : $categorySelect = '' );
		( isset($_POST['subCategoryGroupSelect'])   ? 	$subCategoryGroupSelect = trim($_POST['subCategoryGroupSelect']) : $subCategoryGroupSelect = '' );
		( isset($_POST['subCategorySelect'])       	? 	$subCategorySelect = trim($_POST['subCategorySelect']) : $subCategorySelect = '' );
		( isset($_POST['partNameInput'])       		? 	$partNameInput = trim($_POST['partNameInput'])  : $partNameInput = '' );
		( isset($_POST['conditionInput'])       	? 	$conditionInput = trim($_POST['conditionInput']) : $conditionInput = '' );
		( isset($_POST['unitWeightInput'])       	? 	$unitWeightInput = trim($_POST['unitWeightInput']) : $unitWeightInput = '' );
		( isset($_POST['partNumberInput'])       	? 	$partNumberInput = (int)($_POST['partNumberInput'])  : $partNumberInput = 0 );
		( isset($_POST['manufacturerInput'])       	? 	$manufacturerInput = trim($_POST['manufacturerInput']) : $manufacturerInput = '' );
		( isset($_POST['madeInInputSelect'])       	? 	$madeInInputSelect = trim($_POST['madeInInputSelect']) : $madeInInputSelect = '' );
		( isset($_POST['fittingMakeInput'])       	? 	$fittingMakeInput = trim($_POST['fittingMakeInput']) : $fittingMakeInput = '' );
		( isset($_POST['colorInput'])       		? 	$colorInput = trim($_POST['colorInput']) : $colorInput = '' );
		( isset($_POST['vehicleTypeSelect'])       	? 	$vehicleTypeSelect = trim($_POST['vehicleTypeSelect']) : $vehicleTypeSelect = '' );
		( isset($_POST['makerInputSelect'])       	? 	$makerInputSelect = $this->getMaker(trim($_POST['makerInputSelect']))  : $makerInputSelect = '' );
		( isset($_POST['makerInput'])       	? 	$makerInput = trim($_POST['makerInput'])  : $makerInput = '' );
		( isset($_POST['modelInputSelect'])     	? 	$modelInputSelect = trim($_POST['modelInputSelect'])  : $modelInputSelect = '' );
		( isset($_POST['modelInput'])     	? 	$modelInput = trim($_POST['modelInput'])  : $modelInput = '' );
		( isset($_POST['classInput'])       		? 	$classInput = trim($_POST['classInput'])  : $classInput = '' );
		( isset($_POST['chassisNoInput'])       		? 	$chassisNoInput = trim($_POST['chassisNoInput'])  : $chassisNoInput = '' );
		( isset($_POST['manDateInput'])       		? 	$manDateInput = trim($_POST['manDateInput'])   : $manDateInput = '' );
		( isset($_POST['yearInput'])       			? 	$yearInput = (int)($_POST['yearInput']) : $yearInput = 0 );
		( isset($_POST['milesInput'])       		? 	$milesInput = (int)($_POST['milesInput'])  : $milesInput = 0 );
		( isset($_POST['measureTypeInput'])       	? 	$measureTypeInput = trim($_POST['measureTypeInput'])  : $measureTypeInput = '' );
		( isset($_POST['engineVolumeInput'])       	? 	$engineVolumeInput = (int)($_POST['engineVolumeInput'])  : $engineVolumeInput = 0 );
		( isset($_POST['axleQtyInput'])       		? 	$axleQtyInput = (int)($_POST['axleQtyInput'])  : $axleQtyInput = 0 );
		( isset($_POST['steeringInput'])       		? 	$steeringInput = trim($_POST['steeringInput'])  : $steeringInput = '' );
		( isset($_POST['transmissionInputSelect'])  ? 	$transmissionInputSelect = trim($_POST['transmissionInputSelect'])               : $transmissionInputSelect = '' );
		( isset($_POST['carFuelInputSelect'])       ? 	$carFuelInputSelect = trim($_POST['carFuelInputSelect'])  : $carFuelInputSelect = '' );
		( isset($_POST['numberCargoInput'])       	? 	$numberCargoInput = (int)($_POST['numberCargoInput']) : $numberCargoInput = 0 );
		( isset($_POST['cargoBoxSizeInput'])       	? 	$cargoBoxSizeInput = trim($_POST['cargoBoxSizeInput']) : $cargoBoxSizeInput = '' );
		( isset($_POST['lengthInput'])       		? 	$lengthInput = trim($_POST['lengthInput']) : $lengthInput = '' );
		( isset($_POST['heightInput'])       		? 	$heightInput = trim($_POST['heightInput']) : $heightInput = '' );
		( isset($_POST['widthInput'])       		? 	$widthInput = trim($_POST['widthInput']) : $widthInput = '' );
		( isset($_POST['totalWeightInput'])       	? 	$totalWeightInput = trim($_POST['totalWeightInput']) : $totalWeightInput = '' );
		( isset($_POST['countryInputSelect'])       ? 	$countryInputSelect = trim($_POST['countryInputSelect']) : $countryInputSelect = '' );
		( isset($_POST['priceInput'])       		? 	$priceInput = (float)($_POST['priceInput']) : $priceInput = 0 );
		( isset($_POST['loadingWeightInput'])       ? 	$loadingWeightInput = trim($_POST['loadingWeightInput']) : $loadingWeightInput = '' );
		( isset($_POST['vinInput'])       			? 	$vinInput = trim($_POST['vinInput'])  : $vinInput = '' );
		( isset($_POST['numberPassengerInput'])     ? 	$numberPassengerInput = (int)($_POST['numberPassengerInput']) : $numberPassengerInput = 0 );
		
		( isset($_POST['doorInput'])     ? 	$doorInput = (int)($_POST['doorInput']) : $doorInput = 0 );
		( isset($_POST['bodyColorInput'])       	? 	$bodyColorInput = trim($_POST['bodyColorInput']) : $bodyColorInput = '' );
		( isset($_POST['enginePowerInput'])       	? 	$enginePowerInput = (int)($_POST['enginePowerInput']) : $enginePowerInput = 0 );
		( isset($_POST['numberCylinderInput'])      ? 	$numberCylinderInput = (int)($_POST['numberCylinderInput'])               : $numberCylinderInput = 0 );
		( isset($_POST['driveAvail'])       		? 	$driveAvail = trim($_POST['driveAvail']) : $driveAvail = 0 );
		( isset($_POST['driveInputSelect'])       	? 	$driveInputSelect = trim($_POST['driveInputSelect']) : $driveInputSelect = '' );
		( isset($_POST['paymentTermCheck'])       	? 	$paymentTermCheck = $this->getAllCheckbox('paymentTermCheck') : $paymentTermCheck = '' );
		( isset($_POST['otherOptionsCheck'])       	? 	$otherOptionsCheck = $this->getAllCheckbox('otherOptionsCheck') : $otherOptionsCheck = '' );
		( isset($_POST['safetyDeviceOptionsCheck'])       	? 	$safetyDeviceOptionsCheck = $this->getAllCheckbox('safetyDeviceOptionsCheck') : $safetyDeviceOptionsCheck = '' );
		( isset($_POST['exOptionsCheck'])       	? 	$exOptionsCheck = $this->getAllCheckbox('exOptionsCheck') : $exOptionsCheck = '' );
		( isset($_POST['inOptionsCheck'])       	? 	$inOptionsCheck = $this->getAllCheckbox('inOptionsCheck') : $inOptionsCheck = '' );
		
		
        
        
       
        
       ( isset($_POST['sellerCommentText'])       	? 	$sellerCommentText = trim($_POST['sellerCommentText'])  : $sellerCommentText = '' );
        
        // strip all slashes.
		
		$categorySelect          	= stripslashes($categorySelect);
		$subCategoryGroupSelect     = stripslashes($subCategoryGroupSelect);
		$subCategorySelect          = stripslashes($subCategorySelect);
		$partNameInput          	= stripslashes($partNameInput);
		$conditionInput          	= stripslashes($conditionInput);
		$unitWeightInput          	= stripslashes($unitWeightInput);
		$partNumberInput          	= stripslashes($partNumberInput);
		$manufacturerInput          = stripslashes($manufacturerInput);
		$madeInInputSelect          = stripslashes($madeInInputSelect);
		$fittingMakeInput          	= stripslashes($fittingMakeInput);
		$colorInput          		= stripslashes($colorInput);
		$vehicleTypeSelect          = stripslashes($vehicleTypeSelect);
		$makerInputSelect         	= stripslashes($makerInputSelect);
		$modelInputSelect        = stripslashes($modelInputSelect);
		$classInput          		= stripslashes($classInput);
		$chassisNoInput          		= stripslashes($chassisNoInput);
		$manDateInput          		= stripslashes($manDateInput);
		$yearInput          		= stripslashes($yearInput);
		$milesInput          		= stripslashes($milesInput);
		$measureTypeInput          	= stripslashes($measureTypeInput);
		$engineVolumeInput          = stripslashes($engineVolumeInput);
		$axleQtyInput         		= stripslashes($axleQtyInput);
		$steeringInput          	= stripslashes($steeringInput);
		$transmissionInputSelect    = stripslashes($transmissionInputSelect);
		$carFuelInputSelect         = stripslashes($carFuelInputSelect);
		$numberCargoInput          	= stripslashes($numberCargoInput);
		$cargoBoxSizeInput          = stripslashes($cargoBoxSizeInput);
		$lengthInput          		= stripslashes($lengthInput);
		$heightInput          		= stripslashes($heightInput);
		$widthInput          		= stripslashes($widthInput);
		$totalWeightInput          	= stripslashes($totalWeightInput);
		$countryInputSelect        	= stripslashes($countryInputSelect);
		$priceInput          		= stripslashes($priceInput);
		$loadingWeightInput         = stripslashes($loadingWeightInput);
		$vinInput          			= stripslashes($vinInput);
		$numberPassengerInput       = stripslashes($numberPassengerInput);
		$doorInput          		= stripslashes($doorInput);
		$bodyColorInput          	= stripslashes($bodyColorInput);
		$enginePowerInput          	= stripslashes($enginePowerInput);
		$numberCylinderInput        = stripslashes($numberCylinderInput);
		$driveAvail          		= stripslashes($driveAvail);
		$driveInputSelect          	= stripslashes($driveInputSelect);
		$paymentTermCheck          	= stripslashes($paymentTermCheck);  
		$otherOptionsCheck          = stripslashes($otherOptionsCheck);  
		$safetyDeviceOptionsCheck   = stripslashes($safetyDeviceOptionsCheck);  
		$exOptionsCheck          	= stripslashes($exOptionsCheck);  
		$inOptionsCheck          	= stripslashes($inOptionsCheck);  
		$sellerCommentText          = stripslashes($sellerCommentText);  
		
        $author						= (int)$_SESSION['log_id'];
		$owner					= (int)$_SESSION['log_id'];
		
        // validate required fields.
        /*if( empty($maker) && empty($maker_sel) ){
            $this->form_message = $this->lang['NEWCAR_INVALID_MAKER_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-error';
            $this->removeMedia($car_id);
            return;
        }
		*/
        // define default value.
        // step 1.
		
		
		

        
        if( empty($measure_type) ){ $measure_type = 'Km'; }
        if( empty($doorInput) ){ $doorInput = 1; }
       
        
        // step 3.

        // save.
        require_once BASE_CLASS . 'class-connect.php';
        $cnx = new Connect();
        $cnx->open();
		
        $categorySelect          	= @mysql_real_escape_string($categorySelect);
		$subCategoryGroupSelect     = @mysql_real_escape_string($subCategoryGroupSelect);
		$subCategorySelect          = @mysql_real_escape_string($subCategorySelect);
		$partNameInput          	= @mysql_real_escape_string($partNameInput);
		$conditionInput          	= @mysql_real_escape_string($conditionInput);
		$unitWeightInput          	= @mysql_real_escape_string($unitWeightInput);
		$partNumberInput          	= @mysql_real_escape_string($partNumberInput);
		$manufacturerInput          = @mysql_real_escape_string($manufacturerInput);
		$madeInInputSelect          = @mysql_real_escape_string($madeInInputSelect);
		$fittingMakeInput          	= @mysql_real_escape_string($fittingMakeInput);
		$colorInput          		= @mysql_real_escape_string($colorInput);
		$vehicleTypeSelect          = @mysql_real_escape_string($vehicleTypeSelect);
		$makerInputSelect         	= @mysql_real_escape_string($makerInputSelect);
		$makerInput         		= @mysql_real_escape_string($makerInput);
		$modelInputSelect        	= @mysql_real_escape_string($modelInputSelect);
		$modelInput        			= @mysql_real_escape_string($modelInput);
		$classInput          		= @mysql_real_escape_string($classInput);
		$manDateInput          		= @mysql_real_escape_string($manDateInput);
		$yearInput          		= @mysql_real_escape_string($yearInput);
		$milesInput          		= @mysql_real_escape_string($milesInput);
		$measureTypeInput          	= @mysql_real_escape_string($measureTypeInput);
		$engineVolumeInput          = @mysql_real_escape_string($engineVolumeInput);
		$axleQtyInput         		= @mysql_real_escape_string($axleQtyInput);
		$steeringInput          	= @mysql_real_escape_string($steeringInput);
		$transmissionInputSelect    = @mysql_real_escape_string($transmissionInputSelect);
		$carFuelInputSelect         = @mysql_real_escape_string($carFuelInputSelect);
		$numberCargoInput          	= @mysql_real_escape_string($numberCargoInput);
		$cargoBoxSizeInput          = @mysql_real_escape_string($cargoBoxSizeInput);
		$lengthInput          		= @mysql_real_escape_string($lengthInput);
		$heightInput          		= @mysql_real_escape_string($heightInput);
		$widthInput          		= @mysql_real_escape_string($widthInput);
		$totalWeightInput          	= @mysql_real_escape_string($totalWeightInput);
		$countryInputSelect        	= @mysql_real_escape_string($countryInputSelect);
		$priceInput          		= @mysql_real_escape_string($priceInput);
		$loadingWeightInput         = @mysql_real_escape_string($loadingWeightInput);
		$vinInput          			= @mysql_real_escape_string($vinInput);
		$numberPassengerInput       = @mysql_real_escape_string($numberPassengerInput);
		$doorInput          		= @mysql_real_escape_string($doorInput);
		$bodyColorInput          	= @mysql_real_escape_string($bodyColorInput);
		$enginePowerInput          	= @mysql_real_escape_string($enginePowerInput);
		$numberCylinderInput        = @mysql_real_escape_string($numberCylinderInput);
		$driveAvail          		= @mysql_real_escape_string($driveAvail);
		$driveInputSelect          	= @mysql_real_escape_string($driveInputSelect); 
		$paymentTermCheck          	= @mysql_real_escape_string($paymentTermCheck); 
		$otherOptionsCheck          = @mysql_real_escape_string($otherOptionsCheck);  
		$safetyDeviceOptionsCheck   = @mysql_real_escape_string($safetyDeviceOptionsCheck);  
		$exOptionsCheck          	= @mysql_real_escape_string($exOptionsCheck);  
		$inOptionsCheck          	= @mysql_real_escape_string($inOptionsCheck);  
		$sellerCommentText          = @mysql_real_escape_string($sellerCommentText);
		$chassisNoInput          	= @mysql_real_escape_string($chassisNoInput);  
        // save to car table.
		
		if(!empty($makerInput)){
			$makerInputSelect=$makerInput;
		}
		if(!empty($modelInput)){
			$modelInputSelect=$modelInput;
		}
		
		$sqlUpdate="UPDATE `product` SET `category` = '$categorySelect',
                                                `sub_category_group`  = '$subCategoryGroupSelect',
                                                `sub_category`  = '$subCategorySelect',
                                                `part_name`  = '$partNameInput',
                                                `condition`  = '$conditionInput',
                                                `unit_weight`  = '$unitWeightInput',
                                                `part_number` = '$partNumberInput',
                                                `manufacturer` = '$manufacturerInput',
                                                `made_in` = '$madeInInputSelect',
                                                `fitting_make` = '$fittingMakeInput',
                                                `color` = '$colorInput',
                                                `vehicle_type` = '$vehicleTypeSelect',
                                                `make` = '$makerInputSelect',
                                                `model` = '$modelInputSelect',
                                                `class` = '$classInput',
												`chassis_no` = '$chassisNoInput',
                                                `manufacturer_date` = '$manDateInput',
                                                `model_year` = '$yearInput',
                                                `mileage` = '$milesInput',
                                                `mileage_type` = '$measureTypeInput',
                                                `engine_volume` = '$engineVolumeInput',
                                                `axle_qty` = '$axleQtyInput',
                                                `steering` = '$steeringInput',
                                                `transmission` = '$transmissionInputSelect',
                                                `fuel_type` = '$carFuelInputSelect',
											   `number_cargo` = '$numberCargoInput',
											   `cargo_box_size` = '$cargoBoxSizeInput',
											   `length` =  '$lengthInput',
											   `height` = '$heightInput',
											   `width` =  '$widthInput',
											   `total_weight` = '$totalWeightInput',
											   `location` = '$countryInputSelect',
											   `price` = '$priceInput',
											   `loading_weight` = '$loadingWeightInput',
											   `vin_number` =  '$vinInput',
											   `number_passenger` = '$numberPassengerInput',
											   `door` = '$doorInput',
											   `exterior_color` = '$bodyColorInput',
											   `engine_power` = '$enginePowerInput',
											   `number_cylinder` = '$numberCylinderInput',
											   `drive_availability` = '$driveAvail',
											   `drive_type` = '$driveInputSelect',
               								   `payment_terms` = '$paymentTermCheck',
											   `created_date` = NOW(),
                                               `other_options` = '$otherOptionsCheck',
											   `safety_device_options` = '$safetyDeviceOptionsCheck ',
											   `exterior_options` = '$exOptionsCheck',
											   `interior_options` = '$inOptionsCheck',
											   `seller_comment` = '$sellerCommentText'
												WHERE `id`='$car_id' AND `owner`='$owner';
                                           ";
		
		$sqlInsert="INSERT INTO `product` (
                                                `id`,
												`owner`,
												`category`,
                                                `sub_category_group`,
                                                `sub_category`,
                                                `part_name`,
                                                `condition`,
                                                `unit_weight`,
                                                `part_number`,
                                                `manufacturer`,
                                                `made_in`,
                                                `fitting_make`,
                                                `color`,
                                                `vehicle_type`,
                                                `make`,
                                                `model`,
                                                `class`,
												`chassis_no`,
                                                `manufacturer_date`,
                                                `model_year`,
                                                `mileage`,
                                                `mileage_type`,
                                                `engine_volume`,
                                                `axle_qty`,
                                                `steering`,
                                                `transmission`,
                                                `fuel_type`,
											   `number_cargo`,
											   `cargo_box_size`,
											   `length`,
											   `height`,
											   `width`,
											   `total_weight`,
											   `location`,
											   `price`,
											   `loading_weight`,
											   `vin_number`,
											   `number_passenger`,
											   `door`,
											   `exterior_color`,
											   `engine_power`,
											   `number_cylinder`,
											   `drive_availability`,
											   `drive_type`,
               								   `payment_terms`,
											   `created_date`,
                                               `other_options`,
											   `safety_device_options`,
											   `exterior_options`,
											   `interior_options`,
											   `seller_comment`,
											   `product_type`
                                             ) VALUES (
                                                '$car_id',
												'$owner',
												'$categorySelect',
												'$subCategoryGroupSelect',
												'$subCategorySelect',
												'$partNameInput',
												'$conditionInput',
												'$unitWeightInput',
												'$partNumberInput',
												'$manufacturerInput',
												'$madeInInputSelect',
												'$fittingMakeInput',
												'$colorInput',
												'$vehicleTypeSelect',
												'$makerInputSelect',
												'$modelInputSelect',
												'$classInput',
												'$chassisNoInput',
												'$manDateInput',
												'$yearInput',
												'$milesInput',
												'$measureTypeInput',
												'$engineVolumeInput',
												'$axleQtyInput',
												'$steeringInput',
												'$transmissionInputSelect',
												'$carFuelInputSelect',
												'$numberCargoInput',
                                                '$cargoBoxSizeInput',
                                                '$lengthInput',
                                                '$heightInput',
                                                '$widthInput',
                                                '$totalWeightInput',
                                                '$countryInputSelect',
                                                '$priceInput',
                                                '$loadingWeightInput',
                                                '$vinInput',
                                                '$numberPassengerInput',
                                                '$doorInput',
                                                '$bodyColorInput',
                                                '$enginePowerInput',
                                                '$numberCylinderInput',
                                                '$driveAvail',
                                                '$driveInputSelect',
                                                '$paymentTermCheck',
												NOW(),
												'$otherOptionsCheck',
												'$safetyDeviceOptionsCheck ',
												'$exOptionsCheck',
												'$inOptionsCheck',
												'$sellerCommentText',
												'$productType'
                                             )";
		//$sql=$sqlInsert;
		if(!$this->existedProduct($car_id)){ 
			$sql=$sqlInsert;
		}else{ 
			$sql=$sqlUpdate;
			
		}
		
        if( !@mysql_query($sql) ){
            require_once BASE_CLASS . 'class-log.php';
            $cnx->close();
			
            LogReport::write('Unable to save to car table due a query error at ' . __FILE__ . ':' . __LINE__);
            $this->form_message = $this->lang['NEWCAR_SAVE_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            $this->removeMedia($car_id);
            return;
        }
        
        // save to featured table.
        //for( $i=0; $i < count($feat); $i++ ){
//            
//            if( isset($feat[$i]) ){
//                $val = explode('[:]',$feat[$i]);
//                $name = stripslashes($val[0]);
//                $icon = $val[1];
//
//                if( !empty($name) && !empty($icon) ){
//                    if( !@mysql_query("INSERT INTO `car_features` (`car_id`, `author`,`feat_name`,`feat_icon`) VALUES ('$car_id', $author,'$name','$icon')") ){
//                        require_once BASE_CLASS . 'class-log.php';
//                        LogReport::write('Unable to save to car_features table due a query error at ' . __FILE__ . ':' . __LINE__);
//                    }
//                }
//            }
//        }
        
        // save deatils table.
       /* if( !@mysql_query("INSERT INTO `car_details` (
                                                     `car_id`,
													 `author`,
                                                     
                                                     `html`
                                                     ) VALUES (
                                                     '$car_id',
													 '$author',
                                                     
                                                     '$html'
                                                     )") ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to save to car_details table due a query error at ' . __FILE__ . ':' . __LINE__);
        }*/
        
        $cnx->close();
        
        $this->form_message =  $this->lang['NEWCAR_SAVE_QUERY_SUCCESS'];
        $this->form_status = true;
        $this->form_style = 'alert-success';
    }
    public function getAllProducts($cid){
		$product=array();
		
		require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        if( !$sql = @mysql_query("SELECT * FROM `product` WHERE `id`='$cid';") ){
            require_once BASE_CLASS . 'class-log.php';
            
            $cnx->close();
            return;
        }
        
        if( @mysql_num_rows($sql) < 1 ){
            $cnx->close();
            return;
        }
        
        
        while( $r = @mysql_fetch_assoc($sql) ){
           	foreach($r as $column=>$value) {
				$product[$column]=$value;
			}
        }
        return $product;
        @mysql_free_result($sql);
        $cnx->close();
      	
	
	}
    /**
     * Private method: remove current vehicle's media
     * <br>---------------------------------------------------------------------
     * @return void
     */
    private function removeMedia($carid){
        if( empty($carid) ){
            return;
        }
        $author = (int)$_SESSION['log_id'];
        require_once BASE_CLASS . 'class-connect.php';
        $cnx = new Connect();
        $cnx->open();
        
        if( $sql = @mysql_query("SELECT `car_id`,`source` FROM `car_media` WHERE `car_id`='$carid' AND `author`=$author") ){
            if( @mysql_num_rows($sql) > 0 ){
                while( $r=@mysql_fetch_assoc($sql) ){
                    @unlink(BASE_ROOT . $r['source']);
                }
                @mysql_free_result($sql);
            }
        }
        
        @mysql_query("DELETE FROM `car_media` WHERE `car_id`='$carid' AND `author`=$author");
        
        $cnx->close();
    }
    
    /**
     * Private method: load feature icons
     * @return array
     */
    /*private function loadFeaturedList(){
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        if( !$sql = @mysql_query("SELECT * FROM `feature_list` ORDER BY `label` ASC") ){
            require_once BASE_CLASS . 'class-log.php';
            
            LogReport::write('Unable to load feature icon list due a query error at ' . __FILE__ . ':' . __LINE__);
            
            $this->feature_icon = array();
            
            return;
        }
        
        if( @mysql_num_rows($sql) < 0 ){
            $cnx->close();
            return;
        }
        
        $this->feature_icon = array();
        
        while( $r = @mysql_fetch_assoc($sql) ){
            $obj = array(
                'id' => $r['id'],
                'label' => stripslashes($r['label']),
                'source' => $r['source']
            );
            
            array_push($this->feature_icon,$obj);
        }
        
        @mysql_free_result($sql);
        $cnx->close();
    }*/
     /**
     * Private method: load option list
     * @return array
     */
	private function existedProduct($cid){
		
		require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        if( !$sql = @mysql_query("SELECT * FROM `product` WHERE `id`='$cid';") ){
            require_once BASE_CLASS . 'class-log.php'; 
            $cnx->close();
            return false;
        }
        
        if( @mysql_num_rows($sql) >0  ){
            return true;
        }

        @mysql_free_result($sql);
        
      	
	}
    private function loadProductOptionList(){
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        if( !$sql = @mysql_query("SELECT * FROM `product_option` ORDER BY `option_name` ASC") ){
            require_once BASE_CLASS . 'class-log.php';
            
            LogReport::write('Unable to load option list due a query error at ' . __FILE__ . ':' . __LINE__);
            
            
            return;
        }
        
        if( @mysql_num_rows($sql) < 0 ){
            $cnx->close();
            return;
        }
        
        
        $this->product_option=array();
        while( $r = @mysql_fetch_assoc($sql) ){
           $this->product_option[$r['option_type']][] = $r['option_name'];
        }
        
        @mysql_free_result($sql);
        $cnx->close();
    }
    /**
     * Private method: load body type list
     * <br>---------------------------------------------------------------------
     * @return array
     */
    private function loadBodyTypeList(){
        $path = BASE_ROOT . 'language/' . $_SESSION['log_language_iso'] . '/car-body-types.php';
        
        if( file_exists($path) ){
            require_once $path;
            $this->body_list = $_CAR_BODY;
        }
        else {
            $this->body_list = array();
        }
    }
	/**
     * Private method: load drive type list
     * <br>---------------------------------------------------------------------
     * @return array
     */
    private function loadDriveTypeList(){
        $path = BASE_ROOT . 'language/' . $_SESSION['log_language_iso'] . '/car-drive-types.php';
        
        if( file_exists($path) ){
            require_once $path;
            $this->drive_list = $_CAR_DRIVE;
        }
        else {
            $this->drive_list = array();
        }
    }
    /**
     * Private method: load country list
     * <br>---------------------------------------------------------------------
     * @return array
     */
	private function loadCountryList(){
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        if( !$sql = @mysql_query("SELECT * FROM `country_list` ORDER BY `country_name` ASC") ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load country list due a query error at ' . __FILE__ . ':' . __LINE__);
            
            $this->country_list = array();
            
            return;
        }
       
        if( @mysql_num_rows($sql) < 0 ){
            $cnx->close();			
            return;
        }
        
        $this->country_list = array();
        
        while( $r = @mysql_fetch_assoc($sql) ){
            $obj['cc'][] = $r['cc'];
            $obj['country_name'][] = stripslashes($r['country_name']);
           
            
            $this->country_list=$obj;
        }
       
        @mysql_free_result($sql);
        $cnx->close();
    }
	/**
     * Private method: load car maker list
     * <br>---------------------------------------------------------------------
     * @return array
     */
	private function loadCarMakerList(){
        require_once BASE_CLASS . 'class-connect.php';
        $product_type='Car';
        $cnx = new Connect();
        $cnx->open();
        
        if( !$sql = @mysql_query("SELECT * FROM `product_maker` WHERE product_type='$product_type' ORDER BY `id` ASC") ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load car maker list due a query error at ' . __FILE__ . ':' . __LINE__);
            
            $this->car_maker_list = array();
            
            return;
        }
       
        if( @mysql_num_rows($sql) < 0 ){
            $cnx->close();			
            return;
        }
        
        $this->car_maker_list = array();
        
        while( $r = @mysql_fetch_assoc($sql) ){
            $obj['id'][] = $r['id'];
            $obj['maker'][] = stripslashes($r['maker']);
           
            $this->car_maker_list=$obj;
        }
       
        @mysql_free_result($sql);
        $cnx->close();
    }
	/**
     * Private method: load fuel type list
     * <br>---------------------------------------------------------------------
     * @return array
     */
	private function loadFuelTypeList(){
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        if( !$sql = @mysql_query("SELECT * FROM `fuel_list` ORDER BY `id` ASC") ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load fuel type list due a query error at ' . __FILE__ . ':' . __LINE__);
            
            $this->fuel_type_list = array();
            
            return;
        }
       
        if( @mysql_num_rows($sql) < 0 ){
            $cnx->close();			
            return;
        }
        
        $this->fuel_type_list = array();
        
        while( $r = @mysql_fetch_assoc($sql) ){
            $obj['id'][] = $r['id'];
            $obj['fuel_name'][] = stripslashes($r['fuel_name']);
           
            $this->fuel_type_list=$obj;
        }
       
        @mysql_free_result($sql);
        $cnx->close();
    }
	/**
     * Private method: load transmission list
     * <br>---------------------------------------------------------------------
     * @return array
     */
	private function loadTransmissionList(){
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        if( !$sql = @mysql_query("SELECT * FROM `transmission_list` ORDER BY `id` ASC") ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load transmission list due a query error at ' . __FILE__ . ':' . __LINE__);
            
            $this->transmission_list = array();
            
            return;
        }
       
        if( @mysql_num_rows($sql) < 0 ){
            $cnx->close();			
            return;
        }
        
        $this->transmission_list = array();
        
        while( $r = @mysql_fetch_assoc($sql) ){
            $obj['id'][] = $r['id'];
            $obj['transmission_name'][] = stripslashes($r['transmission_name']);
           
            $this->transmission_list=$obj;
        }
       
        @mysql_free_result($sql);
        $cnx->close();
    }
	/**
     * Private method: load car type list
     * <br>---------------------------------------------------------------------
     * @return array
     */
	private function loadCarTypeList(){
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        if( !$sql = @mysql_query("SELECT * FROM `vehicle_types` ORDER BY `type_name` ASC") ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load car type list due a query error at ' . __FILE__ . ':' . __LINE__);
            
            $this->vehicle_type_list = array();
            
            return;
        }
       
        if( @mysql_num_rows($sql) < 0 ){
            $cnx->close();			
            return;
        }
        
        $this->vehicle_type_list = array();
        
        while( $r = @mysql_fetch_assoc($sql) ){
            $obj['id'][] = $r['id'];
            $obj['type_name'][] = stripslashes($r['type_name']);
            $this->vehicle_type_list=$obj;
        }
       
        @mysql_free_result($sql);
        $cnx->close();
    }
    /**
     * Private method: load maker unique values list
     * <br>---------------------------------------------------------------------
     * @return void
     */
    private function loadMakerList(){
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        if( !$sql = @mysql_query("SELECT `maker`,`model` FROM `car` ORDER BY `maker` ASC") ){
            require_once BASE_CLASS . 'class-log.php';
            
            $cnx->close();
            
            LogReport::write('Unable to load maker and model list due a query error at ' . __FILE__ . ':' . __LINE__);
            
            $this->form_message = $this->lang['NEWCAR_MAKER_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        
        if( @mysql_num_rows($sql) < 1 ){
            $cnx->close();
            return;
        }
        
        $this->maker_list = array();
        $this->model_list = array();
        
        while( $r = @mysql_fetch_assoc($sql) ){
            array_push($this->maker_list, stripslashes($r['maker']));
            array_push($this->model_list, stripslashes($r['model']));
        }
        
        @mysql_free_result($sql);
        $cnx->close();
        
        $this->maker_list = array_unique($this->maker_list);
        $this->maker_list = array_values($this->maker_list);
        $this->model_list = array_unique($this->model_list);
        $this->model_list = array_values($this->model_list);
    }
    
	/**
     * Private method: load category list
     * <br>---------------------------------------------------------------------
     * @return array
     */
	private function loadCategoryList(){
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        if( !$sql = @mysql_query("SELECT DISTINCT `category_name`, `product_type` FROM `category` ORDER BY `category_name` ASC") ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load category list due a query error at ' . __FILE__ . ':' . __LINE__);
            
            $this->category_list = array();
            
            return;
        }
       
        if( @mysql_num_rows($sql) < 0 ){
            $cnx->close();			
            return;
        }
        
        $this->category_list = array();
        
        while( $r = @mysql_fetch_assoc($sql) ){
            $this->category_list[$r['product_type']][] = $r['category_name'];
            //echo $r['category_name'];
        }
       
        @mysql_free_result($sql);
        $cnx->close();
    }
    /**
     * Public method: get car id
     * <br>---------------------------------------------------------------------
     * @return string The generated car id
     */
    public function getCarId(){
        return $this->car_id;
    }
	//get all checkbox data
	
    public function getAllCheckbox($chkboxName, $joiner='^'){
		$chkbox = $_POST[$chkboxName];
		for( $i = 0; $i < sizeof( $chkbox ); $i++ ){
			$chkbox[$i] = substr( $chkbox[$i], 0, 50 );
		}
		$string = implode("^", $chkbox);
		
		return $string;
	}
	//get car make name
	
    public function getMaker($maker_id){
		require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        if( !$sql = @mysql_query("SELECT `maker` FROM `product_maker` WHERE `id`='$maker_id';") ){
            require_once BASE_CLASS . 'class-log.php';
            
            $cnx->close();
            return $maker_id;
        }
        
        if( @mysql_num_rows($sql) < 1 ){
            $cnx->close();
            return;
        }
        
        
        while( $r = @mysql_fetch_assoc($sql) ){
           	return $r['maker']; 
        }
        
        @mysql_free_result($sql);
        $cnx->close();
      
	}
    /**
     * Public method: get feature list
     * <br>---------------------------------------------------------------------
     * @return array
     */
   /* public function getFeatureIconList(){
        return $this->feature_icon;
    }*/
	/**
     * Public method: get option list
     * <br>---------------------------------------------------------------------
     * @return array
     */
    public function getProductOptionList(){
        return $this->product_option;
    }
	/**
     * Public method: get car maker list
     * <br>---------------------------------------------------------------------
     * @return array
     */
	public function getCarMakerList(){
        return $this->car_maker_list;
    }
	/**
     * Public method: get category list
     * <br>---------------------------------------------------------------------
     * @return array
     */
	public function getCategoryList(){
        return $this->category_list;
    }
    /**
     * Public method: get country list
     * <br>---------------------------------------------------------------------
     * @return array
     */
	public function getCountryList(){
        return $this->country_list;
    }
	/**
     * Public method: get car type list
     * <br>---------------------------------------------------------------------
     * @return array
     */
	public function getVehicleTypeList(){
        return $this->vehicle_type_list;
    }
    /**
     * Public method: get body type list
     * <br>---------------------------------------------------------------------
     * @return array
     */
    public function getBodyList(){
        return $this->body_list;
    }
    /**
     * Public method: get body type list
     * <br>---------------------------------------------------------------------
     * @return array
     */
    public function getDriveList(){
        return $this->drive_list;
    }
    /**
     * Public method: get maker unique values list
     * <br>---------------------------------------------------------------------
     * @return array
     */
    public function getMakerList(){
        return $this->maker_list;
    }
    /**
     * Public method: get fuel unique values list
     * <br>---------------------------------------------------------------------
     * @return array
     */
    public function getFuelTypeList(){
        return $this->fuel_type_list;
    }
    /**
     * Public method: get transmission values list
     * <br>---------------------------------------------------------------------
     * @return array
     */
    public function getTransmissionList(){
        return $this->transmission_list;
    }
    /**
     * Public method: get model unique values list
     * <br>---------------------------------------------------------------------
     * @return array
     */
    public function getModelList(){
        return $this->model_list;
    }
    
    /**
     * Public method: get form status
     * <br>---------------------------------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }

    /**
     * Public method: get form message 
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /**
     * Public method: get form style
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }
}