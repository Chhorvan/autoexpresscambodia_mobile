<?php

if(!isset($_SESSION)) @session_start();

################################################################################

### This disclaimer must be kept intact in order to use this product.        ###

################################################################################

### Project: jT CarFramework [http://intersofts.com]

### Author: J. Toshio Taniguchi

### Since: 27.04.2011

### Version: 1.0.0

### Copyrights: J. Toshio Taniguchi

### Contact: j.taniguchi@taniguchi-blog.com

################################################################################

### CONTRIBUTORS

### - none

################################################################################



class adminnewcar {

    protected $lang;

    protected $form_status;

    protected $form_message;

    protected $form_style;

    protected $maker_list;

    protected $model_list;

    protected $body_list;

    protected $feature_icon;

    protected $car_id;



    /**

     * Constructor

     * <br>---------------------------------------------------------------------

     */

    public function adminnewcar($lang=array()){

        $this->lang = $lang;

        

        // define car id.

        require_once BASE_CLASS . 'class-utilities.php';

        $this->car_id = Utilities::generateRandomString(21);

                

        // load maker list.

        $this->loadMakerList();

        

        // load body type list.

        $this->loadBodyTypeList();

        

        // load featured list.

        $this->loadFeaturedList();

        

        // handle save form.

        if( isset($_POST['savebtn']) ){

            $this->handleSaveForm();

        }

    }

    

    /**

     * Private method: handle save form

     * <br>---------------------------------------------------------------------

     * @return void.

     */

    private function handleSaveForm(){        

        // car id.

        $car_id             = trim($_POST['caridInput']);

        

        // step 1

        ( isset($_POST['makerInput'])       ? $maker = trim($_POST['makerInput'])               : $maker = '' );

        ( isset($_POST['makerInputSelect']) ? $maker_sel = trim($_POST['makerInputSelect'])     : $maker_sel = '' );

        ( isset($_POST['modelInput'])       ? $model = trim($_POST['modelInput'])               : $model = '' );

        ( isset($_POST['modelInputSelect']) ? $model_sel = trim($_POST['modelInputSelect'])     : $model_sel = '' );

        ( isset($_POST['bodyInput'])        ? $body = trim($_POST['bodyInput'])                 : $body = '' );

        ( isset($_POST['bodyInputSelect'])  ? $body_sel = trim($_POST['bodyInputSelect'])       : $body_sel = '' );

        ( isset($_POST['thousandInput'])    ? $price_thousand = (int)$_POST['thousandInput']    : $price_thousand = 0 );

        ( isset($_POST['hundredInput'])     ? $price_hundred = (int)$_POST['hundredInput']      : $price_hundred = 0 );

        ( isset($_POST['decimalInput'])     ? $price_decimal = (int)$_POST['decimalInput']      : $price_decimal = 0 );

        ( isset($_POST['yearInput'])        ? $year = (int)$_POST['yearInput']                  : $year = 0 );

        ( isset($_POST['milesInput'])       ? $miles = (int)$_POST['milesInput']                : $miles = 0 );

        ( isset($_POST['measureTypeInput']) ? $measure_type = trim($_POST['measureTypeInput'])  : $measure_type = '' );

        ( isset($_POST['doorInput'])        ? $doors = (int)$_POST['doorInput']                 : $doors = 0 );

        ( isset($_POST['featuredInput'])    ? $featured = (bool)$_POST['featuredInput']         : $featured = false );

        ( isset($_POST['energyInput'])      ? $energy = trim($_POST['energyInput'])             : $energy = '' );

        

        // step 2

        ( isset($_POST['feat'])             ? $feat = (array)$_POST['feat'] : $feat = array() );

        

        // step 3

        ( isset($_POST['engineSizeInput'])  ? $engine_size = trim($_POST['engineSizeInput'])    : $engine_size = '' );

        ( isset($_POST['trimInput'])        ? $trim = trim($_POST['trimInput'])                 : $trim = '' );

        ( isset($_POST['typeInput'])        ? $type = trim($_POST['typeInput'])                 : $type = '' );

        ( isset($_POST['gearInput'])        ? $gear = trim($_POST['gearInput'])                 : $gear = '' );

        ( isset($_POST['transmissionInput']) ? $transmission = trim($_POST['transmissionInput']) : $transmission = '' );

        ( isset($_POST['fuelInput'])        ? $fuel = trim($_POST['fuelInput'])                 : $fuel = '' );

        ( isset($_POST['bodyColorInput'])   ? $color = trim($_POST['bodyColorInput'])           : $color = '' );

        ( isset($_POST['nrOwnerInput'])     ? $prev_owners = (int)$_POST['nrOwnerInput']        : $prev_owners = 0 );

        ( isset($_POST['lastServiceInput']) ? $last_service = trim($_POST['lastServiceInput'])  : $last_service = '' );

        ( isset($_POST['motInput'])         ? $mot = trim($_POST['motInput'])                   : $mot = '' );

        ( isset($_POST['taxInput'])         ? $tax_label = trim($_POST['taxInput'])             : $tax_label = '' );

        ( isset($_POST['topSpeedInput'])    ? $top_speed = trim($_POST['topSpeedInput'])        : $top_speed = '' );

        ( isset($_POST['torqueInput'])      ? $torque = trim($_POST['torqueInput'])             : $torque = '' );

        ( isset($_POST['kwInput'])          ? $kw = trim($_POST['kwInput'])                     : $kw = '' );

        ( isset($_POST['htmlInput'])        ? $html = trim($_POST['htmlInput'])                 : $html = '' );

        

        // strip all slashes.

        $maker              = stripslashes($maker);

        $maker_sel          = stripslashes($maker_sel);

        $model              = stripslashes($model);

        $model_sel          = stripslashes($model_sel);

        $body               = stripslashes($body);

        $body_sel           = stripslashes($body_sel);

        $measure_type       = stripslashes($measure_type);

        $engine_size        = stripslashes($engine_size);

        $trim               = stripslashes($trim);

        $type               = stripslashes($type);

        $gear               = stripslashes($gear);

        $transmission       = stripslashes($transmission);

        $fuel               = stripslashes($fuel);

        $color              = stripslashes($color);

        $last_service       = stripslashes($last_service);

        $mot                = stripslashes($mot);

        $tax_label          = stripslashes($tax_label);

        $top_speed          = stripslashes($top_speed);

        $torque             = stripslashes($torque);

        $kw                 = stripslashes($kw);

        $html               = stripslashes($html);

        

        // validate required fields.

        if( empty($maker) && empty($maker_sel) ){

            $this->form_message = $this->lang['NEWCAR_INVALID_MAKER_ERROR'];

            $this->form_status = true;

            $this->form_style = 'alert-error';

            $this->removeMedia($car_id);

            return;

        }

        

        if( empty($model) && empty($model_sel) ){

            $this->form_message = $this->lang['NEWCAR_INVALID_MODEL_ERROR'];

            $this->form_status = true;

            $this->form_style = 'alert-error';

            $this->removeMedia($car_id);

            return;

        }

        

        if( empty($body) && empty($body_sel) ){

            $this->form_message = $this->lang['NEWCAR_INVALID_BODY_TYPE_ERROR'];

            $this->form_status = true;

            $this->form_style = 'alert-error';

            $this->removeMedia($car_id);

            return;

        }

        

        // define default value.

        // step 1.

        if( empty($maker) ){ $maker = $maker_sel; }

        if( empty($model) ){ $model = $model_sel; }

        if( empty($body) ){ $body = $body_sel; }

        if( empty($price_thousand) ){ $price_thousand = '0'; }

        if( empty($price_hundred) ){ $price_hundred = '000'; }

        if( empty($price_decimal) ){ $price_decimal = '00'; }

        $formated_price = $price_thousand . $price_hundred . '.' . $price_decimal;

        

        if( empty($year) ){ $year = 1900; }

        if( empty($miles)){ $miles = 999999; }

        if( empty($measure_type) ){ $measure_type = 'km'; }

        if( empty($doors) ){ $doors = 1; }

        ( $featured ? $featured = '1' : $featured = '0' );

        if( empty($energy) ){ $energy = 'A'; }

        switch($energy){

            case 'A':

            case 'B':

            case 'C':

            case 'D':

            case 'E':

            case 'F':

                break;

            default:

                $energy = 'A';

        }

        // step 3.

        if( empty($engine_size) ){ $engine_size = $this->lang['NEWCAR_UNDEFINED_LABEL']; }

        if( empty($trim) ){ $trim = $this->lang['NEWCAR_UNDEFINED_LABEL']; }

        if( empty($type) ){ $type = $this->lang['NEWCAR_UNDEFINED_LABEL']; }

        if( empty($gear) ){ $gear = $this->lang['NEWCAR_UNDEFINED_LABEL']; }

        if( empty($fuel) ){ $fuel = $this->lang['NEWCAR_UNDEFINED_LABEL']; }

        if( empty($color) ){ $color = $this->lang['NEWCAR_UNDEFINED_LABEL']; }

        ( $prev_owners < 1 ? $prev_owners = '0' : $prev_owners = (string)$prev_owners );

        if( empty($last_service) ){ $last_service = $this->lang['NEWCAR_UNDEFINED_LABEL']; }

        if( empty($mot) ){ $mot = $this->lang['NEWCAR_UNDEFINED_LABEL']; }

        if( empty($tax_label) ){ $tax_label = $this->lang['NEWCAR_UNDEFINED_LABEL']; }

        if( empty($top_speed) ){ $top_speed = $this->lang['NEWCAR_UNDEFINED_LABEL']; }

        if( empty($torque) ){ $torque=1; }

        if( empty($kw) ){ $kw=1; }

        if( empty($transmission) ){ $transmission = $this->lang['NEWCAR_UNDEFINED_LABEL']; }

        if( empty($html) ){ $html=''; }

        

        // convert bodytype to integer based on body type list.

        $bodylist = BASE_ROOT . 'language/' . $_SESSION['log_language_iso'] . '/car-body-types.php';

        $cb = $this->body_list;



        foreach( $cb as $k => $v ){

            if( $v == $body ){

                $body = $k;

                break;

            }

        }

        if( empty($body) ){

            $body = '0';

        }

        

        // save.

        require_once BASE_CLASS . 'class-connect.php';

        $cnx = new Connect();

        $cnx->open();

        

        $maker          = @mysql_real_escape_string($maker);

        $model          = @mysql_real_escape_string($model);

        $body           = @mysql_real_escape_string($body);

        $formated_price = @mysql_real_escape_string($formated_price);

        $year           = @mysql_real_escape_string($year);

        $miles          = @mysql_real_escape_string($miles);

        $measure_type   = @mysql_real_escape_string($measure_type);

        $doors          = @mysql_real_escape_string($doors);

        $engine_size    = @mysql_real_escape_string($engine_size);

        $trim           = @mysql_real_escape_string($trim);

        $type           = @mysql_real_escape_string($type);

        $gear           = @mysql_real_escape_string($gear);

        $transmission   = @mysql_real_escape_string($transmission);

        $fuel           = @mysql_real_escape_string($fuel);

        $color          = @mysql_real_escape_string($color);

        $prev_owners    = @mysql_real_escape_string($prev_owners);

        $last_service   = @mysql_real_escape_string($last_service);

        $mot            = @mysql_real_escape_string($mot);

        $tax_label      = @mysql_real_escape_string($tax_label);

        $top_speed      = @mysql_real_escape_string($top_speed);

        $torque         = @mysql_real_escape_string($torque);

        $kw             = @mysql_real_escape_string($kw);

        $html           = @mysql_real_escape_string($html);

        

        // save to car table.

        if( !@mysql_query("INSERT INTO `car` (

                                                `id`,

                                                `maker`,

                                                `model`,

                                                `body_type`,

                                                `price`,

                                                `year`,

                                                `miles`,

                                                `measure_type`,

                                                `doors`,

                                                `status`,

                                                `featured`,

                                                `eco`,

                                                `vin`,

                                                `imported_from_csv`

                                             ) VALUES (

                                                '$car_id',

                                                '$maker',

                                                '$model',

                                                '$body',

                                                '$formated_price',

                                                '$year',

                                                '$miles',

                                                '$measure_type',

                                                '$doors',

                                                '0',

                                                '$featured',

                                                '$energy',

                                                '',

                                                '0'

                                             )") ){

            require_once BASE_CLASS . 'class-log.php';

            $cnx->close();

            LogReport::write('Unable to save to car table due a query error at ' . __FILE__ . ':' . __LINE__);

            $this->form_message = $this->lang['NEWCAR_SAVE_QUERY_ERROR'];

            $this->form_status = true;

            $this->form_style = 'alert-warning';

            $this->removeMedia($car_id);

            return;

        }

        

        // save to featured table.

        for( $i=0; $i < count($feat); $i++ ){

            

            if( isset($feat[$i]) ){

                $val = explode('[:]',$feat[$i]);

                $name = stripslashes($val[0]);

                $icon = $val[1];



                if( !empty($name) && !empty($icon) ){

                    if( !@mysql_query("INSERT INTO `car_features` (`car_id`,`feat_name`,`feat_icon`) VALUES ('$car_id','$name','$icon')") ){

                        require_once BASE_CLASS . 'class-log.php';

                        LogReport::write('Unable to save to car_features table due a query error at ' . __FILE__ . ':' . __LINE__);

                    }

                }

            }

        }

        

        // save deatils table.

        if( !@mysql_query("INSERT INTO `car_details` (

                                                     `car_id`,

                                                     `engine_size`,

                                                     `trim`,

                                                     `type`,

                                                     `gear`,

                                                     `fuel`,

                                                     `color`,

                                                     `prev_owners`,

                                                     `last_service`,

                                                     `mot`,

                                                     `tax_band`,

                                                     `top_speed`,

                                                     `engine_torque_rpm`,

                                                     `engine_power_kw`,

                                                     `transmission_type`,

                                                     `html`

                                                     ) VALUES (

                                                     '$car_id',

                                                     '$engine_size',

                                                     '$trim',

                                                     '$type',

                                                     '$gear',

                                                     '$fuel',

                                                     '$color',

                                                     '$prev_owners',

                                                     '$last_service',

                                                     '$mot',

                                                     '$tax_label',

                                                     '$top_speed',

                                                     '$torque',

                                                     '$kw',

                                                     '$transmission',

                                                     '$html'

                                                     )") ){

            require_once BASE_CLASS . 'class-log.php';

            LogReport::write('Unable to save to car_details table due a query error at ' . __FILE__ . ':' . __LINE__);

        }

        

        $cnx->close();

        

        $this->form_message =  $this->lang['NEWCAR_SAVE_QUERY_SUCCESS'];

        $this->form_status = true;

        $this->form_style = 'alert-success';

    }

    

    /**

     * Private method: remove current vehicle's media

     * <br>---------------------------------------------------------------------

     * @return void

     */

    private function removeMedia($carid){

        if( empty($carid) ){

            return;

        }

        

        require_once BASE_CLASS . 'class-connect.php';

        $cnx = new Connect();

        $cnx->open();

        

        if( $sql = @mysql_query("SELECT `car_id`,`source` FROM `car_media` WHERE `car_id`='$carid'") ){

            if( @mysql_num_rows($sql) > 0 ){

                while( $r=@mysql_fetch_assoc($sql) ){

                    @unlink(BASE_ROOT . $r['source']);

                }

                @mysql_free_result($sql);

            }

        }

        

        @mysql_query("DELETE FROM `car_media` WHERE `car_id`='$carid'");

        

        $cnx->close();

    }

    

    /**

     * Private method: load feature icons

     * @return array

     */

    private function loadFeaturedList(){

        require_once BASE_CLASS . 'class-connect.php';

        

        $cnx = new Connect();

        $cnx->open();

        

        if( !$sql = @mysql_query("SELECT * FROM `feature_list` ORDER BY `label` ASC") ){

            require_once BASE_CLASS . 'class-log.php';

            

            LogReport::write('Unable to load feature icon list due a query error at ' . __FILE__ . ':' . __LINE__);

            

            $this->feature_icon = array();

            

            return;

        }

        

        if( @mysql_num_rows($sql) < 0 ){

            $cnx->close();

            return;

        }

        

        $this->feature_icon = array();

        

        while( $r = @mysql_fetch_assoc($sql) ){

            $obj = array(

                'id' => $r['id'],

                'label' => stripslashes($r['label']),

                'source' => $r['source']

            );

            

            array_push($this->feature_icon,$obj);

        }

        

        @mysql_free_result($sql);

        $cnx->close();

    }

    

    /**

     * Private method: load body type list

     * <br>---------------------------------------------------------------------

     * @return array

     */

    private function loadBodyTypeList(){

        $path = BASE_ROOT . 'language/' . $_SESSION['log_language_iso'] . '/car-body-types.php';

        

        if( file_exists($path) ){

            require_once $path;

            $this->body_list = $_CAR_BODY;

        }

        else {

            $this->body_list = array();

        }

    }

    

    /**

     * Private method: load maker unique values list

     * <br>---------------------------------------------------------------------

     * @return void

     */

    private function loadMakerList(){

        require_once BASE_CLASS . 'class-connect.php';

        

        $cnx = new Connect();

        $cnx->open();

        

        if( !$sql = @mysql_query("SELECT `maker`,`model` FROM `car` ORDER BY `maker` ASC") ){

            require_once BASE_CLASS . 'class-log.php';

            

            $cnx->close();

            

            LogReport::write('Unable to load maker and model list due a query error at ' . __FILE__ . ':' . __LINE__);

            

            $this->form_message = $this->lang['NEWCAR_MAKER_QUERY_ERROR'];

            $this->form_status = true;

            $this->form_style = 'alert-warning';

            return;

        }

        

        if( @mysql_num_rows($sql) < 1 ){

            $cnx->close();

            return;

        }

        

        $this->maker_list = array();

        $this->model_list = array();

        

        while( $r = @mysql_fetch_assoc($sql) ){

            array_push($this->maker_list, stripslashes($r['maker']));

            array_push($this->model_list, stripslashes($r['model']));

        }

        

        @mysql_free_result($sql);

        $cnx->close();

        

        $this->maker_list = array_unique($this->maker_list);

        $this->maker_list = array_values($this->maker_list);

        $this->model_list = array_unique($this->model_list);

        $this->model_list = array_values($this->model_list);

    }

    

    /**

     * Public method: get car id

     * <br>---------------------------------------------------------------------

     * @return string The generated car id

     */

    public function getCarId(){

        return $this->car_id;

    }

    

    /**

     * Public method: get feature list

     * <br>---------------------------------------------------------------------

     * @return array

     */

    public function getFeatureIconList(){

        return $this->feature_icon;

    }

    

    /**

     * Public method: get body type list

     * <br>---------------------------------------------------------------------

     * @return array

     */

    public function getBodyList(){

        return $this->body_list;

    }

    

    /**

     * Public method: get maker unique values list

     * <br>---------------------------------------------------------------------

     * @return array

     */

    public function getMakerList(){

        return $this->maker_list;

    }

    

    /**

     * Public method: get model unique values list

     * <br>---------------------------------------------------------------------

     * @return array

     */

    public function getModelList(){

        return $this->model_list;

    }

    

    /**

     * Public method: get form status

     * <br>---------------------------------------------------------------------

     * @return bool.

     */

    public function getFormStatus()

    {

        return $this->form_status;

    }



    /**

     * Public method: get form message 

     * <br>---------------------------------------------------------------------

     * @return string.

     */

    public function getFormMessage()

    {

        return $this->form_message;

    }



    /**

     * Public method: get form style

     * <br>---------------------------------------------------------------------

     * @return string.

     */

    public function getFormStyle()

    {

        return $this->form_style;

    }

}