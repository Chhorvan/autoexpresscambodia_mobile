<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
include('view/3rdparty/pagination/pagination.php');
class publicAutoSparePart
{
    protected $lang;
    protected $group = 'public';
    protected $slug = 'auto-spare-part';
    protected $form_status;
    protected $form_message;
    protected $form_style;
    private $auto_info;
     var $total_num_row;
    var $pagination_html;
    var $current_page;
    var $total_page;

    /*
     * Constructor -------------------------------------------------------------
     */
    public function publicAutoSparePart($lang=array())
    {
        $this->lang = $lang;
       
		
    }

    function autoInfoSearch(){
       
       
        //echo $item.''.$category;
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        $where=" WHERE 1";

        if(isset( $_GET['item'])) $item = $_GET['item']; else $item ="";
        if(isset( $_GET['make'])) $make = $_GET['make']; else $make ="";
        if(isset( $_GET['keyword'])) $keyword = $_GET['keyword']; else $keyword ="";
        if(isset( $_GET['category'])) $category = $_GET['category']; else $category ="";

        if(!empty($item)){
            $where.=" AND `item`='$item'";
        }
        if(!empty($make)){
            $where.=" AND `make`='$make'";
        }
        if(!empty($keyword)){
            $where.=" AND (`category` LIKE '%$keyword%' OR `make` LIKE '%$keyword%' OR `item` LIKE '%$keyword%' )";
        }
        if(!empty($category)){
            $where.=" AND `category`='$category'";
        }
        ////////
        $sql_search="SELECT * from auto_info $where ORDER BY id DESC"; 
        /////PAGINATION PROCESS//////

        $sql_count_str=$sql_search;
        $sql_count = @mysql_query($sql_count_str);
        $this->total_num_row= @mysql_num_rows($sql_count);
        
        $links = new Pagination ($this->total_num_row);
        $limit=$links->start_display;
        $this->pagination_html= $links->display(); 
        $this->current_page = $links->currentPage();
        $this->total_page= $links->numPages();
        ///////PAGINATION PROCESS///////
        $sql_search.=$limit;  
        
        
        //echo $sql_search;
        if( !$sql = @mysql_query($sql_search) ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load car maker list due a query error at ' . __FILE__ . ':' . __LINE__);
            $cnx->close();
            return;
        }

        if( @mysql_num_rows($sql) < 1 ){
            $cnx->close();
        }
        
         
        $this->auto_info = array(); 
       
        while( $r = @mysql_fetch_assoc($sql)){
           
            array_push($this->auto_info, $r);
          
        }
         
  
        @mysql_free_result($sql);
        $cnx->close();
        return $this->auto_info;

    }
    


    function loadAutoInfo(){
        
        require_once BASE_CLASS . 'class-connect.php';
      
        $cnx = new Connect();
        $cnx->open();
        
        $sql_search="SELECT * from auto_info ORDER BY id DESC ";
        
        if( !$sql = @mysql_query($sql_search) ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load car maker list due a query error at ' . __FILE__ . ':' . __LINE__);
            $cnx->close();
            return;
        }

        if( @mysql_num_rows($sql) < 1 ){
            $cnx->close();
        }
        
        $auto_info = array();
       
        while( $r = @mysql_fetch_assoc($sql)){
           
            array_push($auto_info, $r);
          
        }
         
  
        @mysql_free_result($sql);
        $cnx->close();
        return $auto_info; 
        
    }
    function loadProduct(){
        
        require_once BASE_CLASS . 'class-connect.php';
      
        $cnx = new Connect();
        $cnx->open();
        
        $sql_search="SELECT DISTINCT make from product WHERE make <>'' ";
        
        if( !$sql = @mysql_query($sql_search) ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load car maker list due a query error at ' . __FILE__ . ':' . __LINE__);
            $cnx->close();
            return;
        }

        if( @mysql_num_rows($sql) < 1 ){
            $cnx->close();
        }
        
         
       $auto_info = array();
       
        while( $r = @mysql_fetch_assoc($sql)){
           
            array_push($auto_info, $r);
          
        }
         
  
        @mysql_free_result($sql);
        $cnx->close();
        return $auto_info; 
        
    }

    /*
     * Public method: get page content data ------------------------------------
     * @return array | false.
     */
    public function getPageHTML()
    {
        require_once BASE_ROOT . 'core/class-connect.php';

        $cnx = new Connect();
        $cnx->open();

        if( !$sql = @mysql_query("SELECT * FROM `page` WHERE `page_slug`='$this->slug' AND `page_group`='$this->group' LIMIT 1;") )
        {
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }

        if( @mysql_num_rows($sql) != 1 )
        {
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }

        $r = @mysql_fetch_assoc($sql);

        $result                     = array();
        $result['id']               = $r['id'];
        $result['meta_keywords']    = $r['meta_keywords'];
        $result['meta_description'] = $r['meta_description'];
        $result['meta_robots']      = $r['meta_robots'];
        $result['page_title']       = $r['page_title'];
        $result['page_slug']        = $r['page_slug'];
        $result['page_publish']     = $r['page_publish'];
        $result['page_author']      = $r['page_author'];
        $result['creation']         = $r['creation'];
        $result['editable']         = $r['editable'];
        $result['page_group']       = $r['page_group'];
        $result['html']             = $r['html'];

        @mysql_free_result($sql);
        $cnx->close();

        return $result;
    }
	

    /*
     * Public method: get form status ------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }

    /*
     * Public method: get form message -----------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /*
     * Public method: get form style -------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }

}
?>
