<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
class publicbuycar
{
    protected $lang;
    protected $group = 'public';
    protected $slug = 'car';
    protected $form_status;
    protected $form_message;
    protected $form_style;
	var $province;
	//var $car_info;
	var $car_color;
	var $car_make;
	var $car_year;
	var $car_model;
    /*
     * Constructor -------------------------------------------------------------
     */
    public function publicbuycar($lang=array())
    {
        $this->lang = $lang;
		$this->load_province();
		//$this->load_carinfo();
		$this->load_color();
		$this->load_car_make();
		$this->load_car_year();
		$this->load_car_model();
		
		if(isset($_POST['cmdsubmit'])){
		   $this->add_buyerinfo();
		}
		
    }
	public function add_buyerinfo(){
	
		$full_name=trim($_POST['fullname']);
		$gender=$_POST['gender'];
		$tel=trim($_POST['tel']);
		$email=trim($_POST['email']);
		$city_province=($_POST['province']);
		$make=$_POST['make'];
		$model=$_POST['model'];
		$year=$_POST['year'];
		$color=$_POST['color'];
		$condition=$_POST['condition'];
		$price=trim($_POST['price']);
		$desc=trim($_POST['description']);
		
		require_once dirname(dirname(__FILE__)) . '/config.php';
        require_once BASE_CLASS . 'class-connect.php';

        $cnx = new Connect();
        $cnx->open();
		if($full_name=="" and $gender=="" 
		and $tel=="" and $email=="" and $city_province=="" 
		and $make=="" and $model=="" and $year=="" and $color=="" and $condition=="" and $price=="" and $desc=="" ){
			echo"<script> alert('Can not submit Please fill some informations')</script>";
		}
		else{
		 
		$sql_insert="INSERT INTO buy_car (full_name,gender,tel,email,city_province,make,model,year_car,color,condition_car,price,description)
		VALUES ('$full_name','$gender','$tel','$email','$city_province','$make','$model','$year','$color','$condition','$price','$desc')";
		$result=@mysql_query($sql_insert);
		if($result){
			echo "<script> alert('Submit Success')</script>";
		 }
		}
		
	}
	
	public function load_province(){
	  require_once dirname(dirname(__FILE__)) . '/config.php';
      require_once BASE_CLASS . 'class-connect.php';

      $cnx = new Connect();
      $cnx->open();	
	  
	  $sql="SELECT province_name From province";
	  $query=@mysql_query($sql);
	  $this->province=array();
	  
	  while($row=@mysql_fetch_assoc($query)){	
		 array_push($this->province,$row);
	  }
	}
	
	/*public function load_carinfo(){
	  require_once dirname(dirname(__FILE__)) . '/config.php';
      require_once BASE_CLASS . 'class-connect.php';

      $cnx = new Connect();
      $cnx->open();	
	  $sq="SELECT make, year, model, exterior_color FROM product";
	  $qu=@mysql_query($sq);
	  $this->car_info=array();
	  
	  while($r=@mysql_fetch_assoc($qu)){
		 array_push($this->car_info,$r);
	  }
		
	}*/
	
	public function load_color(){
	  require_once dirname(dirname(__FILE__)) . '/config.php';
      require_once BASE_CLASS . 'class-connect.php';

      $cnx = new Connect();
      $cnx->open();	
	  $sql_color="SELECT DISTINCT exterior_color FROM product";
	  $query_color=@mysql_query($sql_color);
	  $this->car_color=array();
	  
	  while($row_color=@mysql_fetch_assoc($query_color)){
		 array_push($this->car_color,$row_color);
	  }
	}
	public function load_car_make(){
	  require_once dirname(dirname(__FILE__)) . '/config.php';
      require_once BASE_CLASS . 'class-connect.php';

      $cnx = new Connect();
      $cnx->open();	
	  $sql_make="SELECT DISTINCT make FROM product";
	  $query_make=@mysql_query($sql_make);
	  $this->car_make=array();
	  
	  while($row_make=@mysql_fetch_assoc($query_make)){
		 array_push($this->car_make,$row_make);
	  }
	}
	public function load_car_year(){
	  require_once dirname(dirname(__FILE__)) . '/config.php';
      require_once BASE_CLASS . 'class-connect.php';

      $cnx = new Connect();
      $cnx->open();	
	  $sql_year="SELECT DISTINCT year FROM product";
	  $query_year=@mysql_query($sql_year);
	  $this->car_year=array();
	  
	  while($row_year=@mysql_fetch_assoc($query_year)){
		 array_push($this->car_year,$row_year);
	  }
	}
	public function load_car_model(){
	  require_once dirname(dirname(__FILE__)) . '/config.php';
      require_once BASE_CLASS . 'class-connect.php';

      $cnx = new Connect();
      $cnx->open();	
	  $sql_model="SELECT DISTINCT model FROM product";
	  $query_model=@mysql_query($sql_model);
	  $this->car_model=array();
	  
	  while($row_model=@mysql_fetch_assoc($query_model)){
		 array_push($this->car_model,$row_model);
	  }
	  
	}
	
}
?>