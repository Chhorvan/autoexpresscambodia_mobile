<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
class publicemailsent
{


    /*
     * Constructor -------------------------------------------------------------
     */
    public function publicemailsent($lang=array())
    {
        $this->getstock_detail();
    }
	
	private function getstock_detail(){
		require_once BASE_CLASS . 'class-connect.php';

		$cid="";
		if(isset($_GET['cid']))
		$cid=$_GET['cid'];

        $cnx = new Connect();
        $cnx->open();

		$query = "SELECT p.*, c.source as carImg, c.thumb as thumb, cl.country_name as country,
                            cl.cc as flag ,cl.country_name, rg.company_name, rg.name, rg.email
                            FROM product as p
                            LEFT JOIN car_media as c on p.id=c.product_id AND c.primary_photo='1' c.primary_photo='2'
                            INNER JOIN country_list as cl on p.location=cl.cc
                            LEFT JOIN register as rg on p.owner=rg.id
							WHERE p.id='$cid'";

		$result = mysql_query($query);
		if(!$result){
			require_once BASE_CLASS . 'class-log.php';
            $cnx->close();

			$this->form_message = $this->lang['MANAGE_NEWS_LOAD_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
		}

		$this->stock_detail = array();
		if(mysql_num_rows($result)>0){

			while( $r = @mysql_fetch_assoc($result) ){
	            array_push($this->stock_detail, $r);
			}
		}
		$cnx->close();

	}

}
?>