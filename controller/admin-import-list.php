<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - Joel Debolt (Canada)
################################################################################
//override max script execution time. importing images may take quite a while...
ini_set('max_execution_time', 600); // 10 minutes

// Import 3rd party CSV vehicle list. Sample CSV format is available on the 
// installation's documentation directory.
class adminimportlist {
    protected $lang;
    protected $form_status;
    protected $form_message;
    protected $form_style;
    private $body_list;
    private $featured_icon_list;
    
    /**
     * Constructor
     * <br>---------------------------------------------------------------------
     */
    public function adminimportlist($lang=array()){
        $this->lang = $lang;
        
        // load car body-type list.
        require_once BASE_ROOT . 'language/' . $_SESSION['log_language_iso'] . '/car-body-types.php';
        $this->body_list = @$_CAR_BODY;
        
        // load featured icon list.
        $this->loadFeaturedList();
        
        // handle import csv button.
        if( isset($_POST['importbtn']) ){
            $this->handleListImport();
        }
    }
    
    /**
     * Private method: load featured icon list
     * <br>---------------------------------------------------------------------
     * @return void
     */
    private function loadFeaturedList(){
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        if( !$sql = @mysql_query("SELECT `label`,`source` FROM `feature_list`") ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load feature icon list due a query error at ' . __FILE__ . ':' . __LINE__);
            $cnx->close();
            $this->featured_icon_list = array();
        }
        
        if( @mysql_num_rows($sql) < 1 ){
            $cnx->close();
            $this->featured_icon_list = array();
            return;
        }
        
        $this->featured_icon_list = array();
        
        while( $r = @mysql_fetch_assoc($sql) ){
            $obj = array(
                'label' => $r['label'],
                'source' => $r['source']
            );
            array_push($this->featured_icon_list,$obj);
        }
        
        @mysql_free_result($sql);
        $cnx->close();
        return;
    }
    
    /**
     * Private method: handle CSV list import
     * <br>---------------------------------------------------------------------
     * @return void
     */
    private function handleListImport(){
        // initial validation.
        if( !isset($_FILES['csv_file']['name']) ){
            return;
        }
        
        // set validation data
        $ext = strtolower($_FILES['csv_file']['name']);
        $ext = explode('.',$ext);
        $ext = $ext[(count($ext)-1)];
        $size = $_FILES['csv_file']['size'];
        
        // get max upload size.
        $max = $this->getMaxUploadSize();
        $max = ($max * 1024) * 1024;
        
        // validate extension and size.
        if( $ext != 'csv' ){
            $this->form_message = $this->lang['IMPORT_INVALID_FILE_FORMAT'];
            $this->form_status = true;
            $this->form_style = 'alert-error';
            return;
        }
        
        if( $size < 1 || $size > $max ){
            $this->form_message = $this->lang['IMPORT_MAX_FILESIZE'] . $this->getMaxUploadSize() . ' MB';
            $this->form_status = true;
            $this->form_style = 'alert-error';
            return;
        }
        
        // upload CSV file.
        $filename = date('Ymd-His') . '.csv';
        $destination = BASE_ROOT . 'tmp/' . $filename;
        
        if( file_exists($destination) ){
            @unlink($destination);
        }
        
        if( !@move_uploaded_file($_FILES['csv_file']['tmp_name'], $destination) ){
            require_once BASE_CLASS . 'class-log.php';
            
            LogReport::write('Unable to upload CSV list due an error at ' . __FILE__ . ':' . __LINE__);
            
            $this->form_message = $this->lang['IMPORT_UPLOAD_ERROR_MESSAGE'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        
        // process csv.
        $csv_url = $destination;

        // get file content.
        require_once BASE_CLASS . 'class-csv.php';
        $csv = new parseCSV();

        $csv->auto($csv_url);

        // save values into array.
        $result_array = array();

        // get array values.
        foreach( $csv->data as $key => $row ){
            $obj = array();

            foreach( $row as $value => $t ){
                // set obj index.
                $ind = strtolower($value);
                $ind = str_replace(' ', '_', $ind);

                // process options.
                if( $ind == 'options' ){
                    $opt_array = explode(',',$t);
                    $obj[$ind] = $opt_array;
                }

                // process categorized options.
                else if( $ind == 'categorized_options' ){
                    $cat_array = explode('@',$t);
                    $obj[$ind] = $cat_array;
                }

                // process image list.
                else if( $ind == 'imagelist' ){
                    $img_array = explode(',',$t);
                    $obj[$ind] = $img_array;
                }

                else {
                    $obj[$ind] = $t;
                }
            }

            array_push($result_array, $obj);
        }
        
        // remove vehicles that are no longer listed on the CSV import
        // which has the status import_from_csv set to 1.
        $this->filterVehicleEntries($result_array);
        
        require_once BASE_CLASS . 'class-connect.php';
        require_once BASE_CLASS . 'class-log.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        for( $i=0; $i < count($result_array); $i++ ){
            
            if( !isset($result_array[$i]['vin']) ){
                continue;
            }
            
            $q = "SELECT `vin` FROM `car` WHERE `vin`='".$result_array[$i]['vin']."' LIMIT 1;";
            
            // if query fails, stop all requests.
            if( !$sql = @mysql_query($q) ){
                LogReport::write('Unable to query database at ' . __FILE__ . ':' . __LINE__);
                
                $this->form_message = $this->lang['IMPORT_QUERY_CHECK_ERROR'];
                $this->form_status = true;
                $this->form_style = 'alert-warning';
                
                @unlink($destination);
                
                break;
            }
            
            if( @mysql_num_rows($sql) > 0 ){
                $this->updateVehicleData($result_array[$i]);
                continue;
            }
            else {
                $this->insertVehicleData($result_array[$i]);
                continue;
            }
        }
        
        @unlink($destination);
        
        $cnx->close();
    }
    
    /**
     * Private method: remove entries that are no longer listed on<br> the CSV list
     * from the database.
     * <br>---------------------------------------------------------------------
     * @param array $vehicle_list The vehicle list.
     * @return boolean
     */
    private function filterVehicleEntries($vehicle_list){
        // make sure array has at least 1 entry.
        if( !is_array($vehicle_list) || count($vehicle_list) < 1 ){
            return true;
        }
        
        require_once BASE_CLASS . 'class-connect.php';
        require_once BASE_CLASS . 'class-log.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        // get all database vin entries.
        if( !$sql = @mysql_query("SELECT `status`,`vin`,`imported_from_csv` FROM `car` WHERE `imported_from_csv`='1' AND (`status`='0' OR '1')") ){
            LogReport::write('Unable to load VIN list due a query error at ' . __FILE__ . ':' . __LINE__);
            $cnx->close();
            return false;
        }
        
        if( @mysql_num_rows($sql) < 1 ){
            $cnx->close();
            return true;
        }
        
        $db_list = array();
        
        while( $r = @mysql_fetch_assoc($sql) ){
            array_push($db_list, $r['vin']);
        }
        
        @mysql_free_result($sql);
        
        // get vin list.
        $vin_list = array();
        
        for( $i=0; $i < count($vehicle_list); $i++ ){
            if( isset($vehicle_list[$i]['vin']) ){
                array_push($vin_list, $vehicle_list[$i]['vin']);
            }
        }
        
        // get the list of entries which will have status set to 2 (sold).
        $remove_list = array();
        $remove_list = array_diff($db_list, $vin_list);
        
        for( $i=0; $i < count($remove_list); $i++ ){
            if( !@mysql_query("UPDATE `car` SET `status`='2' WHERE `vin`='" . $remove_list[$i] . "' LIMIT 1;") ){
                LogReport::write('Unable to update vehicle VIN # ' . $remove_list[$i] . ' due a query error. Update the vehicle status manually! Error thrown at ' . __FILE__ . ':' . __LINE__);
                continue;
            }
        }
        
        $cnx->close();
        
        unset($vin_list);
        unset($remove_list);
        unset($vehicle_list);
    }
    
    /**
     * Private method: update vehicle entry (updates only price for now...)
     * <br>---------------------------------------------------------------------
     * @param array $vehicle The vehicle object as array.
     * @return void
     */
    private function updateVehicleData($vehicle){
        if( !is_array($vehicle) ){
            return;
        }
        
        $vin = $vehicle['vin'];
    }
    
    /**
     * Private method: insert vehicle to database
     * <br>---------------------------------------------------------------------
     * @param array $vehicle The vehicle object to be add into database.
     * @return void
     */
    private function insertVehicleData($vehicle){
        if( !is_array($vehicle) ){
            return;
        }

        // get car body code.
        $bb = '';
        $body_type = trim($vehicle['body']);

        if( empty($body_type) ){
           $bb = '0';
        } else {
            foreach($this->body_list as $k => $v ){
                if( $v == $body_type ){
                     $bb = $k;
                }
            }
        }

        if( empty($bb) && $bb != 0 ){
           $bb = '0';

           require_once BASE_CLASS . 'class-log.php';
           LogReport::write('IMPORTANT: add the body-name [' . $vehicle['body'] . '] to the body type list at language/xx/car-body-types.php.');
        }
        
        // define car table query.
        require_once BASE_CLASS . 'class-utilities.php';
        
        $cid            = Utilities::generateRandomString(21);
        $maker          = $vehicle['make'];
        $model          = $vehicle['model'];
        $body_type      = $bb;
        $price          = number_format($vehicle['sellingprice'],2);
        $price          = str_replace(',','',$price);
        $year           = $vehicle['year'];
        $miles          = $vehicle['miles'];
        $measure_type   = 'miles';
        $doors          = $vehicle['doors'];
        $status         = '0';
        $featured       = '0';
        $eco            = 'A';
        $vin            = $vehicle['vin'];
        
        // [2013.07.13 update] save vehicle address.
        $address        = $vehicle['dealer_address'];
        $city           = $vehicle['dealer_city'];
        $state          = $vehicle['dealer_state'];
        $zip            = $vehicle['dealer_zip'];
        $phone          = $vehicle['dealer_phone'];
        $fax            = $vehicle['dealer_fax'];
        
        // ignore entries with price 0 (zero) [update:2013.07.11]
        if( !isset($price) || empty($price) || $price < 1 ){
            return;
        }
        
        // define address list query [2013.07.13].
        $address_list_query = "INSERT INTO `car_address` (`car_id`,`address`,`city`,`state`,`zip`,`phone`,`fax`) VALUES ('$cid','$address','$city','$state','$zip','$phone','$fax')";
        
        // define car list query.
        $car_list_query  = "INSERT INTO `car` (`id`,`maker`,`model`,`body_type`,`price`,`year`,`miles`,`measure_type`,`doors`,`status`,`featured`,`eco`,`vin`,`imported_from_csv`)";
        $car_list_query .= " VALUES ('$cid','$maker','$model','$body_type','$price','$year','$miles','$measure_type','$doors','$status','$featured','$eco','$vin','1')";
        
        // define car features query.
        $n_list = array();
        $options = (array)$vehicle['options'];
        
        for( $i=0; $i < count($options); $i++ ){
            array_push($n_list, $options[$i]);
        }
        
        array_unique($n_list);
        $n_list = array_values($n_list);
        $res_list = array();
        $html = '';
        
        // only grab options which has a match to the featured icon list. add the
        // rest of it into the html description.
        for( $i=0; $i < count($n_list); $i++ ){
            for( $u=0; $u < count($this->featured_icon_list); $u++ ){
                if( $n_list[$i] == $this->featured_icon_list[$u]['label']){
                    array_push($res_list, $this->featured_icon_list[$u]);
                    unset($n_list[$i]);
                    break;
                }
            }
        }
        
        $n_list = array_values($n_list);
        
        // update html.
        $html .= '<div><h3>Featured options</h3><ul>';
        for( $i=0; $i < count($n_list); $i++ ){
            $html .= '<li>' . htmlentities($n_list[$i]) . '</li>';
        }
        $html .= '</ul></div>';
        
        unset($n_list);
        unset($options);
        
        // define car details query.
        $engine_size        = $vehicle['engine_description'];
        $trim               = trim($vehicle['trim']);
        $type               = trim($vehicle['marketclass']);
        $gear               = $vehicle['transmission_speed'];
        $fuel               = trim($vehicle['fuel_type']);
        $color              = trim($vehicle['exteriorcolor']);
        $prev_owners        = '';
        $last_service       = '';
        $mot                = '';
        $tax_band           = '';
        $top_speed          = '';
        $engine_torque_rpm  = '';
        $engine_power_kw    = '';
        $transmission_type  = $vehicle['transmission'];
        
        $coptions = (array)$vehicle['categorized_options'];
                
        $html .= '<table>' . 
                 '<tr>'.
                 '<td style="width:150px;"><strong>Type</strong></td>'.
                 '<td>'.$vehicle['type'].'</td>'.
                 '</tr>'.
                 '<tr>'.
                 '<td style="width:150px;"><strong>Stock</strong></td>'.
                 '<td>'.$vehicle['stock'].'</td>'.
                 '</tr>'.
                 '<tr>'.
                 '<td style="width:150px;"><strong>Model number</strong></td>'.
                 '<td>'.$vehicle['modelnumber'].'</td>'.
                 '</tr>'.
                 '<tr>'.
                 '<td style="width:150px;"><strong>Engine Cylinders</strong></td>'.
                 '<td>'.$vehicle['enginecylinders'].'</td>'.
                 '</tr>'.
                 '<tr>'.
                 '<td style="width:150px;"><strong>Engine block type</strong></td>'.
                 '<td>'.$vehicle['engine_block_type'].'</td>'.
                 '</tr>'.
                 '<tr>'.
                 '<td style="width:150px;"><strong>Aspiration type</strong></td>'.
                 '<td>'.$vehicle['engine_aspiration_type'].'</td>'.
                 '</tr>'.
                 '<tr>'.
                 '<td style="width:150px;"><strong>Drive train</strong></td>'.
                 '<td>'.$vehicle['drivetrain'].'</td>'.
                 '</tr>'.
                 '<tr>'.
                 '<td style="width:150px;"><strong>City MPG</strong></td>'.
                 '<td>'.$vehicle['citympg'].'</td>'.
                 '</tr>'.
                 '<tr>'.
                 '<td style="width:150px;"><strong>Highway MPG</strong></td>'.
                 '<td>'.$vehicle['highwaympg'].'</td>'.
                 '</tr>'.
                 '<tr>'.
                 '<td style="width:150px;"><strong>Wheel base code</strong></td>'.
                 '<td>'.$vehicle['wheelbase_code'].'</td>'.
                 '</tr>'.
                 '<tr>'.
                 '<td style="width:150px;"><strong>Passanger capacity</strong></td>'.
                 '<td>'.$vehicle['passengercapacity'].'</td>'.
                 '</tr>'.                
                 '</table>'.
                 '<div>'.
                 '<h3 style="font-family:Arial;">Categorized Options</h3>'.
                 '<ul>';
                 for( $i=0; $i < count($coptions); $i++ ){
                    $html .= '<li>' . $coptions[$i].'</li>';
                 }
        $html .= '</ul></div>';
        
        $html = @mysql_real_escape_string($html);
        $engine_size = @mysql_real_escape_string($engine_size);
        $trim = @mysql_real_escape_string($trim);
        $type = @mysql_real_escape_string($type);
        $gear = @mysql_real_escape_string($gear);
        $fuel = @mysql_real_escape_string($fuel);
        $color = @mysql_real_escape_string($color);
        $prev_owners = @mysql_real_escape_string($prev_owners);
        $last_service = @mysql_real_escape_string($last_service);
        $mot = @mysql_real_escape_string($mot);
        $tax_band = @mysql_real_escape_string($tax_band);
        $top_speed = @mysql_real_escape_string($top_speed);
        $engine_torque_rpm = @mysql_real_escape_string($engine_torque_rpm);
        $engine_power_kw = @mysql_real_escape_string($engine_power_kw);
        $transmission_type = @mysql_real_escape_string($transmission_type);
        $address = @mysql_real_escape_string($address);
        $city = @mysql_real_escape_string($city);
        $state = @mysql_real_escape_string($state);
        $zip = @mysql_real_escape_string($zip);
        $phone = @mysql_real_escape_string($phone);
        $fax = @mysql_real_escape_string($fax);
        
        $car_details_query  = "INSERT INTO `car_details` (`car_id`,`engine_size`,`trim`,`type`,`gear`,`fuel`,`color`,`prev_owners`,`last_service`,`mot`,`tax_band`,`top_speed`,`engine_torque_rpm`,`engine_power_kw`,`transmission_type`,`html`) ";
        $car_details_query .= "VALUES ('$cid','$engine_size','$trim','$type','$gear','$fuel','$color','$prev_owners','$last_service','$mot','$tax_band','$top_speed','$engine_torque_rpm','$engine_power_kw','$transmission_type','$html')";
                
        // set featured icons query.
        $feat_query = array();
        
        for( $i=0; $i < count($res_list); $i++ ){
            array_push($feat_query,"INSERT INTO `car_features` (`car_id`,`feat_name`,`feat_icon`) VALUES ('$cid','".$res_list[$i]['label']."','".$res_list[$i]['source']."')");
        }
        
        // set media query.
        $img = (array)$vehicle['imagelist'];
        $img_array = array();
        
        for( $i=0; $i < count($img); $i++ ){
            if( !empty($img) ){
                // set image destination path.
                $dest = BASE_ROOT . 'image/upload/cars/';
                $ext  = strtolower($img[$i]);
                $ext = explode('.',$ext);
                $ext = $ext[(count($ext)-1)];
                $filename = $vehicle['vin'] . '_' . date('Ymd_His') . '_' . mt_rand('000','999') .'.'.$ext;
                
                if( !empty($img[$i]) ){
                    if( copy($img[$i],$dest.$filename) ){
                        array_push($img_array,'image/upload/cars/' . $filename);
                    }
                }
            }
        }
        
        unset($img);
        
        $media_query = array();
        
        for( $i=0; $i < count($img_array); $i++ ){
            array_push($media_query,"INSERT INTO `car_media` (`car_id`,`type`,`mode`,`source`) VALUES ('$cid','image','exterior','".$img_array[$i]."')");
        }
        
        // query all databases.
        require_once BASE_CLASS . 'class-log.php';
        
        // query car table.
        if( !@mysql_query($car_list_query) ){
            // set log entry.
            LogReport::write('Unable to save car list due a query error at ' . __FILE__ . ':' . __LINE__);
                        
            // set error message.
            $this->form_message = $this->lang['IMPORT_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            
            // remove images saved on car directory.
            $this->removeCarImages($img_array);
            
            return;
        }
        
        // query car address.
        if( !mysql_query($address_list_query) ){
            // remove entry from car table.
            @mysql_query("DELETE FROM `car` WHERE `car_id`='$cid' LIMIT 1;");
            
            // set log entry.
            LogReport::write('Unable to save car address due a query error at ' . __FILE__ . ':' . __LINE__);
                        
            // set error message.
            $this->form_message = $this->lang['IMPORT_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            
            // remove images saved on car directory.
            $this->removeCarImages($img_array);
        }
        
        // query car details list.
        if( !@mysql_query($car_details_query) ){
            // remove entry from car table.
            @mysql_query("DELETE FROM `car` WHERE `car_id`='$cid' LIMIT 1;");
            
            // set log entry.
            LogReport::write('Unable to save car list due a query error at ' . __FILE__ . ':' . __LINE__);
                        
            // set error message.
            $this->form_message = $this->lang['IMPORT_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            
            // remove images saved on car directory.
            $this->removeCarImages($img_array);
            
            return;
        }
        
        // query featured list.
        if( is_array($feat_query) && count($feat_query) > 0 ){
            for( $i=0; $i < count($feat_query); $i++ ){
                if( !@mysql_query($feat_query[$i]) ){
                    // set log entry.
                    LogReport::write('Unable to save car feature entry due a query error at ' . __FILE__ . ':' . __LINE__);
                    continue;
                }
            }
        }
                
        // query car media.
        if( is_array($media_query) && count($media_query) > 0 ){
            for( $i=0; $i < count($media_query); $i++ ){
                if( !@mysql_query($media_query[$i]) ){
                    // remove the image which couldn't be saved on db.
                    @unlink(BASE_ROOT . $img_array[$i]);
                    
                    // set log entry.
                    LogReport::write('Unable to save car image entry due a query error at ' . __FILE__ . ':' . __LINE__);
                    continue;
                }
            }
        }
        
        // unset all arrays.
        unset($car_list_query);
        unset($address_list_query);
        unset($car_details_query);
        unset($media_query);
        unset($feat_query);
        unset($img_array);
        
        // set response.
        $this->form_message = $this->lang['IMPORT_QUERY_SUCCESS'];
        $this->form_status = true;
        $this->form_style = 'alert-success';
        
        return;
    }
    
    /**
     * Private method: remove image list from car directory
     * <br>---------------------------------------------------------------------
     * @param array $img_array
     * @return void
     */
    private function removeCarImages($img_array){
        if( !is_array($img_array) ){
            return;
        }
        
        for( $i=0; $i < count($img_array); $i++ ){
            if( !empty($img_array[$i]) && file_exists(BASE_ROOT . $img_array[$i]) ){
                @unlink(BASE_ROOT . $img_array[$i]);
            }
        }
    }
    
    /**
     * Public method: get maximum upload file size (mb)
     * <br>---------------------------------------------------------------------
     * @return int
     */
    public function getMaxUploadSize(){
        $max_upload = (int)(ini_get('upload_max_filesize'));
        $max_post = (int)(ini_get('post_max_size'));
        $memory_limit = (int)(ini_get('memory_limit'));
        $upload_mb = min($max_upload, $max_post, $memory_limit);
        
        return $upload_mb;
    }
    
    /**
     * Public method: get form status
     * <br>---------------------------------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }

    /**
     * Public method: get form message 
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /**
     * Public method: get form style
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }
}

