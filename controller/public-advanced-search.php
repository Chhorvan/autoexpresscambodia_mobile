<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.2.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
class publicadvancedsearch {
    private $lang;
    private $form_message;
    private $form_status;
    private $form_style;
    private $car_list;
    private $car_details;
    private $search_car_list;
    
    /**
     * Constructor
     * <br>---------------------------------------------------------------------
     */
    public function publicadvancedsearch($lang=array()){
        $this->lang = $lang;
        
        // load car details.
        $this->loadCarDetails();
        
        // handle search form submit.
        if( isset($_POST['filterbtn']) ){
            $this->handleSearchForm();
        }
    }
    
    /**
     * Private method: handle search form
     * <br>---------------------------------------------------------------------
     * @return void
     */
    private function handleSearchForm(){
        // load car list after form submition.
        $this->loadCarList();
        
        $door       = trim($_POST['doorInput']);
        $color      = trim($_POST['colorInput']);
        $trim       = trim($_POST['trimInput']);
        $miles      = trim($_POST['milesInput']);
        $miles      = explode(':',$miles);
        $miles      = $miles[0];
        $torque     = trim($_POST['torqueInput']);
        $fuel       = trim($_POST['fuelInput']);
        $year       = trim($_POST['yearInput']);
        $type       = trim($_POST['typeInput']);
        $price      = trim($_POST['priceInput']);
        $kw         = trim($_POST['kwInput']);
        $transmission = trim($_POST['transmissionInput']);
        $engine_size = trim($_POST['engineSizeInput']);
        $gear       = trim($_POST['gearInput']);
        $eco        = trim($_POST['ecoInput']);
        $tax        = trim($_POST['taxBandInput']);
        
        // set query string.
        // get results for car table first.
        $query = 'SELECT * FROM `car` ';
        $condition = '';
        $has_query = false;
        
        if( $price > 0 && $price != 'any' ){
            $condition .= "`price`='$price' AND";
            $has_query = true;
        }
        
        if( $year != 'any' && strlen($year) == 4 ){
            $condition .= "`year`='$year' AND";
            $has_query = true;
        }
        
        if( $miles > -1 && $miles != 'any' ){
            $condition .= "`miles`='$miles' AND";
            $has_query = true;
        }
        
        if( $door > 0 && $door != 'any' ){
            $condition .= "`doors`='$door' AND";
            $has_query = true;
        }
        
        if( $eco != 'any' ){
            $condition .= "`eco`='$eco' AND ";
            $has_query = true;
        }
        
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        if( $has_query ){
            $query .= " WHERE $condition `status`='0'";
        }
        else {
            $query .= " WHERE `status`='0'";
        }
        
        echo $query;
        if( !$sql = @mysql_query($query) ){
            require_once BASE_CLASS . 'class-log.php';
            
            LogReport::write('Unable to perform search due a query error at ' . __FILE__ . ':' . __LINE__);
            
            $cnx->close();
            $this->form_message = $this->lang['ADVANCED_SEARCH_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        
        $this->search_car_list = array();
        
        if( @mysql_num_rows($sql) < 1 ){
            $cnx->close();
            return;
        }
        
        while( $r = @mysql_fetch_assoc($sql) ){
            $obj = array(
                'id'            => $r['id'],
                'maker'         => stripslashes($r['maker']),
                'model'         => stripslashes($r['model']),
                'body_type'     => $r['body_type'],
                'price'         => stripslashes($r['price']),
                'year'          => $r['year'],
                'miles'         => stripslashes($r['miles']),
                'measure_type'  => stripslashes($r['measure_type']),
                'doors'         => stripslashes($r['doors']),
                'featured'      => (bool)$r['featured'],
                'eco'           => $r['eco']
            );
            array_push($this->search_car_list, $obj);
        }
        
        @mysql_free_result($sql);
        
        // get car details list.
        for( $i=0; $i < count($this->search_car_list); $i++ ){
           if( !$sql = @mysql_query("SELECT * FROM `car_details` WHERE `car_id`='$this->search_car_list[$i]['id']") ){
               require_once BASE_CLASS . 'class-log.php';
               LogReport::write('Unable to load car by id due a query error at ' . __FILE__ . ':' . __LINE__);
               continue;
           } else {
               $nobj = array(
                   'engine_size' => stripslashes($r['engine_size']),
                   'trim' => stripslashes($r['trim']),
                   'type' => stripslashes($r['type']),
                   'gear' => stripslashes($r['gear']),
                   'fuel' => stripslashes($r['fuel']),
                   'color' => stripslashes($r['color']),
                   'prev_owners' => stripslashes($r['prev_owners']),
                   'last_service' => stripslashes($r['last_service']),
                   'mot' => stripslashes($r['mot']),
                   'tax_band' => stripslashes($r['tax_band']),
                   'top_speed' => stripslashes($r['top_speed']),
                   'engine_torque_rpm' => stripslashes($r['engine_torque_rpm']),
                   'engine_power_kw' => stripslashes($r['engine_power_kw']),
                   'transmission_type' => stripslashes($r['transmission_type']),
                   'html' => stripslashes($r['html'])
               );
               
               array_push($this->search_car_list[$i],$nobj);
           }
        }
        
        
        
        
        
    }
    
    /**
     * Private method: load car details list
     * <br>---------------------------------------------------------------------
     * @return void
     */
    private function loadCarDetails(){
        $this->car_details = array();
        
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        if( !$sql = @mysql_query("SELECT * FROM `car_details`") ){
            require_once BASE_CLASS . 'class-log.php';
            
            LogReport::write('Unable to load car details list due a query error at ' . __FILE__ . ':' . __LINE__);
            
            $cnx->close();
            $this->form_message = $this->lang['ADVANCED_SEARCH_DETAILS_LOAD_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        
        $engine_size = array();
        $trim = array();
        $type = array();
        $gear = array();
        $fuel = array();
        $color = array();
        $prev_owner = array();
        $last_service = array();
        $mot = array();
        $tax = array();
        $speed = array();
        $torque = array();
        $kw = array();
        $transmission = array();
        
        while( $r = @mysql_fetch_assoc($sql) ){
            array_push($engine_size,$r['engine_size']);
            array_push($trim, $r['trim']);
            array_push($type,$r['type']);
            array_push($gear,$r['gear']);
            array_push($fuel,$r['fuel']);
            array_push($color,$r['color']);
            array_push($prev_owner,$r['prev_owners']);
            array_push($last_service,$r['last_service']);
            array_push($mot,$r['mot']);
            array_push($tax,$r['tax_band']);
            array_push($speed,$r['top_speed']);
            array_push($torque,$r['engine_torque_rpm']);
            array_push($kw,$r['engine_power_kw']);
            array_push($transmission,$r['transmission_type']);
        }
        
        @mysql_free_result($sql);
        
        // make each array unique.
        $engine_size = array_unique($engine_size);
        $engine_size = array_values($engine_size);
        sort($engine_size);
        $trim = array_unique($trim);
        $trim = array_values($trim);
        sort($trim);
        $type = array_unique($type);
        $type = array_values($type);
        sort($type);
        $gear = array_unique($gear);
        $gear = array_values($gear);
        sort($gear);
        $fuel = array_unique($fuel);
        $fuel = array_values($fuel);
        sort($fuel);
        $color = array_unique($color);
        $color = array_values($color);
        sort($color);
        $prev_owner = array_unique($prev_owner);
        $prev_owner = array_values($prev_owner);
        sort($prev_owner);
        $last_service = array_unique($last_service);
        $last_service = array_values($last_service);
        sort($last_service);
        $mot = array_unique($mot);
        $mot = array_values($mot);
        sort($mot);
        $tax = array_unique($tax);
        $tax = array_values($tax);
        sort($tax);
        $speed = array_unique($speed);
        $speed = array_values($speed);
        sort($speed);
        $torque = array_unique($torque);
        $torque = array_values($torque);
        sort($torque);
        $kw = array_unique($kw);
        $kw = array_values($kw);
        sort($kw);
        $transmission = array_unique($transmission);
        $transmission = array_values($transmission);
        sort($transmission);
        
        // load car table.
        if( !$sql = @mysql_query("SELECT `price`,`year`,`miles`,`measure_type`,`doors`,`status`,`eco` FROM `car` WHERE `status`='0'") ){
            $cnx->close();
            
            require_once BASE_CLASS . 'class-log.php';
            
            LogReport::write('Unable to load car list due a query error at ' . __FILE__ . ':' . __LINE__);
            
            $this->form_message = $this->lang['ADVANCED_SEARCH_LOADER_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            
            return;
        }
        
        $this->car_list = array();
        
        if( @mysql_num_rows($sql) < 1 ){
            $cnx->close();
            return;
        }
        
        $price = array();
        $year  = array();
        $miles = array();
        $doors = array();
        $eco   = array();
        
        while( $r = @mysql_fetch_assoc($sql) ){
            array_push($price,$r['price']);
            array_push($year,(int)$r['year']);
            array_push($miles, $r['miles'] . ':' . $r['measure_type']);
            array_push($doors,$r['doors']);
            array_push($eco,$r['eco']);
        }
        
        @mysql_free_result($sql);
        $cnx->close();
        
        $price = array_unique($price);
        $price = array_values($price);
        sort($price);
        $year = array_unique($year);
        $year = array_values($year);
        sort($year);
        $miles = array_unique($miles);
        $miles = array_values($miles);
        sort($miles);
        $doors = array_unique($doors);
        $doors = array_values($doors);
        sort($doors);
        $eco = array_unique($eco);
        $eco = array_values($eco);
        sort($eco);
        
        $this->car_details = array(
            'engine_size' => $engine_size,
            'trim' => $trim,
            'type' => $type,
            'gear' => $gear,
            'fuel' => $fuel,
            'color' => $color,
            'prev_owner' => $prev_owner,
            'last_service' => $last_service,
            'mot' => $mot,
            'tax' => $tax,
            'speed' => $speed,
            'torque' => $torque,
            'kw' => $kw,
            'transmission' => $transmission,
            'price' => $price,
            'year' => $year,
            'miles' => $miles,
            'door' => $doors,
            'eco' => $eco
        );
        
        return;
    }
    
    /**
     * Private method: load car list
     * <br>---------------------------------------------------------------------
     * @return void
     */
    private function loadCarList(){
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        if( !$sql = @mysql_query("SELECT * FROM `car` WHERE `status`='0' ORDER BY `price` ASC") ){
            $cnx->close();
            
            require_once BASE_CLASS . 'class-log.php';
            
            LogReport::write('Unable to load car list due a query error at ' . __FILE__ . ':' . __LINE__);
            
            $this->form_message = $this->lang['ADVANCED_SEARCH_LOADER_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            
            return;
        }
        
        $this->car_list = array();
        
        if( @mysql_num_rows($sql) < 1 ){
            $cnx->close();
            return;
        }
        
        while( $r = @mysql_fetch_assoc($sql) ){
            $obj = array(
                'id'            => (int)$r['id'],
                'maker'         => stripslashes($r['maker']),
                'model'         => stripslashes($r['model']),
                'body_type'     => $r['body_type'],
                'price'         => $r['price'],
                'year'          => (int)$r['year'],
                'miles'         => $r['miles'],
                'measure_type'  => $r['measure_type'],
                'doors'         => $r['doors'],
                'status'        => $r['status'],
                'featured'      => $r['featured'],
                'eco'           => $r['eco']
            );
            array_push($this->car_list,$obj);
        }
        
        @mysql_free_result($sql);
        $cnx->close();
        
        
        // get body name list.
        $path = BASE_ROOT . 'language/' . $_SESSION['log_language_iso'] . '/car-body-types.php';
        
        if( !file_exists($path) ){
            require_once BASE_CLASS . 'class-log.php';
            
            LogReport::write('Unable to load ' . $path . '. The language file does not exists and is required. Error throw at ' . __FILE__ . ':' . __LINE__);
            
            $this->car_list = array();
            
            $this->form_message = $this->lang['ADVANCED_SEARCH_LOADER_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            
            return;
        }
        
        require_once $path;
        
        $this->body_name_list = $_CAR_BODY;
        
        return;
    }
    
    /**
     * Public method: get car details list of unique values
     * <br>---------------------------------------------------------------------
     * @return array object
     */
    public function getCarDetails(){
        return $this->car_details;
    }
    
    /**
     * Public method: get body name by ID
     * <br>---------------------------------------------------------------------
     * @return string The body name as string.
     */
    public function getBodyNameByID($body_id){
        $result = $this->lang['ADVANCED_SEARCH_UNDEFINED_LABEL'];
        
        if( !is_array($this->body_name_list) ){
            return $result;
        }
        
        foreach($this->body_name_list as $k => $v ){
            if( $k == $body_id ){
                $result = $v;
                break;
            }
        }
        
        return $result;
    }
    
    /**
     * Public method: get car list
     * <br>---------------------------------------------------------------------
     * @return array
     */
    public function getCarList(){
        return $this->car_list;
    }
    
    /**
     * Public method: get form status
     * <br>---------------------------------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }

    /**
     * Public method: get form message 
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /**
     * Public method: get form style
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }
}