<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
include('view/3rdparty/pagination/pagination.php');
class publicForwarderDirectory
{
    protected $lang;
    protected $group = 'public';
    protected $slug = 'buyer-directory';
    protected $form_status;
    protected $form_message;
    protected $form_style;
    var $traderList;
    private $car;
    var $total_num_row;
    var $pagination_html;
    var $current_page;
    var $total_page;
    var $member_type1="Forwarder";
    /*
     * Constructor -------------------------------------------------------------
     */
    public function publicForwarderDirectory($lang=array())
    {
        $this->lang = $lang;
        $this->loadTraderList();
    }

    function loadDealweek(){
        
        require_once BASE_CLASS . 'class-connect.php';
      
        $cnx = new Connect();
        $cnx->open();
        
        $sql_search="SELECT * from deal_week  ORDER BY id DESC limit 5";
        
        if( !$sql = @mysql_query($sql_search) ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load car maker list due a query error at ' . __FILE__ . ':' . __LINE__);
            $cnx->close();
            return;
        }

        if( @mysql_num_rows($sql) < 1 ){
            $cnx->close();
        }
        
         
       $this->deal_week = array();
       
        while( $r = @mysql_fetch_assoc($sql)){
           
            array_push($this->deal_week, $r);
          
        }
         
  
        @mysql_free_result($sql);
        $cnx->close();
        return $this->deal_week; 
        
    }


    /*
     * Public method: get page content data ------------------------------------
     * @return array | false.
     */
    public function getPageHTML()
    {
        require_once BASE_ROOT . 'core/class-connect.php';
        //echo $this->slug;
        $cnx = new Connect();
        $cnx->open();

        if( !$sql = @mysql_query("SELECT * FROM `page` WHERE `page_slug`='$this->slug' AND `page_group`='$this->group' LIMIT 1;") )
        {
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }

        if( @mysql_num_rows($sql) != 1 )
        {
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }

        $r = @mysql_fetch_assoc($sql);

        $result                     = array();
        $result['id']               = $r['id'];
        $result['meta_keywords']    = $r['meta_keywords'];
        $result['meta_description'] = $r['meta_description'];
        $result['meta_robots']      = $r['meta_robots'];
        $result['page_title']       = $r['page_title'];
        $result['page_slug']        = $r['page_slug'];
        $result['page_publish']     = $r['page_publish'];
        $result['page_author']      = $r['page_author'];
        $result['creation']         = $r['creation'];
        $result['editable']         = $r['editable'];
        $result['page_group']       = $r['page_group'];
        $result['html']             = $r['html'];

        @mysql_free_result($sql);
        $cnx->close();

        return $result;
    }
    
    /*
     * Public method: get load trader data ------------------------------------
     * @return array | false.
     */
    private function loadTraderList(){
        require_once BASE_ROOT . 'core/class-connect.php';
        $traderList=array();
        $cnx = new Connect();
        $cnx->open();
        $where="";

        if(isset($_GET['country'])) $country=mysql_real_escape_string(stripcslashes($_GET['country'])); else $country="";
        if(isset($_GET['keyword'])) $keyword=mysql_real_escape_string(stripcslashes($_GET['keyword'])); else $keyword="";
        //if(isset($_GET['business_type'])) $business_type=mysql_real_escape_string(stripcslashes($_GET['business_type'])); else $business_type="";
        //if(isset($_GET['product_type'])) $product_type=mysql_real_escape_string(stripcslashes($_GET['product_type'])); else $product_type="";
        // if(isset($_GET['condition'])) $condition=mysql_real_escape_string(stripcslashes($_GET['condition'])); else $condition="";
        // if(isset($_GET['steering'])) $steering=mysql_real_escape_string(stripcslashes($_GET['steering'])); else $steering="";
        // if(isset($_GET['fuel_type'])) $fuel_type=mysql_real_escape_string(stripcslashes($_GET['fuel_type'])); else $fuel_type="";
        // if(isset($_GET['min_price'])) $min_price=mysql_real_escape_string(stripcslashes($_GET['min_price'])); else $min_price="";
        // if(isset($_GET['max_price'])) $max_price=mysql_real_escape_string(stripcslashes($_GET['max_price'])); else $max_price="";
        // if(isset($_GET['make'])) $make=mysql_real_escape_string(stripcslashes($_GET['make'])); else $make="";
        // if(isset($_GET['category'])) $category=mysql_real_escape_string(stripcslashes($_GET['category'])); else $category="";

        if(!empty($country)){
            $where.=" AND `rg`.`country`='$country' ";
        }
        if(!empty($keyword)){
            $where.=" AND (`rg`.`card_id` LIKE '%$keyword%' OR `rg`.`name` LIKE '%$keyword%' OR `rg`.`company_name` LIKE '%$keyword%' OR `rg`.`introduction` LIKE '%$keyword%') ";
        }
        // if(!empty($business_type)){
        //     $where.=" AND `rg`.`business_type` LIKE '%$business_type%' ";
        // }
        // if(!empty($product_type)){
        //     $where.=" AND `inq`.`vehicle_type` LIKE '%$product_type%' ";
        // }
        // if(!empty($condition)){
        //     $where.=" AND `pd`.`condition` = '$condition' ";
        // }
        // if(!empty($steering)){
        //     $where.=" AND `pd`.`steering` = '$steering' ";
        // }
        // if(!empty($fuel_type)){
        //     $where.=" AND `pd`.`fuel_type` = '$fuel_type' ";
        // }
        // if(!empty($min_price)){
        //     $where.=" AND `pd`.`price` > '$min_price' ";
        // }
        // if(!empty($max_price)){
        //     $where.=" AND `pd`.`price` < '$max_price' ";
        // }
        // if(!empty($make)){
        //     $where.=" AND `pd`.`make` = '$make' ";
        // }
        // if(!empty($category)){
        //     $where.=" AND `pd`.`category` = '$category' ";
        // }
        $query = "SELECT rg.*, 
        cl.country_name as country_name
        FROM register as `rg` 
        INNER JOIN country_list AS cl
        ON rg.country=cl.cc 
        LEFT JOIN shipping_info as si
        ON si.owner=rg.id
        WHERE member_type1 LIKE '%$this->member_type1%' $where  
        GROUP BY `rg`.`id`
        ";
        //echo $query;
        //echo $query;
        /////PAGINATION PROCESS//////

        $sql_count_str=$query;
        $sql_count = @mysql_query($sql_count_str);
        $this->total_num_row= @mysql_num_rows($sql_count);
        
        $links = new Pagination ($this->total_num_row);
        $limit=$links->start_display;
        $this->pagination_html= $links->display(); 
        $this->current_page = $links->currentPage();
        $this->total_page= $links->numPages();
        ///////PAGINATION PROCESS///////
        $query.=$limit;  
       //echo $query;
        $result = mysql_query($query);
        
        
        //echo $query;
        $this->traderList = array();
        while( $r = @mysql_fetch_assoc($result) ){
          //echo 'ok';
            array_push($traderList, $r);
        }
        //echo mysql_error();
        //var_dump($this->userInfo);
        $this->traderList=$traderList;
        $cnx->close();
    }
    function countCountry($product_type=""){
        require_once dirname(dirname(__FILE__)) . '/config.php';
        require_once BASE_CLASS . 'class-connect.php';

        $cnx = new Connect();
        $cnx->open();
        $and="";
        $countcountry=array();
        $and=" AND (`member_type1` LIKE '%seller%' OR `member_type1` LIKE '%forwarder%')";
        if(!empty($product_type)){
            $and.= " AND `pd`.`product_type` = '$product_type'";
        }
        

        //if(!empty($vehicle_type)){
        //  $and.= " AND `vehicle_type`='$vehicle_type'";
        //}

        
        $sql_str="
                SELECT country, cl.country_name, COUNT(DISTINCT(rg.id)) as `number`
                FROM `register` as `rg`
                LEFT JOIN `product` as `pd` ON `pd`.`owner`=`rg`.`id`
                INNER JOIN `country_list` as cl ON `cl`.`cc`=`rg`.`country`
                WHERE 1$and
                GROUP BY `rg`.`country`
                ORDER BY `number` DESC
                LIMIT 0, 10
                ";

        
        if( !$sql = @mysql_query($sql_str) ){
            return 0;
        }
        
        while($row=@mysql_fetch_assoc($sql)){
            array_push($countcountry, $row);
        }
        
        @mysql_free_result($sql);
        $cnx->close();
        return $countcountry;

    }
    /*
     * Public method: get number by condition data ------------------------------------
     * @return array | false.
     */
    public function getForwarderNumberByCountry(){
        require_once BASE_ROOT . 'core/class-connect.php';
        $sellerNumber=array();
        $cnx = new Connect();
        $cnx->open();  
        $where="";
        $count="";
        $join="";

        $query = "SELECT `country`, COUNT(rg.id) AS `number` FROM register as rg 
                WHERE rg.member_type1 LIKE '%$this->member_type1%' GROUP BY rg.`country` ";
        $result = mysql_query($query);
        //echo $query;
        while( $r = @mysql_fetch_assoc($result) ){
          //echo 'ok';
            $sellerNumber[$r['country']]=$r['number'];
        }
        //echo mysql_error();
        //var_dump($this->userInfo);
        return $sellerNumber;
        $cnx->close();
    }
    
    /*
     * Public method: get number by condition like data ------------------------------------
     * @return array | false.
     */
    public function getSellerNumberLike($column, $value){
        require_once BASE_ROOT . 'core/class-connect.php';
        $sellerNumber=0;
        $cnx = new Connect();
        $cnx->open();  
        $query = "SELECT COUNT(*) AS `number` FROM register WHERE member_type1 LIKE '%$this->member_type1%' AND `$column` LIKE '%$value%'";
        $result = mysql_query($query);
        
        while( $r = @mysql_fetch_assoc($result) ){
            //echo 'ok';
            $sellerNumber = $r['number'];
        }
        //echo mysql_error();
        //var_dump($this->userInfo);

        $cnx->close();
        return $sellerNumber;
        
    }

    /*
     * Public method: get number by condition like data ------------------------------------
     * @return array | false.
     */
    public function getNumberByProductType(){
        require_once BASE_ROOT . 'core/class-connect.php';
        $sellerNumber=array();
        $cnx = new Connect();
        $cnx->open();  
        $query = "SELECT inq.vehicle_type, (SELECT COUNT(DISTINCT(owner)) FROM inquiry WHERE vehicle_type=inq.vehicle_type) AS number 
                FROM register as rg 
                INNER JOIN inquiry as inq ON inq.owner=rg.id 
                WHERE rg.member_type1 LIKE '%$this->member_type1%'
                GROUP BY inq.vehicle_type  "
        ;
        //echo $query;
        $result = mysql_query($query);
        
        while( $r = @mysql_fetch_assoc($result) ){
            //echo 'ok';
            $sellerNumber[$r['vehicle_type']]=$r['number'];
        }
        //echo mysql_error();
        //var_dump($this->userInfo);

        $cnx->close();
        return $sellerNumber;
        
    }
    /*
     * Public method: get number by condition like data ------------------------------------
     * @return array | false.
     */
    public function getNumberByProduct($column){
        require_once BASE_ROOT . 'core/class-connect.php';
        $sellerNumber=array();
        $cnx = new Connect();
        $cnx->open(); 
        $where_product_type=""; 
        if(isset($_GET['product_type'])) $product_type=mysql_real_escape_string(stripcslashes($_GET['product_type'])); else $product_type="";
        if(!empty($product_type)){
             $where_product_type=" AND product_type='$product_type'";
        }
        $query = "SELECT pd.`$column`, (SELECT COUNT(DISTINCT(owner)) FROM product WHERE `$column`=pd.`$column`$where_product_type) AS number 
                FROM register as rg 
                LEFT JOIN product as pd ON pd.owner=rg.id 
                WHERE rg.member_type1 LIKE '%$this->member_type1%'
                GROUP BY pd.`$column`  "
        ;
        //echo $query."<br/>";
        $result = mysql_query($query);
        
        while( $r = @mysql_fetch_assoc($result) ){
            //echo 'ok';
            $sellerNumber[$r[$column]]=$r['number'];
        }
        //echo mysql_error();
        //var_dump($this->userInfo);

        $cnx->close();
        return $sellerNumber;
        
    }
    /*
     * Public method: get number by price compare data ------------------------------------
     * @return array | false.
     */
    public function getNumberByPrice($price, $op="="){
        require_once BASE_ROOT . 'core/class-connect.php';
        $traderList=array();
        $cnx = new Connect();
        $cnx->open();
        $where="";
        
        if(isset($_GET['product_type'])) $product_type=mysql_real_escape_string(stripcslashes($_GET['product_type'])); else $product_type="";
       
        if(!empty($product_type)){
            $where.=" AND `pd`.`product_type` LIKE '%$product_type%' ";
        }
        $where.=" AND `pd`.`price`{$op}'$price' ";
        $query = "SELECT rg.*, 
        cl.country_name as country_name
        FROM register as `rg` 
        INNER JOIN country_list AS cl
        ON rg.country=cl.cc 
        LEFT JOIN product as pd
        ON pd.owner=rg.id
        WHERE member_type1 LIKE '%$this->member_type1%' $where  
        GROUP BY `rg`.`id`
        ";
        /////PAGINATION PROCESS//////

        $sql_count_str=$query;
        $sql_count = @mysql_query($sql_count_str);
        return @mysql_num_rows($sql_count);
    }
    
    /*
     * Public method: get form status ------------------------------------------
     * @return bool.
     */
    public function getTraderList()
    {
        return $this->traderList;
    }

    /*
     * Public method: get form status ------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }

    /*
     * Public method: get form message -----------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /*
     * Public method: get form style -------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }
    /*
     * Public method: get vehicle data -------------------------------------------
     * @return string.
     */

}
?>
