<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
class usercarmanagereditfeatures {
    private $lang;
    private $form_message;
    private $form_status;
    private $form_style;
    private $car_id;
    private $car;
    private $feature_list;
    private $all_feature_list;
    
    /**
     * Constructor
     * <br>---------------------------------------------------------------------
     */
    public function usercarmanagereditfeatures($lang=array()){
        $this->lang = $lang;
        
        // validate if car id has been declared and load list.
        $this->car_id = trim($_GET['cid']);
        $this->car = array();
        $this->feature_list = array();
        $this->all_feature_list = array();
        
        if( empty($this->car_id) ){
            $this->media_list = array();
            $this->form_message = $this->lang['EDIT_DETAILS_INVALID_VEHICLE_ID_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-error';
            return;
        }
        else {
            // handle update form.
            if( isset($_POST['savebtn']) ){
                $this->handleUpdateForm();
            }
            
            // load details list.
            $this->loadFeatureList();
        }
    }
    
    /**
     * Private method: handle update vehicle form
     * <br>---------------------------------------------------------------------
     * @return void
     */
    private function handleUpdateForm(){
        $feature = (array)$_POST['featureInput'];
        $author = (int)$_SESSION['log_id'];
        // first, remove all feature icons.
        require_once BASE_CLASS . 'class-connect.php';
        $cnx = new Connect();
        $cnx->open();
        
        if( !@mysql_query("DELETE FROM `car_features` WHERE `car_id`='$this->car_id' AND `author`=$author") ){
            $cnx->close();
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to update vehicle features due a query error at ' . __FILE__ . ':' . __LINE__);
            $this->form_message = $this->lang['EDIT_FEAT_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        
        // now add selected icons.
        for( $i=0; $i < count($feature); $i++ ){
            $data = base64_decode($feature[$i]);
            $data = explode(':',$data);
            $icon = $data[1];
            $label = $data[0];
            
            $label = @mysql_real_escape_string($label);
            
            if( empty($icon) || empty($label) ){
                continue;
            }
            else {
                if( !@mysql_query("INSERT INTO `car_features` (`car_id`, `author`,`feat_name`,`feat_icon`) VALUES ('$this->car_id', $author, '$label','$icon')") ){
                    require_once BASE_CLASS . 'class-log.php';
                    LogReport::write('Unable to insert vehicle features to features table due a query error at ' . __FILE__ . ':' . __LINE__);
                }
            }
        }
        
        $cnx->close();
        $this->form_message = $this->lang['EDIT_FEAT_QUERY_SUCCESS'];
        $this->form_status = true;
        $this->form_style = 'alert-success';
        return;
    }
    
    /**
     * Private method: load feature list
     * <br>---------------------------------------------------------------------
     * @return void
     */
    private function loadFeatureList(){
        // load vehicle info data first.
        require_once BASE_CLASS . 'class-connect.php';
        $author = (int)$_SESSION['log_id'];
        $this->car = array();
        $this->feature_list = array();
        
        $cnx = new Connect();
        $cnx->open();
        
        $this->car_id = @mysql_real_escape_string($this->car_id);
        
        if( !$sql = @mysql_query("SELECT `id`,`maker`,`model`,`year` FROM `car` WHERE `id`='$this->car_id' AND `author`=$author LIMIT 1;") ){
            $cnx->close();
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load vehicle feature list due a query error at ' . __FILE__ . ':' . __LINE__);
            $this->form_message = $this->lang['EDIT_FEAT_LOAD_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        
        if( @mysql_num_rows($sql) != 1 ){
            $cnx->close();
            return;
        }
        
        $r = @mysql_fetch_assoc($sql);
        
        $this->car['id'] = $r['id'];
        $this->car['maker'] = stripslashes($r['maker']);
        $this->car['model'] = stripslashes($r['model']);
        $this->car['year'] = $r['year'];
        
        @mysql_free_result($sql);
        
        // load feature list.
        if( !$sql = @mysql_query("SELECT * FROM `car_features` WHERE `car_id`='$this->car_id' AND `author`=$author") ){
            $cnx->close();
            $this->car = array();
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load vehicle feature list due a query error at ' . __FILE__ . ':' . __LINE__);
            $this->form_message = $this->lang['EDIT_FEAT_LOAD_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        
        while( $r = @mysql_fetch_assoc($sql) ){
            $obj = array(
                'id' => $r['id'],
                'car_id' => $r['car_id'],
                'feat_name' => stripslashes($r['feat_name']),
                'feat_icon' => $r['feat_icon']
            );
            array_push($this->feature_list,$obj);
        }
        
        @mysql_free_result($sql);
        
        // load feature list (all items)
        if( !$sql = @mysql_query("SELECT * FROM `feature_list` ORDER BY `label` ASC") ){
            $cnx->close();
            $this->car = array();
            $this->feature_list = array();
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load feature list due a query error at ' . __FILE__ . ':' . __LINE__);
            $this->form_message = $this->lang['EDIT_FEAT_LOAD_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        
        while( $r = @mysql_fetch_assoc($sql) ){
            $obj = array(
                'id' => $r['id'],
                'label' => stripslashes($r['label']),
                'source' => $r['source']
            );
            array_push($this->all_feature_list,$obj);
        }
        
        @mysql_free_result($sql);
        
        $cnx->close();
        return;
    }

    /**
     * Public method: get vehicle information
     * <br>---------------------------------------------------------------------
     * @return array
     */
    public function getVehicle(){
        return $this->car;
    }

    /**
     * Public method: get all featured list
     * <br>---------------------------------------------------------------------
     * @return array
     */
    public function getAllFeaturedList(){
        return $this->all_feature_list;
    }
    
    /**
     * Public method: get features list
     * <br>---------------------------------------------------------------------
     * @return array
     */
    public function getFeatureList(){
        return $this->feature_list;
    }

    /**
     * Public method: get form status
     * <br>---------------------------------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }

    /**
     * Public method: get form message 
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /**
     * Public method: get form style
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }
}