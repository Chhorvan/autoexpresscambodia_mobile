<?php
if(!isset($_SESSION)) @session_start();

################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
require_once BASE_CLASS .'class-social-network.php';
class publicsocialtest
{
   	
    var $user_profile;
    var $loggedInStatus=false;
    /*
     * Constructor -------------------------------------------------------------
     */
    public function publicsocialtest($lang=array())
    {
    	$social_network=new socialnetwork();

        if(isset($_GET['provider'])){
            //If user not yet logged in call login page

            $this->loggedInStatus=$social_network->loggedInStatus($_GET['provider']);
            if(!$this->loggedInStatus){
                $social_network->handleLoginPage($_GET['provider']);
            }else{
                $this->user_profile=$social_network->getUserProfile();

            }
        }
    }
    //optional 
    public function getSocialLoginGroup(){
    	$social_network=new socialnetwork();
    	return $social_network->getSocialLoginGroup();
    }
    //optional 
    public function getSocialProfileLink(){
    	$social_network=new socialnetwork();
    	return $social_network->getSocialProfileLink();
    }
    
    public function getUserProfile(){
        return $this->user_profile;
    }
}
