<?php
if(!isset($_SESSION)) @session_start();
class publiccontactsellermobile
{
	public $stock_detail;
	public $buyer_detail;
	public $result='';

		 /*
     * Constructor -------------------------------------------------------------
     */
    public function publiccontactsellermobile($lang=array())
    {

		$this->getstock_detail();
		if($this->isLoggedIn()){
			$this->getBuyerDetail();
			if($this->resubmit()){
				$this->sendEmail();
			}elseif(isset($_POST['submit'])){
				//include('ajax/push_tmp_product_inquiry_session.php');
				$this->sendEmail();
				Header("Location: contact-seller?result=".$this->result);
			}
		}

		if( isset($_POST['loginBtn']) )
        {
        	extract($_POST);
            $this->handleLoginSubmit();
            $this->sendEmail();
				Header("Location: contact-seller?result=".$this->result);
        }

	}
	protected function handleLoginSubmit()
    {
     $cid="";
        if(isset($_GET['cid']))
        $cid=$_GET['cid'];
        if(isset($_POST['product_id'])) $product_id=$_POST['product_id']; else $product_id='';
       $pro='?cid='.$product_id."&action=resubmit";

        $email = trim($_POST['emailInput']);
        $pass  = trim($_POST['passwordInput']);
        require_once BASE_CLASS . 'class-utilities.php';


        if( strlen($pass) < 5 || strlen($pass) > 20 )
        {
            $this->error_status = true;
            $this->error_type = 'error';

            $this->error_formname = 'login';
            return;
        }

        // check if email matches the super admin's.
        if( $email == ADMIN_CONTACT_EMAIL )
        {
            // validate admin password.
            require_once BASE_CLASS . 'class-connect.php';

            $db = new Connect();
            $db->open();

            $pass = md5($pass);
            $email = mysql_real_escape_string($email);

            if( !$sql = mysql_query("SELECT `admin_email`, `admin_pass` FROM `setting` WHERE `admin_email`='$email' AND `admin_pass`='$pass'") )
            {
                require_once BASE_CLASS . 'class-log.php';

                LogReport::write('Unable to query database at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

                $db->close();

                $this->error_status = true;
                $this->error_type = 'warning';
                $this->error_message = $this->lang['LOGIN_QUERY_ERROR'];
                $this->error_formname = 'login';
                return;
            }

            if( @mysql_num_rows($sql) != 1 )
            {
                $db->close();

                $this->error_status = true;
                $this->error_type = 'error';
                $this->error_message = $this->lang['LOGIN_INVALID_EMAIL'];
                $this->error_formname = 'login';
                return;
            }
            else
            {
                // set admin session.
                $_SESSION['log_group'] = 'admin';
                $_SESSION['log_name']  = 'Administrator';
                $_SESSION['log_id']    = -1;
                $_SESSION['log_email'] = $email;
                 $_SESSION['tmp_product_id']=$_POST['product_id'];


                 $this->sendEmail();
				Header("Location: contact-seller?result=".$this->result);

               // Header("Location: contact-seller".$pro);
                exit;
            }
        }
        // user is not administrator.
        else
        {
            require_once BASE_CLASS . 'class-connect.php';

            $db = new Connect();
            $db->open();

            $pass = md5($pass);
            $email = mysql_real_escape_string($email);


            //echo $email."Test Email";
            if( !$sql = mysql_query("SELECT * FROM `register` WHERE `log_id`='$email' AND `password`='$pass'") )
            {
                require_once BASE_CLASS . 'class-log.php';

                LogReport::write('Unable to query database at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

                $db->close();

                $this->error_status = true;
                $this->error_type = 'warning';
                $this->error_message = $this->lang['LOGIN_QUERY_ERROR'];
                $this->error_formname = 'login';
                return;
            }

            if( @mysql_num_rows($sql) != 1 )
            {
                $db->close();

                $this->error_status = true;
                $this->error_type = 'error';
                $this->error_message = $this->lang['LOGIN_INVALID_EMAIL'];
                $this->error_formname = 'login';
                return;
            }

            // check if account is active and user is not banned.
            $r = mysql_fetch_assoc($sql);

            @mysql_free_result($sql);
            $db->close();

            if( (bool)$r['banned'] )
            {
                $this->error_status = true;
                $this->error_type = 'warning';
                $this->error_message = $this->lang['LOGIN_BANNED_ACCOUNT'];
                $this->error_formname = 'login';
                return;
            }

            if( !(bool)$r['activated'] )
            {
                $this->error_status = true;
                $this->error_type = 'warning';
                $this->error_message = $this->lang['LOGIN_NON_ACTIVE_ACCOUNT'];
                $this->error_formname = 'login';
                return;
            }

            // set login session.
            $_SESSION['log_group'] = $r['group'];
            $_SESSION['log_name']  = $r['name'];
			$_SESSION['company_name']=$r['company_name'];
			$_SESSION['log_id']    = $r['id'];
            $_SESSION['userlog_id']    = $r['log_id'];
            $_SESSION['log_email'] = $r['email'];
			$_SESSION['register_type'] = $r['register_type'];
			$_SESSION['pro']=$pro;
			$_SESSION['tmp_product_id']=$_POST['product_id'];
			$_SESSION['tmp_message_input']=$_POST['messageInput'];
			$_SESSION['tmp_subject_input']=$_POST['subjectInput'];

			// $this->sendEmail();
			// Header("Location: contact-seller?result=".$this->result);





			$db = new Connect();
            $db->open();

			$query="SELECT * FROM register_types WHERE register_types.code='".$_SESSION['register_type']."'";
			$result = mysql_query($query);

			$row = mysql_fetch_array($result);

			$_SESSION['user_max_image_upload'] = $row['max_image'];
			$_SESSION['user_max_pro'] = $row['max_product'];
			$_SESSION['tmp_product_id']=$_POST['product_id'];
			$_SESSION['tmp_message_input']=$_POST['messageInput'];
			$_SESSION['tmp_subject_input']=$_POST['subjectInput'];
			$_SESSION['log_email']=$r['email'];

			//$this->sendEmail();

			Header("Location: contact-seller?cid=".$_POST['product_id']."&action=submit");



           // Header("Location: contact-seller".$pro);
            exit;
        }
    }


	//Get product that buyer want by id
	private function getstock_detail(){
		require_once BASE_CLASS . 'class-connect.php';

		$cid="";
		if(isset($_GET['cid']))
		$cid=$_GET['cid'];

        $cnx = new Connect();
        $cnx->open();

		$query = "SELECT p.*, c.source as carImg, c.thumb as thumb, cl.country_name as country,
                            cl.cc as flag ,cl.country_name, rg.company_name, rg.name, rg.email
                            FROM product as p
                            LEFT JOIN car_media as c on p.id=c.product_id AND c.primary_photo='1' OR c.primary_photo='2'
                            INNER JOIN country_list as cl on p.location=cl.cc
                            LEFT JOIN register as rg on p.owner=rg.id
							WHERE p.id='$cid'";
		$result = mysql_query($query);
		if(!$result){
			require_once BASE_CLASS . 'class-log.php';
            $cnx->close();

			$this->form_message = $this->lang['MANAGE_NEWS_LOAD_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
		}

		$this->stock_detail = array();

		while( $r = @mysql_fetch_assoc($result) ){
            array_push($this->stock_detail, $r);
		}

		$cnx->close();

	}
	//Check if the user is already logged in
	private function isLoggedIn(){
		if(isset($_SESSION['log_group'])) $log_group=$_SESSION['log_group']; else $log_group="";
		if($log_group=='user'){
			return true;
		}
	}
	//Get buyer detail if user is logged in
	private function getBuyerDetail(){
		if(isset($_SESSION['log_id'])) $log_id=$_SESSION['log_id']; else $log_id="";
		require_once BASE_CLASS . 'class-connect.php';
        $buyer_detail=array();
        $cnx = new Connect();
        $cnx->open();
		$sql="SELECT rg.email FROM register rg WHERE id='$log_id'";
		$result=mysql_query($sql);
		while($row=mysql_fetch_assoc($result)){
			array_push($buyer_detail, $row);
		}
		$this->buyer_detail=$buyer_detail['0'];
		$cnx->close();
	}
	//Process sending email if user is logged in and clicked on submit button
	private function sendEmail(){
		//Get all values by input
		if(isset($_POST['product_id'])) $product_id=$_POST['product_id']; else $product_id='';
		if(isset($_POST['subjectInput'])) $subjectInput=$_POST['subjectInput']; else $subjectInput='';
		if(isset($_POST['messageInput'])) $messageInput=$_POST['messageInput']; else $messageInput='';

		//Get all values by session if exists
		if(isset($_SESSION['tmp_product_id'])) $product_id=$_SESSION['tmp_product_id'];
		if(isset($_SESSION['tmp_subject_input'])) $subjectInput=$_SESSION['tmp_subject_input'];
		if(isset($_SESSION['tmp_message_input'])) $messageInput=$_SESSION['tmp_message_input'];

		if(isset($_SESSION['log_email'])) $log_email=$_SESSION['log_email']; else $log_email='';
		$from=$log_email;

		//$from = $this->buyer_detail['email'];
		$to=$this->stock_detail['email'];
		//var_dump($this->stock_detail);

		$to='nochneang@gmail.com';

		//$to='kysengkean@gmail.com';


		require BASE_CLASS . 'class-user-sendmail.php';

		$send = new UserSendEmail();
		$send->setMessage($messageInput);
    	$send->setSubject($subjectInput);
    	$send->setEmail($to);
    	$send->setSellerCompany($this->stock_detail['0']['company_name']);
    	$send->setEmailFrom($from);
    	$send->setModelYear($this->stock_detail['0']['model_year']);
    	$send->setModel($this->stock_detail['0']['model']);
    	$send->setMake($this->stock_detail['0']['make']);
   		$send->setId($this->stock_detail['0']['id']);
		$send->setProductImage($this->stock_detail['0']['thumb']);
		//var_dump($send);
		$send->send();
		//echo $send->viewMessage();
		$this->result="success";
		//Clear all inquiry after sending email
		$this->clearTmpSession();


	}
	private function clearTmpSession(){
		if(isset($_SESSION['tmp_product_id'])) unset($_SESSION['tmp_product_id']);
		if(isset($_SESSION['tmp_subject_input'])) unset($_SESSION['tmp_subject_input']);
		if(isset($_SESSION['tmp_message_input'])) unset($_SESSION['tmp_message_input']);
	}
	private function resubmit(){

		if(isset($_GET['cid'])) $id=$_GET['cid']; else $id='';
		if(isset($_GET['action'])) $action=$_GET['action']; else $action='';
		if(isset($_SESSION['tmp_product_id'])) $tmp_id=$_SESSION['tmp_product_id']; else $tmp_id='';

		if($action=='submit'&&$id==$tmp_id){
			return true;
		}
	}
}
?>
