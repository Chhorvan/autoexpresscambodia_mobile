<?php

if(!isset($_SESSION)) @session_start();
################################################################################
### This disclaimer must be kept intact in order to use this active_product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
require BASE_CLASS . 'class-message.php';
require BASE_CLASS . 'class-product.php';
class publichome
{
    protected $lang;
    protected $group = 'public';
    protected $slug = 'home';
	private $hit_counter;
    private $car_maker_list;
	private $car_maker_price;
    private $company_info;
    protected $totalNumberUnreadMsg=0;
    private $used_vehicle;
	private $new_vehicle;
	private $vehicleoffer;
	private $loadcarNews;
	private $vehicle_info;
	private $load_dealer;

    public function publichome($lang=array())
    {
        $_SESSION['loginError']='';


        $this->lang = $lang;

        $this->loadusedcar();
		$this->loadnewcar();
		$this->loadspecialoffer();
		$this->loadcarNews();
		$this->loadVehicleType();
		$this->loaddealer();
		$this->load_make();

         if( isset($_SESSION['fb_id']) && isset($_SESSION['log_email']))
        {
            $_SESSION['log_group']='user';
        }

		// check if login form has been submitted ------------------------------
        if( isset($_POST['loginBtn']) )
        {
             $this->handleLoginSubmit();

        }
        if(isset($_SESSION['log_id'])){
            $this->loadUnreadMsgNumber();
        }

    }

	/*
     * Protected method: handle login form submit ------------------------------
     * @return void.
     */
  protected function handleLoginSubmit()
    {
        $email = trim($_POST['emailInput']);
        $pass  = trim($_POST['passwordInput']);
        if(isset($_GET['pg'])) $currentPage= urldecode($_GET['pg']); else $currentPage= BASE_RELATIVE;
        require_once BASE_CLASS . 'class-utilities.php';
        if( strlen($pass) < 5 || strlen($pass) > 20 )
        {
            $this->error_status = true;
            $this->error_type = 'error';
            // $this->error_message = $this->lang['LOGIN_INVALID_EMAIL'];
            $this->error_formname = 'login';
            return;
        }

        // check if email matches the super admin's.
        if( $email == ADMIN_CONTACT_EMAIL )
        {
            // validate admin password.
            require_once BASE_CLASS . 'class-connect.php';

            $db = new Connect();
            $db->open();

            $pass = md5($pass);
            $email = mysql_real_escape_string($email);

            if( !$sql = mysql_query("SELECT `admin_email`, `admin_pass` FROM `setting` WHERE `admin_email`='$email' AND `admin_pass`='$pass'") )
            {
                $db->close();
            }else{
                  header("Location: " . BASE_RELATIVE . $currentPage);
                exit;
            }
            if( @mysql_num_rows($sql) != 1 )
            {
                $db->close();
                $this->error_status = true;
                $this->error_type = 'error';
                $this->error_message = $this->lang['LOGIN_INVALID_EMAIL'];
                $this->error_formname = 'login';
                return;
            }
            else
            {
                $_SESSION['log_group'] = 'admin';
                $_SESSION['log_name']  = 'Administrator';
                $_SESSION['log_id']    = -1;
                $_SESSION['log_email'] = $email;
                header("Location: " . BASE_RELATIVE . $currentPage);
                exit;
            }
        }
        // user is not administrator.
        else
        {
            require_once BASE_CLASS . 'class-connect.php';

            $db = new Connect();
            $db->open();

            $pass = md5($pass);
            $email = mysql_real_escape_string($email);
            //echo $email."Test Email";
            if( !$sql = mysql_query("SELECT banned,activated,session,token,`group`,company_name,register.user_id,log_id,email,register_type,max_image,max_product FROM register LEFT JOIN register_types
on register.register_type=register_types.`code` WHERE `log_id`='$email' AND `password`='$pass'") )
            {
               require_once BASE_CLASS . 'class-log.php';

                LogReport::write('Unable to query database at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());
                $this->error_status = true;
                $this->error_type = 'warning';
                $this->error_message = $this->lang['LOGIN_QUERY_ERROR'];
                $this->error_formname = 'login';
                return;
                $db->close();
            }
            if( @mysql_num_rows($sql) != 1 )
            {
                $_SESSION['loginError']='Invalid ID or password.';
                $this->error_status = true;
                $this->error_type = 'error';
                // $this->error_message = $this->lang['LOGIN_INVALID_EMAIL'];
                $this->error_formname = 'login';
                return;
                $db->close();
            }
            // check if account is active and user is not banned.
            $r = mysql_fetch_assoc($sql);

            @mysql_free_result($sql);
            $db->close();
            if( (bool)$r['banned'] )
            {
                $this->error_status = true;
                $this->error_type = 'warning';
                $this->error_message = $this->lang['LOGIN_BANNED_ACCOUNT'];
                $this->error_formname = 'login';
                return;
            }
            if( !(bool)$r['activated'] )
            {
                $this->error_status = true;
                $this->error_type = 'warning';
                $this->error_message = $this->lang['LOGIN_NON_ACTIVE_ACCOUNT'];
                $this->error_formname = 'login';
                return;
            }
            //Check if user allow to remember the account
            if(isset($_POST['remember_me'])){
                if(empty($r['session'])){
                    if(empty($r['token'])){
                        $r['token']=strtotime("now");
                    }
                    $r['session']=md5($r['log_id']."^".$r['password']."^".$r['token']);
                    $this->updateSessionInDatabase($r['id'], $r['token'], $r['session']);
                }
                setcookie('ussid', $r['session'], strtotime( '+30 days' ) );
            }
            // set login session.
            $_SESSION['log_group'] = $r['group'];
            $_SESSION['log_name']  = $r['name'];
            $_SESSION['company_name']=$r['company_name'];
            $_SESSION['log_id']    = $r['id'];
            $_SESSION['userlog_id']    = $r['log_id'];
            $_SESSION['log_email'] = $r['email'];
            $_SESSION['register_type'] = $r['register_type'];
			$_SESSION['user_max_image_upload'] = $r['max_image'];
            $_SESSION['user_max_pro'] = $r['max_product'];

        }
    }
	function loadMembershiptype(){
        require_once dirname(dirname(__FILE__)) . '/config.php';
        require_once BASE_CLASS . 'class-connect.php';

        $cnx = new Connect();
        $cnx->open();

        $membershiptype=array();
        $sql_str="
                SELECT code,title
                FROM register_types
                ORDER BY code ASC
                ";
        if( !$sql = @mysql_query($sql_str) ){
            return 0;
        }

        while($row=@mysql_fetch_assoc($sql)){
            array_push($membershiptype, $row);
        }

        @mysql_free_result($sql);
        $cnx->close();
        return $membershiptype;
    }
	private function loadVehicleType(){
		require_once BASE_CLASS . 'class-connect.php';
		$cnx = new Connect();
		$cnx->open();

		$sql="SELECT product.body_style,product.make,product.model FROM product";
		$query=@mysql_query($sql);
		$select_vehicle=array();
		while($r=@mysql_fetch_assoc($query)){
				array_push($select_vehicle,$r);
		}
		$this->vehicle_info=$select_vehicle;
		@mysql_free_result($sql);
		$cnx->close();

	}
	public function getVehicleType(){
		return $this->vehicle_info;
	}

    private function loadusedcar(){
					   require_once BASE_CLASS . 'class-connect.php';

						$cnx = new Connect();
						$cnx->open();

						$sql_search="SELECT

								active_product.car_id,
								active_product.desc1,
								active_product.status,
								
								
								active_product.car_type,
								active_product.chassis_no,
								active_product.hit_count,
								active_product.price,
								active_product.currency,
								active_product.make,
								active_product.model,
								active_product.year,
								active_product.price,
								active_product.location,
								
							   active_product.product_type,
							   active_product.exterior_color,
							   active_product.created_date,
							   active_product.transmission,
							   active_product.fuel_type,
							   active_product.mileage

				,product_primary_photo.thumb_url,country_list.country_name,country_list.cc, register.company_name,register.user_id as comid from active_product INNER JOIN register on active_product.`owner`=register.user_id LEFT JOIN product_primary_photo on active_product.car_id=product_primary_photo.car_id INNER JOIN country_list on active_product.location=country_list.cc WHERE active_product.car_type = '0' GROUP BY active_product.car_id ORDER BY created_date DESC limit 0,8";


                    	//$this->hit_counter=mysql_real_escape_string($_POST['hit_count']);
						if( !$sql = @mysql_query($sql_search) ){
							require_once BASE_CLASS . 'class-log.php';
							LogReport::write('Unable to load used Product list due to a query error at ' . __FILE__ . ':' . __LINE__);
							$cnx->close();
							return;
						}
						// echo  $sql_search;

						if( @mysql_num_rows($sql) < 1 ){
							$cnx->close();
						}

						$cache_used_products = array();
						while( $r = @mysql_fetch_assoc($sql) ){
							array_push($cache_used_products, $r);
						}

						//var_dump($this->vehicle);
						$this->vehicle_used=$cache_used_products;
						@mysql_free_result($sql);
						$cnx->close();

    }

 private function loadnewcar(){
					   require_once BASE_CLASS . 'class-connect.php';

						$cnx = new Connect();
						$cnx->open();

						$sql_search="SELECT

								active_product.car_id,
								active_product.desc1,
								active_product.status,
								
								
								active_product.car_type,
								active_product.chassis_no,
								active_product.hit_count,
								active_product.price,
								active_product.currency,
								active_product.make,
								active_product.model,
								active_product.year,
								active_product.price,
								active_product.location,
								
							   active_product.product_type,
							   active_product.exterior_color,
							   active_product.created_date,
							   active_product.transmission,
							   active_product.fuel_type,
							   active_product.mileage

				,product_primary_photo.thumb_url,country_list.country_name,country_list.cc, register.company_name,register.user_id as comid from active_product INNER JOIN register on active_product.`owner`=register.user_id LEFT JOIN product_primary_photo on active_product.car_id=product_primary_photo.car_id INNER JOIN country_list on active_product.location=country_list.cc WHERE active_product.car_type = '1' GROUP BY active_product.car_id ORDER BY created_date DESC limit 0,8";


                    	//$this->hit_counter=mysql_real_escape_string($_POST['hit_count']);
						if( !$sql = @mysql_query($sql_search) ){
							require_once BASE_CLASS . 'class-log.php';
							LogReport::write('Unable to load new Product list due to a query error at ' . __FILE__ . ':' . __LINE__);
							$cnx->close();
							return;
						}
						// echo  $sql_search;

						if( @mysql_num_rows($sql) < 1 ){
							$cnx->close();
						}

						$cache_new_products = array();
						while( $r = @mysql_fetch_assoc($sql) ){
							array_push($cache_new_products, $r);
						}

						//var_dump($this->vehicle);
						$this->vehicle_new=$cache_new_products;
						@mysql_free_result($sql);
						$cnx->close();

    }

 private function loadspecialoffer(){
					   require_once BASE_CLASS . 'class-connect.php';
						
						$cnx = new Connect();
						$cnx->open();
						
						$mysql = "select count(car_id) as count from active_product  ";
						$myquery = @mysql_query($mysql);
						if (@mysql_num_rows($myquery)>0){
							$myrow = @mysql_fetch_assoc($myquery);
							$ranNum = intval($myrow['count']/24);
							$time = date("h");
							$start = $time;
							if ($start== 0){
								$start =1;
								}
							else {
							$start = $start * $ranNum;
							}
							}
						else {
							$start = 0;
							}
						
						
						
						$sql_search="SELECT

								active_product.car_id,
								active_product.desc1,
								
								active_product.chassis_no,
								active_product.hit_count,
								active_product.status,
								active_product.price,
								active_product.make,
								active_product.model,
								active_product.year,
								active_product.price,
								
							   active_product.product_type,
							   active_product.exterior_color,
							   active_product.special_offer,
							   active_product.fuel_type,
							   active_product.mileage

				,product_primary_photo.thumb_url,country_list.country_name,country_list.cc, register.company_name,register.user_id as comid from active_product INNER JOIN register on active_product.`owner`=register.user_id LEFT JOIN product_primary_photo on active_product.car_id=product_primary_photo.car_id INNER JOIN country_list on active_product.location=country_list.cc where active_product.special_offer = 0 GROUP BY active_product.car_id ORDER BY active_product.special_offer DESC limit $start,$ranNum";


                    	//$this->hit_counter=mysql_real_escape_string($_POST['hit_count']);
						if( !$sql = @mysql_query($sql_search) ){
							require_once BASE_CLASS . 'class-log.php';
							LogReport::write('Unable to load Recent Product list due to a query error at ' . __FILE__ . ':' . __LINE__);
							$cnx->close();
							return;
						}


						if( @mysql_num_rows($sql) < 1 ){
							$cnx->close();
						}

						$cache_offer_products = array();
						while( $r = @mysql_fetch_assoc($sql) ){
							array_push($cache_offer_products, $r);
						}


						$this->vehicleoffer=$cache_offer_products;
						@mysql_free_result($sql);
						$cnx->close();

    }
 private function loadcarNews(){
					   require_once BASE_CLASS . 'class-connect.php';

						$cnx = new Connect();
						$cnx->open();

						$sql_search="SELECT id, title, html, short_description, date from news where publish = 1 order by date DESC";


                    	//$this->hit_counter=mysql_real_escape_string($_POST['hit_count']);
						if( !$sql = @mysql_query($sql_search) ){
							require_once BASE_CLASS . 'class-log.php';
							LogReport::write('Unable to load Recent Product list due to a query error at ' . __FILE__ . ':' . __LINE__);
							$cnx->close();
							return;
						}


						if( @mysql_num_rows($sql) < 1 ){
							$cnx->close();
						}

						$cache_carNews = array();
						while( $r = @mysql_fetch_assoc($sql) ){
							array_push($cache_carNews, $r);
						}


						$this->carNews=$cache_carNews;
						@mysql_free_result($sql);
						$cnx->close();

    }
	private function loaddealer(){
	   require_once BASE_CLASS . 'class-connect.php';

		$cnx = new Connect();
		$cnx->open();

		$sql_search="SELECT 
		register.user_id, 
		register.username, 
		register.email,
		register.company_name,
		register.mobile,
		register.image, count(product.owner) as total from register left join product on register.user_id = product.owner where register.`activated` = '1' AND register.`group` = 'user' AND is_deleted='0' OR is_deleted IS NULL group by register.user_id order by count(product.owner) DESC limit 0,3";


		//$this->hit_counter=mysql_real_escape_string($_POST['hit_count']);
		if( !$sql = @mysql_query($sql_search) ){
			require_once BASE_CLASS . 'class-log.php';
			LogReport::write('Unable to load Recent Product list due to a query error at ' . __FILE__ . ':' . __LINE__);
			$cnx->close();
			return;
		}


		if( @mysql_num_rows($sql) < 1 ){
			$cnx->close();
		}

		$cache_dealer = array();
		while( $r = @mysql_fetch_assoc($sql) ){
			$total_count = $r['total'];	
		
			$mysql = "select * from more_image where owner ='".$r['user_id']."' and primary_photo=1";
			$myquery = @mysql_query($mysql);
			if (@mysql_num_rows($myquery)>0){
				$myimage = @mysql_fetch_assoc($myquery);
				$cache_dealer[]=array('user_id'=>$r['user_id'],'username'=>$r['username'],'company_name'=>$r['company_name'],'email'=>$r['email'],
				'mobile'=>$r['mobile'],
				'count'=>$total_count,
				'image'=>$myimage['m_image']
				);
			}
			else {
				$cache_dealer[]=array('user_id'=>$r['user_id'],'username'=>$r['username'],'email'=>$r['email'],
				'mobile'=>$r['mobile'],
				'company_name'=>$r['company_name'],
				'count'=>$total_count,
				'image'=>""
				);
				
			}
		}


		$this->dealer=$cache_dealer;
		@mysql_free_result($sql);
		$cnx->close();
	}	
    /**
     * Private method: load car maker list.
     * <br>---------------------------------------------------------------------
     * @return void.
     */

    /**
     * Private method: load buyer's inquiry list.
     * <br>---------------------------------------------------------------------
     * @return void.
     */

	 function loadcategory(){
        require_once dirname(dirname(__FILE__)) . '/config.php';
        require_once BASE_CLASS . 'class-connect.php';

        $cnx = new Connect();
        $cnx->open();

        $category=array();
        $sql_str="
                SELECT name,title
                FROM product_type_list
                ORDER BY name ASC

                ";


        if( !$sql = @mysql_query($sql_str) ){
            return 0;
        }

        while($row=@mysql_fetch_assoc($sql)){
            array_push($category, $row);
        }

        @mysql_free_result($sql);
        $cnx->close();
        return $category;

    }
	 public function getMyInfo(){
					   require_once BASE_CLASS . 'class-connect.php';
						$log_id=(int)$_SESSION['log_id'];
						$log_email=mysql_real_escape_string($_SESSION['log_email']);
						$myinfo=array();
						$cnx = new Connect();
						$cnx->open();
						$query="SELECT `rg`.`company_name` ,
									(SELECT COALESCE(SUM(car_count +
										truck_count +
										bus_count +
										equipment_count +
										part_count +
										accessories_count +
										motorbike_count +
										aircraft_count +
										watercraft_count), 0) FROM data_count WHERE register_id='$log_id')  AS `product_number`,

									(SELECT COUNT(*) FROM `messages` WHERE `receiver`='$log_id' AND `seen`='0') AS `unread_inbox`,
									(SELECT COUNT(*) FROM `messages` WHERE `receiver`='$log_id' AND `seen`='1') AS `read_inbox`

								FROM `register` AS `rg`
								LEFT JOIN `messages` AS `msg` ON `msg`.`sender`=`rg`.`user_id`

								WHERE `rg`.`user_id`='$log_id'

								";

						if( !$sql = @mysql_query($query) ){

							$cnx->close();
							return;
						}else{
							 while( $r = @mysql_fetch_assoc($sql) ){
								array_push( $myinfo, $r);
							 }
						}
						$cache_myinfo=$myinfo;
						@mysql_free_result($sql);

						$cnx->close();
		 return $myinfo;
	 }
	 
	// car's make
	public function load_make(){
        require_once dirname(dirname(__FILE__)) . '/config.php';
        require_once BASE_CLASS . 'class-connect.php';

        $cnx = new Connect();
        $cnx->open();

        $vehicle_type=array();
        $sql_str="
               SELECT  active_product.make , COUNT(active_product.make) total_make from active_product GROUP BY make ORDER BY total_make DESC

                ";


        if( !$sql = @mysql_query($sql_str) ){
            return 0;
        }

        while($row=@mysql_fetch_assoc($sql)){
            array_push($vehicle_type, $row);
        }

        @mysql_free_result($sql);
        $cnx->close();
        return $vehicle_type;

    } 
	 
    private function loadUnreadMsgNumber(){
        if(isset($_SESSION['log_id'])) $log_id=$_SESSION['log_id'];
        $myMessage=new Message();
        $myMessage->loadTotalNumberUnreadMessages();
        $this->totalNumberUnreadMsg=$myMessage->getTotalNumberUnreadMessages();
    }
	public function getUnreadMsgNumber(){
       return $this->totalNumberUnreadMsg;
    }
    public function getMyProductNumber(){
        if(isset($_SESSION['log_id'])) $log_id=$_SESSION['log_id'];
        $myProduct=new Product();
        /* return $myProduct->getTotalNumber(array("owner='$log_id'")); */
		return $myProduct->getTotalNumber(array("owner='$log_id'"));
    }
    /**
     * Public method: get car maker list.
     * <br>---------------------------------------------------------------------
     * @return array
     */
    public function getCar(){
        return $this->car_maker_list;
    }
    /**
     * Public method: get buyer inquiry list.
     * <br>---------------------------------------------------------------------
     * @return array.
     */
    public function getBuyerInquiryList(){
        return $this->buyer_inquiry_list;
    }

	 public function getCarPrice(){
        return $this->car_maker_price;
    }

    /**
     * Public method: get company informatiion.
     * <br>---------------------------------------------------------------------
     * @return array.
     */
    public function getCompanyInformation(){
        return $this->company_info;
    }

    /*
     * Public method: get form status ------------------------------------------
     * @return bool.
     */

    /*
     * Public method: get form message -----------------------------------------
     * @return string.
     */


    /*
     * Public method: get form style -------------------------------------------
     * @return string.
     */
    public function getVehicle_used(){
        return $this->vehicle_used;
    }

	public function getVehicle_new(){
        return $this->vehicle_new;
    }

	 public function specialoffer(){
        return $this->vehicleoffer;
    }
	public function loadNews(){
        return $this->carNews;
    }
	public function load_dealer(){
        return $this->dealer;
    }
}
