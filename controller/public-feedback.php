<?php 
if(!isset($_SESSION)) @session_start();
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################

class publicfeedback
{
	
	
    protected $register_form_status;    // tracks registration process (complete/error)
    protected $_LANG;
    protected $country_list;
	protected $lang;
    protected $error_status = false;
    protected $error_message;
    protected $error_type;
    protected $error_formname;
    protected $cdata; // holds the tid and ref passed to login page if accessed through
                      // the edit_ds page.
    var $redirect_url;
	var $invalid_log;
	
	var $store_temporary_id;
	//var $store_temporary_comany_name;
	//var $store_temporary_name;
	//var $store_temporary_email;
	//var $store_tempory_comment;
	
    protected $existedSocialAccount=false;
    /*
     * Constructor -------------------------------------------------------------
     */
    public function publicfeedback($lang=array())
    {
        // if group session is set, take user to the home page.
        if( isset($_SESSION['log_group']) && $_SESSION['log_group'] != 'public' )
        {
            header("Location: " . BASE_RELATIVE . DEFAULT_HOME_SLUG);
            exit;
        }
		
		
		 // if group session is set, take user to the home page. no double login!
        if( isset($_SESSION['log_group']) && $_SESSION['log_group'] != 'public'  && $_SESSION['log_group'] != 'admin' )
        {
            header("Location: " . BASE_RELATIVE);
            exit;
        }
        if( isset($_SESSION['log_group']) && $_SESSION['log_group'] == 'admin' )
        {
            header("Location: " . BASE_RELATIVE);
            exit;
        }
		
        // initialize status.
        $this->register_form_status = '';

        // set language vars.
        $this->_LANG = $lang;

        // if feedback form is submit, process it.
        if( isset($_POST['feedback-send']) )
        {		
				//$this->processFeedbackForm();
				$this->processFeedbackForm();

		}
		
		
		
	
	
	
		
		
	
		
		// check if login form has been submitted ------------------------------
        if( isset($_POST['loginBtn']) )
        {
            $this->handleLoginSubmit();
			  //$this->user_login();
        }

    }
	
	////Select Province
//	
//	public function load_province(){
//		require_once BASE_CLASS . 'class-connect.php';
//		$db = new Connect();
//        $db->open();
//		$sql = @mysql_query("SELECT province_name FROM province");
//		$this->province_list = array();
//
//        while( $r = @mysql_fetch_assoc($sql) ){
//            array_push($this->province_list, $r);
//        }
//        @mysql_free_result($sql);
//		return $this->province_list;
//        $cnx->close();	
//	}
	
	
	
	
	
	
	//Process Login Function.
		/*public function user_login(){
			$id_user = trim($_POST['emailInput']);
			$pass  = md5(trim($_POST['passwordInput']));	
			$this->validate_user($id_user,$pass);
			
			echo md5('81dc9bdb52d04dc20036dbd8313ed055');
	
		}
		
		public function validate_user( $id_user_input,$pass_user_input){
		
		    require_once BASE_CLASS . 'class-connect.php';
		    $db = new Connect();
            $db->open();
			$sql = @mysql_query("SELECT log_id,password,activated FROM register WHERE log_id = '$id_user_input' AND password='$pass_user_input' AND activated='1' ");
			$num=@mysql_num_rows($sql);
			if($num>0){
				echo"Welcome";
			}
			else{
				$_SESSION['validate']="Incorrect";
			}	
		}
	*/
		
	
	
	
	
	

    /*
     * Process Feedback form -----------------------------------------------------
     * @return void.
     */
	protected function processFeedbackForm(){
		//condition for valid and invalid.
	  
	   
	   
		//require_once BASE_CLASS .'resize-class.php';
		// Initialize
	
		$log_id = "";
		$name = "";
		$email = "";
		$email_error = "";
		$confirm_email = "";
		$company_name = "";
		$homepage = "";
		$technical = "";
		$product = "";
		$contact = "";
		$revisit = "";
		$recommend_site = "";
		$visit_our_site = "";
		$comment = "";
		extract($_POST);
		
		//$passMD5 = md5($password);
		$email1 = "support@angkorauto.com";
		
		$email1=trim($email1);
					
					require BASE_CLASS . 'mail/PHPMailer.php';
					require BASE_CLASS . 'mail/PHPMailerPlugins/SMTP.class.php';
					
					$message = "";
					$subject = "";	
					$to_1 = "support@angkorauto.com";
					$to_2 = $email1;										
					//$active_link = '<a href="'.BASE_SITE.'activate/?account='.base64_encode($email).'">' . $this->_LANG['SENDEMAIL_ACTVATE_NOW'] . '</a><br>';
					
					//$subject = $this->_LANG['SENDEMAIL_SUBJECT_LABEL'];
					
					/* $message.="Thank you for registering with Angkorauto.<br/>";
					$message.="Please click on the vertification link below to activate your account.<br/>"; */
					$message.='
						<html>							
							<head>
								<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
								<title>Email confirm</title>

								<style>
									
									*{
										margin:0px;
										padding:0px;
									}
									body,div,dl,dt,dd,ul,ol,li,h1,h2,h3,h4,h5,h6,pre,
									form,fieldset,input,textarea,p,blockquote,th,td { 
									  margin:0;
									  padding:0;  
									}

									div,dl,dt,dd,ul,ol,li,h1,h2,h3,h4,h5,h6,pre,
									form,fieldset,input,textarea,p,blockquote,th,td {
									  font-size: 100%;
									}
									p,pre{
										margin: 0;
										letter-spacing:0;
									}
									table {
									  border-collapse:collapse;
									  border-spacing:0;
									}
									fieldset,img { 
									  border:0;
									}
									address,caption,cite,code,dfn,em,strong,th,var {
									  font-style:normal;
									  font-weight:normal;
									}
									ol,ul {
									  list-style:none;
									  margin: 0;
									  padding: 0;
									}
									caption,th {
									  text-align:left;
									}
									h1,h2,h3,h4,h5,h6 {
									  font-weight:normal;
									}
									q:before,q:after {
									  content:"";
									}
									abbr,acronym {
									  border:0;
									}

									img,
									input[type="image"] {
									  vertical-align: bottom;	  
									}
									
									img {
										display:block;
									}

									a {
									  color: blue;
									  text-decoration: underline;
									}

									input[type="submit"],
									input[type="button"],
									input[type="text"],
									input[type="password"],
									textarea {
									  margin:0;
									  padding:0;
									}
									input,
									select,
									textarea {
									  font-size: 100%; /* for iPhone */
									}
									input[type="text"],
									input[type="password"] {
									  vertical-align: baseline;
									}
								</style>

								</head>

								<body style="margin:0 auto; padding:0;">
									<div style="margin:0 auto;line-height:10px;padding:0;width:650px;">
										<div style="width:650px; margin:0 auto; padding:0 auto; background:#eeeeee;">
											
											
											
											<div style="border-bottom:1px dashed #CCC;width:600px;margin:0 auto; ">
											<h2 style="padding:20px 0;text-align:center;color:#2c65b6;font-size:24px;font-weight:bold;">Customer\'s Feedback</h2>
												<p style="color:#555454; line-height:15px">
                                                Name: '.$name.'<br/><br/>
                                                Company: '.$company_name.'<br/><br/>
                                                Email: '.$email.'<br/><br/></p>
                                                
                                                <br/><br/><br/><p style="color:#555454; line-height:15px">
                                                1. Rate our website on the following aspects: <br/><br/>
                                                Homepage : <span style="color:#06F">'.$homepage.'</span><br/><br/>
                                                Technical support : <span style="color:#06F">'.$technical.'</span><br/><br/>
                                                Product Information : <span style="color:#06F">'.$product.'</span><br/><br/>
                                                Contacts : <span style="color:#06F">'.$contact.'</span><br/><br/></p>
                                                
                                                <br/><br/><br/><p style="color:#555454; line-height:15px">
                                                2. How likely are you to: <br/><br/>
                                           		Revisit this site on a regular basis : <span style="color:#06F">'.$revisit.'</span><br/><br/>
                                                Recommend our site : <span style="color:#06F">'.$recommend_site.'</span><br/><br/></p>
                                                
                                                
                                                <br/><br/><br/><p style="color:#555454; line-height:15px">
                                                3. How often do you visit our site?<br/><br/>
                                                <span style="color:#06F">'.$visit_our_site.'</span><br/><br/></p>
                                                
                                                <br/><br/><br/><p style="color:#555454; line-height:15px">
                                                4. Suggestion<br/><br/>
                                             
                                                '.$comment.'<br/><br/></p>
												
												<p style="clear:both"></p>
											</div>
                                            
                                            
                                            
											
											<p style="text-align:center;font-size:14px;padding:15px 0 20px 0;">Copyright © 2014 Angkorauto. All Rights Reserved by Softbloom co., ltd</p>
										</div>
									</div>
								</body>
						</html>
					';					
					
					$mail_send = new PHPMailer();
					$mail_send->IsSMTP();
					$mail_send->Host     = "ssl://angkorauto.com";
					$mail_send->Port     = "465";
					$mail_send->SMTPAuth = "yes";
					$mail_send->Username = "support@angkorauto.com";
					$mail_send->Password = "Angkorauto168";
					$mail_send->FromName = "angkorauto.com";
					$mail_send->From     = $to_1;
					$mail_send->AddAddress($to_2);		
					$mail_send->Subject  = $subject;		
					$mail_send->MsgHTML($message);
					$mail_send->IsHTML(true);		
					$mail_send->Send();	
					if(!$mail_send->Send()) {
					   echo "Error sending: " . $mail_send->ErrorInfo;					   					   
					}else {
				
		
						require_once BASE_CLASS . 'class-connect.php';
						$db = new Connect();
						$db->open();
		
		if( !$sql = @mysql_query("INSERT INTO `feedback` (`name`,`email`,`company_name`,`home_page`, `technical_support`, `product_info`, `contact`, `revisit_site_on_regular_basis`, `recommend_site`,`visit_our_site`, `comment`, `date`)			VALUES('$name','$email','$company_name','$homepage','$technical','$product','$contact','$revisit','$recommend_site','$visit_our_site','$comment','".date('Y-m-d')."')") ) {

							
							require_once BASE_CLASS . 'class-log.php';
							LogReport::write('Unable to query database at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

							$db->close();
							$this->register_form_status = 'query';

							return;
						}
					
						header("Location: " .BASE_RELATIVE ."feedback-success");
						exit;					
					
				}
	   	 
} 	//End of Process Form



	
	

	
 

    /*
     * Getter: get form status -------------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->error_status;
    }

    /*
     * Getter: get form status type --------------------------------------------
     * @return string (error | warning | complete).
     */
    public function getFormStatusType()
    {
        return $this->error_type;
    }

    /*
     * Getter: get form status message -----------------------------------------
     * @return string.
     */
    public function getStatusMessage()
    {
        return $this->error_message;
    }

    /*
     * Getter: get form name ---------------------------------------------------
     * @return string.
     */
    public function getFormName()
    {
        return $this->error_formname;
    }
	
    /**
     * Public method: get country list
     * <br>---------------------------------------------------------------------
     * @return array
     */
	public function getCountryList(){
        return $this->country_list;
    }
    /*
     * Get registration status -------------------------------------------------
     * @return string.
     */
    public function getRegistrationStatus()
    {
        return $this->register_form_status;
    }
}