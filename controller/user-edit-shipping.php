<?php
if(!isset($_SESSION)) @session_start();
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################

class usereditshipping {
    protected $lang;
    protected $form_status;
    protected $form_message;
    protected $form_style;
   
    //protected $feature_icon;
	protected $country_list;
    protected $vessel_type_list;
    protected $line_list;
    
	var $invalidShippingInfo=false;

    /**
     * Constructor
     * <br>---------------------------------------------------------------------
     */

    public function usereditshipping($lang=array()){
        $this->lang = $lang;
        
        // define car id.
        require_once BASE_CLASS . 'class-utilities.php';
        
         // load country list.
		$this->loadCountryList();
		$this->loadVesselTypeList();
	       $this->loadLineList();
     
        if( isset($_POST['savebtn']) ){
            $this->handleSaveForm();
        }
    }
 
    
    /**
     * Private method: handle save form
     * <br>---------------------------------------------------------------------
     * @return void.
     */
    private function handleSaveForm(){        
        

        ( isset($_GET['cid'])                       ?   $cid = (int)trim($_GET['cid']) : $cid = '' );
		( isset($_POST['countryLoadingSelect'])     ? 	$countryLoadingSelect = trim($_POST['countryLoadingSelect']) : $countryLoadingSelect = '' );
		( isset($_POST['portLoadingSelect'])       	? 	$portLoadingSelect = trim($_POST['portLoadingSelect']) : $portLoadingSelect = '' );
		( isset($_POST['portLoadingInput'])       	? 	$portLoadingInput = trim($_POST['portLoadingInput']) : $portLoadingInput = '' );
		( isset($_POST['countryDestSelect'])       	? 	$countryDestSelect = trim($_POST['countryDestSelect']) : $countryDestSelect = '' );
		( isset($_POST['portDestSelect'])       	? 	$portDestSelect = trim($_POST['portDestSelect']) : $portDestSelect = '' );
		( isset($_POST['portDestInput'])       		? 	$portDestInput = trim($_POST['portDestInput']) : $portDestInput = '' );
		( isset($_POST['vesselTypeSelect'])       	? 	$vesselTypeSelect = trim($_POST['vesselTypeSelect']) : $vesselTypeSelect = '' );
		( isset($_POST['vesselTypeInput'])       	? 	$vesselTypeInput = trim($_POST['vesselTypeInput']) : $vesselTypeInput = '' );
		( isset($_POST['sizeSelect'])       		? 	$sizeSelect = trim($_POST['sizeSelect']) : $sizeSelect = '' );
		( isset($_POST['sizeInput'])       			? 	$sizeInput = trim($_POST['sizeInput']) : $sizeInput = '' );
		( isset($_POST['oceanFreightInput'])       	? 	$oceanFreightInput = (int)trim($_POST['oceanFreightInput']) : $oceanFreightInput = 0 );
		( isset($_POST['transitTimeSelect'])       	? 	$transitTimeSelect = (int)trim($_POST['transitTimeSelect']) : $transitTimeSelect = 0 );
		( isset($_POST['lineSelect'])       		? 	$lineSelect = trim($_POST['lineSelect']) : $lineSelect = '' );
		( isset($_POST['lineInput'])       			? 	$lineInput = trim($_POST['lineInput']) : $lineInput = '' );
		( isset($_POST['remarkInput'])       		? 	$remarkInput = trim($_POST['remarkInput']) : $remarkInput = '' );
		
        // strip all slashes.
		
        $cid                    = stripslashes($cid);
		$countryLoadingSelect  	= stripslashes($countryLoadingSelect);
		$portLoadingSelect     	= stripslashes($portLoadingSelect);
		$portLoadingInput     	= stripslashes($portLoadingInput);
		$countryDestSelect     	= stripslashes($countryDestSelect);
		$portDestSelect     	= stripslashes($portDestSelect);
		$portDestInput     		= stripslashes($portDestInput);
		$vesselTypeSelect     	= stripslashes($vesselTypeSelect);
		$vesselTypeInput     	= stripslashes($vesselTypeInput);
		$sizeSelect     		= stripslashes($sizeSelect);
		$sizeInput     			= stripslashes($sizeInput);
		$oceanFreightInput     	= stripslashes($oceanFreightInput);
		$transitTimeSelect     	= stripslashes($transitTimeSelect);
		$lineSelect     		= stripslashes($lineSelect);
		$lineInput     			= stripslashes($lineInput);
		$remarkInput     		= stripslashes($remarkInput);
		 
		
        $author						= (int)$_SESSION['log_id'];
		$owner					= (int)$_SESSION['log_id'];
		
        // validate required fields.
        /*if( empty($maker) && empty($maker_sel) ){
            $this->form_message = $this->lang['PAGE_INVALID_MAKER_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-error';
            $this->removeMedia($car_id);
            return;
        }
		*/
        // define default value.
        // step 1.

       

        // save.
        require_once BASE_CLASS . 'class-connect.php';
        $cnx = new Connect();
        $cnx->open();
		
        $cid                    = @mysql_real_escape_string($cid);
		$countryLoadingSelect  	= @mysql_real_escape_string($countryLoadingSelect);
		$portLoadingSelect     	= @mysql_real_escape_string($portLoadingSelect);
		$portLoadingInput     	= @mysql_real_escape_string($portLoadingInput);
		$countryDestSelect     	= @mysql_real_escape_string($countryDestSelect);
		$portDestSelect     	= @mysql_real_escape_string($portDestSelect);
		$portDestInput     		= @mysql_real_escape_string($portDestInput);
		$vesselTypeSelect     	= @mysql_real_escape_string($vesselTypeSelect);
		$vesselTypeInput     	= @mysql_real_escape_string($vesselTypeInput);
		$sizeSelect     		= @mysql_real_escape_string($sizeSelect);
		$sizeInput     			= @mysql_real_escape_string($sizeInput);
		$oceanFreightInput     	= @mysql_real_escape_string($oceanFreightInput);
		$transitTimeSelect     	= @mysql_real_escape_string($transitTimeSelect);
		$lineSelect     		= @mysql_real_escape_string($lineSelect);
		$lineInput     			= @mysql_real_escape_string($lineInput);
		$remarkInput     		= @mysql_real_escape_string($remarkInput);
		  
        // save to car table.
		
		if(!empty($portLoadingInput)){
			$portLoadingSelect=$portLoadingInput;
		}
		if(!empty($portDestInput)){
			$portDestSelect=$portDestInput;
		}
		if(!empty($vesselTypeInput)){
			$vesselTypeSelect=$vesselTypeInput;
		}
		if(!empty($sizeInput)){
			$sizeSelect=$sizeInput;
		}
		if(!empty($lineInput)){
			$lineSelect=$lineInput;
		}

		if( empty($countryLoadingSelect)
            ||empty($countryDestSelect)
            ||empty($portLoadingSelect)
            ||empty($portDestSelect)
            ||empty($vesselTypeSelect)
            ||empty($sizeSelect)
            ||empty($oceanFreightInput)
            ){
            require_once BASE_CLASS . 'class-log.php';
            $cnx->close();
            
            LogReport::write('Unable to save to car table due a query error at ' . __FILE__ . ':' . __LINE__);
            $this->form_message = $this->lang['PAGE_SAVE_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            
            return;
        }


		$sqlUpdate="UPDATE `shipping_info` SET `country_loading`='$countryLoadingSelect',
                                                `country_dest`='$countryDestSelect',
                                                `port_loading`='$portLoadingSelect',
                                                `port_dest`='$portDestSelect',
                                                `vessel_type`='$vesselTypeSelect',
                                                `size`='$sizeSelect',
                                                `ocean_freight_price`='$oceanFreightInput',
                                                `transit_time`='$transitTimeSelect',
                                                `line`='$lineSelect',
                                                `remark`='$remarkInput',
                                                `written_date`= NOW()
                                                WHERE `id`='$cid' AND `owner`='$owner'
                                             ";
		$sqlInsert="INSERT INTO `shipping_info` (
                                                `country_loading`,
												`country_dest`,
												`port_loading`,
                                                `port_dest`,
                                                `vessel_type`,
                                                `size`,
                                                `ocean_freight_price`,
                                                `transit_time`,
                                                `line`,
                                                `remark`,
                                                `owner`,
                                                `written_date`
                                             ) VALUES (
                                                '$countryLoadingSelect',
												'$countryDestSelect',
												'$portLoadingSelect',
                                                '$portDestSelect',
                                                '$vesselTypeSelect',
                                                '$sizeSelect',
                                                '$oceanFreightInput',
                                                '$transitTimeSelect',
                                                '$lineSelect',
                                                '$remarkInput',
                                                '$owner',
                                                NOW()
                                             )";
		//$sql=$sqlInsert;
        ///$sql="";
		if($this->existShippingInfo($cid)){
            $sql=$sqlUpdate;
        }else{
            $sql=$sqlInsert;
        }
		


        if( !@mysql_query($sql) ){
            require_once BASE_CLASS . 'class-log.php';
            
            $cnx->close();
			
            LogReport::write('Unable to save to car table due a query error at ' . __FILE__ . ':' . __LINE__);
            $this->form_message = $this->lang['PAGE_SAVE_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            
            return;
        }

        $cnx->close();
        
        $this->form_message =  $this->lang['PAGE_SAVE_QUERY_SUCCESS'];
        $this->form_status = true;
        $this->form_style = 'alert-success';
    }
    public function getShippingInfo($cid){
       
		$shippingInfo=array();
		$owner = (int)$_SESSION['log_id'];
		require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        if( !$sql = @mysql_query("SELECT * FROM `shipping_info` WHERE `id`='$cid' AND `owner`='$owner';") ){
            require_once BASE_CLASS . 'class-log.php';
            
            $cnx->close();
            return;
        }
        
        if( @mysql_num_rows($sql) < 1 ){
        	$this->invalidShippingInfo=true;
            $cnx->close();
            return;
        }
        
        
        while( $r = @mysql_fetch_assoc($sql) ){
           	foreach($r as $column=>$value) {
				$shippingInfo[$column]=$value;
			}
        }
        return $shippingInfo;
        @mysql_free_result($sql);
        $cnx->close();
      	
	
	}
    public function existShippingInfo($cid){
       
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        if( !$sql = @mysql_query("SELECT * FROM `shipping_info` WHERE `id`='$cid'") ){
            require_once BASE_CLASS . 'class-log.php';
            
            $cnx->close();
            return;
        }
        
        if( @mysql_num_rows($sql) > 0 ){
            
            
            return true;
        }
        
        
        
        @mysql_free_result($sql);
       
        
    
    }
    
	
    /**
     * Private method: load country list
     * <br>---------------------------------------------------------------------
     * @return array
     */
	private function loadCountryList(){
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        if( !$sql = @mysql_query("SELECT * FROM `country_list` ORDER BY `country_name` ASC") ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load country list due a query error at ' . __FILE__ . ':' . __LINE__);
            
            $this->country_list = array();
            
            return;
        }
       
        if( @mysql_num_rows($sql) < 0 ){
            $cnx->close();			
            return;
        }
        
        $this->country_list = array();
        
        while( $r = @mysql_fetch_assoc($sql) ){
            $obj['cc'][] = $r['cc'];
            $obj['country_name'][] = stripslashes($r['country_name']);
           
            
            $this->country_list=$obj;
        }
       
        @mysql_free_result($sql);
        $cnx->close();
    }
	/**
     * Private method: load vessel type list
     * <br>---------------------------------------------------------------------
     * @return array
     */
    private function loadVesselTypeList(){
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        if( !$sql = @mysql_query("SELECT DISTINCT `vessel_type` FROM `shipping_info` WHERE `vessel_type` <> '' ORDER BY `vessel_type` ASC") ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load vessel type due a query error at ' . __FILE__ . ':' . __LINE__);
            $this->vessel_type_list = array();
            return;
        }
       
        if( @mysql_num_rows($sql) < 0 ){
            $cnx->close();          
            return;
        }
        
        $this->vessel_type_list = array();
        
        while( $r = @mysql_fetch_assoc($sql) ){
            if($r['vessel_type']!="Other")  
                $this->vessel_type_list[]=$r['vessel_type'];
        }
       
        @mysql_free_result($sql);
        $cnx->close();
    }
    /**
     * Private method: load vessel type list
     * <br>---------------------------------------------------------------------
     * @return array
     */
    private function loadLineList(){
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        if( !$sql = @mysql_query("SELECT DISTINCT `line` FROM `shipping_info` WHERE `line` <> '' ORDER BY `line` ASC") ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load line list a query error at ' . __FILE__ . ':' . __LINE__);
            $this->line_list = array();
            return;
        }
       
        if( @mysql_num_rows($sql) < 0 ){
            $cnx->close();          
            return;
        }
        
        $this->line_list = array();
        
        while( $r = @mysql_fetch_assoc($sql) ){
            $this->line_list[]=$r['line'];
        }
       
        @mysql_free_result($sql);
        $cnx->close();
    }
    /**
     * Public method: get vessel type list
     * <br>---------------------------------------------------------------------
     * @return array
     */
    public function getVesselTypeList(){
        return $this->vessel_type_list;
    }
    /**
     * Public method: line list
     * <br>---------------------------------------------------------------------
     * @return array
     */
    public function getLineList(){
        return $this->line_list;
    }
    /**
     * Public method: get country list
     * <br>---------------------------------------------------------------------
     * @return array
     */
	public function getCountryList(){
        return $this->country_list;
    }
	
    public function getFormStatus()
    {
        return $this->form_status;
    }

    /**
     * Public method: get form message 
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /**
     * Public method: get form style
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }
}