<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
class userApplyPre {
    protected $lang;
    protected $form_status;
    protected $form_message;
    protected $form_style;
    protected $news;
	private $vehicle;
    
    /**
     * Constructor
     * <br>---------------------------------------------------------------------
     */
    public function userApplyPre($lang=array()){
        $this->lang = $lang;
        
        // this load news.
        $this->loadNews();
    }
	    
    /**
     * Private method: load last 10 news
     * <br>---------------------------------------------------------------------
     * @return void
     */
    private function loadNews(){
        require_once BASE_CLASS . 'class-connect.php';
        
        $this->news = array();
        
        $cnx = new Connect();
        $cnx->open();
        $query = "SELECT
                p.*,
                r.email,
                (SELECT source FROM car_media WHERE primary_photo = 1 and product_id = p.id LIMIT 1) AS carImg,
                cl.country_name AS country,
                cl.cc AS flag
				FROM ( product AS p LEFT JOIN car_media AS c ON p.id = c.product_id )
					LEFT JOIN country_list AS cl ON p.location = cl.cc
					INNER JOIN register AS r ON p.`owner` = r.id 
				WHERE p.id = '" . $_GET['p_id'] . "' AND r.email='".$_SESSION['log_email']. "'
				GROUP BY p.id";
        if( !$sql = @mysql_query($query) ){ 
            $cnx->close();
            
            require_once BASE_CLASS . 'class-log.php';
            
            LogReport::write('Unable to load news due a query error at ' . __FILE__ . ':' . __LINE__);
            
            $this->form_message = $this->lang['NEWS_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            
            return;
        }
        
        if( @mysql_num_rows($sql) < 1 ){
            $cnx->close();
            return;
        }
        
        while( $r = @mysql_fetch_assoc($sql) ){
            $obj = array(
                'id' => $r['id'],
                'title' => $r['model_year'] . ' ' . $r['make'] . ' '. $r['model'],
				'special_offer' => $r['special_offer'],
				'carImg' => $r['carImg']
            );
            
            array_push($this->news,$obj);
        }
        
        @mysql_free_result($sql);
        $cnx->close();
    }
    
    /**
     * Public method: get news list
     * <br>---------------------------------------------------------------------
     * @return array
     */
    public function getNews(){
        return $this->news;
    }
    
    /**
     * Public method: get form status
     * <br>---------------------------------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }
    
    /**
     * Public method: get form message
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }
    
    /**
     * Public method: get form style
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }
	 
	 public function getVehicleData(){
        return $this->news;
    }
	
	
	public function getbtnsend()
	{
	   return $this->btnsend;	
	}
	
	
}