<?php

################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
include('resize-class.php');

class userwriteinquiry
{
    protected $lang;
    protected $group = 'user';
    protected $slug = 'write-inquiry';
    protected $form_status;
    protected $form_message;
    protected $form_style;
    Protected $country_list;
    protected $inquiry_list;
    Protected $url;
    Protected $status="";
     
    /*
     * Constructor -------------------------------------------------------------
     */
    public function userwriteinquiry($lang=array())
    {
        $this->lang = $lang;
       
        $this->loadCountryList();
        $this->loadinquiry();

        if( isset($_POST['submit']) )
        { 
          $this->writeinquiry();
          //echo 'hello world';
        }

        if (isset($_POST['cancel'])) 
        {
            //echo 'hello world';
            header('Location:'.BASE_RELATIVE.'write-inquiry');
        }


    }

    public function loadinquiry()
    {
        if(isset($_GET['in_id']))
        {
            $id=$_GET['in_id'];
            require_once BASE_CLASS . 'class-connect.php'; 
			$owner_id=$_SESSION['log_id'];
            $cnx = new Connect();
            $cnx->open();

            if( !$sql = @mysql_query("SELECT i.*,c.`country_name`
                                      FROM `inquiry` i LEFT JOIN `country_list` c ON i.`country`=c.`cc` 
                                             
                                      WHERE i.`id`='$id' and owner='$owner_id';") )
            {
                require_once BASE_CLASS . 'class-log.php';
                LogReport::write('Unable to load car maker list due a query error at ' . __FILE__ . ':' . __LINE__);
                $cnx->close();
                return;
            }
            if( @mysql_num_rows($sql) < 1 ){
                $cnx->close();
            }
            $inquiry_list = array(); 
       
            while( $r = @mysql_fetch_assoc($sql)){
               
                array_push($inquiry_list, $r);
              
            }
             
      
            @mysql_free_result($sql);
            //var_dump($inquiry_list);
            $cnx->close();
            return $inquiry_list;           
            }
    }

    function loadExpertCompany(){       
       
        //echo $item.''.$category;
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
       
        
        ////////
        $sql_search="SELECT * from register ORDER BY id DESC limit 5"; 
        
        //echo $sql_search;
        if( !$sql = @mysql_query($sql_search) ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load car maker list due a query error at ' . __FILE__ . ':' . __LINE__);
            $cnx->close();
            return;
        }

        if( @mysql_num_rows($sql) < 1 ){
            $cnx->close();
        }
        
         
        $expertCompany = array(); 
       
        while( $r = @mysql_fetch_assoc($sql)){
           
            array_push($expertCompany, $r);
          
        }
         
  
        @mysql_free_result($sql);
        $cnx->close();
        return $expertCompany;

    }

    private function uploadfile(){

        $allowedExts = array("gif", "jpeg", "jpg", "png");
        $temp = explode(".", $_FILES["myfile"]["name"]);
        $extension = end($temp);
        if ((($_FILES["myfile"]["type"] == "image/gif")
        || ($_FILES["myfile"]["type"] == "image/jpeg")
        || ($_FILES["myfile"]["type"] == "image/jpg")
        || ($_FILES["myfile"]["type"] == "image/pjpeg")
        || ($_FILES["myfile"]["type"] == "image/x-png")
        || ($_FILES["myfile"]["type"] == "image/png"))
        && ($_FILES["myfile"]["size"] < 20480000)
        && in_array($extension, $allowedExts))
        {
            if ($_FILES["myfile"]["error"] > 0)
            {
                $this->form_message = $_FILES["myfile"]["error"];
                $this->form_status = true;
                $this->form_style = 'alert-error';
                $this->status="upload";
                //echo "Return Code: " . $_FILES["myfile"]["error"] . "<br>";
            }
            else
            {

                LogReport::write('File Upload Successfully ' . $_FILES["myfile"]["name"] . ':' . $_FILES["myfile"]["type"]. '. ' . ($_FILES["myfile"]["size"] / 1024).'kB' );
 
               //echo "Upload: " . $_FILES["file"]["name"] . "<br>";
               // echo "Type: " . $_FILES["file"]["type"] . "<br>";
               // echo "Size: " . ($_FILES["file"]["size"] / 1024) . " kB<br>";
               // echo "Temp file: " . $_FILES["file"]["tmp_name"] . "<br>";

                if (file_exists("images/my-inquiry/" . $_FILES["myfile"]["name"]))
                {
                    LogReport::write('This file is already exists' . $_FILES["myfile"]["name"] . ':' . $_FILES["myfile"]["type"]. '. ' . ($_FILES["myfile"]["size"] / 1024).'kB' );
                    $dtime = new DateTime(); 
                    $dtime->format('YYYY-mmm-dd H:i:s');
                    $timestamp = $dtime->getTimestamp();
                    $filename =  $timestamp.".". $extension;
                    move_uploaded_file($_FILES["myfile"]["tmp_name"],
                    "images/my-inquiry/" . $filename);
                    $main_image=new resize("images/my-inquiry/" . $filename);
                    $main_image->resizeImage(185, 105, 'crop');
                    $main_image->saveImage("images/my-inquiry/" . $filename, 100);  
                    $this->url="images/my-inquiry/".$filename;
                     
                }
                else 
                {
                   
                    $dtime = new DateTime(); 
                    $dtime->format('YYYY-mmm-dd H:i:s');
                    $timestamp = $dtime->getTimestamp();
                    $filename =  $timestamp.".". $extension;
                    move_uploaded_file($_FILES["myfile"]["tmp_name"],
                    "images/my-inquiry/" . $filename);
                    $main_image=new resize("images/my-inquiry/" . $filename);
                    $main_image->resizeImage(185, 105, 'crop');
                    $main_image->saveImage("images/my-inquiry/" . $filename, 100);
                   $this->url="images/my-inquiry/".$filename;
                     

                    //echo "Stored in: " . "upload/" . $_FILES["file"]["name"];
                }
            }
        }
        else
        {
             LogReport::write('This file is Invalid file' . $_FILES["myfile"]["name"] . ':' . $_FILES["myfile"]["type"]. '.' .($_FILES["myfile"]["size"] / 1024).'kB' );
                $this->form_message = 'This file is Invalid file.';
                $this->form_status = true;
                $this->form_style = 'alert-error';
             
            $this->status="upload";
            return;
        }

    }


    public function getstatus(){
        return $this->status;
    }

    public function getCountryList(){
        return $this->country_list;
    }
    /*
     * Public method: get page content data ------------------------------------
     * @return array | false.
     */
    protected function writeinquiry(){ 

        /*$vihicletype=$_POST['rdo-car-type'];
        echo $vihicletype;*/
        require_once BASE_CLASS . 'class-connect.php';
        // Initialize 
        $owner=$_SESSION['log_id'];
        //$dates=date('Y-m-d H:i:s');
        $vihicletype=$_POST['rdo-car-type']; 
        $condition=$_POST['rdo-car-condition'];

        $make1=$_POST['make'];
        $make="";
        $db1 = new Connect();
        $db1->open();
        if( !$sql1 = @mysql_query("SELECT `maker` FROM `product_maker` WHERE `id`='$make1';"))
        {
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load country list due a query error at ' . __FILE__ . ':' . __LINE__);
            $db1->close();
            $make=""; 
        }
        else
        {
            while( $r = @mysql_fetch_assoc($sql1) )
            {
                $make = $r['maker'];
            }
            @mysql_free_result($sql1);
            $db1->close();
             
        }  
       


        $model1=$_POST['model'];
        $model="";
        $db2 = new Connect();
        $db2->open();
        if( !$sql1 = @mysql_query("SELECT `model` FROM `product_model` WHERE `id`='$model1';"))
        {
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load country list due a query error at ' . __FILE__ . ':' . __LINE__);
            $db2->close();
            $model=""; 
        }
        else
        {
            while( $r = @mysql_fetch_assoc($sql1) )
            {
                $model = $r['model'];
            }
            @mysql_free_result($sql1);
            $db2->close();
             
        }


        $modelyearfrom=$_POST['model-year-from'];
        $modelyearto=$_POST['model-year-from'];
        $steering=$_POST['rdo-steering'];
        $transmission=$_POST['transmission'];
        $fueltype= $_POST['fuel-type'];
        $orderqty=$_POST['order-qty'];
        $country=$_POST['country'];
        $port=$_POST['port'];
        $pricefrom=$_POST['price-from'];
        if ($pricefrom==""){
            $pricefrom=0;
        }
        $priceto=$_POST['price-to'];
        $priceterms="";
        $wtc="";
        $paymentterms="";
        if (isset($_POST['priceterm']))
            {$priceterms=$_POST['priceterm'];
             $pric="";
             $i=0;
             foreach ($priceterms as $priceterm) {
                if($i==0){$pric.=$priceterm;}
                else{$pric.=",".$priceterm;}
                $i++;
             }
             $priceterms=$pric; 
            }
        
        if(isset($_POST['receive-wanted'])){
            $wtc=$_POST['receive-wanted']; 
            $s="";
            $i=0;
            foreach ($wtc as $wt) {
                if($i==0){$s.=$wt;}
                else{$s.=",".$wt;}
                $i++;
            }
            $wtc=$s;
        }

        if(isset($_POST['payment-term'])){
            $paymentterms=$_POST['payment-term']; 
            $p=""; 
            $i=0;
            foreach ($paymentterms as $paymentterm) {
                if($i==0){$p.= $paymentterm;}
                else{
                    $p.=",".$paymentterm;
                }
                
                 $i++;
            }

            $paymentterms=$p;
        } 
        
         
        $message=$_POST['message'];
        $this->uploadfile(); 
        $urls=$this->url;
        require_once BASE_CLASS . 'class-connect.php'; 
        $db = new Connect();
		$owner_id=$_SESSION['log_id'];
        $db->open();

        if(!isset($_GET['in_id']))
        {
            
            //echo "hello";
            if($make=="" || $model=="" || $modelyearfrom=="" ||
               $modelyearto=="" || $transmission=="" || $fueltype=="" ||
               $orderqty=="" || $country=="" || $port=="" || $pricefrom=="" ||
               $priceto=="" || $wtc=="" || $paymentterms=="" || $this->url=="")
            { 
               $this->status='unfill';
               return;
            }
            if($this->status !="")
            {
                return;
            }
 
            $query="INSERT INTO `inquiry` (`owner`,`dates`,`vehicle_type`, 
                            `condition`,`makid` ,`make`,`modelid`,`model`, `year_from`, `year_to`, 
                            `steering`, `transmission`, `fuel_type`, `order_quantity`, 
                            `country`, `port`, `price_from`, `price_to`, `price_term`, 
                            `want_to_receive`, `payment_terms`, `message`,`fileurl`) 
                    VALUES('$owner',now(),'$vihicletype', '$condition',$make1,
                     '$make',$model1 ,'$model', '$modelyearfrom', '$modelyearto', 
                     '$steering','$transmission', '$fueltype', 
                     '$orderqty','$country', '$port','$pricefrom','$priceto',
                     '$priceterms', '$wtc', '$paymentterms','$message','$urls')";

             } 
         else
         {
            $id=$_GET['in_id'];
            if($this->url=="")
            {
                $query="UPDATE `inquiry` set 
                `vehicle_type`='$vihicletype',`condition`='$condition',`makid`=$make1,
                `make`='$make',`modelid`=$model1,`model`='$model',`year_from`='$modelyearfrom',
                `year_to`='$modelyearto',`steering`='$steering',`transmission`='$transmission',
                `fuel_type`='$fueltype',`order_quantity`='$orderqty',`country`='$country',
                `port`='$port',`price_from`='$pricefrom',`price_to`='$priceto',`price_term`='$priceterms',
                `want_to_receive`='$wtc',`payment_terms`='$paymentterms',`message`='$message' WHERE id='$id' and owner='$owner_id';";
            }
            else
            {
                $query="UPDATE `inquiry` set 
                `vehicle_type`='$vihicletype',`condition`='$condition',`makid`=$make1,
                `make`='$make',`modelid`=$model1,`model`='$model',`year_from`='$modelyearfrom',
                `year_to`='$modelyearto',`steering`='$steering',`transmission`='$transmission',
                `fuel_type`='$fueltype',`order_quantity`='$orderqty',`country`='$country',
                `port`='$port',`price_from`='$pricefrom',`price_to`='$priceto',`price_term`='$priceterms',
                `want_to_receive`='$wtc',`payment_terms`='$paymentterms',`message`='$message',`fileurl`='$urls' WHERE id='$id' and owner='$owner_id';";
            }
         } 
 
        if( !$sql = @mysql_query($query) ) {
                
                require_once BASE_CLASS . 'class-log.php'; 
                LogReport::write('Unable to Insert to Database ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

                $db->close();
                $this->form_message = 'Unable to load page content due an internal error.';
                $this->form_status = true;
                $this->form_style = 'alert-error';
                $this->status = 'query';
                
                return;
            }

            else
            {
                $db->close(); 
                $this->status = 'complete';
                if(!isset($_GET['in_id'])){
                    header('Location:'.BASE_RELATIVE.'write-inquiry');
                }
                
                return;

            }


    }

    private function loadCountryList(){
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        if( !$sql = @mysql_query("SELECT * FROM `country_list` ORDER BY `country_name` ASC") ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load country list due a query error at ' . __FILE__ . ':' . __LINE__);
            
            $this->country_list = array();
            
            return;
        }
       
        if( @mysql_num_rows($sql) < 0 ){
            $cnx->close();          
            return;
        }
        
        $this->country_list = array();
        
        while( $r = @mysql_fetch_assoc($sql) ){
            $obj['cc'][] = $r['cc'];
            $obj['country_name'][] = stripslashes($r['country_name']);
           
            
            $this->country_list=$obj;
        }
       
        @mysql_free_result($sql);
        $cnx->close();
    }

    public function getPageHTML()
    {
        require_once BASE_ROOT . 'core/class-connect.php';

        $cnx = new Connect();
        $cnx->open();

        if( !$sql = @mysql_query("SELECT * FROM `page` WHERE `page_slug`='$this->slug' AND `page_group`='$this->group' LIMIT 1;") )
        {
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }

        if( @mysql_num_rows($sql) != 1 )
        {
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }

        $r = @mysql_fetch_assoc($sql);

        $result                     = array();
        $result['id']               = $r['id'];
        $result['meta_keywords']    = $r['meta_keywords'];
        $result['meta_description'] = $r['meta_description'];
        $result['meta_robots']      = $r['meta_robots'];
        $result['page_title']       = $r['page_title'];
        $result['page_slug']        = $r['page_slug'];
        $result['page_publish']     = $r['page_publish'];
        $result['page_author']      = $r['page_author'];
        $result['creation']         = $r['creation'];
        $result['editable']         = $r['editable'];
        $result['page_group']       = $r['page_group'];
        $result['html']             = $r['html'];

        @mysql_free_result($sql);
        $cnx->close();

        return $result;
    }

    /*
     * Public method: get form status ------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }

    /*
     * Public method: get form message -----------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /*
     * Public method: get form style -------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }

}
?>
