<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - Joel DeBolt
### - Edmunds.com
################################################################################
ini_set('max_execution_time', 300); //300 seconds = 5 minutes

class adminaddvehiclebyvin {
    private $lang;
    private $form_message;
    private $form_status;
    private $form_style;
    private $company_list;
    private $feature_list;
    private $save_status = false;
    
    /**
     * Constructor
     * <br>---------------------------------------------------------------------
     */
    public function adminaddvehiclebyvin($lang=array()){
        $this->lang = $lang;
        
        // if both are not set, send user to the normail add vehicle form.
        if( !isset($_SESSION['VIN_DATA']) && !isset($_POST['saveBtn']) ){
            header("Location: " . BASE_RELATIVE . 'home/new-car/');
            exit;
        }
        
        // unset vin_data session. from now on, only post is valid.
        if( isset($_SESSION['VIN_DATA']) ){
            // initialize form session.
            $_SESSION['form_car'] = (array)$_SESSION['VIN_DATA']['CAR_TABLE'];
            $_SESSION['form_feature'] = (array)$_SESSION['VIN_DATA']['CAR_FEATURE'];
            $_SESSION['form_media'] = (array)$_SESSION['VIN_DATA']['CAR_MEDIA'];
            $_SESSION['form_vin'] = $_SESSION['VIN_DATA']['VIN'];
            
            unset($_SESSION['VIN_DATA']);
        }
        
        // handle save vehicle form.
        if( isset($_POST['saveBtn']) ){
            $this->handleSaveForm();
        }
        
        // load feature list.
        $this->loadFeatureList();
    }
    
    /**
     * Private method: handle save vehicle form.
     * @return void
     */
    private function handleSaveForm(){
        ( isset($_POST['makerInput']) ? $maker = trim($_POST['makerInput']) : $maker = '' );
        ( isset($_POST['modelInput']) ? $model = trim($_POST['modelInput']) : $model = '' );
        ( isset($_POST['yearInput']) ? $year = trim($_POST['yearInput']) : $year = '' );
        ( isset($_POST['bodyInput']) ? $body = trim($_POST['bodyInput']) : $body = '' );
        ( isset($_POST['priceInput']) ? $price = (int)$_POST['priceInput'] : $price = (int)$_SESSION['form_car']['price_base'] );
        ( isset($_POST['doorInput']) ? $door = trim($_POST['doorInput']) : $door = $_SESSION['form_car']['doors'] );
        ( isset($_POST['milesInput']) ? $mileage = trim($_POST['milesInput']) : $mileage = '' );
        ( isset($_POST['featuredInput']) ? $featured = (string)$_POST['featuredInput'] : $featured = '0' );
        ( isset($_POST['ecoInput']) ? $eco = trim($_POST['ecoInput']) : $eco = 'A' );
        ( isset($_POST['engineSizeInput']) ? $engine_size = trim($_POST['engineSizeInput']) : $engine_size = '' );
        ( isset($_POST['trimInput']) ? $trim = trim($_POST['trimInput']) : $trim = '' );
        ( isset($_POST['typeInput']) ? $type = trim($_POST['typeInput']) : $type = '' );
        ( isset($_POST['gearInput']) ? $gear = trim($_POST['gearInput']) : $gear = '' );
        ( isset($_POST['transmissionInput']) ? $transmission = trim($_POST['transmissionInput']) : $transmission = '' );
        ( isset($_POST['fuelInput']) ? $fuel = trim($_POST['fuelInput']) : $fuel = '' );
        ( isset($_POST['bodyColorInput']) ? $body_color = trim($_POST['bodyColorInput']) : $body_color = '' );
        ( isset($_POST['prevOwnersInput']) ? $prev_owners = trim($_POST['prevOwnersInput']) : $prev_owners = '' );
        ( isset($_POST['lastServiceInput']) ? $last_service = trim($_POST['lastServiceInput']) : $last_service = '' );
        ( isset($_POST['motInput']) ? $mot = trim($_POST['motInput']) : $mot = '' );
        ( isset($_POST['taxBandInput']) ? $tax_band = trim($_POST['taxBandInput']) : $tax_band = '' );
        ( isset($_POST['topSpeedInput']) ? $top_speed = trim($_POST['topSpeedInput']) : $top_speed = '' );
        ( isset($_POST['torqueInput']) ? $torque_rpm = trim($_POST['torqueInput']) : $torque_rpm = '' );
        ( isset($_POST['powerInput']) ? $power_kw = trim($_POST['powerInput']) : $power_kw = '' );
        ( isset($_POST['featInput']) ? $feature_array = (array)$_POST['featInput'] : $feature_array = array() );
        ( isset($_SESSION['form_vin']) ? $vin  = $_SESSION['form_vin'] : $vin = '' );
        
        // get body type.
        $body = $this->getBodyTypeByName($body);
        $price = number_format($price, 2);
        $price = str_replace(',', '', $price);
        
        // validate required fields.
        if( empty($vin) ){
            $this->form_message = $this->lang['ADD_VIN_EMPTY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        if( empty($maker) ){
            $this->form_message = $this->lang['ADD_INVALID_MAKER_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-error';
            return;
        }
        if( empty($model) ){
            $this->form_message = $this->lang['ADD_INVALID_MODEL_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-error';
            return;
        }
        if( empty($price) ){
            $this->form_message = $this->lang['ADD_INVALID_PRICE_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-error';
            return;
        }
        if( empty($year) || strlen($year) != 4 || !is_numeric($year) ){
            $this->form_message = $this->lang['ADD_INVALID_YEAR_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-error';
            return;
        }
        if( empty($door) || strlen($door) > 2 ){
            $this->form_message = $this->lang['ADD_INVALID_DOORS_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-error';
            return;
        }
        
        // make sure all data will fit on db.
        $maker = substr($maker, 0, 150);
        $model = substr($model, 0, 150);
        $price = substr($price, 0, 11);
        $mileage = substr($mileage, 0, 11);
        $engine_size = substr($engine_size,0,150);
        $trim = substr($trim,0,150);
        $type = substr($type,0,150);
        $gear = substr($gear,0,30);
        $fuel = substr($fuel,0,150);
        $body_color = substr($body_color,0,150);
        $prev_owners = substr($prev_owners,0,30);
        $last_service = substr($last_service,0,100);
        $mot = substr($mot,0,30);
        $tax_band = substr($tax_band,0,200);
        $top_speed = substr($top_speed,0,30);
        $torque_rpm = substr($torque_rpm,0,30);
        $power_kw = substr($power_kw,0,30);
        $transmission = substr($transmission,0,150);
        
        // check if the VIN already is registered on inventory.
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        if( !$sql = @mysql_query("SELECT `vin` FROM `car` WHERE `vin`='$vin' LIMIT 1;") ){
            require_once BASE_CLASS . 'class-log.php';
            $cnx->close();
            LogReport::write('Unable to validate VIN due a query error at ' . __FILE__ . ':' . __LINE__);
            $this->form_message = $this->lang['ADD_VALIDATE_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        
        if( @mysql_num_rows($sql) > 0 ){
            $cnx->close();
            
            $this->form_message = $this->lang['ADD_VIN_EXISTS_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-info';
            return;
        }
        
        $cnx->close();
        
        // now that all required data is validate, download media images.
        $int_list = $_SESSION['form_media']['interior'];
        $ext_list = $_SESSION['form_media']['exterior'];
        
        $interior_list = array();
        $exterior_list = array();
        $dest          = BASE_ROOT . 'image/upload/cars/';
        
        for( $i=0; $i < count($int_list); $i++ ){
            $orig = $int_list[$i];
            
            if( !empty($orig) ){
                $ext = strtolower($orig);
                $ext = explode('.', $ext);
                $ext = $ext[(count($ext)-1)];
                
                $filename = $vin . '_' . date('Ymd_His') . '_' . mt_rand('000','999') . '.' . $ext;
                
                if( @copy($orig, $dest . $filename) ){
                    array_push($interior_list, 'image/upload/cars/' . $filename);
                }
            }
        } // end for.
        
        for( $i=0; $i < count($ext_list); $i++ ){
            $orig = $ext_list[$i];
            
            if( !empty($orig) ){
                $ext = strtolower($orig);
                $ext = explode('.', $ext);
                $ext = $ext[(count($ext)-1)];
                
                $filename = $vin . '_' . date('Ymd_His') . '_' . mt_rand('000','999') . '.' . $ext;
                
                if( @copy($orig, $dest . $filename) ){
                    array_push($exterior_list, 'image/upload/cars/' . $filename);
                }
            }
            
            unset($orig);
            unset($ext);
            unset($filename);
        } // end for.
        
        unset($int_list);
        unset($ext_list);
        unset($dest);
        
        // generate a car id.
        require_once BASE_CLASS . 'class-utilities.php';
        $car_id = Utilities::generateRandomString(21);
        
        // now that media is downloaded, save data into database.
        // save car table.
        $cnx->open();
        
        $maker = @mysql_real_escape_string($maker);
        $model = @mysql_real_escape_string($model);
        $body = @mysql_real_escape_string($body);
        $price = @mysql_real_escape_string($price);
        $year = @mysql_real_escape_string($year);
        $mileage = @mysql_real_escape_string($mileage);
        $door = @mysql_real_escape_string($door);
        $featured = @mysql_real_escape_string($featured);
        $eco = @mysql_real_escape_string($eco);
        
        if( !@mysql_query("INSERT INTO `car` (`id`,`maker`,`model`,`body_type`,`price`,`year`,`miles`,`measure_type`,`doors`,`status`,`featured`,`eco`,`vin`,`imported_from_csv`) 
                                      VALUES ('$car_id','$maker','$model','$body','$price','$year','$mileage','miles','$door','0','$featured','$eco','$vin','1')") ){
            require_once BASE_CLASS . 'class-log.php';
            
            LogReport::write('Unable to save vehicle due a query error at ' . __FILE__ . ':' . __LINE__);
            
            $this->form_message = $this->lang['ADD_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            
            $this->removeDownloadedMedia($exterior_list);
            $this->removeDownloadedMedia($interior_list);
            
            $cnx->close();
            
            return;
        }
        
        // save details.
        $engine_size = @mysql_real_escape_string($engine_size);
        $trim = @mysql_real_escape_string($trim);
        $type = @mysql_real_escape_string($type);
        $gear = @mysql_real_escape_string($gear);
        $fuel = @mysql_real_escape_string($fuel);
        $body_color = @mysql_real_escape_string($body_color);
        $prev_owners = @mysql_real_escape_string($prev_owners);
        $last_service = @mysql_real_escape_string($last_service);
        $mot = @mysql_real_escape_string($mot);
        $tax_band = @mysql_real_escape_string($tax_band);
        $top_speed = @mysql_real_escape_string($top_speed);
        $torque_rpm = @mysql_real_escape_string($torque_rpm);
        $power_kw = @mysql_real_escape_string($power_kw);
        $transmission = @mysql_real_escape_string($transmission);
        $html_content = $_SESSION['form_car']['html'];
        $html_content = @mysql_real_escape_string($html_content);
        
        if( !@mysql_query("INSERT INTO `car_details` (`car_id`,`engine_size`,`trim`,`type`,`gear`,`fuel`,`color`,`prev_owners`,`last_service`,`mot`,`tax_band`,`top_speed`,`engine_torque_rpm`,`engine_power_kw`,`transmission_type`,`html`) 
                                              VALUES ('$car_id','$engine_size','$trim','$type','$gear','$fuel','$body_color','$prev_owners','$last_service','$mot','$tax_band','$top_speed','$torque_rpm','$power_kw','$transmission','$html_content')") ){
            @mysql_query("DELETE FROM `car` WHERE `id`='$car_id' LIMIT 1;");
            
            require_once BASE_CLASS . 'class-log.php';
            
            LogReport::write('Unable to save vehicle due a query error at ' . __FILE__ . ':' . __LINE__);
            
            $this->form_message = $this->lang['ADD_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            
            $this->removeDownloadedMedia($exterior_list);
            $this->removeDownloadedMedia($interior_list);
            
            $cnx->close();
            
            return;
        }
        
        // save car features.
        if( count($feature_array) > 0 ){
            for( $i=0; $i < count($feature_array); $i++ ){
                $label = explode(':',$feature_array[$i]);
                
                $source = '';
                
                if( count($label) == 2 ){
                    $source = $label[1];
                    $label = $label[0];
                }
                                
                if( !empty($label) && !empty($source) ){
                    @mysql_query("INSERT INTO `car_features` (`car_id`,`feat_name`,`feat_icon`) VALUES ('$car_id','$label','$source')");
                }
            }
        }
        
        // save media.
        for( $i=0; $i < count($interior_list); $i++ ){
            $source = $interior_list[$i];
            
            if( file_exists(BASE_ROOT . $source) ){
                if( !@mysql_query("INSERT INTO `car_media` (`car_id`,`type`,`mode`,`source`) VALUES ('$car_id','image','interior','$source')") ){
                    @unlink(BASE_ROOT . $interior_list[$i]);
                }
            }
        }
        for( $i=0; $i < count($exterior_list); $i++ ){
            $source = $exterior_list[$i];
            
            if( file_exists(BASE_ROOT . $source) ){
                if( !@mysql_query("INSERT INTO `car_media` (`car_id`,`type`,`mode`,`source`) VALUES ('$car_id','image','exterior','$source')") ){
                    @unlink(BASE_ROOT . $exterior_list[$i]);
                }
            }
        }
        
        // unset sessions.
        unset($_SESSION['form_car']);
        unset($_SESSION['form_feature']);
        unset($_SESSION['form_media']);
        unset($_SESSION['form_vin']);
        unset($interior_list);
        unset($exterior_list);
        
        // return positive message.
        $this->save_status = true;
        
        $this->form_message = $this->lang['ADD_VEHICLE_QUERY_SUCCESS'];
        $this->form_status = true;
        $this->form_style = 'alert-success';
        
        return;
    }
    
    /**
     * Private method: remove downloaded media.
     * @param array $list The media list to be removed.
     * @return void
     */
    private function removeDownloadedMedia($list){
        if( is_array($list) ){
            for( $i=0; $i < count($list); $i++ ){
                $source = $list[$i];
                $source = str_replace(BASE_RELATIVE, BASE_ROOT, $source);
                
                if( file_exists($source) ){
                    @unlink($source);
                }
            }
        }
    }
    
    /**
     * Private method: load features list
     * @return void
     */
    private function loadFeatureList(){
        require_once BASE_CLASS . 'class-connect.php';
        
        $this->feature_list = array();
        
        $cnx = new Connect();
        $cnx->open();
        
        if( !$sql = @mysql_query("SELECT * FROM `feature_list`") ){
            require_once BASE_CLASS . 'class-log.php';
            
            LogReport::write('Unable to load feature list due a query error at ' . __FILE__ . ':' . __LINE__);
            
            $this->form_message = $this->lang['ADD_FEATURE_LOADER_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            
            $cnx->close();
            
            return;
        }
        
        if( @mysql_num_rows($sql) > 0 ){
            while( $r = @mysql_fetch_assoc($sql) ){
                $obj = array(
                    'label' => stripslashes($r['label']),
                    'source' => stripslashes($r['source'])
                );
                array_push($this->feature_list, $obj);
            }
            
            @mysql_free_result($sql);
        }
        
        $cnx->close();
        
        unset($sql);
        
        return;
    }
    
    /**
     * Public getter: get save status
     * @return boolean
     */
    public function getSaveStatus(){
        return $this->save_status;
    }
    
    /**
     * Public getter: get features list
     * @return array
     */
    public function getFeatureList(){
        return $this->feature_list;
    }
    
    /**
     * Public method: get body id by name.
     * @param string $body_type The body type's name.
     * @return int The id for the body type.
     */
    public function getBodyTypeByName($body_type){
        $path = BASE_ROOT . 'language/' . $_SESSION['log_language_iso'] . '/car-body-types.php';
        
        if( !file_exists($path) ){
            return '0';
        }
        
        require_once $path;
        
        $list = $_CAR_BODY;
        $result = 0;
        
        if( !is_array($list) ){
            return $result;
        }
        
        foreach( $list as $k => $v ){
            if( $v == $body_type ){
                $result = $k;
                break;
            }
        }
        
        return $result;
    }
    
    /**
     * Public method: get form status
     * <br>---------------------------------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }

    /**
     * Public method: get form message 
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /**
     * Public method: get form style
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }
}