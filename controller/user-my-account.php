<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
class usermyaccount 
{
    protected $lang;
    protected $form_message;
    protected $form_status;
    protected $form_style;
    protected $acc_list;


    /*
     * Constructor -------------------------------------------------------------
     */
    public function usermyaccount($lang=array())
    {
        $this->lang = $lang;
        
        // handle update info form.
        if( isset($_POST['updatebtn']) )
        {
            $this->handleUpdateForm();
        }
        
        // handle remove account form.
        if( isset($_POST['removebtn']) )
        {
            $this->handleRemoveAccountForm();
        }
        
        // load account information.
        $this->loadAccountInfo();
    }
    
    /*
     * Private method: remove account form -------------------------------------
     * @return void.
     */
    private function handleRemoveAccountForm()
    {
        $id = (int)$_POST['accId'];
        $email = $_SESSION['log_email'];
        
        // validate.
        if( $id < 1 || empty($email) )
        {
            return;
        }
        
        // remove account from database.
        require_once BASE_ROOT . 'core/class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        if( !@mysql_query("DELETE FROM `register` WHERE `id`='$id' LIMIT 1;") )
        {
            $cnx->close();
            
            require_once BASE_ROOT . 'core/class-log.php';
            
            LogReport::write('Unable to remove account due a query error at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());
            
            $this->form_message = $this->lang['MY_ACCOUNT_REMOVE_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            
            return;
        }
        
        // remove all template entries from database.
        if( !@mysql_query("DELETE FROM `ecommds_user_template` WHERE `author`='$email'") )
        {
            require_once BASE_ROOT . 'core/class-log.php';
            
            LogReport::write('URGENT: Unable to remove user template registration at table [ecommds_user_template] due a query error at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());
        }
        
        $cnx->close();
        
        // remove user files.
        $path = BASE_ROOT . 'app/ecommds/userFiles/@' . $email . '/';
        
        if( file_exists($path) )
        {
            $this->recursive_remove_directory($path);
        }
        
        // redirect user to logout script.
        header("Location: " . BASE_RELATIVE . 'logout.php');
        exit;
    }
    
    /*
     * Private method: handle update form --------------------------------------
     * @return void.
     */
    private function handleUpdateForm()
    {
        $id = (int)$_POST['uidHid'];
        $pass = trim($_POST['passInput']);
        $name = trim($_POST['nameInput']);
        
        $pass = stripslashes($pass);
        $name = stripslashes($name);
        
        // validate.
        if( strlen($name) < 3 && !ereg(' ', $name) )
        {
            $this->form_message = $this->lang['MY_ACCOUNT_INVALID_NAME'];
            $this->form_status = true;
            $this->form_style = 'alert-error';
            
            return;
        }
        
        $query = "UPDATE `register` SET ";
        
        if( !empty($pass) )
        {
            $pass = md5($pass);
            $query .= "`password`='$pass', ";
        }
        
        $query .= "`name`='$name' WHERE `id`='$id' LIMIT 1;";
        
        require_once BASE_ROOT . 'core/class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        if( !@mysql_query($query) )
        {
            $cnx->close();
            
            require_once BASE_ROOT . 'core/class-log.php';
            
            LogReport::write('Unable to update user information due a query error at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());
            
            $this->form_message = $this->lang['MY_ACCOUNT_UPDATE_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            
            return;
        }
        
        $cnx->close();
        
        $this->form_message = $this->lang['MY_ACCOUNT_UPDATE_QUERY_SUCCESS'];
        $this->form_status = true;
        $this->form_style = 'alert-success';
        
        return;
    }
    
    /*
     * Private method: load account information --------------------------------
     * @return void.
     */
    private function loadAccountInfo()
    {
        $user = $_SESSION['log_email'];
        
        require_once BASE_ROOT . 'core/class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        if( !$sql = @mysql_query("SELECT `id`,`email`,`name`,`creation` FROM `register` WHERE `email`='$user' LIMIT 1;") )
        {
            $cnx->close();
            $this->acc_list = array();
            
            require_once BASE_ROOT . 'core/class-log.php';
            
            LogReport::write('Unable to load user registration information due a query error at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());
            
            $this->form_message = $this->lang['MY_ACCOUNT_LOADER_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            
            return;
        }
        
        if( @mysql_num_rows($sql) != 1 )
        {
            $cnx->close();
            $this->acc_list = array();
            return;
        }
        
        $r = @mysql_fetch_assoc($sql);
        
        $o = array(
            'id' => $r['id'],
            'email' => $r['email'],
            'name' => $r['name'],
            'creation' => $r['creation']
        );
        
        $this->acc_list = $o;
        $cnx->close();
        
        return;
    }
    
    /*
     * Public method: get account information ----------------------------------
     */
    public function getAccountInfo()
    {
        return $this->acc_list;
    }

    /*
     * Public method: get form status ------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }
    
    /*
     * Public method: get form message -----------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }
    
    /*
     * Public method: get form style -------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }
    
    /*
     * Private method: remove directory ----------------------------------------
     */
    private function recursive_remove_directory($directory, $empty=FALSE)
    {
        // if the path has a slash at the end we remove it here
        if(substr($directory,-1) == '/')
        {
            $directory = substr($directory,0,-1);
        }

        // if the path is not valid or is not a directory ...
        if(!file_exists($directory) || !is_dir($directory))
        {
            // ... we return false and exit the function
            return FALSE;

        // ... if the path is not readable
        }
        elseif(!is_readable($directory))
        {
            // ... we return false and exit the function
            return FALSE;

        // ... else if the path is readable
        }
        else
        {
            // we open the directory
            $handle = opendir($directory);

            // and scan through the items inside
            while (FALSE !== ($item = readdir($handle)))
            {
                // if the filepointer is not the current directory
                // or the parent directory
                if($item != '.' && $item != '..')
                {
                    // we build the new path to delete
                    $path = $directory.'/'.$item;

                    // if the new path is a directory
                    if(is_dir($path)) 
                    {
                        // we call this function with the new path
                        $this->recursive_remove_directory($path);

                    // if the new path is a file
                    }
                    else
                    {
                        // we remove the file
                        @unlink($path);
                    }
                }
            }
            // close the directory
            closedir($handle);

            // if the option to empty is not set to true
            if($empty == FALSE)
            {
                // try to delete the now empty directory
                if(!rmdir($directory))
                {
                    // return false if not possible
                    return FALSE;
                }
            }
            // return success
            return TRUE;
        }
    }
}
?>
