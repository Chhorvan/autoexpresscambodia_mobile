<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################

class admingroups 
{
    protected $lang;
    protected $form_status;
    protected $form_message;
    protected $form_style;
    protected $group_list;
    protected $form_type;
    
    /*
     * Constructor -------------------------------------------------------------
     */
    public function admingroups($lang=array())
    {
        $this->lang = $lang;
        
        // handle delete form button.
        if( isset($_POST['deleteBtn']) )
        {
            $this->handleDeleteForm();
        }
        
        // handle save form button.
        if( isset($_POST['saveBtn']) )
        {
            $this->handleSaveForm();
        }
        
        // load group list.
        $this->loadGroupList();
    }
    
    /*
     * Protected method: handle save form --------------------------------------
     * @return void.
     */
    protected function handleSaveForm()
    {
        $group = trim($_POST['groupInput']);
        
        // validate.
        require_once BASE_CLASS . 'class-utilities.php';
        
        // make sure group string contains only characteres and digits.
        $group = Utilities::genetateSlug($group);
        
        if( empty($group) )
        {
            $this->form_message = $this->lang['GROUPS_SAVE_EMPTY_GROUP_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-error';
            return;
        }
        
        // validate for reserved groups.
        if( $group == 'admin' || $group == 'administrator' || $group == 'public' )
        {
            $this->form_message = $this->lang['GROUPS_SAVE_RESERVED_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-error';
            return;
        }
        
        // save to database.
        require_once BASE_CLASS . 'class-connect.php';
        
        $db = new Connect();
        $db->open();
        
        $group = mysql_real_escape_string($group);
        
        if( !@mysql_query("INSERT INTO `group` (`group`) VALUES ('$group')") )
        {
            $db->close();
            
            require_once BASE_CLASS . 'class-log.php';
            
            LogReport::write('Unable to save group due query error at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());
            
            $this->form_message = $this->lang['GROUPS_SAVE_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-error';
            return;
        }
        
        $db->close();
        
        $this->form_message = $this->lang['GROUPS_SAVE_QUERY_SUCCESS'];
        $this->form_status = true;
        $this->form_style = 'alert-success';
        return;
    }
    
    /*
     * Protected method: handle delete form ------------------------------------
     * @return void.
     */
    protected function handleDeleteForm()
    {
        $id = trim($_POST['idInput']);
        $id = (int)$id;
        
        if( $id < 1 )
        {
            $this->form_message = $this->lang['GROUPS_INVALID_ID'];
            $this->form_status = true;
            $this->form_style = 'alert-error';
            $this->form_type = 'manager';
            return;
        }
        
        // remove from database.
        require_once BASE_CLASS . 'class-connect.php';
        
        $db = new Connect();
        $db->open();
        
        if( !@mysql_query("DELETE FROM `group` WHERE `id`='$id' LIMIT 1;") )
        {
            require_once BASE_CLASS . 'class-log.php';
            
            $db->close();
            
            LogReport::write('Unable to remove group from database due a query error at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());
            
            $this->form_message = $this->lang['GROUPS_DELETE_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-error';
            $this->form_type = 'manager';
            return;
        }
        
        $db->close();
        
        $this->form_message = $this->lang['GROUPS_DELETE_QUERY_SUCCESS'];
        $this->form_status = true;
        $this->form_style = 'alert-success';
        $this->form_type = 'manager';
        return;
    }
    
    /*
     * Protected method: load group list ---------------------------------------
     * @return void.
     */
    protected function loadGroupList()
    {
        require_once BASE_CLASS . 'class-connect.php';
        
        $db = new Connect();
        $db->open();
        
        if( !$sql = @mysql_query("SELECT * FROM `group` ORDER BY `group`") )
        {
            require_once BASE_CLASS . 'class-log.php';
            
            $db->close();
            
            LogReport::write('Unable to query group table at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());
            LogReport::fatalError($this->lang['GROUPS_LOAD_GROUP_QUERY_ERROR']);
        }
        
        $this->group_list = array();
        
        if( @mysql_num_rows($sql) > 0 )
        {
            while( $r = @mysql_fetch_assoc($sql) )
            {
                $obj = array(
                    'id' => $r['id'],
                    'group' => $r['group']
                );
                
                array_push($this->group_list, $obj);
            }
            
            @mysql_free_result($sql);
        }
        
        $db->close();
    }
    
    /*
     * Public method: get group list -------------------------------------------
     * @return array.
     */
    public function getGroupList()
    {
        return $this->group_list;
    }
    
    /*
     * Public method: get form status ------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }
    
    /*
     * Public method: get form message -----------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }
    
    /*
     * Public method: get form style -------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }
    
    /*
     * Public method: get form type --------------------------------------------
     * @return string.
     */
    public function getFormType()
    {
        return $this->form_type;
    }
}
