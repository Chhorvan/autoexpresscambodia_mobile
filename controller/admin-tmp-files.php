<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
class admintmpfiles {
    private $lang;
    private $form_message;
    private $form_status;
    private $form_style;
    
    private $source_path="image/upload/cars/";
    private $preview_path="image/upload/cars/preview_gallery-";
    private $thumb_path="image/upload/cars/thumb_gallery-";
    public $totalTmpRecord=0;
    /**
     * Constructor
     * <br>---------------------------------------------------------------------
     */
    public function admintmpfiles($lang=array()){
        $this->lang = $lang;
        if(isset($_POST['submit_delete'])){
            $this->handleDelete();
        }
        //$this->executeDataCount();
        // // handle remove news.
        // if( isset($_POST['removebtn']) ){
        //     $this->handleForm();
        // }
        
        // // load news list.
        // $this->loadNewsList();
    }
    function handleDelete(){
        $tmpData=$this->getAllTmpData();
        //delete physical files first
        foreach($tmpData as $data){
            $image=basename($data['source']);
            //echo $this->source_path.$image."<br/>";
            if(file_exists($this->source_path.$image)){
                unlink($this->source_path.$image);
            }
            if(file_exists($this->preview_path.$image)){
                unlink($this->preview_path.$image);
            }
            if(file_exists($this->thumb_path.$image)){
                unlink($this->thumb_path.$image);
            }
        }
        //delete from database
        $this->deleteTmpData();
    }
    function getAllTmpData(){
        require_once BASE_CLASS . 'class-connect.php';
        $cnx = new Connect();
        $cnx->open();
        $sql="SELECT * FROM car_media cm WHERE NOT EXISTS (SELECT p.id from product p WHERE p.id = cm.product_id);";
        $result=@mysql_query($sql);
        $this->totalTmpRecord=mysql_num_rows($result);
        $tmp_data=array();
        while($row=@mysql_fetch_assoc($result)){
            array_push($tmp_data, $row);
        }
        $cnx->close();
        
        return $tmp_data;
    }
    function deleteTmpData(){
        //image\upload\
        require_once BASE_CLASS . 'class-connect.php';
        $cnx = new Connect();
        $cnx->open();
        //$sql="DELETE FROM car_media cm WHERE cm.product_id NOT IN (SELECT id from product p where p.id = cm.product_id)";
        $sql="DELETE FROM car_media WHERE product_id NOT IN (SELECT id FROM product);";
        $result=@mysql_query($sql);
        $tmp_data=array();
        if(!$result){
            $this->form_message = "Error! Could not clear temporary data from database.";
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        while($row=@mysql_fetch_assoc($result)){
            array_push($tmp_data, $row);
        }

        $this->form_message =  "Success! Temporary data has been cleared.";
        $this->form_status = true;
        $this->form_style = 'alert-success';
        $cnx->close();
        
        return $tmp_data;
    }
    
    /**
     * Public method: get form status
     * <br>---------------------------------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }

    /**
     * Public method: get form message 
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /**
     * Public method: get form style
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }
}