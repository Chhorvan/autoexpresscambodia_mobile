<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
include('resize-class.php');
class userRegisterDealoftheweek
{
    protected $lang;
    protected $group = 'public';
    protected $slug = 'car';
    protected $form_status;
    protected $form_message;
    protected $form_style;
    Protected $status="";
    private $url="";
    
    /*
     * Constructor -------------------------------------------------------------
     */
    public function userRegisterDealoftheweek($lang=array())
    {
        $this->lang = $lang;
           //echo "Test";

         if(isset($_POST['deal_upload'])){
            
            $this->saveRegisterDeal();
            $this->getstatus();

         }
		
    }

    private function registerUploadfile(){

        $allowedExts = array("gif", "jpeg", "jpg", "png");
        $temp = explode(".", $_FILES["myfile"]["name"]);
        $extension = end($temp);
        if ((($_FILES["myfile"]["type"] == "image/gif")
        || ($_FILES["myfile"]["type"] == "image/jpeg")
        || ($_FILES["myfile"]["type"] == "image/jpg")
        || ($_FILES["myfile"]["type"] == "image/pjpeg")
        || ($_FILES["myfile"]["type"] == "image/x-png")
        || ($_FILES["myfile"]["type"] == "image/png"))
        && ($_FILES["myfile"]["size"] < 20480000)
        && in_array($extension, $allowedExts))
        {
            if ($_FILES["myfile"]["error"] > 0)
            {
                $this->form_message = $_FILES["myfile"]["error"];
                $this->form_status = true;
                $this->form_style = 'alert-error';
                $this->status="upload";
                //echo "Return Code: " . $_FILES["myfile"]["error"] . "<br>";
            }
            else
            {

                LogReport::write('File Upload Successfully ' . $_FILES["myfile"]["name"] . ':' . $_FILES["myfile"]["type"]. '. ' . ($_FILES["myfile"]["size"] / 1024).'kB' );
 
               //echo "Upload: " . $_FILES["file"]["name"] . "<br>";
               // echo "Type: " . $_FILES["file"]["type"] . "<br>";
               // echo "Size: " . ($_FILES["file"]["size"] / 1024) . " kB<br>";
               // echo "Temp file: " . $_FILES["file"]["tmp_name"] . "<br>";

                if (file_exists("image/deal_week/" . $_FILES["myfile"]["name"]))
                {
                    LogReport::write('This file is already exists' . $_FILES["myfile"]["name"] . ':' . $_FILES["myfile"]["type"]. '. ' . ($_FILES["myfile"]["size"] / 1024).'kB' );
                    $dtime = new DateTime(); 
                    $dtime->format('YYYY-mmm-dd H:i:s');
                    $timestamp = $dtime->getTimestamp();
                    $filename =  $timestamp.".". $extension;
                    move_uploaded_file($_FILES["myfile"]["tmp_name"],
                    "image/deal_week/" . $filename);
                    $main_image=new resize("image/deal_week/" . $filename);
 
                    $main_image->resizeImage(210, 155, 'exact');
 
                    $main_image->saveImage("image/deal_week/" . $filename, 100);  
                    $this->url="image/deal_week/".$filename;
                     
                }
                else 
                {
                   
                    $dtime = new DateTime(); 
                    $dtime->format('YYYY-mmm-dd H:i:s');
                    $timestamp = $dtime->getTimestamp();
                    $filename =  $timestamp.".". $extension;
                    move_uploaded_file($_FILES["myfile"]["tmp_name"],
                    "image/deal_week/" . $filename);
                    $main_image=new resize("image/deal_week/" . $filename);
 
                    $main_image->resizeImage(210, 155, 'exact');
 
                    $main_image->saveImage("image/deal_week/" . $filename, 100);
                   $this->url="image/deal_week/".$filename;
                     

                    //echo "Stored in: " . "upload/" . $_FILES["file"]["name"];
                }
            }
        }
        else
        {
             LogReport::write('This file is Invalid file' . $_FILES["myfile"]["name"] . ':' . $_FILES["myfile"]["type"]. '.' .($_FILES["myfile"]["size"] / 1024).'kB' );
                $this->form_message = 'This file is Invalid file.';
                $this->form_status = true;
                $this->form_style = 'alert-error';
             
            $this->status="upload";
            return;
        }

    }


    /*
     * Public method: get page content data ------------------------------------
     * @return array | false.
     */



    function closetags($html) {

      #put all opened tags into an array

      preg_match_all('#<([a-z]+)(?: .*)?(?<![/|/ ])>#iU', $html, $result);

      $openedtags = $result[1];   #put all closed tags into an array

      preg_match_all('#</([a-z]+)>#iU', $html, $result);

      $closedtags = $result[1];

      $len_opened = count($openedtags);

      # all tags are closed

      if (count($closedtags) == $len_opened) {

        return $html;

      }

      $openedtags = array_reverse($openedtags);

      # close tags

      for ($i=0; $i < $len_opened; $i++) {

        if (!in_array($openedtags[$i], $closedtags)){

          $html .= '</'.$openedtags[$i].'>';

        } else {

          unset($closedtags[array_search($openedtags[$i], $closedtags)]);    }

        }  return $html;
    } 




    public function saveRegisterDeal(){
         
        require_once BASE_CLASS . 'class-connect.php';
        $title = $_POST['rd_title'];
        $message = $this->closetags($_POST['rd_htmlInput']);
        $cnx = new Connect();
        $cnx->open();
        
        $this->registerUploadfile(); 
        $image_name=$this->url;       

        if($title=='' || $message==''){            
            $this->status ='error';
        }  
        else{
        $sql_insert="INSERT INTO deal_week (title, message, `date`, owner, source, name) VALUES ('$title', '$message', NOW(), '{$_SESSION['log_id']}', '$image_name', '{$_SESSION['log_name']}')";
        $this->status = 'query_dealweek';
        //echo $sql_insert;
        if( !$sql = @mysql_query($sql_insert) ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load car maker list due a query error at ' . __FILE__ . ':' . __LINE__);
            $deal_id=mysql_insert_id();
            
            $this->status = 'query_dealweek';
            $cnx->close();
            return;
        }
        $deal_id=mysql_insert_id();
        }
        @mysql_free_result($sql);
        $cnx->close();
       
    }


    public function getPageHTML()
    {
        require_once BASE_ROOT . 'core/class-connect.php';

        $cnx = new Connect();
        $cnx->open();

        if( !$sql = @mysql_query("SELECT * FROM `page` WHERE `page_slug`='$this->slug' AND `page_group`='$this->group' LIMIT 1;") )
        {
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }

        if( @mysql_num_rows($sql) != 1 )
        {
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }

        $r = @mysql_fetch_assoc($sql);

        $result                     = array();
        $result['id']               = $r['id'];
        $result['meta_keywords']    = $r['meta_keywords'];
        $result['meta_description'] = $r['meta_description'];
        $result['meta_robots']      = $r['meta_robots'];
        $result['page_title']       = $r['page_title'];
        $result['page_slug']        = $r['page_slug'];
        $result['page_publish']     = $r['page_publish'];
        $result['page_author']      = $r['page_author'];
        $result['creation']         = $r['creation'];
        $result['editable']         = $r['editable'];
        $result['page_group']       = $r['page_group'];
        $result['html']             = $r['html'];

        @mysql_free_result($sql);
        $cnx->close();

        return $result;
    }
	

    /*
     * Public method: get form status ------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }
        public function getstatus()
    {
        return $this->status;
    }

    /*
     * Public method: get form message -----------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /*
     * Public method: get form style -------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }

}
?>
