<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
class userMyInquiryDetail
{
    protected $lang;
    protected $group = 'public';
    protected $slug = 'car';
    protected $form_status;
    protected $form_message;
    protected $form_style;
    /*
     * Constructor -------------------------------------------------------------
     */
    public function userMyInquiryDetail($lang=array())
    {
        $this->lang = $lang;
		$this->listmyinquiry();
		
        // if register form is submit, process it.
        if( isset($_POST['submit']) )
        {
            $this->processReplyForm();
        }
		
    }
	
	 public function listmyinquiry()
    {

        require_once BASE_ROOT . 'core/class-connect.php';

        $id=0;
        if(isset($_GET['id'])){
            $id=$_GET['id']; 
        }
        

        $cnx = new Connect();
        $cnx->open();
        if( !$sql = @mysql_query("SELECT  i.*,r.`company_name`,r.`name`,
             r.`card_id`,r.`tel`,r.`fax`,r.`country`,r.`address`,r.`image`  
             FROM `inquiry` `i` LEFT JOIN `register` `r` ON i.`owner`=r.`id`
             WHERE i.`id`='$id';") )
        {
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }
         $this->list_inuquliry = array();
        while( $r = @mysql_fetch_assoc($sql) ){

            array_push($this->list_inuquliry, $r);
     
        }
        $cnx->close();
        //var_dump($this->list_inuquliry);
        return $this->list_inuquliry;


    }

	
	
    protected function listmyinquiry111111()
    {
		$sender = "";
		$receiver = "";
		$product = "";
		$i_sMessage = "";
		$str = "";
		
		extract($_POST);
		
		$str = $i_sMessage;
		
		$i = strrpos($str, "---------- [ Original Message ] ----------");
		$l = strlen($str) - $i;
		$str = substr($str, $i, $l);
		$i_sMessage = str_ireplace($str, "", $i_sMessage);
		
        require_once BASE_ROOT . 'core/class-connect.php';

        $cnx = new Connect();
        $cnx->open();

		
		$row = mysql_fetch_array(mysql_query("SELECT * FROM message ORDER BY id DESC"));
		$id = $row['id'];
		$id++;
		
		if( $sql = mysql_query("INSERT INTO `message` (`id`, `message`, `send_id`, `respon_id`, `duration`, `seen`, `product_id`) VALUES('$id++', '$i_sMessage', '$sender', '$receiver', '".date('Y-m-d H:i:s')."', '0', '$product')") ) {
			
			$cnx->close();
			$this->form_status = 'complete';
		} else {
			require_once BASE_CLASS . 'class-log.php';
			
			LogReport::write('Unable to query database at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());
			
			$cnx->close();
			$this->form_status = 'query';
		}
	}

    /*
     * Public method: get page content data ------------------------------------
     * @return array | false.
     */
    public function getPageHTML()
    {
        require_once BASE_ROOT . 'core/class-connect.php';

        $cnx = new Connect();
        $cnx->open();

        if( !$sql = @mysql_query("SELECT * FROM `page` WHERE `page_slug`='$this->slug' AND `page_group`='$this->group' LIMIT 1;") )
        {
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }

        if( @mysql_num_rows($sql) != 1 )
        {
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }

        $r = @mysql_fetch_assoc($sql);

        $result                     = array();
        $result['id']               = $r['id'];
        $result['meta_keywords']    = $r['meta_keywords'];
        $result['meta_description'] = $r['meta_description'];
        $result['meta_robots']      = $r['meta_robots'];
        $result['page_title']       = $r['page_title'];
        $result['page_slug']        = $r['page_slug'];
        $result['page_publish']     = $r['page_publish'];
        $result['page_author']      = $r['page_author'];
        $result['creation']         = $r['creation'];
        $result['editable']         = $r['editable'];
        $result['page_group']       = $r['page_group'];
        $result['html']             = $r['html'];

        @mysql_free_result($sql);
        $cnx->close();

        return $result;
    }

    /*
     * Public method: get form status ------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }

    /*
     * Public method: get form message -----------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /*
     * Public method: get form style -------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }

}
?>
