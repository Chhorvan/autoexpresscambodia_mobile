<?php
if(!isset($_SESSION)) @session_start();
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################

class userreservedcars {
    protected $lang;
    protected $form_status;
    protected $form_message;
    protected $form_style;
    protected $reserve_list;
    protected $car;
    protected $car_details;
    protected $car_media;
    protected $car_features;
    
    /**
     * Constructor
     * <br>---------------------------------------------------------------------
     */
    public function userreservedcars($lang=array()){
        $this->lang = $lang;
        
        // handle remove vehicle reservation.
        if( isset($_POST['removeInput']) ){
            $this->handleForm();
        }
        
        // load user reservation list.
        $this->loadReservationList();
    }
    
    /**
     * Private method: handle remove form
     * @return void
     */
    private function handleForm(){
        $cid = (string)$_POST['carInput'];
        $rid = (int)$_POST['reservationInput'];
        
        if( empty($cid) || empty($rid) ){
            return;
        }
        
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        $cid = @mysql_real_escape_string($cid);
        $rid = @mysql_real_escape_string($rid);
        
        // remove vehicle from user list.
        if( !@mysql_query("DELETE FROM `reservation` WHERE `id`='$rid' LIMIT 1;") ){
            require_once BASE_CLASS . 'class-log.php';
            
            $cnx->close();
            
            LogReport::write('Unable to remove vehicle due a query error at ' . __FILE__ . ':' . __LINE__);
            
            $this->form_message = $this->lang['RESERVED_REMOVE_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            
            return;
        }
        
        // reset car status.
        if( !@mysql_query("UPDATE `car` SET `status`='0' WHERE `id`='$cid' LIMIT 1;") ){
            require_once BASE_CLASS . 'class-log.php';
            
            $cnx->close();
            
            LogReport::write('WARNING: Unable to reset car id '.$cid.' status due a query error at ' . __FILE__ . ':' . __LINE__);
            
            $this->form_message = $this->lang['RESERVED_REMOVE_QUERY_SUCCESS'];
            $this->form_status = true;
            $this->form_style = 'alert-success';
            
            return;
        }
        
        $cnx->close();
        $this->form_message = $this->lang['RESERVED_REMOVE_QUERY_SUCCESS'];
        $this->form_status = true;
        $this->form_style = 'alert-success';

        return;
    }
    
    /**
     * Private method: load reservation list
     * <br>---------------------------------------------------------------------
     * @return void
     */
    private function loadReservationList(){
        require_once BASE_CLASS . 'class-connect.php';
        require_once BASE_CLASS . 'class-utilities.php';
        
        $this->reserve_list = array();
        
        $cnx = new Connect();
        $cnx->open();
        
        // only 1 car is allowed to be reserved for each user.
        if( !$sql = @mysql_query("SELECT * FROM `reservation` WHERE `user_id`='".$_SESSION['log_id']."' LIMIT 1;") ){
            require_once BASE_CLASS . 'class-log.php';
            
            $cnx->close();
            
            LogReport::write('Unable to load car reservation list due a query error at ' . __FILE__ . ':' . __LINE__);
            $this->form_message = $this->lang['RESERVED_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        
        if( @mysql_num_rows($sql) != 1 ){
            $cnx->close();
            return;
        }
        
        $r = @mysql_fetch_assoc($sql);
        @mysql_free_result($sql);
        
        // define date format.
        $date = explode('-', $r['date']);
        $day  = $date[2];
        $month = $date[1];
        $year = $date[0];
        $format = 'yyyymmdd';
        
        if( $_SESSION['log_language_iso'] != 'en' ){
            $format = 'ddmmyyyy';
        }
                
        $this->reserve_list = array(
            'id'        => $r['id'],
            'car_id'    => $r['car_id'],
            'name'      => stripslashes($r['name']),
            'email'     => stripslashes($r['email']),
            'phone'     => stripslashes($r['phone']),
            'address'   => stripslashes($r['address']),
            'city'      => stripslashes($r['city']),
            'zip'       => stripslashes($r['zip']),
            'country'   => stripslashes($r['country']),
            'code'      => $r['code'],
            'browser'   => stripslashes($r['browser']),
            'ip'        => $r['ip'],
            'date'      => Utilities::checkDateFormat($year,$month,$day,$format)
        );
        
        // load car by id.
        if( !$sql = @mysql_query("SELECT * FROM `car` WHERE `id`='".$this->reserve_list['car_id']."' AND `status`='1' LIMIT 1;") ){
            require_once BASE_CLASS . 'class-log.php';
            $cnx->close();
            $this->reserve_list = array();
            
            LogReport::write('Unable to load car list due a query error at ' . __FILE__ . ':' . __LINE__);
            $this->form_message = $this->lang['RESERVED_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        
        // if the car is not found it means that the car status changed or it has
        // been removed from the database. attempt to remove the car from the user
        // reservation table.
        if( @mysql_num_rows($sql) != 1 ){
            if( !@mysql_query("DELETE FROM `reservation` WHERE `id`='".$this->reserve_list['id']."' LIMIT 1;") ){
                require_once BASE_CLASS . 'class-log.php';
                $cnx->close();
                
                $this->reserve_list = array();
                
                LogReport::write('Unable to remove reserved car from user list due a query error at ' . __FILE__ . ':' . __LINE__);
                
                $this->form_message = $this->lang['RESERVED_QUERY_ERROR'];
                $this->form_status = true;
                $this->form_style = 'alert-warning';
                
                return;
            }
            else {
                $cnx->close();
                $this->reserve_list = array();
                return;
            }
        }
        
        $r = @mysql_fetch_assoc($sql);
        @mysql_free_result($sql);
        
        $this->car = array(
            'id'            => $r['id'],
            'maker'         => stripslashes($r['maker']),
            'model'         => stripslashes($r['model']),
            'body_type'     => $this->getBodytypeById($r['body_type']),
            'price'         => Utilities::formatPrice($r['price'],CURRENCY_CODE),
            'year'          => $r['year'],
            'miles'         => $r['miles'],
            'measure_type'  => $r['measure_type'],
            'doors'         => $r['doors'],
            'status'        => $r['status'],
            'featured'      => $r['featured']
        );
        
        // load car details.
        if( !$sql = @mysql_query("SELECT * FROM `car_details` WHERE `car_id`='".$this->reserve_list['car_id']."' LIMIT 1;") ){
            $cnx->close();
            $this->reserve_list = array();
            $this->car = array();
            
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load car details list due a query error at ' . __FILE__ . ':' . __LINE__);

            $this->form_message = $this->lang['RESERVED_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';

            return;
        }
        
        // if car details is not declared, set default values.
        if( @mysql_num_rows($sql) != 1 ){
            $this->car_details = array(
                'id'                => $this->lang['RESERVED_UNDEFINED_LABEL'],
                'car_id'            => $this->lang['RESERVED_UNDEFINED_LABEL'],
                'engine_size'       => $this->lang['RESERVED_UNDEFINED_LABEL'],
                'trim'              => $this->lang['RESERVED_UNDEFINED_LABEL'],
                'type'              => $this->lang['RESERVED_UNDEFINED_LABEL'],
                'gear'              => $this->lang['RESERVED_UNDEFINED_LABEL'],
                'fuel'              => $this->lang['RESERVED_UNDEFINED_LABEL'],
                'color'             => $this->lang['RESERVED_UNDEFINED_LABEL'],
                'prev_owners'       => $this->lang['RESERVED_UNDEFINED_LABEL'],
                'last_service'      => $this->lang['RESERVED_UNDEFINED_LABEL'],
                'mot'               => $this->lang['RESERVED_UNDEFINED_LABEL'],
                'tax_band'          => $this->lang['RESERVED_UNDEFINED_LABEL'],
                'top_speed'         => $this->lang['RESERVED_UNDEFINED_LABEL'],
                'engine_torque_rpm' => $this->lang['RESERVED_UNDEFINED_LABEL'],
                'engine_power_kw'   => $this->lang['RESERVED_UNDEFINED_LABEL'],
                'transmission_type' => $this->lang['RESERVED_UNDEFINED_LABEL'],
                'html' => ''
            );
        }
        else {
            $r = @mysql_fetch_assoc($sql);
            @mysql_free_result($sql);
            
            $this->car_details = array(
                'id'                => $r['id'],
                'car_id'            => $r['car_id'],
                'engine_size'       => stripslashes($r['engine_size']),
                'trim'              => stripslashes($r['trim']),
                'type'              => stripslashes($r['type']),
                'gear'              => stripslashes($r['gear']),
                'fuel'              => stripslashes($r['fuel']),
                'color'             => stripslashes($r['color']),
                'prev_owners'       => stripslashes($r['prev_owners']),
                'last_service'      => stripslashes($r['last_service']),
                'mot'               => stripslashes($r['mot']),
                'tax_band'          => stripslashes($r['tax_band']),
                'top_speed'         => stripslashes($r['top_speed']),
                'engine_torque_rpm' => stripslashes($r['engine_torque_rpm']),
                'engine_power_kw'   => stripslashes($r['engine_power_kw']),
                'transmission_type' => stripslashes($r['transmission_type']),
                'html'              => stripslashes($r['html'])
            );
        }
        
        // load car media.
        if( !$sql = @mysql_query("SELECT * FROM `car_media` WHERE `car_id`='".$this->reserve_list['car_id']."'") ){
            $cnx->close();
            $this->reserve_list = array();
            $this->car = array();
            $this->car_details = array();
            
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load car media list due a query error at ' . __FILE__ . ':' . __LINE__);

            $this->form_message = $this->lang['RESERVED_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';

            return;
        }
        
        $this->car_media = array();
        
        if( @mysql_num_rows($sql) > 0 ){
            while( $r = @mysql_fetch_assoc($sql) ){
                $obj = array(
                    'id'        => $r['id'],
                    'car_id'    => $r['car_id'],
                    'type'      => $r['type'],
                    'mode'      => $r['mode'],
                    'source'    => $r['source']
                );
                
                array_push($this->car_media,$obj);
            }
            
            @mysql_free_result($sql);
        }
        else {
            // no media found. add default image.
            array_push($this->car_media, 'image/default_main_image.jpg');
        }
        
        // load car features.
        if( !$sql = @mysql_query("SELECT * FROM `car_features` WHERE `car_id`='".$this->reserve_list['car_id']."'") ){
            $cnx->close();
            $this->reserve_list = array();
            $this->car = array();
            $this->car_details = array();
            $this->car_media = array();
            
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load car features list due a query error at ' . __FILE__ . ':' . __LINE__);

            $this->form_message = $this->lang['RESERVED_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';

            return;
        }
        
        $this->car_features = array();
        
        if( @mysql_num_rows($sql) > 0 ){
            while( $r = @mysql_fetch_assoc($sql) ){
                $obj = array(
                    'id'        => $r['id'],
                    'car_id'    => $r['car_id'],
                    'feat_name' => stripslashes($r['feat_name']),
                    'feat_icon' => $r['feat_icon']
                );
                
                array_push($this->car_features,$obj);
            }
            
            @mysql_free_result($sql);
        }
        
        $cnx->close();
        return;
    }
    
    /**
     * Private method: get body type by id
     * <br>---------------------------------------------------------------------
     * @param $bodyid The body type id.
     * @return string
     */
    private function getBodytypeById($bodyid){
        $path = BASE_ROOT . 'language/' . $_SESSION['log_language_iso'] . '/car-body-types.php';
        
        if( !file_exists($path) ){
            $result = $this->lang['RESERVED_UNKNOWN_BODY_LABEL'];
        }
        else {
            require_once $path;
            
            $result = $this->lang['RESERVED_UNKNOWN_BODY_LABEL'];
            
            foreach($_CAR_BODY as $k => $v ){
                if( $k == $bodyid ){
                    $result = $v;
                    break;
                }
            }
        }
        
        return $result;
    }
    
    /**
     * Public method: get car media
     * <br>---------------------------------------------------------------------
     * @return array
     */
    public function getCarMedia(){
        return $this->car_media;
    }
    
    /**
     * Public method: get car list
     * <br>---------------------------------------------------------------------
     * @return array
     */
    public function getCar(){
        return $this->car;
    }
    
    /**
     * Public method: get car details
     * <br>---------------------------------------------------------------------
     * @return array
     */
    public function getCarDetails(){
        return $this->car_details;
    }
    
    /**
     * Public method: get car features
     * <br>---------------------------------------------------------------------
     * @return array
     */
    public function getCarFeatures(){
        return $this->car_features;
    }
    
    /**
     * Public method: get reservation list
     * <br>---------------------------------------------------------------------
     * @return array
     */
    public function getReservationList(){
        return $this->reserve_list;
    }
    
    /**
     * Public method: get form status
     * <br>---------------------------------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }

    /**
     * Public method: get form message 
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /**
     * Public method: get form style
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    } 
}
