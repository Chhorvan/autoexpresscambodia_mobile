<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
include('view/3rdparty/pagination/pagination.php');
class publicShippingEstimate
{
    protected $lang;
    protected $group = 'public';
    protected $slug = 'car';
    protected $form_status;
    protected $form_message;
    protected $form_style;
    protected $shippingInfoList=array();
    protected $total_num_row;
    var $pagination_html;
    /*
     * Constructor -------------------------------------------------------------
     */
    public function publicShippingEstimate($lang=array())
    {
        $this->lang = $lang;
		$this->loadShippingInfoList();
        $this->loadCountryList();
        $this->loadVesselTypeList();
        $this->loadLineList();
    }

    /*
     * Public method: get page content data ------------------------------------
     * @return array | false.
     */
    public function getPageHTML()
    {
        require_once BASE_ROOT . 'core/class-connect.php';

        $cnx = new Connect();
        $cnx->open();

        if( !$sql = @mysql_query("SELECT * FROM `page` WHERE `page_slug`='$this->slug' AND `page_group`='$this->group' LIMIT 1;") )
        {
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }

        if( @mysql_num_rows($sql) != 1 )
        {
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }

        $r = @mysql_fetch_assoc($sql);

        $result                     = array();
        $result['id']               = $r['id'];
        $result['meta_keywords']    = $r['meta_keywords'];
        $result['meta_description'] = $r['meta_description'];
        $result['meta_robots']      = $r['meta_robots'];
        $result['page_title']       = $r['page_title'];
        $result['page_slug']        = $r['page_slug'];
        $result['page_publish']     = $r['page_publish'];
        $result['page_author']      = $r['page_author'];
        $result['creation']         = $r['creation'];
        $result['editable']         = $r['editable'];
        $result['page_group']       = $r['page_group'];
        $result['html']             = $r['html'];

        @mysql_free_result($sql);
        $cnx->close();

        return $result;
    }
	/*
     * Public method: get shipping info list data ------------------------------------
     * @return array | false.
     */



    public function loadShippingInfoList()
    {
        require_once BASE_ROOT . 'core/class-connect.php';
        
        $where = " WHERE 1 ";

        if(isset($_GET['countryLoadingSelect'])) $countryLoadingSelect=$_GET['countryLoadingSelect']; else $countryLoadingSelect="";
        if(isset($_GET['countryDestSelect']))    $countryDestSelect=$_GET['countryDestSelect']; else $countryDestSelect="";
        if(isset($_GET['portLoadingSelect'])) $portLoadingSelect=$_GET['portLoadingSelect']; else $portLoadingSelect="";
        if(isset($_GET['portDestSelect'])) $portDestSelect=$_GET['portDestSelect']; else $portDestSelect="";
        if(isset($_GET['vesselTypeSelect'])) $vesselTypeSelect=$_GET['vesselTypeSelect']; else $vesselTypeSelect="";
        if(isset($_GET['sizeSelect'])) $sizeSelect=$_GET['sizeSelect']; else $sizeSelect="";

        $countryLoadingSelect=stripcslashes(mysql_real_escape_string($countryLoadingSelect));
        $countryDestSelect=stripcslashes(mysql_real_escape_string($countryDestSelect));
        $portLoadingSelect=stripcslashes(mysql_real_escape_string($portLoadingSelect));
        $portDestSelect=stripcslashes(mysql_real_escape_string($portDestSelect));
        $vesselTypeSelect=stripcslashes(mysql_real_escape_string($vesselTypeSelect));
        $sizeSelect=stripcslashes(mysql_real_escape_string($sizeSelect));

        if(!empty($countryLoadingSelect))
            $where.=" AND `country_loading`='$countryLoadingSelect' ";
        if(!empty($countryDestSelect))
            $where.=" AND `country_dest`='$countryDestSelect' ";
        if(!empty($portLoadingSelect))
            $where.=" AND `port_loading`='$portLoadingSelect' ";
        if(!empty($portDestSelect))
            $where.=" AND `port_dest`='$portDestSelect' ";
        if(!empty($vesselTypeSelect))
            $where.=" AND `vessel_type`='$vesselTypeSelect' ";
        if(!empty($sizeSelect))
            $where.=" AND `size`='$sizeSelect' ";


        $cnx = new Connect();
        $cnx->open();
        $shippingInfoList=array();
        /////PAGINATION PROCESS//////
        $sql_count_str="SELECT `sl`.*, `cl`.`country_name` as `cl_name`, `cd`.`country_name` as `cd_name`, `rg`.`name` as `author`
            FROM `shipping_info` AS `sl` 
            INNER JOIN `country_list` AS `cl` ON `sl`.`country_loading`= `cl`.`cc`
            INNER JOIN `country_list` AS `cd` ON `sl`.`country_dest`= `cd`.`cc`
            INNER JOIN `register` AS `rg` ON `sl`.`owner`=`rg`.`id`
            $where
            ORDER BY `sl`.`written_date` DESC
            ";
        $sql_count = @mysql_query($sql_count_str);
        $this->total_num_row= @mysql_num_rows($sql_count);
        
        $links = new Pagination ($this->total_num_row);
        $limit=$links->start_display;
        $this->pagination_html.= $links->display(); 
        ///////PAGINATION PROCESS///////
        $sql_str=$sql_count_str.$limit;    
         
        if( !$sql = @mysql_query($sql_str)){
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to shipping info list at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }
        
        
        while($r = @mysql_fetch_assoc($sql)){
           $shippingInfoList[] = $r;
           
        }
        $this->shippingInfoList=$shippingInfoList;
        @mysql_free_result($sql);
        $cnx->close();
    }
    /**
     * Private method: load country list
     * <br>---------------------------------------------------------------------
     * @return array
     */
    private function loadCountryList(){
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        if( !$sql = @mysql_query("SELECT * FROM `country_list` ORDER BY `country_name` ASC") ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load country list due a query error at ' . __FILE__ . ':' . __LINE__);
            
            $this->country_list = array();
            
            return;
        }
       
        if( @mysql_num_rows($sql) < 0 ){
            $cnx->close();          
            return;
        }
        
        $this->country_list = array();
        
        while( $r = @mysql_fetch_assoc($sql) ){
            $obj['cc'][] = $r['cc'];
            $obj['country_name'][] = stripslashes($r['country_name']);
           
            
            $this->country_list=$obj;
        }
       
        @mysql_free_result($sql);
        $cnx->close();
    }
    /**
     * Private method: load vessel type list
     * <br>---------------------------------------------------------------------
     * @return array
     */
    private function loadVesselTypeList(){
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        if( !$sql = @mysql_query("SELECT DISTINCT `vessel_type` FROM `shipping_info` WHERE `vessel_type` <> '' ORDER BY `vessel_type` ASC") ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load vessel type due a query error at ' . __FILE__ . ':' . __LINE__);
            $this->vessel_type_list = array();
            return;
        }
       
        if( @mysql_num_rows($sql) < 0 ){
            $cnx->close();          
            return;
        }
        
        $this->vessel_type_list = array();
        
        while( $r = @mysql_fetch_assoc($sql) ){
            if($r['vessel_type']!="Other")  
                $this->vessel_type_list[]=$r['vessel_type'];
        }
       
        @mysql_free_result($sql);
        $cnx->close();
    }
    /**
     * Private method: load vessel type list
     * <br>---------------------------------------------------------------------
     * @return array
     */
    private function loadLineList(){
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        if( !$sql = @mysql_query("SELECT DISTINCT `line` FROM `shipping_info` WHERE `line` <> '' ORDER BY `line` ASC") ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load line list a query error at ' . __FILE__ . ':' . __LINE__);
            $this->line_list = array();
            return;
        }
       
        if( @mysql_num_rows($sql) < 0 ){
            $cnx->close();          
            return;
        }
        
        $this->line_list = array();
        
        while( $r = @mysql_fetch_assoc($sql) ){
            $this->line_list[]=$r['line'];
        }
       
        @mysql_free_result($sql);
        $cnx->close();
    }
    /**
     * Public method: get vessel type list
     * <br>---------------------------------------------------------------------
     * @return array
     */
    public function getVesselTypeList(){
        return $this->vessel_type_list;
    }
    /**
     * Public method: line list
     * <br>---------------------------------------------------------------------
     * @return array
     */
    public function getLineList(){
        return $this->line_list;
    }
    /**
     * Public method: get country list
     * <br>---------------------------------------------------------------------
     * @return array
     */
    public function getCountryList(){
        return $this->country_list;
    }
    /*
     * Public method: get form status ------------------------------------------
     * @return bool.
     */

    public function getFormStatus()
    {
        return $this->form_status;
    }
    /*
     * Public method: get form status ------------------------------------------
     * @return bool.
     */
    public function getShippingInfoList()
    {
        return $this->shippingInfoList;
    }

    /*
     * Public method: get form message -----------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /*
     * Public method: get form style -------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }

}
?>
