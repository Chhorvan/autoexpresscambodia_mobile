<?php
ob_start();
if(!isset($_SESSION)) @session_start();
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
require_once BASE_CLASS . 'class-message.php';
require_once BASE_CLASS . 'class-product.php';
require_once BASE_CLASS . 'class-utilities.php';
class usermessageboxdetail
{
    protected $lang;
    protected $group = 'user';
    protected $slug = 'message';
    protected $form_status;
    protected $form_message;
    protected $form_style;
	protected $delete_form_message;
    protected $restore_form_message;
    private $outbox_pagination;
    private $inbox_pagination;
    private $trash_pagination;
    private $num_msg_per_page;
    private $box_flag;
    private $message;
    private $message_detail=array();
    private $messageAttachments=array();
    private $errorMessages=array();
    private $cmessage_id='';
    public $messageTemplates=array();
    private $productImages=array();

    /*
     * Constructor -------------------------------------------------------------
     */

    public function usermessageboxdetail($lang=array())
    {
        $this->lang = $lang;

        $this->message=new Message();
        $this->loadMessage();
        
        //$this->loadMessageAttachments();
        $this->loadMessageTemplates();
        //$this->downloadAttachment();
        if(isset($_POST['sendButton'])){
            if($this->composeMessage()){
                header("Location: messagebox-detail?result=success&id=".$this->cmessage_id);
            }else{
                //header("Location: messagebox-detail?result=fail&id=".$this->cmessage_id);
            }
        }else{
            array_merge($this->errorMessages, $this->message->getErrorMessages());
        }
    }
    
    /* Send a message */
    private function composeMessage(){
        if(isset($_POST['sendToInput'])) $sendTo=$_POST['sendToInput']; else $sendTo='';
        if(isset($_POST['subjectInput'])) $subjectInput=$_POST['subjectInput']; else $subjectInput='';
        if(isset($_POST['messageInput'])) $messageInput=$_POST['messageInput']; else $messageInput='';

        $allowedExts=array('xls', 'doc', 'docx', 'ppt', 'pptx', 'pps', 'jpg', 'gif', 'bmp', 'zip', 'pdf', 'png', 'hwp', 'txt');

        $files=$_FILES['file'];
        if(!empty($sendTo)){
            if($this->message->composeMessage($sendTo, $subjectInput, $messageInput)){
                /* Attach File if user select files */
                if(count($files)>0){
                    $this->message->attachFiles($this->message->last_insert_msg_id, $files, $allowedExts);
                }
                $this->sendEmail();
                return true;
                
            }
            array_merge($this->errorMessages, $this->message->getErrorMessages());
        }
    }
    private function sendEmail(){
        
        require BASE_CLASS . 'class-user-sendmail.php';
        require BASE_CLASS . 'class-user.php';
        /* Initial Class */
        $send = new UserSendEmail();
        $fromProfile=new User();
        $toProfile=new User();

        if(isset($_POST['sendToInput'])) $sendTo=$_POST['sendToInput']; else $sendTo='';
        if(isset($_POST['subjectInput'])) $subjectInput=$_POST['subjectInput']; else $subjectInput='';
        if(isset($_POST['messageInput'])) $messageInput=$_POST['messageInput']; else $messageInput='';

        if(isset($_SESSION['company_name'])) $fromCompanyName=$_SESSION['company_name'];
        if(isset($_SESSION['log_id'])) $log_id=$_SESSION['log_id'];
        if(isset($_SESSION['log_email'])) $log_email=$_SESSION['log_email']; else $log_email='';

        $fromProfile->loadProfile($log_id, array('address', 'country', 'address', 'tel', 'email', 'country_list.country_name as country_name'));
        $fromProfileDetail=$fromProfile->getProfile();

        $toProfile->loadProfile($sendTo, array('email', 'address', 'company_name', 'country_list.country_name as country_name'));
        $toProfileDetail=$toProfile->getProfile();
        /* Set values to member of class */
        /* $send->setId($this->message_detail['product_id']);
        $send->setModelYear($this->stock_detail['0']['model_year']);
        $send->setModel($this->stock_detail['0']['model']);
        $send->setMake($this->stock_detail['0']['make']);
        $send->setProductImage($this->stock_detail['0']['thumb']);*/
        $sentMessageAttachments=$this->message->getMessageAttachments($this->message->last_insert_msg_id);
        
        $allAttachmentHtml = $this->getAllAttachmentHtml($sentMessageAttachments);
    
        $send->setMessage($messageInput);
        $send->setMessageAttachmentContainer($allAttachmentHtml);
        $send->setSubject($subjectInput);
        $send->setEmail($toProfileDetail['email']);
        $send->setToCompanyName($toProfileDetail['company_name']);
        
        $send->setFromCompanyName($fromCompanyName);
        $send->setEmailFrom($log_email);
        $send->setFromProfileLink($fromProfile->getProfileLink());
        //$send->setFromAddress($fromProfileDetail['address'].", ".$fromProfileDetail['country_name']);
        $send->setFromProfileContainer(array("Company Name"=>"<a href='".$fromProfile->getProfileLink()."'/>".$fromCompanyName."</a>", 
                                                "Country"=>"<img src='".$fromProfile->getFlagUrl($fromProfileDetail['country'])."'/>".$fromProfileDetail['country_name'],
                                                "Address"=>$fromProfileDetail['address'],
                                                "Tel"=>$fromProfileDetail['tel'],
                                                "Email"=>$fromProfileDetail['email']
                                    ));
        $send->setFromProfileImage($fromProfile->getPrimaryThumbUrl());
      
        //var_dump($send);
        $send->send();

        //echo $send->viewMessage();
        $this->result="success";
        //Clear all inquiry after sending email
        return true;
    }
    private function getAllAttachmentHtml($messageAttachments = array()){
        /* Initial Variable */
        $attach_icon['pic_icon']=array('jpg', 'gif', 'bmp', 'png');
        $attach_icon['excel_icon']=array('xls');
        $attach_icon['word_icon']=array('doc', 'docx');
        $attach_icon['ppt_icon']=array('ppt', 'pptx');
        $attach_icon['zip_icon']=array('zip');
        $attach_icon['pdf_icon']=array('pdf');
        $attach_icon['hwp_icon']=array('hwp');
        $attach_icon['txt_icon']=array('txt');
        $html='<table>';

        foreach($messageAttachments as $attachment){

            /* explode to array temp to get extension */
            $temp = explode(".", $attachment['file_name']);

            /* get extension eg. jpg or gif*/
            $extension = strtolower(end($temp));
            
            $src='unknown_icon.png';
            foreach($attach_icon as $key=>$icon){
                if(in_array($extension, $icon)){
                    $src=$key.".png";
                }
            }

            $html.= "<tr>
                    <td><a target='_blank' href='".BASE_SITE."download-attachment?download={$attachment['encode_name']}'><img src='".BASE_SITE."images/$src'/></a></td>
                    <td><a target='_blank' href='".BASE_SITE."download-attachment?download={$attachment['encode_name']}'>{$attachment['file_title']}</a></td>
                    <td><a target='_blank' href='".BASE_SITE."download-attachment?download={$attachment['encode_name']}'><img title='download' alt='download' src='".BASE_SITE."images/download_icon.png'/></a></td>
                </tr>
            ";
        }
        $html.='</table>';
        return $html;
    }
    public function loadMessageTemplates(){
        /* GET ALL MESSAGE TEMPLATES*/
        $this->messageTemplates=$this->message->getMessageTemplates();
    }
    /* GET MESSAGE DETAIL BY URL GET ID */
    public function loadMessage(){
       
        /* Initial Variable */
        if(isset($_GET['id'])) $id=$_GET['id']; else $id='';
        if(!empty($id)){
            /* GET message by message id */
            $message_detail=$this->message->getMessageById($id, array('sender.company_name as sender_company', 'receiver.company_name as receiver_company'));
            $this->message->markAsSeenMessage($id);
            $this->cmessage_id=$id;
            $this->message_detail=$message_detail;
            /* GET array encode names */
            $this->messageAttachments=$this->message->getMessageAttachments($id);
        }else{
            $this->errorMessages[]="Unable to get Message detail. Please open any message in your message box.";
        }
    }

    public function getProductDetail($id){
        /* Class Implement */
        $product=new Product();
        $message_detail=$this->getMessage();
        $product_detail=$product->getProductById($message_detail['product_id'], array('steering', 'location', 'city', 'price', 'owner', 'country_name', 'company_name'));
        $this->productImages=$product->getProductImagesById($message_detail['product_id']);
        return $product_detail;
    }
    /* GET MESSAGE DETAIL  */
    public function getMessageAttachments(){
        return $this->messageAttachments;
    }
    /* GET MESSAGE DETAIL  */
    public function getMessage(){
        return $this->message_detail;
    }
    /*
     * Public method: get form status ------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }
    /* GET Error Message */
    public function getErrorMessages(){
        return $this->errorMessages;
    }
    /*
     * Public method: get form message -----------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /*
     * Public method: get form style -------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }

    public function getMessageTemplates(){
        /* GET ALL MESSAGE TEMPLATES*/
        return $this->messageTemplates;
    }
    public function getProductImages(){
        /* GET ALL Product images */
        return $this->productImages;
    }
}
?>
