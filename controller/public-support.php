<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
class publicsupport
{
    protected $lang;
    protected $form_status;
    protected $form_message;
    protected $form_style;
    
    /**
     * Constructor
     * <br>---------------------------------------------------------------------
     * @param type $lang
     */
    public function publicsupport($lang=array())
    {
        $this->lang = $lang;
        
        // handle submit form.
        if( isset($_POST['sendbtn']) )
        {
            $this->handleForm();
        }
    }
    
    /**
     * Private method: submit send support message form.
     * <br>---------------------------------------------------------------------
     * @return void.
     */
    private function handleForm()
    {
        ( isset($_POST['nameInput']) ? $name = trim($_POST['nameInput']) : $name = '');
        ( isset($_POST['emailInput']) ? $email = trim($_POST['emailInput']) : $email = '');
        ( isset($_POST['deptInput']) ? $dept = trim($_POST['deptInput']) : $dept = '' );
        ( isset($_POST['subjectInput']) ? $subject = trim($_POST['subjectInput']) : $subject = '' );
        ( isset($_POST['messageInput']) ? $message = trim($_POST['messageInput']) :  $message = '' );
        ( isset($_POST['acptInput']) ? $accept = trim($_POST['acptInput']) : $accept = '' );
        
        // set form session.
        $_SESSION['support_name'] = $name;
        $_SESSION['support_email'] = $email;
        $_SESSION['support_dept'] = $dept;
        $_SESSION['support_subject'] = $subject;
        $_SESSION['support_message'] = $message;
        
        // validate.
        if( strlen($name) < 3 )
        {
            $this->form_message = $this->lang['SUPPORT_INVALID_NAME'];
            $this->form_status = true;
            $this->form_style = 'alert-error';
            return;
        }
        
        require_once BASE_CLASS . 'class-utilities.php';
        
        if( !Utilities::checkEmail($email) )
        {
            $this->form_message = $this->lang['SUPPORT_INVALID_EMAIL'];
            $this->form_status = true;
            $this->form_style = 'alert-error';
            return;
        }
        
        if( strlen($subject) < 3 )
        {
            $this->form_message = $this->lang['SUPPORT_INVALID_SUBJECT'];
            $this->form_status = true;
            $this->form_style = 'alert-error';
            return;
        }
        
        if( strlen($message) < 5 )
        {
            $this->form_message = $this->lang['SUPPORT_INVALID_MESSAGE'];
            $this->form_status = true;
            $this->form_style = 'alert-error';
            return;
        }
        
        if( $accept != 'yes' )
        {
            $this->form_message = $this->lang['SUPPORT_INVALID_ACCEPT'];
            $this->form_status = true;
            $this->form_style = 'alert-error';
            return;
        }
        
        unset($_SESSION['support_name']);
        unset($_SESSION['support_email']);
        unset($_SESSION['support_dept']);
        unset($_SESSION['support_subject']);
        unset($_SESSION['support_message']);
        
        $name = stripslashes($name);
        $email = stripslashes($email);
        $dept = stripslashes($dept);
        $subject = stripslashes($subject);
        $message = stripslashes($message);
        $message = nl2br($message);
        
        // send email to administrator.
        require_once BASE_CLASS . 'class-sendmail.php';
        
        $mail = new SendEmail();
        $mail->setEmail(ADMIN_CONTACT_EMAIL);
        $mail->setSubject($subject);
        $mail->setMessage($message);
        $mail->send();
        
        // register message to database.
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        $name = mysql_real_escape_string($name);
        $email = mysql_real_escape_string($email);
        $dept = mysql_real_escape_string($dept);
        $subject = mysql_real_escape_string($subject);
        $message = mysql_real_escape_string($message);
        
        $msg = '<b>Department:</b> ' . $dept . '<br/>';
        $msg .= $message;
        
        if( !@mysql_query("INSERT INTO `contact` (`email`,
                                                  `name`,
                                                  `subject`,
                                                  `message`,
                                                  `creation`,
                                                  `replied`,
                                                  `read`,
                                                  `ip`,
                                                  `browser`) VALUES (
                                                  '$email',
                                                  '$name',
                                                  '$subject',
                                                  '$msg',
                                                  '".date('Y-m-d')."',
                                                  '0',
                                                  '0',
                                                  '".$_SERVER['REMOTE_ADDR']."',
                                                  '".$_SERVER['HTTP_USER_AGENT']."')") )
        {
            $cnx->close();
            
            require_once BASE_CLASS . 'class-log.php';
            
            LogReport::write('Unable to save contact message at ' . __FILE__ . ':' . __LINE__ . ' Message: ' . $msg);
        }
        
        $cnx->close();
        
        $this->form_message = $this->lang['SUPPORT_MESSAGE_SENT'];
        $this->form_status = true;
        $this->form_style = 'alert-success';
        return;
    }
    
    /**
     * Public method: get form status
     * <br>---------------------------------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }
    
    /**
     * Public method: get form message
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }
    
    /**
     * Public method: get form style
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }
}