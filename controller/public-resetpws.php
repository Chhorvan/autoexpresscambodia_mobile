
<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
class publicresetpws
{
    protected $lang;
    protected $group = 'public';
    protected $slug = 'car';
    protected $form_status;
    protected $form_message;
    protected $form_style;
	
	var $message_error;
	var $old_passwrod;
    /*
     * Constructor -------------------------------------------------------------
     */
    public function publicresetpws($lang=array())
    {
        $this->lang = $lang;
		if(isset($_POST['resetpws'])){
			if($_POST['password']=="" and $_POST['re-password']==""){
			   $this->message_error="Please reset again";
			}
			else{
			   $this->resetPassword();  
			}
		}
		
    }

    /*
     * Public method: get page content data ------------------------------------
     * @return array | false.
     */
    public function getPageHTML()
    {
        require_once BASE_ROOT . 'core/class-connect.php';

        $cnx = new Connect();
        $cnx->open();

        if( !$sql = @mysql_query("SELECT * FROM `page` WHERE `page_slug`='$this->slug' AND `page_group`='$this->group' LIMIT 1;") )
        {
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }

        if( @mysql_num_rows($sql) != 1 )
        {
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }

        $r = @mysql_fetch_assoc($sql);

        $result                     = array();
        $result['id']               = $r['id'];
        $result['meta_keywords']    = $r['meta_keywords'];
        $result['meta_description'] = $r['meta_description'];
        $result['meta_robots']      = $r['meta_robots'];
        $result['page_title']       = $r['page_title'];
        $result['page_slug']        = $r['page_slug'];
        $result['page_publish']     = $r['page_publish'];
        $result['page_author']      = $r['page_author'];
        $result['creation']         = $r['creation'];
        $result['editable']         = $r['editable'];
        $result['page_group']       = $r['page_group'];
        $result['html']             = $r['html'];

        @mysql_free_result($sql);
        $cnx->close();

        return $result;
    }
	

    /*
     * Public method: get form status ------------------------------------------
     * @return bool.
     */
	 
	public function resetPassword(){
		
		require_once BASE_ROOT . 'core/class-connect.php';

        $cnx = new Connect();
        $cnx->open();
		
		$keyReset = $_POST['keyReset'];
		
		$newpws = md5($_POST['password']);
		$confirm = md5($_POST['re-password']);
		
		if($newpws === $confirm){	

			$sql="SELECT `password` FROM register WHERE key_resetpws='".$keyReset."'";
			$query_sql=mysql_query($sql);
			$row=mysql_fetch_array($query_sql);
			$row_pass=$row['password'];
			
			if($row_pass==$newpws){
				$this->old_passwrod='Old password can not update';
				//echo"Old password can not update";
			}
			else{

			$query = "UPDATE register SET password='".$newpws."' WHERE key_resetpws='".$keyReset."'";
			if($result = mysql_query($query)){
				echo"
				<script>
					window.location='change-password';
				</script> 
				";	
			}
		  }
		}
		else{
		      $this->message_error="Please reset again";
		}
		$cnx->close();
	}
	
    public function getFormStatus()
    {
        return $this->form_status;
    }

    /*
     * Public method: get form message -----------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /*
     * Public method: get form style -------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }

}
?>
