<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
include('view/3rdparty/pagination/pagination.php');
class publicCommunityNews
{
    protected $lang;
    protected $group = 'public';
    protected $slug = 'community-news';
    protected $form_status;
    protected $form_message;
    protected $form_style;
    private $news_list;
 
    var $total_num_row;
    var $pagination_html;
     var $current_page;
    var $total_page;
 

    /*
     * Constructor -------------------------------------------------------------
     */
    public function publicCommunityNews($lang=array())
    {
        $this->lang = $lang;
		$this->loadNewsList();
    }

    public function gettotal()
    {
        return $this->total_num_row;
    }

    public function gettotalpage()
    {
        return $this->total_page;
    }

    public function getcurrentpage()
    {
        return $this->current_page;
    }
 
    private function loadNewsList(){
        require_once BASE_CLASS . 'class-connect.php';
        require_once BASE_CLASS . 'class-utilities.php';
        
        $cnx = new Connect();
        $cnx->open();

        $sql_str="SELECT * FROM `news` ORDER BY `id` DESC";        
        /////PAGINATION PROCESS//////

        $sql_count_str=$sql_str;
        $sql_count = @mysql_query($sql_count_str);
        $this->total_num_row= @mysql_num_rows($sql_count);
        
 
        $links = new Pagination ($this->total_num_row);
 
        $limit=$links->start_display;
        $this->pagination_html.= $links->display(); 
         $this->current_page = $links->currentPage();
            $this->total_page= $links->numPages();
        ///////PAGINATION PROCESS///////
        $sql_str.=$limit;  
        // load news.
        if( !$sql = @mysql_query($sql_str) ){

            require_once BASE_CLASS . 'class-log.php';
            $cnx->close();
            
            LogReport::write('Unable to load news list due a query error at ' . __FILE__ . ':' . __LINE__);
            
            $this->form_message = $this->lang['MANAGE_NEWS_LOAD_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        
        $this->news_list = array();
        
        if( @mysql_num_rows($sql) < 1 ){
            $cnx->close();
            return;
        }
        
        ( $_SESSION['log_language_iso'] == 'en' ? $format = 'yyyymmdd' : $format = 'ddmmyyyy' );
        
        while( $r = @mysql_fetch_assoc($sql) ){
            $day = explode('-',$r['date']);
            $date = Utilities::checkDateFormat($day[0],$day[1],$day[2],$format);
            $date=date("F j, Y",strtotime($r['date']));
            $obj = array(
                'id' => $r['id'],
                'title' => stripslashes($r['title']),
                'type' => stripslashes($r['product_type']),
                'html' => stripslashes($r['html']),
                'date' => $date,
                'time' => $r['time'],
                'author' => stripslashes($r['author'])
            );
            array_push($this->news_list,$obj);
        }
        
        @mysql_free_result($sql);
        $cnx->close();
    }

    public function getNewsList(){
        return $this->news_list;
    }
    

    /*
     * Public method: get page content data ------------------------------------
     * @return array | false.
     */



    public function getPageHTML()
    {
        require_once BASE_ROOT . 'core/class-connect.php';

        $cnx = new Connect();
        $cnx->open();

        if( !$sql = @mysql_query("SELECT * FROM `page` WHERE `page_slug`='$this->slug' AND `page_group`='$this->group' LIMIT 1;") )
        {
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }

        if( @mysql_num_rows($sql) != 1 )
        {
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }

        $r = @mysql_fetch_assoc($sql);

        $result                     = array();
        $result['id']               = $r['id'];
        $result['meta_keywords']    = $r['meta_keywords'];
        $result['meta_description'] = $r['meta_description'];
        $result['meta_robots']      = $r['meta_robots'];
        $result['page_title']       = $r['page_title'];
        $result['page_slug']        = $r['page_slug'];
        $result['page_publish']     = $r['page_publish'];
        $result['page_author']      = $r['page_author'];
        $result['creation']         = $r['creation'];
        $result['editable']         = $r['editable'];
        $result['page_group']       = $r['page_group'];
        $result['html']             = $r['html'];

        @mysql_free_result($sql);
        $cnx->close();

        return $result;
    }
	

    /*
     * Public method: get form status ------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }

    /*
     * Public method: get form message -----------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /*
     * Public method: get form style -------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }

}
?>
