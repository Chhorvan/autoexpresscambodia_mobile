<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
class publicbuyerinquiryDetail
{
    protected $lang;
    protected $group = 'public';
    protected $slug = 'buyerinquiry';
    protected $form_status;
    protected $form_message;
    protected $form_style;
    protected $list_inuquliry;
    protected $list_user;    
     
    

    /*
     * Constructor -------------------------------------------------------------
     */
    public function publicbuyerinquiryDetail($lang=array())
    {
        $this->lang = $lang;
        $this->listinquiry();
        $this->listuser();
        
        if( isset($_POST['submit']) )
        {
            $this->processReplyForm();
        }

    }


    function loadExpertCompany(){       
       
        //echo $item.''.$category;
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
       
        
        ////////
        $sql_search="SELECT * from register ORDER BY id DESC limit 5"; 
        
        //echo $sql_search;
        if( !$sql = @mysql_query($sql_search) ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load car maker list due a query error at ' . __FILE__ . ':' . __LINE__);
            $cnx->close();
            return;
        }

        if( @mysql_num_rows($sql) < 1 ){
            $cnx->close();
        }
        
         
        $expertCompany = array(); 
       
        while( $r = @mysql_fetch_assoc($sql)){
           
            array_push($expertCompany, $r);
          
        }
         
  
        @mysql_free_result($sql);
        $cnx->close();
        return $expertCompany;

    }

    /*
     * Public method: get page content data ------------------------------------
     * @return array | false.
     */
    public function listuser()
    {
        if(isset($_SESSION['log_email']))
        {
            $id=$_SESSION['log_email'];
            require_once BASE_ROOT . 'core/class-connect.php';
            $cnx = new Connect();
            $cnx->open();
            if( !$sql = @mysql_query("SELECT r.*,c.`cc`,c.`country_name` 
             FROM  `register` r LEFT JOIN `country_list` c ON r.`country`=c.`country_name` 
             WHERE `email`='$id';") )
            {
                $cnx->close();

                require_once BASE_ROOT . 'core/class-log.php';

                LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());
      
                return;
            }
            else
            {

            $this->list_user = array();
            while( $r = @mysql_fetch_assoc($sql) ){

                array_push($this->list_user, $r);
     
            }
        $cnx->close();
        //var_dump($this->list_user);
        return $this->list_user; 
            }
        }

    }

    public function listinquiry()
    {
        require_once BASE_ROOT . 'core/class-connect.php';
        $id=0;
        if(isset($_GET['id'])){
            $id=$_GET['id']; 
        }
        
        $cnx = new Connect();
        $cnx->open();
        if( !$sql = @mysql_query("SELECT  i.*,r.`company_name`,r.`name`,
             r.`card_id`,r.`tel`,r.`fax`,r.`country`,r.`address`,r.`image`,r.`email`  
             FROM `inquiry` `i` LEFT JOIN `register` `r` ON i.`owner`=r.`id`
             WHERE i.`id`='$id';") )
        {
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }
         $this->list_inuquliry = array();
        while( $r = @mysql_fetch_assoc($sql) ){

            array_push($this->list_inuquliry, $r);
     
        }
        $cnx->close();
        //var_dump($this->list_inuquliry);
        return $this->list_inuquliry;


    }

    public function getPageHTML()
    {
        require_once BASE_ROOT . 'core/class-connect.php';

        $cnx = new Connect();
        $cnx->open();

        if( !$sql = @mysql_query("SELECT * FROM `page` WHERE `page_slug`='$this->slug' AND `page_group`='$this->group' LIMIT 1;") )
        {
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }

        if( @mysql_num_rows($sql) != 1 )
        {
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }

        $r = @mysql_fetch_assoc($sql);

        $result                     = array();
        $result['id']               = $r['id'];
        $result['meta_keywords']    = $r['meta_keywords'];
        $result['meta_description'] = $r['meta_description'];
        $result['meta_robots']      = $r['meta_robots'];
        $result['page_title']       = $r['page_title'];
        $result['page_slug']        = $r['page_slug'];
        $result['page_publish']     = $r['page_publish'];
        $result['page_author']      = $r['page_author'];
        $result['creation']         = $r['creation'];
        $result['editable']         = $r['editable'];
        $result['page_group']       = $r['page_group'];
        $result['html']             = $r['html'];

        @mysql_free_result($sql);
        $cnx->close();

        return $result;
    }

    /*
     * Public method: get form status ------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }

    /*
     * Public method: get form message -----------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /*
     * Public method: get form style -------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }

    public function loadMsgTemplate(){
        require_once BASE_ROOT . 'core/class-connect.php';
        $cnx = new Connect();
        $cnx->open();

        $sql_query = "SELECT * FROM message_template";
        if(!$rows = mysql_query($sql_query)){
            die("Unable to load message template. ". __FILE__ .':'. __LINE__);
        }

        $message_templates = array();
        while($record = mysql_fetch_array($rows)){
            array_push($message_templates, $record);
        }
        $cnx->close();

        return $message_templates;
    }

    public function processReplyForm()
    {        
        $sender = "";
        $receiver = "";
        $product = "";
        $i_sMessage = "";
        $str = "";
        $m_id = "";
        
        extract($_POST);
        extract($_GET);
        
        $get_ip = $this->getIpAddresses();
        $ips = $get_ip['user'];
        
        $str = $i_sMessage;
        
        $i = strrpos($str, "---------- [ Original Message ] ----------");
        $l = strlen($str) - $i;
        $str = substr($str, $i, $l);
        $i_sMessage = str_ireplace($str, "", $i_sMessage);
        
        $breaks = array("\n");
        $i_sMessage = str_ireplace($breaks, "<br />", $i_sMessage);
        
        require_once dirname(dirname(__FILE__)) . '/config.php';
        require_once BASE_ROOT . 'core/class-connect.php';

        $cnx = new Connect();
        $cnx->open();
        //$dir = dirname(dirname(__FILE__)). "/upload/message-attachment/";
        
        $row = mysql_fetch_array(mysql_query("SELECT * FROM message ORDER BY id DESC"));
        $id = $row['id'];
        $id++;

        $row_rep = mysql_fetch_array(mysql_query("SELECT * FROM inquiry WHERE id = '{$_GET['id']}'"));
        
        $port = $row_rep['port'];
        $pay_term = $row_rep['payment_terms'];
        $price_term = $row_rep['price_term'];
        $country = mysql_fetch_array(mysql_query("SELECT country_name FROM country_list WHERE cc = '{$row_rep['country']}'"));
        $country = $country['country_name'];
        $buyer_id = $row_rep['owner'];

        if( $sql = mysql_query("INSERT INTO `message` (`id`, `message`, `sent_id`, `response_id`, `duration`, `seen`, `country`, `port_name`, `payment_term`, `price_term`, `buyer_id`, `ip`) VALUES('$id++', '$i_sMessage', '$sender', '$receiver', '".date('Y-m-d H:i:s')."', '0', '$country', '$port', '$pay_term', '$price_term', $buyer_id, '$ips')") ) {           
            if(isset($_POST['att_file_name'])){
                require_once BASE_CLASS . 'class-file-system.php';
                
                foreach ($_POST['att_file_name'] as $key => $value) {
                    $explode_file = explode('/', $_POST['att_file_name'][$key]);
                    $file_name = $explode_file[0];
                    $string_date = $explode_file[1]; 
                    $extension = end($explode_file); 
                    $encode_name = md5($file_name . $string_date). '.' .$extension;                   
                    
                    $location = "/upload/message-attachment/{$receiver}/";
                    if(FileSystem::createDir($dir = dirname(dirname(__FILE__)) . $location)){
                        rename($dir. $file_name. $string_date .'.tmp', $dir. $encode_name);
                        mysql_query("INSERT INTO message_attachment (message_ref, file_name, encode_name, location) VALUES ($id, '{$file_name}.{$extension}', '$encode_name', '$location')");
                    }
                }   
            }   

            $this->form_status = 'complete';
        } 
        else 
        {
            require_once BASE_CLASS . 'class-log.php';
            
            LogReport::write('Unable to query database at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_status = 'query';
        }
        $cnx->close();
    }

    public function getIpAddresses()
    {
       $ipAddresses = array();
       if (isset($_SERVER["HTTP_X_FORWARDED_FOR"])) {

          $ipAddresses['proxy'] = isset($_SERVER["HTTP_CLIENT_IP"]) ? $_SERVER["HTTP_CLIENT_IP"] : $_SERVER["REMOTE_ADDR"];
          $ipAddresses['user'] = $_SERVER["HTTP_X_FORWARDED_FOR"];

       } else {
          $ipAddresses['user'] = isset($_SERVER["HTTP_CLIENT_IP"]) ? $_SERVER["HTTP_CLIENT_IP"] : $_SERVER["REMOTE_ADDR"];
       }
       return $ipAddresses;
    }



}
?>
