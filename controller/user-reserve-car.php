<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################

class userreservecar {
    protected $lang;
    protected $form_status;
    protected $form_message;
    protected $form_style;
    protected $car_id;
    protected $car;
    protected $reservation_policy;
    protected $allow;
    
    /**
     * Constructor
     * <br>---------------------------------------------------------------------
     */
    public function userreservecar($lang=array()){
        $this->lang = $lang;
        
        // check if user already as a reserved car.
        // only 1 car is allowed to be reserved.
        $this->allow = $this->validateUser();
        
        if( !$this->allow ){
            return;
        }
        
        // set car id
        $this->car_id = trim($_GET['cid']);
        
        if( empty($this->car_id) ){
            $this->car = array();
            return;
        }
        
        // handle reserve form.
        if( isset($_POST['okbtn']) ){
            $this->handleForm();
        }

        // include body type list.
        $path = BASE_ROOT . 'language/' . $_SESSION['log_language_iso'] . '/car-body-types.php';
        
        if( file_exists($path) ){
            require_once $path;
            $this->body_type_list = $_CAR_BODY;
        }
        else {
            $this->body_type_list = array();
        }
        
        // load car information.
        $this->loadCar();
        
        // load reservation policy.
        $this->loadPolicy();
    }
    
    /**
     * Public method: allow reservation
     * <br>---------------------------------------------------------------------
     * @return bool
     */
    public function allowReservation(){
        return $this->allow;
    }
    
    /**
     * Private method: check if user has already any reserved cars.
     * <br>---------------------------------------------------------------------
     * @return boolean
     */
    private function validateUser(){
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        if( !$sql = @mysql_query("SELECT `email` FROM `reservation` WHERE `email`='".$_SESSION['log_email']."' LIMIT 1;") ){
            require_once BASE_CLASS . 'class-log.php';
            
            LogReport::write('Unable to validate if user has already reserved cars due a query error at ' . __FILE__ . ':' . __LINE__);
            
            $cnx->close();
            
            return false;
        }
        
        $row = @mysql_num_rows($sql);
        $cnx->close();
        
        if( $row > 0 ){
            return false;
        }
        else {
            return true;
        }
    }
    
    /**
     * Public method: load reservation policy.
     * <br>---------------------------------------------------------------------
     * @return void
     */
    private function loadPolicy(){
        $path = BASE_ROOT . 'language/'.$_SESSION['log_language_iso'].'/reservation_policy.php';
        $error = false;
        
        if(file_exists($path) ){
            if( $fo = @fopen($path,'r') ){
                if( $data = @fread($fo,  filesize($path)) ){
                    $this->reservation_policy = $data;
                }
                else {
                    $error = true;
                }
            }
            else {
                $error = true;
            }
            
            @fclose($fo);
        }
        
        if( $error ){
            require_once BASE_CLASS . 'class-log.php';
            
            LogReport::write('Unable to load reservation policy due an internal error at ' . __FILE__ . ':' . __LINE__);
        }
    }
    
    /**
     * Private method: handle reservation form
     * <br>---------------------------------------------------------------------
     * @return void
     */
    private function handleForm(){
        $name       = $_SESSION['log_name'];
        $email      = $_SESSION['log_email'];
        $phone      = trim($_POST['phoneInput']);
        $address    = trim($_POST['addressInput']);
        $city       = trim($_POST['cityInput']);
        $zip        = trim($_POST['zipInput']);
        $coutry     = trim($_POST['countryInput']);
        $car_id     = trim($this->car_id);
        ( isset($_POST['agreeInput']) ? $agree = (bool)$_POST['agreeInput'] : $agree = false );
        $code       = trim($_POST['rcode']);
        
        // strip slashes.
        $name = stripslashes($name);
        $email = stripslashes($email);
        $phone = stripslashes($phone);
        $address = stripslashes($address);
        $city = stripslashes($city);
        $zip = stripslashes($zip);
        $coutry = stripslashes($coutry);
        
        // set form session.
        $_SESSION['reserve_phone'] = $phone;
        $_SESSION['reserve_address'] = $address;
        $_SESSION['reserve_city'] = $city;
        $_SESSION['reserve_zip'] = $zip;
        $_SESSION['reserve_country'] = $coutry;
        
        // validate entries.
        if( empty($phone) ){
            $this->form_message = $this->lang['RESERVE_FORM_FILLALL_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-error';
            
            return;
        }
        
        $phone = str_replace('+', '', $phone);
        $phone = str_replace('(', '', $phone);
        $phone = str_replace(')', '', $phone);
        $phone = str_replace('-', '', $phone);
        $phone = str_replace('/', '', $phone);
        $phone = str_replace(' ', '', $phone);
        $phone = str_replace('\\', '', $phone);
        
        if(is_int($phone) && strlen($phone) > 6 ){
            $this->form_message = $this->lang['RESERVE_INVALID_PHONE_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-error';
            
            return;
        }
        
        if( strlen($address) < 5 ){
            $this->form_message = $this->lang['RESERVE_INVALID_ADDRESS_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-error';
            
            return;
        }
        
        if( strlen($city) < 3 ){
            $this->form_message = $this->lang['RESERVE_INVALID_CITY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-error';
            
            return;
        }
        
        if( strlen($zip) < 4 ){
            $this->form_message = $this->lang['RESERVE_INVALID_ZIP_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-error';
            
            return;
        }
        
        if( strlen($code) < 3 ){
            $this->form_message = $this->lang['RESERVE_INVALID_COUNTRY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-error';
            
            return;
        }
        
        if( !$agree ){
            $this->form_message = $this->lang['RESERVE_INVALID_AGREE_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-error';
            
            return;
        }
        
        if( empty($code) ){
            require_once BASE_CLASS . 'class-utilities.php';
            $code = Utilities::generateRandomString(7);
        }
        
        $date = date('Y-m-d');
        $ip   = $_SERVER['REMOTE_ADDR'];
        $browser = $_SERVER['HTTP_USER_AGENT'];
        
        // unset session.
        unset($_SESSION['reserve_phone']);
        unset($_SESSION['reserve_address']);
        unset($_SESSION['reserve_city']);
        unset($_SESSION['reserve_zip']);
        unset($_SESSION['reserve_country']);
        
        // insert into database.
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        $name = @mysql_real_escape_string($name);
        $email = @mysql_real_escape_string($email);
        $phone = @mysql_real_escape_string($phone);
        $address= @mysql_real_escape_string($address);
        $city = @mysql_real_escape_string($city);
        $zip = @mysql_real_escape_string($zip);
        $country = @mysql_real_escape_string($coutry);
        $code = @mysql_real_escape_string($code);
        $userid = $_SESSION['log_id'];
        
        if( !@mysql_query("INSERT INTO `reservation` (
                                                      `car_id`,
                                                      `user_id`,
                                                      `name`,
                                                      `email`,
                                                      `phone`,
                                                      `address`,
                                                      `city`,
                                                      `zip`,
                                                      `country`,
                                                      `code`,
                                                      `browser`,
                                                      `ip`,
                                                      `date`
                                                      ) VALUES (
                                                      '$car_id',
                                                      '$userid',
                                                      '$name',
                                                      '$email',
                                                      '$phone',
                                                      '$address',
                                                      '$city',
                                                      '$zip',
                                                      '$country',
                                                      '$code',
                                                      '$browser',
                                                      '$ip',
                                                      '$date'
                                                      )") ){
            require_once BASE_CLASS . 'class-log.php';
            
            LogReport::write('Unable to save reservation due a query error at ' . __FILE__ . ':' . __LINE__ . ' ' . mysql_error());
            
            $cnx->close();
            $this->form_message = $this->lang['RESERVE_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            
            return;
        }
        
        // update car status.
        if( !@mysql_query("UPDATE `car` SET `status`='1' WHERE `id`='$car_id' LIMIT 1;") ){
            @mysql_query("DELETE FROM `reservation` WHERE `code`='$code' LIMIT 1;");
            
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to save reservation due a query error at ' . __FILE__ . ':' . __LINE__);
            
            $cnx->close();
            $this->form_message = $this->lang['RESERVE_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            
            return;
        }
        
        $cnx->close();
        
        // send user reservation confirmation email.
        require_once BASE_CLASS . 'class-sendmail.php';
        
        $mail = new SendEmail();
        $mail->setEmail($email);
        $mail->setSubject($this->lang['RESERVE_EMAIL_CONFIRM_SUBJECT']);
        
        // create message.
        $msg = $this->lang['RESERVE_MSG_DEAR'] . ' ' . $name . ',';
        $msg .= $this->lang['RESERVE_MSG_MESSAGE'];
        
        $mail->setMessage($msg);
        $mail->send();        
        
        // output message.
        $this->form_message = $this->lang['RESERVE_QUERY_SUCCESS'];
        $this->form_status = true;
        $this->form_style = 'alert-success';
        
        return;
    }
    
    /**
     * Private method: load car information
     * <br>---------------------------------------------------------------------
     * @return void
     */
    private function loadCar(){
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        if( !$sql = @mysql_query("SELECT * FROM `car` WHERE `id`='$this->car_id' AND `status`='0' LIMIT 1;") ){
            require_once BASE_CLASS . 'class-log.php';
            
            LogReport::write('Unable to load car information due a query error at ' . __FILE__ . ':' . __LINE__);
            
            $cnx->close();
            return;
        }
        
        $this->car = array();
        
        if( @mysql_num_rows($sql) != 1 ){
            $cnx->close();
            return;
        }
        
        $r = @mysql_fetch_assoc($sql);
        @mysql_free_result($sql);
        
        $this->car = array(
            'maker' => $r['maker'],
            'model' => $r['model'],
            'body_type' => $r['body_type'],
            'price' => $r['price'],
            'year' => $r['year'],
            'miles' => $r['miles'],
            'measure_type' => $r['measure_type'],
            'doors' => $r['doors']
        );
        
        $cnx->close();
        return;
    }
    
    /**
     * Public method: get body type by id
     * @return string
     */
    public function getBodyTypeByID($id){
        if( count($this->body_type_list) < 1 ){
            return $this->lang['CAR_DETAIL_BODY_TYPE_UNKNOWN'];
        }
        
        $result = '';
        
        foreach($this->body_type_list as $k => $v ){
            if( $k == $id ){
                $result = $v;
                break;
            }
        }
        
        return $result;
    }
    
    /**
     * Public method: get reservation policy terms
     * <br>---------------------------------------------------------------------
     * @return string
     */
    public function getReservationPolicy(){
        return $this->reservation_policy;
    }

    /**
     * Public method: get car information
     * <br>---------------------------------------------------------------------
     * @return array
     */
    public function getCar(){
        return $this->car;
    }
    
    /**
     * Public method: get form status
     * <br>---------------------------------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }

    /**
     * Public method: get form message 
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /**
     * Public method: get form style
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }    
}
