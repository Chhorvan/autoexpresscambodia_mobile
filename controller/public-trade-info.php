<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
include('view/3rdparty/pagination/pagination.php');
class publicTradeInfo
{
    protected $lang;
    protected $group = 'public';
    protected $slug = 'trade-info';
    protected $form_status;
    protected $form_message;
    protected $form_style;
    Protected $trad_list;

    protected $total_num_row;
    protected $total_page;
    protected $current_page;
    var $pagination_html;
    /*
     * Constructor -------------------------------------------------------------
     */
    public function publicTradeInfo($lang=array())
    {
        $this->lang = $lang;
        $this->get_tradlist(); 
    }

    public function gettotal()
    {
        return $this->total_num_row;
    }

    public function gettotalpage()
    {
        return $this->total_page;
    }

    public function getcurrentpage()
    {
        return $this->current_page;
    }

    public function get_tradlist()
    {       require_once BASE_ROOT . 'core/class-connect.php';

        $cnx = new Connect();
        $cnx->open();
        if(isset($_GET['submit'])){

            $category="";
            $searchoption="";
            $text="";

            if(isset($_GET['category']))
            {
                $category=$_GET['category'];
                
            }
            if(isset($_GET['searchoption']))
            {
                $searchoption=$_GET['searchoption'];
               
            }
            if(isset($_GET['text']))
            {
                $text=$_GET['text'];
            }
            $query="";
            if($category=="All" and $searchoption=="" and $text=="" )
            {
                $query="SELECT tr.*,r.`name` FROM trade_info tr 
                LEFT JOIN register r ON tr.`writer`=r.`id` 
                ORDER BY 1 DESC ";
            } 
            elseif($category!="All" and $searchoption=="" and $text=="")
            {
                    $query="SELECT tr.*,r.`name` FROM trade_info tr 
                    LEFT JOIN register r ON tr.`writer`=r.`id` 
                    WHERE `category`='$category'
                    ORDER BY 1 DESC ";
            }
            elseif($category=="All" and $searchoption!="" )
            {
               if($searchoption=='topic')
               {
                    $query="SELECT tr.*,r.`name` FROM trade_info tr 
                    LEFT JOIN register r ON tr.`writer`=r.`id` 
                    WHERE `topic`='$text'
                    ORDER BY 1 DESC ";
               }
               elseif($searchoption=='writer')
               {
                    $query="SELECT tr.*,r.`name` FROM trade_info tr 
                    LEFT JOIN register r ON tr.`writer`=r.`id` 
                    WHERE r.`name`='$text'
                    ORDER BY 1 DESC ";
               }
            }
            elseif($category=="All" and $searchoption=="")
            {
                    $query="SELECT tr.*,r.`name` FROM trade_info tr 
                    LEFT JOIN register r ON tr.`writer`=r.`id` 
                    WHERE r.`name`='$text' OR `topic`='$text'
                    ORDER BY 1 DESC ";
            }
            elseif($category!="All" and $searchoption=="")
            {
                    $query="SELECT tr.*,r.`name` FROM trade_info tr 
                    LEFT JOIN register r ON tr.`writer`=r.`id` 
                    WHERE `category`='$category' and (`topic`='$text' 
                    OR r.`name`='$text')
                    ORDER BY 1 DESC ";
            }
            else 
            {
               if($searchoption=='topic')
               {
                    $query="SELECT tr.*,r.`name` FROM trade_info tr 
                    LEFT JOIN register r ON tr.`writer`=r.`id` 
                    WHERE `category`='$category' and `topic`='$text'
                    ORDER BY 1 DESC ";
               }
               elseif($searchoption=='writer')
               {
                    $query="SELECT tr.*,r.`name` FROM trade_info tr 
                    LEFT JOIN register r ON tr.`writer`=r.`id` 
                    WHERE `category`='$category' and r.`name`='$text'
                    ORDER BY 1 DESC ";
               }
            }
       
            
            $sql_count_str= $query;
            $sql_count = @mysql_query($sql_count_str);
            $this->total_num_row= @mysql_num_rows($sql_count);
            
            $links = new Pagination ($this->total_num_row,20);
            $limit=$links->start_display;
            $this->pagination_html= $links->display(); 
            $this->total_page=$links->numPages();
            $this->current_page=$links->currentPage();
            ///////PAGINATION PROCESS///////
            //$sql_str=$sql_count_str.$limit;  
            $query.=$limit;
             
             
            if( !$sql =@mysql_query($query.";") )
            {
                $cnx->close();

                require_once BASE_ROOT . 'core/class-log.php';

                LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

                $this->form_message = 'Unable to load page content due an internal error.';
                $this->form_status = true;
                $this->form_style = 'alert-error';

                return;
            }
 
        }
        elseif(isset($_GET['mywrite'])  )
        {
            if(isset($_SESSION['log_id'])){
                $id=$_SESSION['log_id'];
                $query="SELECT tr.*,r.`name` FROM trade_info tr 
                LEFT JOIN register r ON tr.`writer`=r.`id` 
                WHERE `writer`='$id'
                ORDER BY 1 DESC ";

                $sql_count_str= $query;
                $sql_count = @mysql_query($sql_count_str);
                $this->total_num_row= @mysql_num_rows($sql_count);
                
                $links = new Pagination ($this->total_num_row,20);
                $limit=$links->start_display;
                $this->pagination_html= $links->display(); 
                $this->total_page=$links->numPages();
                $this->current_page=$links->currentPage();
                ///////PAGINATION PROCESS///////
                //$sql_str=$sql_count_str.$limit;  
                $query.=$limit; 

                    if( !$sql = @mysql_query($query.";" ) )
                    {
                        $cnx->close();

                        require_once BASE_ROOT . 'core/class-log.php';

                        LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

                        $this->form_message = 'Unable to load page content due an internal error.';
                        $this->form_status = true;
                        $this->form_style = 'alert-error';

                        return;
                    }
            }
            else{
                header('Location:'.BASE_RELATIVE.'login/');
            }
             
        }
        else
        {
 
            /////PAGINATION PROCESS//////
        $query="SELECT tr.*,r.`name` FROM trade_info tr LEFT JOIN register r ON tr.`writer`=r.`id` ORDER BY 1 DESC ";

        $sql_count_str= $query;
        $sql_count = @mysql_query($sql_count_str);
        $this->total_num_row= @mysql_num_rows($sql_count);
        
        $links = new Pagination ($this->total_num_row,20);
        $limit=$links->start_display;
        $this->pagination_html= $links->display(); 
        $this->total_page=$links->numPages();
        $this->current_page=$links->currentPage();
        ///////PAGINATION PROCESS///////
        //$sql_str=$sql_count_str.$limit;  
        $query.=$limit;
         

            if( !$sql = @mysql_query($query.";" ) )
            {
                $cnx->close();

                require_once BASE_ROOT . 'core/class-log.php';

                LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

                $this->form_message = 'Unable to load page content due an internal error.';
                $this->form_status = true;
                $this->form_style = 'alert-error';

                return;
            }
             
             
        }

        $this->trad_list = array();
        while( $r = @mysql_fetch_assoc($sql) ){

                array_push($this->trad_list, $r);
         
            }
        //var_dump($this->trad_list);
        @mysql_free_result($rows_count);
        @mysql_free_result($sql);
        return $this->trad_list;


    }



    /*
     * Public method: get page content data ------------------------------------
     * @return array | false.
     */
    public function getPageHTML()
    {
        require_once BASE_ROOT . 'core/class-connect.php';

        $cnx = new Connect();
        $cnx->open();

        if( !$sql = @mysql_query("SELECT * FROM `page` WHERE `page_slug`='$this->slug' AND `page_group`='$this->group' LIMIT 1;") )
        {
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }

        if( @mysql_num_rows($sql) != 1 )
        {
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }

        $r = @mysql_fetch_assoc($sql);

        $result                     = array();
        $result['id']               = $r['id'];
        $result['meta_keywords']    = $r['meta_keywords'];
        $result['meta_description'] = $r['meta_description'];
        $result['meta_robots']      = $r['meta_robots'];
        $result['page_title']       = $r['page_title'];
        $result['page_slug']        = $r['page_slug'];
        $result['page_publish']     = $r['page_publish'];
        $result['page_author']      = $r['page_author'];
        $result['creation']         = $r['creation'];
        $result['editable']         = $r['editable'];
        $result['page_group']       = $r['page_group'];
        $result['html']             = $r['html'];

        @mysql_free_result($sql);
        $cnx->close();

        return $result;
    }
	

    /*
     * Public method: get form status ------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }

    /*
     * Public method: get form message -----------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /*
     * Public method: get form style -------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }

}
?>
