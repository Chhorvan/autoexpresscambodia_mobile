<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
class usercarmanagereditmedia {
    private $lang;
    private $form_message;
    private $form_status;
    private $form_style;
    private $car_id;
    private $media_list;
    private $car;
    
    /**
     * Constructor
     * <br>---------------------------------------------------------------------
     */
    public function usercarmanagereditmedia($lang=array()){
        $this->lang = $lang;
        
        // handle upload video.
        if( isset($_POST['vidbtn']) ){
            $this->handleVideoForm();
        }
        
        // handle upload image.
        if( isset($_POST['imgbtn']) ){
            $this->handleImageForm();
        }
        
        // validate if car id has been declared and load list.
        $this->car_id = trim($_GET['cid']);
        
        if( empty($this->car_id) ){
            $this->media_list = array();
            $this->form_message = $this->lang['MEDIA_MANAGER_INVALID_CARID_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-error';
            return;
        }
        else {
            $this->loadList();
        }
    }
    
    /**
     * Private method: save new image
     * <br>---------------------------------------------------------------------
     * @return void
     */
    private function handleImageForm(){
        $carid = trim($_POST['carIdInput']);
        $type = trim($_POST['typeInput']);
        $ext = $_FILES['fileInput']['name'];
        $author = (int)$_SESSION['log_id'];
        // validate.
        if( empty($carid) || empty($type) ){
            return false;
        }
        
        $bytes = $_FILES['fileInput']['size'];
        $max = $this->return_bytes(ini_get('post_max_size'));
        
        if( $bytes < 1 || $bytes > $max ){
            $this->form_message = $this->lang['MEDIA_MANAGER_UPLOAD_MAX_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-error';
            return;
        }
        
        switch($type){
            case 'interior':
            case 'exterior':
                break;
            default:
                $type = 'interior';
        }
        
        // get file extension.
        $ext = explode('.',$ext);
        $ext = $ext[(count($ext)-1)];
        $ext = strtolower($ext);
        
        switch ($ext){
            case 'jpg':
            case 'png':
            case 'gif':
                break;
            default:
                $this->form_message = $this->lang['MEDIA_MANAGER_INVALID_FILETYPE'];
                $this->form_status = true;
                $this->form_style = 'alert-error';
                return;
        }
        
        // generate new file name.
        $fname = date('Ymd') . '_' . date('His') . '_' . mt_rand('0000000', '9999999') . '.' . $ext;
        
        // define upload path.
        $path = BASE_ROOT . 'image/upload/cars/' . $fname;
        
        if( !@move_uploaded_file($_FILES['fileInput']['tmp_name'], $path) ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to move uploaded file at ' . __FILE__ . ':' . __LINE__);
            $this->form_message = $this->lang['MEDIA_MANAGER_MOVEFILE_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        
        // resize uploaded image.
        require_once BASE_CLASS . 'class-utilities.php';
        
        if( !Utilities::resizeImageWithFixSize($path, 400, 300) ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to move uploaded file at ' . __FILE__ . ':' . __LINE__);
            $this->form_message = $this->lang['MEDIA_MANAGER_MOVEFILE_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            @unlink($path);
            return;
        }
                
        // save to database.
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        $carid = @mysql_real_escape_string($carid);
        $url = 'image/upload/cars/' . $fname;
        
        if( !@mysql_query("INSERT INTO `car_media` (
                                                    `car_id`,
													`author`,
                                                    `type`,
                                                    `mode`,
                                                    `source`
                                                    ) VALUES (
                                                    '$carid',
													$author,
                                                    'image',
                                                    '$type',
                                                    '$url'
                                                    )") ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to save uploaded file due a query error at ' . __FILE__ . ':' . __LINE__);
            $cnx->close();
            $this->form_message = $this->lang['MEDIA_MANAGER_MOVEFILE_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            @unlink($path);
            return;
        }
        
        $this->form_message = $this->lang['MEDIA_MANAGER_UPLOAD_SUCCESS'];
        $this->form_status = true;
        $this->form_style = 'alert-success';
        $cnx->close();
    }
    
    /**
     * Private method: returns the max. upload size allowed by the server.
     * <br>---------------------------------------------------------------------
     * @return int Bytes
     */
    private function return_bytes($val) {
        $val = trim($val);
        $last = strtolower($val[strlen($val)-1]);
        switch($last) {
            // The 'G' modifier is available since PHP 5.1.0
            case 'g':
                $val *= 1024;
            case 'm':
                $val *= 1024;
            case 'k':
                $val *= 1024;
        }

        return $val;
    }
    
    /**
     * Private method: save new video url
     * <br>---------------------------------------------------------------------
     * @return void
     */
    private function handleVideoForm(){
        $carid = trim($_POST['carIdInput']);
        $url = trim($_POST['videoInput']);
        $author = (int)$_SESSION['log_id'];
        // validate.
        if( empty($carid) || empty($url) ){
            $this->form_message = $this->lang['MEDIA_MANAGER_VIDEO_URL_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-error';
            return;
        }
        
        // save url.
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        $carid = @mysql_real_escape_string($carid);
        $url = @mysql_real_escape_string($url);
        
        if( !@mysql_query("INSERT INTO `car_media` (
                                                    `car_id`,
													`author`,
                                                    `type`,
                                                    `mode`,
                                                    `source`
                                                    ) VALUES (
                                                    '$carid',
													$author,
                                                    'video',
                                                    'video',
                                                    '$url'
                                                    )") ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to save car media due a query error at ' . __FILE__ . ':' . __LINE__);
            $cnx->close();
            $this->form_message = $this->lang['MEDIA_MANAGER_VIDEO_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        
        $cnx->close();
        $this->form_message = $this->lang['MEDIA_MANAGER_VIDEO_QUERY_SUCCESS'];
        $this->form_status = true;
        $this->form_style = 'alert-success';
        return;
    }
    
    /**
     * Private method: load car media list
     * <br>---------------------------------------------------------------------
     * @return array
     */
    private function loadList(){
        require_once BASE_CLASS . 'class-connect.php';
        $author = (int)$_SESSION['log_id'];
        $this->media_list = array();
        
        $cnx = new Connect();
        $cnx->open();
        
        $this->car_id = @mysql_real_escape_string($this->car_id);
        
        if( !$sql = @mysql_query("SELECT * FROM `car_media` WHERE `car_id`='$this->car_id' AND `author`=$author ORDER BY `mode` DESC") ){
            $cnx->close();
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load car media list due a query error at ' . __FILE__ . ':' . __LINE__);
            $this->form_message = $this->lang['MEDIA_MANAGER_LOAD_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        
        if( @mysql_num_rows($sql) < 1 ){
            $cnx->close();
            return;
        }
        
        while( $r = @mysql_fetch_assoc($sql) ){
            $obj = array(
                'id' => $r['id'],
                'car_id' => $r['car_id'],
                'type' => $r['type'],
                'mode' => $r['mode'],
                'source' => $r['source']
            );
            array_push($this->media_list,$obj);
        }
        
        @mysql_free_result($sql);
        
        // get vehicle maker, model and year
        if( !$sql = @mysql_query("SELECT `id`,`maker`,`model`,`year` FROM `car` WHERE `id`='$this->car_id' LIMIT 1;") ){
            $cnx->close();
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load car maker due a query error at ' . __FILE__ . ':' . __LINE__);
            $this->form_message = $this->lang['MEDIA_MANAGER_LOAD_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            $this->media_list = array();
            return;
        }
        
        if( @mysql_num_rows($sql) != 1 ){
            $cnx->close();
            $this->media_list = array();
            return;
        }
        
        $r = @mysql_fetch_assoc($sql);
        @mysql_free_result($sql);
        
        $this->car = array(
            'maker' => stripslashes($r['maker']),
            'model' => stripslashes($r['model']),
            'year' => $r['year']
        );
        
        $cnx->close();
        return;
    }
    
    /**
     * Public method: get vehicle maker
     * <br>---------------------------------------------------------------------
     * @return array
     */
    public function getVehicleMaker(){
        return $this->car;
    }
    
    /**
     * Public method: get media list
     * <br>---------------------------------------------------------------------
     * @return array
     */
    public function getMediaList(){
        return $this->media_list;
    }
    
    /**
     * Public method: get form status
     * <br>---------------------------------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }

    /**
     * Public method: get form message 
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /**
     * Public method: get form style
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }
}
