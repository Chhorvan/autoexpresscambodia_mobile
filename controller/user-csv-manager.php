<?php
if(!isset($_SESSION)) @session_start();
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################

class usercsvmanager {
    protected $lang;
    protected $form_status;
    protected $form_message;
    protected $form_style;
    protected $maker_list;
    protected $model_list;
    protected $body_list;
    //protected $feature_icon;
	protected $country_list;
    protected $car_id;
	protected $drive_list;
	protected $vehicle_type_list;
	protected $car_maker_list;
	protected $fuel_type_list;
	protected $transmission_list;
	protected $category_list;
	protected $product_option;
	protected $product_type='Car';
	var $invalidProduct=false;
    /**
     * Constructor
     * <br>---------------------------------------------------------------------
     */
    public function usercsvmanager($lang=array()){
        $this->lang = $lang;
        
        // define car id.
        require_once BASE_CLASS . 'class-utilities.php';
        $this->car_id = Utilities::generateRandomString(21);
         
        if( isset($_POST['savebtn']) ){
            $this->handleSaveForm();
        }
    }
 
    private function escapeMe(&$val) {                //& in $val
	    $val  = mysql_real_escape_string(stripslashes($val));
	}
    /**
     * Private method: handle save form
     * <br>---------------------------------------------------------------------
     * @return void.
     */
    private function handleSaveForm(){ 
    	require_once BASE_CLASS . 'class-csv.php'; 
    	require_once BASE_CLASS . 'class-connect.php';
        require_once BASE_CLASS . 'class-utilities.php';
        //$this->car_id = Utilities::generateRandomString(21);
         
        $cnx = new Connect();
        $cnx->open();

        $csv = new parseCSV('car.csv');
		$products=$csv->data;

		$validProductTypes=array('Car', 'Truck', 'Bus', 'Equipment', 'Part', 'Aircraft', 'Ship', 'Motorbike');
		$validField['Car']=array('condition', 'vehicle_type', 'make', 'model', 'class', 'chassis_no', 'manufacturer_date', 'model_year', 'mileage', 'mileage_type', 'engine_volume', 'steering', 'transmission', 'fuel_type', 'location', 'price', 'number_passenger', 'door', 'exterior_color', 'engine_power', 'number_cylinder', 'drive_availability', 'drive_type', 'payment_terms', 'safety_device_options', 'exterior_options', 'interior_options', 'seller_comment');

		if(count($products)>=2){
			if(isset($_POST['productTypeInputSelect'])) $productTypeInputSelect=$_POST['productTypeInputSelect']; else $productTypeInputSelect='';
			$productTypeInputSelect=mysql_real_escape_string(stripslashes($productTypeInputSelect));

			if(!$this->validateInput($productTypeInputSelect, $validProductTypes)){
				return;
			}
			//$validSteering=array('LHD', 'RHD');
			
			$field_keys=array_keys($products[0]);
			//serialize to array key
			array_walk_recursive($field_keys, array($this, 'escapeMe'));
			//Validate field of product type if it is valid
			foreach($field_keys as $field_key){
				if(!in_array($field_key, $validField[$productTypeInputSelect])){
					echo "Invalid Field: ". $field_key."<br/>";
					return;
				}
			}
			//join field name from csv
			
			$validField[$productTypeInputSelect][]="product_type";
            
			//$field_names=implode('`, `', $validField[$productTypeInputSelect]);
			$field_names="(`id`";
			foreach($validField[$productTypeInputSelect] as $field){
				$field_names.=", `$field`";
			}
			$field_names.=")";
			
			foreach($products as $row){

				//serialize to array value
				$row['product_type']=$productTypeInputSelect;
				$row['id']=Utilities::generateRandomString(21);
				array_walk_recursive($row, array($this, 'escapeMe'));
				$mix_tmp="('{$row['id']}'";
				foreach($validField[$productTypeInputSelect] as $field){
					$mix_tmp.=", '{$row[$field]}'";
				}
				$mix_tmp.=")";
				$value_rows[]=$mix_tmp;
			}

			$field_values=implode(', ', $value_rows);

			$sql="INSERT INTO product $field_names VALUES $field_values";
			
			//If import success it outputs 'Success'
			if(mysql_query($sql)) echo "Success";

			//echo mysql_error();
			//echo $sql;
		}
		@mysql_free_result($sql);
        $cnx->close();
        return;
 	}
 	private function validateInput($input, $validarrays){
 		if(in_array($input, $validarrays)) return true;
 	}
 	
	private function validateCountry($cc){
	 	$sql="SELECT * FROM country_list WHERE cc='$cc'";
	 	$result=mysql_query($sql);
	 	if(!$result){
	 		return false;
	 	}
	 	if(mysql_num_rows($result)>0){
	 		return true;
	 	}

	}
    
    /**
     * Private method: remove current vehicle's media
     * <br>---------------------------------------------------------------------
     * @return void
     */
    private function removeMedia($carid){
        if( empty($carid) ){
            return;
        }
        $author = (int)$_SESSION['log_id'];
        require_once BASE_CLASS . 'class-connect.php';
        $cnx = new Connect();
        $cnx->open();
        
        if( $sql = @mysql_query("SELECT `car_id`,`source` FROM `car_media` WHERE `car_id`='$carid' AND `author`=$author") ){
            if( @mysql_num_rows($sql) > 0 ){
                while( $r=@mysql_fetch_assoc($sql) ){
                    @unlink(BASE_ROOT . $r['source']);
                }
                @mysql_free_result($sql);
            }
        }
        
        @mysql_query("DELETE FROM `car_media` WHERE `car_id`='$carid' AND `author`=$author");
        
        $cnx->close();
    }
    
    
     /**
     * Private method: load option list
     * @return array
     */
	private function existedProduct($cid, $owner){
		
		require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        if( !$sql = @mysql_query("SELECT * FROM `product` WHERE `id`='$cid' AND `owner`='$owner';") ){
            require_once BASE_CLASS . 'class-log.php'; 
            $cnx->close();
            return false;
        }
        
        if( @mysql_num_rows($sql) >0  ){
            return true;
        }

        @mysql_free_result($sql);
        
      	
	}
    
    
    /**
     * Public method: get car id
     * <br>---------------------------------------------------------------------
     * @return string The generated car id
     */
    public function getCarId(){
        return $this->car_id;
    }
	//get all checkbox data
	
	
    /**
     * Public method: get form status
     * <br>---------------------------------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }

    /**
     * Public method: get form message 
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /**
     * Public method: get form style
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }
}