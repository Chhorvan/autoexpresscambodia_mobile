<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
class adminaddnews {
    protected $lang;
    protected $form_status;
    protected $form_message;
    protected $form_style;
    
    /**
     * Constructor
     * <br>---------------------------------------------------------------------
     */
    public function adminaddnews($lang=array()){
        $this->lang = $lang;
        
        // handle news form.
        if( isset($_POST['savebtn']) ){
            $this->handleForm();
        }
    }
    
    /**
     * Private method: handle save form
     * <br>---------------------------------------------------------------------
     * @return void
     */
    private function handleForm(){
        $title = trim($_POST['titleInput']);
        $body  = trim($_POST['bodyInput']);
        $productType=trim($_POST['productTypeSelect']);
		$publish = $_POST['publish'];

        $title = stripslashes($title);
        $body  = stripslashes($body);
        $productType  = stripslashes($productType);

        $_SESSION['news_title'] = $title;
        $_SESSION['news_body'] = $body;
        
        if( empty($title) || empty($body) ){
            $this->form_message = $this->lang['ADDNEWS_FORM_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-error';
            return;
        }
        
        // save news.
        unset($_SESSION['news_title']);
        unset($_SESSION['news_body']);
        
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        $title = @mysql_real_escape_string($title);
        $body = @mysql_real_escape_string($body);
        $productType = @mysql_real_escape_string($productType);
        
        $date = date('Y-m-d');
        $time = date('H:i:s');
        $author = $_SESSION['log_name'];
        
        if(empty($productType)||empty($title)||empty($body)){
            require_once BASE_CLASS . 'class-log.php';
            
            LogReport::write('Unable to save news due a query error at ' . __FILE__ . ':' . __LINE__);
            
            $cnx->close();
            $this->form_message = $this->lang['ADDNEWS_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }

        if( !@mysql_query("INSERT INTO `news` (
                                                `title`,
                                                `product_type`,
                                                `html`,
                                                `date`,
                                                `time`,
                                                `author`,
												publish
                                              ) VALUES (
                                                '$title',
                                                '$productType',
                                                '$body',
                                                '$date',
                                                '$time',
                                                '$author',
												$publish
                                              )") ){
            require_once BASE_CLASS . 'class-log.php';
            
            LogReport::write('Unable to save news due a query error at ' . __FILE__ . ':' . __LINE__);
            
            $cnx->close();
            $this->form_message = $this->lang['ADDNEWS_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        
        $cnx->close();
        $this->form_message = $this->lang['ADDNEWS_QUERY_SUCCESS'];
        $this->form_status = true;
        $this->form_style = 'alert-success';
        return;
    }
    
    /**
     * Public method: get form status
     * <br>---------------------------------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }

    /**
     * Public method: get form message 
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /**
     * Public method: get form style
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }
}