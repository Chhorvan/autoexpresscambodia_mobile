<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
include('view/3rdparty/pagination/pagination.php');

class adminMakeManager
{
    protected $lang;
    protected $group = 'admin';
    protected $slug = 'car';
    protected $form_status;
    protected $form_message;
    protected $form_style;
    protected $members;
    var $total_num_row;
    var $pagination_html;
    var $current_page;
    var $total_page;

    var $member_type1="Seller";
    /**
     * Constructor
     * <br>---------------------------------------------------------------------
     */
    public function adminmakemanager($lang=array()){
        $this->lang = $lang;
        //if user submit press button save changes
        if(isset($_POST['save_changes'])){
            $this->handleSaveChanges();
        }
        //if user submit press button delete selected
        if(isset($_POST['delete_selected'])){
            $this->handleDelete();
        }
        if(isset($_POST['submitSave'])){
            $this->handleSaveNew();
        }
        
        //$this->loadCityList();
        
    }
    private function handleSaveNew(){
        require_once BASE_CLASS . 'class-connect.php';
        $cnx = new Connect();
        $cnx->open();

        if(isset($_POST['newMakeInput'])) $newMakeInput=$_POST['newMakeInput']; else $newMakeInput='';
        if(isset($_POST['product_type'])) $product_type=$_POST['product_type']; else $product_type='';
        
        $sql= "INSERT INTO `product_maker` (`maker`, `product_type`) VALUES('$newMakeInput', '$product_type');";

        if(empty($newMakeInput)){
            
            $cnx->close();
            $this->form_message = "Unable to save empty make! Please insert it in the textbox.";
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            
            return;
        }
        if( !@mysql_query($sql) ){
            require_once BASE_CLASS . 'class-log.php';
            $cnx->close();
            
            LogReport::write('Unable to add make due to a query error at ' . __FILE__ . ':' . __LINE__);
            $this->form_message = $this->lang['PAGE_SAVE_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            
            return;
        }
        $cnx->close();
        
        $this->form_message =  "Makes were added successfully!";
        $this->form_status = true;
        $this->form_style = 'alert-success';
    }

    private function handleSaveChanges(){
        if(isset($_POST['old_make'])) $old_makes=$_POST['old_make']; else $old_makes=array();
        if(isset($_POST['new_make'])) $new_makes=$_POST['new_make']; else $new_makes=array();
        if(isset($_POST['product_type'])) $product_type=$_POST['product_type']; else $product_type='';
        //Delete existing make that match new makes first to prevent duplicate
        //$this->deletePreviousMake($new_makes, $product_type);
        if(count($new_makes)<=0){
            $this->form_message =  "Makes were changed successfully!";
            $this->form_status = true;
            $this->form_style = 'alert-success';
            return;
        }

        $this->changeMakeInProduct();
        require_once BASE_CLASS . 'class-connect.php';
        $cnx = new Connect();
        $cnx->open();
        $in_makes=array();
        $sql= "UPDATE `product_maker` SET `maker` = CASE `id` ";
        foreach($new_makes as $key=>$value){
            array_push($in_makes, $key);
            $sql.="WHEN '{$key}' THEN '{$value}' ";
        }
        //Sql of cities that were changed
        $in_old_makes = implode("', '", $in_makes);
        $sql .= "END ";
        //Change only cities that were edit
        $sql .= "WHERE id IN ('$in_old_makes') AND product_type='$product_type'";
        //echo $sql;
        if( !@mysql_query($sql) ){
            require_once BASE_CLASS . 'class-log.php';
            //echo mysql_error();
            $cnx->close();
            
            LogReport::write('Unable to change make due to a query error at ' . __FILE__ . ':' . __LINE__);
            $this->form_message = "Unable to save changes! Please make sure that you enter correct data.";
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            
            return;
        }
        $cnx->close();
        
        $this->form_message =  "Makes were changed successfully!";
        $this->form_status = true;
        $this->form_style = 'alert-success';
    }
    private function changeMakeInProduct(){
        if(isset($_POST['old_make'])) $old_makes=$_POST['old_make']; else $old_makes=array();
        if(isset($_POST['new_make'])) $new_makes=$_POST['new_make']; else $new_makes=array();
        if(isset($_POST['product_type'])) $product_type=$_POST['product_type']; else $product_type='';
        //Delete existing make that match new makes first to prevent duplicate
        //$this->deletePreviousMake($new_makes, $product_type);
        
        require_once BASE_CLASS . 'class-connect.php';
        $cnx = new Connect();
        $cnx->open();

        
        $in_makes=array();
        $sql= "UPDATE `product` SET `make` = CASE `make` ";
        foreach($new_makes as $key=>$value){
            array_push($in_makes, $old_makes[$key]);
            $sql.="WHEN '{$old_makes[$key]}' THEN '{$value}' ";
        }
        //Sql of cities that were changed
        $in_old_makes = implode("', '", $in_makes);
        $sql .= "END ";
        //Change only cities that were edit
        $sql .= "WHERE make IN ('$in_old_makes') AND product_type='$product_type'";
        //echo $sql;
        if( !@mysql_query($sql) ){
            require_once BASE_CLASS . 'class-log.php';
            //echo mysql_error();
            $cnx->close();
            
            LogReport::write('Unable to change make due to a query error at ' . __FILE__ . ':' . __LINE__);
            $this->form_message = "Unable to save changes! Please make sure that you enter correct data.";
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            
            return;
        }
        $cnx->close();
        
        $this->form_message =  "Makes were changed successfully!";
        $this->form_status = true;
        $this->form_style = 'alert-success';
    }
    private function handleDelete(){
        require_once BASE_CLASS . 'class-connect.php';
        $cnx = new Connect();
        $cnx->open();
        //code here
        if(isset($_POST['makeCheck'])) $makeCheck=$_POST['makeCheck']; else $makeCheck=array();
        if(isset($_POST['product_type'])) $product_type=$_POST['product_type']; else $product_type='';
        if(count($makeCheck)<=0){
            $this->form_message = "Please check the cities to be deleted!";
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        $checked_makes = implode("', '", array_keys($makeCheck));
        $sql="DELETE FROM product_maker WHERE id IN ('$checked_makes') AND product_type='$product_type'";
        //execute delete
        if( !@mysql_query($sql) ){
            require_once BASE_CLASS . 'class-log.php';
            $cnx->close();
            LogReport::write('Unable to delete makes in this category due to a query error at ' . __FILE__ . ':' . __LINE__);
            $this->form_message = 'Unable to delete makes with this category';
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        $cnx->close();
        $this->deleteFromModelTable();
        $this->form_message =  "Makes with the selected category were deleted successfully!";
        $this->form_status = true;
        $this->form_style = 'alert-success';
    }
    private function deleteFromModelTable(){
        require_once BASE_CLASS . 'class-connect.php';
        $cnx = new Connect();
        $cnx->open();
        //code here
        if(isset($_POST['makeCheck'])) $makeCheck=$_POST['makeCheck']; else $makeCheck=array();
        if(isset($_POST['product_type'])) $product_type=$_POST['product_type']; else $product_type='';
        if(count($makeCheck)<=0){
            return;
        }
        $checked_makes = implode("', '", array_keys($makeCheck));
        $sql="DELETE FROM product_model WHERE maker IN ('$checked_makes')";
        //execute delete
        //echo $sql;
        if( !@mysql_query($sql) ){
            require_once BASE_CLASS . 'class-log.php';
            $cnx->close();
            LogReport::write('Unable to delete makes in this category due to a query error at ' . __FILE__ . ':' . __LINE__);
            $this->form_message = 'Unable to delete makes in this category';
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        $cnx->close();
    }
    /*
     * Public method: get form status ------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }

    /*
     * Public method: get form message -----------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /*
     * Public method: get form style -------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }

}
?>
