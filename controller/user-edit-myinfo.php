<?php
if(!isset($_SESSION)) @session_start();

################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################

class userEditMyinfo
{
	protected $form_status;
    protected $form_message;
    protected $form_style;
    protected $lang;


    protected $register_form_status;    // tracks registration process (complete/error)
    protected $_LANG;
    protected $list_inuquliry;
    protected $list_inuquliryimg;
    protected $country_list;
	protected $img_path="";
	protected $thumb_path="";
	var $msg='';
	var $toolTip=array();
	
	//message for current password and confirm password.
	var $message_for_current_password;
	var $message_for_confirm_password;
	
	//load province
	var $province;
	
    /*
     * Constructor -------------------------------------------------------------
     */
    public function userEditMyinfo($lang=array())
    {
		require_once BASE_CLASS .'ImageManipulator.php';
		require_once BASE_CLASS . 'class-utilities.php';
        // if group session is set, take user to the home page.
        if( isset($_SESSION['log_group']) && $_SESSION['log_group'] != 'user' && $_SESSION['log_group'] != 'admin')
        {
            header("Location: " . BASE_RELATIVE . DEFAULT_HOME_SLUG);
            exit;
        }
        $_SESSION['error_extantion']='';
        //var_dump($_SESSION);
        $this->loadCountryList();

        $this->listinquiry();
        $this->listinquiryimg();
        // initialize status.
        if(isset($_GET['msg'])) $msg=htmlspecialchars($_GET['msg']); else $msg='';
        $this->msg=$msg;
        if($this->msg=='incomplete_seller'){
			$this->form_message = "Sorry, your info is not yet completed! Please check the required field to continue.";
		    $this->form_status = true;
		    $this->form_style = 'alert-warning';
		}
        // set language vars.
        $this->_LANG = $lang;
        $this->lang = $lang;
        $reg_id=  $_SESSION['log_id'];
        $userlog_id=  $_SESSION['userlog_id'];
		


        if( isset($_POST['savebtn']) ){

        	extract($_POST);

    		$primary_photo="";
    		$cid = trim($_POST['caridInput']);

    		// //Clear Primary Photo

   			for($j=0; $j<count($_FILES['fileInput']['name']); $j++){

		    	if(!isset($_FILES['fileInput']['name'][$j]) ){
		       		$form_error = true;
		       		$form_message = $_LANG['NEWCAR_INVALID_REQUEST_ID_LABEL'];
		   		}
   		 		else{

		        	// get file extension.
			        $ext =$_FILES['fileInput']['name'][$j];
			        $ext = explode('.', $ext);
			        $ext = $ext[(count($ext)-1)];
			        $ext = strtolower($ext);


			        switch($ext){
			            case 'jpg':
			            case 'gif':
			            case 'png':
			                $nogood = false;
			                break;
			            default:
			                $nogood = true;
			    	}

			        if($nogood==true){
			        	$_SESSION['error_extantion']='Invalid extension !';

			        	 $form_error = true;



			        }
			        else {
			            // upload file.
			            $path  = BASE_ROOT . 'upload/profile/';
			            $fname = date('Ymd_His') . '_' . mt_rand('0000000','9999999') . '.' . $ext;

						$preview_gallery_name="preview_gallery-".$fname;
						$special_offer_name="special_offer-".$fname;
						$list_car_name="list_car-".$fname;
			            $thumb_gallery_name="thumb_gallery-".$fname;

			            // upload failed.
			            if( $_FILES['fileInput']['size'][$j] < 1 ){
			                $form_error = true;
			                $form_message = $_LANG['NEWCAR_INVALID_IMAGE_SIZE_LABEL'];
							echo '<script>alert("'.$form_message.'");</script>';
							echo '<meta http-equiv="refresh" content="0" />';
			            }
			            else {
			            	// save to database.
			                require_once BASE_CLASS . 'class-connect.php';

			                $cnx = new Connect();
			                $cnx->open();
							
							

			                // upload the image.
			                if( !@move_uploaded_file($_FILES['fileInput']['tmp_name'][$j], $path.$fname) ){
			                    $form_error = true;
			                    $form_message = $_LANG['NEWCAR_UPLOAD_FAILURE_LABEL'];
			                    require_once BASE_CLASS . 'class-log.php';
			                    LogReport::write('Unable to upload image at ' . __FILE__ . ':' . __LINE__);
			                }
			                else {
								// *** 1) Initialise / load image
								// $list = getimagesize($path.$fname);

								 $main_image=new ImageManipulator($path.$fname);
								 // $preview_gallery = new resize($path.$fname);
							 	//$special_offer = new resize($path.$fname);
							   	//	$list_car = new resize($path.$fname);
					 		 	$thumb_gallery = new ImageManipulator($path.$fname);
								// *** 2) Resize image (options: exact, portrait, landscape, auto, crop)
								//$main_image->resize(305, 225);
								// $preview_gallery->resizeImage(100,100,'auto');
						       	// $special_offer->resizeImage(156, 90, 'exact');
							   	// $list_car->resizeImage(315, 225, 'crop');
							    //$thumb_gallery->resize(305, 225);
								// *** 3) Save image
								 $main_image->save($path.$fname);
								 // $preview_gallery->saveImage($path.$preview_gallery_name, 100);
								//$special_offer->saveImage($path.$special_offer_name, 100);
								//	$list_car->saveImage($path.$list_car_name, 100);
								//$thumb_gallery->save($path.$thumb_gallery_name);
								// move_uploaded_file($_FILES['fileInput']['tmp_name'][$j], $path.$fname);



			                    // check if thumb was created successfully.
								//Define path upload to database
								$image_path='upload/profile/';
								$protocol = strpos(strtolower($_SERVER['SERVER_PROTOCOL']),'https') === FALSE ? 'http://' : 'https://';
								$host = $_SERVER['HTTP_HOST'];
								$full_url=$protocol.$host.$_SERVER['REQUEST_URI'];
								//Condition for debug in local
								if($host=='localhost'){
									$start=strlen($protocol.$host);
									$end = strpos($full_url, '/', $start + 1);
									$length=$end-$start;
									$local_host=substr($full_url, $start +1, $length-1);
									$host="localhost/{$local_host}";
								}

								//Reset Primary photo and Set it Again
								//generate full destination
								$destination = $fname;
								$thumb=$image_path."thumb_gallery-".$fname;
								$user_id = $_SESSION['log_id'];



								// //Clear Primary Photo
								//  $sql_clear_primary="UPDATE more_image set primary_photo=0 where `owner`=".$user_id;
								//  mysql_query($sql_clear_primary);

							 $sql_select_primary="select * from more_image where `owner`=".$user_id;
							$result=mysql_query($sql_select_primary);
							$num=mysql_num_rows($result);
								if($num>0){
									$primary_photo=0;
								}else{
									if($j==0){
										$primary_photo=1;
									}

								}



								if( !@mysql_query("INSERT INTO more_image (`owner`, `m_thumb`, `m_image`,`primary_photo`) VALUES (
																	{$user_id},
																	'$thumb',
																	'$destination',
																	'$primary_photo'
																	)") ){
									require_once BASE_CLASS . 'class-log.php';
									LogReport::write('Unable to save image for car_media table due a query error at ' . __FILE__ . ':' . __LINE__);
									$form_error = true;
									// $form_message = $_LANG['NEWCAR_UPLOAD_QUERY_ERROR'];
									$cnx->close();
									@unlink($path.$fname);
								}
								else{
									$primary_photo=0;
									$last_id=mysql_insert_id();

									if($this->processRegisterForm2()){

										// header("Location: edit-myinfo#image_upload");
									}
								}
			                }
			            }
			             header("Location: edit-myinfo#image_upload");
			        }

    			}

        	}




    	}


      
		
		
		
		
		
		
		
		
		
		// if register form is submit, process it.
        if( isset($_POST['btnupdate']) )
        {
		    
			 // check password verify
				require_once BASE_CLASS . 'class-connect.php';

				$db = new Connect();
				$db->open();
				$sql = mysql_query("SELECT password FROM register WHERE user_id = '".$reg_id."'");
				$result=mysql_fetch_array($sql);
				
			    $verify_password=md5($_POST['vpassword']);
				$pass=md5($_POST['password']);
				$confirm_pass=md5($_POST['confirm_password']);
				
				//$current_pass=md5($_POST['cpassword']);
				
				if(($verify_password==$result['password']) and ($pass==$confirm_pass)){
				
					$this->processRegisterForm();
					
				}
				elseif(($verify_password==$result['password']) and ($pass!=$confirm_pass)){
				   echo"<script type='text/javascript'>";
				   echo"alert('Check New and Confirm password again')";
				   echo"</script>";
				
				}
				elseif(($verify_password==$result['password']) and ($pass=="") and ($confirm_pass=="")){
					$new_and_conf="";
					echo"Both are null";
				
				}
				else{
				   echo"<script type='text/javascript'>";
				   echo"alert('Can not verify')";
				   echo"</script>";
				   
				}
			
			
			
			
			
        }//end btnupdate.
		
		
    }
	
	//load province
	public function load_province(){
		require_once BASE_CLASS . 'class-connect.php';
		$db = new Connect();
        $db->open();
		$sql = @mysql_query("SELECT province_name FROM province");
		$this->province_list = array();

        while( $r = @mysql_fetch_assoc($sql) ){
            array_push($this->province_list, $r);
        }
        @mysql_free_result($sql);
		return $this->province_list;
        $cnx->close();	
	}
	
    private function existUserId($id){
    	require_once BASE_CLASS . 'class-connect.php';

        $cnx = new Connect();
        $cnx->open();
        $sql = "SELECT user_id FROM register WHERE log_id='$id'";
        $result=mysql_query($sql);
        if(mysql_num_rows($result)>0){
        	return true;
        }
        $cnx->close();
    }
    private function existEmail($email){
    	require_once BASE_CLASS . 'class-connect.php';

        $cnx = new Connect();
        $cnx->open();
        $sql = "SELECT email FROM register WHERE email='$email'";
        $result=mysql_query($sql);
        if(mysql_num_rows($result)>0){
        	return true;
        }
        $cnx->close();
    }
    public function getCountryList(){
        return $this->country_list;
    }

    private function loadCountryList(){
        require_once BASE_CLASS . 'class-connect.php';

        $cnx = new Connect();
        $cnx->open();

        if( !$sql = @mysql_query("SELECT * FROM `country_list` ORDER BY `country_name` ASC") ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load country list due a query error at ' . __FILE__ . ':' . __LINE__);

            $this->country_list = array();

            return;
        }

        if( @mysql_num_rows($sql) < 0 ){
            $cnx->close();
            return;
        }

        $this->country_list = array();

        while( $r = @mysql_fetch_assoc($sql) ){
            $obj['cc'][] = $r['cc'];
            $obj['country_name'][] = stripslashes($r['country_name']);
			$obj['phonecode'][]=$r['phonecode'];

            $this->country_list=$obj;
        }

        @mysql_free_result($sql);
        $cnx->close();
    }
    public function validatePassword($cpassword, $password){
    	$log_group=$_SESSION['log_group'];

    	if(strlen($cpassword) >= 4 && strlen($password) >= 4 && $log_group=='user'){
    		return true;
    	}elseif($log_group=='admin'){
    		return true;
    	}


    }
        public function listinquiry()
    {

        require_once BASE_ROOT . 'core/class-connect.php';

        $id=0;
        if(isset($_GET['id'])){
            $id=$_GET['id'];
        }
        if(isset($_GET['id'])) $getId=htmlspecialchars($_GET['id']); else $getId='';

		if(empty($getId)||$_SESSION['log_group']=='user'){
			$reg_id=  $_SESSION['log_id'];
		}elseif($_SESSION['log_group']=='admin'){
			$reg_id=  $getId;
		}
        //$reg_id=  $_SESSION['log_id'];


        $cnx = new Connect();
        $cnx->open();
        if( !$sql = @mysql_query("SELECT *
             FROM  `register` WHERE `user_id`='$reg_id';") )
        {
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }
         $this->list_inuquliry = array();
        while( $r = @mysql_fetch_assoc($sql) ){

            array_push($this->list_inuquliry, $r);

        }
        $cnx->close();
        //var_dump($this->list_inuquliry);
        return $this->list_inuquliry;


    }
         public function listinquiryimg()
    {

        require_once BASE_ROOT . 'core/class-connect.php';

        $id=0;
        if(isset($_GET['id'])){
            $id=$_GET['id'];
        }
        if(isset($_GET['id'])) $getId=htmlspecialchars($_GET['id']); else $getId='';

		if(empty($getId)||$_SESSION['log_group']=='user'){
			$reg_id=  $_SESSION['log_id'];
		}elseif($_SESSION['log_group']=='admin'){
			$reg_id=  $getId;
		}
        //$reg_id=  $_SESSION['log_id'];


        $cnx = new Connect();
        $cnx->open();
        if( !$sql = @mysql_query("SELECT *
             FROM  `more_image` WHERE `owner`='$reg_id' ORDER BY id asc ;") )
        {
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }
         $this->list_inuquliryimg = array();
        while( $r = @mysql_fetch_assoc($sql) ){

            array_push($this->list_inuquliryimg, $r);

        }
        $cnx->close();
        //var_dump($this->list_inuquliry);
        return $this->list_inuquliryimg;


    }

	/*  -----------------Company Information..------------------------*/




    /*
     * Process registration form -----------------------------------------------
     * @return void.
     */
     protected function processRegisterForm2()
    {
		// Initialize
		if(isset($_GET['id'])) $getId=htmlspecialchars($_GET['id']); else $getId='';

		if(empty($getId)||$_SESSION['log_group']=='user'){
			$reg_id=  $_SESSION['log_id'];
		}elseif($_SESSION['log_group']=='admin'){
			$reg_id=  $getId;
		}

		$userlog_id=  $_SESSION['userlog_id'];
		$myurl="";
		$member_type_1 = array();
		$member_type_1_string = "";
		$member_type_2 = array();
		$member_type_2_string = "";
		$business_type = array();
		$business_type_string = "";
		$business_field = array();
		$business_field_string = "";
		$id = "";
		$cpassword="";
		$chekpass="";
		$password = "";
		$confirm_password = "";
		$email = "";
		$email_error = "";
		$introduction = "";
		$msg = "";
		$company_name = "";
		$name = "";
		$card_id = "";
		$country = "";
		$reg_name= "";
		$address = "";
		$post_code = "";
		$tel = "";
		$fax = "";
		$phonecode = "";
		$website = "";
		$available_services = array();
		$available_services_string = "";
		//$term = "";

		extract($_POST);

		// set registration form session.
		// $_SESSION['reg_id'] = $id;
		$_SESSION['userlog_id']=$id;
		$_SESSION['reg_email'] = $email;
		//$_SESSION['reg_confirm_email'] = $confirm_email;
		$_SESSION['reg_company_name'] = $company_name;
        $_SESSION['reg_name'] = $name;
		$_SESSION['reg_card_id'] = $card_id;
		$_SESSION['reg_country'] = $country;
		$_SESSION['reg_address'] = $address;
		$_SESSION['reg_post_code'] = $post_code;
		$_SESSION['reg_tel'] = $tel;
		$_SESSION['reg_fax'] = $fax;
		$_SESSION['reg_name'] = $reg_name;
		$_SESSION['reg_mobile'] = $mobile;
		$_SESSION['reg_website'] = $website;

        // unset security session.
        unset($_SESSION['security_question_a']);
        unset($_SESSION['security_question_b']);


		require_once BASE_CLASS . 'class-utilities.php';


        // validate fields.
		/* checks if any member_type_1 is checked */
		if( isset($member_type_1) ) {
			/* Iterate the member_type_1 array and get the keys and values */
			foreach($member_type_1 as $key => $value) {
				if( isset($member_type_1[$key]) ) {
					$_SESSION['reg_member_type_1_'.$key] = $value;
				}
				$member_type_1_string.= $value.",";
			}

			/* unset member_type_1 session */
			if( empty($member_type_1[0]) ) { unset($_SESSION['reg_member_type_1_0']); }
			if( empty($member_type_1[1]) ) { unset($_SESSION['reg_member_type_1_1']); }
			if( empty($member_type_1[2]) ) { unset($_SESSION['reg_member_type_1_2']); }

			$member_type_1 = substr($member_type_1_string, 0, -1);
		}

		/* checks if any member_type_2 is checked */
		if( isset($member_type_2) ) {
			/* Iterate the member_type_2 array and get the keys and values */
			foreach($member_type_2 as $key => $value) {
				if( isset($member_type_2[$key]) ) {
					$_SESSION['reg_member_type_2_'.$key] = $value;
				}
				$member_type_2_string.= $value.",";
			}

			/* unset member_type_2 session */
			if( empty($member_type_2[0]) ) { unset($_SESSION['reg_member_type_2_0']); }
			if( empty($member_type_2[1]) ) { unset($_SESSION['reg_member_type_2_1']); }

			$member_type_2 = substr($member_type_2_string, 0, -1);
		}

		/* checks if any business_type is checked */
		if( isset($business_type) ) {
			/* Iterate the business_type array and get the keys and values */
			foreach($business_type as $key => $value) {
				if( isset($business_type[$key]) ) {
					$_SESSION['reg_business_type_'.$key] = $value;
				}
				$business_type_string.= $value.",";
			}

			/* unset business_type session */
			if( empty($business_type[0]) ) { unset($_SESSION['reg_business_type_0']); }
			if( empty($business_type[1]) ) { unset($_SESSION['reg_business_type_1']); }

			$business_type = substr($business_type_string, 0, -1);
		}

		/* checks if any business_field is checked */
		if( isset($business_field) ) {
			/* Iterate the business_field array and get the keys and values */
			foreach($business_field as $key => $value) {
				if( Utilities::Required($business_field[$key]) ) {
					$_SESSION['reg_business_field_'.$key] = $value;
				}
				$business_field_string.= $value.",";
			}

			/* unset business_field session */
			if( empty($business_field[0]) ) { unset($_SESSION['reg_business_field_0']); }
			if( empty($business_field[1]) ) { unset($_SESSION['reg_business_field_1']); }
			if( empty($business_field[2]) ) { unset($_SESSION['reg_business_field_2']); }
			if( empty($business_field[3]) ) { unset($_SESSION['reg_business_field_3']); }
			if( empty($business_field[4]) ) { unset($_SESSION['reg_business_field_4']); }
			if( empty($business_field[5]) ) { unset($_SESSION['reg_business_field_5']); }
			if( empty($business_field[6]) ) { unset($_SESSION['reg_business_field_6']); }
			if( empty($business_field[7]) ) { unset($_SESSION['reg_business_field_7']); }
			if( empty($business_field[8]) ) { unset($_SESSION['reg_business_field_8']); }
			if( empty($business_field[9]) ) { unset($_SESSION['reg_business_field_9']); }

			$business_field = substr($business_field_string, 0, -1);
		}

		/* checks if any available_services is checked */
		if( isset($available_services) ) {
			/* Iterate the available_services array and get the keys and values */
			foreach($available_services as $key => $value) {
				if( isset($available_services[$key]) ) {
					$_SESSION['reg_available_services_'.$key] = $value;
				}
				$available_services_string.= $value.",";
			}

			/* unset available_services session */
			if( empty($available_services[0]) ) { unset($_SESSION['reg_available_services_0']); }
			if( empty($available_services[1]) ) { unset($_SESSION['reg_available_services_1']); }
			if( empty($available_services[2]) ) { unset($_SESSION['reg_available_services_2']); }
			if( empty($available_services[3]) ) { unset($_SESSION['reg_available_services_3']); }
			if( empty($available_services[4]) ) { unset($_SESSION['reg_available_services_4']); }
			if( empty($available_services[5]) ) { unset($_SESSION['reg_available_services_5']); }
			if( empty($available_services[6]) ) { unset($_SESSION['reg_available_services_6']); }
			if( empty($available_services[7]) ) { unset($_SESSION['reg_available_services_7']); }

			$available_services = substr($available_services_string, 0, -1);
		}
		// $file = $_FILES['file1']['name'];
		// //var_dump($file);
		// //$cname=$_POST['company_name'];
		// //echo $file."";
		// $uploadedfile = $_FILES['file1']['tmp_name'];
		// Сheck that we have a file
		// if((!empty($_FILES["file1"])) && ($_FILES['file1']['error'] == 0)) {
		// 	// Check if the file is JPEG or PNG image and it's size is less than 350Kb
		// 	$ext = substr(strtolower($file), strrpos($file, '.') + 1);

		//     if ( $ext != "php" ) {
		// 		$cnx = new Connect();
	 //            $cnx->open();
		// 		$fname = date('Ymd_His') . '_' . mt_rand('0000000','9999999') . '.' . $ext;
		// 	    move_uploaded_file($_FILES['file1']['tmp_name'] ,BASE_ROOT."/upload/".$fname);
	 //            $my_info=$fname;
		// 		$thum_myinfo=$fname;
		// 		$resize_myinfo=new resize(BASE_ROOT."/upload/".$fname);
		//         $resize_thumb = new resize(BASE_ROOT."/upload/".$fname);
		// 	    $resize_myinfo->resizeImage(500, 400, 'auto');
		//         $resize_thumb->resizeImage(160, 120, 'auto');
		// 	    $resize_myinfo->saveImage(BASE_ROOT."/upload/myinfo/".$fname, 100);
		//         $resize_thumb->saveImage(BASE_ROOT."/upload/thumb/".$fname, 100);
		// 		if(file_exists(BASE_ROOT."/upload/".$fname)){
		// 			unlink(BASE_ROOT."/upload/".$fname);
		// 		}
		// 	    if(file_exists(BASE_ROOT."/upload/thumb/".$_SESSION['old_image'])){
		// 			unlink(BASE_ROOT."/upload/thumb/".$_SESSION['old_image']);
		// 		}
		// 	   if(file_exists(BASE_ROOT."/upload/myinfo/".$_SESSION['old_image'])){
		// 			unlink(BASE_ROOT."/upload/myinfo/".$_SESSION['old_image']);
		// 		}
		// 		$sql_update="UPDATE register SET image = '$fname' WHERE log_id = '".$_SESSION['userlog_id']."'";

		// 		mysql_query($sql_update);

	 //            $this->register_form_status = 'complete';
	 //            header( "refresh:0;url=".BASE_RELATIVE."my-info");
	 //           	$cnx->close();
	 //        }else{
		// 		$file = "";
		// 		$this->register_form_status = 'upload';
		// 	}
		// }
		// else
		// {
		// 	$file = "";
  //           //$this->register_form_status = 'upload';
		// }
		//validate user id
        // check if email is already registered.
        require_once BASE_CLASS . 'class-connect.php';

        $db = new Connect();
        $db->open();
        // filter values.
        $email = mysql_real_escape_string($email);
        $name = mysql_real_escape_string($name);
        $passMD5="";
        $password_value="";
        if(!empty($password)){
		    if ($cpassword==""&&$_SESSION['log_group']!='admin'){
				$passMD5=$chekpass;
			}else{
				$passMD5 = md5($password);
			}
        	$password_value="`password`='$passMD5', ";
		}
		// echo $chekpass."<br/>".$passMD5;
        // register the user account.
		mysql_query("UPDATE data_count set `country`='$country' WHERE `register_id`='$reg_id';");
        if($sql = @mysql_query("UPDATE  `register` SET `log_id`='$id',`email`='$email', 
        	`member_type1`='$member_type_1', `member_type2`='$member_type_2',        	
        	`company_name`='$company_name', `country`='$country',        	
        	`mobile`='$mobile', `web`='$website', `avalible_service`='$available_services',
        	`introduction`='$introduction', `address`='$address'
			WHERE `user_id`='$reg_id';") ) {

	            require_once BASE_CLASS . 'class-log.php';

	            LogReport::write('Unable to query database at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

	            $db->close();
	            $this->register_form_status = 'query';

				// header("Location: " .BASE_RELATIVE . "message-myinfo");

	            return;
        }
        else{
			if((!empty($_FILES["file1"])) && ($_FILES['file1']['error'] == 0)) {
				mysql_query("UPDATE register SET image = '$file' WHERE log_id = '$id'");
			}

			if((!empty($_FILES["file2"])) && ($_FILES['file2']['error'] == 0)) {
				$this ->uploadimage();
				$mysql_query="insert into more_image (m_image, m_thumb) values('".$this->img_path."', '".$this->thumb_path."')";
				//echo $mysql_query;
				mysql_query($mysql_query);
			}

            $db->close();
            $this->register_form_status = 'complete';
            return true;
            // header ( "refresh:0;url=".BASE_RELATIVE."my-info" );
        }

    }
    protected function processRegisterForm()
    {
		// Initialize
		if(isset($_GET['id'])) $getId=htmlspecialchars($_GET['id']); else $getId='';

		if(empty($getId)||$_SESSION['log_group']=='user'){
			$reg_id=  $_SESSION['log_id'];
		}elseif($_SESSION['log_group']=='admin'){
			$reg_id=  $getId;
		}

		$userlog_id=  $_SESSION['userlog_id'];
		$myurl="";
		$member_type_1 = array();
		$member_type_1_string = "";
		$member_type_2 = array();
		$member_type_2_string = "";
		$business_type = array();
		$business_type_string = "";
		$business_field = array();
		$business_field_string = "";
		$id = "";
		$cpassword="";
		$chekpass="";
		$password = "";
		$confirm_password = "";
		$email = "";
		$email_error = "";
		$introduction = "";
		$msg = "";
		$company_name = "";
		$name = "";
		$card_id = "";
		$country = "";
		$reg_name= "";
		$address = "";
		$post_code = "";
		$tel = "";
		$fax = "";
		$phonecode = "";
		$website = "";
		$available_services = array();
		$available_services_string = "";
		//$term = "";
		
		//New update
		$first_name="";
		$last_name="";
		$company_name="";
		$province_city="";
		$address="";
		

		extract($_POST);

		// set registration form session.
		// $_SESSION['reg_id'] = $id;
		$_SESSION['userlog_id']=$id;
		$_SESSION['reg_email'] = $email;
		//$_SESSION['reg_confirm_email'] = $confirm_email;
		$_SESSION['reg_company_name'] = $company_name;
        $_SESSION['reg_name'] = $name;
		$_SESSION['reg_card_id'] = $card_id;
		$_SESSION['reg_country'] = $country;
		$_SESSION['reg_address'] = $address;
		$_SESSION['reg_post_code'] = $post_code;
		$_SESSION['reg_tel'] = $tel;
		$_SESSION['reg_fax'] = $fax;
		$_SESSION['reg_name'] = $reg_name;
		$_SESSION['reg_mobile'] = $mobile;
		$_SESSION['reg_website'] = $website;

        // unset security session.
        unset($_SESSION['security_question_a']);
        unset($_SESSION['security_question_b']);


		require_once BASE_CLASS . 'class-utilities.php';


        // validate fields.
		/* checks if any member_type_1 is checked */
		if( isset($member_type_1) ) {
			/* Iterate the member_type_1 array and get the keys and values */
			foreach($member_type_1 as $key => $value) {
				if( isset($member_type_1[$key]) ) {
					$_SESSION['reg_member_type_1_'.$key] = $value;
				}
				$member_type_1_string.= $value.",";
			}

			/* unset member_type_1 session */
			if( empty($member_type_1[0]) ) { unset($_SESSION['reg_member_type_1_0']); }
			if( empty($member_type_1[1]) ) { unset($_SESSION['reg_member_type_1_1']); }
			if( empty($member_type_1[2]) ) { unset($_SESSION['reg_member_type_1_2']); }

			$member_type_1 = substr($member_type_1_string, 0, -1);
		}

		/* checks if any member_type_2 is checked */
		if( isset($member_type_2) ) {
			/* Iterate the member_type_2 array and get the keys and values */
			foreach($member_type_2 as $key => $value) {
				if( isset($member_type_2[$key]) ) {
					$_SESSION['reg_member_type_2_'.$key] = $value;
				}
				$member_type_2_string.= $value.",";
			}

			/* unset member_type_2 session */
			if( empty($member_type_2[0]) ) { unset($_SESSION['reg_member_type_2_0']); }
			if( empty($member_type_2[1]) ) { unset($_SESSION['reg_member_type_2_1']); }

			$member_type_2 = substr($member_type_2_string, 0, -1);
		}

		/* checks if any business_type is checked */
		if( isset($business_type) ) {
			/* Iterate the business_type array and get the keys and values */
			foreach($business_type as $key => $value) {
				if( isset($business_type[$key]) ) {
					$_SESSION['reg_business_type_'.$key] = $value;
				}
				$business_type_string.= $value.",";
			}

			/* unset business_type session */
			if( empty($business_type[0]) ) { unset($_SESSION['reg_business_type_0']); }
			if( empty($business_type[1]) ) { unset($_SESSION['reg_business_type_1']); }

			$business_type = substr($business_type_string, 0, -1);
		}

		/* checks if any business_field is checked */
		if( isset($business_field) ) {
			/* Iterate the business_field array and get the keys and values */
			foreach($business_field as $key => $value) {
				if( Utilities::Required($business_field[$key]) ) {
					$_SESSION['reg_business_field_'.$key] = $value;
				}
				$business_field_string.= $value.",";
			}

			/* unset business_field session */
			if( empty($business_field[0]) ) { unset($_SESSION['reg_business_field_0']); }
			if( empty($business_field[1]) ) { unset($_SESSION['reg_business_field_1']); }
			if( empty($business_field[2]) ) { unset($_SESSION['reg_business_field_2']); }
			if( empty($business_field[3]) ) { unset($_SESSION['reg_business_field_3']); }
			if( empty($business_field[4]) ) { unset($_SESSION['reg_business_field_4']); }
			if( empty($business_field[5]) ) { unset($_SESSION['reg_business_field_5']); }
			if( empty($business_field[6]) ) { unset($_SESSION['reg_business_field_6']); }
			if( empty($business_field[7]) ) { unset($_SESSION['reg_business_field_7']); }
			if( empty($business_field[8]) ) { unset($_SESSION['reg_business_field_8']); }
			if( empty($business_field[9]) ) { unset($_SESSION['reg_business_field_9']); }

			$business_field = substr($business_field_string, 0, -1);
		}

		/* checks if any available_services is checked */
		if( isset($available_services) ) {
			/* Iterate the available_services array and get the keys and values */
			foreach($available_services as $key => $value) {
				if( isset($available_services[$key]) ) {
					$_SESSION['reg_available_services_'.$key] = $value;
				}
				$available_services_string.= $value.",";
			}

			/* unset available_services session */
			if( empty($available_services[0]) ) { unset($_SESSION['reg_available_services_0']); }
			if( empty($available_services[1]) ) { unset($_SESSION['reg_available_services_1']); }
			if( empty($available_services[2]) ) { unset($_SESSION['reg_available_services_2']); }
			if( empty($available_services[3]) ) { unset($_SESSION['reg_available_services_3']); }
			if( empty($available_services[4]) ) { unset($_SESSION['reg_available_services_4']); }
			if( empty($available_services[5]) ) { unset($_SESSION['reg_available_services_5']); }
			if( empty($available_services[6]) ) { unset($_SESSION['reg_available_services_6']); }
			if( empty($available_services[7]) ) { unset($_SESSION['reg_available_services_7']); }

			$available_services = substr($available_services_string, 0, -1);
		}
		if(isset($_FILES['file1']['name'])){
			$file = $_FILES['file1']['name'];
		}
		
		//var_dump($file);
		//$cname=$_POST['company_name'];
		//echo $file."";
		if(isset($_FILES['file1']['tmp_name'])){
		$uploadedfile = $_FILES['file1']['tmp_name'];
		}
		// Сheck that we have a file
		if((!empty($_FILES["file1"])) && ($_FILES['file1']['error'] == 0)) {
			// Check if the file is JPEG or PNG image and it's size is less than 350Kb
			$ext = substr(strtolower($file), strrpos($file, '.') + 1);

		    if ( $ext != "php" ) {
				$cnx = new Connect();
	            $cnx->open();
				$fname = date('Ymd_His') . '_' . mt_rand('0000000','9999999') . '.' . $ext;
			    move_uploaded_file($_FILES['file1']['tmp_name'] ,BASE_ROOT."/upload/".$fname);
	            $my_info=$fname;
				$thum_myinfo=$fname;
				$resize_myinfo=new resize(BASE_ROOT."/upload/".$fname);
		        $resize_thumb = new resize(BASE_ROOT."/upload/".$fname);
			    $resize_myinfo->resizeImage(500, 400, 'auto');
		        $resize_thumb->resizeImage(160, 120, 'auto');
			    $resize_myinfo->saveImage(BASE_ROOT."/upload/myinfo/".$fname, 100);
		        $resize_thumb->saveImage(BASE_ROOT."/upload/thumb/".$fname, 100);
				if(file_exists(BASE_ROOT."/upload/".$fname)){
					unlink(BASE_ROOT."/upload/".$fname);
				}
			    if(file_exists(BASE_ROOT."/upload/thumb/".$_SESSION['old_image'])){
					unlink(BASE_ROOT."/upload/thumb/".$_SESSION['old_image']);
				}
			   if(file_exists(BASE_ROOT."/upload/myinfo/".$_SESSION['old_image'])){
					unlink(BASE_ROOT."/upload/myinfo/".$_SESSION['old_image']);
				}
				$sql_update="UPDATE register SET image = '$fname' WHERE log_id = '".$_SESSION['userlog_id']."'";

				mysql_query($sql_update);

	            $this->register_form_status = 'complete';
	            header( "refresh:0;url=".BASE_RELATIVE."my-info");
	           	$cnx->close();
	        }else{
				$file = "";
				$this->register_form_status = 'upload';
			}
		}
		else
		{
			$file = "";
            //$this->register_form_status = 'upload';
		}
		//validate user id
        // check if email is already registered.
        require_once BASE_CLASS . 'class-connect.php';

        $db = new Connect();
        $db->open();
        // filter values.
        $email = mysql_real_escape_string($email);
        $name = mysql_real_escape_string($name);
        $passMD5="";
        $password_value="";
        if(!empty($password)){
		    if ($cpassword==""&&$_SESSION['log_group']!='admin'){
				$passMD5=$chekpass;
			}else{
				$passMD5 = md5($password);
			}
        	$password_value="`password`='$passMD5', ";
		}
		//echo $chekpass."<br/>".$passMD5;
        // register the user account.
		//echo $reg_id;
		//mysql_query("UPDATE data_count set `country`='$country' WHERE `register_id`='$reg_id';");
		
		if($password!="" and $confirm_password!=""){
			$new_password=md5($confirm_password);
			 if($sql = @mysql_query("UPDATE  `register` SET `username`='$id',`email`='$email',`company_name`='$company_name',`mobile`='$mobile',`firstname`='$first_name',`lastname`='$last_name',`province`='$province_city',`address`='$address',`password`='$new_password' WHERE `user_id`='$reg_id';") ) {
			
			   echo $id."<br/>".$email;

	            require_once BASE_CLASS . 'class-log.php';

	            LogReport::write('Unable to query database at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

	            $db->close();
	            $this->register_form_status = 'query';
				
				
				header("Location: " .BASE_RELATIVE . "message-myinfo");

	            return;
        }
			else{
			if((!empty($_FILES["file1"])) && ($_FILES['file1']['error'] == 0)) {
				mysql_query("UPDATE register SET image = '$file' WHERE log_id = '$id'");
			}

			if((!empty($_FILES["file2"])) && ($_FILES['file2']['error'] == 0)) {
				$this ->uploadimage();
				$mysql_query="insert into more_image (m_image, m_thumb) values('".$this->img_path."', '".$this->thumb_path."')";
				//echo $mysql_query;
				mysql_query($mysql_query);
			}

            $db->close();
            $this->register_form_status = 'complete';
            header ( "refresh:0;url=".BASE_RELATIVE."my-info" );
        }
	}
	
	
	//Not update password.
	
	if($sql = @mysql_query("UPDATE  `register` SET `username`='$id',`email`='$email',`company_name`='$company_name',`mobile`='$mobile',`firstname`='$first_name',`lastname`='$last_name',`province`='$province_city',`address`='$address' WHERE `user_id`='$reg_id';") ) {
			
			   echo $id."<br/>".$email;

	            require_once BASE_CLASS . 'class-log.php';

	            LogReport::write('Unable to query database at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

	            $db->close();
	            $this->register_form_status = 'query';
				
				
				header("Location: " .BASE_RELATIVE . "message-myinfo");

	            return;
        }
			else{
			if((!empty($_FILES["file1"])) && ($_FILES['file1']['error'] == 0)) {
				mysql_query("UPDATE register SET image = '$file' WHERE log_id = '$id'");
			}

			if((!empty($_FILES["file2"])) && ($_FILES['file2']['error'] == 0)) {
				$this ->uploadimage();
				$mysql_query="insert into more_image (m_image, m_thumb) values('".$this->img_path."', '".$this->thumb_path."')";
				//echo $mysql_query;
				mysql_query($mysql_query);
			}

            $db->close();
            $this->register_form_status = 'complete';
            header ( "refresh:0;url=".BASE_RELATIVE."my-info" );
        }
		
       
}

    /*
     * Get registration status -------------------------------------------------
     * @return string.
     */
    public function getRegistrationStatus()
    {
        return $this->register_form_status;
    }
	public function getAllImage()
	{
		require_once BASE_CLASS . 'class-connect.php';

        $db = new Connect();
        $db->open();
		$image = array();
		$mysql_query="select * from more_image";
		//echo $mysql_query;
		if($sql=mysql_query($mysql_query)){
			 while( $r = @mysql_fetch_assoc($sql) ){
				 array_push($image,$r);
			 }

		}
		return $image;
    }



	public function uploadimage()

	{

		$allowedExts = array("gif", "jpeg", "jpg", "png");
		$temp = explode(".", $_FILES["file2"]["name"]);
		$extension = strtolower( end($temp));

		if ((($_FILES["file2"]["type"] == "image/gif")
		|| ($_FILES["file2"]["type"] == "image/jpeg")
		|| ($_FILES["file2"]["type"] == "image/jpg")
		|| ($_FILES["file2"]["type"] == "image/pjpeg")
		|| ($_FILES["file2"]["type"] == "image/x-png")
		|| ($_FILES["file2"]["type"] == "image/png"))
		&& ($_FILES["file2"]["size"] < 8000000)
		&& in_array($extension, $allowedExts)) {
		  if ($_FILES["file2"]["error"] > 0) {
			return false;
		  } else {


			  move_uploaded_file($_FILES["file2"]["tmp_name"],
			  'upload/myinfo/' . $_FILES["file2"]["name"]);
			  $this->img_path= 'upload/myinfo/' . $_FILES["file2"]["name"];
			  // *** Include the class
				include_once("resize-class.php");

				// *** 1) Initialise / load image
				$resizeObj = new resize('upload/myinfo/' . $_FILES["file2"]["name"]);

				// *** 2) Resize image (options: exact, portrait, landscape, auto, crop)
				$resizeObj -> resizeImage(160, 116, 'crop');

				// *** 3) Save image
				$resizeObj -> saveImage('upload/thumb/' . $_FILES["file2"]["name"], 100);
				$this->thumb_path='upload/thumb/' . $_FILES["file2"]["name"];
			  return true;
			}


		} else {
		  return false;
		}



	}
	/**
     * Public method: get form status
     * <br>---------------------------------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }

    /**
     * Public method: get form message
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /**
     * Public method: get form style
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }
}


