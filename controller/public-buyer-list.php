<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
include('view/3rdparty/pagination/pagination.php');
class publicbuyerlist
{
    protected $lang;
    protected $group = 'public';
    protected $slug = 'car';
    protected $form_status;
    protected $form_message;
    protected $form_style;
	
	private $load_buyer;
	// assign variable	
	var $pagination_html="";
	var $total_buyer="";	
	 /*
     * Constructor -------------------------------------------------------------
     */
    public function  publicbuyerlist($lang=array()){
	   $this->lang = $lang;
	   $this->loadbuyer();
	}
	private function loadbuyer(){
	   require_once BASE_CLASS . 'class-connect.php';

		$cnx = new Connect();
		$cnx->open();
		
		global $pagination_html;
		global $total_buyer;		
		
		$sql_search="SELECT * From buy_car";				
		
		if(isset($_POST['cmdsearch'])){
			$where=" WHERE full_name LIKE '%".$_POST['search']."%'";			
		}
		
		$sql_search.=$where;		
		
				
		
		/////PAGINATION PROCESS//////
        
        $itemPerPage=12;
        $sql_count = @mysql_query($sql_search);
        $total_buyer= @mysql_num_rows($sql_count);

        $links = new Pagination ($total_buyer, $itemPerPage);
        $limit=$links->start_display;
        $pagination_html.= $links->display();
		
		$sql_search.=$limit;
		//$this->hit_counter=mysql_real_escape_string($_POST['hit_count']);
		if( !$sql = @mysql_query($sql_search) ){
			require_once BASE_CLASS . 'class-log.php';
			LogReport::write('Unable to load Recent Product list due to a query error at ' . __FILE__ . ':' . __LINE__);
			$cnx->close();
			return;
		}
		
		
		if( @mysql_num_rows($sql) < 1 ){
			$cnx->close();
		}

		$cache_buyer = array();
		
		while( $r = @mysql_fetch_assoc($sql) ){
			//$total_count = $r['total'];			
			
			$mysql = "SELECT * From buy_car";
			$myquery = @mysql_query($mysql);						
			
			if (@mysql_num_rows($myquery)>0){
				//$myimage = @mysql_fetch_assoc($myquery);
				$cache_buyer[]=array('full_name'=>$r['full_name'],'gender'=>$r['gender'],'tel'=>$r['tel'],'email'=>$r['email'],
				'city_province'=>$r['city_province'],'make'=>$r['make'],'model'=>$r['model'],'year_car'=>$r['year_car'],
				'color'=>$r['color'],'condition_car'=>$r['condition_car'],'price'=>$r['price'],'description'=>$r['description']
				);			
			}
			else {
				$cache_buyer[]=array('full_name'=>$r['full_name'],'gender'=>$r['gender'],'tel'=>$r['tel'],'email'=>$r['email'],
				'city_province'=>$r['city_province'],'make'=>$r['make'],'model'=>$r['model'],'year_car'=>$r['year_car'],
				'color'=>$r['color'],'condition_car'=>$r['condition_car'],'price'=>$r['price'],'description'=>$r['description']
				);
				
			}			
		}		
		
		$this->buyer=$cache_buyer;
		@mysql_free_result($sql);
		$cnx->close();
	}
	
	public function load_buyer(){
        return $this->buyer;
    }
}


?>