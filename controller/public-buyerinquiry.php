<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
include('view/3rdparty/pagination/pagination.php');
class publicbuyerinquiry
{
    protected $lang;
    protected $group = 'public';
    protected $slug = 'buyerinquiry';
    protected $form_status;
    protected $form_message;
    protected $form_style;
    protected $listarray;
    Protected $country_list;
 
    protected $total_num_row;
    protected $total_page;
    protected $current_page;
    var $pagination_html;


    /*
     * Constructor -------------------------------------------------------------
     */
    public function publicbuyerinquiry($lang=array())
    {
        $this->lang = $lang;
        $this->listinquiry();
        $this->loadCountryList();
        if(isset($_GET['btnsearch'])){
            $this->loadCountryList();
        }
    }
    
    function loadExpertCompany(){       
       
        //echo $item.''.$category;
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
       
        
        ////////
        $sql_search="SELECT * from register WHERE register_type='ec' ORDER BY RAND() LIMIT 0,20"; 
      
        //echo $sql_search;
        if( !$sql = @mysql_query($sql_search) ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load car maker list due a query error at ' . __FILE__ . ':' . __LINE__);
            $cnx->close();
            return;
        }

        if( @mysql_num_rows($sql) < 1 ){
            $cnx->close();
        }
        
         
        $expertCompany = array(); 
       
        while( $r = @mysql_fetch_assoc($sql)){
           
            array_push($expertCompany, $r);
          
        }
         
  
        @mysql_free_result($sql);
        $cnx->close();
        return $expertCompany;

    }

    /*
     * Public method: get page content data ------------------------------------
     * @return array | false.
     */

    private function loadCountryList(){
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        if( !$sql = @mysql_query("SELECT * FROM `country_list` ORDER BY `country_name` ASC") ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load country list due a query error at ' . __FILE__ . ':' . __LINE__);
            
            $this->country_list = array();
            
            return;
        }
       
        if( @mysql_num_rows($sql) < 0 ){
            $cnx->close();          
            return;
        }
        
        $this->country_list = array();
        
        while( $r = @mysql_fetch_assoc($sql) ){
            $obj['cc'][] = $r['cc'];
            $obj['country_name'][] = stripslashes($r['country_name']);
           
            
            $this->country_list=$obj;
        }
        @mysql_free_result($sql);
        $cnx->close();
    }


    public function getCountryList(){
        return $this->country_list;
    }

    public function gettotal()
    {
        return $this->total_num_row;
    }

    public function gettotalpage()
    {
        return $this->total_page;
    }

    public function getcurrentpage()
    {
        return $this->current_page;
    }

    public function listinquiry( )
    {

         require_once BASE_ROOT . 'core/class-connect.php';

        $cnx = new Connect();
        $cnx->open();
        if(isset($_GET['btnsearch'])){

            $type="";
            $country="";
            $option="";
            $text="";
            if(isset($_GET['rdo-car-type']))
            {
                $type=$_GET['rdo-car-type'];
                
            }
            if(isset($_GET['countryInput']))
            {
                $country=$_GET['countryInput'];
               
            }
            if(isset($_GET['dpsearch']))
            {
                $option=$_GET['dpsearch'];
            }
            if(isset($_GET['searchtext']))
            {
               $text=$_GET['searchtext']; 
            }

            $query=""; 
            $query="SELECT i.*,cl.`country_name` FROM `inquiry` `i` 
                LEFT JOIN `country_list` `cl` ON `country`=`cc`
                LEFT JOIN `register` r ON i.`owner`= r.`card_id` "
                 ;

            if($type=="all")
            {
                $query="SELECT i.*,cl.`country_name` FROM `inquiry` `i` 
                LEFT JOIN `country_list` `cl` ON `country`=`cc`
                LEFT JOIN `register` r ON i.`owner`= r.`id` "
                ;
                if($country!="")
                {
                    $query.="WHERE i.`country`= '$country' ";
                }
                
                if($option!="")
                {

                    if($country!="")
                    {
                        if($option=="buyerid")
                        {
                            $query.="AND r.`card_id`='$text' AND r.`member_type1` LIKE 'Buyer%'";
                              
                        }
                        if($option=="​sellerid")
                        {

                            $query.="AND r.`card_id`='$text' 
                                AND (r.`member_type1` LIKE 'Buyer,Seller%' 
                                or r.`member_type1` LIKE 'Seller%') ";
                             
                        }
                        if($option=="itemnumber")
                        {
                            $query.="AND i.`id`='$text' ";
                             
                        }
                    }
                    else
                    {
                        
                        if($option=="buyerid")
                        {  
                            $query.="WHERE r.`card_id`='$text' AND r.`member_type1` LIKE 'Buyer%'";
                        }
                         
                        if($option=="sellerid")
                        {
                             
                             $query.="WHERE r.`card_id`='$text' 
                                AND (r.`member_type1` LIKE 'Buyer,Seller%' 
                                or r.`member_type1` LIKE 'Seller%') ";
                        }
                        if($option=="itemnumber")
                        {
                            $query.="WHERE i.`id`='$text' ";
                        }
                    }

                }

                if($option=="" && $text!="")
                {
                    if($country=="")
                    {
                            $query.="WHERE i.`id`='$text' 
                            OR (r.`card_id`='$text' AND r.`member_type1` LIKE 'Buyer%') 
                            OR (r.`card_id`='$text' 
                                AND (r.`member_type1` LIKE 'Buyer,Seller%' 
                                or r.`member_type1` LIKE 'Seller%')) "; 
                    }
                    else
                    {
                       $query.="AND i.`id`='$text' 
                        OR (r.`card_id`='$text' AND r.`member_type1` LIKE 'Buyer%') 
                        OR (r.`card_id`='$text' 
                        AND (r.`member_type1` LIKE 'Buyer,Seller%' 
                        or r.`member_type1` LIKE 'Seller%')) ";   
                    }

                   
                }
                


            } 
            else
            {
                $query="SELECT i.*,cl.`country_name` FROM `inquiry` `i` 
                LEFT JOIN `country_list` `cl` ON `country`=`cc`
                LEFT JOIN `register` r ON i.`owner`= r.`id` 
                WHERE i.`vehicle_type`='$type' "
                ;
                if($country!="")
                {
                    $query.="AND i.`country`='$country' ";
                }
                
                if($option!="" && $text!="")
                {
                     
                        if($option=="buyerid")
                        {
                            $query.="AND r.`card_id`='$text' AND r.`member_type1` LIKE 'Buyer%'";
                        }
                        elseif( $option=="sellerid")
                        {
                            $query.="AND r.`card_id`='$text' 
                                AND (r.`member_type1` LIKE 'Buyer,Seller%' 
                                or r.`member_type1` LIKE 'Seller%') ";
                        }
                        elseif($option=="itemnumber")
                        {
                            $query.="AND i.`id`='$text' ";
                        }
                    

                     

                }

                if($option=="" && $text!="")
                {
                   $query.="AND (i.`id`='$text' 
                            OR r.`card_id`='$text' AND r.`member_type1` LIKE 'Buyer%') 
                            OR (r.`card_id`='$text' 
                                AND (r.`member_type1` LIKE 'Buyer,Seller%' 
                                or r.`member_type1` LIKE 'Seller%')) "; 
                }
                

            }

            $query.="ORDER BY 1 DESC ";  
            $sql_count_str= $query;
            $sql_count = @mysql_query($sql_count_str);
            $this->total_num_row= @mysql_num_rows($sql_count);
            
            $links = new Pagination ($this->total_num_row,10);
            $limit=$links->start_display;
            $this->pagination_html= $links->display(); 
            $this->total_page=$links->numPages();
            $this->current_page=$links->currentPage();
            ///////PAGINATION PROCESS///////
            //$sql_str=$sql_count_str.$limit;  
            $query.=$limit;


            if( !$sql =@mysql_query($query) )
            {
                $cnx->close();

                require_once BASE_ROOT . 'core/class-log.php';

                LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

                $this->form_message = 'Unable to load page content due an internal error.';
                $this->form_status = true;
                $this->form_style = 'alert-error';

                return;
            }
 
        }
        else
        {
 
            /////PAGINATION PROCESS//////
        $query="SELECT i.*,cl.`country_name` FROM `inquiry` `i` LEFT JOIN `country_list` `cl` ON
                `country`=`cc` ORDER BY 1 DESC ";

        $sql_count_str= $query;
        $sql_count = @mysql_query($sql_count_str);
        $this->total_num_row= @mysql_num_rows($sql_count);
        
        $links = new Pagination ($this->total_num_row,10);
        $limit=$links->start_display;
        $this->pagination_html= $links->display(); 
        $this->total_page=$links->numPages();
        $this->current_page=$links->currentPage();
        ///////PAGINATION PROCESS///////
        //$sql_str=$sql_count_str.$limit;  
        $query.=$limit;
        

            if( !$sql = @mysql_query($query.";" ) )
            {
                $cnx->close();

                require_once BASE_ROOT . 'core/class-log.php';

                LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

                $this->form_message = 'Unable to load page content due an internal error.';
                $this->form_status = true;
                $this->form_style = 'alert-error';

                return;
            }
             
             
        }

        $this->listarray = array();
            while( $r = @mysql_fetch_assoc($sql) ){

                array_push($this->listarray, $r);
         
            }
        //var_dump($this->listarray);
        @mysql_free_result($rows_count);
        @mysql_free_result($sql);
        return $this->listarray;


    }

    public function getPagination(){
        return $this->pagination;
    }

    public function getPageHTML()
    {
        require_once BASE_ROOT . 'core/class-connect.php';

        $cnx = new Connect();
        $cnx->open();

        if( !$sql = @mysql_query("SELECT * FROM `page` WHERE `page_slug`='$this->slug' AND `page_group`='$this->group' LIMIT 1;") )
        {
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }

        if( @mysql_num_rows($sql) != 1 )
        {
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }

        $r = @mysql_fetch_assoc($sql);

        $result                     = array();
        $result['id']               = $r['id'];
        $result['meta_keywords']    = $r['meta_keywords'];
        $result['meta_description'] = $r['meta_description'];
        $result['meta_robots']      = $r['meta_robots'];
        $result['page_title']       = $r['page_title'];
        $result['page_slug']        = $r['page_slug'];
        $result['page_publish']     = $r['page_publish'];
        $result['page_author']      = $r['page_author'];
        $result['creation']         = $r['creation'];
        $result['editable']         = $r['editable'];
        $result['page_group']       = $r['page_group'];
        $result['html']             = $r['html'];

        @mysql_free_result($sql);
        $cnx->close();

        return $result;
    }

    /*
     * Public method: get form status ------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }

    /*
     * Public method: get form message -----------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /*
     * Public method: get form style -------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }

}
?>
