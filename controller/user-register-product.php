<?php

if(!isset($_SESSION)) @session_start();
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################

class userregisterproduct {
    protected $lang;
    protected $form_status;
    protected $form_message;
    protected $form_style;
	 private $vehicle;
 
    /**
     * Constructor
     * <br>---------------------------------------------------------------------
     */
    public function userregisterproduct($lang=array()){
        $this->lang = $lang;
        $user_detail=$this->getUserDetail();
        if(($user_detail['member_type1']!='Seller'&&$user_detail['member_type1']!='Both')
            ||$user_detail['email']==''
            ||$user_detail['country']==''
            ||$user_detail['company_name']==''
        ){
            //Invalid Seller
            header("Location: edit-myinfo?msg=incomplete_seller");
        }
	}
    private function getUserDetail(){
        require_once BASE_CLASS . 'class-connect.php';        
        $cnx = new Connect();
        $cnx->open();
        $user_id=$_SESSION['log_id'];
        $user_detail=array();
        $sql="SELECT member_type1, email, country, company_name FROM register WHERE `id`='$user_id' ";
        $result=mysql_query($sql);
        while($row=mysql_fetch_assoc($result)){
            $user_detail=$row;
        }
        $cnx->close();
        return $user_detail;
    }
	
	 
}