<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
/*
 * Handles and render menus based on group authentication status.
 */
class MenuManager
{
    protected $menu_path;
    protected $user_group;
    protected $cms_menu;
    protected $subcategory_list;
    protected $menu_topic;
    protected $menu_link;

    /*
     * Constructor -------------------------------------------------------------
     * - loads menu items based on user group status.
     * @param $user_group - the current user's group status.
     * @return void.
     */
    public function MenuManager($user_group)
    {
        // validate group.
        if( empty($user_group) )
        {
            $user_group = 'public';
        }
        
        $this->user_group = $user_group;
                
        // load CMS menu items which are enabled (display) 
        $path = BASE_ROOT . 'view/menus/' . $user_group . '-menu.php';
        
        if( !file_exists($path) )
        {
            $path = '';
        }
        
        $this->menu_path = $path;
    }
    
    /*
     * Load menu ---------------------------------------------------------------
     * @return void.
     */
    public function loadMenu($glang)
    {
        $p = $this->menu_path;
        
        if( !empty($p) )
        {
            $_GLANG = $glang;
            
            require_once $p;
            
            // add public menu if not administrator.
            if( $this->user_group != 'admin' && $this->user_group != 'public' ){
                $ppath = BASE_ROOT . 'view/menus/public-menu.php';
                if( file_exists($ppath) ){
                    require_once $ppath;
                }
            }
        }
        
        if( $this->user_group != 'admin' )
        {
            // load CMS and article page list.
            $pages_list = $this->loadPageList();

            $this->cms_menu = '             <div id="cms-wrapper">' . "\r\n";

            if( count($this->menu_topic) > 0 )
            {
                for( $t=0; $t < count($this->menu_topic); $t++ )
                {
                    // display header.
                    $this->cms_menu .= '                <span class="tab-title"> '. stripslashes($this->menu_topic[$t]['label']) .'</span>' . "\r\n";
                    
                    // display links.
                    for( $l=0; $l < count($this->menu_link); $l++ )
                    {
                        if( $this->menu_link[$l]['menu_topic_id'] == $this->menu_topic[$t]['id'] )
                        {
                            $pmslug = $this->menu_link[$l]['page_url'];
                            $pmslug = explode('-',$pmslug);
                            $newslug = '';
                            
                            for( $s=1; $s < count($pmslug); $s++ )
                            {
                                $newslug .= $pmslug[$s];
                                
                                if( ($s+1) < count($pmslug) )
                                {
                                    $newslug .= '-';
                                }
                            }
                            
                            $this->cms_menu .= '                    <a href="'. BASE_RELATIVE . DEFAULT_HOME_SLUG .'/'.$newslug.'/">'.$this->menu_link[$l]['label'].'</a>' . "\r\n";
                        }
                    }
                }
            }
                    
            $this->cms_menu .= '             </div>' . "\r\n";

            echo $this->cms_menu;
            
        }
    }
        
    /*
     * Public method: get CMS page list ----------------------------------------
     * @return string.
     */
    public function getCMSMenu()
    {
        return $this->cms_menu;
    }
    
    /*
     * Protected method: load page list ----------------------------------------
     * @return array.
     */
    protected function loadPageList()
    {
        require_once BASE_CLASS . 'class-connect.php';
        
        $db = new Connect();
        $db->open();
        
        // set user group.
        
        
        if( $this->user_group == 'admin' ){
            $group_query = "`group`='admin'";
        }
        else if( $this->user_group == 'user' ) {
            $group_query = "`group`='public' OR `group`='user'";
        }
        else {
            $group_query = "`group`='$this->user_group'";
        }
        
        // load menu topics.
        if( !$sql = @mysql_query("SELECT * FROM `menu_topic` WHERE `enabled`='1' AND ".$group_query." ORDER BY `order` ASC") )
        {
            require_once BASE_CLASS . 'class-log.php';
            
            $db->close();
            $this->menu_link = array();
            $this->menu_topic = array();
            
            LogReport::write('Unable to load menu_topic list due a query error at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());
            return;
        }
        
        if( @mysql_num_rows($sql) < 1 )
        {
            $db->close();
            $this->menu_link = array();
            $this->menu_topic = array();
            return;
        }
        
        $topic = array();
        
        while( $r = @mysql_fetch_assoc($sql) )
        {
            $o = array(
                'id' => $r['id'],
                'label' => $r['label']
            );
            
            array_push($topic,$o);
        }
        
        @mysql_free_result($sql);
        
        // load menu links.
        if( !$sql = @mysql_query("SELECT * FROM `menu_link` WHERE `enabled`='1' ORDER BY `order` ASC") )
        {
            require_once BASE_CLASS . 'class-log.php';
            
            $link = array();
            
            LogReport::write('Unable to load menu_link list due a query error at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());
        }
        else
        {
            if( @mysql_num_rows($sql) > 0 )
            {
                $link = array();
                
                while( $r = @mysql_fetch_assoc($sql) )
                {
                    $o = array(
                        'id' => $r['id'],
                        'label' => $r['label'],
                        'page_url' => $r['page_url'],
                        'menu_topic_id' => $r['menu_topic_id']
                    );
                    
                    array_push($link,$o);
                }
                
                @mysql_free_result($sql);
            }
            else
            {
                $link = array();
            }
        }
        
        $db->close();
        
        $this->menu_link = $link;
        $this->menu_topic = $topic;
    }
}