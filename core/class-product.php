<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
/*
 * Send message
 */

if(!isset($_SESSION)) session_start();


class Product
{
    protected $allMessages;
    protected $errorMessages=array();
    protected $total_number=array();

    protected $pagination_html;
    protected $limit=array();
    protected $itemPerPage=20;

    /*
     * Constructor -------------------------------------------------------------
     */
    public function __construct(){ 
        //$this->loadAjaxPagination();
    }
    public function getProductById($id, $arr_field=array()){
        require_once BASE_ROOT.'config.php';
        require_once BASE_CLASS . 'class-connect.php';
        $product =array();
        $cnx = new Connect();
        $cnx->open();
        $sql_field='';
        // /$log_id=$_SESSION['log_id'];
        if(count($arr_field)>0){
            $sql_field=implode(", ", $arr_field);
        }else{
            $sql_field="*";
        }

        $sql="SELECT $sql_field FROM product 
            LEFT JOIN register ON register.id=product.owner
            LEFT JOIN country_list ON country_list.cc = product.location
            WHERE product.id='$id'";
        $result=mysql_query($sql);
        if($result){
            while($row=mysql_fetch_assoc($result)){
                
                $product= $row;
            }
        }else{
             $this->errorMessages[]="Unable to get product with this id. Unknown Error occurred.";
        }
        $cnx->close();
        return $product;
    }
    /* Get all product images by id */
    public function getProductImagesById($id){
        require_once BASE_ROOT.'config.php';
        require_once BASE_CLASS . 'class-connect.php';
        $product_images =array();
        $cnx = new Connect();
        $cnx->open();
        $sql="SELECT * FROM car_media WHERE product_id='$id' ORDER BY primary_photo DESC";
        $result=mysql_query($sql);
        if($result){
            while($row=mysql_fetch_assoc($result)){
                array_push($product_images, $row);
            }
        }else{
             $this->errorMessages[]="Unable to get image of this product. Unknown Error occurred.";
        }
        $cnx->close();
        return $product_images;
    }

    public function getTotalNumber($where=array()){
        require_once BASE_ROOT.'config.php';
        require_once BASE_CLASS . 'class-connect.php';
        $product_images =array();
        $cnx = new Connect();
        $cnx->open();
        $where_sql="";
        if(count($where)>0){
             $where_sql="AND ".implode(" AND ", $where);
        }
        $sql="SELECT COUNT(*) as total_number FROM active_product WHERE 1 $where_sql";

        $result=mysql_query($sql);
        if(!$result){
            $cnx->close();
            $this->errorMessages[]="Unable to get number of products. An unknown error occurred";
        }

        $row=mysql_fetch_assoc($result);
        
        $cnx->close();
        return $row['total_number'];
    }
}
?>

