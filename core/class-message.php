<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
/*
 * Send message
 */

if(!isset($_SESSION)) session_start();

class Message
{
    protected $allMessages;
    protected $errorMessages=array();
    protected $total_number=array();
    protected $total_unread_number=0;

    protected $pagination_html;
    protected $limit=array();
    protected $itemPerPage=20;
    protected $destPath="upload/message-attachment/";

    public $last_insert_msg_id='';
    /*
     * Constructor -------------------------------------------------------------
     */
    public function __construct(){
        if(isset($_SESSION['log_id'])){
            $this->loadTotalNumberMessage();
        }
        //$this->loadAjaxPagination();
    }
    /**
    * Function get all attachment encode name from database by message id
    * @param $message_id: message id
    * @return array row of message_attachment
    */
    public function getMessageAttachments($message_id){
        /* Connect to database */
        require_once BASE_CLASS . 'class-connect.php';
        $cnx = new Connect();
        $cnx->open();
        /* Initial variable */
        $log_id=$_SESSION['log_id'];
        $messageAttachments=array();
        /* DATABASE PROCESS */
        $sql="SELECT msga.*
            FROM message_attachment msga
            INNER JOIN messages msg ON msg.id= msga.message_id
            WHERE msga.message_id='$message_id' AND (msg.sender='$log_id' OR  msg.receiver='$log_id')
        ";

        $result=mysql_query($sql);
        if($result){
            while($row=mysql_fetch_assoc($result)){
                array_push($messageAttachments, $row);
            }
        }else{
            $this->errorMessages[]="Unable to get the attachment list. An unknown error occurred.";
        }
        $cnx->close();
        return $messageAttachments;
    }
    /**
    * Function get attachment filename from database
    * @param $encodeName: encode_name match with database
    * @return string file url if found or empty if not found
    */
    public function getAttachmentByEncodeName($encodeName){
        /* Connect to database */
        require_once BASE_CLASS . 'class-connect.php';
        $cnx = new Connect();
        $cnx->open();
        /* Initial variable */
        $log_id=$_SESSION['log_id'];
        $messageAttachments=array();
        /* DATABASE PROCESS */
        $sql="SELECT msga.file_path, msga.file_name, msga.file_title
            FROM message_attachment msga
            INNER JOIN messages msg ON msg.id= msga.message_id
            WHERE msga.encode_name='$encodeName' AND (msg.sender='$log_id' OR  msg.receiver='$log_id')
        ";
        $result=mysql_query($sql);
        if($result){
            while($row=mysql_fetch_assoc($result)){
                $messageAttachments=$row;
            }
        }else{
            $this->errorMessages[]="Unable to get the attachment file. The files maybe has been removed or you don't have permission to download it.";
        }
        $cnx->close();
        return $messageAttachments;
    }
    /* Function get message template from database */
    public function getMessageTemplates(){
        /* Connect to database */

        require_once BASE_CLASS . 'class-connect.php';
        $cnx = new Connect();
        $cnx->open();
        /* Initial variable */
        $messageTemplate=array();
        /* DATABASE PROCESS */
        $sql="SELECT * FROM message_template";
        $result=mysql_query($sql);
        if($result){
            while($row=mysql_fetch_assoc($result)){
                array_push($messageTemplate, $row);
            }
        }else{
            $this->errorMessages[]="Unable to get message with this id. An unknown error occurred.";
        }
        $cnx->close();
        return $messageTemplate;
    }
    /* Function mark as read/unread message by message id */
    public function markAsSeenMessage($id, $seen=1){
        /* Connecct to database */

        require_once BASE_CLASS . 'class-connect.php';
        $cnx = new Connect();
        $cnx->open();
        /* Initial variable */
        $log_id=$_SESSION['log_id'];
        if($seen==0){
            $message_status="Unread";
        }else{
            $message_status="Read";
        }
        //$user_role=$this->getUserRoleById($id);
        $sql="UPDATE messages SET seen='$seen' WHERE id='$id' AND receiver='$log_id'";
        $result=mysql_query($sql);
        if($result){
            $cnx->close();
            return true;
        }else{
            $this->errorMessages[]='Unable to mark message as $message_status! Please make sure that you are logged in and filled all the required fields.';
        }
        $cnx->close();
        return false;
    }
    /* Function get log in user role type by message id: return sender/receiver/undefined */
    public function getUserRoleById($id){
        /* Connecct to database */

        require_once BASE_CLASS . 'class-connect.php';
        $cnx = new Connect();
        $cnx->open();
        /* Initial variable */

        $role='';
        $log_id=$_SESSION['log_id'];
        $sql="SELECT sender, receiver FROM messsages WHERE id='$id'";
        //echo $sql;
        $result=mysql_query($sql);
        if($result){
            while($row=mysql_fetch_assoc($result)){
                if($row['sender']==$log_id)
                    $role= "sender";
                elseif($row['receiver']==$log_id)
                    $role= "receiver";
                else
                    $role= "undefined";
            }
        }else{
            $this->errorMessages[]='Unable to send message! Please make sure that you are logged in and filled all the required fields.';
        }
        $cnx->close();
        return $role;
    }
    public function loadAjaxPagination(){

        include(BASE_ROOT.'view/3rdparty/pagination-ajax/pagination.php');
        $links=array();
        $limit=array();
        $pagination_html=array();
        $total_number=$this->getTotalNumberMessages();

        /* PAGINATION PROCESS */
        $itemPerPage=$this->itemPerPage;
        /* INBOX, SENT, TRASH PAGINATION */
        $links['inbox'] = new Pagination ($total_number['inbox'], $itemPerPage);
        $links['sent'] = new Pagination ($total_number['sent'], $itemPerPage);
        $links['trash'] = new Pagination ($total_number['trash'], $itemPerPage);
        /* Get Limit sql query string*/
        $limit['inbox']=$links['inbox']->start_display;
        $limit['sent']=$links['sent']->start_display;
        $limit['trash']=$links['trash']->start_display;
        /* Pagination Html */
        $pagination_html['inbox']= $links['inbox']->display();
        $pagination_html['sent']= $links['sent']->display();
        $pagination_html['trash']= $links['trash']->display();
        /* Set them to members of class to use in other functions */
        $this->limit = $limit;
        $this->pagination_html=$pagination_html;
    }
    /*
        Function load number of message of all type
    */

    private function loadTotalNumberMessage(){
        $this->loadTotalNumberUnreadMessages();

        require_once BASE_CLASS . 'class-connect.php';
        $cnx = new Connect();
        $cnx->open();
        $log_id=$_SESSION['log_id'];
        $total_number=array();
        $total_number['inbox']=0;
        $total_number['sent']=0;
        $total_number['trash']=0;
        $sql = "SELECT
                    (SELECT COUNT(*) FROM messages WHERE receiver='$log_id' AND receiver_is_deleted='0') AS inbox_count,
                    (SELECT COUNT(*) FROM messages WHERE sender='$log_id' AND sender_is_deleted='0') AS sent_count,
                    (SELECT COUNT(*) FROM messages WHERE (sender='$log_id' AND sender_is_deleted='1') OR (receiver='$log_id' AND receiver_is_deleted='1')) AS trash_count
        ";

        $result=mysql_query($sql);

        if($result){
            while ($row=mysql_fetch_assoc($result)) {
                $total_number['inbox']=$row['inbox_count'];
                $total_number['sent']=$row['sent_count'];
                $total_number['trash']=$row['trash_count'];

                array_push($this->total_number, $total_number);
                $cnx->close();
                return true;
            }
        }else{
            return false;
        }
        $cnx->close();
    }
    /* Function create new message providing 3 parameters and 1 optional parameters*/
    public function composeMessage($receiver, $subject, $message, $product_id='', $sender=''){
        /* Connecct to database */

        require_once BASE_CLASS . 'class-connect.php';

        if(!$this->existUserId($receiver)){
            return;
        }
        $cnx = new Connect();
        $cnx->open();
        /* Initial variable */
        $subject=mysql_real_escape_string(stripslashes($subject));
        $message=mysql_real_escape_string(stripslashes($message));
        $product_id=mysql_real_escape_string(stripslashes($product_id));

        if(empty($sender)){
            $log_id=$_SESSION['log_id'];
        }else{
            $log_id=$sender;
        }
        $sql="INSERT INTO messages (sender, receiver, `subject`, `message`, `product_id`, `created_date`, `updated_date`)
            VALUES ('$log_id', '$receiver', '$subject', '$message', '$product_id', NOW(), NOW())
        ";

        $result=mysql_query($sql);
        if($result){
            $this->last_insert_msg_id=mysql_insert_id();
            $cnx->close();
            return true;
        }else{
            $this->errorMessages[]='Unable to send message! Please make sure that you are logged in and filled all the required fields.';
        }

        $cnx->close();
        return false;

    }

    /**
    *    Function attach files to message
    * @param $message_id: id of message that was inserted
    * @param $files: array element file
    * @param $allowedExts: set array of allowed extension leave it, will upload all file type
    * @return true/false
    */
    public function attachFiles($message_id, $files, $allowedExts=array()){
        /* Connect to database */

        require_once BASE_CLASS . 'class-connect.php';
        require_once BASE_CLASS . 'class-utilities.php';
        $cnx = new Connect();
        $cnx->open();
        /* Initial variable */
        $log_id=$_SESSION['log_id'];
        $arr_sql_value=array();
        $str_sql_value='';
        /* Diverse array files first so we can loop as simple way */
        $files=Utilities::diverse_array($files);
        /* Database process */
        $sql="INSERT INTO message_attachment (message_id, `encode_name`, `file_path`, `file_name`, `file_title`) VALUES ";
        /* Upload file each loop */
        foreach($files as $file){
            $file_title=$file['name'];
            /* explode to array temp to get extension */
            $temp = explode(".", $file["name"]);
            /* get extension eg. jpg or gif*/
            $extension = end($temp);
            /* set physical destination file name*/
            $destFileName=Utilities::generateStringByNow($file["name"].$message_id).".".$extension;
            /* set encoded name for security so user can't see physical file name */
            $encodeName=md5($destFileName);
            /* start upload file */
            if(Utilities::uploadAnyFile($file, $this->destPath, $destFileName, $allowedExts)){
                $arr_sql_value[]="('$message_id', '$encodeName', '$this->destPath', '$destFileName', '$file_title')";
            }
        }
        $str_sql_value=implode(", ", $arr_sql_value);
        $sql.=$str_sql_value;
        $result=mysql_query($sql);
        if($result){
            $cnx->close();
            return true;
        }else{
            $this->errorMessages[]='Unable to attach files to the message! An unknwon error occurred.';
        }

        $cnx->close();
        return false;

    }

    /* Function load all message from database to get method */
    public function loadAllMessages($type){
        /* Connect to database */

        require_once BASE_CLASS . 'class-connect.php';
        $cnx = new Connect();
        $cnx->open();
        /* Initial variable */
        $log_id=$_SESSION['log_id'];
        $where='WHERE 1';
        $allMessages=array();
        /* Get Message Type */
        $type=mysql_real_escape_string(stripslashes($type));
        /* Show by condition on message type */
        if($type=="inbox"){
            $where.=" AND ms.receiver='$log_id' AND ms.receiver_is_deleted='0' ORDER BY ms.created_date DESC";
        }elseif($type=="sent"){
            $where.=" AND ms.sender='$log_id' AND ms.sender_is_deleted='0' ORDER BY ms.created_date DESC";
        }elseif($type=="trash"){
            $where.=" AND ((ms.sender_is_deleted='1' AND ms.sender='$log_id')
                    OR (ms.receiver_is_deleted='1' AND ms.receiver='$log_id'))
                    ORDER BY ms.created_date DESC
                ";
        }else{
            $where.=" AND ((ms.sender='$log_id' AND ms.sender_is_deleted <= 1)
                    OR (ms.receiver='$log_id' AND ms.receiver_is_deleted <= 1))
                    ORDER BY CASE
                        WHEN ms.receiver='$log_id' THEN ms.receiver_del_date DESC
                        WHEN ms.sender='$log_id' THEN ms.sender_del_date DESC
                        ELSE ms.updated_date DESC
                    END
                ";
        }
        /* Process Query */
        $sql="SELECT `ms`.*, rg_sender.company_name as sender_company, rg_receiver.company_name as receiver_company FROM messages `ms`
            LEFT JOIN register rg_sender ON rg_sender.id=ms.sender
            LEFT JOIN register rg_receiver ON rg_receiver.id=ms.receiver
            $where {$this->limit[$type]}
        ";
        $result=mysql_query($sql);
        if($result){
            while($row=mysql_fetch_assoc($result)){
                array_push($allMessages, $row);
            }
        }else{
            $this->errorMessages[]="Unable to load messages from database. An unknown error occured.";
        }
        /* Return message to global variable */
        $this->allMessages=$allMessages;
        $cnx->close();
    }
    /* Function get message from database by message id */
    public function getMessageById($id, $arr_field=array()){
        /* Connect to database */

        require_once BASE_CLASS . 'class-connect.php';
        $cnx = new Connect();
        $cnx->open();
        /* Initial variable */
        $log_id=$_SESSION['log_id'];
        $where=" AND (messages.sender='$log_id' OR messages.receiver='$log_id') ";
        $message=array();

        $sql_field='';
        // /$log_id=$_SESSION['log_id'];
        if(count($arr_field)>0){
            $sql_field=implode(", ", $arr_field);
        }else{
            $sql_field="";
        }
        /* DATABASE PROCESS */
        $sql="SELECT messages.*, $sql_field FROM messages
            LEFT JOIN register receiver ON receiver.id=messages.receiver
            LEFT JOIN register sender ON sender.id=messages.sender
            WHERE messages.id='$id' $where
        ";

        $result=mysql_query($sql);
        if($result){
            while($row=mysql_fetch_assoc($result)){
                $message = $row;
                $cnx->close();
                return $message;
            }
            $this->errorMessages[]="Unable to get message with this id. Message with this id does not exist.";
        }else{
            $this->errorMessages[]="Unable to get message with this id. An unknown error occurred.";
        }
        $cnx->close();
    }
    /*
        Function: move message to Trash.
        Parameter: array message ID, '1' means soft delete; '2' means hard delete
        return: true if success
    */
    public function deleteMessages($array_id, $level='1'){
        /* Connecct to database */

        require_once BASE_CLASS . 'class-connect.php';
        $cnx = new Connect();
        $cnx->open();
        /* Initial variable */
        $log_id=$_SESSION['log_id'];
        $where ="";
        $ids = implode("', '", $array_id);
        /* Process Query */
        /* Update receiver del flag if the deleter is receiver */
        $sql="UPDATE `messages` SET ";
        $sql.="receiver_is_deleted = CASE ";
        foreach($array_id as $del_id){
            $del_id=mysql_real_escape_string(stripslashes($del_id));
            $sql.="WHEN `id`='{$del_id}' AND receiver='$log_id' THEN '$level' ";
        }
        $sql.= "ELSE receiver_is_deleted ";
        $sql .= "END, ";
        //receiver_deleted_date=NOW(),
        /* Update receiver del date if the deleter is receiver */
        $sql .= "receiver_del_date = CASE ";
        foreach($array_id as $del_id){
            $del_id=mysql_real_escape_string(stripslashes($del_id));
            $sql.="WHEN `id`='{$del_id}' AND receiver='$log_id' THEN NOW() ";
        }
        $sql.= "ELSE receiver_del_date ";
        $sql .= "END, ";
        /* Update sender del date if the deleter is sender */
        $sql .= "sender_del_date = CASE ";
        foreach($array_id as $del_id){
            $del_id=mysql_real_escape_string(stripslashes($del_id));
            $sql.="WHEN `id`='{$del_id}' AND sender='$log_id' THEN NOW() ";
        }
        $sql.= "ELSE sender_del_date ";
        $sql .= "END, ";
        /* Update sender del flag if the deleter is sender */
        $sql .= "sender_is_deleted = CASE ";
        foreach($array_id as $del_id){
            $del_id=mysql_real_escape_string(stripslashes($del_id));
            $sql.="WHEN `id`='{$del_id}' AND sender='$log_id' THEN '$level' ";
        }
        $sql.= "ELSE sender_is_deleted ";
        $sql .= "END ";
        $sql .= "WHERE id IN ('$ids')";
        $sql .= " AND (receiver='$log_id' OR sender='$log_id')";

        $result=mysql_query($sql);
        if($result){
            if(mysql_affected_rows()>0){
                $this->loadTotalNumberMessage();
                return true;
            }else{
                $this->errorMessages[]='There is no record(s) to delete at the moment.';
            }
        }else{
            $this->errorMessages[]='Unable to delete. Please try again later.';
        }
        $cnx->close();
    }

    /*
        Function: Check if the user with id exist
        Parameters: int user id
        Return: true if found
    */
    private function existUserId($id){
        /* Connecct to database */

        require_once BASE_CLASS . 'class-connect.php';
        $cnx = new Connect();
        $cnx->open();
        $sql="SELECT * FROM register WHERE id='$id'";
        $result=mysql_query($sql);
        if($result){
            if(mysql_num_rows($result)>0){
                return true;
            }else{
                $this->errorMessages[]='This recipient is not exist in our database.';
            }
        }else{
            $this->errorMessages[]='Unable to validate recipient! An unknown error occurred.';
        }
    }
    /*
        Function count number of unread message
    */
 
    public function loadTotalNumberUnreadMessages(){
 
        require_once BASE_CLASS . 'class-connect.php';
        $cnx = new Connect();
        $cnx->open();
        $log_id=$_SESSION['log_id'];
        $total_unread_number=0;
        $sql = "SELECT COUNT(*) as unread_count
            FROM messages
            WHERE receiver='$log_id' AND seen='0' AND receiver_is_deleted='0'
        ";

        $result=mysql_query($sql);

        if($result){
            while ($row=mysql_fetch_assoc($result)){
                $total_unread_number=$row['unread_count'];
                $this->total_unread_number=$total_unread_number;
                $cnx->close();
                return;
            }
        }
        $cnx->close();
        $this->total_unread_number=$total_unread_number;
        return;
    }
    /* function return array all messages */
    public function getAllMessages(){
        return $this->allMessages;
    }
    /* function return array error messages */
    public function getErrorMessages(){
        return $this->errorMessages;
    }
    /* function return total number of all messages with parameter type */
    public function getTotalNumberMessages(){
        return $this->total_number['0'];
    }
    /* function return total number of all messages with parameter type */
    public function getTotalNumberUnreadMessages(){
        return $this->total_unread_number;
    }
    /* function return array all messages */
    public function setItemPerPage($itemPerPage){
        $this->itemPerPage=$itemPerPage;
    }
    /* function return ajax pagination html by parameter type */
    public function getAjaxPaginationHtml($type){
        return $this->pagination_html[$type];
    }
}
?>

