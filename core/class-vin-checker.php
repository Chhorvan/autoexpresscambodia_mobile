<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
/**
 * class-vin-checker.php
 * Evaluates ether if a VIN is valid or not (Vehicle Identification Number).<br>
 * The math to calculate these are on Wiki. Also note: 11111111111111111 will<br>
 * validate against the procedure above. Straight-ones (seventeen consecutive<br>
 * '1's) will suffice the check-digit. This is because a value of one,<br>
 * multiplied against 89 (sum of weights), is still 89. And 89 % 11 is 1, the<br>
 * check digit. This is an easy way to test a VIN-check algorithm.<br>
 */
class VINChecker
{
    /**
     * Define the transliteration array
     * <br>---------------------------------------------------------------------
     * @var type array
     */
    public static $transliteration = array(
        'A'=>1, 'B'=>2, 'C'=>3, 'D'=>4, 'E'=>5, 'F'=>6, 'G'=>7, 'H'=>8, 
        'J'=>1, 'K'=>2, 'L'=>3, 'M'=>4, 'N'=>5, 'P'=>7, 'R'=>9,
        'S'=>2, 'T'=>3, 'U'=>4, 'V'=>5, 'W'=>6, 'X'=>7, 'Y'=>8, 'Z'=>9,
    );
    
    /**
     *
     * @var type Define weight array
     * <br>---------------------------------------------------------------------
     * @var type array
     */
    public static $weights = array(8,7,6,5,4,3,2,10,0,9,8,7,6,5,4,3,2);

    /**
     * Public static method: The checksum method is used to validate whether or 
     * not a VIN is valid. It will return an array with two keys: status and 
     * message:
     * The "status" will either be boolean TRUE or FALSE
     * The "message" will be a string describing the status
     * <br>---------------------------------------------------------------------
     * @param $vin The VIN of the vehicle
     * @return array
     */
    public static function checksum($vin)
    {
        $vin = strtoupper($vin);
        $length = strlen($vin);
        $sum = 0;

        if($length != 17)
        {
            return array('status'=>false, 'message'=>'VIN is not the right length');
        }

        for($x=0; $x<$length; $x++)
        {
            $char = substr($vin, $x, 1);

            if(is_numeric($char))
            {
                $sum += $char * self::$weights[$x];
            }
            else
            {
                if(!isset(self::$transliteration[$char]))
                {
                    return array('status'=>false, 'message'=>'VIN contains an invalid character.');
                }

                $sum += self::$transliteration[$char] * self::$weights[$x];
            }
        }

        $remainder = $sum % 11;
        $checkdigit = $remainder == 10 ? 'X' : $remainder;

        if(substr($vin, 8, 1) != $checkdigit)
        {
            return array('status'=>false, 'message'=>'The VIN is not valid.');
        }

        return array('status'=>true, 'message'=>'The VIN is valid.');
    }
}