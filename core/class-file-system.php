<?php
class FileSystem
{    
    public static function createDir($path){
        if (is_dir($path)) return true;
        $prev_path = substr($path, 0, strrpos($path, '/', -2) + 1 );
        return (static::createDir($prev_path) && is_writable($prev_path)) ? mkdir($path) : false;
    }
}
?>