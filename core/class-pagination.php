<?php
class Pagination{
    private $total_records;
    private $show_records;
    private $current_page_index;
    private $page_count;
    private $query_uri;
    private $class_name;
    private $target;
    private $page_slug;

    public function Pagination($total_records, $current_page, $show_records = 5, $page_slug = 'page'){
        $this->total_records = $total_records;
        $this->show_records = $show_records <= 0 ? 1 : $show_records;
        $this->page_slug = $page_slug;

        // Calculate number of page.
        $this->page_count = floor($this->total_records / $this->show_records);
        if(($this->total_records % $this->show_records) !== 0){
            $this->page_count++;
        }

        // Set current page index.
        $this->current_page_index = $this->total_records == 0 ? -1 : ($current_page <= 0 ? 0 : $current_page - 1);
        if($this->page_count < $this->current_page_index + 1){
            $this->current_page_index = $this->page_count;
        }
    }

    public function getStartIndex(){
        $start = $this->current_page_index * $this->show_records;
        return $start > 0 ? $start : 0;
    }

    public function getEndIndex(){
        $end = $this->getStartIndex() + $this->show_records;
        if($end > $this->total_records){ $end = $this->total_records; }
        return $end;
    }

    public function setCurrentPage($current_page){
        $this->current_page_index = $current_page - 1;
    }

    public function getCurrentPage(){
        return $this->current_page_index + 1;
    }

    public function setTotalRecord($total_records){
        $this->total_records = $total_records;
    }

    public function getTotalRecord(){
        return $this->total_records;
    }

    public function setNumberOfRecordPerPage($show_records){
        $this->show_records = $show_records;
    }

    public function getNumberOfRecordPerPage(){
        return $this->show_records;
    }

    public function getNumberOfPage(){
        return $this->page_count;
    }

    public function setQueryString($query_uri){
        $this->query_uri = trim($query_uri, '&');
    }

    public function getQueryString(){
        return $this->query_uri;
    }

    public function generateLink($class_name = 'pagination', $target = '_self', $bla = '...'){
        if($this->page_count == 1){
            return;
        }

        $this->class_name = $class_name;
        $this->target = $target;

        $pagination_string = '<div class="' . $this->class_name . '">' .
                                $this->first() .
                                $this->renderPrevious() .
                                $this->renderPagination($bla) .
                                $this->renderNext() .
                                $this->last() .
                                $this->setStyle($this->class_name) .
                              '</div>';

        return $pagination_string;
    }

    private function first($caption = 'First'){
        if($this->getCurrentPage() > 1){
            $str = trim($this->page_slug . '=1&'. $this->query_uri, '&');
            return '<a href="?'. $str .'" target="' . $this->target . '">'. $caption .'</a>';
        }

        return '';
    }

    private function last($caption = 'Last'){
        if($this->getCurrentPage() < $this->page_count){
            $str = trim($this->page_slug . '='. $this->page_count. '&'. $this->query_uri, '&');
            return '<a href="?'. $str .'" target="' . $this->target . '">'. $caption .'</a>';
        }

        return '';
    }

    private function renderPrevious($str_prev = '<<'){
        if($this->getCurrentPage() > 1){
            $str = trim($this->page_slug . '='. ($this->getCurrentPage()-1). '&'. $this->query_uri, '&');
            return '<a href="?'. $str .'" target="' . $this->target . '">'. $str_prev .'</a>';
        }

        return '';
    }

    private function renderNext($str_next = '>>'){
        if($this->getCurrentPage() < $this->page_count){
            $str = trim($this->page_slug . '='. ($this->getCurrentPage()+1). '&'. $this->query_uri, '&');
            return '<a href="?'. $str .'" target="' . $this->target . '">'. $str_next .'</a>';
        }

        return '';
    }

    private function renderPagination($bla,$total_link = 5 ,$mid_link = 3 ,$side_link = 1 )
    {
        $pagination_string = '';

        if($this->page_count > $total_link){
            if($this->current_page_index < $mid_link - 1){
                for($i=0; $i<$mid_link; $i++){
                    if($this->current_page_index === $i) {
                        $pagination_string .= '<span>'. ($i+1) .'</span>';
                    }
                    else{
                        $str = trim($this->page_slug . '='. ($i+1). '&'. $this->query_uri, '&');
                        $pagination_string .= '<a href="?'. $str .'" target="' . $this->target . '">'. ($i+1) .'</a>';
                    }
                }
                $pagination_string .= $bla;
                for($i=$side_link; $i>0; $i--){
                    $page = $this->page_count - $i + 1;
                    $str = trim($this->page_slug . '='. $page. '&'. $this->query_uri, '&');
                    $pagination_string .= '<a href="?'. $str .'" target="' . $this->target . '">'. $page .'</a>';
                }
            }
            elseif($this->current_page_index >= ($this->page_count - $mid_link) + 1){
                for($i=0; $i<$side_link; $i++){
                    $str = trim($this->page_slug . '='. ($i+1). '&'. $this->query_uri, '&');
                    $pagination_string .= '<a href="?'. $str .'" target="' . $this->target . '">'. ($i+1) .'</a>';
                }
                $pagination_string .= $bla;
                for($i=$mid_link; $i>0; $i--){
                    $page = $this->page_count - $i + 1;
                    if($this->current_page_index == $page - 1) {
                        $pagination_string .= '<span>'. $page .'</span>';
                    }
                    else{
                        $str = trim($this->page_slug . '='. $page. '&'. $this->query_uri, '&');
                        $pagination_string .= '<a href="?'. $str .'" target="' . $this->target . '">'. $page .'</a>';
                    }
                }
            }
            else{
                $current_page = $this->current_page_index + 1;
                if($mid_link % 2){
                    $left = ($mid_link - 1) / 2;
                    $right = $left;
                }
                else{
                    $mid_page = round($this->page_count / 2);
                    if($current_page < $mid_page){
                        $left = ($mid_link / 2) - 1;
                        $right = $mid_link - $left - 1;
                    }
                    else{
                        $right = ($mid_link / 2) - 1;
                        $left = $mid_link - $right - 1;
                    }
                }

                for($i=0; $i<$side_link; $i++){
                    $str = trim($this->page_slug . '='. ($i+1). '&'. $this->query_uri, '&');
                    $pagination_string .= '<a href="?'. $str .'" target="' . $this->target . '">'. ($i+1) .'</a>';
                }
                $pagination_string .= ($i+1 == $current_page-$left ? '' : $bla);
                for($i=$current_page-$left; $i<=$current_page + $right; $i++){
                    if($i == $current_page){
                        $pagination_string .= '<span>'. $i .'</span>';
                    }
                    else{
                        $str = trim($this->page_slug . '='. $i. '&'. $this->query_uri, '&');
                        $pagination_string .= '<a href="?'. $str .'" target="' . $this->target . '">'. $i .'</a>';
                    }
                }
                $pagination_string .= ($i == $this->page_count - $side_link + 1 ? '' : $bla);
                for($i=$side_link; $i>0; $i--){
                    $page = $this->page_count - $i + 1;
                    $str = trim($this->page_slug . '='. $page. '&'. $this->query_uri, '&');
                    $pagination_string .= '<a href="?'. $str .'" target="' . $this->target . '">'. $page .'</a>';
                }
            }
        }
        else{
             for($i=0; $i<$this->page_count; $i++){
                if($i == $this->current_page_index){
                    $pagination_string .= '<span>'. ($i+1) .'</span>';
                }
                else{
                    $str = trim($this->page_slug . '='. ($i+1). '&'. $this->query_uri, '&');
                    $pagination_string .= '<a href="?'. $str .'" target="' . $this->target . '">'. ($i+1) .'</a>';
                }
             }
        }


        return $pagination_string;
    }

    private function setStyle(){
        return   "<style type='text/css'>
                    ." . $this->class_name . "{
                        text-align: center !important;
                    }

                    ." . $this->class_name . " a{
                        font-family: Arial, Helvetica, Sans-Serif !important;
                        font-size: 12px !important;
                        cursor: pointer !important;
                        text-decoration: none !important;
                        color: #00688B !important;
                        border: 1px solid #00688B !important;
                        margin: 0 3px !important;
                        padding: 5px 10px !important;
                    }

                    ." . $this->class_name . " a:hover {
                        background-color: blue !important;
                        color: white !important;
                    }

                    ." . $this->class_name . " span{
                        font-family: Arial, Helvetica, Sans-Serif !important;
                        font-size: 12px !important;
                        font-weight: bold !important;
                        color: #00688B !important;
                        border: 1px solid #00688B !important;
                        background-color: #E0EEEE !important;
                        margin: 0 3px !important;
                        padding: 5px 10px !important;
                    }
                </style>";
    }
}
?>
