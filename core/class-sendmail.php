<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
/*
 * Send email using pre-formatted template. 
 */
class SendEmail
{
    protected $template_data;
    protected $has_data;
    protected $to;
    protected $subject;
    protected $message;
    
    /*
     * Constructor -------------------------------------------------------------
     */
    public function SendEmail()
    {
        // load template source.
        $path = BASE_ROOT . 'view/email/email-confirm.html';
        
        $this->template_data = '';
        $this->has_data = false;
        
        if(file_exists($path) )
        {
            if( $fo = @fopen($path, 'r') )
            {
                if( $this->template_data = @fread($fo, filesize($path)) )
                {
                    $this->has_data = true;
                }
                
                @fclose($fo);
            }
        }
    }
    
    /*
     * Public method: send email -----------------------------------------------
     * @return bool.
     */
    public function send()
    {
        if( empty($this->subject) || empty($this->message) || empty($this->to) )
        {
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Email not send. Required parameters Subject, Message and Email not set. Error thrown at ' . __FILE__ . ':' . __LINE__ .'.');
            
            return false;
        }
        else
        {
            $email_body = '';
            
            // if template exists, use it.
            if( !empty($this->template_data) )
            {
                $email_body = '<html><head><title>Angkorauto</title></head><body>';
                $email_body .= str_replace('[:::MESSAGE:::]', $this->message, $this->template_data);
                $email_body .= '</body></html>';
            }
            // no template loaded.
            else
            {
                $email_body = '<html><head><title>Angkorauto</title></head><body>';
                $email_body .= $this->message;
                $email_body .= '</body></html>';
            }
            
            // set email headers.  
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";          
          
            $headers .= 'From:Angkorauto.com<noreply@angkorauto.com>' . "\r\n";
            $headers .= 'X-Mailer:Welcome to Angkorauto.com' ;
            
            if(!mail($this->to, $this->subject, $email_body, $headers))
            {
                require_once BASE_CLASS . 'class-log.php';
                LogReport::write('Unable to send activation email to ['.$this->to.']. Error thrown at ' . __FILE__ . ':' . __LINE__ .'.');

                return false;
            }
            else
            {
                return true;
            }
        }
    }
    
    /*
     * Set message -------------------------------------------------------------
     * @param $message - the message to be sent.
     * @return void.
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }
    
    /*
     * Set subject -------------------------------------------------------------
     * @param $subject - the subject string.
     * @return void.
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }
    
    /*
     * Set email destination ---------------------------------------------------
     * @param $email - email address to be sent.
     * @return void.
     */
    public function setEmail($email)
    {
        $this->to = $email;
    }
}
