<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
/*
 * Loads setting values --------------------------------------------------------
 */
if(!isset($_SESSION)) @session_start();

class Setting
{
    protected $language;
    protected $language_iso;
    protected $fwname;
    protected $analytics;
    protected $meta_favicon;
    protected $meta_canonic;
    protected $meta_copyrights;
    protected $admin_email;
    protected $admin_pass;
    protected $enable_top_header;
    protected $enable_banner_header;
    protected $enable_menu_header;
    protected $enable_foot_header;
    protected $default_home_slug;
    protected $allow_reservation;
    protected $live_chat_enabled;
    protected $theme;
    protected $allow_social = false;
    protected $currency_option_list;

    /*
     * Constructor -------------------------------------------------------------
     * - load settings from database.
     */
    public function Setting()
    {
        require_once BASE_CLASS . 'class-connect.php';
        
        $db = new Connect();
        $db->open();
        
        $sql = mysql_query("SELECT * FROM `setting` LIMIT 1;");
        
        if( !$sql )
        {
            require_once BASE_CLASS . 'class-log.php';
            
            LogReport::write('Unable to load settings at ' . __FILE__.':'.__LINE__.'. ' . mysql_error());
            LogReport::fatalError('Unable to load page due an internal error. Please notify the system administrator.');
        }
        
        // setting table should not contain more than 1 entry.
        if( @mysql_num_rows($sql) != 1 )
        {
            $db->close();
            
            require_once BASE_CLASS . 'class-log.php';
            
            LogReport::write('Unable to load settings at ' . __FILE__.':'.__LINE__.'. The framework has not been set yet. Setting table has no values.');
            LogReport::fatalError('Unable to load page due an internal error. Please notify the system administrator.');
        }
        
        $r = @mysql_fetch_assoc($sql);
        
        @mysql_free_result($sql);
        $db->close();
        
        $this->language = stripslashes($r['language']);
        $this->language_iso = $r['language_iso'];
        $this->fwname = stripslashes($r['fwname']);
        $this->analytics = stripslashes($r['analytics']);
        $this->meta_favicon = $r['meta_favicon'];
        $this->meta_copyrights = stripslashes($r['meta_copyrights']);
        $this->meta_canonic = $r['meta_canonic'];
        $this->admin_email = $r['admin_email'];
        $this->admin_pass = $r['admin_pass'];
        $this->enable_banner_header = (bool)$r['enable_top_header'];
        $this->enable_foot_header = (bool)$r['enable_foot_header'];
        $this->enable_menu_header = (bool)$r['enable_menu_header'];
        $this->enable_top_header = (bool)$r['enable_foot_header'];
        $this->default_home_slug = $r['default_home_slug'];
        $this->allow_reservation = (bool)$r['allow_car_reservation'];
        $this->live_chat_enabled = (bool)$r['live_chat'];
        $this->theme = stripslashes($r['default_theme']);
        $this->allow_social = (bool)$r['allow_social'];
        
        define('CURRENCY_SYMBOL', $r['currency_symbol']);
        define('CURRENCY_CODE', $r['currency_code']);
        
        $_SESSION['CURRENCY_SYMBOL'] = CURRENCY_SYMBOL;
        $_SESSION['CURRENCY_CODE'] = CURRENCY_CODE;
        $_SESSION['CURRENT_THEME'] = $this->theme;
        
        // set language session if not yet initialized.
        if( !isset($_SESSION['log_language']) )
        {
            $_SESSION['log_language'] = "English";
            $_SESSION['log_language_iso'] = "en";
        }
        else
        {
            $this->language = $_SESSION['log_language'];
            $this->language_iso = $_SESSION['log_language_iso'];
        }
        
        if( isset($_GET['lang']) )
        {
            $l = trim($_GET['lang']);
            $l = strtolower($l);
            $l = addslashes($l);
            
            if( file_exists(BASE_ROOT . 'language/' . $l . '/global.php') )
            {
                require_once BASE_CLASS . 'class-language.php';
                
                $lang = new LanguageFormat();
                
                $fl = $lang->getLanguageNameByISO($l);
                
                $_SESSION['log_language_iso'] = $l;
                $_SESSION['log_language'] = $fl;
            }
        }
    }
	
	
    
    /**
     * Public method: get social network status
     * <br>---------------------------------------------------------------------
     * @return bool
     */
    public function getSocialStatus(){
        return $this->allow_social;
    }
    
    /**
     * Public getter: get theme name
     * <br>---------------------------------------------------------------------
     * @return string
     */
    public function getThemeDir(){
        return $this->theme;
    }
    
    /**
     * Public getter: get live chat status
     * <br>---------------------------------------------------------------------
     * @return boolean
     */
    public function getLiveChatStatus(){
        if( $_SESSION['log_group'] == 'admin' ){
            return false;
        }
        
        return $this->live_chat_enabled;
    }
    
    /*
     * Get default language ----------------------------------------------------
     * @return $string.
     */
    public function getDefaultLanguage()
    {
        return $this->language;
    }
    
    /*
     * Get allow car reservation -----------------------------------------------
     * @return $string.
     */
    public function getAllowReservation()
    {
        return $this->allow_reservation;
    }
    
    /*
     * Get language ISO value --------------------------------------------------
     * @return string.
     */
    public function getLanguageISO()
    {
        return $this->language_iso;
    }
    
    /*
     * Get framework name ------------------------------------------------------
     * @return string.
     */
    public function getFrameworkName()
    {
        return $this->fwname;
    }
    
    /*
     * Get analytics code ------------------------------------------------------
     * @return string.
     */
    public function getAnalytics()
    {
        return stripslashes($this->analytics);
    }
    
    /*
     * Get meta copyrights -----------------------------------------------------
     * @return string.
     */
    public function getMetaCopyrights()
    {
        return $this->meta_copyrights;
    }
    
    /*
     * Get meta canonic --------------------------------------------------------
     * @return string.
     */
    public function getMetaCanonic()
    {
        return $this->meta_canonic;
    }
    
    /*
     * Get administrator's email -----------------------------------------------
     * @return string.
     */
    public function getAdminEmail()
    {
        return $this->admin_email;
    }
    
    /*
     * Get admin password ------------------------------------------------------
     */
    public function getAdminPassword()
    {
        return $this->admin_pass;
    }
    
    /*
     * Get favicon url ---------------------------------------------------------
     * @return string.
     */
    public function getFaviconURL()
    {
        return $this->meta_favicon;
    }
    
    /*
     * Get top menu enabled status ---------------------------------------------
     * @return bool.
     */
    public function getTopHeaderStatus()
    {
        return $this->enable_top_header;
    }
    
    /*
     * Get menu enabled status -------------------------------------------------
     * @return bool.
     */
    public function getMenuHeaderStatus()
    {
        return $this->enable_menu_header;
    }
    
    /*
     * Get banner enabled status -----------------------------------------------
     * @return bool.
     */
    public function getBannerHeaderStatus()
    {
        return $this->enable_banner_header;
    }
    
    /*
     * Get foot enabled status -------------------------------------------------
     * @return bool.
     */
    public function getFootHeaderStatus()
    {
        return $this->enable_foot_header;
    }
    
    /*
     * Get default home slug ---------------------------------------------------
     * @return string.
     */
    public function getHomeSlug()
    {
        return $this->default_home_slug;
    }
    
}