<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
/**
 * Converts language name i ISO format. 
 */
class LanguageFormat 
{
    /*
     * Country / ISO array.
     */
    protected $country_array = array(
        'Afrikaans' => 'af',
        'Albanian' => 'sq',
        'Arabic' => 'ar',
        'Bulgarian' => 'bg',
        'Chinese' => 'zh',
        'Croatian' => 'hr',
        'Czech' => 'cs',
        'Danish' => 'da',
        'Dutch' => 'nl',
        'English' => 'en',
		'Khmer' => 'kh',
        'Esperanto' => 'eo',
        'Estonian' => 'et',
        'Fiji' => 'fj',
        'French' => 'fr',
        'Georgian' => 'ka',
        'German' => 'de',
        'Greek' => 'el',
        'Hebrew' => 'he',
        'Hindi' => 'hi',
        'Hungarian' => 'hu',
        'Irish' => 'ga',
        'Italian' => 'it',
        'Japanese' => 'ja',
        'Kannada' => 'kn',
        'Korean' => 'ko',
        'Latin' => 'la',
        'Lithuanian' => 'lt',
        'Macedonian' => 'mk',
        'Norwegian' => 'no',
        'Polish' => 'pl',
        'Portuguese' => 'pt',
        'Portuguese' => 'br',
        'Romanian' => 'ro',
        'Russian' => 'ru',
        'Serbian' => 'sr',
        'Serbo-Croatian' => 'sh',
        'Slovak' => 'sk',
        'Slovenian' => 'sl',
        'Spanish' => 'es',
        'Swedish' => 'sv',
        'Tagalog' => 'tl',
        'Thai' => 'th',
        'Turkish' => 'tr',
        'Ukrainian' => 'uk'
    );
    protected $installer_language_list;
    
    /*
     * Constructor -------------------------------------------------------------
     */
    public function LanguageFormat()
    {
        // load installed language packs list.
        $this->loadInstalledLanguagePacks();
    }
    
    /*
     * Protected method: load installed language packs -------------------------
     * @return void.
     */
    protected function loadInstalledLanguagePacks()
    {
        $path = BASE_ROOT . 'language/';
        
        if( !$dir = @opendir($path) )
        {
            require_once BASE_CLASS . 'class-log.php';
            
            LogReport::write('Unable to open directory opendir() at ' . __FILE__ . ':' . __LINE__ . '. Probable cause: access denied.');
            
            $this->installer_language_list = array('english');
        }
        else
        {
            $this->installer_language_list = array();
            
            while( ($file = @readdir($dir)) !== false )
            {
                if( $file != '.' && $file != '..' && $file != '.htaccess' )
                {
                    array_push($this->installer_language_list, $file);
                }
            }
            
            @closedir($dir);
        }
    }

    /*
     * Public method: get installed language packs -----------------------------
     * @return array.
     */
    public function getInstalledLanguagePacks()
    {
        return $this->installer_language_list;
    }
    
    /*
     * Public method: get language ISO by language string ----------------------
     * @param $country - the language string (in english)
     * @return string. (iso-639-1)
     */
    public function getLanguageISOByName($language)
    {
        if( empty($language) )
        {
            return 'en'; // default english if country is empty.
        }
        
        $result = $this->country_array[ucfirst($language)];
        
        if( empty($result) )
        {
            $result = 'en';
        }
        
        return $result;
    }
    
    /*
     * Public method: get language name by ISO ---------------------------------
     * @param $iso - ISO code for the language.
     * @return string - language name.
     */
    public function getLanguageNameByISO($iso)
    {
        $result = 'English';
        
        foreach( $this->country_array as $k => $v )
        {
            if( $v == $iso )
            {
                $result = $k;
                break;
            }
        }
        
        return $result;
    }
    
    /*
     * Public method: get country list -----------------------------------------
     * @return array.
     */
    public function getCountryList()
    {
        $country = array();
        $arr = $this->country_array;
        
        foreach( $arr as $k => $value )
        {
            array_push($country, $k);
        }
        
        return $country;
    }
    
}

