<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
/*
 * Send message
 */

if(!isset($_SESSION)) session_start();

require_once BASE_ROOT.'config.php';
class User
{

    protected $errorMessages=array();
    protected $total_number=array();
    
    protected $pagination_html;
    protected $limit=array();
    protected $itemPerPage=20;
    protected $id;
    protected $profile;
    /*
     * Constructor -------------------------------------------------------------
     */
    public function __construct(){ 
       
    }
    
    /* Load Single Profile */
    public function loadProfile($id, $arr_field=array()){
        require_once BASE_CLASS . 'class-connect.php';

        $profile =array();
        $cnx = new Connect();
        $cnx->open();
        $sql_field='';
        $this->id=$id;
        // /$log_id=$_SESSION['log_id'];
        if(count($arr_field)>0){
            $sql_field=implode(", ", $arr_field);
        }else{
            $sql_field="*";
        }

        $sql="SELECT $sql_field FROM register 
            LEFT JOIN country_list ON country_list.cc = register.country
            LEFT JOIN register_types ON register.register_type=register_types.code
            WHERE register.id='$id' LIMIT 1";
        $result=mysql_query($sql);
        
        if($result){
            while($row=mysql_fetch_assoc($result)){
                $profile= $row;
            }
        }else{
             $this->errorMessages[]="Unable to get user with this id. Unknown Error occurred.";
        }
        $cnx->close();
        $this->profile=$profile;
    }
    /* get Primary photo thumb full url */
    public function getPrimaryThumbUrl(){
        
        require_once BASE_CLASS . 'class-connect.php';
       
        $cnx = new Connect();
        $cnx->open();
        $photo_url='';
        $sql="SELECT mi.m_thumb, mi.m_image FROM more_image mi WHERE owner='$this->id' ORDER BY primary_photo DESC LIMIT 1";
        $result=mysql_query($sql);
        if($result){
            while($row=mysql_fetch_assoc($result)){
                $photo_url= BASE_SITE.$row['m_thumb'];
            }
        }else{
             $this->errorMessages[]="Unable to get primary thumb url with this id. Unknown Error occurred.";
        }
        
        $cnx->close();
        return $photo_url;
    }
    /* get profile detail full url */
    public function getProfileLink(){
        return BASE_SITE."company-detail?id=".$this->id;
    }
    /* get country flag full url */
    public function getFlagUrl($cc){
        return BASE_SITE."images/flag/$cc.png";
    }
    /* function return array error messages */
    public function getErrorMessages(){
        return $this->errorMessages;
    }
    /* Get array profile */
    public function getProfile(){
        return $this->profile;
    }
}
?>

