<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
/*
 * Handles feedback messages and display 'em in Twitter messaging style.
 * This class is not meant to be initialized!!!
 * Usage: FeedbackMessage::display($message, $type);
 * -----------------------------------------------------------------------------
 * This class makes use of the [view/js/global.js] file!!!
 * USE THE PROPER METHOD TO DISPLAY ALERT FEEDBACK BLOCKS!!!
 */
class FeedbackMessage
{
    /*
     * Display feedback message in Twitter style -------------------------------
     * Use displayOnPage() on the page itself. Do not use this on controllers!
     * @param $message - the message to be displayed.
     * @param $type - the style type (error | success)
     * @return void.
     */
    public static function displayOnPage($message, $type='error')
    {
        if( empty($message) )
        {
            return;
        }
        
        switch($type)
        {
            case 'error':
                $style = 'feedback_error';
                break;
            case 'success':
                $style = 'feedback_success';
                break;
            default: $style = '';
        }
        
        if( empty($style) )
        {
            return;
        }
        
        $result = '         <div id="feed_return_block" class="'.$style.'">'.$message.'</div>' . "\r\n";
        
        echo $result;
    }
}
