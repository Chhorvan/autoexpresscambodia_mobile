<?php
#########################################################################
### JT_VIDGEN 
#########################################################################
( isset($_POST['vid']) ? $id = trim($_POST['vid']) : $id = '' );

if( empty($id) ){
	die('');
}

require_once '../config.php';
require_once BASE_CLASS .'class-connect.php';

$cnx = new Connect();
$cnx->open();

$id = @mysql_real_escape_string($id);

if( !$sql = @mysql_query("SELECT * FROM `car_media` WHERE `car_id`='$id' AND (`mode`='interior' OR `mode`='exterior')") ){
	$cnx->close();
	die('Unable to load media. Video could not be loaded. Error AJ20');
}

if( @mysql_num_rows($sql) < 1 ){
	$cnx->close();
	die('There is no video available for this vehicle.');
}

$url = '';

while( $r = @mysql_fetch_assoc($sql) ){
	$url .= $r['source'] . ',';
}

@mysql_free_result($sql);
$cnx->close();

// remove last comma.
$url = substr($url, 0, strlen($url)-1);

die($url);
