<?php
if(!isset($_SESSION)) @session_start();
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################

$carId = trim($_POST['car']);

if( $_SESSION['log_group'] == 'public' || !isset($_SESSION['log_id']) ){
    echo 'login';
    exit;
}
else {
    $uid = $_SESSION['log_id'];
    
    // validate ids.
    if( empty($carId) || $uid < 1 ){
        echo 'error';
        exit;
    }
    
    // save car to favorite list.
    else {
        
        require_once dirname(dirname(__FILE__)) . '/config.php';
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        // check if car is already on the list.
        if( !$sql = @mysql_query("SELECT `user_id`,`car_id` FROM `user_favorites` WHERE `user_id`='$uid' AND `car_id`='$carId' LIMIT 1;") ){
            require_once BASE_CLASS . 'class-log.php';
            
            LogReport::write('Unable to validate favorite list table due a query error at ' . __FILE__ . ':' . __LINE__);
            
            $cnx->close();
            
            echo 'error';
            exit;
        }
        
        if( @mysql_num_rows($sql) > 0 ){
            $cnx->close();
            
            echo 'ok';
            exit;
        }
        
        if( !@mysql_query("INSERT INTO `user_favorites` (`user_id`,`car_id`,`date`) VALUES('$uid','$carId','".date('Y-m-d')."')") ){
            require_once BASE_CLASS . 'class-log.php';
            
            LogReport::write('Unable to save to favorite list table due a query error at ' . __FILE__ . ':' . __LINE__);
            
            $cnx->close();
            
            echo 'error';
            exit;
        }
        
        $cnx->close();
        
        echo 'ok';
        exit;
    }
}
