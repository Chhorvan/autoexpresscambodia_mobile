<?php
header('Content-Type: application/json');

require_once dirname(dirname(__FILE__)) . '/config.php';
require_once BASE_CLASS . 'class-connect.php';
require_once BASE_CLASS . 'class-file-system.php';

$cnx = new Connect();
$cnx->open();
$tmp_ext = '.tmp';	

if(isset($_FILES['upload_attachment_file'])){
	$dir = dirname(dirname(__FILE__)) . '/' . $_POST['location'];
	FileSystem::createDir($dir);
	$file_list = $_FILES['upload_attachment_file'];

	foreach($file_list['name'] as $key => $file_name){
		$tmp = $file_list['tmp_name'][$key];
		$explode_file = explode('.', $file_name);
		$extension = end($explode_file);
		array_pop($explode_file);
		$file_name = implode('.', $explode_file);	
		$string_date = $_POST['date_string'];

		if(move_uploaded_file($tmp, $dir. $file_name. $string_date. $tmp_ext) === true){
			$response[] = array(
				'status' => 'Successfully upload',
				'file_index' => $_POST['index']
			);
		}
		else{    
			$response[] = array(
				'status' => 'failed upload',
				'file_index' => -1
			);
		}
	}

	echo json_encode($response);
}
elseif(isset($_POST['del_attachment_file'])){
	$dir = dirname(dirname(__FILE__)) . '/' . $_POST['del_attachment_file']['location'];
	FileSystem::createDir($dir);
	$file_name = $_POST['del_attachment_file']['file_name'];
	$string_date = $_POST['del_attachment_file']['date_string'];

	$full_file_path = $dir. $file_name. $string_date. $tmp_ext;
	if(file_exists($full_file_path))
	{
		unlink($full_file_path);
		echo json_encode(array('status' => 1));
	}
	else
	{
		echo json_encode(array('status' => 0));
	}
}
$cnx->close();