<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
$maker = trim($_POST['maker_filter']);

if( !isset($maker) ){
    exit;
}

require_once dirname(dirname(__FILE__)) . '/config.php';
require_once BASE_CLASS . 'class-connect.php';

$cnx = new Connect();
$cnx->open();

if( !$sql = mysql_query("SELECT `maker`,`model`,`status` FROM `car` WHERE `maker`='$maker' AND `status`='0'") ){
    require_once BASE_CLASS . 'class-log.php';
    LogReport::write('Unable to load car model list due a query error at ' . __FILE__ . ':' . __LINE__);
    
    $cnx->close();
    echo '';
    exit;
}

if( @mysql_num_rows($sql) < 1 ){
    $cnx->close();
    echo '';
    exit;
}

$model_arr = array();

while( $r = @mysql_fetch_assoc($sql) ){
    array_push($model_arr, $r['model']);
}

@mysql_free_result($sql);
$cnx->close();

$model_arr = array_unique($model_arr);
$model_arr = array_values($model_arr);
sort($model_arr);

$result = '';

for( $i=0; $i < count($model_arr); $i++ ){
    $result .= $model_arr[$i];
    
    if( ($i+1) < count($model_arr) ){
        $result .= ',';
    }
}

echo $result;
exit;