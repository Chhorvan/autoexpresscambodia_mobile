<?php
if(!isset($_SESSION)) @session_start();
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################

// retrieve var.
$cid = trim($_GET['cid']);

// connect to database.
require_once dirname(dirname(__FILE__)) . '/config.php';
require_once BASE_CLASS . 'class-connect.php';

$cnx = new Connect();
$cnx->open();

$cid = str_replace(stripslashes(BASE_ROOT), '', $cid);
$cid = @mysql_real_escape_string($cid);

if( !@mysql_query("DELETE FROM `product` WHERE `id`='$cid' LIMIT 1;") ){
    require_once BASE_CLASS . 'class-log.php';
    LogReport::write('Unable to remove car image due a query error at ' . __FILE__ . ':' . __LINE__);
    $cnx->close();
    
    die('ng,Unable to remove product due a query error.');
}

$cnx->close();

// remove the actual image.
//if( file_exists(BASE_ROOT . $url) ){
//    @unlink(BASE_ROOT . $url);
//}

//die('ok,File removed successfully!');
