<?php
if(!isset($_SESSION)) @session_start();
/*
 * Updates the selected grid type into session.
 */
( isset($_REQUEST['sess']) ? $list_type = trim($_REQUEST['sess']) : $list_type = 'list' );

switch ( $list_type ){
    case 'list':
    case 'thumb':
        break;
    default:
        $list_type = 'list';
}

$_SESSION['grid_type'] = $list_type;
die($list_type);