<?php
	require_once dirname(dirname(__FILE__)) . '/config.php';
	require_once BASE_CLASS . 'class-connect.php';
	
	$engine1_from = '1.1';
	$engine1_to = '1.5';	
	$engine2_from = '1.6';
	$engine2_to = '2.0';
	$engine3_from = '2.1';
	$engine3_to = '3.0';
	$engine4_from = '3.1';
	$engine4_to = '4.0';
	$engine5_value = '4.1up';
	
	function getengine_volume1(){        
        $cnx = new Connect();
        $cnx->pdoOpen();        
        $product_type='Car';
        $engine_volume=array();
		
		// Engine Size
		global	$engine1_from;
		global	$engine1_to;		
		
        if(!empty($product_type)){            			
			$where = "WHERE pd.engine != '0' AND pd.product_type = '$product_type' AND pd.engine >= $engine1_from AND pd.engine <= $engine1_to";
        }

        $sql=" SELECT pd.engine, pd.product_type, COUNT(pd.engine) as num1
            from active_product as pd
			INNER JOIN register as rg on pd.owner=rg.user_id	
                $where                
            ";
			
        $cnx->pdoExecuteQuery($sql);
        $engine_volume=$cnx->getRecords();
        @mysql_free_result($sql);
        $cnx->close();
        return $engine_volume;
    }
	
	function getengine_volume2(){        
        $cnx = new Connect();
        $cnx->pdoOpen();        
        $product_type='Car';
        $engine_volume=array();
		
		// Engine Size
		global $engine2_from;
		global $engine2_to;
		
        if(!empty($product_type)){            						
			$where = "WHERE pd.engine != '0' AND pd.product_type = '$product_type' AND pd.engine >= $engine2_from AND pd.engine <= $engine2_to";
        }

        $sql=" SELECT pd.engine, pd.product_type, COUNT(pd.engine) as num2
            from active_product as pd
			INNER JOIN register as rg on pd.owner=rg.user_id	
                $where                
            ";
			
        $cnx->pdoExecuteQuery($sql);
        $engine_volume=$cnx->getRecords();
        @mysql_free_result($sql);
        $cnx->close();
        return $engine_volume;
    }
	
	function getengine_volume3(){        
        $cnx = new Connect();
        $cnx->pdoOpen();        
        $product_type='Car';
        $engine_volume=array();
		
		// Engine Size
		global $engine3_from;
		global $engine3_to;
		
        if(!empty($product_type)){            						
			$where = "WHERE pd.engine != '0' AND pd.product_type = '$product_type' AND pd.engine >= $engine3_from AND pd.engine <= $engine3_to";
        }

        $sql=" SELECT pd.engine, pd.product_type, COUNT(pd.engine) as num3
            from active_product as pd
			INNER JOIN register as rg on pd.owner=rg.user_id	
                $where                
            ";
			
        $cnx->pdoExecuteQuery($sql);
        $engine_volume=$cnx->getRecords();
        @mysql_free_result($sql);
        $cnx->close();
        return $engine_volume;
    }
	
	function getengine_volume4(){        
        $cnx = new Connect();
        $cnx->pdoOpen();        
        $product_type='Car';
        $engine_volume=array();
		
		// Engine Size
		global $engine4_from;
		global $engine4_to;
		
        if(!empty($product_type)){            						
			$where = "WHERE pd.engine != '0' AND pd.product_type = '$product_type' AND pd.engine >= $engine4_from AND pd.engine <= $engine4_to";
        }

        $sql=" SELECT pd.engine, pd.product_type, COUNT(pd.engine) as num4
            from active_product as pd
			INNER JOIN register as rg on pd.owner=rg.user_id	
                $where                
            ";
			
        $cnx->pdoExecuteQuery($sql);
        $engine_volume=$cnx->getRecords();
        @mysql_free_result($sql);
        $cnx->close();
        return $engine_volume;
    }
	
	function getengine_volume5(){        
        $cnx = new Connect();
        $cnx->pdoOpen();        
        $product_type='Car';
        $engine_volume=array();
		
		// Engine Size
		global $engine5_value;
		
        if(!empty($product_type)){            						
			$where = "WHERE pd.engine != '0' AND pd.product_type = '$product_type' AND pd.engine = '$engine5_value'";
        }

        $sql=" SELECT pd.engine, pd.product_type, COUNT(pd.engine) as num5
            from active_product as pd
			INNER JOIN register as rg on pd.owner=rg.user_id	
                $where                
            ";
			
        $cnx->pdoExecuteQuery($sql);
        $engine_volume=$cnx->getRecords();
        @mysql_free_result($sql);
        $cnx->close();
        return $engine_volume;
    }
	getengine_volume1();
	getengine_volume2();
	getengine_volume3();
	getengine_volume4();
	getengine_volume5();
?>
