<?php
if(!isset($_SESSION)) @session_start();
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - Joel DeBolt (for the suggestion of random vehicles)
################################################################################
if( !isset($_SESSION['log_group']) || empty($_POST['current_code']) || empty($_POST['currency_symbol']) ){
    die('ng?Unable to initialize session.');
}

require_once dirname(dirname(__FILE__)) . '/config.php';
require_once BASE_CLASS . 'class-connect.php';

$cc = trim($_POST['current_code']);
$cs = trim($_POST['currency_symbol']);

$cnx = new Connect();
$cnx->open();

if( !$sql = @mysql_query("SELECT `id`,`maker`,`model`,`price`,`year`,`status`,`featured` FROM `car` WHERE `status`='0' ORDER BY RAND() LIMIT 4;") ){
    $cnx->close();
    
    require_once BASE_CLASS . 'class-log.php';
    LogReport::write('Unable to load car list for home page latest vehicle due a query error at ' . __FILE__ . ':' . __LINE__);
    die('ng?Unable to load car list due a query error.');
}

if( @mysql_num_rows($sql) < 1 ){
    $cnx->close();
    die('ok?');
}

$result = 'ok?';
require_once BASE_CLASS . 'class-utilities.php';

while( $r = @mysql_fetch_assoc($sql) ){
    $url = getThumbById($r['id']);
    $result .= $r['id'] . ';' . $r['maker'] . ';' . $r['model'] . ';' . $cs . Utilities::formatPrice($r['price'],$cc) . ';' . $r['year'] . ';' . $_POST['base'] . $url . ';' . $r['featured'];
    $result .= '[]';
}

@mysql_free_result($sql);
$cnx->close();

die($result);

function getThumbById($id){
    if( empty($id) ){
        return 'image/default_main_image.jpg';
    }
    
    require_once dirname(dirname(__FILE__)) . '/config.php';
    require_once BASE_CLASS . 'class-connect.php';

    $cnx = new Connect();
    $cnx->open();
    
    if( !$sql = @mysql_query("SELECT `car_id`,`mode`,`source` FROM `car_media` WHERE `car_id`='$id' AND `mode`='exterior' LIMIT 1;") ){
        $cnx->close();
        return 'image/default_main_image.jpg';
    }
    else {
        $r = @mysql_fetch_assoc($sql);
        @mysql_free_result($sql);
        $cnx->close();
        
        $url = $r['source'];
        
        if( empty($url) ){
            $url = 'image/default_main_image.jpg';
        }
        
        return $url;
    }
}