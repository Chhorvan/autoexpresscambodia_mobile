<?php

	function executeDataCount(){
		require_once dirname(dirname(__FILE__)) . '/config.php';
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
		$owner=$_SESSION['log_id'] ;
		
        $truncate_sql="TRUNCATE data_count";
        $insert_sql="INSERT INTO data_count (register_id, 
                        country, 
                        car_count, 
                        truck_count, 
                        bus_count, 
                        part_count, 
                        accessories_count, 
                        equipment_count, 
                        motorbike_count, 
                        aircraft_count, 
                        watercraft_count,
						suv_count,
						van_count,
						pickup_count
						) 
                    SELECT `rg`.`user_id`,
                        `rg`.`country`,
                        (SELECT COUNT(id) FROM active_product_car WHERE owner=`rg`.`user_id`),
                        (SELECT COUNT(id) FROM active_product_truck WHERE owner=`rg`.`user_id`),
                        (SELECT COUNT(id) FROM active_product_bus WHERE owner=`rg`.`user_id`),
                        (SELECT COUNT(id) FROM active_product_part WHERE owner=`rg`.`user_id`),
                        (SELECT COUNT(id) FROM active_product_accessories WHERE owner=`rg`.`user_id`),
                        (SELECT COUNT(id) FROM active_product_equipment WHERE owner=`rg`.`user_id`),
                        (SELECT COUNT(id) FROM active_product_motorbike WHERE owner=`rg`.`user_id`),
                        (SELECT COUNT(id) FROM active_product_aircraft WHERE owner=`rg`.`user_id`),
                        (SELECT COUNT(id) FROM active_product_watercraft WHERE owner=`rg`.`user_id`),
						(SELECT COUNT(id) FROM active_product_suv WHERE owner=`rg`.`user_id`),
						(SELECT COUNT(id) FROM active_product_van WHERE owner=`rg`.`user_id`),
						(SELECT COUNT(id) FROM active_product_pickup WHERE owner=`rg`.`user_id`)
                    FROM activated_seller rg 
                    LEFT JOIN product pd ON pd.owner=rg.user_id 
                    GROUP BY rg.user_id

        ";
        if( !mysql_query($truncate_sql) ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to truncate data_count due a query error at ' . __FILE__ . ':' . __LINE__);
            $cnx->close();
            $this->form_message = $this->lang['MANAGE_TRUNCATE_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        //echo $insert_sql;
		
		//echo $insert_sql;
		
        if( !mysql_query($insert_sql) ){
			echo $insert_sql;
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to save count to data_count due a query error at ' . __FILE__ . ':' . __LINE__);
            $cnx->close();
            $this->form_message = $this->lang['MANAGE_SAVED_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }

        $cnx->close();
 //       $this->form_message = "SUCCESS";
//        $this->form_status = true;
//        $this->form_style = 'alert-success';
        return;
    
	}

?>