<?php



function getSellersByCountry($product_type=""){
        require_once dirname(dirname(__FILE__)) . '/config.php';
        require_once BASE_CLASS . 'class-connect.php';

        $cnx = new Connect();
        $cnx->pdoOpen();
        $and="";

        $countcountry=array();
        //$and=" AND (`member_type1` LIKE '%seller%' OR `member_type1` LIKE '%forwarder%')";
        if(!empty($product_type)){
            $and.= " AND `pd`.`product_type` = '$product_type'";
        }


        //if(!empty($vehicle_type)){
        //  $and.= " AND `vehicle_type`='$vehicle_type'";
        //}


        $sql_str="
                SELECT
                    dc.country,
                    cl.country_name,
                    COUNT(dc.register_id) as `number`,
                    SUM(dc.car_count) as car_count,
					SUM(dc.suv_count) as suv_count,
					SUM(dc.van_count) as van_count,
					SUM(dc.pickup_count) as pickup_count,
                    SUM(dc.truck_count) as truck_count,
                    SUM(dc.bus_count) as bus_count,
                    SUM(dc.equipment_count) as equipment_count,
                    SUM(dc.part_count) as part_count,
                    SUM(dc.accessories_count) as accessories_count,
                    SUM(dc.motorbike_count) as motorbike_count,
                    SUM(dc.aircraft_count) as aircraft_count,
                    SUM(dc.watercraft_count) as watercraft_count,

                    SUM(dc.suv_count) as suv_count,
                    SUM(dc.van_count) as van_count,
                    SUM(dc.pickup_count) as pickup_count,
                    SUM(car_count +

                        truck_count +
                        bus_count +
                        equipment_count +
                        part_count +
                        accessories_count +
                        motorbike_count +
                        aircraft_count +
                        watercraft_count +
                        suv_count +
                        van_count +
                        pickup_count) as `product_number`
                FROM `data_count` as `dc`
                INNER JOIN `country_list` as cl ON `cl`.`cc`=`dc`.`country`
                INNER JOIN `activated_register` as rg ON rg.id=dc.register_id

                WHERE 1 $and
                GROUP BY `dc`.`country`
                ORDER BY `number` DESC, `product_number` DESC

                ";
        //echo $sql_str;
        $cnx->pdoExecuteQuery($sql_str);
        $countcountry=$cnx->getRecords();

        @mysql_free_result($sql);
        $cnx->close();
        return $countcountry;

    }
?>
