<?php
header('Content-Type: application/json');

require_once dirname(dirname(__FILE__)) . '/config.php';
require_once BASE_CLASS . 'class-connect.php';

$cnx = new Connect();
$cnx->open();

if(isset($_POST['msg_template_add']) && $_POST['msg_template_add'] === 'true'){
	if(mysql_query("INSERT INTO message_template (title, text) VALUE ('{$_POST['msg_template_title']}', '{$_POST['msg_template_text']}')")){
		print_r(json_encode(array('add' => 'Successfully added.')));
	}
	else{
		print_r(json_encode(array('add' => 'Error add.')));
	}
}
elseif(isset($_POST['msg_template_edit']) && $_POST['msg_template_edit'] === 'true'){
	if(mysql_query("UPDATE message_template SET title='{$_POST['msg_template_title']}', text='{$_POST['msg_template_text']}'
							WHERE id = {$_POST['msg_template_id']}")){
		print_r(json_encode(array('add' => 'Successfully edited.')));
	}
	else{
		print_r(json_encode(array('add' => 'Error update')));
	}
}
elseif(isset($_POST['msg_template_delete']) && $_POST['msg_template_delete'] === 'true'){
	if(mysql_query("DELETE FROM message_template WHERE id = {$_POST['msg_template_id']}")){
		print_r(json_encode(array('add' => 'Successfully deleted.')));
	}
	else{
		print_r(json_encode(array('add' => 'Error delete')));
	}
}
else{
	if(!$result = mysql_query("SELECT * FROM message_template WHERE id = {$_POST['msg_template_id']}")){
		print_r (json_encode(array('select' => 'Could not load record: ' . mysql_error($con))));
	}
	else{
		$message_template = array();
		while($row = @mysql_fetch_assoc($result)){
			array_push($message_template, $row);
		}
		echo json_encode($message_template);
	}
}
$cnx->close();


if(isset($_POST['msg_template_add'])){
	unset($_POST['msg_template_add']);
}
if(isset($_POST['msg_template_edit'])){
	unset($_POST['msg_template_edit']);
}
if(isset($_POST['msg_template_delete'])){
	unset($_POST['msg_template_delete']);
}
if(isset($_POST['msg_template_id'])){
	unset($_POST['msg_template_id']);
}
if(isset($_POST['msg_template_title'])){
	unset($_POST['msg_template_title']);
}
if(isset($_POST['msg_template_text'])){
	unset($_POST['msg_template_text']);
}
?>
