<?php
if(!isset($_SESSION)) @session_start();
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################

$id = trim($_POST['mid']);
$id = (int)$id;

if( !isset($_SESSION['log_group']) || $_SESSION['log_group'] != 'admin' && $_SESSION['log_group'] != 'user' ){
    die();
}

if( empty($id) || $id < 1 ){
    die('ng,Invalid request.');
}


require_once dirname(dirname(__FILE__)) . '/config.php';
require_once BASE_CLASS . 'class-connect.php';

$cnx = new Connect();
$cnx->open();

$id = @mysql_real_escape_string($id);

// get source.
if( !$sql = @mysql_query("SELECT `id`,`type`,`source` FROM `car_media` WHERE `id`='$id'") ){
    require_once BASE_CLASS . 'class-log.php';
    LogReport::write('Unable to query car media table due a query error at ' . __FILE__ . ':' . __LINE__);
    $cnx->close();
    die('ng,Unable to validate id due an internal error.');
}

if( @mysql_num_rows($sql) != 1 ){
    $cnx->close();
    die('ng,Media not found on database.');
}

$r = @mysql_fetch_assoc($sql);
@mysql_free_result($sql);

$has_source = true;
$source = '';

if( $r['type'] == 'video' ){
    $has_source = false;
}
else {
    $source = BASE_ROOT . $r['source'];
}

// remove entry from database.
if( !@mysql_query("DELETE FROM `car_media` WHERE `id`='$id' LIMIT 1;") ){
    require_once BASE_CLASS . 'class-log.php';
    LogReport::write('Unable to remove car media table due a query error at ' . __FILE__ . ':' . __LINE__);
    $cnx->close();
    die('ng,Unable to remove media due an internal error.');
}

// remove source if image.
if( file_exists($source) ){
    @unlink($source);
}

die('ok,success');