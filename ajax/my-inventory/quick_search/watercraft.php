<?php 
    if(!isset($_SESSION['mobile'])) $mobile=false; else $mobile=$_SESSION['mobile'];
    $_GET['product_type']="Watercraft";
?>

                    <?php
                    
                        if ($mobile==false) {
                    ?>
                    <div id="second_search">
                        <!-- <div id="search_title" class="clearfix">
                            <p><a href="#" class="current" id="quick_search">Quick Search</a> <span>&nbsp;&nbsp; | &nbsp;&nbsp;</span> <a href="#" id="ad_search">Advance Search</a></p>
                        </div> -->
                        <div id="search_wrap" class="clearfix">
                            <form method="get" action="my-inventory">
                                <input type="hidden" class="formSelection" name="product_type" id="product_type" value="<?php if(isset($_GET['product_type'])) echo htmlspecialchars($_GET['product_type']);?>"/>
                                <input type="hidden" class="formSelection" name="s" id="offset" value="<?php if(isset($_GET['s'])) echo htmlspecialchars($_GET['s']);?>"/>
                                <input type="hidden" class="formSelection" name="owner" id="owner" value="<?php if(isset($_GET['owner'])) echo htmlspecialchars($_GET['owner']);?>"/>
                                
                                <select class="watercraft_condition" name="make" id="make">
                                    <option value="">- Condition -</option>
                                            <option value="New">New</option>
                                            <option value="Used">Used</option>
                                </select>
                                
                                
                                <select class="watercraft_country" name="model" id="model">
                                    <?php 
                                        include('../../../ajax/load_country.php');
                                    ?>
                                </select>
                                <span class="watercraft">Year / Month</span>
                                <select class="watercraft_year formSelection " name="year_from" id="year_from_adv">
                                    <option value="">--Year--</option>
                                    <?php
                                             for($i=date('Y');$i>=date('Y')-20;$i--){
                                                     echo "<option value='$i'>$i</option>";
                                                 }
                                        ?>
                                </select>
                                <select class="watercraft_year formSelection " name="month_from" id="month_from_adv">
                                    <option value="">--Month--</option>
                                    <?php
                                         for($i=1;$i<=12;$i++){
                                             echo "<option value='$i'>$i</option>";
                                         }
                                         ?>
                                </select>
                                <span class="slash"> / </span>
                                <select class="watercraft_year formSelection " name="year_to" id="year_to_adv">
                                    <option value="">--Year--</option>
                                    <?php
                                             for($i=date('Y');$i>=date('Y')-20;$i--){
                                                     echo "<option value='$i'>$i</option>";
                                                 }
                                        ?>
                                </select>
                                <select class="watercraft_year formSelection " name="month_to" id="month_to_adv">
                                    <option value="">--Month--</option>
                                    <?php
                                         for($i=1;$i<=12;$i++){
                                             echo "<option value='$i'>$i</option>";
                                         }
                                         ?>
                                </select>
                                <span class="price"> Price </span>
                                <input type="text" class="watercraft_price" name="price_from" value="" placeholder="$1000" id="price_from_adv" onkeypress="return isNumberKey(event)" />
                                <span class="sep"> - </span>
                                <input type="text" class="watercraft_price" name="price_to" value="" placeholder="$1000" id="price_to_adv" onkeypress="return isNumberKey(event)" />
                                <input type="text" class="text_search" name="customSearch" placeholder="Model, name..." id="customSearch" />
                                <input type="submit" id="submit_search" value="" class="search linkfade" />
                            </form>
                        </div>
                                         
                        </form>
                    </div>

                    
                    <?php }
                    else{
                        ?>                        
                    <div class="new_searchwrap">
                        <div class="newsearch">
                            <form method="get" action="vehicle">
                                <input type="hidden" class="formSelection" name="product_type" id="product_type" value="<?php if(isset($_GET['product_type'])) echo htmlspecialchars($_GET['product_type']);?>"/>
                                <input type="hidden" class="formSelection" name="s" id="offset" value="<?php if(isset($_GET['s'])) echo htmlspecialchars($_GET['s']);?>"/>
                                <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class=" top">

                                        <select class="condition formSelection" name="condition" id="condition">
                                            <option value="">- Condition -</option>
                                            <option value="New">New</option>
                                            <option value="Used">Used</option>
                                            
                                        </select>

                                    </td>
                                </tr>
                                <tr>
                                    <td >
                                        <select class="make formSelection" name="make" id="make">                                    
                                            <?php
                                                include('../../../ajax/load_car_make.php');
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td >
                                        <select class="model formSelection" name="model" id="model">
                                            <option value="">- Model -</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td >
                                        <select class="steering formSelection" name="steering" id="steering">
                                            <option selected value="">- Steering -</option>
                                            <option value="LHD">LHD</option>
                                            <option value="RHD">RHD</option>
                                        </select>
                                    </td>
                                </tr>
                                <!--<tr>
                                    <td >
                                        <select class="country formSelection" name="country" id="country">
                                            <?php
                                                include('../../../ajax/load_country.php');
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                                -->
                                <tr>
                                    <td >
                                        <select class="ful_type formSelection" name="fuel_type" id="fuel_type">
                                            <?php
                                                include('load_saved_fuel_type.php');
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                               
                                
                                <tr>
                                    <td >
                                        <div id="divYear">
                                            <!-- <div id="year"> <p>Year :</p></div> -->
                                            <select class="year formSelection" name="year_from" id="year_from">
                                                <option value="">From</option>
                                                 <?php
                                                 for($i=1990;$i<=2014;$i++){
                                                     echo "<option value='$i'>$i</option>";
                                                 }
                                                 ?>
                                            </select>
                                                 
                                            <select class="year_end toSelection" name="year_to" id="year_to">
                                                <option value="">To</option>
                                                 <?php
                                                 for($i=1990;$i<=2014;$i++){
                                                     echo "<option value='$i'>$i</option>";
                                                 }
                                                 ?>
                                            </select>
                                        </div>

                                    </td>
                                </tr>
                                <tr>
                                    <td class="search">
                                        <input type="text" class="textBox" name="customSearch" placeholder="Model, name..." id="customSearch" />
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <input type="submit" id="submit_search" value="" class="search" />
                                    </td>
                                </tr>
                            </table>
                            </form>
                        </div>
                    </div>
                        <?php
                    }
                    ?>
                    





