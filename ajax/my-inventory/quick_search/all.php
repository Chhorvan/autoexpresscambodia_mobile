<?php
if(!isset($_SESSION)) session_start();
if(!isset($_SESSION['mobile'])) $mobile=false;
$_GET['product_type']="";
?>


<div id="search_wrap" >
	<form method="get" action="my-inventory">
		<input type="hidden" class="formSelection" name="product_type" id="product_type" value="<?php if(isset($_GET['product_type'])) echo htmlspecialchars($_GET['product_type']);?>"/>
		<input type="hidden" class="formSelection" name="s" id="offset" value="<?php if(isset($_GET['s'])) echo htmlspecialchars($_GET['s']);?>"/>
		<input type="hidden" class="formSelection" name="owner" id="owner" value="<?php if(isset($_GET['owner'])) echo htmlspecialchars($_GET['owner']);?>"/>
		<input type="hidden" class="formSelection" name="page" id="page" value="<?php if(isset($_GET['page'])) echo htmlspecialchars($_GET['page']);?>"/>
	   
			
		<table>
			<tr>
				<td style="padding:0px">
					<div class="bgsearch fa-caret-down">
						<select class="make formSelection" name="make" id="make">
							<?php

								include('../../../ajax/load_car_make.php');
							?>
						</select>
					</div>
					
				</td>
				
				<td style="padding:0px">
					<div class="bgsearch fa-caret-down">
						<select class="model formSelection" name="model" id="model">
							 <option value="">Model</option>
						</select>
					</div>
					
				</td>
				
				<td style="padding:0px">
					<div class="bgsearch fa-caret-down">
						<select class="year formSelection" name="year" id="year_from">
					<option value="">Year</option>
					 <?php
					 for($i=date('Y');$i>=date('Y')-20;$i--){
						 echo "<option value='$i'>$i</option>";
					 }
					 ?>
				</select>
					</div>
					
				</td>
				
				 <td style="padding:0px">
					<div class="bgsearch fa-caret-down">
						 <select class="condition formSelection" name="condition" id="condition">
						<option value="">Condition</option>
						<option value="1">New</option>
						<option value="0">Used</option>

					</select>
					</div>
					
				</td>
				
				 <td style="padding:0px">
					<div class="bgsearch1">
						<input style="border:none" type="text"  name="customSearch" placeholder="Keyword" id="customSearch" />
					</div>
					
				</td>
				
				 <td style="padding:0px; padding-left:25px">
					<input type="submit" id="submit_search" value="" class="search linkfade" />
					
				</td>

			</tr>
		</table>      
	</form>
</div><!-- #search_wrap -->