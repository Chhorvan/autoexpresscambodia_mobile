<?php
header('Content-Type: application/json');

require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once BASE_CLASS . 'class-connect.php';
$cnx = new Connect();
$cnx->open();

if(isset($_GET['product_type']))
{
	$product_type = mysql_real_escape_string($_GET['product_type']);
	$product_maker = array();
	
	if( !$result = mysql_query("SELECT DISTINCT `make` FROM `product` WHERE product_type='$product_type' AND `make` != '' ORDER BY `make`")){
        require_once BASE_CLASS . 'class-log.php';
        LogReport::write('Unable to load car type list due a query error at ' . __FILE__ . ':' . __LINE__);            
    }
    elseif(mysql_num_rows($result) > 0){
        while( $r = mysql_fetch_assoc($result)){
            array_push($product_maker, $r);
        }

        echo json_encode($product_maker);
    }
}

$cnx->close();
