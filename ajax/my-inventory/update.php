<?php

if(!isset($_SESSION)) @session_start();
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once BASE_CLASS . 'class-connect.php';
$cnx = new Connect();
$cnx->open();
if(isset($_POST['delete'])) $delete=$_POST['delete']; else $delete=array();

$ids = implode("', '", $delete);


$sql="UPDATE `product` SET status='Reserved' ";
$sql .= "WHERE car_id IN ('$ids') ";
if(!@mysql_query($sql)){
    require_once BASE_CLASS . 'class-log.php';
    LogReport::write('Unable to delete product due a query error at ' . __FILE__ . ':' . __LINE__);
    echo "Update Fail!";
}
if(mysql_affected_rows()>0){
	echo "Update  Success!";
}else{
	echo "Update failed! No item with these IDs in your Inventory.";
}
$cnx->close();

