jQuery(document).ready(function($) {
	//Rollover
	var postfix = '_on';
	$('.over').not('[src*="'+ postfix +'."]').each(function() {

		var img = $(this);
		var src = img.attr('src');
		var src_on = src.substr(0, src.lastIndexOf('.'))
		           + postfix
		           + src.substring(src.lastIndexOf('.'));
		$('<img>').attr('src', src_on);
		img.hover(
			function() {
				img.attr('src', src_on);
			},
			function() {
				img.attr('src', src);
			}
		);
	});

	//Smooth Scrolling
	jQuery.easing.quart = function (x, t, b, c, d) {
		return -c * ((t=t/d-1)*t*t*t - 1) + b;
	};  
	
	$(function () {
		$('.pageTop, .link_top a').click(function () {
			$('html,body').animate({ scrollTop: 0 }, 1000, 'quart');
			return false;
		});
	});

	/**
	 * MENU RESPONSITE
	 * ------------------------------------------------------------------------------
	 */
	$('#menuNav ul li').click(function(){
		//$('#menuNav ul li a').removeClass('current');
		//$('#menuNav ul li a').addClass('current');
	}) 
	$('#menuRes').click(function(){		
		/* if($(this).hasClass('current')){
			$(this).removeClass('current');
		}else{
			$(this).addClass('current');
		} */
		//show its submenu
		$("ul#dropMenu").stop().slideToggle(300).css({'display':'block'});	  
		
	})
	
	// Function invocation
	js_popup();
	
	// Link image
	link_image();
	
	// Lazy loading
	lazy_load();
	
	// Fade on mouse over
	fade_over();
	
	// Language dropdown
	language_dropdown();
	
});


$(document).ready(function() {

    $("a.fancybox").fancybox({		        
        'width': 900,
        'height': 470,                          
        //'autoScale': false,
		'transitionIn': 'none',
		'transitionOut': 'none',		
		'scrolling': 'yes',
		'centerOnScroll': true, 
		'autoCenter'	: true, 
		'type':	'iframe'
    });
	
});






/**
 * PAGE PRINT
 * ------------------------------------------------------------------------------
 */
function js_page_print(){
	window.print();
	return false;
}


/**
 * POP UP
 * ------------------------------------------------------------------------------
 */
function js_popup(){
	var js_para = null;
	$('a[class^="js_popup_"], area[class^="js_popup_"]').each(function(index){
		$(this).click(function(){
			var wo = null;
			// get window width & height
			js_para = $(this).attr('class').match(/[0-9]+/g);
			// get window.name
			window.name ? js_para[2] = window.name+'_' : js_para[2] = ('');
			wo = window.open(this.href, 'popup'+index,'width='+js_para[0]+',height='+js_para[1]+',scrollbars=no');
			
			var left = (screen.width/2)-(js_para[0]/2);
			var top = (screen.height/2)-(js_para[1]/2);
			wo.moveTo(left,top);
			wo.resizeTo(js_para[0],js_para[1]);
			return false;
		});
	});
}


/**
 * CLOSE
 * ------------------------------------------------------------------------------
 */
function js_window_close(){
	window.close();
	return false;
}


/**
 * LAZY LOAD
 * ------------------------------------------------------------------------------
 */
function lazy_load(){
	//$("img.lazy").lazyload({ effect : "fadeIn" });
}


/**
 * LINK IMAGE
 * ------------------------------------------------------------------------------
 */
function link_image() {
	$('.linkimg').hover(function(){
		$(this).stop().animate({'opacity':0.7}, 500);
	}, function(){
		$(this).stop().animate({'opacity':1}, 500);
	});
}


/**
 * FADE OVER
 * ------------------------------------------------------------------------------
 */
function fade_over() {
	$('.fadeOver').hover(function(){
		$(this).stop().animate({'opacity':0}, 500);
	}, function(){
		$(this).stop().animate({'opacity':1}, 500);
	});
}

/**
 * LANGUAGE DROPDOWN
 * ------------------------------------------------------------------------------
 */
function language_dropdown() {

	// turn select into dl
	createDropDown();

	var $dropTrigger = $(".dropdown dt a");
	var $languageList = $(".dropdown dd ul");

	// open and close list when button is clicked
	$dropTrigger.toggle(function() {
		$languageList.slideDown(200);
		$dropTrigger.addClass("active");
	}, function() {
		$languageList.slideUp(200);
		$(this).removeAttr("class");
	});

	// close list when anywhere else on the screen is clicked
	$(document).bind('click', function(e) {
		var $clicked = $(e.target);
		if (! $clicked.parents().hasClass("dropdown"))
			$languageList.slideUp(200);
			$dropTrigger.removeAttr("class");
	});

	// when a language is clicked, make the selection and then hide the list
	$(".dropdown dd ul li a").click(function() {
		var clickedValue = $(this).parent().attr("class");
		var clickedTitle = $(this).find("em").html();
		$("#target dt").removeClass().addClass(clickedValue);
		$("#target dt em").html(clickedTitle);
		$languageList.hide();
		$dropTrigger.removeAttr("class");
	});
}
		// actual function to transform select to definition list
		function createDropDown(){
			var $form = $("div#country-select form");
			$form.hide();
			var source = $("#country-options");
			source.removeAttr("autocomplete");
			var selected = source.find("option:selected");
			var options = $("option", source);
			$("#country-select").append('<dl id="target" class="dropdown"></dl>')
			$("#target").append('<dt class="' + selected.val() + '"><a href="#"><span class="flag"></span><em>' + selected.text() + '</em></a></dt>')
			$("#target").append('<dd><ul></ul></dd>')
			options.each(function(){
				$("#target dd ul").append('<li class="' + $(this).val() + '"><a href="' + $(this).attr("title") + '"><span class="flag"></span><em>' + $(this).text() + '</em></a></li>');
				});
		}