var selected_msg = {},
    selected_files = [],
    current_number_file_upload = 0,
    date_string;

function addMessageTemplate(){
    // Draw box.
    var x = Math.max( document.body.scrollLeft,
                      document.documentElement.scrollLeft,
                      window.pageXOffset ),
        y = Math.max( document.body.scrollTop,
                      document.documentElement.scrollTop,
                      window.pageYOffset ),
        doc_width = Math.max( document.body.scrollWidth,
                            document.body.offsetWidth,
                            document.documentElement.clientWidth,
                            document.documentElement.scrollWidth,
                            document.documentElement.offsetWidth ),
        doc_height = Math.max(document.body.scrollHeight,
                            document.body.offsetHeight,
                            document.documentElement.clientHeight,
                            document.documentElement.scrollHeight,
                            document.documentElement.offsetHeight ),
        w_width = window.innerWidth
                || document.documentElement.clientWidth
                || document.body.clientWidth,
        w_height = window.innerHeight
                || document.documentElement.clientHeight
                || document.body.clientHeight;

    var win_block = document.createElement('div');
    win_block.id = 'win_block';
    win_block.style.width = '100%';
    win_block.style.height = doc_height +'px';
    win_block.style.backgroundColor = '#000';
    win_block.style.zIndex = '9998';
    win_block.style.left = '0';
    win_block.style.top = '0';
    win_block.style.opacity = '0.5';
    win_block.style.position = 'absolute';

    var pop_up_form = document.createElement('form');
    pop_up_form.id = 'pop_up_form_msg_template';
    pop_up_form.name = 'pop_up_form_msg_template';
    pop_up_form.method = 'post';
    pop_up_form.style.width = '400px';
    pop_up_form.style.backgroundColor = '#f0f0f0';
    pop_up_form.style.borderRadius = '10px';
    pop_up_form.style.zIndex = '9999';
    pop_up_form.style.position = 'absolute';
    pop_up_form.style.padding = '20px';
    pop_up_form.style.border = '5px solid cornflowerblue';
    pop_up_form.style.boxShadow = '0 0 10px rgba(0,0,0,.5)';

    // Add elements
    var innerElementHTML = '<span style="display: block; padding-bottom: 5px; font-weight: bold;">Title</span>' +
                            '<input id="msg_template_title" name="msg_template_title" type="text">' +
                            '<span id="msg_template_title_validate" style="padding-left: 5px; color: red; display: none;">* Field cannot be blank</span>' +
                            '<span style="display: block; padding-top: 20px; padding-bottom: 5px; font-weight: bold;">Message Template</span>' +
                            '<textarea id="msg_template_text" name="msg_template_text" rows="10" style="width: 100%; padding: 0px; margin: 0px; resize: none;"></textarea>' +
                            '<span id="msg_template_text_validate" style="padding-top: 5px; color: red; display: none;">* Field cannot be blank</span>' +
                            '<div style="text-align: center"><input type="submit" id="btn_msg_template_add" name="btn_msg_template_add" value="Add" style="width: 60px; height: 30px; margin-top: 10px;">' +
                            '<button type="button" id="btn_msg_template_cancel" style="width: 60px; height: 30px; margin-top: 10px; margin-left: 5px;">Cancel</button></div>';
    pop_up_form.innerHTML = innerElementHTML;

    // Appended to the page
    document.body.appendChild(win_block);
    document.body.appendChild(pop_up_form);

    // Middle the window.
    document.getElementById('pop_up_form_msg_template').style.width = pop_up_form.clientWidth + 'px';
    document.getElementById('pop_up_form_msg_template').style.left = x + (w_width - pop_up_form.clientWidth)/2 + 'px';
    document.getElementById('pop_up_form_msg_template').style.top = y + (w_height - pop_up_form.clientWidth)/2 + 'px';

    document.getElementById('pop_up_form_msg_template').addEventListener('submit', function(e){
        e.preventDefault();

        var title = document.getElementById('msg_template_title').value.trim();
        var text = document.getElementById('msg_template_text').value.trim();
        var data = {};

        if( title === ''){
            var validate_title = document.getElementById('msg_template_title_validate');
            validate_title.style.display = 'inline-block';
        }
        if( text === ''){
            var validate_text = document.getElementById('msg_template_text_validate');
            validate_text.style.display = 'inline-block';
        }

        if(title !== '' && text !== ''){
            data['msg_template_add'] = 'true';
            data['msg_template_title'] = title;
            data['msg_template_text'] = text;
            $.ajax({
                url: 'ajax/message-template.php',
                type: 'post',
                data: data,
                success: function(d){
                    location.reload(true);
                    //console.log(d);
                }
            });
        }
    });

    // Cancel
    document.getElementById('btn_msg_template_cancel').addEventListener('click', function(e){
        document.getElementById('pop_up_form_msg_template').remove();
        document.getElementById('win_block').remove();
    });
}

function editMessageTemplate(){
    if($.isEmptyObject(selected_msg)){
        alert('Please select a message template.');
        return;
    }

    // Draw box.
    var x = Math.max( document.body.scrollLeft,
                      document.documentElement.scrollLeft,
                      window.pageXOffset ),
        y = Math.max( document.body.scrollTop,
                      document.documentElement.scrollTop,
                      window.pageYOffset ),
        doc_width = Math.max( document.body.scrollWidth,
                            document.body.offsetWidth,
                            document.documentElement.clientWidth,
                            document.documentElement.scrollWidth,
                            document.documentElement.offsetWidth ),
        doc_height = Math.max(document.body.scrollHeight,
                            document.body.offsetHeight,
                            document.documentElement.clientHeight,
                            document.documentElement.scrollHeight,
                            document.documentElement.offsetHeight ),
        w_width = window.innerWidth
                || document.documentElement.clientWidth
                || document.body.clientWidth,
        w_height = window.innerHeight
                || document.documentElement.clientHeight
                || document.body.clientHeight;

    var win_block = document.createElement('div');
    win_block.id = 'win_block';
    win_block.style.width = '100%';
    win_block.style.height = doc_height +'px';
    win_block.style.backgroundColor = '#000';
    win_block.style.zIndex = '9998';
    win_block.style.left = '0';
    win_block.style.top = '0';
    win_block.style.opacity = '0.5';
    win_block.style.position = 'absolute';

    var pop_up_form = document.createElement('form');
    pop_up_form.id = 'pop_up_form_msg_template';
    pop_up_form.name = 'pop_up_form_msg_template';
    pop_up_form.method = 'post';
    pop_up_form.style.width = '400px';
    pop_up_form.style.backgroundColor = '#f0f0f0';
    pop_up_form.style.borderRadius = '10px';
    pop_up_form.style.zIndex = '9999';
    pop_up_form.style.position = 'absolute';
    pop_up_form.style.padding = '20px';
    pop_up_form.style.border = '5px solid cornflowerblue';
    pop_up_form.style.boxShadow = '0 0 10px rgba(0,0,0,.5)';

    // Add elements
    var innerElementHTML = '<span style="display: block; padding-bottom: 5px; font-weight: bold;">Title</span>' +
                            '<input id="msg_template_title" name="msg_template_title" type="text">' +
                            '<span id="msg_template_title_validate" style="padding-left: 5px; color: red; display: none;">* Field cannot be blank</span>' +
                            '<span style="display: block; padding-top: 20px; padding-bottom: 5px; font-weight: bold;">Message Template</span>' +
                            '<textarea id="msg_template_text" name="msg_template_text" rows="10" style="width: 100%; padding: 0px; margin: 0px; resize: none;"></textarea>' +
                            '<span id="msg_template_text_validate" style="padding-top: 5px; color: red; display: none;">* Field cannot be blank</span>' +
                            '<div style="text-align: center"><input type="submit" id="btn_msg_template_edit" name="btn_msg_template_edit" value="Edit" style="width: 60px; height: 30px; margin-top: 10px;">' +
                            '<button type="button" id="btn_msg_template_cancel" style="width: 60px; height: 30px; margin-top: 10px; margin-left: 5px;">Cancel</button></div>';
    pop_up_form.innerHTML = innerElementHTML;

    // Appended to the page
    document.body.appendChild(win_block);
    document.body.appendChild(pop_up_form);

    // Middle the window.
    document.getElementById('pop_up_form_msg_template').style.width = pop_up_form.clientWidth + 'px';
    document.getElementById('pop_up_form_msg_template').style.left = x + (w_width - pop_up_form.clientWidth)/2 + 'px';
    document.getElementById('pop_up_form_msg_template').style.top = y + (w_height - pop_up_form.clientWidth)/2 + 'px';

    // Cancel
    document.getElementById('btn_msg_template_cancel').addEventListener('click', function(e){
        document.getElementById('pop_up_form_msg_template').remove();
        document.getElementById('win_block').remove();
    });

    // Submit form
    document.getElementById('pop_up_form_msg_template').addEventListener('submit', function(e){
        e.preventDefault();


        var title = document.getElementById('msg_template_title').value.trim();
        var text = document.getElementById('msg_template_text').value.trim();
        var data = {};

        if( title === ''){
            var validate_title = document.getElementById('msg_template_title_validate');
            validate_title.style.display = 'inline-block';
        }
        if( text === ''){
            var validate_text = document.getElementById('msg_template_text_validate');
            validate_text.style.display = 'inline-block';
        }

        if(title !== '' && text !== ''){
            data['msg_template_edit'] = 'true';
            data['msg_template_title'] = title;
            data['msg_template_text'] = text;
            data['msg_template_id'] = selected_msg['msg_template_id'];
            $.ajax({
                url: 'ajax/message-template.php',
                type: 'post',
                data: data,
                success: function(d){
                    location.reload(true);
                    //console.log(d);
                }
            });
        }
    });

    // Load message template to edit
    document.getElementById('msg_template_title').value = selected_msg['title'];
    document.getElementById('msg_template_text').value = selected_msg['text'];
}

function deleteMessageTemplate(){
    if($.isEmptyObject(selected_msg)){
        alert('Please select a message template.');
        return;
    }

    if(confirm('Are you sure to delete this message template?')){
        var data = {};
        data['msg_template_delete'] = 'true';
        data['msg_template_id'] = selected_msg['msg_template_id'];
        $.ajax({
            url: 'ajax/message-template.php',
            type: 'post',
            data: data,
            success: function(d){
                location.reload(true);
                //console.log(d);
            }
        });
    }
}

function selectMsgTemplate(){
    if($('#select_message_template').val() === ''){
        selected_msg = {};

        return;
    }

    var old_msg = $('#i_sMessage').val();

    $.ajax({
        url: 'ajax/message-template.php',
        type: 'post',
        data: 'msg_template_id=' + $('#select_message_template').val(),
        success: function(data){

            var a = jQuery.makeArray(data);
            a.forEach( function(element, index, array){
                $('#i_sMessage').val(element['text'] + old_msg);
                selected_msg['title'] = element['title'];
                selected_msg['text'] = element['text'];
            });
        }
    });

    selected_msg['msg_template_id'] = $('#select_message_template').val();
}

// function repplyMsg(){
//         var sms = {};

//         var m_id='<?php  if(isset($_GET['m_id'])) echo $_GET['m_id'];?>';
//         alert(m_id);
//     //     var old_msg = $('#i_sMessage').val();
//     //     sms['c_id'] = c_id;
//     //     sms['old_msg'] = old_msg;
//     //     alert(old_msg);

//     // $.ajax({
//     //     url: 'ajax/message-template.php',
//     //     type: 'post',
//     //     data: sms,
//     //     success: function(data){
//     //         console.log(data);
//     //     // $('#i_sMessage').val(element['text'] + old_msg);

//     //     }
//     // });


// }

function chooseAttachFile(file_dialogue, max_file_size, destination){
    var total_size = 0,
        fail_files = '',
        fail = '',
        allowed = $('#allowed_file_format').text().toLowerCase().split(','),
        utc_date = new Date();

    date_string = '' + utc_date.getFullYear() +
                    ((utc_date.getMonth() + 1) < 10 ? '0' + (utc_date.getMonth() + 1) : (utc_date.getMonth() + 1)) +
                    (utc_date.getDate() < 10 ? '0' + utc_date.getDate() : utc_date.getDate()) +
                    (utc_date.getHours() < 10 ? '0' + utc_date.getHours() : utc_date.getHours()) +
                    (utc_date.getMinutes() < 10 ? '0' + utc_date.getMinutes() : utc_date.getMinutes()) +
                    (utc_date.getSeconds() < 10 ? '0' + utc_date.getSeconds() : utc_date.getSeconds()) +
                    (utc_date.getMilliseconds() < 10 ? '0' + utc_date.getMilliseconds() : utc_date.getMilliseconds());

    allowed.forEach(function(item, index){
        allowed[index] = item.trim();
    });

    // Caculate last total file size
    for (var i=0; i<selected_files.length; i++){
        total_size += selected_files[i]['size'];
    }
    for(var i=0; i<file_dialogue.files.length; i++){
        total_size += file_dialogue.files[i]['size'];
        if(total_size > max_file_size){
            alert('Total size must be less than ' + document.getElementById('max_allowed_file_size').innerHTML + '.');
            return;
        }
    }

    // Validate files
    for(var i=0; i<file_dialogue.files.length; i++){

        var arr_file_name = file_dialogue.files[i]['name'].split('.'),
            extension = arr_file_name[arr_file_name.length-1].toLowerCase(),
            file_name;

        arr_file_name.pop();
        file_name = arr_file_name.join('.');

        for (var j=0 ; j<allowed.length; j++){
            if(extension === allowed[j]){
                // Push into array
                selected_files.push(Array.prototype.slice.call(file_dialogue.files)[i]);

                // Creat html element
                var file = document.createElement('span'),
                    tag = document.createElement('input'),
                    btn_abort =  document.createElement('a'),
                    btn_remove = document.createElement('a');

                file.style.display = 'block';
                file.className = 'attachment_file';
                file.innerHTML = shortenString(file_dialogue.files[i]['name'], 30);

                btn_abort.innerHTML = 'abort';
                btn_abort.className = 'btn_att_file_abort';
                btn_abort.style.display = 'inline-block';
                btn_abort.href = 'javascript:this onclick';

                btn_remove.innerHTML = 'Remove';
                btn_remove.className = 'btn_att_file_remove';
                btn_remove.style.display = 'none';
                btn_remove.href = 'javascript:this onclick';

                tag.className = 'att_file_name';
                tag.name = 'att_file_name[]';
                tag.type = 'hidden';
                tag.value = file_name + '/' + date_string +  '/' + extension;

                // Show file elements in the container
                //file.appendChild(btn_abort);
                file.appendChild(btn_remove);
                file.appendChild(tag);
                document.getElementById('att_file_list').appendChild(file);

                break;
            }
            else{
                if(j === allowed.length - 1 ){
                    fail_files += file_dialogue.files[i]['name'] + '\n';
                }
            }
        }
    }

    // Upload files
    if(selected_files.length > 0){
        document.getElementById('att_file_list').style.display = 'block';
        uploadAttachment(destination);
    }

    // Echo the invalid files
    if(fail_files !== ''){
        alert('These files are not allowed\n' + fail_files);
    }
}

function uploadAttachment(destination){

    for(var i= current_number_file_upload; i<selected_files.length; i++){
        var file_list = new FormData(),
            percent;

        file_list.append('upload_attachment_file[]', selected_files[i]);
        file_list.append('date_string', date_string);
        file_list.append('location', destination);
        file_list.append('index', i);

        var xmlhttp = new XMLHttpRequest();
        xmlhttp.addEventListener('readystatechange', function(data){
            if(this.readyState === 4){
                if(this.status === 200){
                    var file_info = JSON.parse(this.response);

                    //Add event handler to remove button
                    document.getElementsByClassName('btn_att_file_remove')[file_info[0].file_index].addEventListener('click', function(e){
                        var file_element = this.parentNode,
                            parent = file_element.parentNode,
                            current_index,
                            file_info = [];

                        file_info = file_element.querySelector('.att_file_name').value.split('/');

                        // Find current node index
                        for(var x=0; x<parent.childNodes.length; x++){
                            if(parent.children[x] === file_element){
                                current_index = x;
                                break;
                            }
                        }

                        selected_files.splice(current_index,1);
                        --current_number_file_upload;
                        file_element.remove();  // Remove self
                        if(!parent.hasChildNodes()) {
                            // disable show file container
                            document.getElementById('att_file_list').style.display = 'none';
                        }

                        // Remove uploaded file
                        var removing_file = {
                            file_name   : file_info[0],
                            date_string : file_info[1],
                            extension   : file_info[2],
                            location    : destination
                        };
                        removeUploadFile(removing_file);
                    });

                    document.getElementsByClassName('btn_att_file_remove')[file_info[0].file_index].style.display = 'inline-block';
                    //document.getElementsByClassName('btn_att_file_abort')[file_info[0].file_index].style.display = 'none';
                }
            }
        });

        xmlhttp.upload.addEventListener('progress', function(e){
            if(e.lengthComputable === true){
                percent = Math.round((e.loaded / e.total) * 100);
            }
        });

        xmlhttp.open('post', 'ajax/upload_message_attachment.php', true);
        xmlhttp.send(file_list);
    }

    current_number_file_upload = selected_files.length;

    return xmlhttp;
}

function removeUploadFile(o){
    var xmlhttp = new XMLHttpRequest(),
        del_file = new FormData();

    del_file.append('del_attachment_file[file_name]', o.file_name);
    del_file.append('del_attachment_file[extension]', o.extension);
    del_file.append('del_attachment_file[date_string]', o.date_string);
    del_file.append('del_attachment_file[location]' , o.location);

    xmlhttp.addEventListener('readystatechange', function(){
        if(this.readyState === 4){
            if(this.status === 200){
                //console.log(this.response);
            }
        }
    });

    xmlhttp.open('post', 'ajax/upload_message_attachment.php');
    xmlhttp.send(del_file);
}

function shortenString(source, length){
    var index_of_point = source.indexOf('.'),
        extension =  source.substring(index_of_point),
        file_name = source.substring(0, index_of_point),
        file_shorten;

        if(file_name.length > length){
            file_shorten = file_name.substring(0, length) + '...' + extension;
        }
        else{
            file_shorten = file_name + extension;
        }

    return file_shorten;
}

function md5(str) {
  //  discuss at: http://phpjs.org/functions/md5/
  // original by: Webtoolkit.info (http://www.webtoolkit.info/)
  // improved by: Michael White (http://getsprink.com)
  // improved by: Jack
  // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  //    input by: Brett Zamir (http://brett-zamir.me)
  // bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  //  depends on: utf8_encode
  //   example 1: md5('Kevin van Zonneveld');
  //   returns 1: '6e658d4bfcb59cc13f96c14450ac40b9'

  var xl;

  var rotateLeft = function(lValue, iShiftBits) {
    return (lValue << iShiftBits) | (lValue >>> (32 - iShiftBits));
  };

  var addUnsigned = function(lX, lY) {
    var lX4, lY4, lX8, lY8, lResult;
    lX8 = (lX & 0x80000000);
    lY8 = (lY & 0x80000000);
    lX4 = (lX & 0x40000000);
    lY4 = (lY & 0x40000000);
    lResult = (lX & 0x3FFFFFFF) + (lY & 0x3FFFFFFF);
    if (lX4 & lY4) {
      return (lResult ^ 0x80000000 ^ lX8 ^ lY8);
    }
    if (lX4 | lY4) {
      if (lResult & 0x40000000) {
        return (lResult ^ 0xC0000000 ^ lX8 ^ lY8);
      } else {
        return (lResult ^ 0x40000000 ^ lX8 ^ lY8);
      }
    } else {
      return (lResult ^ lX8 ^ lY8);
    }
  };

  var _F = function(x, y, z) {
    return (x & y) | ((~x) & z);
  };
  var _G = function(x, y, z) {
    return (x & z) | (y & (~z));
  };
  var _H = function(x, y, z) {
    return (x ^ y ^ z);
  };
  var _I = function(x, y, z) {
    return (y ^ (x | (~z)));
  };

  var _FF = function(a, b, c, d, x, s, ac) {
    a = addUnsigned(a, addUnsigned(addUnsigned(_F(b, c, d), x), ac));
    return addUnsigned(rotateLeft(a, s), b);
  };

  var _GG = function(a, b, c, d, x, s, ac) {
    a = addUnsigned(a, addUnsigned(addUnsigned(_G(b, c, d), x), ac));
    return addUnsigned(rotateLeft(a, s), b);
  };

  var _HH = function(a, b, c, d, x, s, ac) {
    a = addUnsigned(a, addUnsigned(addUnsigned(_H(b, c, d), x), ac));
    return addUnsigned(rotateLeft(a, s), b);
  };

  var _II = function(a, b, c, d, x, s, ac) {
    a = addUnsigned(a, addUnsigned(addUnsigned(_I(b, c, d), x), ac));
    return addUnsigned(rotateLeft(a, s), b);
  };

  var convertToWordArray = function(str) {
    var lWordCount;
    var lMessageLength = str.length;
    var lNumberOfWords_temp1 = lMessageLength + 8;
    var lNumberOfWords_temp2 = (lNumberOfWords_temp1 - (lNumberOfWords_temp1 % 64)) / 64;
    var lNumberOfWords = (lNumberOfWords_temp2 + 1) * 16;
    var lWordArray = new Array(lNumberOfWords - 1);
    var lBytePosition = 0;
    var lByteCount = 0;
    while (lByteCount < lMessageLength) {
      lWordCount = (lByteCount - (lByteCount % 4)) / 4;
      lBytePosition = (lByteCount % 4) * 8;
      lWordArray[lWordCount] = (lWordArray[lWordCount] | (str.charCodeAt(lByteCount) << lBytePosition));
      lByteCount++;
    }
    lWordCount = (lByteCount - (lByteCount % 4)) / 4;
    lBytePosition = (lByteCount % 4) * 8;
    lWordArray[lWordCount] = lWordArray[lWordCount] | (0x80 << lBytePosition);
    lWordArray[lNumberOfWords - 2] = lMessageLength << 3;
    lWordArray[lNumberOfWords - 1] = lMessageLength >>> 29;
    return lWordArray;
  };

  var wordToHex = function(lValue) {
    var wordToHexValue = '',
      wordToHexValue_temp = '',
      lByte, lCount;
    for (lCount = 0; lCount <= 3; lCount++) {
      lByte = (lValue >>> (lCount * 8)) & 255;
      wordToHexValue_temp = '0' + lByte.toString(16);
      wordToHexValue = wordToHexValue + wordToHexValue_temp.substr(wordToHexValue_temp.length - 2, 2);
    }
    return wordToHexValue;
  };

  var x = [],
    k, AA, BB, CC, DD, a, b, c, d, S11 = 7,
    S12 = 12,
    S13 = 17,
    S14 = 22,
    S21 = 5,
    S22 = 9,
    S23 = 14,
    S24 = 20,
    S31 = 4,
    S32 = 11,
    S33 = 16,
    S34 = 23,
    S41 = 6,
    S42 = 10,
    S43 = 15,
    S44 = 21;

  str = this.utf8_encode(str);
  x = convertToWordArray(str);
  a = 0x67452301;
  b = 0xEFCDAB89;
  c = 0x98BADCFE;
  d = 0x10325476;

  xl = x.length;
  for (k = 0; k < xl; k += 16) {
    AA = a;
    BB = b;
    CC = c;
    DD = d;
    a = _FF(a, b, c, d, x[k + 0], S11, 0xD76AA478);
    d = _FF(d, a, b, c, x[k + 1], S12, 0xE8C7B756);
    c = _FF(c, d, a, b, x[k + 2], S13, 0x242070DB);
    b = _FF(b, c, d, a, x[k + 3], S14, 0xC1BDCEEE);
    a = _FF(a, b, c, d, x[k + 4], S11, 0xF57C0FAF);
    d = _FF(d, a, b, c, x[k + 5], S12, 0x4787C62A);
    c = _FF(c, d, a, b, x[k + 6], S13, 0xA8304613);
    b = _FF(b, c, d, a, x[k + 7], S14, 0xFD469501);
    a = _FF(a, b, c, d, x[k + 8], S11, 0x698098D8);
    d = _FF(d, a, b, c, x[k + 9], S12, 0x8B44F7AF);
    c = _FF(c, d, a, b, x[k + 10], S13, 0xFFFF5BB1);
    b = _FF(b, c, d, a, x[k + 11], S14, 0x895CD7BE);
    a = _FF(a, b, c, d, x[k + 12], S11, 0x6B901122);
    d = _FF(d, a, b, c, x[k + 13], S12, 0xFD987193);
    c = _FF(c, d, a, b, x[k + 14], S13, 0xA679438E);
    b = _FF(b, c, d, a, x[k + 15], S14, 0x49B40821);
    a = _GG(a, b, c, d, x[k + 1], S21, 0xF61E2562);
    d = _GG(d, a, b, c, x[k + 6], S22, 0xC040B340);
    c = _GG(c, d, a, b, x[k + 11], S23, 0x265E5A51);
    b = _GG(b, c, d, a, x[k + 0], S24, 0xE9B6C7AA);
    a = _GG(a, b, c, d, x[k + 5], S21, 0xD62F105D);
    d = _GG(d, a, b, c, x[k + 10], S22, 0x2441453);
    c = _GG(c, d, a, b, x[k + 15], S23, 0xD8A1E681);
    b = _GG(b, c, d, a, x[k + 4], S24, 0xE7D3FBC8);
    a = _GG(a, b, c, d, x[k + 9], S21, 0x21E1CDE6);
    d = _GG(d, a, b, c, x[k + 14], S22, 0xC33707D6);
    c = _GG(c, d, a, b, x[k + 3], S23, 0xF4D50D87);
    b = _GG(b, c, d, a, x[k + 8], S24, 0x455A14ED);
    a = _GG(a, b, c, d, x[k + 13], S21, 0xA9E3E905);
    d = _GG(d, a, b, c, x[k + 2], S22, 0xFCEFA3F8);
    c = _GG(c, d, a, b, x[k + 7], S23, 0x676F02D9);
    b = _GG(b, c, d, a, x[k + 12], S24, 0x8D2A4C8A);
    a = _HH(a, b, c, d, x[k + 5], S31, 0xFFFA3942);
    d = _HH(d, a, b, c, x[k + 8], S32, 0x8771F681);
    c = _HH(c, d, a, b, x[k + 11], S33, 0x6D9D6122);
    b = _HH(b, c, d, a, x[k + 14], S34, 0xFDE5380C);
    a = _HH(a, b, c, d, x[k + 1], S31, 0xA4BEEA44);
    d = _HH(d, a, b, c, x[k + 4], S32, 0x4BDECFA9);
    c = _HH(c, d, a, b, x[k + 7], S33, 0xF6BB4B60);
    b = _HH(b, c, d, a, x[k + 10], S34, 0xBEBFBC70);
    a = _HH(a, b, c, d, x[k + 13], S31, 0x289B7EC6);
    d = _HH(d, a, b, c, x[k + 0], S32, 0xEAA127FA);
    c = _HH(c, d, a, b, x[k + 3], S33, 0xD4EF3085);
    b = _HH(b, c, d, a, x[k + 6], S34, 0x4881D05);
    a = _HH(a, b, c, d, x[k + 9], S31, 0xD9D4D039);
    d = _HH(d, a, b, c, x[k + 12], S32, 0xE6DB99E5);
    c = _HH(c, d, a, b, x[k + 15], S33, 0x1FA27CF8);
    b = _HH(b, c, d, a, x[k + 2], S34, 0xC4AC5665);
    a = _II(a, b, c, d, x[k + 0], S41, 0xF4292244);
    d = _II(d, a, b, c, x[k + 7], S42, 0x432AFF97);
    c = _II(c, d, a, b, x[k + 14], S43, 0xAB9423A7);
    b = _II(b, c, d, a, x[k + 5], S44, 0xFC93A039);
    a = _II(a, b, c, d, x[k + 12], S41, 0x655B59C3);
    d = _II(d, a, b, c, x[k + 3], S42, 0x8F0CCC92);
    c = _II(c, d, a, b, x[k + 10], S43, 0xFFEFF47D);
    b = _II(b, c, d, a, x[k + 1], S44, 0x85845DD1);
    a = _II(a, b, c, d, x[k + 8], S41, 0x6FA87E4F);
    d = _II(d, a, b, c, x[k + 15], S42, 0xFE2CE6E0);
    c = _II(c, d, a, b, x[k + 6], S43, 0xA3014314);
    b = _II(b, c, d, a, x[k + 13], S44, 0x4E0811A1);
    a = _II(a, b, c, d, x[k + 4], S41, 0xF7537E82);
    d = _II(d, a, b, c, x[k + 11], S42, 0xBD3AF235);
    c = _II(c, d, a, b, x[k + 2], S43, 0x2AD7D2BB);
    b = _II(b, c, d, a, x[k + 9], S44, 0xEB86D391);
    a = addUnsigned(a, AA);
    b = addUnsigned(b, BB);
    c = addUnsigned(c, CC);
    d = addUnsigned(d, DD);
  }

  var temp = wordToHex(a) + wordToHex(b) + wordToHex(c) + wordToHex(d);

  return temp.toLowerCase();
}

function utf8_encode(argString) {
  //  discuss at: http://phpjs.org/functions/utf8_encode/
  // original by: Webtoolkit.info (http://www.webtoolkit.info/)
  // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // improved by: sowberry
  // improved by: Jack
  // improved by: Yves Sucaet
  // improved by: kirilloid
  // bugfixed by: Onno Marsman
  // bugfixed by: Onno Marsman
  // bugfixed by: Ulrich
  // bugfixed by: Rafal Kukawski
  // bugfixed by: kirilloid
  //   example 1: utf8_encode('Kevin van Zonneveld');
  //   returns 1: 'Kevin van Zonneveld'

  if (argString === null || typeof argString === 'undefined') {
    return '';
  }

  var string = (argString + ''); // .replace(/\r\n/g, "\n").replace(/\r/g, "\n");
  var utftext = '',
    start, end, stringl = 0;

  start = end = 0;
  stringl = string.length;
  for (var n = 0; n < stringl; n++) {
    var c1 = string.charCodeAt(n);
    var enc = null;

    if (c1 < 128) {
      end++;
    } else if (c1 > 127 && c1 < 2048) {
      enc = String.fromCharCode(
        (c1 >> 6) | 192, (c1 & 63) | 128
      );
    } else if ((c1 & 0xF800) != 0xD800) {
      enc = String.fromCharCode(
        (c1 >> 12) | 224, ((c1 >> 6) & 63) | 128, (c1 & 63) | 128
      );
    } else { // surrogate pairs
      if ((c1 & 0xFC00) != 0xD800) {
        throw new RangeError('Unmatched trail surrogate at ' + n);
      }
      var c2 = string.charCodeAt(++n);
      if ((c2 & 0xFC00) != 0xDC00) {
        throw new RangeError('Unmatched lead surrogate at ' + (n - 1));
      }
      c1 = ((c1 & 0x3FF) << 10) + (c2 & 0x3FF) + 0x10000;
      enc = String.fromCharCode(
        (c1 >> 18) | 240, ((c1 >> 12) & 63) | 128, ((c1 >> 6) & 63) | 128, (c1 & 63) | 128
      );
    }
    if (enc !== null) {
      if (end > start) {
        utftext += string.slice(start, end);
      }
      utftext += enc;
      start = end = n + 1;
    }
  }

  if (end > start) {
    utftext += string.slice(start, stringl);
  }

  return utftext;
}

function format( m, v){
    if (!m || isNaN(+v)) {
        return v; //return as it is.
    }
    //convert any string to number according to formation sign.
    var v = m.charAt(0) == '-'? -v: +v;
    var isNegative = v<0? v= -v: 0; //process only abs(), and turn on flag.

    //search for separator for grp & decimal, anything not digit, not +/- sign, not #.
    var result = m.match(/[^\d\-\+#]/g);
    var Decimal = (result && result[result.length-1]) || '.'; //treat the right most symbol as decimal
    var Group = (result && result[1] && result[0]) || ',';  //treat the left most symbol as group separator

    //split the decimal for the format string if any.
    var m = m.split( Decimal);
    //Fix the decimal first, toFixed will auto fill trailing zero.
    v = v.toFixed( m[1] && m[1].length);
    v = +(v) + ''; //convert number to string to trim off *all* trailing decimal zero(es)

    //fill back any trailing zero according to format
    var pos_trail_zero = m[1] && m[1].lastIndexOf('0'); //look for last zero in format
    var part = v.split('.');
    //integer will get !part[1]
    if (!part[1] || part[1] && part[1].length <= pos_trail_zero) {
        v = (+v).toFixed( pos_trail_zero+1);
    }
    var szSep = m[0].split( Group); //look for separator
    m[0] = szSep.join(''); //join back without separator for counting the pos of any leading 0.

    var pos_lead_zero = m[0] && m[0].indexOf('0');
    if (pos_lead_zero > -1 ) {
        while (part[0].length < (m[0].length - pos_lead_zero)) {
            part[0] = '0' + part[0];
        }
    }
    else if (+part[0] == 0){
        part[0] = '';
    }

    v = v.split('.');
    v[0] = part[0];

    //process the first group separator from decimal (.) only, the rest ignore.
    //get the length of the last slice of split result.
    var pos_separator = ( szSep[1] && szSep[ szSep.length-1].length);
    if (pos_separator) {
        var integer = v[0];
        var str = '';
        var offset = integer.length % pos_separator;
        for (var i=0, l=integer.length; i<l; i++) {

            str += integer.charAt(i); //ie6 only support charAt for sz.
            //-pos_separator so that won't trail separator on full length
            if (!((i-offset+1)%pos_separator) && i<l-pos_separator ) {
                str += Group;
            }
        }
        v[0] = str;
    }

    v[1] = (m[1] && v[1])? Decimal+v[1] : "";
    return (isNegative?'-':'') + v[0] + v[1]; //put back any negation and combine integer and fraction.
};


