jQuery(document).ready(function($) {
	// Tab Sign Up	
	/* $(".tabClick").click(function(){		
		$(this).addClass('bgCurrent').siblings().removeClass('bgCurrent');				
	});
	
	$("#tabCompany").click(function(){
		$("#companyForm").css("display","block");
		$("#personalForm").css("display","none");
	});
	$("#tabPersonal").click(function(){
		$("#personalForm").css("display","block");
		$("#companyForm").css("display","none");
	});
	*/
	
	// Generate a simple captcha
	function randomNumber(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    };
    $('#captchaOperation').html([randomNumber(1, 10), '+', randomNumber(1, 20), '='].join(' '));

	
	// Validation Field input
    $('#defaultForm').bootstrapValidator({
		//        live: 'disabled',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
             user_id: {
                message: 'The ID is not valid',
                validators: {
                    notEmpty: {
                        message: 'The ID is required and cannot be empty'
                    },
 
                    stringLength: {
                        min: 5,
                        max: 25,
                        message: 'ID must be in between 5 to 25 digits'
                    },
                    regexp: {
                        regexp:"^[a-zA-Z0-9]+$",
                        message: 'The ID can only contain letters and numbers'
                    },
					remote: {

                        url: 'ajax/register_chek_available.php',
                        message: 'The ID is not available',
						 data: function(validator) {
                            return {
                                user_id: validator.getFieldElements('user_id').val()
							};
						}
					}
				}
 


            },
			
			company_name:{
				validators:{
					notEmpty: {
                        message: 'The Company Name is required and cannot be empty'
                    },
					regexp: {
                        regexp:"^[a-zA-Z0-9 ]+$",
                        message: 'The Company Name contain only letters, number and space'
                    },
					stringLength: {
                        min: 5,
                        max: 30,
                        message: 'Company Name is between 5 - 30 cahracters'
                    }
				}
			}, 
 
            email: {
                validators: {
					notEmpty: {
                        message: ' '
                    },
                    emailAddress: {
                        message: 'Enter valid email address'
                    },
					remote: {

                        url: 'ajax/register_chek_available.php',
                        message: 'The email is not available',
						 data: function(validator) {
                            return {
                                email: validator.getFieldElements('email').val()
							};
						}
					}
                }
            },
			
			/* company_email: {
                validators: {					
                    emailAddress: {
                        message: 'The input is not a valid email address. If not, registration will be cancelled.'
                    },
					remote: {

                        url: 'ajax/register_chek_available.php',
                        message: 'The email is not available',
						 data: function(validator) {
                            return {
                                email: validator.getFieldElements('email').val()
							};
						}
					}
                }
            }, */					
			
			first_name: {
                validators: {
				    
                    notEmpty: {
                        message: 'The First Name is required and cannot be empty'
                    },
					
					regexp:{
                        regexp:"^[a-zA-Z]+$",
                        message: 'The First Name can only contain letters'
                    },
					
					stringLength: {
                        min: 2,
                        max: 30,
                        message: 'First Name user can enter 2 - 30 digits'
                    },
                }
            },
			
			last_name: {
                validators: {
				    
                    notEmpty: {
                        message: 'The Last Name is required and cannot be empty'
                    },
					
					regexp:{
                        regexp:"^[a-zA-Z]+$",
                        message: 'The Last Name can only contain letters'
                    },
					
					stringLength: {
                        min: 2,
                        max: 30,
                        message: 'Last Name user can enter 2 - 30 digits'
                    },
                }
            },
			
			gender: {
                validators: {
				    
                    notEmpty: {
                        message: 'The Gender is required, Please select your gender'
                    },										
                }
            },
			
			province_city: {
                validators: {
				    
                    notEmpty: {
                        message: 'The City/Province is required, Please select your city/province'
                    },										
                }
            },
			
			mobile: {
                validators: {
				    
                    notEmpty: {
                        message: 'The Phone Number is required and cannot be empty'
                    },
					
					regexp:{
                        regexp:"^[0-9]+$",
                        message: 'The Phone Number can only contain number'
                    },
					
					stringLength: {
                        min: 9,
                        max: 20,
                        message: 'Phone Number must be alleast 9 to 20 digits'
                    },
                }
            },
			
			/* company_phone: {
                validators: {				                       
					
					regexp:{
                        regexp:"^[0-9]+$",
                        message: 'The Phone Number can only contain number'
                    },
					
					stringLength: {
                        min: 9,
                        max: 20,
                        message: 'The Phone Number must be more than 9 and less than 20 characters long'
                    },
                }
            }, */
			
			/* company_fax: {
                validators: {				                       
					
					regexp:{
                        regexp:"^[0-9]+$",
                        message: 'The Phone Number can only contain number'
                    },
					
					stringLength: {
                        min: 4,
                        max: 20,
                        message: 'The Phone Number must be more than 4 and less than 20 characters long'
                    },
                }
            },
			
			company_website: {
				validators: {
                    uri: {
                        message: 'The website address is not valid'
                    }
                }
			}, */
			
			password: {
                validators: {
                    notEmpty: {
                        message: 'The Password is required and cannot be empty'
                    },
					stringLength: {
                        min: 5,
                        max: 30,
                        message: 'Password must be more than 5 to 30 digits'
                    },
                }
            },
			
            confirm_password: {
                validators: {
                    notEmpty: {
                        message: 'The Confirm Password is required and cannot be empty'
                    },
                    identical: {
                        field: 'password',
                        message: 'Type correct password and must be same'
                    },
                    different: {
                        field: 'log_id',
                        message: 'The Password cannot be the same as ID'
                    }
                }
            },
			
			country: {
                validators: {
                    notEmpty: {
                        message: 'The country is required'
                    }
                }
            },
		

           kreusna_reg_name: {
                validators: {
					callback: {
                        message: 'input security number',
                        callback:function(value,validator,$field){
                            var item = parseInt($('#tempSecurityNum').val());
                            return value == item;
                        }
                    }

                }
            },

	
            reg_name: {
                validators: {
					notEmpty: {
                        message: 'input security number'
                    },
					identical: {
                        field: 'temp',
                        message: 'Invalid'
                    }
                }
            }
			
        }
    });
})