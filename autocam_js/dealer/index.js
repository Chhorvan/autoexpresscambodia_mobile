// loading dealer
function showLoading(){
	$("#loading_image").css("display", "block");
}
function hideLoading(){
	$("#loading_image").css("display", "none");	
}

$(document).ready(function() {
	// Loading Bottom
	var loading_bottom = $('#loading_image');
	var showTopLink = 200;

	$(window).scroll( function(){
		var y = $(window).scrollTop();										
		
		if( y > showTopLink  ) {					
			$("img#loading_image").addClass("loadBottom");					
			$("#footer").addClass("padBottom");
		}else {					
			$("img#loading_image").removeClass("loadBottom");
			$("#footer").removeClass("padBottom");
		}
	});
	
	// load dealer	
	$(".loadDealer").load("ajax/dealer/load_dealer.php", function() {
		// hidden loading dealer
		hideLoading();
		$("img.lazy").lazyload({
			   effect : "fadeIn"
		});
	});	
	
	//Ajax events for pagination click	
	$(document.body).on('click', "#pagegination a", function(e){
		current_offset=$(this).attr('offset');			
		reloadvechicle(current_offset);
		return false;		
	});
	
	//reload my inventory body function
	function reloadvechicle(setOffset){
		//ajax loading item list
		showLoading();

		$("#offset").val(setOffset);
		// $("#search_wrap form #owner").val(owner);

	   var search_filter=$('#load_form').serializeArray();

		$.ajax({
			type:"get",
			url: "ajax/dealer/load_dealer.php",
			data: $.param(search_filter),
			success: function(data){
				//do stuff after the AJAX calls successfully completes
				hideLoading();
				$(".loadDealer").html(data);
				$("img.lazy").lazyload({
					effect : "fadeIn"
				});
			}
		});
	}				
});

// jQuery for dealer search
$(function(){
	$(".search").keyup(function() { 
		var searchid = $(this).val();
		var dataString = 'search='+ searchid;
		
		if(searchid){
			$.ajax({
				type: "POST",
				url: "ajax/dealer/load_dealer_search.php",
				data: dataString,
				cache: false,
				success: function(html){
					$("#result").html(html).show();
				}
			});
		}
		return false;    
	});
	 
	jQuery(document).live("click", function(e) { 
		var $clicked = $(e.target);
		if (! $clicked.hasClass("search")){
		jQuery("#result").fadeOut(); 
		}
	});		

	jQuery(".show").live("click",function(e){
		$name = $('span.name', this).html();
		var decoded = $("<div/>").html($name).text();
		$('#searchid').val(decoded);
		
		// click submit
		s_dealer = $('#searchid').val();	
		s_dealer=s_dealer.split(' ').join('-');
		if(s_dealer!=''){
			s_dealer="s_dealer="+s_dealer;			
		}
		
		$(".loadDealer").load("ajax/dealer/load_dealer.php?"+s_dealer, function() {
			// hidden loading dealer
			hideLoading();
			$("img.lazy").lazyload({
				   effect : "fadeIn"
			});
		});
	});	
	
	$('#searchid').click(function(){
		jQuery("#result").fadeIn();
	});
})