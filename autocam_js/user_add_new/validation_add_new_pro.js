// JavaScript Document
$('.form-herizontal').submit(function(){
	
	$('input[type="text"]').removeClass('text-box-err');
	$('.error').html('');
	
	//if you user didn't check to agree our term of use and policy			
	//if($('#agree').prop('checked') == false){
//		$('.warming').html('You need to agree our term of use and policy!');
//		$('.warming').fadeIn('slow');		
//		return false;
//	}else{
//		$('.warming').fadeOut('slow');
//	}
	
	$.post($('.form-herizontal').attr('action'), $('.form-herizontal').serialize(), function(data) {
		
		if (data.status == true) {
			$('.succed').html('You have successfully add new product!');
			$('.succed').fadeIn('slow');
			$('.form-herizontal').trigger('reset');
		}else{
			$.each(data.errors, function(key, val) {			
				$('#'+ key).addClass('text-box-err');
				$('#'+ key +'_err').html(val);
				$('.error').fadeIn('slow');
			})
		}
	}, 'json');
	
	return false;
});