
      $(function() {
            $("img.imgThNail").mouseover(function(e) {
                  var imgName = $(this).attr("src");
                  $(this).css({
                        opacity:0.5
                  });

                  $("#loadImage").css({
                        display: 'none'
                  });
                  $("#Img1").css({
                        display: 'block'
                  });
                  $("#loadImage").attr('src', imgName).load(function() {
                        $("#Img1").css({
                              display: 'none'
                        });
                        $("#loadImage").css({
                              display: 'block'
                        });
                  });
            });

            $("img.imgThNail").mouseout(function(e) {
                  $(this).css({
                        opacity:1
                  });
				  
				  
            });
      });
		$(document).ready(function() {
			/*
			*   Examples - images
			*/

			$("a#img1").fancybox();

			$("a#img2").fancybox({
				'overlayShow'	: false,
				'transitionIn'	: 'elastic',
				'transitionOut'	: 'elastic'
			});

			$("a#img3").fancybox({
				'transitionIn'	: 'none',
				'transitionOut'	: 'none'	
			});

			$("a#img4").fancybox({
				'opacity'		: true,
				'overlayShow'	: true,
				'transitionIn'	: 'elastic',
				'transitionOut'	: 'none'
			});


			$("a[rel=example_group]").fancybox({
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'titlePosition' 	: 'over',
				'titleFormat'		: function(title, currentArray, currentIndex, currentOpts) {
					return '<span id="fancybox-title-over">Image ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
				}
			});

			/*
			*   Examples - various
			*/

			$("#various1").fancybox({
				'titlePosition'		: 'inside',
				'transitionIn'		: 'none',
				'transitionOut'		: 'none'
			});

			$("#various2").fancybox();

			$("#various3").fancybox({
				'width'				: '75%',
				'height'			: '75%',
				'autoScale'			: false,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'type'				: 'iframe'
			});

			$("#various4").fancybox({
				'padding'			: 0,
				'autoScale'			: false,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none'
			});
		});