// base url
var baseurl = document.getElementById('baseurl').value;

$(document).ready(function($) {			
	// Loading Bottom
	var loading_bottom = $('#loading_image');
	var showTopLink = 330;

	$(window).scroll( function(){
		var y = $(window).scrollTop();										
		
		if( y > showTopLink  ) {					
			$("img#loading_image").addClass("loadBottom");					
		}else {					
			$("img#loading_image").removeClass("loadBottom");
		}
	});
	
	// load make
	var make = "";
	$('#product_make').change(function(e) {
			//product_type=product_type.split(' ').join('+');	
			var make = $(this).val();
			make = make.split(' ').join('+');				
			$('#product_model').load('ajax/load_car_model.php?make='+make);
			if(make=$(this).click()){
				$('#product_model').reload();
			}				
	});	
	
	// text box search	
	$("#searchCar").on("click", function () {
		$('#product_make option').prop('selected', function() {
			return this.defaultSelected;
		});
		
		$('#product_model option').prop('selected', function() {
			return this.defaultSelected;
		});
		$('#product_year option').prop('selected', function() {
			return this.defaultSelected;
		});
	});	
	$(".selectCar").on("click", function () {
		document.getElementById("searchCar").value = "";
	});

	var current_offset="0";
	//Ajax events for pagination click
	$(document.body).on('click', "#pagernation a", function(e){
		current_offset=$(this).attr('offset');
		reloadvechicle(current_offset);
		return false;
	});
	//reload my inventory body function
	function reloadvechicle(setOffset){
		//ajax loading item list
		showLoading();

         $("#offset").val(setOffset);
		// $("#search_wrap form #owner").val(owner);

       var search_filter=$('#load_form').serializeArray();

        $.ajax({
            type:"get",
            url: baseurl+"ajax/vechicle/load_vechicle.php",
            data: $.param(search_filter),
            success: function(data){
            //do stuff after the AJAX calls successfully completes
				hideLoading();
            	$(".load_Car").html(data);
				$("img.lazy").lazyload({
					effect : "fadeIn"
		        });

            }
        });
	}	
	
});

/* Submit value */
$('#submit').click(function(event) {	
	// loading vechicle
	showLoading();
				
	search_car = $('#searchCar').val();		
	if(search_car!=''){		
		search_car=search_car.split(' ').join('+');
		search_car="&search_car="+search_car;		
	}
	
	make = $('#product_make option:selected').val();		
	 if(make!=''){
		make=make.split(' ').join('+');
		make="&make="+make;
	}
	
	model = $('#product_model option:selected').val();		
	 if(model!=''){
		model=model.split(' ').join('+');
		model="&model="+model;
	}
	
	year = $('#product_year option:selected').val();
	if(year!=''){
		year="&year="+year;
	}		

	$(".load_Car").load(baseurl+"ajax/vechicle/load_vechicle.php?result=1"+vehicle_type+search_car+make+model+year, function() {
		// hidden loading vechicle
		hideLoading();
		$("img.lazy").lazyload({
			   effect : "fadeIn"
		});
	});						
});

// jQuery for dealer search
$(function(){
	$(".search").keyup(function() { 
		// enable arrow keyup
		if(e.keyCode == 40){  
            Navigate(1);
        }
        if(e.keyCode==38){
            Navigate(-1);
        }
	
		var searchid = $(this).val();
		var dataString = 'search='+ searchid;
		
		if(searchid != ""){
			$("#result").css("height", "300px");
		}else{
			$("#result").css("height", "0");
		}
		
		if(searchid){
			$.ajax({
				type: "POST",
				url: baseurl+"ajax/vechicle/load_vehicle_search.php",
				data: dataString,
				cache: false,
				success: function(html){
					$("#result").html(html).show();
				}
			});
		}
		return false;    
	});
	 
	jQuery(document).live("click", function(e) { 
		var $clicked = $(e.target);
		if (! $clicked.hasClass("search")){
		jQuery("#result").fadeOut(); 
		}
	});		

	jQuery(".show").live("click",function(e){
		$name = $('span.name', this).html();
		var decoded = $("<div/>").html($name).text();
		$('#searchCar').val(decoded);
		
		// click submit
		search_car = $('#searchCar').val();	
		search_car=search_car.split(' ').join('+');
		if(search_car!=''){
			search_car="search_car="+search_car;			
		}
		
		$(".load_Car").load(baseurl+"ajax/vechicle/load_vechicle.php?"+search_car, function() {
			// hidden loading dealer
			hideLoading();
			$("img.lazy").lazyload({
				   effect : "fadeIn"
			});
		});
	});	
	
	$('#searchCar').click(function(){
		jQuery("#result").fadeIn();
	});
})