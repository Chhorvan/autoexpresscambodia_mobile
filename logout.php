<?php
if(!isset($_SESSION)) @session_start();
require_once 'core/class-social-network.php';
require_once 'config.php';
$social_network=new socialnetwork();
session_destroy();
$social_network->logoutAllProvider();
//setcookie("ussid","",time()-3600,'/');
if(isset($_COOKIE['ussid'])) $logout_session="?logout=".htmlspecialchars($_COOKIE['ussid']); else $logout_session='';
header("Location: " . BASE_RELATIVE . $logout_session);
exit;