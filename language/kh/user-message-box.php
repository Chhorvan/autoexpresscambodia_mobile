<?php
$_LANG = array(
    'DELETE_COMPLETE_STATUS' => 'Message has been deleted successfully!',
    'DELETE_COMPLETE_LABEL' => 'Message has been deleted successfully!',
    'DELETE_SELECT_ERROR_STATUS' => 'Please select any message to delete!',
    'DELETE_SELECT_ERROR_LABEL' => 'Please select any message to delete!',
    'DELETE_ERROR_STATUS' => 'Some messages could not delete!',
    'DELETE_ERROR_LABEL' => 'Some messages could not delete!'
);