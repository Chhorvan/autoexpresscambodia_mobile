<?php
$_LANG = array(
    'PAGE_DELETE_QUERY_SUCCESS'=> 'Shipping was deleted!',
    'PAGE_DELETE_QUERY_ERROR' => 'Unable to delete from database!',
    'PAGE_NOT_FOUND_QUERY_ERROR' => 'This item does not exist or you may not have permission to delete it!',
    
);