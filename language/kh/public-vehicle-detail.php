<?php
$_LANG = array(
    'REGISTER_COMPLETE_STATUS' => 'Message has been sent successfully!',
    'REGISTER_COMPLETE_LABEL' => 'Message has been sent successfully!',
    'REGISTER_ERROR_STATUS' => 'Message sending failed!',
    'REGISTER_ERROR_LABEL' => 'Message sending failed!',
    'REGISTER_SESSION_STATUS' => 'Please login first!',
    'REGISTER_SESSION_LABEL' => 'Please login first!',
	'PUBLIC_VEHICLE_DETAIL_QUICK_SEARCH' => 'ស្វែងរក',
    'PUBLIC_VEHICLE_DETAIL_ANY_MAKER_OPTION' => 'ក្រុមហ៊ុនផលិត',
    'PUBLIC_VEHICLE_DETAIL_ANY_MODEL_OPTION' => 'ម៉ាក',
    'PUBLIC_VEHICLE_DETAIL_SELECT_TYPE_LABEL' => 'ប្រភេទរថយន្ត',
	'PUBLIC_VEHICLE_DETAIL_MIN_YEAR_LABEL' => 'ចាប់ពីឆ្នាំ',
	'PUBLIC_VEHICLE_DETAIL_MAX_YEAR_LABEL' => 'ដល់ឆ្នាំ',
	'PUBLIC_VEHICLE_DETAIL_MIN_PRICE_LABEL' => 'ចាប់ពីតម្លៃ',
	'PUBLIC_VEHICLE_DETAIL_MAX_PRICE_LABEL' => 'ដល់តម្លៃ',
    'PUBLIC_VEHICLE_DETAIL_SEARCH_BUTTON_LABEL' => 'ស្វែងរក',
	'PUBLIC_VEHICLE_DETAIL_SPECIAL_DEAL' => 'ផលិតផលច្រើនទៀតពីអ្នកលក់',
	'PUBLIC_VEHICLE_DETAIL_STEERING' => 'ដៃចង្កូត',
	'PUBLIC_VEHICLE_DETAIL_FUEL_TYPE' => 'ប្រភេទប្រេង',
	'PUBLIC_VEHICLE_DETAIL_MILEAGE' => 'ចំងាយ',
	'PUBLIC_VEHICLE_DETAIL_USED_CAR' => 'ឡានមួយទឹក',
	'PUBLIC_VEHICLE_DETAIL_NEW_CAR' => 'ឡានថ្មី',
	'PUBLIC_VEHICLE_DETAIL_MOST_POPULAR_BRANDS' => 'រថយន្តកំពុងពេញនិយម',
	'PUBLIC_VEHICLE_DETAIL_MOBILE' => 'ទូរស័ព',
	'PUBLIC_VEHICLE_DETAIL_LOCATION' => 'ខេត្ត / ក្រុង',
	'PUBLIC_VEHICLE_DETAIL_CONTACT' => 'ទំនាក់ទំនង',
	'PUBLIC_VEHICLE_DETAIL_EMAIL' => 'អ៊ីម៉ែល',
	'PUBLIC_VEHICLE_DESCRIPTION' => 'ពត៌មានទូទៅ',
	'PUBLIC_VEHICLE_DETAIL_HIT' => 'ចូលមើល',
	'PUBLIC_VEHICLE_DETAIL_CATEGORY' => 'ប្រភេទ',
	'PUBLIC_VEHICLE_DETAIL_STOCK' => 'លេខកូដ',
	'PUBLIC_VEHICLE_DETAIL_COLOR' => 'ព៌ណ',
	'PUBLIC_VEHICLE_DETAIL_MODEL_YEAR' => 'ឆ្នាំផលិត',
	'PUBLIC_VEHICLE_DETAIL_CHASSIS' => 'លេខម៉ាសីុន',
	'PUBLIC_VEHICLE_DETAIL_BODY' => 'ប្រភេទរថយន្ត',
	'PUBLIC_VEHICLE_DETAIL_ENGINE' => 'ម៉ាសីុន',
	'PUBLIC_VEHICLE_DETAIL_TRANSMISSION' => 'ប្រអប់លេខ',
	'PUBLIC_VEHICLE_PASSENGER' => 'ចំនួនកៅអី',
	'PUBLIC_VEHICLE_DRIVE' => 'ប្រភេទ កាឡេកង់',
	'PUBLIC_VEHICLE_DETAIL_SELLER' => 'អ្នកលក់',
	
);