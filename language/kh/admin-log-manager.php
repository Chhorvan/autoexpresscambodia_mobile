<?php
$_LANG = array(
    'LOG_DELETE_FW_LOG_ERROR' => 'Unable to delete log file due an internal error.',
    'LOG_DELETE_FW_LOG_SUCCESS' => 'Log entry removed successfully!',
    'LOG_HEADER' => 'Error log manager',
    'LOG_NO_ERRORS_LOG' => 'The system is operating normally. No errors registered.',
    'LOG_DATE_LABEL' => 'Date',
    'LOG_DOWNLOAD_LABEL' => 'Download',
    'LOG_DELETE_LABEL' => 'Delete'
);