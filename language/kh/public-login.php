<?php
$_LANG = array(
    'LOGIN_STATUS'=>'ចូលក្នុង',
	'LOGIN_EMAIL'=>'ឈ្មោះសំគាល់',
	'LOGIN_PASSWORD'=>'លេខសំងាត់',
	'LOGIN_REMEMBER_ME'=>'ចងចាំខ្ញុំ',
	'LOGIN_FORGOT_ID'=>'ភ្លេចអត្តលេខ',
	'LOGIN_FORGOT_PASSWORD'=>'ភ្លេចលេខសំងាត់',
	'LOGIN_REMEMBER'=>'មិនទាន់ជាសមាជិក ',
	'LOGIN_REGISTER'=>'ចុះឈ្មោះ'
);