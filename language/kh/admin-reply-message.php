<?php
$_LANG = array(
    'REPLAY_SUBJECT_REQUIRED' => 'The subject field is required.',
    'REPLAY_INVALID_USER_EMAIL' => 'The user\'s e-mail address is invalid. The message cannot be sent.',
    'REPLAY_MAIL_FAILURE' => 'Unable to send e-mail to user due an internal error. See log for more details.',
    'REPLAY_STATUS_UPDATE_FAILURE' => 'Your message has been sent but message status could not be updated due an internal error.',
    'REPLAY_SUCCESS' => 'Message replied successfully!',
    'REPLY_TO' => 'Reply message to',
    'REPLY_SUBJECT' => 'Subject',
    'REPLY_GOBACK_LABEL' => 'Go Back'
);