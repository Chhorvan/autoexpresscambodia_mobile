<?php
$_LANG = array(
    'EDIT_FEAT_LOAD_QUERY_ERROR' => 'Unable to load feature list due an internal error. See log for details.',
    'EDIT_FEAT_UPDATE_BUTTON' => 'Save changes',
    'EDIT_FEAT_HEADER' => 'Manage featured',
    'EDIT_FEAT_QUERY_ERROR' => 'Unable to update vehicle features due an internal error. See log for details.',
    'EDIT_FEAT_QUERY_SUCCESS' => 'Vehicle features updated successfully!'
);