<?php
$_LANG = array(
    'CMS_EDIT_INVALID_PAGE_REQUEST' => 'Invalid page request. Please go back and try again.',
    'CMS_EDIT_PAGE_QUERY_ERROR' => 'Unable to load page content due an internal error.',
    'CMS_EDIT_NO_CONTENT_RESULT' => 'Unable to load page content. The page request seems to be invalid.',
    'CMS_EDIT_CONTENT_UPDATE_QUERY_ERROR' => 'Unable to update page content due an internal error. See log for details.',
    'CMS_EDIT_CONTENT_UPDATE_SUCCESS' => 'The page has been successfully updated!',
    'CMS_EDIT_HEADER_LABEL' => 'Page content editor',
    'CMS_EDIT_SAVE_BUTTON' => 'Save changes',
    'CMS_EDIT_REQUEST_ERROR_LABEL' => 'Invalid page request. Please go back and try again.',
    'CMS_EDIT_EDIT_CSS_BUTTON' => 'Edit CSS',
    'CMS_EDIT_EDIT_JS_BUTTON' => 'Edit Javascript',
    'CMS_EDIT_CSS_HEADER' => 'Edit CSS',
    'CMS_EDIT_JS_HEADER' => 'Edit Javascript',
    'CMS_EDIT_CSS_WRITE_ERROR' => 'Unable to update CSS due an internal error. See log for details.',
    'CMS_EDIT_CSS_WRITE_SUCCESS' => 'CSS updated successfully!',
    'CMS_EDIT_JS_WRITE_ERROR' => 'Unable to update JS due an internal error. See log for details.',
    'CMS_EDIT_JS_WRITE_SUCCESS' => 'JS updated successfully!'
);