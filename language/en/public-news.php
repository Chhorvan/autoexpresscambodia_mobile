<?php
$_LANG = array(
    'NEWS_HEADER' => 'Latest news',
    'NEWS_NONEWS_LABEL' => 'There are no news at the moment. Check it out later!',
    'NEWS_QUERY_ERROR' => 'Unable to load news due an internal error. Please notify the support.'
);