<?php
$_LANG = array(
    'PAGE_SAVE_QUERY_SUCCESS' => 'Member types were changed successfully.',
    'PAGE_SAVE_QUERY_ERROR' => 'Error! Some member types could not be changed. See the log file for more info.'
    
);