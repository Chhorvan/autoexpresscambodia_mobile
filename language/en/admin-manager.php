<?php
$_LANG = array(
    'MANAGE_NEWS_HEADER' => 'Manage news',
    'MANAGE_NEWS_LOAD_QUERY_ERROR' => 'Unable to load news list due an internal error. See log for details.',
    'MANAGE_NEWS_NORESULT_LABEL' => 'There are no news to be managed.',
    'MANAGE_NEWS_REMOVE_CONFIRM' => 'Are you sure you wish to permanently remove this news?',
    'MANAGE_NEWS_QUERY_ERROR' => 'Unable to remove news due an internal error. See log for details.',
    'MANAGE_NEWS_QUERY_SUCCESS' => 'News removed successfully!'
);