<?php
$_LANG = array(
    'GROUPS_LOAD_GROUP_QUERY_ERROR' => 'Unable to load group list due an internal error. See log for details.',
    'GROUPS_CONFIRM_DELETE' => 'Are you sure you wish to permanently remove this group?\rAll users within the group will no longer have access to their accounts.',
    'GROUPS_INVALID_ID' => 'Please use the manager controls to remove groups.',
    'GROUPS_DELETE_QUERY_ERROR' => 'Unable to remove group due an internal error. See log for details.',
    'GROUPS_DELETE_QUERY_SUCCESS' => 'Group removed successfully!',
    'GROUPS_SAVE_EMPTY_GROUP_ERROR' => 'The field group is required.',
    'GROUPS_SAVE_QUERY_ERROR' => 'Unable to save group due an internal error. See log for details.',
    'GROUPS_SAVE_QUERY_SUCCESS' => 'Group save successfully!',
    'GROUPS_SAVE_RESERVED_ERROR' => 'The group name you are trying to use is reserved to the system.',
    'GROUPS_NEW_GROUP_LABEL' => 'New group',
    'GROUPS_IMPORTANT_LABEL' => '<span class="label label-important">IMPORTANT</span> The group <b>admin</b> and <b>public</b> are reserved by the system.',
    'GROUPS_GROUP_NAME_LABEL' => 'Group name',
    'GROUPS_GROUP_NAME_PLACEHOLDER' => 'Enter a group name. Use only characters and numbers.',
    'GROUPS_SAVE_LABEL' => 'Save group',
    'GROUPS_NOGROUP_LABEL' => 'There are no groups to be managed.',
    'GROUPS_DELETE_BUTTON_LABEL' => 'Delete',
    'GROUPS_GROUP_LABEL' => 'Group'
);