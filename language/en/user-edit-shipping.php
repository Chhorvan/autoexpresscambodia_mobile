<?php
$_LANG = array(
    'PAGE_HEADER' => 'Add New Shipping',
	'PAGE_EDIT_HEADER' => 'Edit Shipping',
    
    'PAGE_PRICE_LABEL' => 'Price ($)',
    'PAGE_PRICE_PLACEHOLDER' => '32000',
    'PAGE_PORT_LOADING_LABEL' => 'Port of Loading',
    'PAGE_PORT_DESTINATION_LABEL' => 'Port of Destination',
	'PAGE_PORT_PLACEHOLDER' => 'Other port',

    'PAGE_VESSEL_TYPE_LABEL' => 'Vessel Type',
    'PAGE_VESSEL_TYPE_PLACEHOLDER' => 'Other vessel type',
    'PAGE_SIZE_LABEL' => 'Size',
    'PAGE_SIZE_PLACEHOLDER' => 'Other size',

    'PAGE_OCEAN_FREIGHT_LABEL' => 'Ocean Freight',
    'PAGE_TRANSIT_TIME_LABEL' => 'Transit Time',

    'PAGE_LINE_LABEL' => 'Line',
    'PAGE_LINE_PLACEHOLDER' => 'Other line',
    'PAGE_REMARK_LABEL' => 'Remark',
    'PAGE_SAVE_BUTTON' => 'Save Now',
    'PAGE_SAVE_QUERY_ERROR' => 'Unable to save shipping info!',
    'PAGE_SAVE_QUERY_SUCCESS' => 'Shipping info saved successfully!'
);