<?php
$_LANG = array(
    'MANAGE_NEWS_HEADER' => 'Manage news',
    'MANAGE_TRUNCATE_QUERY_ERROR' => 'Unable to truncate data_count due a query error. See log for details.',
    'MANAGE_NEWS_NORESULT_LABEL' => 'There are no news to be managed.',
    'MANAGE_NEWS_REMOVE_CONFIRM' => 'Are you sure you wish to permanently remove this news?',
    'MANAGE_SAVED_QUERY_ERROR' => 'Unable to save count to data_count due a query error. See log for details.',
    'MANAGE_SAVED_QUERY_SUCCESS' => 'Data count executed successfully!'
);