<?php
$_LANG = array(
    'IMPORT_HEADER' => 'Import CSV List',
    'IMPORT_IMPORTANT_LABEL' => 'IMPORTANT',
    'IMPORT_IMPORTANT_MESSAGE' => 'Make sure the CSV file you are trying to import complies with the required format. Using non-standard formats may result in unexpected errors. A sample for the CSV can be found on the <code>docs</code> directory.',
    'IMPORT_BUTTON_LABEL' => 'Import',
    'IMPORT_INVALID_FILE_FORMAT' => 'Invalid file format. Please care to read notice above before uploading a list.',
    'IMPORT_MAX_FILESIZE' => 'The file you are trying to use is too large. The maximum file size allowed in your server is ',
    'IMPORT_UPLOAD_ERROR_MESSAGE' => 'Unable to upload CSV file due an internal error. See log for details.',
    'IMPORT_QUERY_CHECK_ERROR' => 'Unable to import CSV file due an internal error. See log for details.',
    'IMPORT_QUERY_ERROR' => 'Unable to import CSV list due an internal error. See log for details.',
    'IMPORT_QUERY_SUCCESS' => 'List imported successfully! Make sure to check the imported vehicles!'
);