<?php
$_GLOB = array(
    'UNAUTHORIZED_ACCESS_MESSAGE' => 'You must have the proper permissions to view this page.',
     
    
    // ADMINISTRATOR MENU ------------------------------------------------------
    // CONTACT
    'ADMIN_MENU_CONTACT_TAB' => 'Contacts',
    'ADMIN_MENU_INBOX_LABEL' => 'Inbox',
    'ADMIN_MENU_NEWMESSAGE_LABEL' => 'Compose a Message',
    'ADMIN_MENU_EDIT_TEMPLATE_LABEL' => 'Edit E-mail Template',
    
    // GENERAL
    'ADMIN_MENU_GENERAL_TAB' => 'General',
    'ADMIN_MENU_FRAMEWORK_SETTINGS_LABEL' => 'Framework Settings',
    'ADMIN_MENU_MANAGE_ACCOUNTS_LABEL' => 'Manage Accounts',
    'ADMIN_MENU_TERMS_OF_USE_LABEL' => 'Terms of Use Policy',
    'ADMIN_MENU_PRIVACY_POLICY_LABEL' => 'Privacy Policy',
    'ADMIN_MENU_GROUP_MAMAGER_LABEL' => 'Manage User Groups',
    'ADMIN_MENU_LOG_MAMAGER_LABEL' => 'Log Manager',
    'ADMIN_MENU_COMPANY_DETAILS_LABEL' => 'Company details',
    
    // MODULES MENU
    'ADMIN_MENU_PRETTY_PHOTO_SETTINGS' => 'PrettyPhoto Settings',
    'ADMIN_MENU_NIVO_SLIDER_SETTINGS' => 'Nivo Slider Setting',
    'ADMIN_MENU_MODULES_HEADER_LABEL' => 'Modules',
    'ADMIN_MENU_UPDATE_INSTALLER' => 'Update Installer',
    
    // CMS MENU
    'ADMIN_MENU_CMS_MANAGER_HEADER_LABEL' => 'CMS Manager',
    'ADMIN_MENU_MENU_MANAGER_LABEL' => 'Menu Anchor Manager',
    'ADMIN_MENU_CREATE_NEW_MENU_LABEL' => 'Menu Manager',
    'ADMIN_MENU_CREATE_NEW_PAGE_LABEL' => 'Create a New Page',
    'ADMIN_MENU_PAGE_MANAGER_LABEL' => 'Pages Manager',
    'ADMIN_MENU_THEME_HEADER_LABEL' => 'Theme',
    'ADMIN_MENU_THEME_MANAGER_LABEL' => 'Theme Manager',
    
    // ADMIN TOP MENU
    'ADMIN_TOPMENU_HOME_LABEL' => 'Home',
    'ADMIN_TOPMENU_LIVECHAT_LABEL' => 'Live Support Manager',
    
    // PUBLIC MENU HEADER
    'PUBLIC_TOPMENU_HOME_LABEL' => 'Home',
    'PUBLIC_TOPMENU_CATEGORY_LABEL' => 'Browse vehicles',
    'PUBLIC_TOPMENU_SELLER_LABEL' => 'Sell your car',
    'PUBLIC_TOPMENU_ABOUTUS_LABEL' => 'Financing',
    'PUBLIC_TOPMENU_MYACCOUNT_LABEL' => 'Find Us',
    // GLOBAL
    'LOGOUT_ANCHOR' => 'Logout',
    'PUBLIC_MENU_GENERAL_LABEL' => 'General',
    
    // TOP MENU HEADER
    'TOP_MENU_REGISTER' => 'Register',
    'TOP_MENU_LOGIN' => 'Login',
    'TOP_MENU_SUPPORT' => 'support',
    
    // CAR MANAGER TAB
    'TOP_MENU_CAR_MANAGER_HEADER' => 'Car Manager',
    'TOP_MENU_NEW_CAR_LABEL' => 'Add new car',
    'TOP_MENU_CAR_MANAGER_LABEL' => 'Manage cars',
    'TOP_MENU_RESERVATION_MANAGER_LABEL' => 'Car reservation manager',
    'TOP_MENU_FEATURE_ICON_LABEL' => 'Featured icon manager',
    'TOP_MENU_RESERVATION_POLICY_LABEL' => 'Edit reservation policy',
    'TOP_MENU_IMPORT_LIST' => 'Import CSV list',
    
    // NEWS TAB
    'TOP_MENU_NEWSLETTER_LABEL' => 'Send newsletter',
    'TOP_MENU_NEWS_HEADER' => 'News Manager',
    'TOP_MENU_WRITE_NEWS_LABEL' => 'Add news',
    'TOP_MENU_NEWS_MANAGER_LABEL' => 'Manage news',
    
    // MANAGE STATIC PAGES
    'TOP_MENU_STATICPAGE_HEADER' => 'Manage Static Pages',
    'TOP_MENU_STATICPAGE_LABEL' => 'Manage static pages',
    
    // USER MENU
    'TOP_MENU_MY_ACCOUNT_LABEL' => 'My account',
    'TOP_MENU_CAR_RESERVATION_LABEL' => 'My reservations',
    'TOP_MENU_CAR_FAVORITES_LABEL' => 'Favorites list',
    
    // FOOT HEADER
    'FOOT_HEADER_RIGHTS_RESERVED' => 'All rights reserved. Copyrights',
    'FOOT_HEADER_ABOUTUS_LABEL' => 'About Us',
    'FOOT_HEADER_TOU_LABEL' => 'Terms of Use',
    'FOOT_HEADER_PRIVACY_LABEL' => 'Privacy',
    'FOOT_HEADER_PRESS_LABEL' => 'Press',
    'FOOT_HEADER_TERMS' => 'About our company',
    'FOOT_HEADER_LANGUAGE_HEADER' => 'Language',
    'FOOT_HEADER_SOCIAL_HEADER' => 'Social Network'
);
