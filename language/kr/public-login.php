<?php
$_LANG = array(
    'LOGIN_INVALID_EMAIL' => 'Invalid ID or password.',
    'LOGIN_QUERY_ERROR' => 'Unable to authenticate account due an internal error. Please contact the support.',
    'LOGIN_BANNED_ACCOUNT' => 'Your account has been blocked. Please contact the support to regard the issue.',
    'LOGIN_NON_ACTIVE_ACCOUNT' => 'Please activate your account before login in. The activation code has been sent to your e-mail box.',
    'LOGIN_INVALID_EMAIL_RECOVER' => 'Please enter a valid e-mail address.',
    'LOGIN_QUERY_ERROR_RECOVER' => 'Unable to validate e-mail address due an internal error. Please contact the support.',
    'LOGIN_QUERY_ERROR_REG_RECOVER' => 'Unable to validate account. Please contact the support.',
    'LOGIN_EMAIL_MESSAGE' => 'Dear user, please click the link below to reset your password.<br>If you did not request your password recovery, ignore this message. The activation link will be automatically disabled in 24 hours.',
    'LOGIN_RESET_LABEL' => 'Reset my password now!',
    'LOGIN_EMAIL_SUBJECT' => 'Password recovery',
    'LOGIN_QUERY_ERROR_EMAIL_RECOVER' => 'Unable to send reset link to your e-mail box due an internal error. Please contact the support.',
    'LOGIN_EMAIL_COMPLETE' => 'A message has been sent to your email box. Follow the instructions on the e-mail to reset your password.',
    'LOGIN_EMAIL_LABEL' => 'E-mail address',
    'LOGIN_EMAIL_PLACEHOLDER' => 'Enter the e-mail address you used to register your account.',
    'LOGIN_PASSWORD_LABEL' => 'Password',
    'LOGIN_PASSWORD_PLACEHOLDER' => 'Enter your password.',
    'LOGIN_LOGIN_BUTTOM' => 'Sign In',
    'LOGIN_FORGOT_PASS_PLACEHOLDER' => 'Use the form below to retrieve your password. The password reset instructions will be attached to the e-mail sent to you through this form.',
    'LOGIN_EMAIL_LABEL' => 'E-mail address',
    'LOGIN_HEADER' => 'Account Authentication'
);